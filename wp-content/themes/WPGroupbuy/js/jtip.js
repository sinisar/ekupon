shop.JT_init = function(input){
  var parent = (input == null) ? 'a.jTip' : input+'.jTip';
  jQuery(parent).hover(
	function(){
	  shop.JT_show(jQuery('span.content' + this.id).html(), parseInt(jQuery('.JT_width'+ this.id).val()));
	},
	function(){
	  jQuery('#JT').remove();
	}
  );
  jQuery(parent).mousemove(function(e){
	var jt_height = jQuery('#JT').height(),	jt_width  = jQuery('#JT').width(), ox = e.pageX + 15,
	oy = e.pageY - 5, border = shop.is_ie() ? 25 : 1, bpl = "left", bpt = "10px",
	pad = "0 0 0 17px",	max_x  = jQuery(document).width() - border,
	wHeight = jQuery(window).height();
	if (self.innerHeight) { // Everyone but IE
	  max_y  = window.pageYOffset + jQuery(window).height() - border;
	} else if (document.documentElement && document.documentElement.clientHeight) { // IE6 Strict
	  max_y  = document.documentElement.scrollTop + wHeight - border;
	} else if (document.body) { // Other IE, such as IE7
	  max_y  = document.body.scrollTop + wHeight - border;
	}
	if((e.pageX + 35 + jt_width) >= max_x){
	  ox = e.pageX - jt_width - 20;
	  pad= "0 17px 0 0";
	  bpl= 'right';
	}
	if((e.pageY + 25 + jt_height) > max_y){
	  //oy = e.pageY - jt_height;
	  oy = max_y - wHeight + border;
	  bpt= "bottom";
      if(jt_height < wHeight){
		oy += wHeight - jt_height;
		if(oy > e.pageY){
		  oy = e.pageY - 5;
		}
		bpt = (e.pageY - oy + 10)+"px";
	  }
	}
	jQuery('#JT').css({left: ox+"px", top: oy+"px", backgroundPosition:bpl+' '+bpt, padding:pad});
  });
};

shop.JT_show = function(content,width){
	width = (width <= 0) ? '200px' : width+'px';
	jQuery("body").append("<div id='JT'><div id='JT_copy' style='width:"+width+"'><div id='JT_title'></div></div></div>");
	jQuery('#JT_copy').html(content);
	jQuery('#JT').show();
};