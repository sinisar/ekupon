var shop = {
  _store : {
	ajax: {},
	data: {},
	method: {},
	variable: {}
  },
}

// Header
shop.header = {
	conf:{city:'', timer: 0,itemWish:'',numItemWish:0,numOrderI:0},
	init:function(){
		shop.header.ads.init();
		shop.header.email.start();
		shop.header.sale.open();
		
		var w = jQuery('.headerCity').width(),
		l = parseInt(jQuery('.headerCity').css("left")),
		pad = 50;
		jQuery('.subCity').css('width', w-pad);
		jQuery('.regMailButton').css("padding", "0 15px 0 "+(w+l+15)+"px");
		
	},
	actionDisplay:{
		showing: 0,
		type: 0,
		wishList: 0,
		order: 0,
		viewMenuUnOrderLike:function(obj, type, event){
			shop.header.actionDisplay.clearMouseClick(event);

			jQuery('.ctrlMenu').addClass('hidden');
			if((shop.header.actionDisplay.showing == 0) || (type != shop.header.actionDisplay.type)){
				shop.header.actionDisplay.showing = 1;
				switch(type){
					case 1:
						shop.header.wishList.viewwishList(function(){
							shop.header.actionDisplay.showMenu(obj);
						});
					break;
					case 2:
						shop.header.quickViewOrder.displayListOrdernew(function(){
							shop.header.actionDisplay.showMenu(obj);
						});
					break;
					default:
						shop.header.actionDisplay.showMenu(obj);
				}
			}else{
				shop.header.actionDisplay.showing = 0;
			}
			shop.header.actionDisplay.type = type;
		},
		showMenu:function(obj){
			jQuery('.ctrlMenu', obj).removeClass('hidden');
		},
		clearMouseClick:function(event){
			if (event.stopPropagation) {
				event.stopPropagation();   // W3C model
			} else {
				event.cancelBubble = true; // IE model
			}
		}
	},
	
	menuCatClick: function(){
		jQuery('.headerFloat .content').slideToggle();
		if(jQuery('.topCat').hasClass('topCatHover')){
			jQuery('.topCat').removeClass('topCatHover')
		}else{
			jQuery('.topCat').addClass('topCatHover')
		}
	},
	/*menuCat: function(){
		jQuery('.topCat').hover(function() {
		jQuery(this).addClass('topCatHover');
		jQuery(this).removeClass('oneBoder');
		}, function() {
			jQuery(this).removeClass('topCatHover');
			jQuery(this).addClass('oneBoder');
		});
	},*/
	menuCat:function(obj, over){
			if(over){
				jQuery(obj).addClass('topCatHover');
				jQuery(obj).removeClass('oneBoder');
			}else{
				jQuery(obj).addClass('oneBoder');
				jQuery(obj).removeClass('topCatHover');
			}
		},
	
	city:{
		conf:{
			city:0,
			safe:'',
			//city_cookie:'cityMC'
		},
		menuCity:function(obj, over){
			if(over){
				jQuery(obj).addClass('headerCityOver');
				jQuery('.content',obj).removeClass('hidden');
			}else{
				jQuery('.content',obj).addClass('hidden');
				jQuery(obj).removeClass('headerCityOver');
			}
		},
		
	},
	email:{
		start:function(){
			jQuery('body').prepend(shop.header.theme.htmlRegMail());
			jQuery('#RegEmail').jNice();
			shop.fix_png('.fixPNG');
			if(shop.cookie.get('showRegEmail') != ''){
				jQuery('#formRegMail').hide();
			};
			shop.enter('#sub-email', function(){shop.header.email.registerCity()});
		},
		hideFormEmail:function() {
			var form = '#formRegMail';
			jQuery(form).slideToggle('slow',function () {
				if(jQuery(form).is(':hidden') == true){
					//shop.cookie.set('showRegEmail', 'not', 86400*365, '/');
				}
			});
		},
	},
};