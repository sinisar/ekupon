How to translate WPG theme, plugin?
-----------------------------------
Theme's language files:
en_EN.mo
en_EN.po

Plugin's language files:
wp-groupbuy-en_EN.mo
wp-groupbuy-en_EN.po
-----------------------------------
This is a rough guide on how to translate WordPress Theme, Plugin using poEdit. 
We will translate it to French as the example.

Step 1
Douplicate en_EN.po, wp-groupbuy-en_EN.po and rename to fr_FR.po, wp-groupbuy-fr_FR.po

Step 2
Download poEdit From http://www.poedit.net/download.php

Step 3
Install poEdit

Step 4
Launch poEdit and open your .po file.

Step 5
Type in the translated string in the small box located at the bottom left hand corner of the screen.
Translate the remaining strings.

Step 6
When you are done, click on the 'Save Catalog' button located on the top, it is the second button from the left.
This will create .mo automatically file when you save the *.po file.

Step 9
Upload both *.po, *.mo to your site into YOUR_WORDPRESS_FOLDER/wp-content/themes/WPGroupbuy/languages/

Step 10
Set WPLANG inside of wp-config.php to your chosen language. For example, if you wanted to use French, you would have:

define ('WPLANG', 'fr_FR');

You are done!