jQuery.noConflict();
jQuery(document).ready(function($) {
	if ($("#advanced-sortables").length > 0 && $('#wg_merchant_details').length > 0) {
		var merchant_tabs = '';
		var merchant_contents = '';
		var tabs = new Array('wg_merchant_details', 'wg_merchant_authorized_users', 'wg_merchant_map_details');
		var active = 'active';
		var block = 'block';
		$.each(tabs, function(index, div_id) {
			if ($('#' + div_id).length > 0) {
				var label = $('#' + div_id + ' .hndle span').html();
				var image_class = '';
				switch (div_id) {
					case 'wg_deal_price':
						image_class = 'general_options';
						break;
					case 'wg_deal_limits':
						image_class = 'inventory_options';
						break
				}
				merchant_tabs += '<li class="' + image_class + ' hide_if_grouped ' + active + '"><a href="#' + div_id + '">' + label + '</a></li>';
				active = '';
				var content = label = $('#' + div_id + ' div.inside').html();
				merchant_contents += '<div class="wc-metaboxes-wrapper panel" id="' + div_id + '" style="display: ' + block + ';">' + content + '</div>';
				block = 'none'
			}
		});
		var innerHtml = '<div class="postbox " id="groupbuy-deal-options">' + '<div class="handlediv"><br></div><h3 class="hndle"><span> ' + object_name_mer.MerchantOptions + '</span></h3>' + '<div class="inside">' + '<div class="panel-wrap product_data">' + '<div class="wc-tabs-back"></div>' + '<ul style="" class="product_data_tabs wc-tabs">' + merchant_tabs + '</ul>' + merchant_contents + '<div class="clear"></div>' + '</div>' + '</div>' + '</div>';
		$("#advanced-sortables").html(innerHtml);
		$('ul.wc-tabs').show();
		$('div.panel-wrap').each(function() {
			$(this).find('div.panel:not(:first)').hide()
		});
		$('ul.wc-tabs a').click(function() {
			var panel_wrap = $(this).closest('div.panel-wrap');
			$('ul.wc-tabs li', panel_wrap).removeClass('active');
			$(this).parent().addClass('active');
			$('div.panel', panel_wrap).hide();
			$($(this).attr('href')).show();
			return false
		})
	}
});