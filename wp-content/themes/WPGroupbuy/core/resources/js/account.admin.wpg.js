jQuery.noConflict();

jQuery(document).ready(function($){

        if($("#advanced-sortables").length > 0 && $('#wg_account_contact_info').length > 0) {
            /**HERE**/
            /**Create left tabs**/
            
            var account_tabs = '';
            var account_contents = '';
            var tabs = new Array('wg_account_contact_info', 'wg_account_purchases', 'wg_account_credits');
             var active = 'active';
             var block = 'block';
             $.each(tabs, function(index, div_id) {
                 if($('#'+div_id).length > 0) {
                     var label = $('#'+div_id + ' .hndle span').html(); // WP 4.3-
                     var image_class = '';
                     account_tabs += '<li class="'+image_class+' hide_if_grouped '+active+'"><a href="#'+div_id+'">'+label+'</a></li>';
                     active = '';

                     var content = label = $('#'+div_id + ' div.inside').html();

                     var update = '';
                     if (div_id !== 'wg_account_credits') {
                         var update = $('#submitpost').html();
                     }
					 if (div_id == 'wg_account_purchases') {
					 	var tmp_ct = content.indexOf('</div>');
					 	var tmp_content_1 = content.substring(0,tmp_ct+6);
					 	var tmp_content_2 = content.substring(tmp_ct+6,content.length);

                         account_contents +=  '<div class="wc-metaboxes-wrapper panel" id="'+div_id+'" style="display: ' +block+';">'+ tmp_content_1 + update  + tmp_content_2 + '</div>';
                     } else {
                     	account_contents +=  '<div class="wc-metaboxes-wrapper panel" id="'+div_id+'" style="display: ' +block+';">'+content  + update + '</div>';
                     }

                     block = 'none';
                 }
            // do your stuff here
          });
        var innerHtml = '<div class="postbox " id="groupbuy-deal-options">'+
                '<div class="handlediv"><br></div><h3 class="hndle"><span> '+ object_name_acc.manageacc +' </span></h3>'+
                '<div class="inside">'+
                '<div class="panel-wrap product_data">'+
                    '<div class="wc-tabs-back"></div>'+
                    '<ul style="" class="product_data_tabs wc-tabs">'+ account_tabs + '</ul>'+ account_contents +
                    '<div class="clear"></div>'+

                '</div>'+
            '</div>'+
        '</div>';
        $("#advanced-sortables").html(innerHtml);
        $('ul.wc-tabs').show();
        $('div.panel-wrap').each(function(){
                $(this).find('div.panel:not(:first)').hide();
        });
        $('ul.wc-tabs a').click(function(){
                var panel_wrap =  $(this).closest('div.panel-wrap');
                $('ul.wc-tabs li', panel_wrap).removeClass('active');
                $(this).parent().addClass('active');
                $('div.panel', panel_wrap).hide();
                $( $(this).attr('href') ).show();
                return false;
        });
            
    }
    //$('#wg_account_credits_box').children('h3.hndle').html(object_name_acc.creditbox);
    $('#wg_account_credits_box').children('div.inside').prepend($('#wg_account_credit_part1').html());
	$('#wg_account_credits_box').children('div.inside').append($('#submitpost').html());
    $('#wg_account_credit_part1').html('');
    
    $( "#wg_account_credit_part1_table" ).find('input.show_hidden').each(function( i ) {
        if(this.checked) {
            var  tr = $(this).parent().parent();
            tr.next().removeAttr('style');
            tr.next().next().removeAttr('style');
        }
    });
    
    $( "#wg_account_credit_part1_table" ).find('input.show_hidden').click(function() {
        var  tr = $(this).parent().parent();
        tr.next().removeAttr('style');
        tr.next().next().removeAttr('style');
    });
	
});

