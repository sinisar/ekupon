jQuery.noConflict();

wpg_admin_deal = {};

jQuery(document).ready(function($){

        if($("#advanced-sortables").length > 0) {
            /**HERE**/
            /**Create left tabs**/
            
            var deal_tabs = '';
            var deal_contents = '';
            var tabs = new Array('wg_deal_price', 'wg_deal_limits', 'wg_deal_details', 
                    'wg_deal_voucher','wg_deal_merchant', 'wg_adaptive_payments', 'wg_deal_attributes', 'wg_deal_theme_meta','wg_deal_custom_options');
             var active = 'active';
             var block = 'block';
             $.each(tabs, function(index, div_id) {
                 if($('#'+div_id).length > 0) {
                     var label = $('#'+div_id + ' .hndle span').html();

                     if (label === object_name.Items) {
                         label = object_name.ItemAttributes
                     }
                     var image_class = '';
                        switch(div_id){
                            case 'wg_deal_price':
                                image_class = 'general_options';
                                break;
                            case 'wg_deal_limits':
                                image_class = 'inventory_options';
                                break;
                            case 'wg_deal_details':
                                image_class = 'shipping_options';
                                break;
                            case 'wg_deal_voucher':
                                image_class = 'linked_product_options';
                                break;
                            case 'wg_deal_merchant':
                                image_class = 'attribute_options';
                                break;
                            case 'wg_adaptive_payments':
                                image_class = 'advanced_options';
                                break;
                            case 'wg_deal_attributes':
                                image_class = 'variation_options';
                                break;
							case 'wg_deal_theme_meta':
                                image_class = 'featured_options';
                                break;
                            case 'wg_deal_custom_options':
                                image_class = 'featured_options'
                                break;
                       }
                     deal_tabs += '<li class="'+image_class+' hide_if_grouped '+active+'"><a href="#'+div_id+'">'+label+'</a></li>';
                     active = '';

                     var content = label = $('#'+div_id + ' div.inside').html();
                     deal_contents +=  '<div class="wc-metaboxes-wrapper panel" id="'+div_id+'" style="display: ' +block+';">'+content + '</div>';
                     block = 'none';
                 }
            // do your stuff here
          });
        var innerHtml = '<div class="postbox " id="groupbuy-deal-options">'+
                '<div class="handlediv"><br></div><h3 class="hndle"><span>'+ object_name.Dealoptions +'</span></h3>'+
                '<div class="inside">'+
                '<div class="panel-wrap product_data">'+
                    '<div class="wc-tabs-back"></div>'+
                    '<ul style="" class="product_data_tabs wc-tabs">'+ deal_tabs + '</ul>'+ deal_contents +
                    '<div class="clear"></div>'+

                '</div>'+
            '</div>'+
        '</div>';
            $("#advanced-sortables").html(innerHtml);
    }
    $('a.wg-deal-attribute-remove').live( 'click', function() {
            $(this).parents('.wg-deal-attributes-row').remove();
            return false;
    });
    $('a#wg_add_attribute').click( function() {
            var $row = $('table#wg-deal-attributes-template .wg-deal-attributes-row').clone();
            $('div#wg-deal-attributes table tbody:first').append($row);
            return false;
    });
    
    
    
    $('a.wg_delete_deal_shipping_rate').live( 'click', function() {
            $(this).parents('.wg_deal_dyn_shipping').remove();
            return false;
    });
    $('a#wg_add_deal_shipping_rate').click( function() {
            var dyn_shipping_size = jQuery('tbody .wg_deal_dyn_shipping').size();
            var $dyn_shipping_row = $('<tr class="wg_deal_dyn_shipping">\
            <td class="quantity"><input class="wg-shipping-quantity" type="text" size="2" name="deal_dynamic_shipping[quantity]['+dyn_shipping_size+']" placeholder="0" >'+dynamic_rate_up+'</td>\
            <td class="rate">'+wg_get_currency_symbol+'<input class="wg-shipping-rate" type="text" size="2" name="deal_dynamic_shipping[rate]['+dyn_shipping_size+']" placeholder="0" ></td>\
            <td class="ship"><input type="checkbox" name="deal_dynamic_shipping[per_item]['+dyn_shipping_size+']" value="TRUE" />&nbsp;'+dynamic_rate_per_item+'</td>\
            <td class="remove" valign="middle"><a type="button" class="button wg_delete_deal_shipping_rate" href="#" title="'+dynamic_rate_remove_option+'"> '+dynamic_rate_remove+'</a></td></tr>');
            $('table#dyn_shipping_table tbody:first').append($dyn_shipping_row);
            return false;
    });
    if(typeof shippable === undefined) {
        $('.shipping_option_' + shippable).show();
    }
    $("input[name$='deal_base_shippable']").click(function() {
            var $selected = $(this).val();
            $('.cloak').hide();
            $('.shipping_option_'+$selected).show();
});
    
    $('ul.wc-tabs').show();
    $('div.panel-wrap').each(function(){
            $(this).find('div.panel:not(:first)').hide();
    });
    $('ul.wc-tabs a').click(function(){
            var panel_wrap =  $(this).closest('div.panel-wrap');
            $('ul.wc-tabs li', panel_wrap).removeClass('active');
            $(this).parent().addClass('active');
            $('div.panel', panel_wrap).hide();
            $( $(this).attr('href') ).show();
            return false;
    });
    wpg_admin_deal.ready($);
    //$(".mceIframeContainer").width(400);
    $('#upload_logo_button').click(function() {
		tb_show(object_name.uploadimage, 'media-upload.php?referer=wpg-upload-settings&amp;type=image&amp;TB_iframe=true&amp;post_id=0', false);
		return false;
	});
	$('#upload_payment_button').click(function() {
        tb_show(object_name.uploadimage, 'media-upload.php?referer=wpg-upload-settings&amp;type=image&amp;TB_iframe=true&amp;post_id=0', false);
        return false;
    });
    // Tooltips
	jQuery(".tips, .help_tip").tipTip({
    	'attribute' : 'title',
    	'fadeIn' : 50,
    	'fadeOut' : 50,
    	'delay' : 200
    });
        
});

wpg_admin_deal.ready = function($) {

	// Dyn Cost

	// Initialize a counter var
	inputCount = 0;
	// Now let's say you click the "add text box" button
	$('#add_dyn_cost').click(function(){
		var value_total = $('#dynamic_purchase_total').val();
		var value_cost = $('#dynamic_purchase_cost').val();
		var table = '<tr id="dyn_cost_'+inputCount+'"><td class="centered_text">'+value_total+'</td><td>'+wg_currency_symbol+'<input id="dyn_cost_' + inputCount + '" type="text" name="deal_dynamic_price['+value_total+']" value="'+value_cost+'" class="tiny-text" size="5"></td><td><a id="delete_dyn_cost_'+inputCount+'" class="delete-dyn-cost button hide-if-no-js" onclick="jQuery(this).parent().parent().remove();">'+ object_name.remove +'</a></td></tr>';
		if ( value_total.length != 0 & value_cost.length != 0 ) {
			$(' '+table+' ').appendTo('#dynamic_costs');
		};
		inputCount++;
	});
	$('.delete-dyn-cost').live('click', function(){
		$(this).parent().parent().remove();
	});

	// Datepicker

	$('#deal_expiration').datetimepicker({minDate: 0});
	$('#wg_deal_exp').datetimepicker({minDate: 0});
	$('#wg_deal_start_date').datetimepicker({minDate: 0});
	$('#voucher_expiration_date').datepicker({minDate: 0});
	$('#wg_deal_voucher_expiration').datepicker({minDate: 0});
};