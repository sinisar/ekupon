/* German translation for the jQuery Timepicker Addon */
/* Written by Sinisa */
(function($) {
    $.timepicker.regional['sl'] = {
        currentText: 'Trenutni',
        closeText: 'Dokončaj',
        amNames: ['AM', 'A'],
        pmNames: ['PM', 'P'],
        timeFormat: 'HH:mm',
        timeSuffix: '',
        timeOnlyTitle: 'Izberi čas',
        timeText: 'Čas',
        hourText: 'Ure',
        minuteText: 'Minute',
        secondText: 'Sekunde',
        millisecText: 'Milisekunde',
        microsecText: 'Mikrosekune',
        timezoneText: 'Časovni pas',
        isRTL: false
    };
    $.timepicker.setDefaults($.timepicker.regional['sl']);
})(jQuery);