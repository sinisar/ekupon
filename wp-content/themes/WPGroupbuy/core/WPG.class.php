<?php
/* ***********************************
*           WPG CLASSES
* ***********************************/

if ( !defined( 'WPG_DEV' ) )
	define( 'WPG_DEV', TRUE );

abstract class WP_Groupbuy {

	// text-domain
	const TEXT_DOMAIN = 'wp-groupbuy';
	
	// Current version
	const WG_VERSION = '2.3';
	
	// DB Version
	const DB_VERSION = 1;
	
	// WPG Name
	const PLUGIN_NAME = 'WPGroupbuy';
	
	// WPG debugging mode: define( 'WPG_DEV', TRUE/FALSE )
	const DEBUG = WPG_DEV;

	// __()
	public static function __( $string ) {
		return __( apply_filters( 'wg_string_'.sanitize_title( $string ), $string ), self::TEXT_DOMAIN );
	}

	// _e()
	public static function _e( $string ) {
		return _e( apply_filters( 'wg_string_'.sanitize_title( $string ), $string ), self::TEXT_DOMAIN );
	}
}