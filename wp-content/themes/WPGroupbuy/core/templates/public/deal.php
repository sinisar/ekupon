<?php get_header(); ?>

<div id="primary" class="site-content">
	<div id="content" role="main">
		<?php if ( have_posts() ) : ?>

			<?php while ( have_posts() ) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

						<header class="entry-header">
							<?php the_post_thumbnail(); ?>
							<h1 class="entry-title">
								<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'wpgroupbuy' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
							</h1>

							<?php wg_add_to_cart_form(); ?>

							<?php if ( comments_open() ) : ?>
								<div class="comments-link">
									<?php comments_popup_link( '<span class="leave-reply">' . __( 'Leave a reply', 'wpgroupbuy' ) . '</span>', __( '1 Reply', 'wpgroupbuy' ), __( '% Replies', 'wpgroupbuy' ) ); ?>
								</div>
							<?php endif; // comments_open() ?>
						</header>

						<div class="entry-content">
							<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'wpgroupbuy' ) ); ?>
							<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'wpgroupbuy' ), 'after' => '</div>' ) ); ?>
						</div>

						<footer class="entry-meta">
							<?php edit_post_link( __( 'Edit', 'wpgroupbuy' ), '<span class="edit-link">', '</span>' ); ?>
							<?php if ( is_singular() && get_the_author_meta( 'description' ) && is_multi_author() ) : ?>
								<div class="author-info">
									<div class="author-avatar">
										<?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'wpgroupbuy_author_bio_avatar_size', 68 ) ); ?>
									</div>
									<div class="author-description">
										<h2><?php printf( __( 'About %s', 'wpgroupbuy' ), get_the_author() ); ?></h2>
										<p><?php the_author_meta( 'description' ); ?></p>
										<div class="author-link">
											<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">
												<?php printf( __( 'View all posts by %s <span class="meta-nav">&rarr;</span>', 'wpgroupbuy' ), get_the_author() ); ?>
											</a>
										</div>
									</div>
								</div>
							<?php endif; ?>
						</footer>
					</article>

			<?php endwhile; ?>

			<?php if ( function_exists('wpgroupbuy_content_nav') ) wpgroupbuy_content_nav( 'nav-below' ); ?>

		<?php else : ?>

			<article id="post-0" class="post no-results not-found">

			<?php if ( current_user_can( 'edit_posts' ) ) :
				// Show a different message to a logged-in user who can add posts.
			?>
				<header class="entry-header">
					<h1 class="entry-title"><?php _e( 'No posts to display', 'wpgroupbuy' ); ?></h1>
				</header>

				<div class="entry-content">
					<p><?php printf( __( 'Ready to publish your first post? <a href="%s">Get started here</a>.', 'wpgroupbuy' ), admin_url( 'post-new.php' ) ); ?></p>
				</div>

			<?php else :
				// Show the default message to everyone else.
			?>
				<header class="entry-header">
					<h1 class="entry-title"><?php _e( 'Nothing Found', 'wpgroupbuy' ); ?></h1>
				</header>

				<div class="entry-content">
					<p><?php _e( 'Apologies, but no results were found. Perhaps searching will help find a related post.', 'wpgroupbuy' ); ?></p>
					<?php get_search_form(); ?>
				</div>
			<?php endif; // end current_user_can() check ?>

			</article>

		<?php endif; // end have_posts() check ?>

	</div>
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
