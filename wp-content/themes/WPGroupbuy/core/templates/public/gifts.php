<?php get_header(); ?>

		<div id="primary" class="site-content">
			<div id="content" role="main">

			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<h1 class="entry-title"><?php the_title(); ?></h1>

					<div class="entry-content">
						<?php the_content(); ?>
					</div>

				</div>


			<?php endwhile; // end of the loop. ?>

			</div>
		</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
