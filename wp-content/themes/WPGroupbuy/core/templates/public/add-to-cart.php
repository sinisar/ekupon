<form class="add-to-cart" method="post" action="<?php wg_add_to_cart_action(); ?>">
	<?php foreach ( $fields as $field ): ?>
		<?php echo $field; ?>
	<?php endforeach; ?>
	<input type="submit" value="<?php wpg_e( $button_text ); ?>" />
</form>
