<?php do_action( 'wg_voucher_pre_header' ) ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php the_title(); ?></title>
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/style/bootstrap.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/style/voucher.css">
    <?php do_action( 'wg_voucher_head' ) ?>
</head>
<!-- <body onload="printpage()"> -->
<body>
<div id="wrapper-site">
    <div id="voucher-top">
        <div class="container">
            <div id="title-section" class="row">
                <div id="title" class="col-md-6">
                    <?php 
                    if ( wg_has_voucher_logo() ) {
                        wg_voucher_logo_image();
                    } elseif ( wg_has_univ_voucher_logo() ) {
                        wg_univ_voucher_logo();
                    } else { ?>
                        <img src="<?php wg_header_logo() ?>" height="100px"/>
                    <?php } ?>
                </div>
                <div id="voucher-id" class="col-md-6">
                    <div class="col-md-8 text-left" style="padding-left: 0px;">
                        <h1><?php wpg_e( 'Gift Voucher' ); ?></h1>
                        <?php echo str_replace( wpg__( 'Voucher for' ), '', get_the_title() ); ?>
                    </div>
                    <div id="qr_code" class="col-md-4">
                        <img src="https://chart.googleapis.com/chart?cht=qr&amp;chs=120x120&amp;chl=<?php echo urlencode( wg_get_voucher_claim_url( wg_get_voucher_security_code(), FALSE ) ) ?>">
                    </div>
                </div>
            </div>
            <div id="deal" class="row">
                <div id="colomn-left" class="col-md-6">
                    <p class="title"><?php wpg_e( 'Recipient:' ); ?> <span><?php esc_attr_e( wg_get_name() ); ?></span></p>
                    <p class="title"><?php wpg_e( 'Reference:' ); ?> <span><?php wg_voucher_security_code(); ?></span></p>
                    <p class="title"><?php wpg_e( 'Fine Print:' ); ?> </p>
                    <div class="pTop16"><?php wg_voucher_fine_print() ?></div>
                </div>
                <div id="colomn-right" class="col-md-6">
                    <p class="title"><?php wpg_e( 'Order number: ' ); ?><span><?php echo wg_get_vouchers_purchase(); ?></span></p>
                    <p class="title"><?php wpg_e( 'Voucher Code: ' ); ?><span><?php wg_voucher_code(); ?></span></p>
                    <p class="title"><?php wpg_e( 'Expires On: ' ); ?><span><?php wg_voucher_expiration_date(); ?></span> </p>
                    <p class="title"><?php wpg_e( 'Redeem at: ' ); ?></p>
                    <div>
                        <?php wg_voucher_locations() ?>
                    </div>
                </div>
            </div>
            <div id="univ-fine-print" class="row">
                <div class="col-md-12">
                    <p class="title"><?php wpg_e( 'Universal Fine Print' ); ?></p>
                    <?php  wg_univ_voucher_fine_print(); ?>
                </div>
            </div>
        </div>
    </div>
    <div id="how_to">
        <div class="container">
            <div class="row">
                <div id="how-column-left"  class="col-md-6">
                    <p  class="title"><?php wpg_e( 'How to use this?' ); ?></p>
                    <div>
                        <?php wg_voucher_usage_instructions() ?>
                    </div>
                </div>
                <div id="how-column-right"  class="col-md-6">
                    <p  class="title"><?php wpg_e( 'Map:' ); ?></p>
                    <div>
                        <?php wg_voucher_map() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="support">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <span><?php wpg_e( 'Contact us for support:' ); ?><?php wg_voucher_support1() ?> <?php wg_voucher_support2() ?></span>
                    <span><?php wg_voucher_legal(); ?></span>
                </div>
            </div>
        </div>
    </div>
    <div id="legal-stuff"></div>
</div>
<?php do_action( 'wg_voucher_footer' ) ?>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>
</body>
<?php do_action( 'wg_voucher_post_body' ) ?>
</html>