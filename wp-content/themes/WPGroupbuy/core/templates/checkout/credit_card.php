<div class="mt38"></div>
	<fieldset id="wg-credit-card">
    <legend><?php wpg_e('Payment Information'); ?></legend>
		<table>
			<tbody>
            <!-- Payment method -->
				<?php foreach ( $fields as $key => $data ): ?>
					<?php if ( $data['weight'] < 1 && !in_array($key, array('cc_name', 'cc_number', 'cc_expiration_month', 'cc_expiration_year', 'cc_cvv')) ): ?>
					<tr>
						<?php if ( $data['type'] != 'checkbox' ): ?>
							<td class="edit-td"><?php wg_form_label($key, $data, 'credit'); ?></td>
							<td><?php wg_form_field($key, $data, 'credit'); ?></td>
						<?php else: ?>
							<td colspan="2">
								<label for="wg_credit_<?php echo $key; ?>"><?php wg_form_field($key, $data, 'credit'); ?> <?php echo $data['label']; ?></label>
							</td>
						<?php endif; ?>
					</tr>
					<?php endif; ?>
				<?php endforeach; ?>
                <!-- End Payment method -->
                
                <!-- Credit Card -->
				<?php if ( $fields['cc_name'] ): ?>
					<tr class="wg_credit_card_field_wrap">
						<td><?php wg_form_label('cc_name', $fields['cc_name'], 'credit'); ?></td>
						<td><?php wg_form_field('cc_name', $fields['cc_name'], 'credit'); ?></td>
					</tr>
				<?php endif; ?>
				<?php if ( $fields['cc_number'] ): ?>
					<tr class="wg_credit_card_field_wrap">
						<td><?php wg_form_label('cc_number', $fields['cc_number'], 'credit'); ?></td>
						<td><?php wg_form_field('cc_number', $fields['cc_number'], 'credit'); ?> <span id="card_image"></span></td>
					</tr>
				<?php endif; ?>
				<?php if ( $fields['cc_expiration_month'] && $fields['cc_expiration_year'] ): ?>
					<tr class="wg_credit_card_field_wrap">
						<td><?php wg_form_label('cc_expiration_year', $fields['cc_expiration_year'], 'credit'); ?></td>
						<td><?php wg_form_field('cc_expiration_month', $fields['cc_expiration_month'], 'credit'); ?> <?php wg_form_field('cc_expiration_year', $fields['cc_expiration_year'], 'credit'); ?></td>
					</tr>
				<?php endif; ?>
				<?php if ( $fields['cc_cvv'] ): ?>
					<tr class="wg_credit_card_field_wrap">
						<td><?php wg_form_label('cc_cvv', $fields['cc_cvv'], 'credit'); ?></td>
						<td><?php wg_form_field('cc_cvv', $fields['cc_cvv'], 'credit'); ?></td>
					</tr>
				<?php endif; ?>
                <!-- Credit Card -->
                
                <!-- Credit & Balance -->
				<?php foreach ( $fields as $key => $data ): ?>
					<?php if ( $data['weight'] > 1 && !in_array($key, array('cc_name', 'cc_number', 'cc_expiration_month', 'cc_expiration_year', 'cc_cvv')) ): ?>
					<tr>
						<?php if ( $data['type'] != 'checkbox' ): ?>
							<td><?php wg_form_label($key, $data, 'credit'); ?></td>
							<td><?php wg_form_field($key, $data, 'credit'); ?></td>
						<?php else: ?>
							<td colspan="2">
								<label for="wg_credit_<?php echo $key; ?>"><?php wg_form_field($key, $data, 'credit'); ?> <?php echo $data['label']; ?></label>
							</td>
						<?php endif; ?>
					</tr>
					<?php endif; ?>
				<?php endforeach; ?>
                <!-- End Credit & Balance -->
			</tbody>
		</table>
	</fieldset>
