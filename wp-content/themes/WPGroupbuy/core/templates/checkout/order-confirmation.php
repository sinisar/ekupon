<p><?php wpg_e( 'Your order is complete.' ); ?></p>
<table>
	<tbody>
		<tr>
			<th scope="row"><?php wpg_e( 'Order Number:' ); ?></th>
			<td><?php echo $order_number; ?></td>
		</tr>
		<tr>
			<th scope="row"><?php wpg_e( 'Total:' ); ?></th>
			<td><?php wg_formatted_money( $total ); ?></td>
		</tr>
	</tbody>
</table>
