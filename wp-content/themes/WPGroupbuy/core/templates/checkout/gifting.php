<div class="checkout_block right_form clearfix">

	<div class="paymentform-info">
		<h2 class="section_heading wg_ff"><?php wpg_e( 'Gift Options' ); ?></h2>
	</div>
	<fieldset id="wg-billing">
		<table class="billing">
			<tbody>
				<?php foreach ( $fields as $key => $data ): ?>
					<tr>
						<?php if ( $data['type'] != 'checkbox' ): ?>
							<td><?php wg_form_label( $key, $data, 'gifting' ); ?></td>
							<td><?php wg_form_field( $key, $data, 'gifting' ); ?></td>
						<?php else: ?>
							<td colspan="2">
								<label for="wg_gifting_<?php echo $key; ?>"><?php wg_form_field( $key, $data, 'gifting' ); ?> <?php echo $data['label']; ?></label>
							</td>
						<?php endif; ?>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</fieldset>

</div>
