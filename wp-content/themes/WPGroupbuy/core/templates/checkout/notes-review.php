<div class="mt38"></div>
<fieldset id="wg-notes">
	<legend><?php wpg_e('Your Notes'); ?></legend>
	<table>
		<tbody>
			<tr>
				<th scope="row"><?php echo $data['label']; ?></th>
				<td><?php echo $notes ?></td>
			</tr>
		</tbody>
	</table>
</fieldset>
