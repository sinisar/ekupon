<fieldset id="wg-account-user-info">
	<legend><?php wpg_e( 'Login' ); ?></legend>
	<table class="account">
		<tbody>
			<tr>
				<td><label for="log"><?php wpg_e( 'Your Username' ) ?>:</label></td>
				<td><span class="wg-form-field wg-form-field-text wg-form-field-required"><input tabindex="11" type="text" name="log" id="log" class="text-input" />
			</span></td>
			</tr>

			<tr>
				<td><label for="pwd"><?php wpg_e( 'Your Password' ) ?>:</label></td>
				<td><span class="wg-form-field wg-form-field-text wg-form-field-required"><input tabindex="12" type="password" name="pwd" id="pwd" class="text-input" />
			</span></td>
			</tr>
			<tr>
				<td>
					<?php wp_nonce_field( 'wg_login_action', 'wg_login' ); ?>
					<?php do_action( 'wpg_login_form_fields' ) ?>
				</td>
				<td>
					<label for="rememberme" class="checkbox-label"><input name="rememberme" id="rememberme" type="checkbox" checked="checked" value="forever" /> <?php wpg_e( 'Keep Me Signed In' ); ?></label>
				</td>

			</tr>
		</tbody>
	</table>
</fieldset>

<p><a href="<?php echo wp_lostpassword_url(); ?>" title="<?php wpg_e( 'Lost password&#63;' ); ?>"><?php wpg_e( 'Forgot your password&#63;' ); ?></a></p>
