<script>
jQuery(document).ready(function($) {
    var creditCard = jQuery('#wg_credit_cc_number');

    creditCard.cardcheck({
        acceptedCards: [
             <?php if ( !empty( $accepted_cards ) ) {
                echo "'" . implode( "', '", $accepted_cards ) . "'";
            } else {
                ?>
                    'visa',
                    'mastercard',
                    'amex',
                <?php
            } ?>
        ],
        iconLocation: '#card_image',
        iconDir: '<?php echo WG_URL ?>/resources/images/credit-cards/',
        onReset: function() {
            creditCard.removeClass('success', 'error');
        },
        onError: function( type ) {
            creditCard.removeClass('success').addClass('error');
        },
        onValidation: function( type, niceName ) {
            creditCard.removeClass('error').addClass('success');
        }

    });

});
</script>
<span id="accepted_credit_cards"><?php wpg_e('Credit Card') ?></span>