<div class="checkout_block left_form clearfix">

	<div class="paymentform-info">
		<h2 class="wg_ff"><?php wpg_e( 'Shipping Information' ); ?></h2>
	</div>
	<fieldset id="wg-shipping">
		<table class="shipping">
			<tbody>
				<?php foreach ( $fields as $key => $data ): ?>
					<tr>
						<?php if ( $data['type'] != 'checkbox' ): ?>
							<td><?php wg_form_label( $key, $data, 'shipping' ); ?></td>
							<td><?php wg_form_field( $key, $data, 'shipping' ); ?></td>
						<?php else: ?>
							<td colspan="2">
								<label for="wg_shipping_<?php echo $key; ?>"><?php wg_form_field( $key, $data, 'shipping' ); ?> <?php echo $data['label']; ?></label>
							</td>
						<?php endif; ?>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</fieldset>

</div>
