<div class="checkoutnote mt38">
  <fieldset id="wg-billing">
  <legend><?php self::_e('Note'); ?></legend>
    <table class="billing">
      <tbody>
        <tr>
          <td class="edit-td odd first"><label for="wg_notes">
              <?php wpg_e('Write note for us about your order') ?>
              <?php if ($required): ?>
              <span class="required">*</span>
              <?php endif ?>
            </label></td>
          <td><span class="wg-form-field wg-form-field-textarea">
            <textarea name="wg_notes" id="wg_notes" placeholder="<?php wpg_e('') ?>"><?php if (isset($_POST[ 'wg_notes' ])) echo esc_textarea($_POST[ 'wg_notes' ]);  ?>
</textarea>
            </span></td>
        </tr>
      </tbody>
    </table>
  </fieldset>
</div>
