<div class="checkout_block left_form clearfix">

	<div class="paymentform-info">
		<h2 class="section_heading wg_ff"><?php wpg_e( 'Your Billing Information' ); ?></h2>
	</div>
	<fieldset id="wg-billing">
		<table class="billing">
			<tbody>
				<?php foreach ( $fields as $key => $data ): ?>
					<tr>
						<?php if ( $data['type'] != 'checkbox' ): ?>
							<td><?php wg_form_label( $key, $data, 'billing' ); ?></td>
							<td><?php wg_form_field( $key, $data, 'billing' ); ?></td>
						<?php else: ?>
							<td colspan="2">
								<label for="wg_billing_<?php echo $key; ?>"><?php wg_form_field( $key, $data, 'billing' ); ?> <?php echo $data['label']; ?></label>
							</td>
						<?php endif; ?>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</fieldset>

</div>
