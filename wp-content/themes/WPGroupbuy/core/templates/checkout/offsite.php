<div class="checkout_block right_form clearfix">
	<div class="paymentform-info">
		<h2 class="section_heading wg_ff"><?php wpg_e( 'Payment Information' ); ?></h2>
	</div>
	<fieldset id="wg-offsite-payment">
		<table>
			<tbody>
				<tr>
					<th scope="row"><?php wpg_e( 'Payment Method' ) ?></th>
					<td><?php echo $name; ?></td>
				</tr>
			</tbody>
		</table>
	</fieldset>
</div>
