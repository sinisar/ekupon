<p>
	<?php wpg_e( 'If you register a merchant with this site, it can be associated with daily deals.' ); ?>
</p>
<p>
	<a href="<?php echo esc_url( WP_Groupbuy_Merchants_Registration::get_url() ); ?>"><?php wpg_e( 'Register a merchant' ); ?></a>
</p>
