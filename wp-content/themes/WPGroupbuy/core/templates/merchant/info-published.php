<h2><?php wpg_e('Merchant Information'); ?></h2>
<table>
	<tbody>
		<?php foreach ( $fields as $key => $data ): ?>
			<tr>
				<?php if ( $data['type'] != 'checkbox' ): ?>
					<td class="edit-td pBottom10"><?php echo $data['label']; ?></td>
					<?php if ( $data['type'] == 'file' ): ?>
						<td><img src="<?php echo $data['default']; ?>" height="150px"></td>
					<?php else: ?>
						<td><?php echo $data['default']; ?></td>
					<?php endif ?>
				<?php else: ?>
					<td colspan="2">
						<label for="wg_contact_<?php echo $key; ?>"><?php wg_form_field( $key, $data, 'contact' ); ?> <?php echo $data['label']; ?></label>
					</td>
				<?php endif; ?>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<p>
	<a href="<?php echo esc_url( WP_Groupbuy_Merchants_Edit::get_url() ); ?>"><?php wpg_e( 'Edit Merchant Information' ); ?></a> | <a href="<?php wpg_account_link(); ?>"><?php wpg_e( 'Back to Dashboard' ); ?></a>
</p>
