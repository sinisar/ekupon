<form id="wpg_merchant_register" method="post" action="<?php wg_merchant_register_url(); ?>" enctype="multipart/form-data">
	<input type="hidden" name="wg_merchant_action" value="<?php echo WP_Groupbuy_Merchants_Registration::FORM_ACTION; ?>" />
	<table class="form-table">
		<tbody>
			<?php foreach ( $fields as $key => $data ): ?>
				<tr>
					<?php if ( $data['type'] != 'checkbox' ): ?>
						<td><?php wg_form_label( $key, $data, 'contact' ); ?></td>
						<td><?php wg_form_field( $key, $data, 'contact' ); ?></td>
					<?php else: ?>
						<td colspan="2">
							<label for="wg_contact_<?php echo $key; ?>"><?php wg_form_field( $key, $data, 'contact' ); ?> <?php echo $data['label']; ?></label>
						</td>
					<?php endif; ?>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<?php WP_Groupbuy_Controller::load_view( 'merchant/register-controls', array() ); ?>
</form>
