<?php

// Pending deals
if ( wg_account_merchant_id() && function_exists( 'wg_deal_preview_available' ) ) {
	$deals= null;
	$args=array(
		'post_type' => wg_get_deal_post_type(),
		'post__in' => wg_get_merchants_deal_ids( wg_account_merchant_id() ),
		'post_status' => array( 'pending', 'draft', 'future', 'private' ),
		'posts_per_page' => -1, // return this many

	);
	$deals = new WP_Query( $args );
	if ( $deals->have_posts() ) {
?>
			<div class="dashboard-container">
				<table class="report_table merchant_dashboard">

					<thead>
						<tr>
							<th class="purchase_history_status"><?php wpg_e( 'Status' ); ?></th>
							<th class="purchase_history_title"><?php wpg_e( 'Pending Deals' ); ?></th>
							<th><?php wpg_e( 'Previews' ); ?></th>
						</tr>
					</thead>

					<tbody>
					<?php
		while ( $deals->have_posts() ) : $deals->the_post();
		if ( wg_get_status() !== 'closed' ) {
?>
							<tr>
								<td class="purchase_history_status"><?php echo get_post_status( get_the_ID() ) ?></td>
								<td class="purchase_history_title">
									<?php the_title() ?>
								</td>
								<td class="td_deal_preview va-middle">
									<?php if ( wg_deal_preview_available() ): ?>
										<a href="<?php wg_deal_preview_link() ?>" class="report_button" target="_blank"><?php wpg_e( 'Deal Preview' ) ?></a>
										<br/>
									<?php endif ?>
									<?php if ( wg_voucher_preview_available() ): ?>
										<a href="<?php wg_voucher_preview_link() ?>" class="report_button" target="_blank"><?php wpg_e( 'Voucher Preview' ) ?></a>
									<?php endif ?>
								</td>
							</tr>
						<?php
		}
		endwhile;
?>
					</tbody>
				</table>
			</div>
			<?php
	}
}
?>

<div class="dashboard-container">

	<?php
if ( wg_account_merchant_id() ) {
	$deals= null;
	$args=array(
		'post_type' => wg_get_deal_post_type(),
		'post__in' => wg_get_merchants_deal_ids( wg_account_merchant_id() ),
		'post_status' => 'publish',
		'posts_per_page' => -1, // return this many

	);
	$deals = new WP_Query( $args );
	if ( $deals->have_posts() ) {
?>

				<span class="full_history_report clearfix"><a href="<?php wg_merchant_purchases_report_url( $postID ) ?>" class="report_button"><?php wpg_e( 'Purchase History' ) ?></a></span>
				<table class="report_table">

					<thead>
						<tr>
							<th class="purchase_history_status"><?php wpg_e( 'Status' ); ?></th>
							<th class="purchase_history_title"><?php wpg_e( 'Deal' ); ?></th>
							<th><?php wpg_e( 'Total Sold' ); ?></th>
							<th><?php wpg_e( 'Published' ); ?></th>
							<th><?php wpg_e( 'Reports' ); ?></th>
						</tr>
					</thead>

					<tbody>
					<?php
		while ( $deals->have_posts() ) : $deals->the_post();
?>
						<tr>
							<td class="purchase_history_status"><?php echo wg_get_status() ?></td>
							<td class="purchase_history_title">
								<?php the_title() ?>
								<br/>
								<a href="<?php the_permalink() ?>"><?php wpg_e( 'View Deal' ) ?></a>
							</td>
							<td><?php wg_number_of_purchases() ?></td>
							<td><?php the_time() ?> <?php the_time() ?></td>
							<td>
								<?php wg_merchant_purchase_report_link() ?>
								<?php wg_merchant_voucher_report_link() ?>
							</td>
						</tr>
					<?php
		endwhile;
?>
					</tbody>
				</table>
				<?php
	} else {
		echo '<p>'.wpg__( 'No sales info.' ).'</p>';
	}
} else {
	echo '<p>'.wpg__( 'Restricted to Businesses.' ).'</p>';
}
?>

</div>
