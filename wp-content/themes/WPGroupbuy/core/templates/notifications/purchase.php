<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="si" lang="si">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="si" />
    <title>Potrdilo nakupa na Izkoristi.me</title>
    <style type="text/css">
        table {border-collapse:separate;}
        a, a:link {text-decoration: none; color: #00788a;}
        a:hover {text-decoration: underline;}
        h2,h2 a,h2 a,h3,h3 a,h3 a,h4,h5,h6,.t_cht {color:#000!important;}
        .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td {line-height: 100%;}
        .ExternalClass {width: 100%;}

        body {
            padding-top: 0!important;
            padding-bottom: 0!important;
            padding-top: 0!important;
            padding-bottom: 0!important;
            margin:0!important;
            width: 100%!important;
            -webkit-text-size-adjust: 100%!important;
            -ms-text-size-adjust: 100%!important;
            -webkit-font-smoothing: antialiased!important;
        }

        .menu-item{
            margin: 0 5px;
        }

        .bgBody {
            background: #d1d2d4;
        }

        [style*='Roboto'] {
            font-family: 'Roboto', Arial, sans-serif!important;
        }
    </style>
</head>
<body>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center" >
        <!--header-->
        <tr>
            <td valign="top" style="background-color:#1b0021;">
                <table  width="100" height="75" border="0" cellspacing="0" cellpadding="0" align="center" style="background-color:#1b0021;font-family:Georgia, serif;" >
                    <tr>
                        <td>
                            <a href="[site_url]" target="_blank" title="Izkoristi.me">
                                <img style="display:block; border:0;" hspace="10" alt="Logo" src="[resources_url]/logo.png">
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <!--END header-->

        <!--navigation-->
        <tr>
            <td valign="top" style="background-color:#d5499f;">
                <table  width="600"  height="34" border="0" cellspacing="0" cellpadding="0" align="center" style="background-color:#d5499f;font-family: Arial, sans-serif, 'Roboto'; color:#fff;" >
                    <tr>
                        <td style="font-family: Arial, sans-serif, 'Roboto'; color:#fff;font-size:12px; text-align: center;">
                            <a class="menu-item" href="[site_url]/deal-categories/lepota-in-zdravje/" target="_blank" title="Lepota in zdravje" style="font-family: Arial, sans-serif, 'Roboto'; color:#fff;font-size:12px; text-align: center;text-decoration: none;">Lepota in zdravje</a>
                            &nbsp;|&nbsp;
                            <a class="menu-item" href="[site_url]/deal-categories/wellness/" target="_blank" title="Wellness" style="font-family: Arial, sans-serif, 'Roboto'; color:#fff;font-size:12px; text-align: center;text-decoration: none;">Wellness</a>
                            &nbsp;|&nbsp;
                            <a class="menu-item" href="[site_url]/deal-categories/gostinstvo/" target="_blank" title="Gostinstvo" style="font-family: Arial, sans-serif, 'Roboto'; color:#fff;font-size:12px; text-align: center;text-decoration: none;">Gostinstvo</a>
                            &nbsp;|&nbsp;
                            <a class="menu-item" href="[site_url]/deal-categories/avto/" target="_blank" title="Avto" style="font-family: Arial, sans-serif, 'Roboto'; color:#fff;font-size:12px; text-align: center;text-decoration: none;">Avto</a>
                            &nbsp;|&nbsp;
                            <a class="menu-item" href="[site_url]/deal-categories/turizem/" target="_blank" title="Turizem" style="font-family: Arial, sans-serif, 'Roboto'; color:#fff;font-size:12px; text-align: center;text-decoration: none;">Turizem</a>
                            &nbsp;|&nbsp;
                            <a class="menu-item" href="[site_url]/deal-categories/storitve/" target="_blank" title="Storitve" style="font-family: Arial, sans-serif, 'Roboto'; color:#fff;font-size:12px; text-align: center;text-decoration: none;">Storitve</a>
                            &nbsp;|&nbsp;
                            <a class="menu-item" href="[site_url]/deal-categories/tecaji/" target="_blank" title="Tečaji" style="font-family: Arial, sans-serif, 'Roboto'; color:#fff;font-size:12px; text-align: center;text-decoration: none;">Tečaji</a>
                            &nbsp;|&nbsp;
                            <a class="menu-item" href="[site_url]/deal-categories/trgovina/" target="_blank" title="Trgovina" style="font-family: Arial, sans-serif, 'Roboto'; color:#fff;font-size:12px; text-align: center;text-decoration: none;">Trgovina</a>
                            &nbsp;|&nbsp;
                            <a class="menu-item" href="[site_url]/deal-categories/last-minute/" target="_blank" title="Last-minute" style="font-family: Arial, sans-serif, 'Roboto'; color:#fff;font-size:12px; text-align: center;text-decoration: none;">Last minute</a>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
        <!--END navigation-->

        <!--content-->
        <tr>
            <td valign="top" align="center" style="background-color:#fff; text-align: center;">
                <table  width="600"  height="34" border="0" cellspacing="0" cellpadding="0" align="center" style="background-color:#fff;font-family: Arial, sans-serif, 'Roboto'; color:#fff;text-align: center;" >
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <!--title-->
                    <tr>
                        <td colspan="9" width="580">
                            <p style="margin:0; display: block; text-decoration: none; font-family: Arial,sans-serif,'Roboto'; color: rgb(27, 0, 33); font-weight: bold; font-size: 22px;">
                                Nakup na [site_title] je zaključen
                            </p>
                        </td>
                    </tr>
                    <!--END title-->
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr align="left">
                        <td width="15"> </td>
                        <td width="570">
                            <img align="top" alt="Shadow-Top" src="[resources_url]/shadow-top.png"/>
                            <p style="margin:0;">
                            <table  border="0" cellspacing="0" cellpadding="0" align="left" style="font-family: Arial, sans-serif, 'Roboto'; font-size:14px; font-weight:normal; text-align: left; color:#1b0021; margin: 20px 0;">
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p style="margin:0; line-height: 20px;">
                                            <b>Pozdravljeni [name],</b>
                                            <br/>
                                            <br/>
                                            Sporočamo vamo, da smo prejeli plačilo za nakup, ki ste ga opravili na [site_title]. Aktivirali smo vam kupone, ki jih najdete na portalu, v razdelku <a href="[site_url]/vouchers" target="_blank">Moji kuponi</a>.
                                            <br/>
                                            S tem je vaš nakup zaključen.
                                            <br/>
                                            <br/>
                                            V nadaljevanju so navedene podrobnosti nakupa.
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>

                                <!--table-->
                                <tr>
                                    <td>
                                        <table  width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="background-color:#fff;font-family: Arial, sans-serif, 'Roboto'; color:#1b0021;text-align: center; font-size: 14px; margin: 20px 0; border: 1px solid #ccc; border-bottom: none;" >
                                            <tr bgcolor="#efefef">
                                                <td colspan="2" width="100%" style="text-align: left; font-weight: bold; padding-left: 10px; border-bottom: 1px solid #ccc;">
                                                    <p>Informacije o plačilu</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" style="text-align: right; border-bottom: 1px solid #ccc;">
                                                    <p>Skupaj:&nbsp;</p>
                                                </td>
                                                <td width="70%" style="text-align: left; font-weight: bold; border-bottom: 1px solid #ccc;">
                                                    <p>[total]</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" style="text-align: right; border-bottom: 1px solid #ccc;">
                                                    <p>ID transakcije:&nbsp;</p>
                                                </td>
                                                <td width="70%" style="text-align: left; font-weight: bold; border-bottom: 1px solid #ccc;">
                                                    <p>[transid]</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%" style="text-align: right; border-bottom: 1px solid #ccc;">
                                                    <p>Datum plačila:&nbsp;</p>
                                                </td>
                                                <td width="70%" style="text-align: left; font-weight: bold; border-bottom: 1px solid #ccc;">
                                                    <p>[date]</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" width="100%" style="border-bottom: 1px solid #ccc;">
                                                    [purchase_details]
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!--END table-->

                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p style="margin:0; font-family: Arial, sans-serif, 'Roboto'; color:#fff;font-size:14px; font-weight:normal; text-align: left;color:#1b0021; line-height: 20px;">
                                            Naj bo vaš dan uspešen!
                                            <br/>
                                            Ekipa [site_title]
                                        </p>
                                    </td>
                                </tr>
                            </table>
                            </p>
                        </td>
                        <td width="15"> </td>
                    </tr>
                </table>
            </td>
        </tr>
        <!--END content-->

        <!--footer-->
        <tr>
            <td valign="top" style="background-color:#fff;">
                <table  width="600" border="0" cellspacing="0" cellpadding="0" align="center" style="background-color:#fff;font-family: Arial, sans-serif, 'Roboto'; color:#fff;text-align: center;" >
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td width="10"> </td>
                        <td height="30" align="center" height="30" style="font-family: Arial, sans-serif, 'Roboto'; color:#fff;font-size:12px; text-align: center;">
                            <img align="top" alt="Shadow-Top" src="[resources_url]/shadow-top.png">
                        </td>
                        <td width="10"> </td>
                    </tr>
                    <tr>
                        <td width="10"> </td>
                        <td align="center" height="20" style="font-family: Arial, sans-serif, 'Roboto'; color:#ccc;font-size:12px; text-align: center;">
                            <a class="menu-item" href="[site_url]/contact/" target="_blank" title="Kontakt" style="font-family: Arial, sans-serif, 'Roboto'; color:#777;font-size:12px; text-align: center;text-decoration: none;">
                                Kontakt
                            </a>
                        </td>
                        <td width="10"> </td>
                    </tr>
                    <tr>
                        <td width="10"> </td>
                        <td height="30" align="center" height="20" style="font-family: Arial, sans-serif, 'Roboto'; color:#fff;font-size:12px; text-align: center;"> </td>
                        <td width="10"> </td>
                    </tr>
                </table>
            </td>
        </tr>
        <!--END footer-->
    </table>
</body>
</html>