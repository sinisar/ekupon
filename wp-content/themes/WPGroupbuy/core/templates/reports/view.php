<?php
if ( empty( $columns ) || empty( $records ) ) {
	do_action('wg_report_view');
	wpg_e( 'No Data' );
} else {
	global $wg_report_pages;
?>
<?php do_action('wg_report_view') ?>
<div id="wpg_report" class="ovauto">
	<?php do_action('wg_report_view_table_start') ?>
	<table class="listingOrder border_1_ddd border_bt_none border_t_none border_r_none" cellspacing="0" cellpadding="0" width="100%">
		<thead>
			<tr align="left">
			<?php foreach ( $columns as $key => $label ): ?>
				<th class="headProfile noneborderbot bRight cart-<?php esc_attr_e( $key ); ?>" scope="col"><?php esc_html_e( $label ); ?></th>
			<?php endforeach; ?>
			</tr>
		</thead>
		<tbody>
			<?php foreach ( $records as $record ): ?>
				<tr>
					<?php foreach ( $columns as $key => $label ): ?>
						<td class="itemProfile bRight cart-<?php esc_attr_e( $key ); ?>">
							<?php if ( isset( $record[$key] ) ) { echo $record[$key]; } ?>
						</td>
					<?php endforeach; ?>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>

<?php do_action('wg_report_view_nav') ?>
<div id="report_navigation pagination clearfix">
  <?php
	if ( $wg_report_pages > 1 ) {
		$report = WP_Groupbuy_Reports::get_instance( $_GET['report'] );
		$report_url = $report->get_url();
		$current_page = $_GET['showpage']-1;
		for ( $i=0; $i < $wg_report_pages; $i++ ) {
			$page_num = (int)$i; $page_num++;
			$active = ( $i == $current_page || ( $i == 0 ) && !isset( $_GET['showpage'] ) ) ? 'active' : '' ;
			$button = '<span class="report_nav_button '.$active.'"><a class="report_button button contrast_button" href="'.add_query_arg( array( 'report' => $_GET['report'], 'id' => $_GET['id'], 'showpage' => $i ), $report_url ).'">'.$page_num.'</a></span> ';
			echo $button;
		}
	}
?>
</div>
<?php do_action('wg_report_view_post_nav') ?>
<?php
}
?>
