<form id="wpg_cart" action="<?php wg_cart_url(); ?>" method="post">
	<table class="listingOrder border_1_ddd border_bt_none border_t_none border_r_none" cellspacing="0" cellpadding="0" width="100%">
		<thead>
			<tr>
			<?php foreach ( $columns as $key => $label ): ?>
				<th class="headProfile noneborderbot bRight cart-<?php esc_attr_e( $key ); ?>" scope="col"><?php esc_html_e( $label ); ?></th>
			<?php endforeach; ?>
			</tr>
		</thead>
		<tbody>
			<?php foreach ( $items as $item ): ?>
				<tr class="<?php esc_attr_e( $key ); ?>">
					<?php foreach ( $columns as $key => $label ): ?>
						<td class="itemProfile bRight cart-<?php esc_attr_e( $key ); ?>">
							<?php if ( isset( $item[$key] ) ) { echo $item[$key]; } ?>
						</td>
					<?php endforeach; ?>
				</tr>
			<?php endforeach; ?>
			<?php foreach ( $line_items as $key => $line ): ?>
				<tr class="<?php esc_attr_e( $key ); ?>">
					<th class="bBottom bRight" scope="row" colspan="<?php echo count( $columns )-1; ?>"><?php esc_html_e( $line['label'] ); ?></th>
					<td class="itemProfile bRight cart-line-item-<?php esc_attr_e( $key ); ?>"><?php esc_html_e( $line['data'] ); ?></td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<div id="wpg_cart_button" class="mTop10">
		<?php foreach ( $cart_controls as $control ): ?>
			<?php echo $control; ?>
		<?php endforeach; ?>
	</div>
</form>
