<div class="deal_block clearfix <?php if ( has_post_thumbnail() ) echo ' has_thumbnail' ?>">
	<?php if ( has_post_thumbnail() ):?>
	<div class="deal_thumbnail clearfix">
		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="post_thumbnail"><?php the_post_thumbnail( 'wpg_widget_thumb' ); ?></a>
	</div>
	<?php endif ?>
	<div class="widget_info clearfix">
		<span class="meta">
			<?php
				if ( wg_can_purchase() || ( function_exists('wg_is_deal_aggregated') && wg_is_deal_aggregated() ) ) {
					?><a href="<?php wg_add_to_cart_url(); ?>" class="small_buynow buynow button"><?php echo $buynow ?><span class="button_price"><?php wg_price(); ?></span></a><?php
				} else {
					echo '<span class="small_buynow buynow button"><span class="button_price">'.wpg__( 'Purchase Unavailable' ).'</span></span>';
				} ?>
		</span>
	</div>
	<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
</div>
