<div id="deal_share" class="clearfix">

	<?php if ( !wg_is_deal_complete() ): ?>

		<?php if ( wg_has_purchase_min() && wg_has_purchases_limit() ): ?>
			<p><?php echo apply_filters( 'wg_message_share_max', sprintf( wpg_e( 'Help reach the goal of %s buyers (limited quantity of %s) and receive %s reward points when your friends save.' ), wg_get_min_purchases(), wg_get_max_purchases(), wg_get_affiliate_credit() ), wg_get_min_purchases(), wg_get_max_purchases(), wg_get_affiliate_credit() ) ?></p>
		<?php elseif ( wg_has_purchase_min() ): ?>
			<p><?php echo apply_filters( 'wg_message_share', sprintf( wpg_e( 'Help reach the goal of %s buyers and receive %s reward points when your friends save.' ), wg_get_min_purchases(), wg_get_affiliate_credit() ), wg_get_min_purchases(), wg_get_affiliate_credit() ) ?><?php  ?></p>
		<?php endif ?>

	<?php endif ?>

	<div id="share_link" class="clearfix txt_l">

		<?php wpg_e( 'Share this URL:' ) ?>
           <input class="share_earn" onclick="this.select();" readonly value="<?php wg_share_link(); ?>">     
	</div>

</div>