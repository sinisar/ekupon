<div id="wpg_checkout">
<form id="wg_gift_redemption" class="" action="<?php wg_voucher_url(); ?>" method="get">
	<table class="">
		<tbody>
			<tr>
				<td><label for="gift_email"><?php wpg_e( 'Email Address' ); ?></label><br/><small>(<?php wpg_e( 'The email address the gift was sent to.' ); ?>)</small></td>
				<td><span class="wg-form-field wg-form-field-text wg-form-field-required"><input type="text" value="<?php esc_attr_e( $email ); ?>" id="gift_email" name="gift_email" size="40" /></span></td>
			</tr>

			<tr>
				<td><label for="gift_code"><?php wpg_e( 'Coupon Code' ); ?></td>
				<td><span class="wg-form-field wg-form-field-text wg-form-field-required"><input type="text" value="" id="gift_code" name="gift_code" size="40" /></span></td>
			</tr>
			<?php #do_action( 'wg_gift_redemption_form' ); ?>
		</tbody>
	</table>
<div align="center">
<input type="submit" class="profileAddGoldV6" value="<?php wpg_e( 'Redeem' ); ?>" />
</div>
</form>
</div>
<script type="text/javascript">
jQuery(document).ready(function(){
  	jQuery("#wg_gift_redemption").validate({
		rules: {
			'gift_email': "required",
			'gift_code': "required"
		  }
	});
	jQuery("#gift_email").focus();
});
</script>
