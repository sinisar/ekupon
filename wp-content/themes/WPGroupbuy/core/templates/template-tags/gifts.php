<?php

// Gift redemption URL
function wg_gift_redemption_url() {
	echo apply_filters( 'wg_gift_redemption_url', wg_get_gift_redemption_url() );
}

// Get the gift redemption url
function wg_get_gift_redemption_url() {
	$url = WP_Groupbuy_Gifts::get_url();
	return apply_filters( 'wg_get_gift_redemption_url', $url );
}

// Send as gift url
function wg_get_send_as_gift_url( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	return apply_filters( 'wg_get_send_as_gift_url', add_query_arg( array( 'gifter' => 1 ), wg_get_add_to_checkout_url() ) );
}
// Prints send as gift url
function wg_send_as_gift_url( $post_id = 0 ) {
	echo apply_filters( 'wg_send_as_gift_url', wg_get_send_as_gift_url( $post_id ) );
}