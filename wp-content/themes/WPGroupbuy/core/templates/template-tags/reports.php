<?php

// Get a deals purchase report url.
function wg_get_deal_purchase_report_url( $post_id = 0, $csv = FALSE ) {
	if ( !$post_id ) {
		$post_id = @$_GET['id'] ? : get_the_ID();
	}

	$report = WP_Groupbuy_Reports::get_instance( 'deal_purchase' );
	if ( $csv ) {
		return apply_filters( 'wg_get_deal_purchase_report_url', add_query_arg( array( 'report' => 'deal_purchase', 'id' => $post_id ), $report->get_csv_url() ) );
	}
	return apply_filters( 'wg_get_deal_purchase_report_url', add_query_arg( array( 'report' => 'deal_purchase', 'id' => $post_id ), $report->get_url() ) );
}

// Print deals purchase report url
function wg_deal_purchase_report_url( $post_id = 0 ) {
	echo apply_filters( 'wg_deal_purchase_report_url', wg_get_deal_purchase_report_url( $post_id ) );
}

// Print deals purchase report link <a>
function wg_deal_purchase_report_link( $post_id = 0 ) {
	$link = '<a href="'.wg_get_deal_purchase_report_url( $post_id ).'" class="report_button">'.wpg__( 'Purchases' ).'</a>';
	echo apply_filters( 'wg_deal_purchase_report_url', $link );
}

// Print the deals purchase CSV url
function wg_deal_purchase_report_csv_url( $post_id = 0 ) {
	echo apply_filters( 'wg_deal_purchase_report_url', wg_get_deal_purchase_report_url( $post_id, true ) );
}

// Get a deals merchants report url
function wg_get_merchant_purchase_report_url( $post_id = 0, $csv = FALSE ) {
	if ( !$post_id ) {
		$post_id = @$_GET['id'] ? : get_the_ID();
	}

	$report = WP_Groupbuy_Reports::get_instance( 'merchant_purchase' );
	if ( $csv ) {
		return apply_filters( 'wg_get_merchant_purchase_report_url', add_query_arg( array( 'report' => 'merchant_purchase', 'id' => $post_id ), $report->get_csv_url() ) );
	}
	return apply_filters( 'wg_get_merchant_purchase_report_url', add_query_arg( array( 'report' => 'merchant_purchase', 'id' => $post_id ), $report->get_url() ) );
}

// Print deals merchants report url
function wg_merchant_purchase_report_url( $post_id = 0 ) {
	echo apply_filters( 'wg_merchant_purchase_report_url', wg_get_merchant_purchase_report_url( $post_id ) );
}

// Print deals merchants report link
function wg_merchant_purchase_report_link( $post_id = 0 ) {
	$link = '<a href="'.wg_get_merchant_purchase_report_url( $post_id ).'" class="viewdetailOrder">'.wpg__( 'View Purchases' ).'</a>';
	echo apply_filters( 'wg_merchant_purchase_report_url', $link );
}

// Print the merchants purchase CSV url
function wg_merchant_purchase_report_csv_url( $post_id = 0 ) {
	echo apply_filters( 'wg_merchant_purchase_report_url', wg_get_merchant_purchase_report_url( $post_id, true ) );
}

// Get a deals voucher report url.
function wg_get_deal_voucher_report_url( $post_id = 0, $csv = FALSE ) {
	if ( !$post_id ) {
		$post_id = @$_GET['id'] ? : get_the_ID();
	}

	$report = WP_Groupbuy_Reports::get_instance( 'deal_voucher' );
	if ( $csv ) {
		return apply_filters( 'wg_get_deal_voucher_report_url', add_query_arg( array( 'report' => 'deal_voucher', 'id' => $post_id ), $report->get_csv_url() ) );
	}
	return apply_filters( 'wg_get_deal_voucher_report_url', add_query_arg( array( 'report' => 'deal_voucher', 'id' => $post_id ), $report->get_url() ) );
}

// Print deals voucher report url
function wg_deal_voucher_report_url( $post_id = 0 ) {
	echo apply_filters( 'wg_deal_voucher_report_url', wg_get_deal_voucher_report_url( $post_id ) );
}

// Print deals voucher report link
function wg_deal_voucher_report_link( $post_id = 0 ) {
	$link = '<a href="'.wg_get_deal_voucher_report_url( $post_id ).'" class="report_button">'.wpg__( 'Voucher Report' ).'</a>';
	echo apply_filters( 'wg_deal_voucher_report_url', $link );
}

// Print the deals voucher CSV url
function wg_deal_voucher_report_csv_url( $post_id = 0 ) {
	echo apply_filters( 'wg_deal_voucher_report_url', wg_get_deal_voucher_report_url( $post_id, true ) );
}

// Get a merchant voucher report url
function wg_get_merchant_voucher_report_url( $post_id = 0, $csv = FALSE ) {
	if ( !$post_id ) {
		$post_id = @$_GET['id'] ? : get_the_ID();
	}

	$report = WP_Groupbuy_Reports::get_instance( 'merchant_voucher' );
	if ( $csv ) {
		return apply_filters( 'wg_get_merchant_voucher_report_url', add_query_arg( array( 'report' => 'merchant_voucher', 'id' => $post_id ), $report->get_csv_url() ) );
	}
	return apply_filters( 'wg_get_merchant_voucher_report_url', add_query_arg( array( 'report' => 'merchant_voucher', 'id' => $post_id ), $report->get_url() ) );
}

// Print merchant voucher report url
function wg_merchant_voucher_report_url( $post_id = 0 ) {
	echo apply_filters( 'wg_merchant_voucher_report_url', wg_get_merchant_voucher_report_url( $post_id ) );
}

// Print merchant voucher report link
function wg_merchant_voucher_report_link( $post_id = 0 ) {
	$link = '<a href="'.wg_get_merchant_voucher_report_url( $post_id ).'" class="viewdetailOrder">'.wpg__( 'Voucher Report' ).'</a>';
	echo apply_filters( 'wg_merchant_voucher_report_url', $link );
}

// Print the merchant voucher CSV url
function wg_merchant_voucher_report_csv_url( $post_id = 0 ) {
	echo apply_filters( 'wg_merchant_voucher_report_url', wg_get_merchant_voucher_report_url( $post_id, true ) );
}

// Get a deals purchases report url
function wg_get_purchases_report_url( $post_id = 0, $csv = FALSE ) {
	if ( !$post_id ) {
		$post_id = @$_GET['id'] ? : get_the_ID();
	}

	$report = WP_Groupbuy_Reports::get_instance( 'purchases' );
	if ( $csv ) {
		return apply_filters( 'wg_get_purchases_report_url', add_query_arg( array( 'report' => 'purchases', 'id' => $post_id ), $report->get_csv_url() ) );
	}
	return apply_filters( 'wg_get_purchases_report_url', add_query_arg( array( 'report' => 'purchases', 'id' => $post_id ), $report->get_url() ) );
}

// Print deals purchases report url
function wg_purchases_report_url( $post_id = 0 ) {
	echo apply_filters( 'wg_purchases_report_url', wg_get_purchases_report_url( $post_id ) );
}

// Print deals purchases report link
function wg_purchases_report_link( $post_id = 0 ) {
	$link = '<a href="'.wg_get_purchases_report_url( $post_id ).'" class="report_button">'.wpg__( 'Purchase History' ).'</a>';
	echo apply_filters( 'wg_purchases_report_url', $link );
}

// Print the deals purchases CSV url
function wg_purchases_report_csv_url( $post_id = 0 ) {
	echo apply_filters( 'wg_purchases_report_url', wg_get_purchases_report_url( $post_id, true ) );
}

// Get a accounts report url
function wg_get_accounts_report_url( $csv = FALSE ) {
	$report = WP_Groupbuy_Reports::get_instance( 'accounts' );
	if ( $csv ) {
		return apply_filters( 'wg_get_accounts_report_url', add_query_arg( array( 'report' => 'accounts'), $report->get_csv_url() ) );
	}
	return apply_filters( 'wg_get_accounts_report_url', add_query_arg( array( 'report' => 'accounts', 'id' => @$_GET['id'] ), $report->get_url() ) );
}

// Print accounts report url
function wg_accounts_report_url() {
	echo apply_filters( 'wg_accounts_report_url', wg_get_accounts_report_url() );
}

// Print accounts report link
function wg_accounts_report_link() {
	$link = '<a href="'.wg_get_accounts_report_url().'" class="report_button">'.wpg__( 'Accounts' ).'</a>';
	echo apply_filters( 'wg_accounts_report_url', $link );
}

// Print the accounts CSV url
function wg_accounts_report_csv_url( $post_id = 0 ) {
	echo apply_filters( 'wg_accounts_report_url', wg_get_accounts_report_url( $post_id, true ) );
}

// Get a merchant purchases report url
function wg_get_merchant_purchases_report_url( $post_id = 0, $csv = FALSE ) {
	if ( !$post_id ) {
		$post_id = @$_GET['id'] ? : get_the_ID();
	}

	$report = WP_Groupbuy_Reports::get_instance( 'merchant_purchases' );
	if ( $csv ) {
		return apply_filters( 'wg_get_merchant_purchases_report_url', add_query_arg( array( 'report' => 'merchant_purchases', 'id' => $post_id ), $report->get_csv_url() ) );
	}
	return apply_filters( 'wg_get_merchant_purchases_report_url', add_query_arg( array( 'report' => 'merchant_purchases', 'id' => $post_id ), $report->get_url() ) );
}

// Print merchant purchases report url
function wg_merchant_purchases_report_url( $post_id = 0 ) {
	echo apply_filters( 'wg_merchant_purchases_report_url', wg_get_merchant_purchases_report_url( $post_id ) );
}

// Print merchant purchases report link
function wg_merchant_purchases_report_link( $post_id = 0 ) {
	$link = '<a href="'.wg_get_merchant_purchases_report_url( $post_id ).'" class="report_button">'.wpg__( 'Purchase History' ).'</a>';
	echo apply_filters( 'wg_merchant_purchases_report_url', $link );
}

// Print the merchant purchases CSV url
function wg_merchant_purchases_report_csv_url( $post_id = 0 ) {
	echo apply_filters( 'wg_merchant_purchases_report_url', wg_get_merchant_purchases_report_url( $post_id, true ) );
}

// Get a credits report url.
function wg_get_credits_report_url( $post_id = 0, $csv = FALSE ) {
	if ( !$post_id ) {
		$post_id = @$_GET['id'] ? : get_the_ID();
	}

	$report = WP_Groupbuy_Reports::get_instance( 'credits' );
	if ( $csv ) {
		return apply_filters( 'wg_get_credits_report_url', add_query_arg( array( 'report' => 'credits', 'id' => $post_id ), $report->get_csv_url() ) );
	}
	return apply_filters( 'wg_get_credits_report_url', add_query_arg( array( 'report' => 'credits', 'id' => $post_id ), $report->get_url() ) );
}

// Print credits report url
function wg_credits_report_url( $post_id = 0 ) {
	echo apply_filters( 'wg_credits_report_url', wg_get_credits_report_url( $post_id ) );
}

// Print credits report link
function wg_credits_report_link( $post_id = 0 ) {
	$link = '<a href="'.wg_get_credits_report_url( $post_id ).'" class="report_button">'.wpg__( 'Purchase History' ).'</a>';
	echo apply_filters( 'wg_credits_report_url', $link );
}

// Print the credits CSV url
function wg_credits_report_csv_url( $post_id = 0 ) {
	echo apply_filters( 'wg_credits_report_url', wg_get_credits_report_url( $post_id, true ) );
}

// Get a credit purchases report url
function wg_get_credit_purchases_report_url( $post_id = 0, $csv = FALSE ) {
	if ( !$post_id ) {
		$post_id = @$_GET['id'] ? : get_the_ID();
	}

	$report = WP_Groupbuy_Reports::get_instance( 'credit_purchases' );
	if ( $csv ) {
		return apply_filters( 'wg_get_credit_purchases_report_url', add_query_arg( array( 'report' => 'credit_purchases', 'id' => $post_id ), $report->get_csv_url() ) );
	}
	return apply_filters( 'wg_get_credit_purchases_report_url', add_query_arg( array( 'report' => 'credit_purchases', 'id' => $post_id ), $report->get_url() ) );
}

// Print credit purchases report url
function wg_credit_purchases_report_url( $post_id = 0 ) {
	echo apply_filters( 'wg_credit_purchases_report_url', wg_get_credit_purchases_report_url( $post_id ) );
}

// Print credit purchases report link
function wg_credit_purchases_report_link( $post_id = 0 ) {
	$link = '<a href="'.wg_get_credit_purchases_report_url( $post_id ).'" class="report_button">'.wpg__( 'Purchase History' ).'</a>';
	echo apply_filters( 'wg_credit_purchases_report_url', $link );
}

// Print the credit purchases CSV url
function wg_credit_purchases_report_csv_url( $post_id = 0 ) {
	echo apply_filters( 'wg_credit_purchases_report_url', wg_get_credit_purchases_report_url( $post_id, true ) );
}

// Get the currently viewed report's CSV download URL
function wg_get_current_report_csv_download_url() {

	if ( !isset( $_GET['report'] ) || $_GET['report'] == '' )
		return;

	$rep = $_GET['report'];
	$report = WP_Groupbuy_Reports::get_instance( $rep );
	$id = ( isset( $_GET['id'] ) ) ? $_GET['id'] : 0 ;
	$showpage = ( isset( $_GET['showpage'] ) ) ? $_GET['showpage'] : 0 ;
	$url = add_query_arg(
		array(
			'report' => $rep,
			'id' => $id,
			'showpage' => $showpage
		),
		$report->get_csv_url() );

	if ( isset( $_GET['filter'] ) && $_GET['filter'] != '' ) {
		$url = add_query_arg(
			array(
				'filter' => $_GET['filter'] ),
			$url );
	}

	return apply_filters( 'wg_get_current_report_csv_download_url', $url );

}

// Print the currently viewed report's CSV download URL
function wg_current_report_csv_download_url() {
	echo apply_filters( 'wg_current_report_csv_download_url', wg_get_current_report_csv_download_url() );
}
