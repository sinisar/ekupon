<?php

function wg_get_account_post_type() {
	return WP_Groupbuy_Account::POST_TYPE;
}

// Checks deals availability
function wg_can_purchase( $post_id = 0, $user_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}

	$account = WP_Groupbuy_Account::get_instance( $user_id );
	return apply_filters( 'wg_can_purchase', $account->can_purchase( $post_id ) );
}

// On account page
function wg_on_account_page() {
	if ( 1 == get_query_var( WP_Groupbuy_Accounts::ACCOUNT_QUERY_VAR ) ) {
		return true;
	}
	return;
}

// On account edit page
function wg_on_account_edit_page() {
	if ( 1 == get_query_var( WP_Groupbuy_Accounts_Edit_Profile::EDIT_PROFILE_QUERY_VAR ) ) {
		return true;
	}
	return;
}

// On account edit page
function wg_on_import_user_page() {
	if ( 1 == get_query_var( WP_Groupbuy_Accounts_Import_Users::IMPORT_USERS_QUERY_VAR ) ) {
		return true;
	}
	return;
}

// On login page
function wg_on_login_page() {
	if ( 1 == get_query_var( WP_Groupbuy_Accounts_Login::LOGIN_QUERY_VAR ) ) {
		return true;
	}
	return;
}

// On registration page
function wg_on_registration_page() {
	if ( 1 == get_query_var( WP_Groupbuy_Accounts_Registration::REGISTER_QUERY_VAR ) ) {
		return true;
	}
	return;
}

// On reset password page
function wg_on_reset_password_page() {
	if ( 1 == get_query_var( WP_Groupbuy_Accounts_Retrieve_Password::RP_QUERY_VAR ) ) {
		return true;
	}
	return;
}

// Get Account credit balance
function wg_get_account_balance( $user_id = 0, $type = WP_Groupbuy_Accounts::CREDIT_TYPE ) {
	$possible_affilite_name = array(
			'rewards',
			'reward',
			'points',
			'point'
		);
	if ( in_array( $type, $possible_affilite_name ) ) {
		$type = WP_Groupbuy_Affiliates::CREDIT_TYPE;
	}
	$account = WP_Groupbuy_Account::get_instance( $user_id );
	$balance = $account->get_credit_balance( $type );
	if ( $balance == '' )
		return apply_filters( 'wg_get_account_balance', '0' );
	return apply_filters( 'wg_get_account_balance', $balance );
}
function wg_account_balance( $user_id = 0 ) {
	echo apply_filters( 'wg_account_balance', wg_get_account_balance( $user_id ) );
}

function wg_merchant_enabled() {
	static $enabled;
	if (!isset($enabled)) {
		$enabled = get_option( 'wg_enable_merchant', class_exists('WP_Groupbuy_Merchant') );
	}
	return $enabled;
}

// Does a user have a merchant assigned
function wg_account_has_merchant( $user_id = 0 ) {
	if ( !wg_merchant_enabled() ) {
		return false;
	}
	$ids = wg_account_merchant_id( $user_id );
	return ( empty( $ids ) ) ? FALSE : TRUE ;
}

// Get merchant ID assigned to a user
function wg_account_merchant_id( $user_id = 0, $all = false ) {
	if ( !$user_id ) {
		$user_id = get_current_user_id();
	}
	$merchant_ids = wg_get_merchants_by_account( $user_id );
	if ( !$all ) {
		$merchant_ids = $merchant_ids[0];
	}
	return apply_filters( 'wg_account_merchant_id', $merchant_ids );
}

// Account URL
function get_wpg_account_link() {
	return apply_filters( 'get_wpg_account_link', wg_get_account_url() );
}

// Account URL
function wpg_account_link() {
	echo apply_filters( 'wpg_account_link', get_wpg_account_link() );
}

// Account URL
function wg_account_url() {
	echo apply_filters( 'wg_account_url', wg_get_account_url() );
}

// Account URL
function wg_get_account_url() {
	$url = WP_Groupbuy_Accounts::get_url();
	return apply_filters( 'wg_get_account_url', $url );
}

// Login URL
function wg_account_login_url() {
	echo apply_filters( 'wg_account_login_url', wg_get_account_login_url() );
}

// Login URL
function wg_get_account_login_url() {
	$url = WP_Groupbuy_Accounts_Login::get_url();
	return apply_filters( 'wg_get_account_login_url', $url );
}

// Lost Password URL
function wg_account_lost_password_url() {
	echo apply_filters( 'wg_account_login_url', wg_get_account_lost_password_url() );
}

// Lost password URL
function wg_get_account_lost_password_url() {
	$url = add_query_arg( array( 'action' => 'lostpassword' ), WP_Groupbuy_Accounts_Login::get_url() );
	return apply_filters( 'wg_get_account_lost_password_url', $url );
}

// Merchant registration URL
function wg_account_register_url() {
	echo apply_filters( 'wg_account_register_url', wg_get_account_register_url() );
}

// Registration URL
function wg_get_account_register_url() {
	$url = WP_Groupbuy_Accounts_Registration::get_url();
	if ( isset( $_REQUEST['redirect_to'] ) ) {
		$url = add_query_arg( array( 'redirect_to' => urlencode( $_REQUEST['redirect_to'] ) ), $url );
	}
	return apply_filters( 'wg_get_account_register_url', $url );
}

// Account Edit URL
function wg_account_edit_url() {
	echo apply_filters( 'wg_account_edit_url', wg_get_account_edit_url() );
}

// Account Edit URL
function wg_get_account_edit_url() {
	$url = WP_Groupbuy_Accounts_Edit_Profile::get_url();
	return apply_filters( 'wg_get_account_edit_url', $url );
}

function wg_account_import_users_url() {
	echo apply_filters( 'wg_account_import_users_url', wg_get_account_import_users_url() );
}

// Account Import Users URL
function wg_get_account_import_users_url() {
	$url = WP_Groupbuy_Accounts_Import_Users::get_url();
	return apply_filters( 'wg_get_account_import_users_url', $url );
}

// Logout URL
function wg_get_logout_url() {
	$url = WP_Groupbuy_Accounts_Login::log_out_url();
	return apply_filters( 'wg_get_logout_url', $url );

}

// Logout URL
function wg_logout_url() {
	$link = '<a href="' . wg_get_logout_url() . '"><span class="icOut">'.wpg__( 'Logout' ).'</span></a>';
	echo apply_filters( 'wg_logout_url', $link );

}

// Get the name of an account, fallback to user login
function wg_get_name( $user_id = 0 ) {
	if ( !$user_id ) {
		$user_id = get_current_user_id();
	}
	$account = WP_Groupbuy_Account::get_instance( $user_id );
	$wg_name = '';
	if ( is_a( $account, 'WP_Groupbuy_Account' ) ) {
		$wg_name = $account->get_name();
	}
	if ( strlen( $wg_name ) < 2 && $user_id) {
		$user = get_userdata( $user_id );
		$wg_name = $user->user_login;
	}
	
	return apply_filters( 'wg_get_name', $wg_name, $user_id );
}

//  Get user address as array
function wg_get_user_address( $user_id = 0 ) {
    if ( !$user_id ) {
        $user_id = get_current_user_id();
    }
    $account = WP_Groupbuy_Account::get_instance( $user_id );
    $wg_address = '';
    if ( is_a( $account, 'WP_Groupbuy_Account' ) ) {
        $wg_address = $account->get_address();
    }

    return apply_filters( 'wg_get_user_address', $wg_address, $user_id );
}



// Print the name of an account
function wg_name( $user_id = 0 ) {
	if ( !$user_id ) {
		$user_id = get_current_user_id();
	}
	$wg_name = wg_get_name( $user_id );
	echo apply_filters( 'wg_name', $wg_name, $user_id );
}

// Return a formatted address
function wg_format_address( $address, $return = 'array', $separator = "\n" ) {
	if ( empty( $address ) ) {
		return '';
	}
	$lines = array();
	if ( !empty($address['first_name']) || !empty($address['last_name']) ) {
		$lines[] = $address['first_name'].' '.$address['last_name'];
	}
	$lines[] = $address['street'];
	$city_line = $address['city'];
	if ( $city_line && ( $address['zone'] || $address['postal_code'] ) ) {
		$city_line .= ', ';
	}
	$city_line .= $address['zone'];
	$city_line = rtrim( $city_line ).' '.$address['postal_code'];
	$city_line = rtrim( $city_line );
	if ( $city_line ) {
		$lines[] = $city_line;
	}
	if ( $address['country'] ) {
		$lines[] = $address['country'];
	}
	switch ( $return ) {
	case 'array':
		return $lines;
	default:
		return apply_filters( 'wg_format_address', implode( $separator, $lines ), $address, $return, $separator );
	}
}

function wg_is_user_guest_purchaser( $user_id = 0 ) {
	if ( !$user_id ) {
		$user_id = get_current_user_id();
	}
	$flag = get_user_meta( $user_id, WP_Groupbuy_Accounts_Checkout::GUEST_PURCHASE_USER_FLAG );
	if ( $flag )
		return TRUE;

	return;
}

function wg_get_purchased_deals( $user_id = 0, $return_array = FALSE ) {
	if ( !$user_id ) {
		$user_id = get_current_user_id();
	}
	// Get all the user's purchases
	$purchases = WP_Groupbuy_Purchase::get_purchases( array(
			'user' => $user_id,
		) );

	$deal_ids = array();
	if ( !empty( $purchases ) ) {
		foreach ( $purchases as $purchase_id ) {
			$purchase = WP_Groupbuy_Purchase::get_instance( $purchase_id );
			$products = $purchase->get_products();
			foreach ( $products as $product ) {
				$deal_id = $product['deal_id'];
				if ( !in_array( $deal_id, $deal_ids ) ) {
					$item = ( $return_array ) ? $product : $deal_id ;
					$deal_ids[] = $item;
				}
			}
		}
	}
	return apply_filters( 'wg_get_purchased_deals', $deal_ids, $user_id, $purchases );
}

// Get the profile url
function wg_get_profile_link( $user_id = 0 ) {
	if ( !$user_id ) {
		$user_id = get_current_user_id();
	}
	$account = WP_Groupbuy_Account::get_instance( $user_id );
	return apply_filters( 'wg_get_profile_link', get_permalink( $account->get_id() ), $user_id );
}

// Get the profile url
function wg_profile_link( $user_id = 0 ) {
	echo apply_filters( 'wg_profile_link', wg_get_profile_link( $user_id ) );
}