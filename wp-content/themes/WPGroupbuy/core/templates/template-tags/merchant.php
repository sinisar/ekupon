<?php

// Get the merchant post type
function wg_get_merchant_post_type() {
	return WP_Groupbuy_Merchant::POST_TYPE;
}

// Currently viewing account page
function wg_on_merchant_account_page() {
	return ( wg_merchant_enabled() && wg_on_merchant_dashboard_page() && wg_on_deal_submit_page() );
}

// Currently viewing merchant dashboard
function wg_on_merchant_dashboard_page() {
	return ( wg_merchant_enabled() && get_query_var( WP_Groupbuy_Merchants_Dashboard::BIZ_DASH_QUERY_VAR ) );
}

// Currently viewing deal submit page
function wg_on_deal_submit_page() {
    return;
	//return ( wg_merchant_enabled() && get_query_var( WP_Groupbuy_Deals_Submit::SUBMIT_QUERY_VAR ) );
}

// Merchant category taxonomy name
function wg_get_merchant_cat_slug() {
	return WP_Groupbuy_Merchant::MERCHANT_TYPE_TAXONOMY;
}

// URLS

// Print merchant account/dashboard url
function wg_merchant_account_url() {
	echo apply_filters( 'wg_merchant_account_url', wg_get_merchant_account_url() );
}

// Return merchant account/dashboard url
function wg_get_merchant_account_url() {
	$url = WP_Groupbuy_Merchants_Dashboard::get_url();
	return apply_filters( 'wg_get_merchant_account_url', $url );
}

// Get merchants url
function wg_get_merchants_url() {
	$url = WP_Groupbuy_Merchant::get_url();
	return apply_filters( 'wg_get_merchants_url', $url );
}

// Print merchants url
function wg_merchants_url() {
	echo apply_filters( 'wg_merchants_url', wg_get_merchants_url() );
}

// Print merchant merchant/register url
function wg_merchant_registration_url() {
	echo apply_filters( 'wg_merchant_registration_url', wg_get_merchant_account_submit_url() );
}

// Return merchant merchant/register url
function wg_get_merchant_registration_url() {
	$url = WP_Groupbuy_Merchants_Registration::get_url();
	return apply_filters( 'wg_get_merchant_registration_url', $url );
}

// Return a merchant's url
function wg_get_merchant_url( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$merchant = WP_Groupbuy_Merchant::get_merchant_object( $post_id );
	if ( empty( $merchant ) ) return;
	return apply_filters( 'wg_get_merchant_url', get_permalink( $merchant->get_ID() ) );
}

// Does the merchant have a url
function wg_has_merchant_url( $post_id = 0 ) {
	return ( apply_filters( 'wg_get_merchant_url', wg_get_merchants_url( $post_id ) != '' ) ) ? TRUE : FALSE ;
}

// Print merchant's url
function wg_merchant_url( $post_id = 0 ) {
	echo apply_filters( 'wg_merchant_url', wg_get_merchant_url( $post_id ) );
}

// Claim URL for Merchants to manage vouchers
function wg_get_voucher_claim_url( $code = null, $redirect = null ) {
	$url = WP_Groupbuy_Merchants_Voucher_Claim::get_url();
	if ( null != $redirect ) {
		$url = add_query_arg( array( 'redirect_to' => $redirect ), $url );
	}
	if ( null == $code ) {
		return apply_filters( 'wg_get_merchants_url', $url );
	}
	$url = add_query_arg( array( WP_Groupbuy_Merchants_Voucher_Claim::BIZ_VOUCHER_CLAIM_ARG => $code ), $url );
	return apply_filters( 'wg_get_merchants_url', $url );
}

// URL for all merchant types. Taxonomy archive page.
function wg_get_merchant_type_url() {
	return apply_filters( 'wg_get_merchant_type_url', site_url( trailingslashit( WP_Groupbuy_Merchant::MERCHANT_TYPE_TAX_SLUG ) ) );
}

// Print merchant type archive url
function wg_merchant_type_url() {
	echo apply_filters( 'wg_merchant_type_url', wg_get_merchant_type_url() );
}

// Print Merchant registration url
function wg_merchant_register_url() {
	echo apply_filters( 'wg_merchant_register_url', wg_get_merchant_register_url() );
}

// Return Merchant registration url
function wg_get_merchant_register_url() {
	$url = WP_Groupbuy_Merchants_Registration::get_url();
	return apply_filters( 'wg_get_merchant_register_url', $url );
}

// Print Merchant account edit url
function wg_merchant_edit_url() {
	echo apply_filters( 'wg_merchant_edit_url', wg_get_merchant_edit_url() );
}

// Return Merchant account edit url
function wg_get_merchant_edit_url() {
	$url = WP_Groupbuy_Merchants_Edit::get_url();
	return apply_filters( 'wg_get_merchant_edit_url', $url );
}


// Misc. Functions

// Return all merchants this user is authorized to manage
function wg_get_merchants_by_account( $user_id = 0 ) {
	if ( null == $user_id ) {
		$user_id = get_current_user_id();
	}
	$merchants = WP_Groupbuy_Merchant::get_merchants_by_account( $user_id );
	return apply_filters( 'wg_get_merchants_by_account', $merchants, $user_id );
}

// Return all merchants this user is authorized to manage
function wg_get_merchant_authorized_users( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$merchant = WP_Groupbuy_Merchant::get_instance( $post_id );
	return apply_filters( 'wg_get_merchant_authorized_users', $merchant->get_authorized_users(), $post_id );
}

// Does this deal have a merchant assigned to it?
function wg_has_merchant( $post_id = 0 ) {
	return ( apply_filters( 'wg_has_merchant_id', !wg_get_merchant_id( $post_id ) ) ) ? FALSE: TRUE ;
}

// Get the merchant ID for a deal
function wg_get_merchant_id( $post_id = 0 ) {
	if ( !wg_merchant_enabled() ) {
		return 0;
	}
	
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$merchant = WP_Groupbuy_Merchant::get_merchant_object( $post_id );
	if ( !is_object( $merchant ) ) return FALSE;
	return apply_filters( 'wg_get_merchant_id', $merchant->get_id() );
}

// Get all purchase ids associated with a merchant. Based on deals associated with a merchant.
function wg_get_merchants_purchase_ids( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deals = wg_get_merchants_deal_ids( wg_account_merchant_id() );
	$purchases = WP_Groupbuy_Purchase::get_purchases( array( 'deal' => $deals ) );
	if ( empty( $purchases ) ) {
		$purchases = array(0);
	}
	return apply_filters( 'wg_get_merchants_purchase_ids', $purchases, $post_id );
}

// Get all deals associated with a merchant
function wg_get_merchants_deal_ids( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$merchant = WP_Groupbuy_Merchant::get_merchant_object( $post_id );
	$deals = $merchant->get_deal_ids();
	if ( empty( $deals ) ) {
		return -1;
	}
	return apply_filters( 'wg_get_merchants_deal_ids', $deals, $post_id );
}

// Get all deals associated with a merchant
function wg_get_merchant_deals( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$merchant = WP_Groupbuy_Merchant::get_instance( $post_id );
    return apply_filters( 'wg_get_merchant_deals', exclude_current_deal($merchant->get_active_deal_ids()) );
}

// Checks to see if a merchant has any associated deals
function wg_has_merchant_deals( $post_id = 0 ) {
	return ( apply_filters( 'wg_has_merchant_deals', !is_array( wg_get_merchant_deals( $post_id ) ) ) ) ? FALSE : TRUE ;
}

// Return a WP_Query object of deals associated with a merchant.
function wg_get_merchant_deals_query( $post_id = 0, $args = array() ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$merch_deals = null;
	$merchant_deals = wg_get_merchant_deals( $post_id );
	if ( empty( $merchant_deals ) ) return FALSE;
	$defaults=array(
		'post_type' => wg_get_deal_post_type(),
		'post__in' => $merchant_deals,
		'post_status' => 'publish'
	);
	$args = wp_parse_args( $args, $defaults );
	$deals = new WP_Query( $args );

	return apply_filters( 'wg_get_merchant_deals_query', $deals );
}


// Merchant Types

// Return the merchant types object
function wg_get_merchant_types( $empty = true ) {
	return apply_filters( 'wg_get_merchant_types', get_terms( WP_Groupbuy_Merchant::MERCHANT_TYPE_TAXONOMY, array( 'hide_empty'=>$empty, 'fields'=>'all' ) ) );

}

// Get the current merchant type being viewed
function wg_get_current_merchant_type( $slug = false ) {
	$taxonomy = get_query_var( 'taxonomy' );
	if ( $taxonomy == WP_Groupbuy_Merchant::MERCHANT_TYPE_TAXONOMY ) {
		global $wp_query;
		if ( $slug ) {
			return apply_filters( 'wg_get_current_merchant_type', $wp_query->get_queried_object()->slug );
		} else {
			return apply_filters( 'wg_get_current_merchant_type', $wp_query->get_queried_object()->name );
		}
	}
	return FALSE;
}

// Print the current merchant type name
function wg_current_merchant_type() {
	echo apply_filters( 'wg_current_merchant_type', wg_get_current_merchant_type() );
}

// Print a list of merchant types for a particular merchant
function wg_get_merchants_types_list( $post_id = 0, $format = 'ul', $all_link = FALSE, $ulclass = 'merchant-type-ul clearfix' ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}

	$types = wg_get_merchants_types( $post_id );

	if ( empty( $types ) )
		return '';

	$tag = $format;

	$list = '';
	if ( $format == 'ul' || $format == 'ol' ) {
		$list .= "<".$format." class='".$ulclass."'>";
		$tag = 'li';
	}

	if ( $all_link != FALSE ) {
		$list .= "<".$tag." id='location_slug_all' class='type-item'>";
		$list .= "<a href='".wg_get_merchants_url()."' title='Visit All Merchant Types Deals' id='location_slug_all'>".$all_link."</a>";
		$list .= "</".$tag.">";
	}
	foreach ( $types as $type ) {
		$link = get_term_link( $type->slug, WP_Groupbuy_Merchant::MERCHANT_TYPE_TAXONOMY );
		$active = ( $type->name == wg_get_current_merchant_type() ) ? 'current_item' : 'item';
		$list .= "<".$tag." id='location_slug_".$type->slug."' class='type-item ".$active."'>";
		$list .= "<a href='".$link."' title='Visit ".$type->name."s Deals' id='location_slug_".$type->slug."'>".$type->name."</a>";
		$list .= "</".$tag.">";
	}

	if ( $format == 'ul' || $format == 'ol' )
		$list .= "</".$format.">";

	echo apply_filters( 'wg_get_merchant_types_list', $list, $format );

}

// Print a list of all merchant types
function wg_get_all_merchant_types_list( $format = 'ul', $all_link = FALSE, $ulclass = 'merchant-type-ul clearfix' ) {
	$types = wg_get_merchant_types();

	if ( empty( $types ) )
		return '';

	$tag = $format;

	$list = '';
	if ( $format == 'ul' || $format == 'ol' ) {
		$list .= "<".$format." class='".$ulclass."'>";
		$tag = 'li';
	}

	if ( $all_link != FALSE ) {
		$list .= "<".$tag." id='location_slug_all' class='type-item'>";
		$list .= "<a href='".wg_get_merchants_url()."' title='Visit All Merchant Types Deals' id='location_slug_all'>".$all_link."</a>";
		$list .= "</".$tag.">";
	}
	foreach ( $types as $type ) {
		$link = get_term_link( $type->slug, WP_Groupbuy_Merchant::MERCHANT_TYPE_TAXONOMY );
		$active = ( $type->name == wg_get_current_merchant_type() ) ? 'current_item' : 'item';
		$list .= "<".$tag." id='location_slug_".$type->slug."' class='type-item ".$active."'>";
		$list .= "<a href='".$link."' title='Visit ".$type->name."s Deals' id='location_slug_".$type->slug."'>".$type->name."</a>";
		$list .= "</".$tag.">";
	}

	if ( $format == 'ul' || $format == 'ol' )
		$list .= "</".$format.">";

	echo apply_filters( 'wg_get_merchant_types_list', $list, $format );

}

// Get a merchant's assigned types
function wg_get_merchants_types( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	return apply_filters( 'wg_get_merchants_types', wp_get_object_terms( $post_id, WP_Groupbuy_Merchant::MERCHANT_TYPE_TAXONOMY ) );
}

// Return the Merchant type. If a merchant has more than one type the first in the array is returned.
function wg_get_merchants_type( $post_id = 0 ) {
	$types = wg_get_merchants_types( $post_id );
	if ( empty( $types ) ) return;
	return apply_filters( 'wg_get_merchants_type', $types[0] );
}

// Print the merchant's type url.
function wg_merchants_type_url( $post_id = 0 ) {
	$type = wg_get_merchants_type( $post_id );
	if ( empty( $type ) ) return;
	$link = '<a href="'.get_term_link( $type->slug, WP_Groupbuy_Merchant::MERCHANT_TYPE_TAXONOMY ).'">'.$type->name.'</a>';

	echo apply_filters( 'wg_merchants_type_url', $link );
}

// Information

// Get the total sold count for a particular merchant. Total is cached via transients for 30 minutes.
function wg_get_merchant_total_sold( $merchant_id = null, $refresh = false ) {

	if ( null == $merchant_id ) {
		$merchant_id = current( wg_get_merchants_by_account() ); // might as well return the first, todo loop it
	}

	$cache_key = 'wg_total_sold_'.$merchant_id;
	if ( !$refresh ) {
		$cache = get_transient( $cache_key );
		if ( !empty( $cache ) ) {
			return $cache;
		}
	}

	$count = 0;
	if ( !is_array( wg_get_merchants_deal_ids( $merchant_id ) ) ) {
		return apply_filters( 'wg_get_merchant_total_sold', $count );
	}

	$args=array(
		'post_type' => wg_get_deal_post_type(),
		'post__in' => wg_get_merchants_deal_ids( $merchant_id ),
		'post_status' => 'any',
		'posts_per_page' => -1, // return this many

	);
	$merch_deals = new WP_Query( $args );
	if ( $merch_deals->have_posts() ) {
		while ( $merch_deals->have_posts() ) : $merch_deals->the_post();
		$count += (int) wg_get_number_of_purchases();
		endwhile;
	}
	set_transient( $cache_key, $count, 60*30 );
	return apply_filters( 'wg_get_merchant_total_sold', $count );
}

// Print the total sold count for a particular merchant.
function wg_merchant_total_sold( $merchant_id = null ) {
	echo apply_filters( 'wg_merchant_total_sold', wg_get_merchant_total_sold( $merchant_id ) );
}

// Merchant Name
function wg_get_merchant_name( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}

	$merchant = WP_Groupbuy_Merchant::get_merchant_object( $post_id );
	if ( empty( $merchant ) ) return;
	return apply_filters( 'wg_get_merchant_name', get_the_title( $merchant->get_ID() ) );
}

// Does merchant have a name set
function wg_has_merchant_name( $post_id = 0 ) {
	return ( wg_merchant_enabled() && apply_filters( 'wg_has_merchant_name', wg_get_merchant_name( $post_id ) != '' ) ) ;
}

// Print merchant name
function wg_merchant_name( $post_id = 0 ) {
	echo apply_filters( 'wg_merchant_name', wg_get_merchant_name( $post_id ) );
}

// Get the merchant description/excerpt
function wg_get_merchant_excerpt( $post_id = 0, $read_more = '...' ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$merchant = WP_Groupbuy_Merchant::get_merchant_object( $post_id );
	if ( empty( $merchant ) ) return;
	$excert = wg_get_excerpt_char_truncation( '400', $merchant->get_ID() );
	return apply_filters( 'wg_get_merchant_excerpt', $excert, $post_id, $read_more );
}

// Does the merchant have an description/excerpt to show
function wg_has_merchant_excerpt( $post_id = 0 ) {
	return ( apply_filters( 'wg_has_merchant_excerpt', wg_get_merchant_excerpt( $post_id ) != '' ) ) ? TRUE : FALSE ;
}

// Print the merchant excerpt
function wg_merchant_excerpt( $post_id = 0 ) {
	echo apply_filters( 'wg_merchant_excerpt', wg_get_merchant_excerpt( $post_id ) );
}

// Get the merchant's contact name
function wg_get_merchant_contact_name( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$merchant = WP_Groupbuy_Merchant::get_merchant_object( $post_id );
	return apply_filters( 'wg_get_merchant_contact_name', $merchant->get_contact_name() );
}

// Does the merchant have a contact name assigned
function wg_has_merchant_contact_name( $post_id = 0 ) {
	return ( apply_filters( 'wg_has_merchant_contact_name', wg_get_merchant_contact_name( $post_id ) != '' ) ) ? TRUE : FALSE ;
}

// Print the contact name
function wg_merchant_contact_name( $post_id = 0 ) {
	echo apply_filters( 'wg_contact_merchant_name', wg_get_merchant_contact_name( $post_id ) );
}

// Get the merchant's city
function wg_get_merchant_city( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$merchant = WP_Groupbuy_Merchant::get_merchant_object( $post_id );
	return apply_filters( 'wg_get_merchant_city', $merchant->get_contact_city() );
}

// Does the merchant have a city assigned
function wg_has_merchant_city( $post_id = 0 ) {
	return ( apply_filters( 'wg_has_merchant_city', wg_get_merchant_city( $post_id ) != '' ) ) ? TRUE : FALSE ;
}

// Print the merchant city
function wg_merchant_city( $post_id = 0 ) {
	echo apply_filters( 'wg_merchant_city', wg_get_merchant_city( $post_id ) );
}

// Get the merchant's street address
function wg_get_merchant_street( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$merchant = WP_Groupbuy_Merchant::get_merchant_object( $post_id );
	return apply_filters( 'wg_get_merchant_street', $merchant->get_contact_street() );
}

// Does the merchant have a street address assigned
function wg_has_merchant_street( $post_id = 0 ) {
	return ( apply_filters( 'wg_has_merchant_street', wg_get_merchant_street( $post_id ) != '' ) ) ? TRUE : FALSE ;
}

// Print the street address of a merchant
function wg_merchant_street( $post_id = 0 ) {
	echo apply_filters( 'wg_merchant_street', wg_get_merchant_street( $post_id ) );
}

// Get the merchant's state
function wg_get_merchant_state( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$merchant = WP_Groupbuy_Merchant::get_merchant_object( $post_id );
	return apply_filters( 'wg_get_merchant_state', $merchant->get_contact_state() );
}

// Does the merchant have a state assigned
function wg_has_merchant_state( $post_id = 0 ) {
	return ( apply_filters( 'wg_has_merchant_state', wg_get_merchant_state( $post_id ) != '' ) ) ? TRUE : FALSE ;
}

// Print the state of a merchant
function wg_merchant_state( $post_id = 0 ) {
	echo apply_filters( 'wg_merchant_state', wg_get_merchant_state( $post_id ) );
}

// Get the merchant's zip
function wg_get_merchant_zip( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$merchant = WP_Groupbuy_Merchant::get_merchant_object( $post_id );
	return apply_filters( 'wg_get_merchant_zip', $merchant->get_contact_postal_code() );
}

// Does the merchant have a zip assigned
function wg_has_merchant_zip( $post_id = 0 ) {
	return ( apply_filters( 'wg_has_merchant_zip', wg_get_merchant_zip( $post_id ) != '' ) ) ? TRUE : FALSE ;
}

// Print the merchants zip
function wg_merchant_zip( $post_id = 0 ) {
	echo apply_filters( 'wg_merchant_zip', wg_get_merchant_zip( $post_id ) );
}

// Get the merchant's country
function wg_get_merchant_country( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$merchant = WP_Groupbuy_Merchant::get_merchant_object( $post_id );
	return apply_filters( 'wg_get_merchant_country', $merchant->get_contact_country() );
}

// Does the merchant have a country assigned
function wg_has_merchant_country( $post_id = 0 ) {
	return ( apply_filters( 'wg_has_merchant_country', wg_get_merchant_country( $post_id ) != '' ) ) ? TRUE : FALSE ;
}

// Print the merchant's country
function wg_merchant_country( $post_id = 0 ) {
	echo apply_filters( 'wg_merchant_country', wg_get_merchant_country( $post_id ) );
}

// Get the merchant's phone
function wg_get_merchant_phone( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Merchant::get_instance( $post_id );
	return apply_filters( 'wg_get_merchant_phone', $deal->get_contact_phone() );
}

// Does the merchant have a phone assigned
function wg_has_merchant_phone( $post_id = 0 ) {
	return ( apply_filters( 'wg_has_merchant_phone', wg_get_merchant_phone( $post_id ) != '' ) ) ? TRUE : FALSE ;
}

// Print the phone of a merchant
function wg_merchant_phone( $post_id = 0 ) {
	echo apply_filters( 'wg_merchant_phone', wg_get_merchant_phone( $post_id ) );
}

// Get the merchant's website
function wg_get_merchant_website( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$merchant = WP_Groupbuy_Merchant::get_merchant_object( $post_id );
	if ( !$merchant ) {
		return apply_filters( 'wg_get_merchant_website', '' );
	}
	return apply_filters( 'wg_get_merchant_website', $merchant->get_website() );
}

// Does the merchant have a website url assigned
function wg_has_merchant_website( $post_id = 0 ) {
	return ( apply_filters( 'wg_has_merchant_website', wg_get_merchant_website( $post_id ) != '' ) ) ? TRUE : FALSE ;
}

// Print the website url of a merchant
function wg_merchant_website( $post_id = 0 ) {
	echo apply_filters( 'wg_merchant_website', wg_get_merchant_website( $post_id ) );
}

// Get the merchant's facebook url
function wg_get_merchant_facebook( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$merchant = WP_Groupbuy_Merchant::get_merchant_object( $post_id );
	return apply_filters( 'wg_get_merchant_facebook', $merchant->get_facebook() );
}

// Does the merchant have a facebook url assigned
function wg_has_merchant_facebook( $post_id = 0 ) {
	return ( apply_filters( 'wg_has_merchant_facebook', wg_get_merchant_facebook( $post_id ) != '' ) ) ? TRUE : FALSE ;
}

// Print the facebook url of a merchant
function wg_merchant_facebook( $post_id = 0 ) {
	echo apply_filters( 'wg_merchant_facebook', wg_get_merchant_facebook( $post_id ) );
}

// Get the merchant's twitter url
function wg_get_merchant_twitter( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$merchant = WP_Groupbuy_Merchant::get_merchant_object( $post_id );
	return apply_filters( 'wg_get_merchant_twitter', $merchant->get_twitter() );
}

// Does the merchant have a twitter url assigned
function wg_has_merchant_twitter( $post_id = 0 ) {
	return ( apply_filters( 'wg_has_merchant_twitter', wg_get_merchant_twitter( $post_id ) != '' ) ) ? TRUE : FALSE ;
}

// Print the twitter url of a merchant
function wg_merchant_twitter( $post_id = 0 ) {
	echo apply_filters( 'wg_merchant_twitter', wg_get_merchant_twitter( $post_id ) );
}

//  get merchant opening hours property
function wg_get_merchant_opening_hours( $post_id = 0 ) {
    if ( !$post_id ) {
        global $post;
        $post_id = $post->ID;
    }
    $merchant = WP_Groupbuy_Merchant::get_merchant_object( $post_id );
    return apply_filters( 'wg_get_merchant_opening_hours', $merchant->get_opening_hours() );
}

// Does the merchant have a twitter url assigned
function wg_has_merchant_opening_hours( $post_id = 0 ) {
    return ( apply_filters( 'wg_has_merchant_opening_hours', wg_get_merchant_opening_hours( $post_id ) != '' ) ) ? TRUE : FALSE ;
}


//	get merchant map width
function wg_merchant_map_width($post_id = 0 ) {
	if (!$post_id) {
		global $post;
		$post_id = $post->ID;
	}
	$merchant = WP_Groupbuy_Merchant::get_merchant_object($post_id);
	if (empty($merchant)) return;
	return apply_filters('wg_get_merchant_map_width', $merchant->get_map_width());
}

//	get merchant map height
function wg_merchant_map_height($post_id = 0 ) {
	if (!$post_id) {
		global $post;
		$post_id = $post->ID;
	}
	$merchant = WP_Groupbuy_Merchant::get_merchant_object($post_id);
	if (empty($merchant)) return;
	return apply_filters('wg_get_merchant_map_height', $merchant->get_map_height());
}

//	get merchant map zoom
function wg_merchant_map_zoom($post_id = 0 ) {
	if (!$post_id) {
		global $post;
		$post_id = $post->ID;
	}
	$merchant = WP_Groupbuy_Merchant::get_merchant_object($post_id);
	if (empty($merchant)) return;
	return apply_filters('wg_get_merchant_map_zoom', $merchant->get_map_zoom());
}

//	get merchant map type
function wg_merchant_map_type($post_id = 0 ) {
	if (!$post_id) {
		global $post;
		$post_id = $post->ID;
	}
	$merchant = WP_Groupbuy_Merchant::get_merchant_object($post_id);
	if (empty($merchant)) return;
	return apply_filters('wg_get_merchant_map_type', $merchant->get_map_type());
}

//	get merchant map show control option
function wg_merchant_map_show_info_window($post_id = 0 ) {
	if (!$post_id) {
		global $post;
		$post_id = $post->ID;
	}
	$merchant = WP_Groupbuy_Merchant::get_merchant_object($post_id);
	if (empty($merchant)) return;
	return apply_filters('wg_get_merchant_map_show_info_window', $merchant->get_map_info_window());
}

//	get google map
function wg_get_merchant_gmap() {

	$map = do_shortcode('[map w="'. wg_merchant_map_width() .'" h="'. wg_merchant_map_height() .'" style="standard" z="'. wg_merchant_map_zoom() .'" marker="yes" infowindow="'. wg_get_merchant_name() .'" infowindowdefault="'. wg_merchant_map_show_info_window() .'" maptype="'. wg_merchant_map_type() .'" hidecontrols="false" address="'. wg_get_merchant_street() .'"]');
	return $map;
}


