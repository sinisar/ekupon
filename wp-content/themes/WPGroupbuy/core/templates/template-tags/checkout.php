<?php

// checkout page.
function wg_on_checkout_page() {
	if ( get_query_var( WP_Groupbuy_Checkouts::CHECKOUT_QUERY_VAR ) ) {
		return TRUE;
	}
	return FALSE;
}

// Get the current checkout page
function wg_get_current_checkout_page( ) {
	$checkout = WP_Groupbuy_Checkouts::get_instance();
	return $checkout->get_current_page();
}

// Get the current checkout page
function wg_current_checkout_page() {
	echo apply_filters( 'wg_current_checkout_page', wg_get_current_checkout_page() );
}

// Print checkout URL
function wg_checkout_url() {
	echo apply_filters( 'wg_checkout_url', wg_get_checkout_url() );
}

// Get checkout URL
function wg_get_checkout_url() {
	return apply_filters( 'wg_get_checkout_url', WP_Groupbuy_Checkouts::get_url() );
}

// Ability to add an item directly to cart and send user to checkout bypassing the cart page.
function wg_get_add_to_checkout_url( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	return apply_filters( 'wg_get_add_to_checkout_url', WP_Groupbuy_Carts::add_to_cart_url( $post_id, trailingslashit( get_option( WP_Groupbuy_Checkouts::CHECKOUT_PATH_OPTION, 'checkout' ) ) ) );
}

// Print url to add an item directly to cart and send user to checkout bypassing the cart page.
function wg_add_to_checkout_url( $post_id = 0 ) {
	echo apply_filters( 'wg_add_to_checkout_url', wg_get_add_to_checkout_url( $post_id ) );
}

// Get offline guide
function wg_get_offsite_purchasing_guide() {
 return get_option(WP_Groupbuy_Offsite_Manual_Purchasing::GUIDE_OPTION);
}