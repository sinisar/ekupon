<?php

// Get the affiliate credit option, credits received if a deal is successfully shared.
function wg_get_affiliate_credit() {
	return apply_filters( 'wg_get_affiliate_credit', get_option( WP_Groupbuy_Affiliates::AFFILIATE_CREDIT_OPTION, '0' ) );
}

// Print the affiliate credit option, credits received if a deal is successfully shared.
function wg_affiliate_credit() {
	echo apply_filters( 'wg_affiliate_credit', wg_get_affiliate_credit() );
}

// Return share link for affiliates
function wg_get_share_link( $deal_id = NULL, $member_login = NULL, $directlink = false ) {
	if ( NULL === $deal_id ) {
		global $post;
		$deal_id = $post->ID;
	}
	if ( !$deal_id ) {
		return '';
	}
	$link = WP_Groupbuy_Affiliates::get_share_link( $deal_id, $member_login, $directlink );
	return apply_filters( 'wg_get_share_link', $link, $deal_id, $member_login, $directlink );
}

// Echo share link for affiliates
function wg_share_link( $deal_id = NULL, $member_login = NULL, $directlink = false ) {
	echo apply_filters( 'wg_share_link', wg_get_share_link( $deal_id, $member_login, $directlink ), $deal_id, $member_login, $directlink );
}

// Is the bitly integration active
function wg_is_bitly_active() {
	return WP_Groupbuy_Affiliates::is_bitly_active();
}

// Get a bit.ly shortened URL
function wg_get_short_url( $url ) {
	$short_url = WP_Groupbuy_Affiliates::get_short_url( $url );
	return apply_filters( 'wg_get_short_url', $short_url, $url );
}

// Return share stats based on user and deal
function wg_get_share_stats( $deal_id = NULL, $member_login = NULL ) {
	$link = WP_Groupbuy_Affiliates::get_share_link( $deal_id, $member_login );
	return apply_filters( 'wg_get_share_stats', wg_get_share_stats_by_short_url( $link ), $deal_id, $member_login );
}

// Returns share stats for a shortened url
function wg_get_share_stats_by_short_url( $short_url = NULL ) {
	if ( NULL === $short_url ) {
		$short_url = WP_Groupbuy_Affiliates::get_share_link();
	}
	$stats = WP_Groupbuy_Affiliates::get_bitly_short_url_stats( $short_url );
	return apply_filters( 'wg_get_share_stats_by_short_url', $stats, $short_url );
}

// Get the clicks for a shared link
function wg_get_share_clicks( $deal_id = NULL, $member_login = NULL ) {
	$link = WP_Groupbuy_Affiliates::get_share_link( $deal_id, $member_login );
	return apply_filters( 'wg_get_share_clicks', wg_get_share_clicks_by_short_url( $link ), $deal_id, $member_login );
}

// Get the clicks for a shortened link
function wg_get_share_clicks_by_short_url( $short_url = NULL ) {
	if ( NULL === $short_url ) {
		$short_url = WP_Groupbuy_Affiliates::get_share_link();
	}
	$clicks = WP_Groupbuy_Affiliates::get_bitly_short_url_clicks( $short_url );
	return apply_filters( 'wg_get_share_clicks_by_short_url', $clicks, $short_url );
}
