<?php
// Constants
// Deal post type
function wg_get_deal_post_type() {
	return WP_Groupbuy_Deal::POST_TYPE;
}
// Location taxonomy name
function wg_get_deal_location_tax() {
	return WP_Groupbuy_Deal::LOCATION_TAXONOMY;
}
// Deal category taxonomy name
function wg_get_deal_cat_slug() {
	return WP_Groupbuy_Deal::CAT_TAXONOMY;
}
// Deal taxonomy name
function wg_get_deal_tag_slug() {
	return WP_Groupbuy_Deal::TAG_TAXONOMY;
}
// URLS
function wg_get_deal_edit_url( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$url = WP_Groupbuy_Deals_Edit::get_url( $post_id );
	return apply_filters( 'wg_get_deal_edit_url', $url );
}
function wg_deal_edit_url( $post_id = 0 ) {
	echo apply_filters( 'wg_deal_edit_url', wg_get_deal_edit_url( $post_id ) );
}
function wg_get_deal_submission_url() {
	$url = WP_Groupbuy_Deals_Submit::get_url();
	return apply_filters( 'wg_get_deal_submission_url', $url );
}
function wg_deal_submission_url() {
	echo apply_filters( 'wg_deal_submission_url', wg_get_deal_submission_url() );
}
// Query for the most recent deal based on location. Cache for two minutes for performance.
function wg_get_latest_deal_link( $location = null, $return_id = false ) {
	
	// get preferred location if it's set from the premium theme
	if ( function_exists( 'wg_get_preferred_location' ) && null == $location ) {
		if ( term_exists( wg_get_preferred_location() ) ) {
			$location = wg_get_preferred_location();
		}
	}
	$latest_deal_id_cache = get_transient( 'wg_latest_deal_id_'.$location );
	if ( !$latest_deal_id_cache ) {
		$args=array(
		'post_type' => wg_get_deal_post_type(),
		'post_status' => 'publish',
		'showposts' => 1,
		'meta_query' => array(
		array(
		'key' => '_expiration_date',
		'value' => array( 0, current_time( 'timestamp' ) ),
		'compare' => 'NOT BETWEEN'
		)
		)
		);
		if ( $location != '' ) {
			$args = array_merge( array( wg_get_deal_location_tax() => $location ), $args );
		}
		$latest_deal = get_posts( $args );
		if ( !empty( $latest_deal ) ) {
			foreach ( $latest_deal as $post ) :
			set_transient( 'wg_latest_deal_id_'.$location, $post->ID, 60*2 );
			if ( $return_id ) {
				return $post->ID;
			}
			$link = get_permalink( $post->ID );
			endforeach;
		}
	} else {
		if ( $return_id ) {
			return $latest_deal_id_cache;
		}
		$link = get_permalink( $latest_deal_id_cache );
	}
	if ( empty( $link ) ) { // fallback to a location loop
		$link = wg_get_deals_link( $location );
	}
	return apply_filters( 'get_wpg_latest_deal_link', $link, $location, $return_id );
}
// Get the link to a deals archive, preferably one based on location.
function wg_get_deals_link( $location = null ) {
	if ( null != $location ) {
		$link = get_term_link( $location, wg_get_location_tax_slug() );
		if ( $link == $location ) {
			$link = get_post_type_archive_link( wg_get_deal_post_type() );
		}
	} else {
		$link = get_post_type_archive_link( wg_get_deal_post_type() );
	}
	return apply_filters( 'wg_get_deals_link', $link, $location );
}
// URLS / Deal Previews
// preview of this deal available if it's currently a draft
function wg_deal_preview_available( $post_id = null ) {
	if ( null === $post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	return apply_filters( 'wg_deal_preview_available', WP_Groupbuy_Deals_Preview::has_key( $deal ) );
}
// Get the preview url to a deal
function wg_get_deal_preview_link( $post_id = null ) {
	if ( null === $post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	if ( wg_deal_preview_available( $post_id ) ) {
		$deal = WP_Groupbuy_Deal::get_instance( $post_id );
		return apply_filters( 'wg_get_deal_preview_link', WP_Groupbuy_Deals_Preview::get_preview_link( $deal ) );
	}
	return;
}
// Print the preview url for the current deal
function wg_deal_preview_link( $post_id = null ) {
	echo apply_filters( 'wg_deal_preview_link', wg_get_deal_preview_link( $post_id ) );
}
// Utility
// Get array of accounts that have purchased a specific deal
function wg_get_deal_purchasers( $post_id = 0, $user_ids = FALSE  ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$ids = array();
	$purchase_ids = WP_Groupbuy_Purchase::get_purchases( array( 'deal' => $post_id ) );
	foreach ( $purchase_ids as $purchase_id ) {
		$purchase = WP_Groupbuy_Purchase::get_instance( $purchase_id );
		if ( $user_ids ) {
			$id = $purchase->get_user();
		}
		else {
			$id = $purchase->get_account_id();
		}
		// Build array
		if ( !in_array( $id, $ids ) ) {
			$ids[] = $id;
		}
	}
	return apply_filters( 'wg_get_deal_purchasers', $ids, $post_id, $user_id );
}
// Get a list of successful Purchases by a given account
function wg_get_purchases_by_account( $post_id = 0, $user_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	if ( !$user_id ) {
		$user_id = get_current_user_id();
	}
	$account = WP_Groupbuy_Account::get_instance( $user_id );
	$deal = WP_Groupbuy_Deal::get_instance( $post_id  );
	return apply_filters( 'wg_get_purchase_by_account', $deal->get_purchases_by_account( $account->get_id() ) );
}
// Get the last viewed deal or return to an appropriate deal instead.
function wg_get_last_viewed_redirect_url( $last_viewed = null ) {
	if ( null == $last_viewed ) {
		$last_viewed = wg_get_cookie( 'last-deal-viewed' );
	}
	if ( isset( $_GET['redirect_to'] ) ) {
		$url = $_GET['redirect_to'];
	} elseif ( !empty( $last_viewed ) ) {
		$post_type = get_post_type_object( wg_get_deal_post_type() );
		$url = site_url( trailingslashit( $post_type->rewrite['slug'] ) . $last_viewed );;
	} elseif ( isset( $_POST['deal_redirect'] ) ) {
		$post_type = get_post_type_object( wg_get_deal_post_type() );
		$url = site_url( trailingslashit( $post_type->rewrite['slug'] ) . $_POST['deal_redirect'] );
	} else {
		$url = wg_get_latest_deal_link();
	}
	return apply_filters( 'wg_get_last_viewed_redirect_url', $url );
}
// The current deal complete
function wg_is_deal_complete( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	return apply_filters( 'wg_get_deal_savings', $deal->is_closed() );
}
// Get the status of a deal
function wg_get_status( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	return apply_filters( 'wg_get_status', $deal->get_status() );
}
// Has this user purchased this deal already
function wg_has_purchased( $post_id = 0, $user_id = 0 ) {
	$bool = FALSE;
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	if ( !$user_id ) {
		$user_id = get_current_user_id();
	}
	$purchases = wg_get_purchases_by_account( $post_id, $user_id );
	if ( is_array( $purchases ) && !empty( $purchases )  ) {
		$bool = TRUE;
	}
	return apply_filters( 'wg_has_purchased', $bool, $post_id, $user_id );
}
// Has this deal reached it's purchase tipping point
function wg_has_deal_tipped( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$bool = FALSE;
	if ( !wg_has_purchase_min( $post_id ) ) {
		$bool = TRUE;
	}
	$purchases = wg_get_number_of_purchases( $post_id );
	$purchase_min = wg_get_min_purchases( $post_id );
	if ( $purchases >= $purchase_min ) {
		$bool = TRUE;
	}
	return apply_filters( 'wg_has_deal_tipped', $bool, $post_id );
}
// The current availability for the deal. Based on tipping point and expiration.
function wg_deal_availability( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	if ( !is_a( $deal, 'WP_Groupbuy_Deal' ) ) {
		return FALSE;
	}
	return apply_filters( 'wg_get_status', $deal->is_open() && wg_can_purchase() > 0 );
}
// Is the deal sold out, i.e. reached its purchase limit.
function wg_is_sold_out( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	return apply_filters( 'wg_is_sold_out', $deal->is_sold_out() );
}
// Return the total number of purchases for the deal
function wg_get_number_of_purchases( $post_id = 0, $recalculate = false ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	return apply_filters( 'wg_get_number_of_purchases', $deal->get_number_of_purchases( $recalculate ) );
}
// Return the total number of purchases for the deal
function wg_number_of_purchases( $post_id = 0, $recalculate = false ) {
	echo apply_filters( 'wg_number_of_purchases', wg_get_number_of_purchases( $post_id, $recalculate ) );
}
// Return the merchant for a deal
function wg_get_merchant( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	return apply_filters( 'wg_get_merchant', $deal->get_merchant() );
}
// Is the this deal todays deal.
function wg_is_latest_deal_page( $todays_deal_id = 0, $post_id = 0 ) {
	$location = wg_get_preferred_location();
	if ( !$todays_deal_id ) {
		$latest_deal_id_cache = get_transient( 'wg_latest_deal_id_'.$location );
		if ( !$latest_deal_id_cache ) {
			$latest_deal_id_cache = wg_get_latest_deal_link( $location, true );
		}
		$todays_deal_id = $latest_deal_id_cache;
	}
	if ( !$todays_deal_id )
	return;
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	if ( $post_id == $todays_deal_id ) {
		return TRUE;
	}
	return FALSE;
}
// Views
function wg_deal_submit_form() {
	$deal_sub = WP_Groupbuy_Deals_Submit::get_instance();
	if ( $deal_sub ) {
		echo $deal_sub->get_form();
	} else {
		return FALSE;
	}
}
// Print a countdown timer
function wg_deal_countdown( $compact = false, $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	if ( !wg_is_deal_complete( $post_id ) ) {
		$end_date = wg_get_deal_end_date( "F j\, Y H:i:s", $post_id );
		$out = '<script type="text/javascript">';
		$out .= 'jQuery(function () {';
		$out .= "var date = new Date ('".$end_date."' );";
		$out .= "jQuery('.countdown-timer-".$post_id."').countdown( { ";
		// countdown args
		$args = "until: date, timezone: ".get_option( 'gmt_offset' );
		if ( $compact ) { $args .= ", compact: true, format: 'DHMS', description: ''"; }
		$out .= apply_filters( 'wg_deal_countdown_js_args', $args );
		$out .= ' } );';
		$out .= ' });';
		$out .= '</script>';
		$out .= '<div class="countdown_timer_wrap countdown_timer_wrap-'.$post_id.'"><span class="countdown_timer_label">'.wpg__( 'Expires in: ' ).'</span><span class="countdown-timer countdown-timer-'.$post_id.'"></span></div>';
	} else {
		$out = "<div id='countdown-timer' class='expired'><span>".wpg__( 'Deal is Complete' )."</span></div>";
	}
	echo apply_filters( 'wg_deal_countdown', $out, $compact, $post_id );
}
// Data / Expiration
// Does the current deal have an expiration
function wg_has_expiration( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	return apply_filters( 'wg_has_expiration', TRUE != $deal->never_expires() );
}
// Get the deals expiration date
function wg_get_deal_end_date( $format = 'F j\, Y H:i:s', $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$date = date( $format , wg_get_expiration_date( $post_id ) );
	return apply_filters( 'wg_deal_end_date', $date );
}
// Get UNIX timestamp of deal expiration
function wg_get_expiration_date( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	return apply_filters( 'wg_get_expiration_date', $deal->get_expiration_date() );
}
// Print UNIX timestamp of deal expiration
function wg_expiration_date( $post_id = 0 ) {
	echo apply_filters( 'wg_expiration_date', wg_get_expiration_date( $post_id ) );
}
// Get the time left for a deal.
function wg_get_days_left( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	if ( wg_deal_availability() ) {
		$end_date = wg_get_expiration_date( $post_id );
		$difference = $end_date - current_time( 'timestamp' );
		if ( floor( $difference/60/60/24 ) >= 1 ) {
			if ( floor( $difference/60/60/24 ) == 1 ) {
				$remain = ( floor( $difference/60/60/24 ) ) . wpg__( ' day' );
			} else {
				$remain = ( floor( $difference/60/60/24 ) ) . wpg__( ' days' );
			}
		} elseif ( floor( $difference/60/60 ) >= 1 ) {
			if ( floor( $difference/60/60 ) == 1 ) {
				$remain = ( floor( $difference/60/60 ) ) . wpg__( ' hour' );
			} else {
				$remain = ( floor( $difference/60/60 ) ) . wpg__( ' hours' );
			}
		} else {
			if ( floor( $difference/60 ) == 1 ) {
				$remain = ( floor( $difference/60 ) ) . wpg__( ' minute' );
			} else {
				$remain = ( floor( $difference/60 ) ) . wpg__( ' minutes' );
			}
		}
	} else {
		$remain = wpg__( 'No time' );
	}
	return apply_filters( 'wg_get_days_left', $remain );
}
// Print the time left for a deal.
function wg_days_left( $post_id = 0 ) {
	echo apply_filters( 'wg_days_left', wg_get_days_left( $post_id ) );
}
//  get coupon value for deal
function wg_get_coupon_value( $post_id = 0, $formatted = false ) {
    if ( !$post_id ) {
        global $post;
        $post_id = $post->ID;
    }
    $deal = WP_Groupbuy_Deal::get_instance( $post_id );
    $get_coupon_value = $deal->get_coupon_value();
    //print "coupon_value: "; print_r($get_coupon_value);
    $price = ( $formatted ) ? wg_get_formatted_money( $get_coupon_value[0] ) : $get_coupon_value[0] ;
    return apply_filters( 'wg_get_coupon_value', $price, $formatted );
}

function wg_get_provision_value( $post_id = 0, $formatted = false ) {
    if ( !$post_id ) {
        global $post;
        $post_id = $post->ID;
    }
    $deal = WP_Groupbuy_Deal::get_instance( $post_id );
    $provision_value = $deal->get_provision_value();
    return apply_filters( 'wg_get_provision_value', $provision_value, $formatted);
}

//  get deal description:
function wg_get_deal_description($post_id = 0, $formatted = false ) {
    if ( !$post_id ) {
        global $post;
        $post_id = $post->ID;
    }
    $deal = WP_Groupbuy_Deal::get_instance( $post_id );
    $deal_description = $deal->get_additional_title_description();
    return $deal_description;
}


// Get the price of the current deal
function wg_get_price( $post_id = 0, $formatted = false ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	$get_price = $deal->get_price();
	$price = ( $formatted ) ? wg_get_formatted_money( $get_price ) : $get_price ;
	return apply_filters( 'wg_get_price', $price, $formatted );
}
// Data / Price
// Echo the price of the current deal
function wg_price( $post_id = 0, $formatted = true ) {
	echo apply_filters( 'wg_price', wg_get_price( $post_id, $formatted ) );
}

//  Coupon value
function wg_coupon_value( $post_id = 0, $formatted = true ) {
    echo apply_filters( 'wg_coupon_value', wg_get_coupon_value( $post_id, $formatted ) );
}

//  Coupon provision value
function wg_provision_value( $post_id = 0, $formatted = true ) {
    echo apply_filters( 'wg_provision_value', wg_get_provision_value( $post_id, $formatted ) );
}

// Does the current deal have dynamic pricing set?
function wg_has_dynamic_price( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	return apply_filters( 'wg_has_dynamic_price', $deal->has_dynamic_price() );
}
// Get the deal dynamic prices
function wg_get_dynamic_prices( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	$dynamic_prices = $deal->get_dynamic_price();
	return apply_filters( 'wg_get_dynamic_prices', $dynamic_prices );
}
// Display a ul list of dynamic prices for the current deal.
function wg_dynamic_prices( $post_id = 0, $show_all = false ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$dynamic_prices = wg_get_dynamic_prices( $post_id );
	$current_price = wg_get_price( $post_id );
	if ( empty( $dynamic_prices ) ) return;
	if ( !$show_all ) {
		foreach ( $dynamic_prices as $limit => $price ) {
			if ( wg_get_number_of_purchases( $post_id ) > $limit ) {
				if ( $current_price != $price ) { // don't take current
					unset( $dynamic_prices[$limit] );
				}
			}
		}
	}
	if ( empty( $dynamic_prices ) ) return;
	$width = 100/count( $dynamic_prices );
	$i = 1;
	foreach ( $dynamic_prices as $limit => $price ) {
		$next = $limit - wg_get_number_of_purchases( $post_id );
		$out .= ' <b>' . str_replace( '.00', '', wg_get_formatted_money( $price ) ) . '</b> ' . sprintf( wpg__( ' after %s /' ), $limit );
		$i++;
	}
	echo apply_filters( 'wg_dynamic_prices', $out, $dynamic_prices, $show_all );
}
// Return the min purchases required for the price to change to the next dynamic price.
function wg_next_dynamic_price_min( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$dynamic_prices = wg_get_dynamic_prices( $post_id );
	if ( empty( $dynamic_prices ) ) return;
	if ( !$show_all ) {
		foreach ( $dynamic_prices as $limit => $price ) {
			if ( wg_get_number_of_purchases( $post_id ) > $limit ) {
				unset( $dynamic_prices[$limit] );
			}
		}
	}
	reset( $dynamic_prices );
	return apply_filters( 'wg_next_dynamic_price_min', key( $dynamic_prices ) );
}
// Return the next price if the minimum purchases are made for the dynamic price.
function wg_next_dynamic_price( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$dynamic_prices = wg_get_dynamic_prices( $post_id );
	if ( empty( $dynamic_prices ) ) return;
	if ( !$show_all ) {
		foreach ( $dynamic_prices as $limit => $price ) {
			if ( wg_get_number_of_purchases( $post_id ) > $limit ) {
				unset( $dynamic_prices[$limit] );
			}
		}
	}
	return apply_filters( 'wg_next_dynamic_price', array_shift( $dynamic_prices ) );
}
// Data / Misc
// Get deal worth
function wg_get_deal_worth( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	return apply_filters( 'wg_get_deal_worth', $deal->get_price() );
}
// Print the current deals worth
function wg_deal_worth( $post_id = 0 ) {
	echo apply_filters( 'wg_deal_worth', wg_get_deal_worth( $post_id ) );
}
// Get the savings for the current deal
function wg_get_deal_savings( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	$savings = $deal->get_amount_saved();
	if ( empty( $savings ) || $savings = '' ) {
		$original_price = str_replace( '$', '', wg_get_deal_worth( $post_id ) );
		if ( empty( $original_price ) ) {
			return apply_filters( 'wg_get_deal_savings', '' );
		}
		$price = wg_get_price( $post_id );
		$savings = wg_get_formatted_money( $original_price - $price );
	}
	return apply_filters( 'wg_get_deal_savings', $savings  );
}
// Print the savings for the current deal
function wg_deal_savings( $post_id = 0 ) {
	echo apply_filters( 'wg_deal_savings', wg_get_deal_savings( $post_id ) );
}
// Calculated savings based on deal price and deal worth; defaults to deal savings if exists
function wg_get_amount_saved( $post_id = 0, $price = null ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	if ( null === $price ) {
		$price = wg_get_price( $post_id );
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	$savings = $deal->get_amount_saved();
	//print "savings:".$savings;
	if ( !empty( $savings ) || $savings != '' ) {
		return apply_filters( 'wg_get_amount_saved', $savings );
	}
	/*
	$original_price = str_replace( '$', '', wg_get_deal_worth( $post_id ) );
	if ( empty( $original_price ) ) return 0;
	$savings = $original_price - $price;
	$savings = number_format( ( $savings/$original_price )*100, 0 );
	return apply_filters( 'wg_get_amount_saved', $savings.'%' );
	*/
}
function wg_show_percentage($deal_amount_saved) {
    $pos = strpos(strval($deal_amount_saved), '\%');
    if ($pos === false && $deal_amount_saved != "") {
        return strval($deal_amount_saved).'%';
    }
    else {
        return strval($deal_amount_saved);
    }
}

// Calculated savings based on deal price and deal worth; defaults to deal savings if exists
function wg_amount_saved( $post_id = 0 ) {
    if(wg_get_deal_show_amount_saved($post_id)) {
        echo apply_filters('wg_amount_saved', "<span class='deal-savings'>" . wg_show_percentage(wg_get_amount_saved($post_id)*(-1)) . "</span>");
    }
    echo apply_filters('wg_amount_saved', "");
}

//	Return amount saved percentage value only
function wg_amount_percentage_saved( $post_id = 0 ) {
	$amount_saved_exploded = explode("%", wg_get_amount_saved( $post_id ));
	if(count($amount_saved_exploded) > 1) {
		return $amount_saved_exploded[0]/100;
	}
}
//	return calculated price with savings
function wg_get_merchant_price($post_id = 0) {
	return number_format(wg_get_deal_worth() - (wg_get_deal_worth() * wg_amount_percentage_saved()), 2);
}

// Get deal highlights
function wg_get_highlights( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	return apply_filters( 'wg_get_highlights', $deal->get_highlights() );
}
// Print the current deal highlights
function wg_highlights( $post_id = 0 ) {
	echo apply_filters( 'wg_highlights', wg_get_highlights( $post_id ) );
}
// Get deal fine print
function wg_get_fine_print( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	return apply_filters( 'wg_get_fine_print', $deal->get_fine_print() );
}
// Get deal fine print
function wg_fine_print( $post_id = 0 ) {
	echo apply_filters( 'wg_fine_print', wg_get_fine_print( $post_id ) );
}
// Get deal rss excerpt
function wg_get_rss_excerpt( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	return apply_filters( 'wg_get_rss_excerpt', $deal->get_rss_excerpt() );
}
// Get the current deals rss excerpt
function wg_rss_excerpt( $post_id = 0 ) {
	echo apply_filters( 'wg_rss_excerpt', wg_get_rss_excerpt( $post_id ) );
}
// Get deal map
function wg_get_map( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	return apply_filters( 'wg_get_map', $deal->get_voucher_map() );
}
// Print the current deals map
function wg_map( $post_id = 0 ) {
	echo apply_filters( 'wg_map', wg_get_map( $post_id ) );
}
// Does the current deal have a map
function wg_has_map( $post_id = 0 ) {
	$map = wg_get_map( $post_id );
	$has = ( $map == '' ) ? FALSE : TRUE ;
	return apply_filters( 'wg_has_map', $has );
}
// Get the shipping rate for a deal
function wg_get_shipping( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	return apply_filters( 'wg_get_shipping', $deal->get_shipping() );
}
// Does the current deal have shipping
function wg_has_shipping( $post_id = 0 ) {
	$shipping = wg_get_shipping( $post_id );
	$has = ( $shipping = '' || $shipping < '0.01' ) ? FALSE : TRUE ;
	return apply_filters( 'wg_has_shipping', $has );
}
// Print the shipping rate for the current deal
function wg_shipping( $post_id = 0 ) {
	echo apply_filters( 'wg_shipping', wg_get_shipping( $post_id ) );
}
// Get the tax for a deal
function wg_get_tax( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	return apply_filters( 'wg_get_tax', $deal->get_tax() );
}
// Does the current deal have have tax
function wg_has_tax( $post_id = 0 ) {
	$tax = wg_get_tax( $post_id );
	$has = ( $tax = '' || $tax < '0.01' ) ? FALSE : TRUE ;
	return apply_filters( 'wg_has_tax', $has );
}
// Print the current deals tax
function wg_tax( $post_id = 0 ) {
	echo apply_filters( 'wg_tax', wg_get_tax( $post_id ) );
}
// Get the deals how to use
function wg_get_how_to_use( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	return apply_filters( 'wg_get_how_to_use', $deal->get_voucher_how_to_use() );
}
// Print the current deals how to use
function wg_how_to_use( $post_id = 0 ) {
	echo apply_filters( 'wg_how_to_use', wg_get_how_to_use( $post_id ) );
}
// Data / Voucher
// Get deal voucher locations
function wg_get_deal_voucher_locations( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	return array_filter( apply_filters( 'wg_get_deal_locations', $deal->get_voucher_locations() ) );
}
// Print an unordered list of locations for the voucher
function wg_deal_voucher_locations( $post_id = 0 ) {
	$locations = wg_get_deal_voucher_locations( $post_id );
	if ( !empty( $locations ) ) {
		$out = "<div class='pRight20'>";
		foreach ( $locations as $local ) {
			$out .= "<div id='viewline1' class='bg_adrview bg_adrviewa mBottom10'>".$local."</div>";
		}
		$out .= "</div>";
		echo apply_filters( 'wg_deal_voucher_locations', $out );
	}
}
// Get the deals voucher expiration
function wg_get_deal_voucher_exp_date( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	return apply_filters( 'wg_get_deal_voucher_exp_date', $deal->get_voucher_expiration_date() );
}
// Print the voucher expiration timestamp
function wg_deal_voucher_exp_date( $post_id = 0 ) {
	echo apply_filters( 'wg_deal_voucher_exp_date', wg_get_deal_voucher_exp_date( $post_id ) );
}
// Data / Purchase Limits
// Get minimum required purchases for a deal
function wg_get_min_purchases( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	return apply_filters( 'wg_get_min_purchases', $deal->get_min_purchases() );
}
// Does the current deal have a purchase minimum, i.e. tipping point
function wg_has_purchase_min( $post_id = 0 ) {
	$min = wg_get_min_purchases( $post_id );
	$has = ( $min = '' || $min < 1 ) ? FALSE : TRUE ;
	return apply_filters( 'wg_has_purchase_min', $has );
}
// Print minimum required purchases for the current deal
function wg_min_purchases( $post_id = 0 ) {
	echo apply_filters( 'wg_min_purchases', wg_get_min_purchases( $post_id ) );
}
// Get maximum purchases for a deal
function wg_get_max_purchases( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	return apply_filters( 'wg_get_get_max_purchases', $deal->get_max_purchases() );
}
// Does the current deal have a purchase limit
function wg_has_purchases_limit( $post_id = 0 ) {
	$max = wg_get_max_purchases( $post_id );
	$has = ( $max = '' || $max < 1 ) ? FALSE : TRUE ;
	return apply_filters( 'wg_has_purchases_limit', $has );
}
// Print maximum purchases for the current deal
function wg_max_purchases( $post_id = 0 ) {
	echo apply_filters( 'wg_max_purchases', wg_get_max_purchases( $post_id ) );
}
// Get remaining required purchases for a deal
function wg_get_remaining_required_purchases( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	return apply_filters( 'wg_get_remaining_required_purchases', $deal->get_remaining_required_purchases() );
}
// Print remaining required purchases for the current deal
function wg_remaining_required_purchases( $post_id = 0 ) {
	echo apply_filters( 'wg_remaining_required_purchases', wg_get_remaining_required_purchases( $post_id ) );
}
// Get maximum purchases allowed per user for a deal
function wg_get_max_purchases_per_user( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	return apply_filters( 'wg_get_max_purchases_per_user', $deal->get_max_purchases_per_user() );
}
// Print maximum purchases allowed per user for the current deal
function wg_max_purchases_per_user( $post_id = 0 ) {
	echo apply_filters( 'wg_max_purchases_per_user', wg_get_max_purchases_per_user( $post_id ) );
}
// Data / Taxonomy
// Locations a deal has assigned
function wg_get_deal_locations( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	return apply_filters( 'wg_get_deal_locations', wp_get_object_terms( $post_id, wg_get_deal_location_tax() ) );
}
// Print the locations the current deal is assigned.
function wg_deal_locations( $post_id = 0 ) {
	$locations = wg_get_deal_locations( $post_id );
	$out = '<ul class="location_url clearfix">';
	foreach ( $locations as $location ) {
		$out .= '<li><a href="'.get_term_link( $location->slug, wg_get_location_tax_slug() ).'">'.$location->name.'</a></li>';
	}
	$out .= '</ul>';
	echo apply_filters( 'wg_deal_locations', $out );
}
// Tags a deal has assigned
function wg_get_deal_tags( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	return apply_filters( 'wg_get_deal_tags', wp_get_object_terms( $post_id, wg_get_deal_tag_slug() ) );
}
// Print the tags the current deal is assigned.
function wg_deal_tags( $post_id = 0 ) {
	$cats = wg_get_deal_tags( $post_id );
	$out = '<ul class="deal_tags clearfix">';
	foreach ( $cats as $cat ) {
		$out .= '<li><a href="'.get_term_link( $cat->slug, wg_get_deal_tag_slug() ).'">'.$cat->name.'</a></li>';
	}
	$out .= '</ul>';
	echo apply_filters( 'wg_deal_tags', $out );
}
// Categories a deal has assigned
function wg_get_deal_categories( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	return apply_filters( 'wg_get_deal_categories', wp_get_object_terms( $post_id, wg_get_deal_cat_slug() ) );
}
//  Get deals show_amount_saved
function wg_get_deal_show_amount_saved( $post_id = 0 ) {
    if ( !$post_id ) {
        global $post;
        $post_id = $post->ID;
    }
    $deal = WP_Groupbuy_Deal::get_instance( $post_id );
    return apply_filters( 'wg_get_deal_show_amount_saved', $deal->get_show_amount_saved() );
}

// Print the categories the current deal is assigned.
function wg_deal_categories( $post_id = 0 ) {
	$cats = wg_get_deal_categories( $post_id );
	$out = '<ul class="deal_categories clearfix">';
	foreach ( $cats as $cat ) {
		$out .= '<li><a href="'.get_term_link( $cat->slug, wg_get_deal_cat_slug() ).'">'.$cat->name.'</a></li>';
	}
	$out .= '</ul>';
	echo apply_filters( 'wg_deal_categories', $out );
}
// Categories, Tags and Locations
// Get the current merchant type being viewed
function wg_get_current_deal_category( $slug = false ) {
	$taxonomy = get_query_var( 'taxonomy' );
	if ( $taxonomy == WP_Groupbuy_Deal::CAT_TAXONOMY ) {
		global $wp_query;
		if ( $slug ) {
			return apply_filters( 'wg_get_current_deal_category', $wp_query->get_queried_object()->slug );
		} else {
			return apply_filters( 'wg_get_current_deal_category', $wp_query->get_queried_object()->name );
		}
	}
	return FALSE;
}
// Return the categories object
function wg_get_categories( $hide_empty = TRUE ) {
	return apply_filters( 'wg_get_categories', get_terms( array( wg_get_deal_cat_slug() ), array('hide_empty' => $hide_empty, 'fields' => 'all', 'orderby' => 'name', 'order' => 'ASC', 'exclude' => '',) ) );
}
// Return a list of categories with formatting options
function wg_list_categories( $format = 'ul' ) {
	$categories = wg_get_categories();
	if ( empty( $categories ) )
	return '';
	$tag = $format;
	$list = '';
	if ( $format == 'ul' || $format == 'ol' ) {
		$list .= "<".$format." class='categoryDeals2' style='padding:0 0 10px'>";
		$tag = 'li';
	}
	foreach ( $categories as $category ) {
		$link = get_term_link( $category->slug, wg_get_deal_cat_slug() );
		$active = ( $category->name == wg_get_current_location() ) ? 'current_item' : 'item';
		$list .= "<".$tag." id='category_slug_".$category->slug."_new' class='category-item ".$active."'>";
		$list .= "<a href='".apply_filters( 'wg_list_category_link', $link )."' title='".sprintf( wpg__( 'Visit %s Deals' ), $category->name )."' id='category_slug_".$category->slug."'>".$category->name."</a>";
		$list .= "</".$tag.">";
	}
	if ( $format == 'ul' || $format == 'ol' )
	$list .= "</".$format.">";
	echo apply_filters( 'wg_list_categories', $list, $format );
}
// Return the tags object
function wg_get_tags( $hide_empty = TRUE ) {
	return apply_filters( 'wg_get_tags', get_terms( array( wg_get_deal_tag_slug() ), array( 'hide_empty' => $hide_empty, 'fields' => 'all' ) ) );
}
// Return a list of tags with formatting options
function wg_list_tags( $format = 'ul' ) {
	$tags = wg_get_tags();
	if ( empty( $tags ) )
	return '';
	$tag = $format;
	$list = '';
	if ( $format == 'ul' || $format == 'ol' ) {
		$list .= "<".$format." class='tags-ul clearfix'>";
		$html_tag = 'li';
	}
	foreach ( $tags as $tag ) {
		$link = get_term_link( $tag->slug, wg_get_deal_tag_slug() );
		$active = ( $tag->name == wg_get_current_location() ) ? 'current_item' : 'item';
		$list .= "<".$html_tag." id='tag_slug_".$tag->slug."' class='tag-item ".$active."'>";
		$list .= "<a href='".apply_filters( 'wg_list_tag_link', $link )."' title='".sprintf( wpg__( 'Visit %s Deals' ), $tag->name )."' id=tag_slug_".$tag->slug."'>".$tag->name."</a>";
		$list .= "</".$html_tag.">";
	}
	if ( $format == 'ul' || $format == 'ol' )
	$list .= "</".$format.">";
	echo apply_filters( 'wg_list_tags', $list, $format );
}

//	Custom options:

function get_array_as_list($custom_options_array, $format = 'ul') {
	if($custom_options_array == '') {
		return null;
	}

	$list = '';
	if ( $format == 'ul' || $format == 'ol' ) {
		$list .= "<".$format." class='tags-ul clearfix'>";
		$html_tag = 'li';
	}

	foreach($custom_options_array as $custom_option) {
			$list .= "<".$html_tag." id='tag_slug_' class='tag-item'>";
			$list .= $custom_option;
		$list .= "</".$html_tag.">";
	}
	if ( $format == 'ul' || $format == 'ol' )
		$list .= "</".$format.">";
	return $list;
}

// Get custom options payment type for a deal
function wg_get_custom_options_payment_type( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	$custom_options_payment_types = $deal->get_custom_options_payment_type();
	return apply_filters( 'wg_get_custom_options_payment_type', get_array_as_list( $custom_options_payment_types) );
}
// Does the current deal have checked payment types
function wg_has_custom_options_payment_type( $post_id = 0 ) {
	return ( apply_filters( 'wg_has_custom_options_payment_type', wg_get_custom_options_payment_type( $post_id ) != '' )) ? TRUE : FALSE ;
}
// Print payment type for the current deal
function wg_custom_options_payment_type( $post_id = 0 ) {
	echo apply_filters( 'wg_custom_options_payment_type', wg_get_custom_options_payment_type( $post_id ) );
}

// Get custom options terms for a deal
function wg_get_custom_options_terms( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	return apply_filters( 'wg_get_custom_options_terms', $deal->get_custom_options_terms() );
}
// Does the current deal have custom options term text info
function wg_has_custom_options_terms( $post_id = 0 ) {
	return (apply_filters( 'wg_has_custom_options_terms', wg_get_custom_options_terms( $post_id ) != '' )) ? TRUE : FALSE ;

}
// Print custom option terms for current deal
function wg_custom_options_terms( $post_id = 0 ) {
	echo apply_filters( 'wg_custom_options_terms', wg_get_custom_options_terms( $post_id ) );
}

// Get custom options redeeem for a deal
function wg_get_custom_options_redeem( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	return apply_filters( 'wg_get_custom_options_redeem', $deal->get_custom_options_redeem() );
}
// Does the current deal have custom options redeeem text info
function wg_has_custom_options_redeem( $post_id = 0 ) {
	return (apply_filters( 'wg_has_custom_options_redeem', wg_get_custom_options_redeem( $post_id ) != '' )) ? TRUE : FALSE ;

}
// Print custom options redeeem for the current deal
function wg_custom_options_redeem( $post_id = 0 ) {
	echo apply_filters( 'wg_custom_options_redeem', wg_get_custom_options_redeem( $post_id ) );
}

// Get custom options other terms for a deal
function wg_get_custom_options_other_terms( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	return apply_filters( 'wg_get_custom_options_other_terms', $deal->get_custom_options_other_terms() );
}
// Does the current deal have custom options other terms text info
function wg_has_custom_options_other_terms( $post_id = 0 ) {
	return (apply_filters( 'wg_has_custom_options_other_terms', wg_get_custom_options_other_terms( $post_id ) != '' )) ? TRUE : FALSE ;

}
// Print custom options other terms for the current deal
function wg_custom_options_other_terms( $post_id = 0 ) {
	echo apply_filters( 'wg_custom_options_other_terms', wg_get_custom_options_other_terms( $post_id ) );
}

//	Get custom options help info for current deal
function wg_get_custom_options_need_help( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	return apply_filters( 'wg_get_custom_options_need_help', $deal->get_custom_options_need_help() );
}
// Does the current deal have a purchase minimum, i.e. tipping point
function wg_has_custom_options_need_help( $post_id = 0 ) {
	return (apply_filters( 'wg_has_custom_options_need_help', wg_get_custom_options_need_help( $post_id ) != '' )) ? TRUE : FALSE ;
}
// Print minimum required purchases for the current deal
function wg_custom_options_need_help( $post_id = 0 ) {
	echo apply_filters( 'wg_custom_options_need_help', wg_get_custom_options_need_help( $post_id ) );
}

//	Get custom options voucher extra text for current deal
function wg_get_custom_options_voucher_extra_text( $post_id = 0 ) {
    if ( !$post_id ) {
        global $post;
        $post_id = $post->ID;
    }
    $deal = WP_Groupbuy_Deal::get_instance( $post_id );
    return apply_filters( 'wg_get_custom_options_voucher_extra_text', $deal->get_custom_options_voucher_extra_text() );
}
// Does the current deal have voucher extra text
function wg_has_custom_options_voucher_extra_text( $post_id = 0 ) {
    return (apply_filters( 'wg_has_custom_options_voucher_extra_text', wg_get_custom_options_voucher_extra_text( $post_id ) != '' )) ? TRUE : FALSE ;
}

function exclude_current_deal($deal_ids) {
	$cleaned_deal_ids = array();

	global $post;
	$current_deal_id = $post->ID;
	foreach($deal_ids as $deal_id) {
		if($deal_id != $current_deal_id) {
			$cleaned_deal_ids[] = $deal_id;
		}
	}
	return $cleaned_deal_ids;
}

function wg_get_deal_title( $post_id ) {
    if ( !$post_id ) {
        global $post;
        $post_id = $post->ID;
    }
    $deal = WP_Groupbuy_Deal::get_instance( $post_id );
    return $deal->get_title();

}

function wg_get_deal_additional_description( $post_id = 0){
    if ( !$post_id ) {
        global $post;
        $post_id = $post->ID;
    }
    $deal = WP_Groupbuy_Deal::get_instance( $post_id );
    return $deal->get_additional_title_description();
}