<?php
/**
 * Created by PhpStorm.
 * User: sinisa
 * Date: 25/06/16
 * Time: 20:07
 */


// Get the Payment Post Type
function wg_get_payment_gateway_post_type() {
    return WP_Groupbuy_Payment_Transaction::POST_TYPE;
}

function wg_get_payment_transaction_id() {
    return WP_Groupbuy_Payment_Transaction::get_transaction_id();
}

function wg_is_transaction_in_process($transaction_id) {
//http-equiv="refresh" content="5"
    $meta_tag_key='http-equiv';
    $meta_tag_value='refresh';
    $meta_content_tag='content';
    $meta_content_value='5';
    //  load current transaction status
    $transaction_current_status = WP_Groupbuy_Payment_Transaction::get_transaction_status($transaction_id);
    $transaction_final_status = WP_Groupbuy_Payment_Transaction::get_transaction_final_status($transaction_current_status);
    if ( $transaction_final_status === WP_Groupbuy_Payment_Transaction::WPG_MEGAPOS_TRANSACTION_FINAL_STATUS_SUCCESS  ) {
        //  redirect
        $redirectUrl = add_query_arg(array(
                'wg_checkout_action' => 'back_from_megapos',
                'back_from_megapos' => WP_Groupbuy_Payment_Transaction::get_transaction_payment_gateway($transaction_id)),
            WP_Groupbuy_Checkouts::get_url());
        wp_redirect($redirectUrl, 303);
    }
    elseif ( $transaction_final_status == WP_Groupbuy_Payment_Transaction::WPG_MEGAPOS_TRANSACTION_FINAL_STATUS_ERROR ) {
        return false;
    }
    //  refresh page after few secondsprint "refresh";
    do_action('wp_head', $meta_tag_key, $meta_tag_value, $meta_content_tag, $meta_content_value);
    return true;
}
?>