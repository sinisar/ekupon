<?php

// Get Purchase Post Type
function wg_get_purchase_post_type() {
	return WP_Groupbuy_Purchase::POST_TYPE;
}

// Get the Payment Post Type
function wg_get_payment_post_type() {
	return WP_Groupbuy_Payment::POST_TYPE;
}

// Print the currency symbol option
function wg_currency_symbol() {
	echo apply_filters( 'wg_currency_symbol', wg_get_currency_symbol() );
}

// Get the currency symbol, filtering out the location string(%)
function wg_get_currency_symbol( $filter_location = TRUE ) {
	$string = WP_Groupbuy_Payment_Processors::get_currency_symbol();
	if ( $filter_location && strstr( $string, '%' ) ) {
		$string = str_replace( '%', '', $string );
	}
	return apply_filters( 'wg_get_currency_symbol', $string );
}

// Print an amount as formatted money.
function wg_formatted_money( $amount, $decimals = TRUE ) {
	echo apply_filters( 'wg_formatted_money', wg_get_formatted_money( $amount, $decimals ), $amount );
}

// Return an amount as formatted money. Place symbol based on location.
function wg_get_formatted_money( $amount, $decimals = TRUE ) {
	$orig_amount = $amount;
	$symbol = wg_get_currency_symbol( FALSE );
	$number = number_format( floatval( $amount ), 2 );
	if ( strstr( $symbol, '%' ) ) {
		$string = str_replace( '%', $number, $symbol );
	} else {
		if($symbol == '€') {
			$string = $number . $symbol;
		}
		else {
			$string = $symbol . $number;
		}
	}
	if ( $number < 0 ) {
		$string = '-'.str_replace( '-', '', $string );
	}
	if ( !$decimals ) {
		$string = str_replace('.00','', $string);
	}
	return apply_filters( 'wg_get_formatted_money', $string, $orig_amount );
}
