<?php

// A wrapper around WP's __() to add the plugin's text domain
function wpg__( $string ) {
	return WP_Groupbuy_Controller::__( $string );
}

// A wrapper around WP's _e() to add the plugin's text domain
function wpg_e( $string ) {
	return WP_Groupbuy_Controller::_e( $string );
}

// login required
function wg_login_required() {
	return WP_Groupbuy_Controller::login_required();
}

// message
function wg_set_message( $message = '', $type = 'info' ) {
	$type = ( $type == 'error' ) ? WP_Groupbuy_Controller::MESSAGE_STATUS_ERROR : WP_Groupbuy_Controller::MESSAGE_STATUS_INFO ;
	WP_Groupbuy_Controller::set_message( $message, $type );
}

function wg_display_messages( $type = null, $ajax = FALSE ) {

	if ( apply_filters( 'wg_display_messages_via_ajax', $ajax ) ) {
		$success = apply_filters( 'wg_display_messages_success_js', "$('#wg_ajax_messages').parent().has('.wg-message').show().delay(5000).slideUp();" );
		?>
		<script type="text/javascript">
			jQuery(document).ready(function($){
				$.ajax({
					url: <?php echo admin_url('admin-ajax.php'); ?>,
					type: "POST",
					data: {
						action: 'wg_display_messages',
						wg_message_type: '<?php echo $type ?>'
					},
					success: function(result){
						$("#wg_ajax_messages").append(result);
						<?php echo $success; ?>
					}
				});
			});
		</script>
		<div id="wg_ajax_messages"></div>
		<?php
	} else {
		WP_Groupbuy_Controller::display_messages( $type );
	}

}

// using permalinks
function wg_using_permalinks() {
	return WP_Groupbuy_Controller::using_permalinks();
}

// admin url
function wg_admin_url( $section = NULL ) {
	$admin_url = admin_url('admin.php?page=wp-groupbuy/'.$section);
	return untrailingslashit($admin_url);
}

// Convert string to a number format
function wg_get_number_format( $value = 1, $dec_point = '.' , $thousands_sep = '' ) {
	$fraction = ( is_null($dec_point) || !$dec_point ) ? 0 : 2 ;
	return apply_filters( 'wg_get_number_format', number_format( floatval( $value ), $fraction, $dec_point, $thousands_sep ) );
}
function wg_number_format( $value = 1, $dec_point = '.' , $thousands_sep = '', $fraction = 2 ) {
	echo apply_filters( 'wg_number_format', wg_get_number_format( $value, $dec_point, $thousands_sep ) );
}

// Get Current Location for theme display
function wg_get_current_tax( $slug = false ) {
	global $wp_query;
	if ( $slug ) {
		return apply_filters( 'wg_get_current_tax', $wp_query->get_queried_object()->slug );
	} else {
		return apply_filters( 'wg_get_current_tax', $wp_query->get_queried_object()->name );
	}
}
function wg_current_tax() {
	echo apply_filters( 'wg_current_tax', wg_get_current_tax() );
}

// Utility to return the $_COOKIE
function wg_get_cookie( $cookie_name = 'wg_deals_site' ) {
	$cookie = '';
	if ( isset( $_COOKIE[ $cookie_name ] ) ) {
		$cookie = $_COOKIE[ $cookie_name ];
	}
	return apply_filters( 'wg_get_cookie', $cookie, $cookie_name );
}

// Recursive check to see if an array is empty
function wg_utility_array_empty( $array = array() ) {
	if ( is_array( $array ) ) {
		foreach ( $array as $value ) {
			if ( !wg_utility_array_empty( $value ) ) {
				return false;
			}
		}
	}
	elseif ( !empty( $array ) ) {
		return false;
	}
	return true;
}


// Developer Tools
if ( !function_exists( 'prp' ) ) {
	function prp( $array ) {
		echo '<pre style="white-space:pre-wrap;">';
		print_r( $array );
		echo '</pre>';
	}
}

if ( !function_exists( 'pp' ) ) {
	function pp() {
		$msg = __v_build_message( func_get_args() );
		echo '<pre style="white-space:pre-wrap; text-align: left; '.
			'font: normal normal 11px/1.4 menlo, monaco, monospaced; '.
			'background: white; color: black; padding: 5px;">'.$msg.'</pre>';
	}
	function dp() {
		$msg = __v_build_message( func_get_args(), 'var_dump' );
		echo '<pre style="white-space:pre-wrap;; text-align: left; '.
			'font: normal normal 11px/1.4 menlo, monaco, monospaced; '.
			'background: white; color: black; padding: 5px;">'.$msg.'</pre>';
	}

	function ep() {
		$msg = __v_build_message( func_get_args() );
		error_log( '**: '.$msg );
	}

	function __v_build_message( $vars, $func = 'print_r', $sep = ', ' ) {
		$msgs = array();

		if ( !empty( $vars ) ) {
			foreach ( $vars as $var ) {
				if ( is_bool( $var ) ) {
					$msgs[] = ( $var ? 'true' : 'false' );
				}
				elseif ( is_scalar( $var ) ) {
					$msgs[] = $var;
				}
				else {
					switch ( $func ) {
					case 'print_r':
					case 'var_export':
						$msgs[] = $func( $var, true );
						break;
					case 'var_dump':
						ob_start();
						var_dump( $var );
						$msgs[] = ob_get_clean();
						break;
					}
				}
			}
		}

		return implode( $sep, $msgs );
	}
}
