<?php
/**
 * Created by PhpStorm.
 * User: sinisa
 * Date: 07/03/2017
 * Time: 22:21
 */

function wg_get_purchase_billing_information( $post_id ) {
    if ( !$post_id ) {
        global $post;
        $post_id = $post->ID;
    }
    $purchase = WP_Groupbuy_Purchase::get_instance( $post_id );
    return $purchase->get_billing_information();
}

function wg_get_purchase_billing_information_property( $post_id, $property_name, $userInfo ) {
    $purchase = wg_get_purchase_billing_information($post_id);

    $returnValue = "";
    if($userInfo) {
        if(isset($purchase['first_name'])) {
            $returnValue .= $purchase['first_name']. ', ';
        }
        if(isset($purchase['last_name'])) {
            $returnValue .= $purchase['last_name']. ', ';
        }
        if(isset($purchase['street'])) {
            $returnValue .= $purchase['street']. ', ';
        }
        if(isset($purchase['city'])) {
            $returnValue .= $purchase['city']. ', ';
        }
        if(isset($purchase['country'])) {
            $returnValue .= $purchase['country'];
        }
        return $returnValue;
    }
    else {
        if (isset($purchase[$property_name])) {
            return $purchase[$property_name];
        }
    }
    return "";
}

