<?php
// Utilities

// Get the voucher post type
function wg_get_voucher_post_type() {
	return WP_Groupbuy_Voucher::POST_TYPE;
}

function wg_on_user_voucher_page($is_user = FALSE) {
    return wg_on_voucher_page() && $is_user;
}

// Currently viewing the vouchers loop/archive
function wg_on_voucher_page() {
	if ( WP_Groupbuy_Voucher::is_voucher_query() && !is_single() ) {
		return true;
	}
	return false;
}

function wg_on_business_voucher_page($is_user = FALSE) {

    return wg_on_voucher_page() && $is_user;

}


function wg_on_user_voucher_active_page($is_user = false) {
    if($is_user) {
        return wg_on_voucher_active_page();
    }
    else {
        return wg_on_business_voucher_active_page();
    }
}

// Currently viewing the active vouchers loop
function wg_on_voucher_active_page() {
	if ( get_query_var( WP_Groupbuy_Vouchers::FILTER_QUERY_VAR ) == WP_Groupbuy_Vouchers::FILTER_ACTIVE_QUERY_VAR ) {
		return true;
	}
	return;
}

// Currently viewing the active vouchers loop
function wg_on_all_business_voucher_page() {
    if ( get_query_var( WP_Groupbuy_Vouchers::FILTER_QUERY_VAR ) == WP_Groupbuy_Vouchers::FILTER_BUSINESS_QUERY_VAR ) {
        return true;
    }
    return;
}

function wg_on_business_voucher_active_page() {
    if ( get_query_var( WP_Groupbuy_Vouchers::FILTER_QUERY_VAR ) == WP_Groupbuy_Vouchers::FILTER_BUSINESS_ACTIVE_QUERY_VAR ) {
        return true;
    }
    return;
}

function wg_on_user_voucher_expired_page($is_user = FALSE) {
    if($is_user){
        return wg_on_voucher_expired_page();
    }
    else {
        return wg_on_business_voucher_expired_page();
    }
}

// Currently viewing the expired vouchers loop
function wg_on_voucher_expired_page() {
	if ( get_query_var( WP_Groupbuy_Vouchers::FILTER_QUERY_VAR ) == WP_Groupbuy_Vouchers::FILTER_EXPIRED_QUERY_VAR ) {
		return true;
	}
	return;
}

function wg_on_business_voucher_expired_page() {
    if ( get_query_var( WP_Groupbuy_Vouchers::FILTER_QUERY_VAR ) == WP_Groupbuy_Vouchers::FILTER_BUSINESS_EXPIRED_QUERY_VAR ) {
        return true;
    }
    return;
}

function wg_on_user_voucher_used_page($is_user = FALSE) {
    if($is_user) {
        return wg_on_voucher_used_page();
    }
    else {
        return wg_on_business_voucher_used_page();
    }
}

// Currently viewing the used vouchers loop
function wg_on_voucher_used_page() {
	if ( get_query_var( WP_Groupbuy_Vouchers::FILTER_QUERY_VAR ) == WP_Groupbuy_Vouchers::FILTER_USED_QUERY_VAR ) {
		return true;
	}
	return;
}

function wg_on_business_voucher_used_page() {
    if ( get_query_var( WP_Groupbuy_Vouchers::FILTER_QUERY_VAR ) == WP_Groupbuy_Vouchers::FILTER_BUSINESS_USED_QUERY_VAR ) {
        return true;
    }
    return;
}

// Get all vouchers from a single purchase id
function wg_get_vouchers_by_purchase_id( $purchase_id = 0 ) {
	return WP_Groupbuy_Voucher::get_vouchers_for_purchase( $purchase_id );
}

// URLS

// Print Vouchers URL
function wg_voucher_url($is_user = FALSE) {
    if($is_user) {
        echo  apply_filters( 'wg_voucher_url', wg_get_vouchers_url() );
    }
    else {
        echo  apply_filters( 'wg_business_voucher_url', wg_get_business_vouchers_url() );
    }
}

function wg_business_voucher_url() {
    echo  apply_filters( 'wg_business_voucher_url', wg_get_business_vouchers_url() );
}

// Print Vouchers URL
function wg_vouchers_url() {
	echo  apply_filters( 'wg_voucher_url', wg_get_vouchers_url() );
}

// Get Vouchers URL
function wg_get_vouchers_url() {
	return apply_filters( 'wg_get_vouchers_url', WP_Groupbuy_Vouchers::get_url() );
}

// Get Vouchers URL
function wg_get_business_vouchers_url() {
    return apply_filters( 'wg_get_business_vouchers_url', WP_Groupbuy_Vouchers::get_business_url() );
}

// Print used Vouchers URL
function wg_voucher_used_url($user_id = FALSE) {
    if( $user_id ) {
        echo  apply_filters( 'wg_voucher_used_url', wg_get_voucher_used_url() );
    }
    else {
        echo  apply_filters( 'wg_business_voucher_used_url', wg_get_business_voucher_used_url() );
    }
}

// Get used Vouchers URL
function wg_get_voucher_used_url() {
	return apply_filters( 'wg_get_voucher_used_url', WP_Groupbuy_Vouchers::get_used_url() );
}


// Get used Vouchers URL
function wg_get_business_voucher_used_url() {
    return apply_filters( 'wg_get_business_voucher_used_url', WP_Groupbuy_Vouchers::get_business_used_url() );
}

// Print expired Vouchers URL
function wg_voucher_expired_url($id_user = FALSE) {
    if($id_user) {
        echo  apply_filters( 'wg_voucher_expired_url', wg_get_voucher_expired_url() );
    }
    else {
        echo  apply_filters( 'wg_business_voucher_expired_url', wg_get_business_voucher_expired_url() );

    }
}

// Get expired Vouchers URL
function wg_get_business_voucher_expired_url() {
    return apply_filters( 'wg_get_business_voucher_expired_url', WP_Groupbuy_Vouchers::get_business_expired_url() );
}

// Get expired Vouchers URL
function wg_get_voucher_expired_url() {
	return apply_filters( 'wg_get_voucher_expired_url', WP_Groupbuy_Vouchers::get_expired_url() );
}

// Print active Vouchers URL
function wg_voucher_active_url($is_user = FALSE) {
    if($is_user) {
        echo  apply_filters( 'wg_voucher_active_url', wg_get_voucher_active_url() );
    }
    else {
        echo  apply_filters( 'wg_voucher_business_active_url', wg_get_voucher_business_active_url() );
    }
}

// Get active Vouchers URL
function wg_get_voucher_active_url() {
	return apply_filters( 'wg_get_voucher_active_url', WP_Groupbuy_Vouchers::get_active_url() );
}

function wg_get_voucher_business_active_url() {
    return apply_filters( 'wg_get_voucher_active_url', WP_Groupbuy_Vouchers::get_business_active_url() );
}

// Print the Voucher link; filtered for add-on use.
function wg_voucher_link( $voucher_id = 0 ) {
	if ( !$voucher_id ) {
		global $post;
		$voucher_id = $post->ID;
	}
	$link = '<a href="'.wg_get_voucher_permalink( $voucher_id ).'" title="'.wpg__( 'Download Voucher' ).'" class="alt_button voucher_download viewdetailOrder viewdetailOrderSmall" target="_blank">'.wpg__( 'Download' ).'</a>';
	echo apply_filters( 'wg_voucher_link', $link, $voucher_id );
}
//  Print a voucher permlink without temp_access => _wpnonce
function wg_voucher_link_to_pdf($voucher_id = 0 ) {
    if ( !$voucher_id ) {
        global $post;
        $voucher_id = $post->ID;
    }
    $voucher_url = wg_get_voucher_permalink( $voucher_id );
    $temp_access = FALSE;
    echo apply_filters( 'wg_voucher_permalink', $voucher_url, $voucher_id, $temp_access );}


// Print a vouchers permalink/url
function wg_voucher_permalink( $voucher_id = 0, $temp_access = FALSE ) {
	if ( !$voucher_id ) {
		global $post;
		$voucher_id = $post->ID;
	}
	$voucher_url = wg_get_voucher_permalink( $voucher_id );
	if ( $temp_access ) {
		$voucher_url = add_query_arg( array( '_wpnonce' => wg_create_temp_voucher_access_nonce( WP_Groupbuy_Voucher::TEMP_ACCESS_KEY . $voucher_id ) ), $voucher_url );
	}
	echo apply_filters( 'wg_voucher_permalink', $voucher_url, $voucher_id, $temp_access );
}
// Creates a random
function wg_create_temp_voucher_access_nonce( $action = -1 ) {
	$uid = 0;
	if ( !wg_is_user_guest_purchaser() ) {
		$user = wp_get_current_user();
		$uid = (int) $user->ID;
	}
	$i = wp_nonce_tick();
	return apply_filters( 'wg_create_temp_voucher_access_nonce', substr(wp_hash($i . $action . $uid, 'nonce'), -12, 10) );
}

// Return a vouchers permalink/url
function wg_get_voucher_permalink( $voucher_id = 0 ) {
	if ( !$voucher_id ) {
		global $post;
		$voucher_id = $post->ID;
	}
	return apply_filters( 'wg_get_voucher_permalink', get_permalink( $voucher_id ), $voucher_id );
}

// Does the voucher have a preview available. Enabled by editor when creating deal.
function wg_voucher_preview_available( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	// If a voucher id is passed get the deal id
	if ( get_post_type( $post_id ) === wg_get_voucher_post_type() ) {
		$post_id = wg_get_vouchers_deal_id( $post_id );
	}
	$deal = WP_Groupbuy_Deal::get_instance( $post_id );
	return apply_filters( 'wg_voucher_preview_available', WP_Groupbuy_Deals_Preview::has_key( $deal ) );
}

// Get the voucher preview url
function wg_get_voucher_preview_link( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	// If a voucher id is passed get the deal id
	if ( get_post_type( $post_id ) === wg_get_voucher_post_type() ) {
		$post_id = wg_get_vouchers_deal_id( $post_id );
	}
	if ( wg_voucher_preview_available( $post_id ) ) {
		$deal = WP_Groupbuy_Deal::get_instance( $post_id );
		return apply_filters( 'wg_get_voucher_preview_link', WP_Groupbuy_Deals_Preview::get_voucher_preview_link( $deal ) );
	}
	return;
}

// Print the voucher preview url
function wg_voucher_preview_link( $post_id = 0 ) {
	echo apply_filters( 'wg_voucher_preview_link', wg_get_voucher_preview_link( $post_id ) );
}


// Misc.

function wg_is_voucher_active( $voucher_id = 0 ) {
	if ( !$voucher_id ) {
		global $post;
		$voucher_id = $post->ID;
	}
	$voucher = WP_Groupbuy_Voucher::get_instance( $voucher_id );
	return apply_filters( 'wg_is_voucher_active', $voucher->is_active(), $voucher_id );
}

// Return an array of deal ids associated with the current users purchased vouchers
function wg_get_purchased_deals_with_vouchers( $status = NULL ) {
	return apply_filters( 'wg_get_purchased_deals_with_vouchers', WP_Groupbuy_Vouchers::get_deal_ids( $status ) );
}

// Print voucher archive page title
function wg_voucher_page_title() {
	echo wg_get_voucher_page_title();
}

// Get the voucher page title based on what's being filtered
function wg_get_voucher_page_title() {
	if ( wg_on_voucher_used_page() ) {
		$title = wpg__( 'Used Purchases' );
	} elseif ( wg_on_voucher_expired_page() ) {
		$title = wpg__( 'Expired Purchases' );
	} elseif ( wg_on_voucher_active_page() ) {
		$title = wpg__( 'Active Purchases' );
	} else {
		$title = wpg__( 'All Purchases' );
	}
	return $title;
}
// Return the deal object associated with a voucher
function wg_get_vouchers_purchase( $voucher_id = 0 ) {
	if ( !$voucher_id ) {
		global $post;
		$voucher_id = $post->ID;
	}
	$voucher = WP_Groupbuy_Voucher::get_instance( $voucher_id );
	$purchase = $voucher->get_purchase_id();
	
	return $purchase;
}

// Return the deal object associated with a voucher
function wg_get_vouchers_deal( $voucher_id = 0 ) {
	if ( !$voucher_id ) {
		global $post;
		$voucher_id = $post->ID;
	}
	$voucher = WP_Groupbuy_Voucher::get_instance( $voucher_id );
	if ( !is_a( $voucher, 'WP_Groupbuy_Voucher' ) ) {
		return FALSE;
	}
	$deal = $voucher->get_deal();
	if ( !is_a( $deal, 'WP_Groupbuy_Deal' ) ) {
		return FALSE;
	}
	return apply_filters( 'wg_get_voucher_deal', $deal, $voucher_id );
}

// Return the deal ID associated with a voucher
function wg_get_vouchers_deal_id( $voucher_id = 0 ) {
	if ( !$voucher_id ) {
		global $post;
		$voucher_id = $post->ID;
	}
	$voucher = WP_Groupbuy_Voucher::get_instance( $voucher_id );
	if ( !is_a( $voucher, 'WP_Groupbuy_Voucher' ) ) {
		return FALSE;
	}
	$deal = $voucher->get_deal();
	if ( !is_a( $deal, 'WP_Groupbuy_Deal' ) ) {
		return FALSE;
	}
	return apply_filters( 'wg_get_voucher_deal_id', $deal->get_ID(), $voucher_id );
}

// Get a vouchers claimed date
function wg_get_voucher_claimed( $voucher_id = 0 ) {
	if ( !$voucher_id ) {
		global $post;
		$voucher_id = $post->ID;
	}
	if ( !$voucher_id ) {
		return FALSE;
	}
	$voucher = WP_Groupbuy_Voucher::get_instance( $voucher_id );
	if ( !is_a( $voucher, 'WP_Groupbuy_Voucher' ) ) {
		return FALSE;
	}
	$claimed_date = $voucher->get_claimed_date();
	if($claimed_date) {
        return apply_filters( 'wg_get_voucher_claimed', date( get_option( 'date_format' ), $voucher->get_claimed_date()), $voucher_id );
    }
    else {
        return false;
    }

}

//  set voucher claimed manually
function wg_set_voucher_claimed($voucher_id = 0){
    if ( !$voucher_id ) {
        global $post;
        $voucher_id = $post->ID;
    }
    if ( !$voucher_id ) {
        return FALSE;
    }
    $voucher = WP_Groupbuy_Voucher::get_instance( $voucher_id );
    if ( !is_a( $voucher, 'WP_Groupbuy_Voucher' ) ) {
        return FALSE;
    }
    return apply_filters( 'wg_set_voucher_claimed', date( get_option( 'date_format' ), $voucher->set_claimed_date()), $voucher_id );
}

//  get voucher redemtion date - when user (last) clicked "Download" button - Downloaded a button
function wg_get_voucher_redemption_date($voucher_id = 0 )
{
    if (!$voucher_id) {
        global $post;
        $voucher_id = $post->ID;
    }
    if (!$voucher_id) {
        return FALSE;
    }
    $voucher = WP_Groupbuy_Voucher::get_instance( $voucher_id );
    if ( !is_a( $voucher, 'WP_Groupbuy_Voucher' ) ) {
        return FALSE;
    }
    $redemtion_date = $voucher->get_redemption_data()['date'];
    return apply_filters( 'wg_get_voucher_redemption_date', $redemtion_date, $voucher_id );

}

//  Check if voucher is redeemd. - if redemption_data != null
function wg_is_voucher_redeemed($voucher_id = 0 ) {
    if ( !$voucher_id ) {
        global $post;
        $voucher_id = $post->ID;
    }
    if ( !$voucher_id ) {
        return '';
    }
    $voucher = WP_Groupbuy_Voucher::get_instance( $voucher_id );
    if($voucher->get_redemption_data() != null) {
        $return = TRUE;
    }
    else {
        $return = FALSE;
    }
    return apply_filters( 'wg_has_voucher_redeemed', $return, $voucher_id );
}

// Check if a voucher is claimed
function wg_is_voucher_claimed( $voucher_id = 0 ) {
	if ( wg_get_voucher_claimed( $voucher_id ) ) {
		$return = TRUE;
	} else {
		$return = FALSE;
	}
	return apply_filters( 'wg_has_voucher_claimed', $return );
}

// Content / Display

// Print the vouchers logo
function wg_voucher_logo( $voucher_id = 0 ) {
	echo apply_filters( 'wg_voucher_logo', wg_get_voucher_logo( $voucher_id ), $voucher_id );
}

// Get the vouchers logo
function wg_get_voucher_logo( $voucher_id = 0 ) {
	if ( !$voucher_id ) {
		global $post;
		$voucher_id = $post->ID;
	}
	if ( !$voucher_id ) {
		return '';
	}
	$voucher = WP_Groupbuy_Voucher::get_instance( $voucher_id );
	return apply_filters( 'wg_get_voucher_logo', $voucher->get_logo(), $voucher_id );
}

// Print the vouchers logo image
function wg_voucher_logo_image( $voucher_id = 0 ) {
	echo apply_filters( 'wg_voucher_logo_image', wg_get_voucher_logo_image( $voucher_id ), $voucher_id );
}

// Get the vouchers logo image
function wg_get_voucher_logo_image( $voucher_id = 0 ) {
	$url = wg_get_voucher_logo( $voucher_id );
	$html = sprintf( '<img src="%s" alt="" />', $url );
	return apply_filters( 'wg_get_voucher_logo_image', $html, $voucher_id );
}

// Does the voucher have a logo
function wg_has_voucher_logo( $voucher_id = 0 ) {
	$logo = wg_get_voucher_logo( $voucher_id );
	return strlen( $logo ) > 0;
}

// Print the vouchers code
function wg_voucher_code( $voucher_id = 0 ) {
	echo apply_filters( 'wg_voucher_code', wg_get_voucher_code( $voucher_id ), $voucher_id );
}

// Get the vouchers code
function wg_get_voucher_code( $voucher_id = 0 ) {
	if ( !$voucher_id ) {
		global $post;
		$voucher_id = $post->ID;
	}
	if ( !$voucher_id ) {
		return '';
	}
	$voucher = WP_Groupbuy_Voucher::get_instance( $voucher_id );
	return apply_filters( 'wg_get_voucher_code', $voucher->get_serial_number(), $voucher_id );
}

// Print the vouchers security code
function wg_voucher_security_code( $voucher_id = 0 ) {
	echo apply_filters( 'wg_voucher_security_code', wg_get_voucher_security_code( $voucher_id ), $voucher_id );
}

// Get the vouchers security code
function wg_get_voucher_security_code( $voucher_id = 0 ) {
	if ( !$voucher_id ) {
		global $post;
		$voucher_id = $post->ID;
	}
	if ( !$voucher_id ) {
		return '';
	}
	$voucher = WP_Groupbuy_Voucher::get_instance( $voucher_id );
	return apply_filters( 'wg_get_voucher_security_code', $voucher->get_security_code(), $voucher_id );
}

// Print the vouchers fine print
function wg_voucher_fine_print( $voucher_id = 0 ) {
	echo apply_filters( 'wg_voucher_fine_print', wg_get_voucher_fine_print( $voucher_id ), $voucher_id );
}

// Get the vouchers fine print
function wg_get_voucher_fine_print( $voucher_id = 0 ) {
	if ( !$voucher_id ) {
		global $post;
		$voucher_id = $post->ID;
	}
	if ( !$voucher_id ) {
		return '';
	}
	$voucher = WP_Groupbuy_Voucher::get_instance( $voucher_id );
	return apply_filters( 'wg_get_voucher_fine_print', $voucher->get_fine_print(), $voucher_id );
}

// Print the vouchers map
function wg_voucher_map( $voucher_id = 0 ) {
	echo apply_filters( 'wg_voucher_map', wg_get_voucher_map( $voucher_id ), $voucher_id );
}

// Get the vouchers map
function wg_get_voucher_map( $voucher_id = 0 ) {
	if ( !$voucher_id ) {
		global $post;
		$voucher_id = $post->ID;
	}
	if ( !$voucher_id ) {
		return '';
	}
	$voucher = WP_Groupbuy_Voucher::get_instance( $voucher_id );
	return apply_filters( 'wg_get_voucher_map', $voucher->get_map(), $voucher_id );
}

// Print the vouchers instructions
function wg_voucher_usage_instructions( $voucher_id = 0 ) {
	echo apply_filters( 'wg_voucher_usage_instructions', wg_get_voucher_usage_instructions( $voucher_id ), $voucher_id );
}

// Get the vouchers instructions
function wg_get_voucher_usage_instructions( $voucher_id = 0 ) {
	if ( !$voucher_id ) {
		global $post;
		$voucher_id = $post->ID;
	}
	if ( !$voucher_id ) {
		return '';
	}
	$voucher = WP_Groupbuy_Voucher::get_instance( $voucher_id );
	return apply_filters( 'wg_get_voucher_usage_instructions', $voucher->get_usage_instructions(), $voucher_id );
}

// Print the vouchers locations
function wg_voucher_locations( $voucher_id = 0 ) {
	$locations = wg_get_voucher_locations( $voucher_id );
	$out = '';
	if ( empty( $locations ) && wg_has_merchant( wg_get_vouchers_deal_id() ) ) {
		$merchant_id = wg_get_merchant_id( wg_get_vouchers_deal_id() );
		if ( wg_has_merchant_name( $merchant_id ) ) {
			$locations[] = wg_get_merchant_name( $merchant_id );
		}
		if ( wg_has_merchant_street( $merchant_id ) ) {
			$locations[] = wg_get_merchant_street( $merchant_id );
		}
		if ( wg_has_merchant_city( $merchant_id ) ) {
			$city_line = wg_get_merchant_city( $merchant_id );
		}
		if ( $city_line && ( wg_has_merchant_state( $merchant_id ) || wg_has_merchant_zip( $merchant_id ) ) ) {
			$city_line .= ', ';
		}
		$city_line .= wg_get_merchant_state( $merchant_id );
		$city_line = rtrim( $city_line ) . ' ' . wg_get_merchant_zip( $merchant_id );
		$city_line = rtrim( $city_line );
		if ( $city_line ) {
			$locations[] = $city_line;
		}
	}
	if ( !empty( $locations ) ) {
		$out .= '<ul class="voucher_locations"><li>';
		$out .= implode( '</li><li>', $locations );
		$out .= '</li></ul>';
	}
	echo apply_filters( 'wg_voucher_locations', $out, $voucher_id );
}

// Get the vouchers locations
function wg_get_voucher_locations( $voucher_id = 0 ) {
	if ( !$voucher_id ) {
		global $post;
		$voucher_id = $post->ID;
	}
	if ( !$voucher_id ) {
		return '';
	}
	$voucher = WP_Groupbuy_Voucher::get_instance( $voucher_id );
	return array_filter( apply_filters( 'wg_get_voucher_locations', $voucher->get_locations(), $voucher_id ) );
}

// Print the vouchers expiration date
function wg_voucher_expiration_date( $voucher_id = 0, $format = '' ) {
	if ( $format == '' ) {
		$format = get_option( 'date_format' );
	}
	$date = wg_get_voucher_expiration_date( $voucher_id );
	$date = ( $date != '' ) ? date( $format, $date ) : '';
	echo apply_filters( 'wg_voucher_expiration_date', $date, $voucher_id );
}

// Get the vouchers expiration date
function wg_get_voucher_expiration_date( $voucher_id = 0 ) {
	if ( !$voucher_id ) {
		global $post;
		$voucher_id = $post->ID;
	}
	if ( !$voucher_id ) {
		return '';
	}
	$voucher = WP_Groupbuy_Voucher::get_instance( $voucher_id );
	if ( !is_a( $voucher, 'WP_Groupbuy_Voucher' ) ) {
		return FALSE;
	}
	$date = $voucher->get_expiration_date();
	if ( empty( $date ) ) $date = 0;
	return apply_filters( 'wg_get_voucher_expiration_date', $date, $voucher_id );
}

// Get the universal voucher logo
function wg_get_univ_voucher_logo() {
	return apply_filters( 'wg_get_univ_voucher_logo', WP_Groupbuy_Vouchers::get_voucher_logo() );
}

// Print the universal voucher logo
function wg_univ_voucher_logo() {
	$url = wg_get_univ_voucher_logo();
	$html = sprintf( '<img src="%s" alt="" />', $url );
	echo apply_filters( 'wg_univ_voucher_logo', $html );
}

// Is a universal voucher logo set
function wg_has_univ_voucher_logo() {
	$logo = wg_get_univ_voucher_logo();
	return strlen( $logo ) > 0;
}

// Get the universal fine print
function wg_get_univ_voucher_fine_print() {
	return apply_filters( 'wg_get_univ_voucher_fine_print', WP_Groupbuy_Vouchers::get_voucher_fine_print() );
}

// Print the universal fine print
function wg_univ_voucher_fine_print() {
	echo apply_filters( 'wg_univ_voucher_fine_print', wg_get_univ_voucher_fine_print() );
}

// Get the universal support option
function wg_get_voucher_support1() {
	return apply_filters( 'wg_get_voucher_support1', WP_Groupbuy_Vouchers::get_voucher_support1() );
}

// Print the universal support option
function wg_voucher_support1() {
	echo apply_filters( 'wg_voucher_support1', wg_get_voucher_support1() );
}

// Get the second universal support option
function wg_get_voucher_support2() {
	return apply_filters( 'wg_get_voucher_support2', WP_Groupbuy_Vouchers::get_voucher_support2() );
}

// Print the second universal support option
function wg_voucher_support2() {
	echo apply_filters( 'wg_voucher_support2', wg_get_voucher_support2() );
}

// Get the universal legal information
function wg_get_voucher_legal() {
	return apply_filters( 'wg_get_voucher_legal', WP_Groupbuy_Vouchers::get_voucher_legal() );
}

// Print the universal legal information
function wg_voucher_legal() {
	echo apply_filters( 'wg_voucher_legal', wg_get_voucher_legal() );
}

// Is this voucher send as gift
function wg_is_voucher_gift( $voucher_id = 0 ) {
    if ( $voucher_id ) {
        $voucher = WP_Groupbuy_Voucher::get_instance( $voucher_id );
        if ( $voucher ) {
            $gift = WP_Groupbuy_Gift::get_gift_for_purchase( $voucher->get_purchase_id() );
            if ( $gift ) {
                return true;
            }
        }
    }
    return false;
}
