<?php

// Get the location taxonomy slug
function wg_get_location_tax_slug() {
	return WP_Groupbuy_Deal::LOCATION_TAXONOMY;
}

// Return a list of locations with formatting options
function wg_get_list_locations( $format = 'ul', $hide_empty = TRUE ) {
	// Form an array of all the locations ( the location term is called 'deals' )
	$locations = wg_get_locations( $hide_empty );

	if ( empty( $locations ) )
		return '';

	$tag = $format;

	$list = '';
	if ( $format == 'ul' || $format == 'ol' ) {
		$list .= "<".$format." class='locations-ul clearfix'>";
		$tag = 'li';
	}

	foreach ( $locations as $location ) {
		if ( $location->taxonomy == wg_get_location_tax_slug() ) {
			$link = get_term_link( $location->slug, wg_get_location_tax_slug() );
			$active = ( $location->name == wg_get_current_location() ) ? 'current_item' : 'item';
			$list .= "<".$tag." id='location_slug_".$location->slug."_new' class='location-item ".$active."'>";
			$list .= "<a href='".apply_filters( 'wg_list_locations_link', $link, $location->slug )."' title='".sprintf( wpg__( 'Visit %s Deals' ), $location->name )."' id='location_slug_".$location->slug."'>".$location->name."</a>";
			$list .= "</".$tag.">";
		}
	}

	if ( $format == 'ul' || $format == 'ol' )
		$list .= "</".$format.">";

	return apply_filters( 'wg_get_list_locations', $list, $format, $hide_empty );
}

// Print a list of locations with formatting options
function wg_list_locations( $format = 'ul', $hide_empty = TRUE ) {
	echo apply_filters( 'wg_list_locations', wg_get_list_locations( $format, $hide_empty ) );
}

// Get the current location being viewed
function wg_get_current_location( $slug = false ) {
	global $wp_query;
	if ( is_tax() ) {
		if ( $slug ) {
			return apply_filters( 'wg_get_current_location', $wp_query->get_queried_object()->slug );
		} else {
			return apply_filters( 'wg_get_current_location', $wp_query->get_queried_object()->name );
		}
	}

}

// Print currently viewed location name
function wg_current_location() {
	echo apply_filters( 'wg_current_location', wg_get_current_location() );
}

// Return the locations object
function wg_get_locations( $hide_empty = TRUE ) {
	//return apply_filters( 'wg_get_locations', get_terms( array( wg_get_location_tax_slug() ), array( 'hide_empty'=>$hide_empty, 'fields'=>'all' ) ) );
	return apply_filters( 'wg_get_locations', get_tax_terms_orderby_meta_value( wg_get_location_tax_slug()));
}
