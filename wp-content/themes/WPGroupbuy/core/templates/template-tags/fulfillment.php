<?php

function wg_get_order_status( $purchase_id = 0 ) {
	if ( !$purchase_id ) {
		global $post;
		$post_id = $post->ID;
		if ( WP_Groupbuy_Purchase::POST_TYPE !== $the_post->post_type ) {
			if ( $the_post->post_type === WP_Groupbuy_Payment::POST_TYPE ) {
				$payment = WP_Groupbuy_Payment::get_instance( $post_id );
				$purchase_id = $payment->get_purchase();
			}
			elseif ( $the_post->post_type === WP_Groupbuy_Voucher::POST_TYPE ) {
				$voucher = WP_Groupbuy_Voucher::get_instance( $post_id );
				$purchase_id = $voucher->get_purchase_id();
			}
			else {
				return apply_filters( 'wg_get_order_status', NULL, 0 );
			}
		} else { $purchase_id = $post_id; } 
	}
	return apply_filters( 'wg_get_order_status', WP_Groupbuy_Fulfillment::get_status( $purchase_id ), $purchase_id );
}