<?php

// attributes
function wg_deal_has_attributes( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	if ( wg_get_deal_post_type() == get_post_type( $post_id ) ) {
		$attributes = WP_Groupbuy_Attribute::get_attributes( $post_id, 'id' );
		if ( !empty( $attributes ) ) {
			return TRUE;
		}
	}
	return;
}

// Get the voucher's title (with attribute titles appeneded) based on the voucher id.
function wg_get_attribute_title_by_voucher_id( $voucher_id, $ids = null ) {
	if ( is_null( $ids ) ) {
		$ids = WP_Groupbuy_Attributes::get_vouchers_attribute_id( $voucher_id );
	}
	if ( empty( $ids ) ) {
		return;
	}
	if ( is_array( $ids ) ) {
		$labels = array();
		foreach ( $ids as $id ) {
			$labels[] = get_the_title( $id );
		}
		$title = implode( ', ', $labels );
	} else {
		$title = get_the_title( $ids );
	}
	return apply_filters( 'wg_get_attribute_title_by_voucher_id', $title, $voucher_id );
}

// Print the voucher's title (with attribute titles appeneded) based on the voucher id.
function wg_attribute_title_by_voucher_id( $voucher_id ) {
	echo apply_filters( 'wg_attribute_title_by_voucher_id', wg_get_attribute_title_by_voucher_id( $voucher_id ) );
}
