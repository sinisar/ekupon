<?php
// On cart Page
function wg_on_cart_page() {
	return WP_Groupbuy_Carts::is_cart_page();
}
// Add to cart form
function wg_add_to_cart_form( $button_text = 'Add to Cart' ) {
	echo apply_filters( 'wg_add_to_cart_form', wg_get_add_to_cart_form( null, $button_text ) );
}
// Return add to cart form
function wg_get_add_to_cart_form( $deal_id = 0,  $button_text = 'Add to Cart' ) {
	$_id = $deal_id;
	if ( !$deal_id ) {
		global $id;
		$_id = $id;
	}
	ob_start();
	WP_Groupbuy_Carts::add_to_cart_form( $_id, $button_text );
	$form = ob_get_clean();
	return apply_filters( 'wg_get_add_to_cart_form', $form );
}
// Print Cart url
function wg_add_to_cart_action() {
	echo apply_filters( 'wg_add_to_cart_action', wg_get_add_to_cart_action() );
}
// Return Cart url
function wg_get_add_to_cart_action() {
	return apply_filters( 'wg_get_add_to_cart_action', wg_get_cart_url() );
}
// Print Cart url
function wg_cart_url() {
	echo apply_filters( 'wg_cart_url', wg_get_cart_url() );
}
// Return Cart url
function wg_get_cart_url() {
	return apply_filters( 'wg_get_cart_url', WP_Groupbuy_Carts::get_url() );
}
// Deals add to cart url
function wg_get_add_to_cart_url( $post_id = 0 ) {
	if ( !$post_id ) {
		global $post;
		$post_id = $post->ID;
	}
	return apply_filters( 'wg_get_add_to_cart_url', WP_Groupbuy_Carts::add_to_cart_url( $post_id ), $post_id );
}
// print deals add to cart url
function wg_add_to_cart_url( $post_id = 0 ) {
	echo apply_filters( 'wg_add_to_cart_url', wg_get_add_to_cart_url( $post_id ) );
}
// Get total of items in cart
function wg_get_cart_item_count() {
	$count = 0;
	$cart = WP_Groupbuy_Cart::get_instance();
	if ( is_a( $cart, 'WP_Groupbuy_Cart' ) ) {
		$count = $cart->item_count();
	}
	return apply_filters( 'wg_cart_item_count', $count );
}
// Print the total amount of items in a cart
function wg_cart_item_count() {
	echo apply_filters( 'wg_cart_item_count', wg_get_cart_item_count() );
}