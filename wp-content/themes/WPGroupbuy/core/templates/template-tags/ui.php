<?php

// Today's deal

// deal today's deal
function wg_is_todays_deal( $post_id = 0 ) {
    if ( !$post_id ) {
        global $post;
        $post_id = $post->ID;
    }
    $bool = FALSE;
    if ( isset( $_GET['todays_deal'] ) && 1 == $_GET['todays_deal'] ) {
        $bool = TRUE;
    }
    return apply_filters( 'wg_is_todays_deal', $bool, $post_id );
}

// Today's deal URL
function wg_get_todays_deal_url() {
    return apply_filters( 'wg_get_todays_deal_url', site_url( wg_get_todays_deal_path() ) );
}

// Today's deal path option
function wg_get_todays_deal_path() {
    return apply_filters( 'wg_get_todays_deal_path', WP_Groupbuy_UI::$todays_deal_path );
}


// Truncation

// Truncate a string, strip tags and append a more link
function wg_get_truncate( $text, $excerpt_length = 44, $more_link = false ) {

    $text = strip_shortcodes( $text );

    $text = apply_filters( 'the_excerpt', $text );
    $text = str_replace( ']]>', ']]&gt;', $text );
    $text = strip_tags( $text );

    $words = explode( ' ', $text, $excerpt_length + 1 );
    if ( count( $words ) > $excerpt_length ) {
        array_pop( $words );
        $text = implode( ' ', $words );
        $text = rtrim( $text );
        $text .= '&hellip;';
    }
    if ( $more_link ) {
        $text = $text.' '.'<a href="'.$more_link.'" class="more">&nbsp;&raquo;</a>';
    }
    return apply_filters( 'wg_get_truncate', $text, $excerpt_length, $more_link );
}

// Echo the truncated string
function wg_truncate( $text, $excerpt_length = 44, $more_link = false ) {
    echo apply_filters( 'wg_truncate', wg_get_truncate( $text, $excerpt_length, $more_link ) );
}

// Truncate a posts (deal, merchant, etc.) excerpt.
function wg_get_excerpt_char_truncation( $len = 200, $post_id = 0 ) {
    if ( !$post_id ) {
        global $post;
        $post_id = $post->ID;
    }
    $post = get_post( $post_id );
    $excerpt = $post->post_excerpt;
    if (  empty( $excerpt ) ) {
        $excerpt = $post->post_content;
    }
    $new_excerpt = substr( $excerpt, 0, $len );
    if ( strlen( $new_excerpt ) < strlen( $excerpt ) ) {
        $new_excerpt = $new_excerpt.'...';
    }
    return apply_filters( 'wg_get_excerpt_char_truncation', $new_excerpt, $len, $post_id );
}

// Print a truncated excerpt for a post (deal, merchant, etc.).
function wg_excerpt_char_truncation( $len = 200, $post_id = 0 ) {
    echo apply_filters( 'wg_excerpt_char_truncation', wg_get_excerpt_char_truncation( $len, $post_id ) );
}

// Truncate a posts (deal, merchant, etc.) title.
function wg_get_title_char_truncation( $len = 50, $post_id = 0 ) {
    if ( !$post_id ) {
        global $post;
        $post_id = $post->ID;
    }
    $title = get_the_title( $post_id );
    $new_title = wg_get_html_truncation( $len, $title );
    if ( strlen( $title ) != strlen( $new_title ) ) {
        $new_title .= '...';
    }
    return apply_filters( 'wg_title_char_truncation', $new_title, $len, $post_id );
}

// Print a truncated title for a post (deal, merchant, etc.).
function wg_title_char_truncation( $len = 200, $post_id = 0 ) {
    echo apply_filters( 'wg_title_char_truncation', wg_get_title_char_truncation( $len, $post_id ) );
}

// Truncate some HTML while not counting HTML elements against the max_length
function wg_get_html_truncation( $max_length, $html ) {
    $printed_length = 0;
    $position = 0;
    $tags = array();
    $out = '';

    while ( $printed_length < $max_length && preg_match( '{</?([a-z]+)[^>]*>|&#?[a-zA-Z0-9]+;}', $html, $match, PREG_OFFSET_CAPTURE, $position ) ) {
        list( $tag, $tag_position ) = $match[0];

        // Print text leading up to the tag.
        $str = substr( $html, $position, $tag_position - $position );
        if ( $printed_length + strlen( $str ) > $max_length ) {
            $out .= ( substr( $str, 0, $max_length - $printed_length ) );
            $printed_length = $max_length;
            break;
        }

        $out .= ( $str );
        $printed_length += strlen( $str );

        if ( $tag[0] == '&' ) {
            // Handle the entity.
            $out .= ( $tag );
            $printed_length++;
        } else {
            // Handle the tag.
            $tag_name = $match[1][0];
            if ( $tag[1] == '/' ) {
                // This is a closing tag.

                $openingTag = array_pop( $tags );
                assert( $openingTag == $tag_name ); // check that tags are properly nested.

                $out .= ( $tag );
            } else if ( $tag[strlen( $tag ) - 2] == '/' ) {
                    // Self-closing tag.
                    $out .= ( $tag );
                } else {
                // Opening tag.
                $out .= ( $tag );
                $tags[] = $tag_name;
            }
        }

        // Continue after the tag.
        $position = $tag_position + strlen( $tag );
    }

    // Print any remaining text.
    if ( $printed_length < $max_length && $position < strlen( $html ) ) {
        $out .= ( substr( $html, $position, $max_length - $printed_length ) );
    }


    // Close any open tags.
    while ( ! empty( $tags ) ) {
        $out .= sprintf( '</%s>', array_pop( $tags ) );
    }

    return apply_filters( 'wg_get_html_truncation', $out, $max_length, $html );
}

// Print truncated HTML
function wg_html_truncation( $max_length = 200, $html ) {
    echo apply_filters( 'wg_html_truncation', wg_get_html_truncation( $max_length, $html ) );
}