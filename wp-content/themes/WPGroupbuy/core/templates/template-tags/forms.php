<?php

// Print form field
function wg_form_field( $key, $data, $category ) {
	echo apply_filters( 'wg_form_field', wg_get_form_field( $key, $data, $category ), $key, $data, $category );
}
//  generate new chechbox array with checked keys set to TRUE:
//  [2] => TRUE, [3] => TRUE;
function generate_checked_checkboxes($options, $post_values){
	$return_array = array();
	// 	Array ( [1] => Default List [3] => Gorenjska regija [2] => Osrednjeslovenska )
	//  [0] => 3 [1] => 2
	foreach ( $options as $option_key => $option_label ) {
		if(in_array($option_key, $post_values)) {
			$return_array[$option_key] = TRUE;
		}
		else {
			$return_array[$option_key] = FALSE;
		}
	}
	return $return_array;
}
// Build and return form field
function wg_get_form_field( $key, $data, $category ) {
	$checked_array = array();
	if($data['type'] == 'checkboxes') {
		if (strpos($key, '[]') !== false) {
			$checkbox_keys = explode("[]", $key);
			if(strpos($checkbox_keys[0] , '[]') == false) {
                //	loading from DB, from account-edit form
                if ($category == 'mailinglist') {
                    $checked_array = WP_Groupbuy_Account::get_user_mailinglists_ids();
                } else {
                    //  on registration form enrole to all mailinglist by default.
                    if ($category == 'user') {
                        $checked_array = $data['options'];
                        for($i=0; $i< count($checked_array); $i++){
                            $checked_array[$i] = TRUE;
                        }
                    }
                    else {
                        //	loading on callback
                        $checked_array = $_REQUEST['wg_' . $category . '_' . $checkbox_keys[0]];
                    }
                }

            }
		}
		else {
			$checked_array = $_REQUEST['wg_'.$category.'_'.$key];
		}
		$data['checked'] = generate_checked_checkboxes($data['options'], $checked_array);
	}

	if ( empty($data['default']) && isset( $_REQUEST['wg_'.$category.'_'.$key] ) && $_REQUEST['wg_'.$category.'_'.$key] != '' ) {
		$data['default'] = $_REQUEST['wg_'.$category.'_'.$key];
	}
	if ( !isset( $data['attributes'] ) || !is_array( $data['attributes'] ) ) {
		$data['attributes'] = array();
	}
	foreach ( array_keys( $data['attributes'] ) as $attr ) {
		if ( in_array( $attr, array( 'name', 'type', 'id', 'rows', 'cols', 'value', 'placeholder', 'size', 'checked' ) ) ) {
			unset( $data['attributes'][$attr] ); // certain attributes are dealt with in other ways
		}
	}
	ob_start();
?>
	<span class="<?php wg_form_field_classes( $data ); ?>">
	<?php if ( $data['type'] == 'textarea' ): ?>
		<textarea name="wg_<?php echo $category; ?>_<?php echo $key; ?>" id="wg_<?php echo $category; ?>_<?php echo $key; ?>" rows="<?php echo isset( $data['rows'] )?$data['rows']:4; ?>" cols="<?php echo isset( $data['cols'] )?$data['cols']:40; ?>" <?php foreach ( $data['attributes'] as $attr => $attr_value ) { echo $attr.'="'.$attr_value.'" '; } ?> <?php if ( isset( $data['required'] ) && $data['required'] ) echo 'required'; ?>><?php echo $data['default']; ?></textarea>
	<?php elseif ( $data['type'] == 'tinycme' ): ?>
		<?php wp_editor( $data['default'], "wg_{$category}_{$key}", array('teeny' => true, 'textarea_rows' => $data['rows']) ); ?> 
	<?php elseif ( $data['type'] == 'select-state' ):  ?>
		<select name="wg_<?php echo $category; ?>_<?php echo $key; ?>" id="wg_<?php echo $category; ?>_<?php echo $key; ?>" <?php foreach ( $data['attributes'] as $attr => $attr_value ) { echo $attr.'="'.$attr_value.'" '; } ?> <?php if ( isset( $data['required'] ) && $data['required'] ) echo 'required'; ?>>
			<?php foreach ( $data['options'] as $group => $states ) : ?>
				<optgroup label="<?php echo $group ?>">
					<?php foreach ( $states as $option_key => $option_label ): ?>
						<option value="<?php echo $option_key; ?>" <?php selected( $option_key, $data['default'] ) ?>><?php echo $option_label; ?></option>
					<?php endforeach; ?>
				</optgroup>
			<?php endforeach; ?>
		</select>
	<?php elseif ( $data['type'] == 'select' ): ?>
		<select name="wg_<?php echo $category; ?>_<?php echo $key; ?>" id="wg_<?php echo $category; ?>_<?php echo $key; ?>" <?php foreach ( $data['attributes'] as $attr => $attr_value ) { echo $attr.'="'.$attr_value.'" '; } ?> <?php if ( isset( $data['required'] ) && $data['required'] ) echo 'required'; ?>>
			<?php foreach ( $data['options'] as $option_key => $option_label ): ?>
			<option value="<?php echo $option_key; ?>" <?php selected( $option_key, $data['default'] ) ?>><?php echo $option_label; ?></option>
			<?php endforeach; ?>
		</select>
	<?php elseif ( $data['type'] == 'multiselect' ): ?>
		<select name="wg_<?php echo $category; ?>_<?php echo $key; ?>[]" id="wg_<?php echo $category; ?>_<?php echo $key; ?>" <?php foreach ( $data['attributes'] as $attr => $attr_value ) { echo $attr.'="'.$attr_value.'" '; } ?> multiple="multiple" <?php if ( isset( $data['required'] ) && $data['required'] ) echo 'required'; ?>>
			<?php foreach ( $data['options'] as $option_key => $option_label ): ?>
				<option value="<?php echo $option_key; ?>" <?php if ( in_array( $option_key, $data['default'] ) ) echo 'selected="selected"' ?>><?php echo $option_label; ?></option>
			<?php endforeach; ?>
		</select>
	<?php elseif ( $data['type'] == 'radios' ): ?>
		<?php foreach ( $data['options'] as $option_key => $option_label ): ?>
			<span class="wg-form-field-radio">
				<label for="wg_<?php echo $category; ?>_<?php echo $key; ?>_<?php esc_attr_e( $option_key ); ?>"><input type="radio" name="wg_<?php echo $category; ?>_<?php echo $key; ?>" id="wg_<?php echo $category; ?>_<?php echo $key; ?>_<?php esc_attr_e( $option_key ); ?>" value="<?php esc_attr_e( $option_key ); ?>" <?php //checked( $option_key, $data['default'] ) ?> /> <?php _e( $option_label ); ?></label>
			</span>
		<?php endforeach; ?>
	<?php elseif ( $data['type'] == 'checkbox' ): ?>
		<input type="checkbox" name="wg_<?php echo $category; ?>_<?php echo $key; ?>"
               id="wg_<?php echo $category; ?>_<?php echo $key; ?>" <?php checked( TRUE, $data['default'] ); ?>
               value="<?php echo isset( $data['value'] )?$data['value']:'On'; ?>"
            <?php foreach ( $data['attributes'] as $attr => $attr_value ) { echo $attr.'="'.$attr_value.'" '; } ?> <?php if ( isset( $data['required'] ) && $data['required'] ) echo 'required'; ?>/>

	<?php elseif ( $data['type'] == 'checkboxes' ): ?>

		<?php foreach ( $data['options'] as $option_key => $option_label ): ?>
            <span class="clearfix">
			<input type="checkbox" name="wg_<?php echo $category; ?>_<?php echo $key; ?>"
                   id="wg_<?php echo $category; ?>_<?php echo $key; ?>" <?php checked( TRUE, $data['checked'][$option_key] ); ?>
                   value="<?php echo isset( $option_key )?$option_key:'On'; ?>"
                <?php foreach ( $data['attributes'] as $attr => $attr_value ) { echo $attr.'="'.$attr_value.'" '; } ?> <?php if ( isset( $data['required'] ) && $data['required'] ) echo 'required'; ?>/>
                <label for="wg_<?php echo $category; ?>_<?php echo $key; ?>_<?php esc_attr_e( $option_key ); ?>" class="menu-item-title"><?php _e( $option_label ); ?></label>
                <br/>
            </span>
		<?php endforeach; ?>
	<?php elseif ( $data['type'] == 'hidden' ): ?>
		<input type="hidden" name="wg_<?php echo $category; ?>_<?php echo $key; ?>" id="wg_<?php echo $category; ?>_<?php echo $key; ?>" value="<?php echo $data['value']; ?>" <?php foreach ( $data['attributes'] as $attr => $attr_value ) { echo $attr.'="'.$attr_value.'" '; } ?> />
	<?php elseif ( $data['type'] == 'file' ): ?>
		<input type="file" name="wg_<?php echo $category; ?>_<?php echo $key; ?>" id="wg_<?php echo $category; ?>_<?php echo $key; ?>" <?php if ( isset( $data['required'] ) && $data['required'] ) echo 'required'; ?>/>
	<?php elseif ( $data['type'] == 'bypass' ): ?>
		<?php echo $data['output']; ?>
	<?php else: ?>
		<input type="<?php echo $data['type']; ?>" name="wg_<?php echo $category; ?>_<?php echo $key; ?>" id="wg_<?php echo $category; ?>_<?php echo $key; ?>" class="text-input" value="<?php echo $data['default']; ?>" placeholder="<?php echo isset( $data['placeholder'] )?$data['placeholder']:''; ?>" size="<?php echo isset( $data['size'] )?$data['size']:40; ?>" <?php foreach ( $data['attributes'] as $attr => $attr_value ) { echo $attr.'="'.$attr_value.'" '; } ?> <?php if ( isset( $data['required'] ) && $data['required'] ) echo 'required'; ?>/>
	<?php endif; ?>
	<?php if ( !empty( $data['description'] ) ): ?>
		<p class="description help_block"><?php echo $data['description'] ?></p>
	<?php endif; ?>
	</span>
	<?php
	return apply_filters( 'wg_get_form_field', ob_get_clean(), $key, $data, $category );
}

// Utility to print form field classes
function wg_form_field_classes( $data ) {
	$classes = implode( ' ', wg_get_form_field_classes( $data ) );
	echo apply_filters( 'wg_form_field_classes', $classes, $data );
}

// Utility to build an array of a form fields classes
function wg_get_form_field_classes( $data ) {
	$classes = array(
		'wg-form-field',
		'wg-form-field-'.$data['type'],
	);
	if ( isset( $data['required'] ) && $data['required'] ) {
		$classes[] = 'wg-form-field-required';
	}
	return apply_filters( 'wg_get_form_field_classes', $classes, $data );
}

// Print form field label
function wg_form_label( $key, $data, $category ) {
	echo apply_filters( 'wg_form_label', wg_get_form_label( $key, $data, $category ), $key, $data, $category );
}

// Build and return a form field label
function wg_get_form_label( $key, $data, $category ) {
	$out = '<label for="wg_'.$category.'_'.$key.'">'.$data['label'].'</label>';
	if ( isset( $data['required'] ) && $data['required'] ) {
		$out .= ' <span class="required">*</span>';
	}
	return apply_filters( 'wg_get_form_label', $out, $key, $data, $category );
}

// Return a quantity select option
function wg_get_quantity_select( $start = 1, $end = 10, $selected = 1, $name = 'quantity_select' ) {
	if ( ( $end - $start ) > 100 ) {
		$input = '<input type="number" name="'.$name.'" value="'.$selected.'" min="'.$start.'" max="'.$end.'">';
		return $input;
	}
	$select = '<select name="'.$name.'">';
	for ( $i=$start; $i < $end+1; $i++ ) {
		$select .= '<option value="'.$i.'" '.selected( $selected, $i, FALSE ).'>'.$i.'</option>';
	}
	$select .= "<select>";
	return $select;
}
