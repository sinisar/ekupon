<?php
	global $wpdb; 
	$url = "http://wpgroupbuy.com/news/plugin/"; // News URL
	
	//Total sales
	$wp_list_table = new WP_Groupbuy_Payments_Table();
	$total_sales = 0;
	$query = "SELECT SQL_CALC_FOUND_ROWS  wp_posts.ID FROM wp_posts  WHERE 1=1  AND wp_posts.post_type = 'wg_payment' AND (wp_posts.post_status = 'publish' OR wp_posts.post_status = 'pending' OR wp_posts.post_status = 'authorized' OR wp_posts.post_status = 'partial' AND STR_TO_DATE(post_date, '%Y-%m-%d') >='2013-09-01' AND STR_TO_DATE(post_date, '%Y-%m-%d') <='2013-09-31')  ORDER BY wp_posts.post_date DESC";
	$payment_ids = $wpdb->get_col( $query );
	foreach ($payment_ids as $payment_id) {
		$total_sales += $wp_list_table->column_total_sales($payment_id);
	}
	
	//Total orders
	$query = "SELECT SQL_CALC_FOUND_ROWS  wp_posts.ID FROM wp_posts  WHERE 1=1  AND wp_posts.post_type = 'wg_purchase' ORDER BY wp_posts.post_date DESC";
	$wp_list_table->prepare_items();
	$order_item = count($wpdb->get_col( $query ));
	
	//Number of done deals
	$number_of_done_deals = 0;
	//Number of active deals
	$number_of_active_deals = 0;
	
	$query = "SELECT SQL_CALC_FOUND_ROWS  {$wpdb->posts}.ID FROM {$wpdb->posts}  WHERE 1=1  AND {$wpdb->posts}.post_type = 'wg_deal' AND ({$wpdb->posts}.post_status = 'publish')  ORDER BY {$wpdb->posts}.post_date";
	$deal_ids = $wpdb->get_col( $query );
	foreach ($deal_ids as $deal_id) {
		$deal = WP_Groupbuy_Deal::get_instance($deal_id);
		if ($deal->is_expired()) {
			$number_of_done_deals++;
		} else if ($deal->get_status() == "open"){
			$number_of_active_deals++;
		}
	}
	
	//Cal number of sales & sales amount
	$first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
	$last_day_this_month = date('Y-m-t');
	$dates = getDatesFromRange($first_day_this_month, $last_day_this_month);
	
	$saleamount = __('Sales amount');
	$salenumber = __('Number of sales');
	
	$statistics = array();
	foreach ($dates as $date) {
		 $statistics[$date] = array( $salenumber => 0 , $saleamount => 0,"date" => $date);
	 }
	$query = "SELECT *, DATE_FORMAT(post_date, '%Y-%m-%d') AS formatted_post_date FROM {$wpdb->posts} WHERE 1=1  AND {$wpdb->posts}.post_type = 'wg_payment' AND ({$wpdb->posts}.post_status = 'publish' OR {$wpdb->posts}.post_status = 'pending' OR {$wpdb->posts}.post_status = 'authorized' OR {$wpdb->posts}.post_status = 'partial' ) AND DATE_FORMAT(post_date, '%Y-%m-%d') >= '".$first_day_this_month."' AND DATE_FORMAT(post_date, '%Y-%m-%d') <= '".$last_day_this_month."'  ORDER BY {$wpdb->posts}.post_date DESC";
	 $wpdb->get_results( $query);
	 $results = $wpdb->last_result;
	 $max_number_of_sales = 1;
	foreach ($results as $result) {
		$date = $result->formatted_post_date;
		$statistics[$date][$saleamount] += $wp_list_table->column_total_sales($result->ID);
		$statistics[$date][$salenumber]++;
		$statistics[$date]["date"] = $date;
		$max_number_of_sales = max(array($max_number_of_sales,$statistics[$date][$salenumber] ));  
	}
	$statistics = array_values($statistics);
	$chart = json_encode($statistics);
	function getDatesFromRange($start, $end){
		$dates = array($start);
		while(end($dates) < $end){
			$dates[] = date('Y-m-d', strtotime(end($dates).' +1 day'));
		}
		return $dates;
	}
	
	?>
	<!DOCTYPE html>
	<html>
	   <head>
	   <link rel="stylesheet" type="text/css" href="<?php echo WG_URL?>/resources/css/jchartfx.css"/>
	   <link rel="stylesheet" type="text/css" href="<?php echo WG_URL?>/resources/css/wpg.styles.css"/>
	   <script type="text/javascript" src="<?php echo WG_URL?>/resources/js/jchartfx.system.js"></script>
	   <script type="text/javascript" src="<?php echo WG_URL?>/resources/js/jchartfx.coreVector.js"></script>
	   <script type="text/javascript" src="<?php echo WG_URL?>/resources/js/jquery-1.10.2.min.js"></script>
	   <script type="text/javascript">
		   window.onload = function () {
		onLoadDoc();
	}
	
	var chart1;
		
		
	function onLoadDoc() {
		chart1 = new cfx.Chart();
			var statistics = $('#statistics').val();
			var items = $.parseJSON(statistics);
			 axis2 = chart1.getAxisY2();
			 axis2.getLabelsFormat().setFormat(cfx.AxisFormat.Currency);
			 chart1.getAxisY().setMax($('#max_number_of_sales').val());
		 chart1.getAxisY().setMin(0);
		 //var data = chart1.getData();
		 //data.setSeries(2);
		 var series1 = chart1.getSeries().getItem(0);
		 var series2 = chart1.getSeries().getItem(1);
	
		 series1.setGallery(cfx.Gallery.Bar);
		 series1.setFillMode(cfx.FillMode.Pattern);
		 series1.setVolume(100);
	
		 series2.setGallery(cfx.Gallery.Lines);
		 series2.setMarkerShape(cfx.MarkerShape.None);
		 //series2.getLine().setWidth(4);
	
		 var axis2 = chart1.getAxisY2();
		 axis2.getGrids().getMajor().setVisible(false);
		 series2.setAxisY(axis2);
		 
	
		 chart1.setDataSource(items);
		chart1.getAxisX().setLabelAngle(90);
			chart1.getAxisX().setStep(1);
		var chartDiv = document.getElementById('ChartDiv1');
		chart1.create(chartDiv);
	}
	   </script>
	   </head>
	   <body>
	<input id="statistics" type="hidden" value='<?php echo $chart?>'></input>
	<input id="max_number_of_sales" type="hidden" value='<?php echo ($max_number_of_sales * 3)?>'></input>
	<div class="wpg-reports-wrap" id="poststuff">
		 <div class="wpg-reports-sidebar">
		<div class="postbox">
			 <h3><span>
			   <?php wpg_e( 'Sales Stats' ); ?>
			   </span></h3>
			 <div class="inside">
			<p><?php wpg_e( 'Total sales' ); ?>: <strong><?php echo wg_get_formatted_money( $total_sales ); ?></strong></p>
            <p><?php wpg_e( 'Total Orders' ); ?>: <strong><?php echo $order_item;?></strong></p>
		  </div>
		   </div>
		<div class="postbox">
			 <h3><span>
			   <?php wpg_e( 'Deals Stats' ); ?>
			   </span></h3>
			 <div class="inside">
			<p><?php wpg_e( 'Total Active Deals' ); ?>: <strong><?php echo $number_of_active_deals?></strong></p>
            <p><?php wpg_e( 'Total Completed Deals' ); ?>: <strong><?php echo $number_of_done_deals?></strong></p>
		  </div>
		   </div>
		<div class="postbox">
			 <h3><span>
			   <?php wpg_e( 'From WPG News' ); ?>
			   </span></h3>
			 <div class="inside">
			<p class="stat"><?php echo file_get_contents($url)?></p>
		  </div>
		   </div>
	  </div>
		 <div>
		<div class="postbox">
			 <h3><span>
			   <?php wpg_e( 'This month\'s sales' ); ?>
			   </span></h3>
			 <div class="inside chart">
			<div id="ChartDiv1" style="width:800px;height:375px;display:inline-block"></div>
		  </div>
		   </div>
	  </div>
	   </div>
	</body>
	</html>
