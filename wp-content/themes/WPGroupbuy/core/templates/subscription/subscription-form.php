
<script type="text/javascript">
jQuery(function(jQuery){

	jQuery(".close-box").click(function() {

      var lightbox = 'close';
      var post_url = '<?php echo admin_url( 'admin-ajax.php' ); ?>';

      jQuery.ajax({
          type: "POST",
          url: post_url,
          data: {
              action: 'wg_close_lightbox',
              lightbox: lightbox,
          },
          cache: false,
          success: function(data) {
              jQuery("#simplemodal-container, div#simplemodal-overlay").remove();                
          }
      });

  });

}); 
</script>
<style type="text/css" media="screen">
  #box_email input[type="button"] {
    background-color: #DBE9F3;
    background-image: -moz-linear-gradient(center bottom , #DBE9F3 0%, #FFFFFF 100%);
    border: 1px solid #9FC2DD;
    color: #468BBF;
    cursor: pointer;
    height: 38px;
    margin-top: 15px;
    border-radius: 3px;
    line-height: 13px;
    font-size: 13px;
    font-weight: bold;
    padding:12px 25px;
}
#wg_skip {
  cursor: pointer;
  text-decoration: underline;
}
</style>
<div id="wg_light_box">

	<form action="" id="wg_lightbox_subscription_form" name="wg_lightbox_subscription_form" method="post" class="clearfix">

    <!-- Box location -->
    <div id="box_location" class="box">
    
    <div class="box_header">
    	<p class="close-box">Close</p>
      <?php wpg_e('Get up to 70% off great deals') ?>
      <span><?php wpg_e('on restaurants, spas and experiences in your city') ?></span>
    </div>
    
      <?php 
		$locations = wg_get_locations( false );
		$no_city_text = get_option(WP_Groupbuy_List_Services::SIGNUP_CITYNAME_OPTION);
		if ( ( !empty($locations) || !empty($no_city_text) ) && $show_locations ) {
		?>
      <p class="box_txt_header">
        <?php wpg_e('what city would you like?'); ?>
      </p>
      <?php
		$current_location = null;
		if ( isset($_COOKIE[ 'wg_location_preference' ]) && $_COOKIE[ 'wg_location_preference' ] != '') {
			$current_location = $_COOKIE[ 'wg_location_preference' ];
		} elseif ( is_tax() ) {
			global $wp_query;
			$query_slug = $wp_query->get_queried_object()->slug;
			if ( isset($query_slug) && !empty( $query_slug ) ) {
				$current_location = $query_slug;
			}
		}
		echo '<select name="deal_location" id="e1" size="1" tabindex="12"><option value="none">&nbsp;</option>';
			foreach ($locations as $location) {
				echo '<option value="'.$location->slug.'" '.selected($current_location,$location->slug).'>'.$location->name.'</option>';
			}
			if ( !empty($no_city_text) ) {
				echo '<option value="notfound">'.esc_attr( $no_city_text ).'</option>';
			}
		echo '</select>';
		?>
        <div class="c"></div>
        <a href="#" id="continue-button" class="sc_button blue medium" rel="slides[buttonlightbox]"><?php wpg_e('Continue') ?></a>
        <br class="c">
      <?php printf(wpg__('Already have an account? <a href="%s" title="Login">Sign In</a>'), wp_login_url()) ?>
      <?php
		} ?>        
    </div>
    <!-- End Box location -->
    
    <!-- Box email -->
    <div id="box_email" class="box">
    <div class="box_header">
      <?php wpg_e('One more step') ?>
      <span><?php wpg_e('We\'ll send you unbeatable deals') ?></span>
    </div>
    <p class="box_txt_header">
        <?php wpg_e('your email address'); ?>
      </p>
      <input class="box_email_form" type="email" name="email_address" id="email_address" value="" tabindex="11">
      <div id="submit">
      <?php wp_nonce_field( 'wg_subscription' );?>
      
      <input type="button" class="box_submit medium" name="wg_subscription" id="wg_subscription" value="<?php wpg_e('Submit') ?>" tabindex="13">
      <br/>
      <a name="wg_skip" id="wg_skip"><?php wpg_e('Skip') ?></a>
    
    </div>
    <p class="box_agree"><?php wpg_e('By subscribing, I agree to the Terms of Service and Privacy Policy.') ?></p>
    </div>
    <!-- End Box email -->
    
    <div id="login"></div>
    
    </form>
</div>
<?php //} ?>