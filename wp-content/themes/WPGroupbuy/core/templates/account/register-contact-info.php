<?php do_action( 'wg_account_register_form_contact_info' ); ?>
<fieldset id="wg-account-contact-info">
	<legend><?php wpg_e( 'Contact Information' ); ?></legend>
	<table class="contact">
		<tbody>
			<?php foreach ( $fields as $key => $data ): ?>
				<tr>
					<?php if ( $data['type'] != 'checkbox' ): ?>
						<td><?php wg_form_label( $key, $data, 'contact' ); ?></td>
						<td><?php wg_form_field( $key, $data, 'contact' ); ?></td>
					<?php else: ?>
						<td colspan="2">
							<label for="wg_contact_<?php echo $key; ?>"><?php wg_form_field( $key, $data, 'contact' ); ?> <?php echo $data['label']; ?></label>
						</td>
					<?php endif; ?>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</fieldset>
