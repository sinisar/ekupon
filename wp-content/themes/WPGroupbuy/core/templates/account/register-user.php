<?php do_action( 'wg_account_register_form_user_account' ); ?>
<fieldset id="wg-account-user-info">
	<legend><?php wpg_e( 'Create Account' ); ?></legend>
	<table class="user">
		<tbody>
			<?php foreach ( $fields as $key => $data ): ?>
				<tr>
					<?php if ( $data['type'] != 'checkbox' || $data['type'] != 'checkboxes'): ?>
						<td><?php wg_form_label( $key, $data, 'user' ); ?></td>
						<td><?php wg_form_field( $key, $data, 'user' ); ?></td>
					<?php else: ?>
						<td colspan="2">
							<label for="wg_user_<?php echo $key; ?>"><?php wg_form_field( $key, $data, 'user' ); ?> <?php echo $data['label']; ?></label>
						</td>
					<?php endif; ?>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</fieldset>
