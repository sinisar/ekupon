<form id="wpg_account_edit" class="registration_layout" action="<?php wg_account_edit_url(); ?>" method="post">
	<input type="hidden" name="wg_account_action" value="<?php echo WP_Groupbuy_Accounts_Edit_Profile::FORM_ACTION; ?>" />
	<?php foreach ( $panes as $pane ) {
		echo $pane['body'];
	} ?>
	<?php do_action( 'wg_account_edit_form' ); ?>
</form>
