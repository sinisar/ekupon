<div id="wg_account_view">
	<?php foreach ( $panes as $pane ) {
		echo $pane['body'];
	} ?>
	<?php do_action( 'wg_account_view' ); ?>
	<p><a href="<?php wg_account_edit_url(); ?>"><?php wpg_e( 'Edit Your Account' ); ?></a></p>
</div>
