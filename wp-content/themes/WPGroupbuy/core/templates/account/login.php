<form action="<?php wg_account_login_url(); ?>" method="post" id="wpg_login" class="registration_layout clearfix">

	<h3 class="register_message message clearfix"><a href="<?php wg_account_register_url() ?>"><?php wpg_e( 'Don&rsquo;t have an account&#63; Register.' ); ?></a></h3>

	<table class="account">
		<tbody>
			<tr>
				<td><label for="log"><?php wpg_e( 'Your Username' ) ?>:</label></td>
				<td><span class="wg-form-field wg-form-field-text wg-form-field-required"><input tabindex="11" type="text" name="log" id="log" class="text-input" />
			</span></td>
			</tr>

			<tr>
				<td><label for="pwd"><?php wpg_e( 'Your Password' ) ?>:</label></td>
				<td><span class="wg-form-field wg-form-field-text wg-form-field-required"><input tabindex="12" type="password" name="pwd" id="pwd" class="text-input" />
			</span></td>
			</tr>
			<tr>
				<td>
					<?php if ( isset( $args['redirect'] ) ) : ?>
						<input type="hidden" name="redirect_to" value="<?php echo $args['redirect']; ?>" />
					<?php endif; ?>
					<?php echo $args['submit']; ?>
				</td>
				<td>
				<?php wp_nonce_field( 'wg_login_action', 'wg_login' ); ?>
				<?php do_action( 'wpg_login_form_fields' ) ?>
					<label for="rememberme" class="checkbox-label"><input name="rememberme" id="rememberme" type="checkbox" checked="checked" value="forever" /> <?php wpg_e( 'Keep Me Signed In' ); ?></label>
				</td>

			</tr>
		</tbody>
	</table>

	<p><a href="<?php echo wp_lostpassword_url(); ?>" title="<?php wpg_e( 'Lost password&#63;' ); ?>"><?php wpg_e( 'Forgot your password&#63;' ); ?></a></p>

</form>
<script type="text/javascript">
jQuery(document).ready(function(){
  	jQuery("#wpg_login").validate({
		rules: {
			'log': "required",
			'pwd': "required"
		  }
	});
	jQuery("#log").focus();
});
</script>
