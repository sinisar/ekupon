<form id="wg_account_register" class="registration_layout"  action="<?php wg_account_register_url(); ?>" method="post" enctype="multipart/form-data">
	<input type="hidden" name="wg_account_action" value="<?php echo WP_Groupbuy_Accounts_Registration::FORM_ACTION; ?>" />
	<?php foreach ( $panes as $pane ) {
	echo $pane['body'];
} ?>
	<?php if ( isset( $args['redirect'] ) ) : ?>
		<input type="hidden" name="redirect_to" value="<?php echo $args['redirect']; ?>" />
	<?php elseif ( isset( $_REQUEST['redirect_to'] ) ) : ?>
		<input type="hidden" name="redirect_to" value="<?php echo $_REQUEST['redirect_to']; ?>" />
	<?php endif; ?>
	<?php do_action( 'wg_account_register_form' ); ?>
</form>
<script type="text/javascript">
jQuery(document).ready(function(){
  	jQuery("#wg_account_register").validate({
		rules: {
			'wg_user_login': "required",
		    'wg_user_email': "required",
		    'wg_user_password': "required",
		    'wg_user_password2': "required",
			'wg_user_mailinglists': "required",
		    'wg_contact_first_name': "required",
		    'wg_contact_last_name': "required",
		    'wg_contact_street': "required",
		    'wg_contact_city': "required",
		    //'wg_contact_zone': "required",
		    //'wg_contact_postal_code': "required",
		    'wg_contact_country': "required"
		  }
	});
	jQuery("#wg_user_login").focus();
});
</script>
