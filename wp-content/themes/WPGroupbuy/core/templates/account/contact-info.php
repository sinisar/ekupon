<div class="contact-info">
	<h3><?php wpg_e( 'Contact Information' ); ?></h3>
	<p>
		<?php echo $name; ?>
		<br /><?php echo $email; ?>
		<br /><?php echo $phone; ?>
		<br /><?php echo wg_format_address( $address, 'string', '<br />' ); ?>
		<br />
		<h3><?php wpg_e( 'Mailing lists' ); ?></h3>
		<div>
			<?php if (count($mailinglist) > 0): ?>
				<ul>
					<?php foreach ( $mailinglist as $option_key => $option_label ): ?>
						<li>
							<label for="wg_mailinglist_id_<?php echo $option_key;?>">
								<?php _e( $option_label ); ?>
							</label>
						</li>
					<?php endforeach; ?>
				</ul>
			<?php endif; ?>
		</div>
	</p>
</div>