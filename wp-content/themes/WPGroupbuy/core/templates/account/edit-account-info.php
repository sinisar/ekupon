<fieldset id="wg-account-account-info">
	<legend><?php wpg_e( 'Account Information' ); ?></legend>
	<table class="account form-table">
		<tbody>
			<?php foreach ( $fields as $key => $data ): ?>
				<tr>
					<?php if ( $data['type'] != 'checkbox' ): ?>
						<td><?php wg_form_label( $key, $data, 'account' ); ?></td>
						<td><?php wg_form_field( $key, $data, 'account' ); ?></td>
					<?php else: ?>
						<td colspan="2">
							<label for="wg_account_<?php echo $key; ?>"><?php wg_form_field( $key, $data, 'account' ); ?> <?php echo $data['label']; ?></label>
						</td>
					<?php endif; ?>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</fieldset>
