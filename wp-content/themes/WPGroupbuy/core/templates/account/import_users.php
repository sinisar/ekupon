<fieldset id="wg-account-import-user">
    <legend><?php wpg_e('Import users'); ?></legend>
    <table class="">
        <tbody><?php //echo "inside import_users: "; print_r($fields);?>
        <?php foreach ( $fields as $key => $data ): ?>
            <tr>
                <?php if ( $data['type'] != 'checkboxes' ): ?>
                    <td class="edit-td"><?php wg_form_label($key, $data, 'import_users'); ?></td>
                    <td><?php wg_form_field($key, $data, 'import_users'); ?>
                        <?php wp_nonce_field( plugin_basename( __FILE__ ), 'wg_import_users_'.$key.'-nonce' ); ?>		</td>

                <?php else: ?>
                    <td colspan="2">
                        <label for="wg_import_users_<?php echo $key; ?>"><?php wg_form_field($key, $data, 'import_users'); ?> <?php echo $data['label']; ?></label>
                    </td>
                <?php endif; ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</fieldset>