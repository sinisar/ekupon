<form id="wpg_account_import" class="registration_layout" action="<?php wg_account_import_users_url(); ?>" method="post" enctype="multipart/form-data">
	<input type="hidden" name="wg_account_action" value="<?php echo WP_Groupbuy_Accounts_Import_Users::FORM_ACTION; ?>" />
	<?php foreach ( $panes as $pane ) {
		echo $pane['body'];
	} ?>
	<?php do_action( 'wg_account_import_form' ); ?>
</form>
