<fieldset id="wg-account-recaptcha">
	<legend class="mTop10"><?php wpg_e( 'For Human' ); ?></legend>
	<table class="collapsable subscription form-table">
		<tbody>
			<tr>
				<td class="edit-td first">
					<label><?php wpg_e( 'Verify' ) ?></label><br/>
				</td>
				<td>
					<?php echo $recaptcha_html ?>
				</td>
			</tr>
		</tbody>
	</table>
</fieldset>
