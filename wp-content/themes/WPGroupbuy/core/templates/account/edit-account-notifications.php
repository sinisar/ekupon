<fieldset id="wg-account-account-subscriptions">
	<legend><?php wpg_e( 'E-Mail Preferences' ); ?></legend>
	<table class="account form-table">
		<tbody>
			<?php foreach ( $fields as $key => $data ): ?>
				<tr>
					<?php if ( $data['type'] != 'checkbox' ): ?>
						<td><?php wg_form_label( $key, $data, 'subscription' ); ?></td>
						<td><?php wg_form_field( $key, $data, 'subscription' ); ?></td>
					<?php else: ?>
						<td colspan="2">
							<label for="wg_account_<?php echo $key; ?>"><?php wg_form_field( $key, $data, 'subscription' ); ?> <?php echo $data['label']; ?></label>
						</td>
					<?php endif; ?>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</fieldset>
