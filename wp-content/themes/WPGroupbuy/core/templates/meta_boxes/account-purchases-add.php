<?php
	$dropdown = wp_dropdown_pages( array(
		'echo' => 0,
		'post_type' => WP_Groupbuy_Deal::POST_TYPE,
		'show_option_none' => wpg__( ' -- Select a Deal -- ' ),
		'name' => WP_Groupbuy_Admin_Purchases::ADD_DEAL_ID_FIELD,
	) );
?>
<div id="account-add-purchase">
	<h4><?php wpg_e( 'Add a new purchase' ); ?> <img width="16" height="16" src="<?php echo (WG_RESOURCES . 'images/help.png')?>" class="help_tip" title="<?php wpg_e( 'If you need to add a purchase for a user manually, please use below form. Enter Deal ID and quantity then click Update button to complete' ); ?>"></h4>
	<?php if ( $dropdown != '' ): ?>
		<label style="margin-right: 25px;"><?php wpg_e( 'Deal' ); ?>: <?php echo $dropdown; ?></label>
	<?php else: ?>
		<script type="text/javascript">
			jQuery(document).ready( function($) {
				var $field = $('#account-add-purchase input');
				var $span = $('#deals_name_ajax');

				var show_deal_name = function() {
					$span.addClass('loading_gif').empty();
					var user_id = $field.val();
					if ( !user_id ) {
						$span.removeClass('loading_gif');
						return;
					}
					$.ajax({
						type: 'POST',
						dataType: 'json',
						url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
						data: {
							action: 'wpg_ajax_get_deal_info',
							id: user_id
						},
						success: function(data) {
							$span.removeClass('loading_gif');
							$span.empty().append(data.title + ' <span style="color:silver">(deal id:' + data.deal_id + ')</span>');
						}
					});
				};
				if ( $('#account-add-purchase input').length > 0 ) {
					$field.live('keyup',show_deal_name);
				}
			});
		</script>
		<style type="text/css">
			.loading_gif {
				background: url( '<?php echo WG_URL; ?>/resources/images/loader.gif') no-repeat 0 center;
				width: auto;
				height: 16px;
				padding-right: 16px;
				padding-bottom: 2px;
				margin-left: 10px;
				margin-top: 10px;
			}
		</style>
		<label style="margin-right: 25px;"><?php wpg_e( 'Enter Deal ID' ); ?>:<br/>
        <input type="text" size="8" name="<?php echo WP_Groupbuy_Admin_Purchases::ADD_DEAL_ID_FIELD; ?>" placeholder="<?php wpg_e('Deal ID') ?>" /></label> <span id="deals_name_ajax">&nbsp;</span>
	<?php endif ?>
	<br/><br/>
	<label><?php wpg_e( 'Quantity' ); ?>:<br/>
    <input type="text" size="3" name="<?php echo WP_Groupbuy_Admin_Purchases::ADD_DEAL_QUANTITY_FIELD; ?>" placeholder="0" /></label>
</div>
