<?php do_action( 'wg_meta_box_deal_limits_pre' ) ?>
<p>
	<p><label for="deal_min_purchases"><strong><?php wpg_e( 'Minimum Required Purchases' ) ?>:</strong></label></p>
	<input type="text" id="deal_min_purchases" name="deal_min_purchases" value="<?php print $minimum; ?>" size="5" /> <img width="16" height="16" src="<?php echo (WG_RESOURCES . 'images/help.png')?>" class="help_tip" title="<?php wpg_e( 'The number of purchases required before the deal is successfully made' ); ?>">
</p>
<p>
	<p><label for="deal_max_purchases"><strong><?php wpg_e( 'Maximum Purchases' ) ?>:</strong></label></p>
	<input type="text" id="deal_max_purchases" name="deal_max_purchases" value="<?php print $maximum; ?>" size="5" /> <img width="16" height="16" src="<?php echo (WG_RESOURCES . 'images/help.png')?>" class="help_tip" title="<?php wpg_e( 'The maximum number of purchases allowed for this deal' ); ?>">
</p>
<p>
	<p><label for="deal_max_purchases_per_user"><strong><?php wpg_e( 'Maximum Purchases per User' ) ?>:</strong></label></p>
	<input type="text" id="deal_max_purchases_per_user" name="deal_max_purchases_per_user" value="<?php print $max_per_user; ?>" size="5" /> <img width="16" height="16" src="<?php echo (WG_RESOURCES . 'images/help.png')?>" class="help_tip" title="<?php wpg_e( 'The maximum number of purchases allowed for this deal for one user' ); ?>">
</p>
<?php do_action( 'wg_meta_box_deal_limits' ) ?>
