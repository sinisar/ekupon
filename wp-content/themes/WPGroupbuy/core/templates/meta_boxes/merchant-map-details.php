<p>
	<label for="map_width"><?php wpg_e( 'Map width' ); ?></label><br />
	<input type="text" name="map_width" id="map_width" value="300" size="50" disabled="true"/>
</p>

<p>
	<label for="map_height"><?php wpg_e( 'Map height' ); ?></label><br />
	<input type="text" name="map_height" id="map_height" value="300" size="50" disabled="true"/>
</p>

<p>
	<label for="map_zoom"><?php wpg_e( 'Map zoom' ); ?></label><br />
	<input type="text" name="map_zoom" id="map_zoom" value="<?php echo esc_attr( $map_zoom ); ?>" size="50" />
</p>

<p>
	<label for="map_type"><?php wpg_e( 'Map type' ); ?></label><br />
	<select name="map_type" id="map_type" class="select2" style="width:300px;">
		<option value="SATELLITE" <?php selected( "SATELLITE", $map_type ); ?>>Satelit</option>
		<option value="HYBRID" <?php selected( "HYBRID", $map_type ); ?>>Hibrid</option>
		<option value="TERRAIN"<?php selected( "TERRAIN", $map_type ); ?>>Pokrajina</option>
	</select>
</p>

<p>
	<input type="checkbox" name="map_show_info_window" id="map_show_info_window" value="yes" <?php checked(true, $map_show_info_window != 'no', true); ?> />
	<label for="map_show_info_window"><?php wpg_e( 'Show map info window' ); ?></label><br />
	<br/>
</p>
