<?php do_action( 'wg_meta_box_deal_details_pre' ) ?>
<p><label for="deal_highlights"><strong><?php wpg_e( 'Deal&rsquo;s Highlights:' ); ?></strong></label></p>
	<?php wp_editor( $deal_highlights, 'deal_highlights', array('teeny' => true) ); ?> 
</p>
<p>
	<p><label for="deal_fine_print"><strong><?php wpg_e( 'Deal&rsquo;s Fine Print:' ); ?></strong></label></p>
	<?php wp_editor( $deal_fine_print, 'deal_fine_print', array('teeny' => true) ); ?> 
</p>
<p>
<p>
<p><label for="deal_merchant"><strong><?php wpg_e( 'Deal Merchant:' ); ?></strong></label> <img width="16" height="16" src="<?php echo (WG_RESOURCES . 'images/help.png')?>" class="help_tip" data-tip="<?php wpg_e( 'Assist Merchant for this deal' ); ?>"></p>
<input type="text" name="merchant_name" id="merchant_name" value="<?php echo esc_attr( $merchant_name ); ?>" size="50"  readonly/>
</p>
<?php do_action( 'wg_meta_box_deal_details' ) ?>
