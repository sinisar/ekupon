<script type="text/javascript" charset="utf-8">
	jQuery(document).ready(function($){
		jQuery(".wg_activate").click(function(event) {
			event.preventDefault();
				if( confirm( '<?php wpg_e("Are you sure? This will make the voucher immediately available for download.") ?>' ) ) {
					var $link = $( this ),
					voucher_id = $link.attr( 'ref' );
					url = $link.attr( 'href' );
					$( "#"+voucher_id+"_activate" ).fadeOut('slow');
					$.post( url, { activate_voucher: voucher_id },
						function( data ) {
								$( "#"+voucher_id+"_activate_result" ).append( '<?php self::_e( 'Activated' ) ?>' ).fadeIn();
							}
						);
				} else {
					// nothing to do.
				}
		});
		jQuery(".wg_deactivate").on('click', function(event) {
			event.preventDefault();
				if( confirm( '<?php wpg_e("Are you sure? This will immediately remove the voucher from customer access.") ?>' ) ) {
					var $deactivate_button = $( this ),
					deactivate_voucher_id = $deactivate_button.attr( 'ref' );
					$( "#"+deactivate_voucher_id+"_deactivate" ).fadeOut('slow');
					$.post( ajaxurl, { action: 'wpg_deactivate_voucher', voucher_id: deactivate_voucher_id, deactivate_voucher_nonce: '<?php echo wp_create_nonce( WP_Groupbuy_Destroy::NONCE ) ?>' },
						function( data ) {
								$( "#"+deactivate_voucher_id+"_deactivate_result" ).append( '<?php self::_e( 'Deactivated' ) ?>' ).fadeIn();
							}
						);
				} else {
					// nothing to do.
				}
		});
	});
</script>
<span class="meta_box_block_divider" style="margin-top: 10px; margin-bottom: 10px;"></span>
<table id="wg_purchases_tables">
	<div style="padding-bottom:10px"><b><?php wpg_e( 'Purchase History' ) ?></b></div>
	<tbody>

<?php

$purchases = WP_Groupbuy_Purchase::get_purchases( array( 'account' => $account->get_ID() ) );

if ( !empty( $purchases ) ) {
	rsort( $purchases );
	// Loop through all the deals a merchant has
        echo '<thead><tr> <th>'.wpg__( 'Order #' ).'</th> <th>'.wpg__( 'Deal name' ).'</th><th>'.wpg__( 'Voucher ID' ).'</th><th>'.wpg__( 'Status' ).'</th><th>'.wpg__( 'Voucher Code' ).'</th><th>'.wpg__( 'Total' ).'</th><th>'.wpg__( 'IP log' ).'</th></tr></thead><tbody>';
	foreach ( $purchases as $purchase_id ) {
		$purchase = WP_Groupbuy_Purchase::get_instance( $purchase_id );
		if ( is_a( $purchase, 'WP_Groupbuy_Purchase' ) ) {
			$user_ip = ( $purchase->get_user_ip() ) ? $purchase->get_user_ip() : wpg__( 'unknown' ) ;
			$vouchers = WP_Groupbuy_Voucher::get_vouchers_for_purchase( $purchase_id );
			foreach ( $vouchers as $key => $voucher_id ) {
				$voucher = WP_Groupbuy_Voucher::get_instance( $voucher_id );
				if ( is_a( $voucher, 'WP_Groupbuy_Voucher' ) ) {
					if ( get_post_status( $voucher_id ) != 'publish' ) {
						$activate_path = 'edit.php?post_type=wg_voucher&activate_voucher='.$voucher_id.'&_wpnonce='.wp_create_nonce( 'activate_voucher' );
						$status = '<span id="'.$voucher_id.'_activate_result"></span><a href="'.admin_url( $activate_path ).'" class="wg_activate button" id="'.$voucher_id.'_activate" ref="'.$voucher_id.'">Activate</a>';
					} else {
						$status = wpg__( 'Activated' );
						$status =  '<span id="'.$voucher_id.'_deactivate_result"></span><a href="javascript:void(0)" class="wg_deactivate button disabled" id="'.$voucher_id.'_deactivate" ref="'.$voucher_id.'">'.wpg__('Deactivate').'</a>';
					}
					echo '<tr>';
                                        if($key == 0) {
                                            echo '<td class="th" rowspan="'.count($vouchers).'">'.$purchase_id.'</td>';
                                        }
                                        echo '<td abbr="'.get_the_title( $purchase_id ).'" class="th" align="right">'.str_replace( wpg__( 'Voucher for ' ), '', get_the_title( $voucher_id ) ).'</th>';
					echo '<td class="th">'.$voucher_id.'</td>';
					echo '<td class="th">'.$status.'</td>';
					echo '<td class="th">'.$voucher->get_serial_number().'</td>';
                                        if($key == 0) {
                                            echo '<td class="th" rowspan="'.count($vouchers).'">'.wg_get_formatted_money( $purchase->get_total() ).'</td>';
                                            echo '<td class="th" rowspan="'.count($vouchers).'">'.$user_ip.'</td>';
                                        }
					echo '</tr>';
				}

			}
		}

	}
        
}
?>
	</tbody>
</table>
