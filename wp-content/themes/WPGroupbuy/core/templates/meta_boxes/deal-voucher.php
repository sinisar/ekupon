<?php do_action( 'wg_meta_box_deal_voucher_pre' ) ?>
<p id="voucher_expiration_edit">
    <label for="voucher_expiration_date"><strong><?php wpg_e( 'Voucher&rsquo;s Expiration Date:' ); ?></strong></label></br>
    <input type="text" name="voucher_expiration_date" id="voucher_expiration_date" class="medium-text" value="<?php if ( !empty( $voucher_expiration_date ) ) echo date( 'm/d/Y', $voucher_expiration_date ) ?>"  tabindex="511"> <img width="16" height="16" src="<?php echo (WG_RESOURCES . 'images/help.png')?>" class="help_tip" title="<?php wpg_e( 'Choose the voucher&rsquo;s expiration date. Make sure it isn\'t the same with the deal\'s expiration date' ); ?>">
</p>
<span class="meta_box_block_divider" style="margin-top: 10px; margin-bottom: 10px;"></span>

<!--
<p id="voucher_logo_edit">
    <label for="voucher_logo"><strong><?php //wpg_e( 'Voucher&rsquo;s Logo:' ); ?></strong></label><br/>
    <input name="voucher_logo" type="text" id="voucher_logo" tabindex="509" value="<?php //echo $voucher_logo; ?>" size="50">
    <input id="upload_image_button" class="button" type="button" value="<?php //wpg_e( 'Upload Image', 'wpgroupbuy'); ?>" />
    <img width="16" height="16" src="<?php //echo (WG_RESOURCES . 'images/help.png')?>" class="help_tip" title="<?php //wpg_e( 'Copy URL of image here or click upload image button. To remove image, just delete URL path on text field and save changes' ); ?>">
</br><img src="<?php //echo $voucher_logo; ?>">
<script type="text/javascript">
jQuery(document).ready(function($){
    var custom_uploader;
    $('#upload_image_button').click(function(e) {
        e.preventDefault();
        //If the uploader object has already been created, reopen the dialog
        if (custom_uploader) {
            custom_uploader.open();
            return;
        }
 
        //Extend the wp.media object
        custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Choose Image',
            button: {
                text: 'Choose Image'
            },
            multiple: false
        });
 
        //When a file is selected, grab the URL and set it as the text field's value
        custom_uploader.on('select', function() {
            attachment = custom_uploader.state().get('selection').first().toJSON();
            $('#voucher_logo').val(attachment.url);
        });
 
        //Open the uploader dialog
        custom_uploader.open();
    });
});
</script>

<span class="meta_box_block_divider" style="margin-top: 10px; margin-bottom: 10px;"></span>
<p id="voucher_prefix_edit">
    <p><label for="voucher_id_prefix"><strong><?php //wpg_e( 'Voucher&rsquo;s ID Prefix:' ); ?></strong></label></p>
    <input type="text" name="voucher_id_prefix" id="voucher_id_prefix" class="small-text" tabindex="510" value="<?php //echo $voucher_id_prefix; ?>"> <img width="16" height="16" src="<?php //echo (WG_RESOURCES . 'images/help.png')?>" class="help_tip" title="<?php //wpg_e( 'Leave this blank if you prefer to use your global settings' ); ?>">
</p>
<p id="voucher_serial_edit">
    <p><label for="voucher_serial_numbers"><strong><?php //wpg_e( 'Voucher&rsquo;s Code:' ); ?></strong></label> <img width="16" height="16" src="<?php //echo (WG_RESOURCES . 'images/help.png')?>" class="help_tip" title="<?php //wpg_e( 'Comma separated list. Leave blank if you want system automatically generate voucher code' ); ?>"></p>
    <textarea rows="3" cols="40" name="voucher_serial_numbers" tabindex="510" id="voucher_serial_numbers" style="width:60%"><?php //echo $voucher_serial_numbers; ?></textarea>
</p>
<p id="voucher_locations_edit">
    <p><label for="voucher_location_1"><strong><?php //wpg_e( 'Voucher&rsquo;s Redemption Addresses:' ); ?></strong></label> <img width="16" height="16" src="<?php //echo (WG_RESOURCES . 'images/help.png')?>" class="help_tip" title="<?php //wpg_e( 'Enter addresses where client uses your voucher' ); ?>"></p>
    <?php //$line_format = wpg__( 'Line %d:' ); ?>
    <?php //foreach ( $voucher_locations as $index => $location ) { ?>
        <input type="text" name="voucher_locations[]" id="voucher_location_<?php //echo $index+1; ?>" size="50" tabindex="<?php //echo 512+$index; ?>" value="<?php //esc_attr_e( $location ); ?>"><br />
    <?php //} ?>
</p>
<p id="voucher_map_edit">
    <p><label for="voucher_map"><strong><?php //wpg_e( 'Map:' ); ?></strong></label> <img width="16" height="16" src="<?php //echo (WG_RESOURCES . 'images/help.png')?>" class="help_tip" title="<?php //wpg_e( 'Go to MapQuest or Google Maps and create a map with multiple or single locations. Click on \'Link/Embed\' at the the top right of your map (MapQuest) or the link icon to the left of your map (Google Maps), copy the code from \'Paste HTML to embed in website\' here.' ); ?>"></p>
    <input type="text" name="voucher_map" id="voucher_map" size="80" tabindex="<?php //echo 512+count( $voucher_locations ); ?>" value="<?php //echo esc_attr_e( $voucher_map ); ?>">
</p>
    <?php //echo $voucher_map; ?>
</p>

<span class="meta_box_block_divider" style="margin-top: 10px; margin-bottom: 10px;"></span>
<p id="voucher_howto_edit">
    <p><label for="voucher_how_to_use"><strong><?php //wpg_e( 'How to use voucher?' ); ?></strong></label></p>
    <?php //wp_editor( $voucher_how_to_use, 'voucher_how_to_use', array('teeny' => true) ); ?>
</p>
-->
<?php do_action( 'wg_meta_box_deal_voucher' ) ?>
