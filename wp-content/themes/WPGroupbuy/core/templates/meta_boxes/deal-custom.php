<?php do_action( 'wg_meta_box_deal_custom_pre' ) ?>

<span class="meta_box_block_divider" style="margin-top: 10px; margin-bottom: 10px;"></span>
<p>
    <label for="merchant_name"><h4><?php wpg_e( 'Merchant name' ); ?></h4></label></p>
    <input type="text" name="merchant_name" id="merchant_name" value="<?php echo esc_attr( $merchant_name ); ?>" size="50"  readonly/>
</p>
<p>
	<p><label for="custom_options_terms"><h3><?php wpg_e( 'Terms and conditions' ); ?></h3></label></p>
    <?php wp_editor( $custom_options_terms, 'custom_options_terms', array('teeny' => true, 'editor_height' => 200) ); ?>
</p>
<span class="meta_box_block_divider" style="margin-top: 10px; margin-bottom: 10px;"></span>

<p>
	<p><label for="custom_options_redeem"><h3><?php wpg_e( 'Redeemability:' ); ?></h3></label></p>
    <?php wp_editor( $custom_options_redeem, 'custom_options_redeem', array('teeny' => true, 'editor_height' => 200) ); ?>
</p>
<span class="meta_box_block_divider" style="margin-top: 10px; margin-bottom: 10px;"></span>

<p>
	<p><label for="custom_options_other_terms"><h3><?php wpg_e( 'Other terms:' ); ?></h3></label></p>
    <?php wp_editor( $custom_options_other_terms, 'custom_options_other_terms', array('teeny' => true, 'editor_height' => 200) ); ?>
</p>
<span class="meta_box_block_divider" style="margin-top: 10px; margin-bottom: 10px;"></span>

<p>
	<p><label for="custom_options_need_help"><h3><?php wpg_e( 'Help:' ); ?></h3></label></p>
    <?php wp_editor( $custom_options_need_help, 'custom_options_need_help', array('teeny' => true, 'editor_height' => 200) ); ?>
</p>


<p>
    <p><label for="custom_options_voucher_extra_text"><h3><?php wpg_e( 'Voucher extra text:' ); ?></h3></label></p>
    <?php wp_editor( $custom_options_voucher_extra_text, 'custom_options_voucher_extra_text', array('teeny' => true, 'editor_height' => 200) ); ?>
</p>
<span class="meta_box_block_divider" style="margin-top: 10px; margin-bottom: 10px;"></span>

<?php do_action( 'wg_meta_box_deal_details' ) ?>
