<p>
    <label for="user_id"><?php wpg_e( 'User Id' ); ?></label><br />
    <input type="text" name="user_id" id="user_id" value="<?php echo esc_attr( $user_id ); ?>" size="50" />
</p>
<p>
	<label for="contact_name"><?php wpg_e( 'Contact Name' ); ?></label><br />
	<input type="text" name="contact_name" id="contact_name" value="<?php echo esc_attr( $contact_name ); ?>" size="50" />
</p>
<p>
	<label for="contact_street"><?php wpg_e( 'Contact Street' ); ?></label><br />
	<input type="text" name="contact_street" id="contact_street" value="<?php echo esc_attr( $contact_street ); ?>" size="50" />
</p>
<p>
	<label for="contact_city"><?php wpg_e( 'Contact City' ); ?></label><br />
	<input type="text" name="contact_city" id="contact_city" value="<?php echo esc_attr( $contact_city ); ?>" size="50" />
</p>
<p>
	<label for="contact_postal_code"><?php wpg_e( 'Contact Postal Code' ); ?></label><br />
	<input type="text" name="contact_postal_code" id="contact_postal_code" value="<?php echo esc_attr( $contact_postal_code ); ?>" size="5" />
</p>
<p>
	<label for="contact_country"><?php wpg_e( 'Contact Country' ); ?></label><br />
	<select name="contact_country" id="contact_country" class="select2" style="width:350px">
		<option></option>
		<?php $options = WP_Groupbuy_Controller::get_country_options(); ?>
		<?php foreach ( $options as $key => $label ): ?>
			<option value="<?php esc_attr_e( $key ); ?>" <?php selected( $key, $contact_country ); ?>><?php esc_html_e( $label ); ?></option>
		<?php endforeach; ?>
	</select>
</p>
<p>
	<label for="contact_phone"><?php wpg_e( 'Contact Phone' ); ?></label><br />
	<input type="text" name="contact_phone" id="contact_phone" value="<?php echo esc_attr( $contact_phone ); ?>" size="50" />
</p>
<p>
	<label for="website"><?php wpg_e( 'Website' ); ?></label><br />
	<input type="text" name="website" id="website" value="<?php echo esc_attr( $website ); ?>" size="50" />
</p>
<p>
	<label for="facebook"><?php wpg_e( 'Facebook' ); ?></label><br />
	<input type="text" name="facebook" id="facebook" value="<?php echo esc_attr( $facebook ); ?>" size="50" />
</p>
<p>
	<label for="twitter"><?php wpg_e( 'Twitter' ); ?></label><br />
	<input type="text" name="twitter" id="twitter" value="<?php echo esc_attr( $twitter ); ?>" size="50" />
</p>

<p>
    <p><label for="opening_hours"><?php wpg_e( 'Opening hours' ); ?></label><br /></p>
    <input type="text" name="opening_hours" id="opening_hours" value="<?php echo esc_attr( $opening_hours ); ?>" size="100"/>
</p>
