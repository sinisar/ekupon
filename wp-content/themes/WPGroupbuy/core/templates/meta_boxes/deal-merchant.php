<?php do_action( 'wg_meta_box_merchant' ) ?>
<p>
	<p><label for="deal_merchant"><strong><?php wpg_e( 'Deal Merchant:' ); ?></strong></label> <img width="16" height="16" src="<?php echo (WG_RESOURCES . 'images/help.png')?>" class="help_tip" title="<?php wpg_e( 'Assist Merchant for this deal' ); ?>"></p>
	<select name="deal_merchant" id="deal_merchant" class="select2" style="width:300px;">
		<option></option>
		<?php
foreach ( $merchants as $merchant ) {
	echo '<option value="' . $merchant->ID . '" ' . selected( $merchant->ID, $merchant_id ) . '>' . esc_html( $merchant->post_title ) .  "</option>\n";
} ?>
	</select>
</p>
<?php do_action( 'wg_meta_box_merchant' ) ?>
