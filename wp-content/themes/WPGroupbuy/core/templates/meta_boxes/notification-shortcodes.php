<p>
	<?php wpg_e( 'The following shortcodes are available to use.' ); ?>
</p>
<?php
foreach ( $type['shortcodes'] as $shortcode ) : if ( isset( $shortcodes[$shortcode] ) ) : ?>
<p>
	<strong>[<?php echo $shortcode; ?>]</strong> &mdash;
	<?php echo $shortcodes[$shortcode]['description']; ?>
</p>
<?php endif; endforeach; ?>
