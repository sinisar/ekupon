<?php do_action( 'wg_meta_box_deal_attributes_pre' ) ?>
<?php
	$categories = WP_Groupbuy_Attribute::get_attribute_taxonomies(); ?>
<div id="wg-deal-attributes"></br>
	<table class="widefat">
		<thead>
			<tr>
				<th id="attributes_sku"><?php wpg_e( 'Sku' ); ?></th>
				<th id="attributes_label"><?php wpg_e( 'Label' ); ?></th>
				<th id="attributes_price"><?php wpg_e( 'Price' ); ?></th>
				<th id="attributes_max_purch"><?php wpg_e( 'Max. Purchases' ); ?></th>
				<th id="attributes_desc"><?php wpg_e( 'Description' ); ?></th>
				<?php if ( !empty($categories) ): ?><th><?php wpg_e( 'Category' ); ?></th><?php endif; ?>
				<?php do_action( 'wg_meta_box_deal_attributes_rows_header' ) ?>
				<th id="attributes_remove"></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ( $attributes as $post_id => $data ): ?>
				<?php if ( !is_numeric( $post_id ) ) { $post_id = 0; } ?>
				<tr class="wg-deal-attributes-row">
					<td class="sku">
						<input class="wg-deal-attribute hidden" type="hidden" value="<?php echo $post_id; ?>" name="wg-attribute[attribute_id][]" />
						<input class="wg-deal-attribute" type="text" size="10" value="<?php esc_attr_e( $data['sku'] ); ?>" name="wg-attribute[sku][]" placeholder="<?php wpg_e( 'Sku: ' ); echo $post_id; ?>" />
					</td>
					<td class="title"><input disabled="disabled" class="wg-deal-attribute" type="text" size="15" value="<?php esc_attr_e( $data['title'] ); ?>" name="wg-attribute[title][]" placeholder="<?php wpg_e( 'Title' ); ?>" /></td>
					<td class="price"><input class="wg-deal-attribute" type="text" size="10" placeholder="<?php wpg_e( 'Default' ); ?>" value="<?php if ( $data['price']!=WP_Groupbuy_Attribute::DEFAULT_PRICE ) {esc_attr_e( $data['price'] );} ?>" name="wg-attribute[price][]" /></td>
					<td class="max_purchases"><input class="wg-deal-attribute" type="text" size="5" value="<?php esc_attr_e( $data['max_purchases'] ); ?>" name="wg-attribute[max_purchases][]" /></td>
					<td class="description"><textarea name="wg-attribute[description][]" cols="20" rows="3" placeholder="<?php wpg_e( 'Description' ); ?>"><?php echo esc_textarea( $data['description'] ); ?></textarea></td>
					<td class="category">
						<table width="100%" class="widefat">
						<?php foreach ( WP_Groupbuy_Attribute::get_attribute_taxonomies() as $taxonomy ): ?>
							<tr>
								<th scope="row"><label for="<?php echo 'wg-attribute[category]['.$taxonomy->name.'][]'; ?>"><?php echo $taxonomy->labels->name; ?>: </label></th>
								<td>
									<?php wp_dropdown_categories( array(
											'show_option_none' => '-- '.wpg__( 'None' ).' --',
											'taxonomy' => $taxonomy->name,
											'selected' => isset( $data['categories'][$taxonomy->name] )?$data['categories'][$taxonomy->name]:0,
											'name' => 'wg-attribute[category]['.$taxonomy->name.'][]',
											'hide_empty' => FALSE,
										) ); ?>
								</td>
							</tr>
						<?php endforeach; ?>
						</table>
					</td>
					<?php do_action( 'wg_meta_box_deal_attributes_rows', $post_id, $data ) ?>
					<td class="remove" valign="middle"><a type="button" class="wg-deal-attribute-remove" href="#" title="<?php wpg_e( 'Remove New Option' ); ?>"><img src="<?php echo (WG_RESOURCES . 'images/delete.png')?>"></a></td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>
<h4><a class="button" href="#" id="wg_add_attribute"><?php wpg_e( 'Add New' ); ?></a></h4>
<table id="wg-deal-attributes-template" style="display: none;"><tbody>
	<tr class="wg-deal-attributes-row">
		<td class="sku">
			<input class="wg-deal-attribute hidden" type="hidden" value="0" name="wg-attribute[attribute_id][]" />
			<input class="wg-deal-attribute" type="text" size="10" value="" name="wg-attribute[sku][]" placeholder="<?php wpg_e( 'Sku' ); ?>" />
		</td>
		<td class="title"><input class="wg-deal-attribute" type="text" size="15" value="" name="wg-attribute[title][]" placeholder="<?php wpg_e( 'Title' ); ?>" /><p class="description"><small><?php wpg_e( '(Permanent & Required)' ) ?></small></p></td>
		<td class="price"><input class="wg-deal-attribute" type="text" size="10" value="" name="wg-attribute[price][]" placeholder="<?php wpg_e( 'Default' ); ?>" /><p class="description"><small><?php wpg_e( '(Leave blank to use Deal price)' ) ?></small></p></td>
		<td class="max_purchases"><input class="wg-deal-attribute" type="text" size="5" value="" name="wg-attribute[max_purchases][]" /></td>
		<td class="description"><textarea name="wg-attribute[description][]" cols="20" rows="3" placeholder="<?php wpg_e( 'Description' ); ?>"></textarea></td>
		<?php if ( !empty($categories) ): ?>
			<td class="category">
				<table width="100%" class="widefat">
				<?php foreach ( WP_Groupbuy_Attribute::get_attribute_taxonomies() as $taxonomy ): ?>
					<tr>
						<th scope="row"><label for="<?php echo 'wg-attribute[category]['.$taxonomy->name.'][]'; ?>"><?php echo $taxonomy->labels->name; ?>: </label></th>
						<td>
							<?php wp_dropdown_categories( array(
								'show_option_none' => '-- '.wpg__( 'None' ).' --',
								'taxonomy' => $taxonomy->name,
								'selected' => 0,
								'name' => 'wg-attribute[category]['.$taxonomy->name.'][]',
								'hide_empty' => FALSE,
							) ); ?>
						</td>
					</tr>
				<?php endforeach; ?>
				</table>
			</td>
		<?php endif ?>
		<?php do_action( 'wg_meta_box_deal_attributes_rows_js' ) ?>
		<td class="remove" valign="middle"><a type="button" class="wg-deal-attribute-remove" href="#" title="<?php wpg_e( 'Remove This Option' ); ?>"><img src="<?php echo (WG_RESOURCES . 'images/delete.png')?>"></a></td>
	</tr>
</tbody></table>
<?php do_action( 'wg_meta_box_deal_attributes' ) ?>
