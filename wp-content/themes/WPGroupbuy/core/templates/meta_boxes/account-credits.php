<style>#postbox-container-2 #normal-sortables {display:none}</style>
<div id="wg_account_credit_part1">
<table class="form-table" id="wg_account_credit_part1_table">
	<tbody>
		<?php foreach ( $credit_types as $key => $data ): ?>
			<tr>
				<th colspan="2" scope="row" style="width:5%; border-bottom:1px solid #eee;">&nbsp;&nbsp;<strong><?php echo $data['label']; ?></strong>: <span class="credit-point"><?php esc_attr_e( $data['balance'] ); ?></span></th>
			</tr>
			<tr>
				<th width="20%" scope="row">&nbsp;&nbsp;<?php wpg_e( 'Action' ) ?>: </th>
				<td width="80%">
					<input class="show_hidden" type="radio" name="account_credit_action[<?php esc_attr_e( $key ); ?>]" id="add" value="add" /> <?php wpg_e( 'Add' ) ?><br/>
                    <input class="show_hidden" type="radio" name="account_credit_action[<?php esc_attr_e( $key ); ?>]" id="deduct" value="deduct" /> <?php wpg_e( 'Deduct' ) ?><br/>
                    <input class="show_hidden" type="radio" name="account_credit_action[<?php esc_attr_e( $key ); ?>]" id="change" value="change" /> <?php wpg_e( 'Change to' ) ?>
				</td>
			</tr>
			<tr style="display:none">
				<th scope="row">&nbsp;&nbsp;<?php wpg_e( 'Amount' ) ?>:</th>
				<td>
					<input name="account_credit_balance[<?php esc_attr_e( $key ); ?>]" type="text" size="10" />
				</td>
			</tr>
			<tr style="display:none">
				<th scope="row">&nbsp;&nbsp;<?php wpg_e( 'Comment' ) ?>:</th>
				<td>
					<textarea name="account_credit_notes[<?php esc_attr_e( $key ); ?>]" rows="4" style="width:99%;"></textarea>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
</div>
<?php foreach ($credit_types as $key => $data): ?>
	<table id="wg_purchases_tables">
		<div style="padding:10px 0"><b><?php echo $data['label']; ?> <?php wpg_e( 'Logs' ) ?></b></div>
		<tbody>
		<thead><th><?php wpg_e( 'Date' ) ?></th><th><?php wpg_e( 'Recorded by' ) ?></th><th><?php wpg_e( 'Notes' ) ?></th><th><?php wpg_e( 'Amount' ) ?></th><th><?php wpg_e( 'Total' ) ?></th></thead>
	<?php

	$records = WP_Groupbuy_Record::get_records_by_type_and_association( $account->get_ID(), WP_Groupbuy_Accounts::$record_type . '_' . $key );

	if ( apply_filters( 'wg_include_purchases_in_creditlog', '__return_true' ) ) {
		$purchases = WP_Groupbuy_Purchase::get_purchases( array( 'account' => $account->get_ID() ) );
	} else {
		$purchases = array();
	}

	if ( !empty( $purchases ) || !empty( $records ) ) {
		$items = array();

		// Loop through all the records
		foreach ( $records as $record_id ) {
			foreach ( $credit_types as $credit_key => $credit_data ) {

				$record = WP_Groupbuy_Record::get_instance( $record_id );
				$record_data = $record->get_data();

				$record_post = $record->get_post();
				$author = get_userdata( $record_post->post_author );
				$balance = (int)$record_data['current_total'];
				$balance = ($record_data['current_total_'.$credit_key]) ? $record_data['current_total_'.$credit_key] : $record_data['current_total'] ;
				$prior = (int)$record_data['prior_total'];
				$adjustment = ( $balance == (int)$record_data['adjustment_value'] ) ? (int)$record_data['adjustment_value'] - $prior : $balance - $prior ;
				$plusminus = ( $adjustment > 0 ) ? '+' : '';
				$items[get_the_time( 'U', $record_id )] = array(
					'date' => get_the_time( 'U', $record_id ),
					'recorded' => $author->user_login,
					'note' => $record_post->post_content,
					'amount' => $plusminus . $adjustment,
					'total' => $balance,
				);
			}
		}

		// Loop through all the purchases
		foreach ( $purchases as $purchase_id ) {
			$purchase = WP_Groupbuy_Purchase::get_instance( $purchase_id );
			$amount = $purchase->get_total( WP_Groupbuy_Affiliate_Credit_Payments::PAYMENT_METHOD );
			if ( $amount > 0.1 ) {
				$items[get_the_time( 'U', $purchase_id )] = array(
					'date' => get_the_time( 'U', $purchase_id ),
					'recorded' => wpg__( 'User' ),
					'note' => get_the_title( $purchase_id ),
					'amount' => number_format( floatval( $amount ), 2 ),
					'total' => wpg__( 'N/A' ),
				);
			}
		}
		uasort( $items, array( 'WP_Groupbuy_Records', 'sort_callback' ) );
		foreach ( $items as $key => $value ) {
			echo '<tr><td class="th">'.date( get_option( 'date_format' ).', '.get_option( 'time_format' ), $value['date'] ).'</td>';
			echo '<td class="th">'.$value['recorded'].'</td>';
			echo '<td class="th">'.$value['note'].'</td>';
			echo '<td class="th">'.$value['amount'].'</td>';
			echo '<td class="th">'.$value['total'].'</td>';
			echo '</tr>';
		}
	}

	?>
		</tbody>
	</table>
<?php endforeach ?>
