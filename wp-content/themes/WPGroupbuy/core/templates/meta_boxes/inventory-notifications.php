<span class="meta_box_block_divider" style="margin-top: 10px; margin-bottom: 10px;"></span>
<div class="inventory-notifications">
	<p>
		<label><input id="inventory-notification-toggle" type="checkbox" name="<?php echo WP_Groupbuy_Fulfillment::NOTIFICATION_STATUS_META_KEY; ?>" value="1" <?php checked($notify); ?> /> <?php wpg_e('Send low inventory notification email'); ?></label>
	</p>
	<div class="inventory-notification-options">
		<p>
			<p><label for="<?php echo WP_Groupbuy_Fulfillment::NOTIFICATION_LEVEL_META_KEY; ?>"><strong><?php wpg_e( 'Inventory Level' ) ?>:</strong></label></p>
			<input type="text" id="<?php echo WP_Groupbuy_Fulfillment::NOTIFICATION_LEVEL_META_KEY; ?>" name="<?php echo WP_Groupbuy_Fulfillment::NOTIFICATION_LEVEL_META_KEY; ?>" value="<?php print $level; ?>" size="5" /> <img width="16" height="16" src="<?php echo (WG_URL . '/resources/images/help.png')?>" class="help_tip" data-tip="<?php wpg_e( 'Send a notification when this many are left in stock' ); ?>">
		</p>
		<p>
			<p><label for="<?php echo WP_Groupbuy_Fulfillment::NOTIFICATION_RECIPIENT_META_KEY; ?>"><strong><?php wpg_e( 'Notification Email' ) ?>:</strong></label></p>
			<input type="text" id="<?php echo WP_Groupbuy_Fulfillment::NOTIFICATION_RECIPIENT_META_KEY; ?>" name="<?php echo WP_Groupbuy_Fulfillment::NOTIFICATION_RECIPIENT_META_KEY; ?>" value="<?php print $recipient; ?>" size="30" /> <img width="16" height="16" src="<?php echo (WG_URL . '/resources/images/help.png')?>" class="help_tip" data-tip="<?php printf(wpg__( 'Notifications will be sent to this address. If blank, emails will be sent to %s.' ), esc_html(get_option('admin_email'))); ?>">
		</p>
	</div>
</div>