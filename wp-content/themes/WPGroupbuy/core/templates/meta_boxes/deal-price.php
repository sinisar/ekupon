<?php do_action( 'wg_meta_box_deal_price_pre', $price, $dynamic_price, $shipping, $shippable, $shipping_dyn, $shipping_mode, $tax, $taxable, $taxrate, $value, $show_amount_saved, $coupon_value, $provision_value ) ?>
<script type="text/javascript">
	var wg_currency_symbol = "<?php wg_currency_symbol(); ?>";
</script>
<div id="deal_pricing_meta_wrap 1" class="clearfix">
	<span class="meta_box_block_divider" style="margin-top: 10px; margin-bottom: 10px;"></span>

	<div class="wg_meta_column float_left" style="float:none;">
		<p>
			<label for="deal_base_price"><strong><?php wpg_e( 'Price' ); ?>:</strong></label>
			<input id="deal_base_price" type="text" size="5" value="<?php echo $price; ?>" name="deal_base_price" /><?php wg_currency_symbol();  ?>
			<img width="16" height="16" src="<?php echo (WG_URL . '/resources/images/help.png')?>" class="help_tip" title="<?php wpg_e( 'Normal price before discount (redna cena)' ); ?>">
		</p>
        <!--
		<p>
			<p><label for="deal_value"><strong><?php //wpg_e( 'Deal&rsquo;s value:' ); ?></strong></label>
			<input type="text" name="deal_value" id="deal_value" class="small-text" tabindex="503" value="<?php //  echo esc_textarea( $deal_value ); ?>"><?php //wg_currency_symbol(); ?>
			<img width="16" height="16" src="<?php //echo (WG_URL . '/resources/images/help.png')?>" class="help_tip" title="<?php //wpg_e( 'Enter price you want for this deal (cena ponudnika)' ); ?>"></p>
		</p>
		-->
        <p>
        <p><label for="deal_coupon_value"><strong><?php wpg_e( 'Deal&rsquo;s coupon value:' ); ?></strong></label>
            <input type="text" name="deal_coupon_value" id="deal_coupon_value" class="small-text" tabindex="503" value="<?php echo esc_textarea( $deal_coupon_value[0] ); ?>"><?php wg_currency_symbol(); ?>
            <img width="16" height="16" src="<?php echo (WG_URL . '/resources/images/help.png')?>" class="help_tip" title="<?php wpg_e( 'Coupon price (cena kupona)' ); ?>"></p>
        </p>
        <p>
			<p><label for="deal_amount_saved"><strong><?php wpg_e( 'Deal&rsquo;s savings:' ); ?></strong></label>
			<input name="deal_amount_saved" type="text" id="deal_amount_saved" tabindex="504" value="<?php esc_attr_e( $deal_amount_saved ); ?>%" class="small-text" readonly> <img width="16" height="16" src="<?php echo (WG_URL . '/resources/images/help.png')?>" class="help_tip" title="<?php wpg_e( 'You should add something like \'50%\' to make it work perfectly on website' ); ?>"></p>
		</p>
        <p><label for="deal_provision_value"><strong><?php wpg_e( 'Deal&rsquo;s provision value:' ); ?></strong></label>
            <input type="text" name="deal_provision_value" id="deal_provision_value" class="small-text" tabindex="503" value="<?php echo esc_textarea( $deal_provision_value ); ?>">%
            <img width="16" height="16" src="<?php echo (WG_URL . '/resources/images/help.png')?>" class="help_tip" title="<?php wpg_e( 'Coupon provision (cena provizije)' ); ?>"></p>
        </p>

        <p>
            <p><label for="deal_show_amount_saved"><strong><?php self::_e( 'Display deal amount saved' ); ?></strong></label>
            <input name="deal_show_amount_saved" id="deal_show_amount_saved" type="checkbox" value="1"  <?php checked( $deal_show_amount_saved, 1 ); ?>/><img width="16" height="16" src="<?php echo (WG_URL . '/resources/images/help.png')?>" class="help_tip" title="<?php wpg_e( 'Check to display percentage of amount saved.' ); ?>"></p>
        </p>
        <p>
	</div>
    
    <span class="meta_box_block_divider" style="margin-top: 10px; margin-bottom: 10px;"></span>
	<div class="wg_meta_column float_right" style="float:none;">
		<div id="dynamic_pricing">
			<p><legend><strong><?php wpg_e( 'Dynamic Pricing' ); ?></strong> <img width="16" height="16" src="<?php echo (WG_URL . '/resources/images/help.png')?>" class="help_tip" title="<?php wpg_e( 'Enter number of purchases and discount price' ); ?>"></legend></p>
			<table id="dyn_price_table" class="widefat">
				<thead>
					<tr>
						<th class="left"><?php wpg_e( 'Purchases' ); ?></th>
						<th><?php wpg_e( 'Price' ); ?></th>
						<th></th>
					</tr>
				</thead>

				<tbody id="dynamic_costs">
					<tr>
						<td class="centered_text">
							<input type="text" name="dynamic_purchase_total" id="dynamic_purchase_total" class="tiny-text" size="5"/>
						</td>
						<td>
							<input type="text" name="dynamic_purchase_cost" id="dynamic_purchase_cost" class="tiny-text" size="5"/><?php wg_currency_symbol(); ?>
						</td>
						<td>
							<a id="add_dyn_cost" class="add-dyn-cost button hide-if-no-js"><?php wpg_e( 'Add' ); ?></a>
						</td>
					</tr>
					<?php
if ( is_array( $dynamic_price ) ) {
	foreach ( $dynamic_price as $total => $cost ) {
?>
									<tr>
										<td class="centered_text">
											<?php echo $total; ?>
										</td>
										<td>
											<?php wg_currency_symbol(); ?><input type="text" name="deal_dynamic_price[<?php echo $total; ?>]" value="<?php echo $cost; ?>" class="tiny-text" size="5">
										</td>
										<td>
											<a id="delete_dyn_cost" class="delete-dyn-cost button hide-if-no-js"><?php wpg_e( 'Remove' )  ?></a>
										</td>
									</tr>
								<?php
	}
}
?>
				</tbody>
			</table>
		</div>
		<?php do_action( 'wg_meta_box_deal_price_right', $price, $dynamic_price, $shipping, $shippable, $shipping_dyn, $shipping_mode, $tax, $taxable, $taxrate ) ?>
	</div>
        </br>
</div>

<span class="meta_box_block_divider" style="margin-top: 10px; margin-bottom: 10px;"></span>
    <?php  do_action( 'wg_meta_box_deal_price_left', $price, $dynamic_price, $shipping, $shippable, $shipping_dyn, $shipping_mode, $tax, $taxable, $taxrate ) ?>
    
<?php //do_action( 'wg_meta_box_deal_price', $price, $dynamic_price, $shipping, $shippable, $shipping_dyn, $shipping_mode, $tax, $taxable, $taxrate ) ?>
