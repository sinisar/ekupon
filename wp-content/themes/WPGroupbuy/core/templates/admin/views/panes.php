<fieldset id="wg-account-fields-info">
	<legend><?php self::_e('Additional Information'); ?></legend>
	<table class="account-fields">
		<tbody>
			<?php foreach ( $fields as $key => $data ): ?>
				<tr>
					<?php if ( $data['type'] != 'checkbox' ): ?>
						<td><?php wg_form_label($key, $data, 'account_fields'); ?></td>
						<td>
							<?php wg_form_field($key, $data, 'account_fields'); ?>
							<?php if ( $data['desc'] != '' ): ?>
								<br/><small><?php echo $data['desc']  ?></small>	
							<?php endif ?>
						</td>
					<?php else: ?>
						<td colspan="2">
							<label for="wg_account_<?php echo $key; ?>"><?php wg_form_field($key, $data, 'account_fields'); ?> <?php echo $data['label']; ?></label>
						</td>
					<?php endif; ?>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</fieldset>
<script type="text/javascript">
jQuery(document).ready(function(){
  	jQuery("#wg_account_register").validate({
		rules: {
			// Add all the fields you want the jQuery Validate script to check.
			'wg_account_fields_source': "required",
		  }
	});
});
</script>