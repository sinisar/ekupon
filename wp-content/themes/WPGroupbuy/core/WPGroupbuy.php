<?php

/* Core version */
define( 'WG_CORE', '2.3' );

// WPG directory 
define( 'WG_PATH', dirname(__FILE__) . '/');

// URL
define( 'WG_URL', strpos( WG_PATH, 'plugins' ) ? plugins_url( '', __FILE__ ) : get_template_directory_uri() . '/core/');

// URL to resources/ directory
define( 'WG_RESOURCES', WG_URL . 'resources/' );
// Minimum supported version of WordPress
define( 'WPG_SUPPORTED_WP_VERSION', version_compare( get_bloginfo( 'version' ), '2.0', '>=' ) );
// Minimum supported version of PHP
define( 'WPG_SUPPORTED_PHP_VERSION', version_compare( phpversion(), '5.2.4', '>=' ) );

/* Start PHP session if not yet started */
if( !session_id() ) {
    session_start();
}

// Compatibility check
if ( WPG_SUPPORTED_WP_VERSION && WPG_SUPPORTED_PHP_VERSION ) {
	wp_groupbuy_load();
	do_action( 'wp_groupbuy_load' );
} else {
	
	// Disable WPG if compatibility check fails
	wg_deactivate_plugin();
	add_action( 'admin_head', 'wpg_fail_notices' );
	function wpg_fail_notices() {
		if ( !WPG_SUPPORTED_WP_VERSION ) {
			echo '<div class="error"><p><strong>WPGroupbuy</strong> requires WordPress 3.3 or higher. Please upgrade WordPress and activate the WPGroupbuy plugin again.</p></div>';
		}
		if ( !WPG_SUPPORTED_PHP_VERSION ) {
			echo '<div class="error"><p><strong>WPGroupbuy</strong> requires PHP 5.2.4 or higher. Please upgrade PHP and activate the WPGroupbuy plugin again.</p></div>';
		}
	}
}

function wg_require_dir( $dir, $ext = '.php' ) {
	
	// GLOB is slow => Use DirectoryInterator
	foreach ( new DirectoryIterator( $dir ) as $item ) {
		$currentFile = (string) $item;
		if ( strpos( $currentFile, $ext ) !== false ) {
			// print("REQUIRE: ".$dir . $currentFile ."<br/>");
            require_once( $dir . $currentFile );
		}
	}
}

// Load the WPG application
function wp_groupbuy_load() {
	if ( class_exists( 'WP_Groupbuy' ) ) {
		wg_deactivate_plugin();
		return;
	}
	// router
	require_once WG_PATH.'/includes/controllers/router/router.php';
	// classes
	require_once WG_PATH.'/WPG.class.php';

	require_once WG_PATH.'/includes/classes/WPG.Model.class.php';
	require_once WG_PATH.'/includes/classes/WPG.PostType.class.php';

	wg_require_dir( WG_PATH.'/includes/classes/' );

	require_once WG_PATH.'/includes/controllers/WPG.Controller.class.php';
	require_once WG_PATH.'/includes/controllers/WPG.PaymentProcessors.class.php';
	require_once WG_PATH.'/includes/controllers/WPG.OffsiteProcessors.class.php';
	wg_require_dir( WG_PATH.'/includes/controllers/' );
	
	// ATTENTION: this files have to load in specific order, otherway website has a troubles
    require_once WG_PATH.'/includes/controllers/payment-gateway/WPG.MegaPOS.Diners.class.php';
    require_once WG_PATH.'/includes/controllers/payment-gateway/WPG.MegaPOS.Klik.class.php';
    require_once WG_PATH.'/includes/controllers/payment-gateway/WPG.MegaPOS.Moneta.class.php';
    require_once WG_PATH.'/includes/controllers/payment-gateway/WPG.MegaPOS.Card.class.php';
    require_once WG_PATH.'/includes/controllers/payment-gateway/WPG.OfflinePayment.class.php';
    require_once WG_PATH.'/includes/controllers/payment-gateway/WPG.UPN.Payment.class.php';

	wg_require_dir( WG_PATH.'includes/controllers/payment-gateway/' );

	do_action( 'wg_register_processors' );

	// template tags
	wg_require_dir( WG_PATH.'/templates/template-tags/' );

	// License Class
	//if( wpg_license_valid() ){

		do_action( 'wg_init' );

		do_action( 'wg_init_classes' );

		do_action( 'wg_init_controllers' );
				
		do_action( 'wg_loaded' );
	/*
	} else {
		add_action( 'admin_menu', 'wpg_license_menu' );
	}
	*/

}
// do_action when plugin is activated.
register_activation_hook( __FILE__, 'wg_plugin_activated' );
function wg_plugin_activated() {
	do_action( 'wg_plugin_activation_hook' );
}
// do_action when plugin is deactivated.
register_deactivation_hook( __FILE__, 'wg_plugin_deactivated' );
function wg_plugin_deactivated() {
	do_action( 'wg_plugin_deactivation_hook' );
}

function wg_deactivate_plugin() {
	if ( is_admin() && ( !defined( 'DOING_AJAX' ) || !DOING_AJAX ) ) {
		require_once ABSPATH.'/wp-admin/includes/plugin.php';
		deactivate_plugins( __FILE__ );
	}
}