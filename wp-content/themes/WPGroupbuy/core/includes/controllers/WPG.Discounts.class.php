<?php

add_action( 'wg_init_controllers', array( 'WP_Groupbuy_Discount', 'init' ), 145 );

class WP_Groupbuy_Discount extends WP_Groupbuy_Controller {

	public static function init() {
		add_filter('wg_addons', array(get_class(),'wg_addon'), 10, 1);
	}

	public static function wg_addon( $addons ) {
		$addons['discount_codes'] = array(
			'label' => self::__('Discount Codes'),
			'files' => array(
				__FILE__,
				WG_PATH. '/includes/controllers/WPG.Discounts.class.php',
				WG_PATH. '/includes/controllers/payment-gateway/WPG.DiscountPayment.class.php',
				WG_PATH. '/templates/template-tags/discount.php'
			),
			'callbacks' => array(
				array('WG_Discount', 'init'),
			),
		);
		return $addons;
	}

}

class WG_Discounts extends WP_Groupbuy_Controller {

	const DISCOUNT_NAME = 'wg_discount_code';
	private static $discount_payment_processor;

	public static function init() {
		
		self::$discount_payment_processor = WP_Groupbuy_Discount_Payments::get_instance();

		add_filter( 'wg_get_theme_custom_css', array(get_class(),'custom_css') );

		// REGISTRATION
		// add_filter('wg_account_registration_panes', array(get_class(), 'get_registration_panes'),100);
		// add_filter('wg_validate_account_registration', array(get_class(), 'validate_account_fields'), 10, 4);
		// add_action('wg_registration', array(get_class(), 'process_registration'), 50, 5);

		// Meta Boxes
		add_action('add_meta_boxes', array(get_class(), 'add_meta_boxes'));
		add_action('save_post', array( get_class(), 'save_meta_boxes' ), 10, 2 );

		// Admin columns
		add_filter ('manage_edit-'.WG_Discount::POST_TYPE.'_columns', array( get_class(), 'register_columns' ) );
		add_filter ('manage_'.WG_Discount::POST_TYPE.'_posts_custom_column', array( get_class(), 'column_display' ), 10, 2 );
		add_filter( 'manage_edit-'.WG_Discount::POST_TYPE.'_sortable_columns', array( get_class(), 'sortable_columns' ) );

		add_action( 'admin_init', array( get_class(), 'queue_deal_resources' ) );

		if ( is_admin() ) {
			// Admin columns
			add_action( 'load-edit.php', array(get_class(), 'ajax_callback' ) );
		}

		add_filter('wg_cart_extras', array( get_class(), 'cart_discount') );
		add_filter('wg_cart_line_items', array( get_class(), 'line_items' ), 10, 2 );
		add_filter('wg_cart_get_total', array( get_class(), 'cart_total' ), 10, 2 );

	}
	
	public static function custom_css( $css ) {
		ob_start();
		include WG_PATH.'resources/css/discount-codes.css';
		$new_css = ob_get_clean();
		$css .= apply_filters('wg_discount_custom_css', $new_css);
		return $css;
	}

	public static function get_discount( WG_Discount $discount, WP_Groupbuy_Cart $cart ) {
	
		if ( !is_a($discount,'WG_Discount') ) {
			return FALSE; // Expired
		}

		if ( $discount->is_expired() ) {
			return FALSE; // Expired
		}
		
		if ( $discount->get_limit() <= count($discount->get_purchases()) ) {
			return FALSE;
		}

		// Check cart requirements
		$products = $cart->get_items();
		$limited_to = $discount->get_deals();
		if ( !empty($limited_to) ) {
			$valid = FALSE;
			foreach ($products as $key => $item) {
				if ( in_array( $item['deal_id'], $limited_to )) {
					$valid = TRUE;
				}
			}
			if ( !$valid ) {
				return FALSE;
			}
		}

		// Single Use
		if ( $discount->is_single_use() && $discount->user_used() ) {
			return FALSE;
		}
		
		$get_discount = $discount->get_discount();
		if ( $discount->is_percentage() ) {
			// If limited just get the price of the limited deals.
			if ( !empty( $limited_to ) ) {
				foreach ($products as $key => $item) {
					if ( in_array( $item['deal_id'], $limited_to )) {
						$deal = WP_Groupbuy_Deal::get_instance( $item['deal_id'] );
						if ( is_object( $deal ) ) {
							$price = $deal->get_price( NULL, $item['data'] )*$item['quantity'];
							$total += $price;
						}
					}
				}
			}
			// If not limited just get the subtotal of the cart
			else {
				$total = $cart->get_subtotal();
			}
			// Build total discount
			if ( $get_discount >= 1 ) {
				$percentage = $get_discount/100;
			}
			$get_discount = $total*$percentage;
		}

		return $get_discount;

	}

	public static function cart_discount_total() {
		$discount_total = WG_Discount::get_cart_discount_total();
		return $discount_total;
	}

	public static function queue_deal_resources() {
		$post_id = isset( $_GET['post'] ) ? (int)$_GET['post'] : -1;
		if ( ( isset( $_GET['post_type'] ) && WG_Discount::POST_TYPE == $_GET['post_type'] ) || WG_Discount::POST_TYPE == get_post_type( $post_id ) ) {
			// Timepicker
			wp_enqueue_script( 'wg_frontend_deal_submit' );
			wp_enqueue_style( 'wg_frontend_deal_submit_timepicker_css' );
		}
	}

	public static function add_meta_boxes() {
		add_meta_box('discounts', self::__('Discount Details'), array(get_class(), 'show_meta_boxes'), WG_Discount::POST_TYPE, 'advanced', 'high');
	}

	public static function show_meta_boxes( $post, $metabox ) {
		$discount = WG_Discount::get_instance($post->ID);
		switch ( $metabox['id'] ) {
			case 'discounts':
				self::show_meta_box($discount, $post, $metabox);
				break;
			default:
				self::unknown_meta_box($metabox['id']);
				break;
		}
	}

	private static function show_meta_box( WG_Discount $discount, $post, $metabox ) {

		$deals = is_array($discount->get_deals()) ?  implode(',', $discount->get_deals()) : '' ;
		$code = $discount->get_code();
		$exp = $discount->get_exp();
		$purchases = $discount->get_purchases();
		$roles = $discount->get_roles();
		$limit = $discount->get_limit();
		$disc_option = $discount->get_discount();
		$percentage = (bool)$discount->is_percentage();
		$combo = (bool)$discount->is_combo();
		$single_use = (bool)$discount->is_single_use();
		
		print self::_load_view_string('meta-box', array(
			'deals' => $deals,
			'code' => is_null( $code ) ? '' : $code,
			'discount' => ($disc_option == '') ? '0' : $disc_option,
			'exp' => ($exp == 0) ? time()+1209600 : $exp,
			'purchases' => empty( $purchases ) ? '' : $purchases,
			'limit' => ($limit == 0) ? '100' : $limit,
			'roles' => empty( $roles ) ? array() : $roles,
			'percentage' => $percentage,
			'combo' => $combo,
			'single_use' => $single_use,
		), FALSE);
	}

	public static function save_meta_boxes( $post_id, $post ) {
		// only continue if it's an account post
		if ( $post->post_type != WG_Discount::POST_TYPE ) {
			return;
		}
		// don't do anything on autosave, auto-draft, bulk edit, or quick edit
		if ( wp_is_post_autosave( $post_id ) || $post->post_status == 'auto-draft' || defined('DOING_AJAX') || isset($_GET['bulk_edit']) ) {
			return;
		}
		// save all the meta boxes
		$discount = WG_Discount::get_instance($post_id);
		if ( !is_a($discount, 'WG_Discount') ) {
			return; // The account doesn't exist
		}
		self::save_meta_box($discount, $post_id, $post);
	}

	private static function save_meta_box( WG_Discount $discount, $post_id, $post ) {

		$deals = null;
		if ( isset( $_POST['deals'] ) && $_POST['deals'] != '' ) {
			$deals = explode(',', $_POST['deals']);
			$deals = array_map('trim', $deals);
		}
		$discount->set_deals($deals);
		
		$code = isset( $_POST['code'] ) ? $_POST['code'] : '';
		$discount->set_code($code);
		$disc_option = isset( $_POST['discount'] ) ? (int)$_POST['discount'] : 0;
		$discount->set_discount($disc_option);
		$roles = isset( $_POST['roles'] ) ? $_POST['roles'] : '0';
		$discount->set_roles($roles);
		$limit = isset( $_POST['limit'] ) ? (int)$_POST['limit'] : '100';
		$discount->set_limit($limit);
		$roles = isset( $_POST['roles'] ) ? $_POST['roles'] : '0';
		$discount->set_roles($roles);

		$exp = empty( $_POST['exp'] ) ? WG_Discount::NO_EXPIRATION_DATE : strtotime( $_POST['exp'] );
		$discount->set_exp($exp);

		if ( isset($_POST['combo']) && $_POST['combo'] ) {
			$discount->set_combo(1);
		} else {
			$discount->set_combo(0);
		}
		if ( isset($_POST['percentage']) && $_POST['percentage'] ) {
			$discount->set_percentage(1);
		} else {
			$discount->set_percentage(0);
		}
		if ( isset($_POST['single_use']) && $_POST['single_use'] ) {
			$discount->set_single_use(1);
		} else {
			$discount->set_single_use(0);
		}
	}

	public static function ajax_callback() {
		if ( !current_user_can('edit_posts') ) {
			return; // security check
		}

		$post_id = isset( $_GET['post'] ) ? (int)$_GET['post'] : 0;
		if ( ( isset( $_GET['post_type'] ) && WG_Discount::POST_TYPE != $_GET['post_type'] ) || ( $post_id && WG_Discount::POST_TYPE != get_post_type( $post_id ) ) ) {  // meant to coupon discount pages only
			if ( !isset($_REQUEST['check_coupon_code'] ) ) { // make sure it's not the ajax_callback on the edit page.
				return;
			}
		}

		$coupon_code = wp_verify_nonce( @$_REQUEST['nonce'], 'check_coupon_code') ? @$_REQUEST['check_coupon_code'] : '';
		
		if ( $coupon_code ) {
			$discount_ids = WG_Discount::get_discounts_by_code($coupon_code);
			if ( empty($discount_ids) || in_array( $_REQUEST['post_id'], $discount_ids) ) {
				echo "VALID";
			} else {
				echo "INUSE";
			}
			exit();
		} 
	}

	/**
	 * Add the default pane to the account edit form
	 */
	public function get_registration_panes( array $panes ) {
		$panes['discount_code'] = array(
			'weight' => 10,
			'body' => self::_load_view_string('panes', array( 'fields' => self::fields() )),
		);
		return $panes;
	}

	private function fields( $account = NULL ) {
		$fields = array(
			'discount_code' => array(
				'weight' => 0,
				'label' => self::__('Discount Code'),
				'type' => 'text',
				'required' => TRUE,
			),
		);
		$fields = apply_filters('discount_only_fields', $fields);
		return $fields;
	}

	private static function _load_view_string( $path, $args ) {
		ob_start();
		if (!empty($args)) extract($args);
		@include('lib/discount/'.$path.'.php');
		return ob_get_clean();
	}


	public static function register_columns( $columns ) {
		unset($columns['date']);
		$columns['code'] = self::__('Code');
		return $columns;
	}


	public static function column_display( $column_name, $id ) {
		global $post;
		$discount = WG_Discount::get_instance($id);

		if ( !is_a($discount,'WG_Discount') ) 
			return; // return for that temp post

		switch ( $column_name ) {
			case 'id':
				echo $id;
		        break;

			case 'code':
				echo $discount->get_code();
				break;

			default:
				# code...
				break;
		}
	}

	public static function sortable_columns( $columns ) {
		$columns['code'] = 'code';
		return $columns;
	}

	public static function does_code_exist( $code ) {
		$discount_ids = WG_Discount::get_discounts_by_code($code);
		if ( !empty($discount_ids) ) {
			return TRUE;
		}
		return FALSE;
	}

	public function column_orderby( $vars ) {
		if (isset( $vars['orderby']) && is_admin()) {
			switch ($vars['orderby']) {
				case 'code':
					$vars = array_merge( $vars, array(
						'meta_key' => WG_Discount::$meta_keys['code'],
						'orderby' => 'meta_value_num'
					) );
				break;
				default:
					# code...
					break;
			}
		}
 
		return $vars;
	}


	
	/**
	 * Hook into cart to show the discount
	 */
	public static function cart_has_discount() {
		$discount = self::cart_discount_total();
		if ( $discount >= 0.01 ) {
			return TRUE;
		}
		return;
	}
	
	public static function line_items( $line_items, WP_Groupbuy_Cart $cart ) {
		$has_discount = self::cart_has_discount();
		if ( $has_discount ) {
			$i = 0;
			$discounts = array();
			$discount_codes = WG_Discount::get_cart_discounts();
			foreach ($discount_codes as $code => $deducted ) {
				$discount = WG_Discount::get_instance_by_code($code);
				$discount_row = array(
					'discount'.$i => array(
						'label' => $discount->get_title(),
						'data' => wg_get_formatted_money(-$deducted),
						'weight' => 25+$i,
					),
				);
				$i++;
				$line_items = array_merge($discount_row,$line_items);
			}
		}
		return $line_items;
	}
	
	public static function cart_total( $total, WP_Groupbuy_Cart $cart ) {
		$discount = self::cart_discount_total();
		if ( $discount ) {
			$total = $total-$discount;
		}
		return $total;
	}

}