<?php

if ( !function_exists( 'WG_Router_load' ) ) {
	function WG_Router_load() {
		require_once 'router_utility.class.php';
		require_once 'router.class.php';
		require_once 'route.class.php';
		require_once 'router_page.class.php';
		add_action( 'init', array( 'WG_Router_Utility', 'init' ), -100, 0 );
		add_action( WG_Router_Utility::PLUGIN_INIT_HOOK, array( 'WG_Router_Page', 'init' ), 0, 0 );
		add_action( WG_Router_Utility::PLUGIN_INIT_HOOK, array( 'WG_Router', 'init' ), 1, 0 );
	}

	WG_Router_load();
}
