<?php

class WG_Router_Page extends WG_Router_Utility {
	const POST_TYPE = 'wp_router_page';

	protected static $rewrite_slug = 'WG_Router';
	protected static $post_id = 0;

	protected $contents = '';
	protected $title = '';
	protected $template = '';

	public static function init() {
		self::register_post_type();
	}

	// Register a post type to use when displaying pages
	private static function register_post_type() {
		$args = array(
			'public' => FALSE,
			'show_ui' => FALSE,
			'exclude_from_search' => TRUE,
			'publicly_queryable' => TRUE,
			'show_in_menu' => FALSE,
			'show_in_nav_menus' => FALSE,
			'supports' => array( 'title' ),
			'has_archive' => TRUE,
			'rewrite' => array(
				'slug' => self::$rewrite_slug,
				'with_front' => FALSE,
				'feeds' => FALSE,
				'pages' => FALSE,
			)
		);
		register_post_type( self::POST_TYPE, $args );
	}

	// Get the ID of the placeholder post
	protected static function get_post_id() {
		if ( !self::$post_id ) {
			$posts = get_posts( array(
					'post_type' => self::POST_TYPE,
					'post_status' => 'publish',
					'posts_per_page' => 1,
				) );
			if ( $posts ) {
				self::$post_id = $posts[0]->ID;
			} else {
				self::$post_id = self::make_post();
			}
		}
		return self::$post_id;
	}

	// Make a new placeholder post
	private static function make_post() {
		$post = array(
			'post_title' => self::__( 'WP Router Placeholder Page' ),
			'post_status' => 'publish',
			'post_type' => self::POST_TYPE,
		);
		$id = wp_insert_post( $post );
		if ( is_wp_error( $id ) ) {
			return 0;
		}
		return $id;
	}

	public function __construct( $contents, $title, $template = '' ) {
		$this->contents = $contents;
		$this->title = $title;
		$this->template = $template;
		$this->add_hooks();
	}

	protected function add_hooks() {
		add_action( 'pre_get_posts', array( $this, 'edit_query' ), 10, 1 );
		add_action( 'the_post', array( $this, 'set_post_contents' ), 10, 1 );
		add_filter( 'the_title', array( $this, 'get_title' ), 10, 2 );
		add_filter( 'single_post_title', array( $this, 'get_single_post_title' ), 10, 2 );
		if ( $this->template ) {
			add_filter( 'template_include', array( $this, 'override_template' ), 10, 1 );
		}
		add_filter( 'redirect_canonical', array( $this, 'override_redirect' ), 10, 2 );
	}

	// Edit WordPress's query so it finds our placeholder page
	public function edit_query( WP_Query $query ) {
		if ( isset( $query->query_vars[self::QUERY_VAR] ) ) {
			// make sure we get the right post
			$query->query_vars['post_type'] = self::POST_TYPE;
			$query->query_vars['p'] = self::get_post_id();

			// override any vars WordPress set based on the original query
			$query->is_single = TRUE;
			$query->is_singular = TRUE;
			$query->is_404 = FALSE;
			$query->is_home = FALSE;
		}
	}

	// Override the global $pages array
	public function set_post_contents( $post ) {
		global $pages;
		$pages = array( $this->contents );
	}

	// Set the title for the placeholder page
	public function get_title( $title, $post_id ) {
		if ( $post_id == self::get_post_id() ) {
			$title = $this->title;
		}
		return $title;
	}

	public function get_single_post_title( $title, $post ) {
		return $this->get_title( $title, $post->ID );
	}

	public function override_template( $template ) {
		if ( $this->template && file_exists( $template ) ) {
			return $this->template;
		}
		return $template;
	}

	public function override_redirect( $redirect_url, $requested_url ) {
		if ( get_query_var( 'WG_Route' ) ) {
			return FALSE;
		}
		return $redirect_url;
	}
}
