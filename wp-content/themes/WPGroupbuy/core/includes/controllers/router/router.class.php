<?php

class WG_Router extends WG_Router_Utility {
	const ROUTE_CACHE_OPTION = 'WG_Router_route_hash';
	private $routes = array();

	// The one instance of this singleton class
	private static $instance;

	public static function init() {
		self::$instance = self::get_instance();
	}

	// Add a new route
	public function add_route( $id, array $properties ) {
		if ( $route = $this->create_route( $id, $properties ) ) {
			$this->routes[$id] = $route;
		}
		return $route;
	}

	// Get a previously registered route
	public function get_route( $id ) {
		if ( isset( $this->routes[$id] ) ) {
			return $this->routes[$id];
		} else {
			return NULL;
		}
	}

	// Update each property included in $changes for the given route
	public function edit_route( $id, array $changes ) {
		if ( !isset( $this->routes[$id] ) ) {
			return;
		}
		foreach ( $changes as $key => $value ) {
			if ( $key != 'id' ) {
				try {
					$this->routes[$id]->set( $key, $value );
				} catch ( Exception $e ) {
					// Error setting the property. Failing silently
				}
			}
		}
	}

	// Get rid of the route with the given $id
	public function remove_route( $id ) {
		if ( isset( $this->routes[$id] ) ) {
			unset( $this->routes[$id] );
		}
	}

	public function get_url( $route_id, $arguments = array() ) {
		$route = $this->get_route( $route_id );
		if ( !$route ) {
			return home_url();
		} else {
			return $route->url( $arguments );
		}
	}

	public static function get_instance() {
		if ( !self::$instance ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	// Hook into WordPress
	private function __construct() {
		add_action( 'init', array( $this, 'generate_routes' ), 1000, 0 );
		add_action( 'parse_request', array( $this, 'parse_request' ), 10, 1 );
		add_filter( 'rewrite_rules_array', array( $this, 'add_rewrite_rules' ), 10, 1 );
		add_filter( 'query_vars', array( $this, 'add_query_vars' ), 10, 1 );
	}

	private function __clone() {
		trigger_error( __CLASS__.' may not be cloned', E_USER_ERROR );
	}

	private function __sleep() {
		trigger_error( __CLASS__.' may not be serialized', E_USER_ERROR );
	}

	// WordPress hook callbacks

	// Announce to other plugins that it's time to create rules
	public function generate_routes() {
		do_action( 'wg_router_generate_routes', $this );
		do_action( 'wg_router_alter_routes', $this );
		$rules = $this->rewrite_rules();
		if ( $this->hash( $rules ) != get_option( self::ROUTE_CACHE_OPTION ) ) {
			$this->flush_rewrite_rules();
		}
	}

	// Update WordPress's rewrite rules array with registered routes
	public function add_rewrite_rules( $rules ) {
		$new_rules = $this->rewrite_rules();
		update_option( self::ROUTE_CACHE_OPTION, $this->hash( $new_rules ), true );
		return $new_rules + $rules;
	}

	// Add all query vars from registered routes to WP's recognized query vars
	public function add_query_vars( $vars ) {
		$route_vars = $this->query_vars();
		$vars = array_merge( $vars, $route_vars );
		return $vars;
	}

	// If a callback is in order, call it.
	public function parse_request( WP $query ) {
		$this->redirect_placeholder($query);
		if ( $id = $this->identify_route( $query ) ) {
			$this->routes[$id]->execute( $query );
		}
	}

	// Redirect the placeholder page back to the front page
	protected function redirect_placeholder( $query ) {
		if ( !empty( $query->query_vars[WG_Router_Page::POST_TYPE]) ) {
			wp_redirect(home_url(), 303);
			exit();
		}
	}

	// Identify the route based on the request's query variables
	protected function identify_route( $query ) {
		if ( !isset( $query->query_vars[self::QUERY_VAR] ) ) {
			return NULL;
		}
		$id = $query->query_vars[self::QUERY_VAR];
		if ( !isset( $this->routes[$id] ) || !is_a( $this->routes[$id], 'WG_Route' ) ) {
			return NULL;
		}
		return $id;
	}

	protected function create_route( $id, array $properties ) {
		try {
			$route = new WG_Route( $id, $properties );
		} catch ( Exception $e ) {
			return NULL;
		}
		return $route;
	}

	// Get the array of rewrite rules from all registered routes
	protected function rewrite_rules() {
		$rules = array();
		foreach ( $this->routes as $id => $route ) {
			$rules = array_merge( $rules, $route->rewrite_rules() );
		}
		return $rules;
	}

	// Get an array of all query vars used by registered routes
	protected function query_vars() {
		$vars = array();
		foreach ( $this->routes as $id => $route ) {
			$vars = array_merge( $vars, $route->get_query_vars() );
		}
		$vars[] = self::QUERY_VAR;
		return $vars;
	}

	// Create a hash of the registered rewrite rules
	protected function hash( $rules ) {
		return md5( serialize( $rules ) );
	}

	// Tell WordPress to flush its rewrite rules
	protected function flush_rewrite_rules() {
		flush_rewrite_rules();
	}
}
