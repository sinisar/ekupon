<?php
/**
 * Created by PhpStorm.
 * User: sinisa
 * Date: 27/12/2018
 * Time: 21:33
 */

add_action( 'wg_init_controllers', array( 'WP_Groupbuy_MerchantVouchersReport', 'init' ), 55 );

class WP_Groupbuy_MerchantVouchersReport extends WP_Groupbuy_Controller
{
    const MERCHANT_VOUCHERS_REPORT_PATH_OPTION = 'wg_merchant_vouchers_report_path';
    const MERCHANT_VOUCHERS_REPORT_QUERY_VAR = 'wg_merchant_vouchers_report';
    const MERCHANT_VOUCHERS_REPORT_BASE = 'merchant_vouchers_report';
    private static $merchant_vouchers_report = self::MERCHANT_VOUCHERS_REPORT_BASE;
    protected static $settings_page;
    private static $instance;

    public static function init() {
        self::$settings_page = self::register_settings_page( self::MERCHANT_VOUCHERS_REPORT_BASE, self::__( 'Merchant Voucher payments report' ), self::__( 'Merchant vouchers' ), 10, FALSE, 'records', array(get_class(), "display_table"));
        self::$merchant_vouchers_report = get_option( self::MERCHANT_VOUCHERS_REPORT_PATH_OPTION, self::$merchant_vouchers_report );
        self::register_path_callback( self::$merchant_vouchers_report, array( get_class(), 'on_payment_importer_page' ), self::MERCHANT_VOUCHERS_REPORT_QUERY_VAR, self::MERCHANT_VOUCHERS_REPORT_BASE );
    }

    public static function display_table() {
        //Create an instance of our package class...
        $wp_list_table = new WP_Groupbuy_MerchantsVouchersReports_Table();
        //Fetch, prepare, sort, and filter our data...
        $wp_list_table->prepare_items();

        ?>


        <style type="text/css">
            #payment_deal_id-search-input, #purchase_id-search-input, #payment_account_id-search-input { width:5em; margin-left: 10px; float: left;}
        </style>
        <div class="wrap">
            <?php screen_icon(); ?>
            <h2 class="nav-tab-wrapper">
                <?php self::display_admin_tabs(); ?>
            </h2>

            <?php $wp_list_table->views() ?>
            <form id="payments-filter" method="get" style="margin-top:5px;">
                <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
                <input type="hidden" name="tab" value="<?php echo $_REQUEST['tab'] ?>" />
                <div>
                <p class="search-box payment_search">
                    <input style="width:300px;" type="text" id="payment_account_id-search-input" name="search_value" placeholder="<?php wpg_e( "Search by merchant name/ID" ) ?>" value="<?php echo (isset($_GET['search_value']) ? $_GET['search_value'] : "" )?>" >
                    <input type="submit" name="" id="search-submit" class="button" value="<?php self::_e( 'Search' ) ?>" >

                </p>
                </div>
                <?php $wp_list_table->display() ?>
            </form>
        </div>
        <?php
    }
}


if ( !class_exists( 'WP_List_Table' ) ) {
    require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
}
class WP_Groupbuy_MerchantsVouchersReports_Table extends WP_List_Table
{
    protected static $post_type = WP_Groupbuy_Merchant::POST_TYPE;

    function __construct()
    {
        global $status, $page;

        parent::__construct(array(
            'singular' => 'merchant voucher',
            'plural' => 'merchant vouchers',
            'ajax' => false
        ));

    }
    //  TODO: check if we really need this ?
    function get_views()
    {
        /*
        $status_links = array();
        $num_posts = wp_count_posts(self::$post_type, 'readable');
        $class = '';
        $allposts = '';

        $total_posts = array_sum((array)$num_posts);

        // Subtract post types that are not included in the admin all list.
        foreach (get_post_stati(array('show_in_admin_all_list' => false)) as $state)
            $total_posts -= $num_posts->$state;

        $class = empty($_REQUEST['post_status']) ? ' class="current"' : '';
        $status_links['all'] = "<a href='admin.php?page=wp-groupbuy/voucher_records&tab=wp-groupbuy/payment_records{$allposts}'$class>" . sprintf(_nx('All <span class="count">(%s)</span>', 'All <span class="count">(%s)</span>', $total_posts, 'posts'), number_format_i18n($total_posts)) . '</a>';

        foreach (get_post_stati(array('show_in_admin_status_list' => true), 'objects') as $status) {
            $class = '';

            $status_name = $status->name;

            if (empty($num_posts->$status_name))
                continue;

            if (isset($_REQUEST['post_status']) && $status_name == $_REQUEST['post_status'])
                $class = ' class="current"';

            // replace "Published" with "Complete".
            $label = wpg__('Payment&nbsp;') . str_replace('Published', 'Complete', translate_nooped_plural($status->label_count, $num_posts->$status_name));
            $status_links[$status_name] = "<a href='admin.php?page=wp-groupbuy/voucher_records&tab=wp-groupbuy/gift_records&post_status=$status_name'$class>" . sprintf($label, number_format_i18n($num_posts->$status_name)) . '</a>';
        }
*/
        return $status_links;
    }

    function extra_tablenav($which)
    {
        ?>
        <div class="alignleft actions">
            <?php
            if ('top' == $which && !is_singular()) {

                $this->months_dropdown(self::$post_type);

                submit_button(__('Filter'), 'secondary', false, false, array('id' => 'post-query-submit'));
            }
            ?>
        </div>
        <?php
    }


    // Text or HTML to be placed inside the column <td>
    function column_default($item, $column_name)
    {
        switch ($column_name) {
            default:
                return apply_filters('wg_mngt_merchant_vouchers_report_column_' . $column_name, $item);
        }
    }


    // Text to be placed inside the column <td> (movie title only)
    function column_merchant_id($item)
    {
        //print "item: ";
        //print_r($item);
        //break;
        //$this->display_table1();
        //return "$item->post_title";
        return $item->ID;
        $gift = WP_Groupbuy_Gift::get_instance($item->ID);
        $purchase_id = $gift->get_purchase_id();
        $purchase = WP_Groupbuy_Purchase::get_instance($purchase_id);
        if ($purchase) {
            $user_id = $purchase->get_original_user();
        } else {
            $user_id = 0;
        }
        $account_id = WP_Groupbuy_Account::get_account_id_for_user($user_id);

        //Build row actions
        $actions = array(
            'order' => sprintf('<a href="admin.php?page=wp-groupbuy/voucher_records&tab=wp-groupbuy/purchase_records&s=%s">Order</a>', $purchase_id),
            'purchaser' => sprintf('<a href="post.php?post=%s&action=edit">' . wpg__('Purchaser') . '</a>', $account_id),
        );

        //Return the title contents
        return sprintf('%1$s <span style="color:silver">(order&nbsp;id:%2$s)</span>%3$s',
            $item->post_title,
            $purchase_id,
            $this->row_actions($actions)
        );
    }

    function column_merchant_name($item)
    {
        return "$item->post_title";


        $gift = WP_Groupbuy_Gift::get_instance($item->ID);
        $purchase_id = $gift->get_purchase_id();
        $purchase = WP_Groupbuy_Purchase::get_instance($purchase_id);

        if ($purchase) {
            wg_formatted_money($purchase->get_total());
        } else {
            wg_formatted_money(0);
        }
    }

    function column_sold_vouchers($item)
    {
        //return "$item->post_title";

        //  $deal->get_number_of_purchases()
        // get_deal_ids
        $merchant = WP_Groupbuy_Merchant::get_instance($item->ID);
        //print_r($merchant);
        $dealIdsList = $merchant->get_deal_ids();
        //print_r($dealIdsList);
        $sumMerchantPurchases = 0;
        foreach ($dealIdsList as $deal => $item) {
            $deal = WP_Groupbuy_Deal::get_instance($item);
            /*
            print "item:";
            print_r($item);
            print "deal";
            print_r($deal);
            */
            $deal_purchase = $deal->get_number_of_purchases();
            $sumMerchantPurchases += $deal_purchase;
        }

        return "$sumMerchantPurchases";

        $gift = WP_Groupbuy_Gift::get_instance($item->ID);
        $purchase_id = $gift->get_purchase_id();
        $purchase = WP_Groupbuy_Purchase::get_instance($purchase_id);
        $products = $purchase ? $purchase->get_products() : array();

        $i = 0;
        $hover = "";
        $display = "";
        foreach ($products as $product => $item) {
            $i++;
            // Display deal name and link
            $hover .= '<p><strong><a href="' . get_edit_post_link($item['deal_id']) . '">' . get_the_title($item['deal_id']) . '</a></strong>&nbsp;<span style="color:silver">(id:' . $item['deal_id'] . ')</span>';
            $display .= '<p class="title-hover"><strong><a href="' . get_edit_post_link($item['deal_id']) . '">' . get_the_title($item['deal_id']) . '</a></strong>&nbsp;<span style="color:silver">(id:' . $item['deal_id'] . ')</span><br/>';
            // Build details
            $details = array(
                'Quantity' => $item['quantity'],
                'Unit Price' => wg_get_formatted_money($item['unit_price']),
                'Total' => wg_get_formatted_money($item['price'])
            );
            // Filter to add attributes, etc.
            $details = apply_filters('wg_purchase_deal_column_details', $details, $item, $products);
            // display details
            foreach ($details as $label => $value) {
                $hover .= '<br />' . $label . ': ' . $value;
            }
            // Build Payment methods
            $payment_methods = array();
            foreach ($item['payment_method'] as $method => $payment) {
                $payment_methods[] .= $method . ' &mdash; ' . wg_get_formatted_money($payment);
            }
            echo '</p>';
            if (count($products) > $i) {
                $hover .= '<span class="meta_box_block_divider"></span>';
            }
        }
        //Build row actions
        $actions = array(
            'detail' => $hover,
        );
        //Return the title contents
        return sprintf('%1$s %2$s', $display, $this->row_actions($actions));

    }
/*
    function column_payment_method($item)
    {
        return "$item->post_title";

        $gift_id = $item->ID;
        $gift = WP_Groupbuy_Gift::get_instance($gift_id);
        $purchase_id = $gift->get_purchase_id();
        $purchase = WP_Groupbuy_Purchase::get_instance($purchase_id);
        echo '<p>';
        echo '<strong>' . wpg__('Recipient:') . '</strong> <span class="editable_string wg_highlight" ref="' . $gift_id . '">' . $gift->get_recipient() . '</span><input type="text" id="' . $gift_id . '_recipient_input" class="option_recipient cloak" value="' . $gift->get_recipient() . '" />';
        echo '<br/><span style="color:silver">' . wpg__('code') . ': ' . $gift->get_coupon_code() . '</span>';
        echo '</p>';
        echo '<p><span id="' . $gift_id . '_activate_result"></span><a href="/wp-admin/edit.php?post_type=wg_purchase&resend_gift=' . $gift_id . '&_wpnonce=' . wp_create_nonce('resend_gift') . '" class="wg_resend_gift button" id="' . $gift_id . '_activate" ref="' . $gift_id . '">Resend</a></p>';
        return;

    }

    function column_payment_status($item)
    {
        return "$item->post_title";


        $gift = WP_Groupbuy_Gift::get_instance($item->ID);

        $claimed = $gift->get_claimed();

        if ($claimed) {
            $status = '<strong>' . wpg__('Complete') . '</strong><br/>';
        } else {
            $status = '<strong>' . wpg__('Pending Claim') . '</strong><br/>';
        }

        $date = $claimed ?: $item->post_date;

        $status .= '<span style="color:silver">';
        $status .= mysql2date(get_option('date_format') . ' - ' . get_option('time_format'), $date);
        $status .= '</span>';

        return $status;
    }
*/
    function column_manage($item) {
        return sprintf('<a href="admin.php?page=wp-groupbuy/voucher_records&tab=wp-groupbuy/merchant_vouchers_payments_report&s=%s">Voucher Payment Report</a>', $item->ID);
    }


    function get_columns()
    {
        $columns = array(
            'merchant_id' => wpg__('Merchant ID'),
            'merchant_name' => wpg__('Merchant name'),
            'sold_vouchers' => wpg__('Sold vouchers'),
            /*
            'voucher_id' => wpg__('Voucher ID'),
            'deal_id' => wpg__('Deal ID'),
            'payment_date' => wpg__('Payment date'),
            'payment_method' => wpg__('Payment method'),
            'payment_status' => wpg__('Voucher status'),
            //  TODO: export status -> oznaci da je ze placan na knof
            */
            'manage' => wpg__('Manage')
        );
        return apply_filters('wg_mngt_voucher_reports_columns', $columns);
    }

    function get_sortable_columns()
    {
        $sortable_columns = array();
        return apply_filters('wg_mngt_voucher_reports_sortable_columns', $sortable_columns);
    }

    function get_bulk_actions()
    {
        $actions = array();
        return apply_filters('wg_mngt_voucher_reports_bulk_actions', $actions);
    }


    // Prep data.
    function prepare_items()
    {
        // records per page to show
        $per_page = 25;

        // Define our column headers.
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();

        // Build an array to be used by the class for column headers.
        $this->_column_headers = array($columns, $hidden, $sortable);
        $filter = (isset($_REQUEST['post_status'])) ? $_REQUEST['post_status'] : 'all';
        //  TODO: filter by merchants that have sold (completed purchase process by customers) at least one voucher
        $args = array(
            'post_type' => WP_Groupbuy_Merchant::POST_TYPE,
            'posts_per_page' => $per_page,
            'paged' => $this->get_pagenum()
        );
        /*
        // Check purchases based on Deal ID
        if (isset($_GET['search_type']) && $_GET['search_type'] == 'deal_id' && $_GET['search_value'] != '') {

            if (WP_Groupbuy_Deal::POST_TYPE != get_post_type($_GET['search_value']))
                return; // not a valid search

            $purchase_ids = WP_Groupbuy_Purchase::get_purchases(array('deal' => $_GET['search_value']));

            $meta_query = array(
                'meta_query' => array(
                    array(
                        'key' => '_purchase',
                        'value' => $purchase_ids,
                        'type' => 'numeric',
                        'compare' => 'IN'
                    )
                ));
            $args = array_merge($args, $meta_query);
        }*/
        /*
        // Check payments based on Account ID
        if (isset($_GET['search_type']) && $_GET['search_type'] == 'account_id' && $_GET['search_value'] != '') {

            // if ( WP_Groupbuy_Account::POST_TYPE != get_post_type( $_GET['search_value'] ) )
            // 	return; // not a valid search

            // $purchase_ids = WP_Groupbuy_Purchase::get_purchases( array( 'account' => $_GET['search_value'] ) );

            // $meta_query = array(
            // 	'meta_query' => array(
            // 		array(
            // 			'key' => '_purchase',
            // 			'value' => $purchase_ids,
            // 			'type' => 'numeric',
            // 			'compare' => 'IN'
            // 		)
            // 	) );
            // $args = array_merge( $args, $meta_query );

            $author_id = WP_Groupbuy_Account::get_user_id_for_account($_GET['search_value']);
            $args = array_merge($args, array('author' => $author_id));

        }
        */
        //if ( WP_Groupbuy_Merchant::POST_TYPE != get_post_type( $_GET['search_value'] ) )
        // 	return; // not a valid search

        // Search
        if (isset($_GET['search_type']) && $_GET['search_type'] == 's' && $_GET['search_value'] != '') {
            $args = array_merge($args, array('s' => $_GET['search_value']));
        }
        // Filter by date
        if (isset($_GET['m']) && $_GET['m'] != '') {
            $args = array_merge($args, array('m' => $_GET['m']));
        }
         // print "args: ";
         // print_r($args);
        //break;

        $voucherMerchants = new WP_Query($args);

        //  load all payments for each

        //print_r($voucherPayments);

        // Sorted data to the items property, where it can be used by the rest of the class.
        $this->items = apply_filters('wg_mngt_voucher_reports_items', $voucherMerchants->posts);

        $this->set_pagination_args(array(
            'total_items' => $voucherMerchants->found_posts,
            'per_page' => $per_page,
            'total_pages' => $voucherMerchants->max_num_pages
        ));
    }
}

