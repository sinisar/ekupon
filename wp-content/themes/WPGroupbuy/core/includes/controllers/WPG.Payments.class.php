<?php

add_action( 'wg_init_controllers', array( 'WP_Groupbuy_Payments', 'init' ), 65 );

class WP_Groupbuy_Payments extends WP_Groupbuy_Controller {

	const PAYMENTS_PATH_OPTION = 'wg_payments_path';
	const PAYMENTS_QUERY_VAR = 'wg_view_payments';
	private static $payments_path = 'payments';
	private static $instance;
	protected static $settings_page;

	public static function init() {
		self::$settings_page = self::register_settings_page( 'payment_records', self::__( 'View Payment Records' ), self::__( 'Payments' ), 8, FALSE, 'records', array( get_class(), 'display_table' ) );
		self::$payments_path = get_option( self::PAYMENTS_PATH_OPTION, self::$payments_path );
		self::register_path_callback( self::$payments_path, array( get_class(), 'on_payment_page' ), self::PAYMENTS_QUERY_VAR, 'payments' );
		add_action( 'init', array( get_class(), 'wp_init' ) );
	}

	// Init all other Payments classes
	public static function wp_init() {
		WP_Groupbuy_Payments_Gateway::init();
		WP_Groupbuy_Payments_Transaction_Update::init();
	}

	public static function register_settings_fields() {
		$page = WP_Groupbuy_UI::get_settings_page();
		$section = 'WG_URL_paths';
		add_settings_section( $section, self::__( 'Change your URL Paths' ), array( get_class(), 'display_payments_paths_section' ), $page );

		// Settings
		register_setting( $page, self::PAYMENTS_PATH_OPTION );
		add_settings_field( self::PAYMENTS_PATH_OPTION, self::__( 'Payments Dashboard Path' ), array( get_class(), 'display_payments_path' ), $page, $section );
	}


	public static function display_payments_paths_section() {
		WP_Groupbuy_Controller::flush_rewrite_rules();
	}

	public static function display_payments_path() {
		echo trailingslashit( get_home_url() ) . ' <input type="text" name="'.self::PAYMENTS_PATH_OPTION.'" id="'.self::PAYMENTS_PATH_OPTION.'" value="' . esc_attr( self::$payments_path ) . '"  size="40" /><br />';
	}

	public static function get_url() {
		if ( self::using_permalinks() ) {
			return trailingslashit( home_url( self::$payments_path ) );
		} else {
			return add_query_arg( self::PAYMENTS_QUERY_VAR, 1, home_url() );
		}
	}

	public static function on_payment_page() {
		self::get_instance(); // make sure the class is instantiated
	}

	private function __clone() {
		trigger_error( __CLASS__.' may not be cloned', E_USER_ERROR );
	}
	private function __sleep() {
		trigger_error( __CLASS__.' may not be serialized', E_USER_ERROR );
	}
	public static function get_instance() {
		if ( !( self::$instance && is_a( self::$instance, __CLASS__ ) ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}
	private function __construct() {
		self::do_not_cache(); // never cache the account pages

	}


	public static function display_table() {
		//Create an instance of our package class...
		$wp_list_table = new WP_Groupbuy_Payments_Table();
		//Fetch, prepare, sort, and filter our data...
		$wp_list_table->prepare_items();

		wg_head_script_variables();

		?>
		<script type="text/javascript">
			jQuery(document).ready(function($){
				jQuery(".show_payment_detail").live('click', function(e) {
					e.preventDefault();
					var payment_id = $(this).attr("id");
					$('#payment_detail_'+payment_id).toggle();
				});

				$('a.mark_complete').live('click', function(e) {
					e.preventDefault();
					
					$this = $(this)
					
					$this.after(wg_loading);

					var purchase_id = $(this).data("id");

					// Get new select list
					$.post( wg_ajax_url, { action: 'wg_mark_payment_complete', id: purchase_id },
						function( result ) {
							$this.next().remove();
							if ( result == '0' ) {
								$this.parents('td').children('span.payment_status').text('<?php self::_e("Complete") ?>');
								$this.parent().remove();
							} else {
								alert('Could NOT mark payment as paid.\n' + result);
							};
						}
					);
				});

			});
		</script>
		<style type="text/css">
			#payment_deal_id-search-input, #payment_id-search-input, #payment_purchase_id-search-input, #payment_account_id-search-input { width:5em; margin-left: 10px;}
		</style>
		<div class="wrap">
			<?php screen_icon(); ?>
			<h2 class="nav-tab-wrapper">
				<?php self::display_admin_tabs(); ?>
			</h2>

			 <?php $wp_list_table->views() ?>
			<form id="payments-filter" method="get" style="margin-top:5px;">
				<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
                                <input type="hidden" name="tab" value="<?php echo $_REQUEST['tab'] ?>" />
				
				<p class="search-box payment_search">
					<input style="width:150px;" type="text" id="payment_account_id-search-input" name="search_value" placeholder="<?php wpg_e( "Quick Search" ) ?>" value="<?php echo (isset($_GET['search_value']) ? $_GET['search_value'] : "" )?>">
                                        <?php
                                        $selectSearchBox = array(
                                            array("value" => "user_email", "text" => self::__( 'User email' ), "selected" => "selected"),
                                            array("value" => "account_id", "text" => self::__( 'Account ID' ), "selected" => ""),
                                            array("value" => "purchase_id", "text" => self::__( 'Purchase ID' ), "selected" => ""),
                                            array("value" => "deal_id", "text" => self::__( 'Deal ID' ), "selected" => ""),
                                            array("value" => "s", "text" => self::__( 'Payment ID' ), "selected" => ""),
                                           
                                        );
                                        ?>
                                        <select name="search_type">
                                        <?php
                                        foreach($selectSearchBox as $el) {
                                             $el['selected'] = @$_GET['search_type'] == $el['value'] ? 'selected' : '';
                                             echo '<option '.$el['selected'].' value="'.$el['value'].'">'.$el['text'].'</option>';
                                        }
                                        ?>
                                        </select>
					<input type="submit" name="" id="search-submit" class="button" value="<?php self::_e( 'Search' ) ?>">
				</p>
				<?php $wp_list_table->display() ?>
			</form>
		</div>
		<?php
	}

}

if ( !class_exists( 'WP_List_Table' ) ) {
	require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
}
class WP_Groupbuy_Payments_Table extends WP_List_Table {
	protected static $post_type = WP_Groupbuy_Payment::POST_TYPE;

	function __construct() {
		global $status, $page;

		//Set parent defaults
		parent::__construct( array(
				'singular' => 'payment',
				'plural' => 'payments',
				'ajax' => false
			) );

	}

	function get_views() {

		$status_links = array();
		$num_posts = wp_count_posts( self::$post_type, 'readable' );
		$class = '';
		$allposts = '';

		$total_posts = array_sum( (array) $num_posts );

		// Subtract post types that are not included in the admin all list.
		foreach ( get_post_stati( array( 'show_in_admin_all_list' => false ) ) as $state )
			$total_posts -= $num_posts->$state;

		$class = empty( $_REQUEST['post_status'] ) ? ' class="current"' : '';
		$status_links['all'] = "<a href='admin.php?page=wp-groupbuy/voucher_records&tab=wp-groupbuy/payment_records{$allposts}'$class>" . sprintf( _nx( 'All <span class="count">(%s)</span>', 'All <span class="count">(%s)</span>', $total_posts, 'posts' ), number_format_i18n( $total_posts ) ) . '</a>';

		foreach ( get_post_stati( array( 'show_in_admin_status_list' => true ), 'objects' ) as $status ) {
			$class = '';

			$status_name = $status->name;

			if ( empty( $num_posts->$status_name ) )
				continue;

			if ( isset( $_REQUEST['post_status'] ) && $status_name == $_REQUEST['post_status'] )
				$class = ' class="current"';

			// replace "Published" with "Complete".
			$label = str_replace( 'Published', 'Complete', translate_nooped_plural( $status->label_count, $num_posts->$status_name ) );
			$status_links[$status_name] = "<a href='admin.php?page=wp-groupbuy/voucher_records&tab=wp-groupbuy/payment_records&post_status=$status_name'$class>" . sprintf( $label, number_format_i18n( $num_posts->$status_name ) ) . '</a>';
		}

		return $status_links;
	}

	function extra_tablenav( $which ) {
		?>
		<div class="alignleft actions"> <?php
		if ( 'top' == $which && !is_singular() ) {

			$this->months_dropdown( self::$post_type );

			submit_button( __( 'Filter' ), 'secondary', false, false, array( 'id' => 'post-query-submit' ) );
		} ?>
		<a href="<?php wg_credit_purchases_report_url() ?>"  class="button"><?php wpg_e('View Credit Payments') ?></a>
		</div>
		<?php
	}


	// Text or HTML to be placed inside the column <td>
	function column_default( $item, $column_name ) {
		switch ( $column_name ) {
		default:
			return apply_filters( 'wg_mngt_payments_column_'.$column_name, $item );
		}
	}


	// Text to be placed inside the column <td> (movie title only)
	function column_title( $item ) {
		$payment = WP_Groupbuy_Payment::get_instance( $item->ID );
		$account = $payment->get_account();
		if ( !is_a( $account, 'WP_Groupbuy_Account' ) ) {
			if ( get_post_status( $item->ID ) == 'trash' ) {
				return sprintf( wpg__( 'Want to empty the trash? <a href="%s">Empty</a>.' ), 'wp-admin/edit.php?post_status=trash&post_type=wg_payment' );
			} else {
				return sprintf( wpg__( 'ERROR: No account associated.<br/>Want to remove this record? <a href="%s">Trash it</a>.' ), get_edit_post_link( $item->ID ) );
			}
		}
		$purchase_id = $payment->get_purchase();

		//Build row actions
		$actions = array(
			'order'    => sprintf( '<a href="admin.php?page=wp-groupbuy/voucher_records&tab=wp-groupbuy/purchase_records&s=%s">Order</a>', $purchase_id ),
			'account'    => sprintf( '<a href="post.php?post=%s&action=edit">'.wpg__( 'Account' ).'</a>', $account->get_ID() ),
		);

		//Return the title contents
		return sprintf( '%1$s <span style="color:silver">(order&nbsp;id:%2$s)</span>%3$s',
			$item->post_title,
			$purchase_id,
			$this->row_actions( $actions )
		);
	}

	function column_total( $item ) {
		$payment = WP_Groupbuy_Payment::get_instance( $item->ID );
		$payment_method = $payment->get_payment_method();
		$deals = $payment->get_deals();
		$purchase_id = $payment->get_purchase();
		$purchase = WP_Groupbuy_Purchase::get_instance( $purchase_id );
		if ( !$purchase ) {
			return;
		}

		if ( empty( $deals ) ) {
			echo '<em>'.wpg__( 'Order Total' ).': '.wg_get_formatted_money( $purchase->get_total() ).'</em>';
			return;
		}

		// Find the total via calculated costs per item within the payment, since shipping/tax and other extras are added to these totals and not the payment->amount.
		if ( $payment_method == WP_Groupbuy_Affiliate_Credit_Payments::PAYMENT_METHOD ) {
			$total = $payment->get_amount();
		} else {
			$total = 0;
			foreach ( $deals as $deal_id => $items ) {
				foreach ( $items as $item ) {
					foreach ( $item['payment_method'] as $method => $payment ) {
						if ( $method == $payment_method ) {
							$total += $payment;
						}
					}
				}
			}
		}

		$sub_total = wpg__( 'Subtotal' ).': '.wg_get_formatted_money( $purchase->get_subtotal() ).'<br/>';
		$shipping = wpg__( 'Shipping' ).': '.wg_get_formatted_money( $purchase->get_shipping_total() ).'<br/>';
		$tax = wpg__( 'Tax' ).': '.wg_get_formatted_money( $purchase->get_tax_total() ).'<br/>';
		$payment_total = '<strong>'.wpg__( 'Payment Total' ).':</strong> '.wg_get_formatted_money( $total ).'<br/>';
		$order_total = '<em>'.wpg__( 'Order Total' ).': '.wg_get_formatted_money( $purchase->get_total() ).'</em>';
		
		//Build row actions
		$actions = array(
			'detail'    => $sub_total. $shipping. $tax .$order_total,
		);

		//Return the title contents
		return sprintf( '%1$s %2$s', $payment_total, $this->row_actions( $actions ) );
	}
        
        function column_total_sales($item_id) {
            $payment = WP_Groupbuy_Payment::get_instance( $item_id  );
            $payment_method = $payment->get_payment_method();
            $deals = $payment->get_deals();
            $purchase_id = $payment->get_purchase();

            if ( !$purchase_id ) {
            	error_log("Payment " . $payment->get_id() . " has no corresponding purchase record " );
                return 0;
            } else {
            	$purchase = WP_Groupbuy_Purchase::get_instance( $purchase_id );
            }

            if ( empty( $deals ) ) {
                return 0;
            }

            // Find the total via calculated costs per item within the payment, since shipping/tax and other extras are added to these totals and not the payment->amount.
            if ( $payment_method == WP_Groupbuy_Affiliate_Credit_Payments::PAYMENT_METHOD ) {
                    $total = $payment->get_amount();
            } else {
                    $total = 0;
                    foreach ( $deals as $deal_id => $items ) {
                            foreach ( $items as $item ) {
                                    foreach ( $item['payment_method'] as $method => $payment ) {
                                            if ( $method == $payment_method ) {
                                                    $total += $payment;
                                            }
                                    }
                            }
                    }
            }
            return $total;
        }
	function column_account( $item ) {
		$payment = WP_Groupbuy_Payment::get_instance( $item->ID );
		$account = $payment->get_account();
		if ( !is_a( $account, 'WP_Groupbuy_Account' ) ) {
			return;
		}
		//Build row actions
		$actions = array(
			'account'    => sprintf( '<a href="post.php?post=%s&action=edit">'.wpg__( 'Account' ).'</a>', $account->get_ID() ),
			'user'    => sprintf( '<a href="user-edit.php?user_id=%s">'.wpg__( 'User' ).'</a>', WP_Groupbuy_Account::get_user_id_for_account( $account->get_ID() ) ),
		);

		//Return the title contents
		return sprintf( '%1$s <span style="color:silver">(id:%2$s)</span>%3$s',
			$account->get_name(),
			$account->get_ID(),
			$this->row_actions( $actions )
		);
	}

	function column_deals( $item ) {
		$payment = WP_Groupbuy_Payment::get_instance( $item->ID );
		$deals = $payment->get_deals();
		if ( empty( $deals ) ) {
			wpg_e( 'No Data.' );
			return;
		}
		$i = 0;
                $hover = "";
                $display = "";
		foreach ( $deals as $deal_id => $items ) {
			foreach ( $items as $item ) {
				$i++;
				// Display deal name and link
				$hover .=  '<p><strong><a href="'.get_edit_post_link( $item['deal_id'] ).'">'.get_the_title( $item['deal_id'] ).'</a></strong>&nbsp;<span style="color:silver">(id:'.$item['deal_id'].')</span>';
                                $display .=  '<p class="title-hover"><strong><a href="'.get_edit_post_link( $item['deal_id'] ).'">'.get_the_title( $item['deal_id'] ).'</a></strong>&nbsp;<span style="color:silver">(id:'.$item['deal_id'].')</span><br/>';
				// Build details
				$details = array(
					__('Quantity') => $item['quantity'],
					__('Unit Price') => wg_get_formatted_money( $item['unit_price'] ),
					__('Total') => wg_get_formatted_money( $item['price'] )
				);
				// Filter to add attributes, etc.
				$details = apply_filters( 'wg_purchase_deal_column_details', $details, $item, $items );
				// display details
				foreach ( $details as $label => $value ) {
					$hover .=  '<br />'.$label.': '.$value;
				}
				// Build Payment methods
				$payment_methods = array();
				foreach ( $item['payment_method'] as $method => $payment ) {
					$payment_methods[] .= $method.' &mdash; '.wg_get_formatted_money( $payment );
				}
				// Display payment methods
				$hover .=  '<br/><strong>'.wpg__( 'Payment Methods' ).':</strong><br/>';
				$hover .=  implode( '<br/>', $payment_methods );
				$hover .=  '</p>';
				if ( ( count( $items )+count( $deals )-1 ) > $i ) {
					$hover .=  '<span class="meta_box_block_divider"></span>';
				}
			}
		}
                
		//Build row actions
		$actions = array(
			'detail'    => $hover,
		);
		//Return the title contents
		return sprintf( '%1$s %2$s', $display, $this->row_actions( $actions ) );

	}

	function column_data( $item ) {
		$payment = WP_Groupbuy_Payment::get_instance( $item->ID );
		$method = $payment->get_payment_method();
		//Build row actions
		$actions = array(
			'detail'    => sprintf( '<a href="#" class="show_payment_detail" id="%s">'.wpg__( 'Transaction Log' ).'</a><br/><textarea cols="20" rows="2" id="payment_detail_%s" style="display:none;">%s</textarea>', $item->ID, $item->ID, print_r( $payment->get_data(), TRUE ) )
		);
		//Return the title contents
		return sprintf( '%1$s %2$s', $method, $this->row_actions( $actions ) );
	}

	function column_status( $item ) {
		$payment = WP_Groupbuy_Payment::get_instance( $item->ID );
		$status = '<span class="payment_status">' . ucfirst( str_replace( 'publish', 'complete', $item->post_status ) ) . '</span>';
		$status .= '<br/><span style="color:silver">';
		$status .= mysql2date( get_option( 'date_format' ).' - '.get_option( 'time_format' ), $item->post_date );
		$status .= '</span>';
	
		if ( $item->post_status !== 'publish' ) {
			$actions['mark_complete'] = '<a href="#' .$item->ID . '" data-id="' .$item->ID . '" class="mark_complete">Mark Complete</a>';
			$status .= $this->row_actions( $actions );
		}

		return $status;
	}


	function get_columns() {
		$columns = array(
			'status'  => wpg__( 'Status' ),
			'title'  => wpg__( 'Payment' ),
			'total'  => wpg__( 'Totals' ),
			'deals'  => wpg__( 'Deals' ),
			'account'    => wpg__( 'Account' ),
			'data'  => wpg__( 'Data' )
		);
		return apply_filters( 'wg_mngt_payments_columns', $columns );
	}

	function get_sortable_columns() {
		$sortable_columns = array(
		);
		return apply_filters( 'wg_mngt_paymentssortable_columns', $sortable_columns );
	}

	function get_bulk_actions() {
		$actions = array();
		return apply_filters( 'wg_mngt_paymentsbulk_actions', $actions );
	}


	// Prep data.
	function prepare_items() {

		// records per page to show
		$per_page = 25;

		// Define our column headers.
		$columns = $this->get_columns();
		$hidden = array();
		$sortable = $this->get_sortable_columns();


		// Build an array to be used by the class for column headers.
		$this->_column_headers = array( $columns, $hidden, $sortable );

		$filter = ( isset( $_REQUEST['post_status'] ) ) ? $_REQUEST['post_status'] : 'all';
		$args=array(
			'post_type' => WP_Groupbuy_Payment::POST_TYPE,
			'post_status' => $filter,
			'posts_per_page' => $per_page,
			'paged' => $this->get_pagenum(),
			'post_status' => array( WP_Groupbuy_Payment::STATUS_PENDING, WP_Groupbuy_Payment::STATUS_AUTHORIZED, WP_Groupbuy_Payment::STATUS_COMPLETE, WP_Groupbuy_Payment::STATUS_PARTIAL ),
		);
		// Check payments based on Deal ID
		if ( isset( $_GET['search_type'] ) && $_GET['search_type'] == 'deal_id' && $_GET['search_value'] != '') {

			if ( WP_Groupbuy_Deal::POST_TYPE != get_post_type( $_GET['search_value'] ) )
				return; // not a valid search

			$purchases = WP_Groupbuy_Purchase::get_purchases( array( 'deal' => $_GET['search_value'] ) );
			$payment_ids = array();
			foreach ( $purchases as $purchase_id ) {
				$payment_ids = array_merge( $payment_ids, WP_Groupbuy_Payment::get_payments_for_purchase( $purchase_id ) );
			}
			$args = array_merge( $args, array( 'post__in' => $payment_ids ) );
		}
		// Check payments based on Purchase ID
		if ( isset( $_GET['search_type'] ) && $_GET['search_type'] == 'purchase_id' && $_GET['search_value'] != '' ) {

			if ( WP_Groupbuy_Purchase::POST_TYPE != get_post_type( $_GET['search_value'] ) )
				return; // not a valid search

			$payment_ids = WP_Groupbuy_Payment::get_payments_for_purchase( $_GET['search_value'] );
			if ( empty( $payment_ids ) )
				return;

			$args = array_merge( $args, array( 'post__in' => $payment_ids ) );
		}
		// Check payments based on Account ID
		if ( isset( $_GET['search_type'] ) && $_GET['search_type'] == 'account_id' && $_GET['search_value'] != '' ) {
            $args = $this->search_by_account($args, $_GET['search_value']);
		}

        //  search by user email
        if( isset($_GET['search_type']) && $_GET['search_type'] == 'user_email' && $_GET['search_value'] != '' ) {
            $user_email = $_GET['search_value'];
            $user = get_user_by('email', $user_email);

            if ($user != null) {
                $account = WP_Groupbuy_Account::get_instance($user->ID);
                $account_id = $account->get_account_id_for_user($user->ID);
                $args = $this->search_by_account($args, $account_id);

            }
        }

		// Search
		if ( isset( $_GET['search_type'] ) && $_GET['search_type'] == 's' && $_GET['search_value'] != '' ) {
			$args = array_merge( $args, array( 's' => $_GET['search_value'] ) );
		}
		// Filter by date
		if ( isset( $_GET['m'] ) && $_GET['m'] != '' ) {
			$args = array_merge( $args, array( 'm' => $_GET['m'] ) );
		}
		$payments = new WP_Query( $args );

		// Sorted data to the items property, where it can be used by the rest of the class.
		$this->items = apply_filters( 'wg_mngt_paymentsitems', $payments->posts );

		// Register our pagination options & calculations.
		$this->set_pagination_args( array(
				'total_items' => $payments->found_posts,
				'per_page'  => $per_page,
				'total_pages' => $payments->max_num_pages
			) );
	}
	
    function search_by_account ( $args, $account_id ) {
        if ( WP_Groupbuy_Account::POST_TYPE != get_post_type( $account_id ) )
            return; // not a valid search

        $purchase_ids = WP_Groupbuy_Purchase::get_purchases( array( 'account' => $account_id ) );

        $meta_query = array(
            'meta_query' => array(
                array(
                    'key' => '_purchase_id',
                    'value' => $purchase_ids,
                    'type' => 'numeric',
                    'compare' => 'IN'
                )
            ) );
        $args = array_merge( $args, $meta_query );
        return $args;
    }
}
class WP_Groupbuy_Payments_Gateway extends WP_Groupbuy_Controller {

	const PAYMENTS_GATEWAY_STATUS_PATH_OPTION = 'wg_payments_gateway_status_path';
	const PAYMENTS_GATEWAY_STATUS_QUERY_VAR = 'wg_payments_gateway_status';
	private static $payments_status_path = 'payments/status';
	private static $instance;
	private static $payment_transaction_id = 0;

	public static function init() {
		self::$payments_status_path = get_option( self::PAYMENTS_GATEWAY_STATUS_PATH_OPTION, self::$payments_status_path );
		self::register_path_callback( self::$payments_status_path, array( get_class(), 'on_payments_gateway_status_page' ), self::PAYMENTS_GATEWAY_STATUS_QUERY_VAR, 'payments/view' );
	}

	public static function register_settings_fields() {
		$page = WP_Groupbuy_UI::get_settings_page();
		$section = 'WG_URL_paths';
		// Settings
		register_setting( $page, self::PAYMENTS_GATEWAY_STATUS_PATH_OPTION );
		add_settings_field( self::PAYMENTS_GATEWAY_STATUS_PATH_OPTION, self::__( 'Paymnet gateway status path' ), array( get_class(), 'display_payments_gateway_status_path' ), $page, $section );
	}

	public static function get_url() {
		if ( self::using_permalinks() ) {
			return trailingslashit( home_url( self::$payments_path ) );
		} else {
			return add_query_arg( self::PAYMENTS_GATEWAY_STATUS_QUERY_VAR, 1, home_url() );
		}
	}

	public static function display_payments_gateway_status_path() {
		echo trailingslashit( get_home_url() ) . ' <input type="text" name="' . self::PAYMENTS_GATEWAY_STATUS_PATH_OPTION . '" id="' . self::PAYMENTS_GATEWAY_STATUS_PATH_OPTION . '" value="' . esc_attr( self::$payments_status_path ) . '" size="40"/><br />';
	}


	public static function on_payments_gateway_status_page() {
		self::get_instance(); // make sure the class is instantiated
	}

	private function __clone() {
		trigger_error( __CLASS__.' may not be cloned', E_USER_ERROR );
	}
	private function __sleep() {
		trigger_error( __CLASS__.' may not be serialized', E_USER_ERROR );
	}

	public static function get_instance() {
		if ( !( self::$instance && is_a( self::$instance, __CLASS__ ) ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	private function __construct() {
		self::do_not_cache(); // never cache the account pages
		add_filter( 'template_include', array( get_class(), 'override_template' ) );
		add_action( 'pre_get_posts', array( $this, 'edit_query' ), 10, 1 );
		add_action( 'the_post', array( $this, 'view_payment_status' ), 10, 1 );
		add_filter( 'the_title', array( $this, 'get_title' ), 10, 2 );
		add_filter( 'wg_payments_status_panes', array( $this, 'get_panes' ), 0, 2 );
	}

	public static function override_template( $template ) {
		if(WP_Groupbuy_Payment_Transaction::is_payment_transaction_query()) {
			$template = self::locate_template( array(
				'payments/single.php',
			), $template );
			return $template;
		}
	}

	// Edit the query on the profile edit page to select the user's account.
	public function edit_query( WP_Query $query ) {
        self::$payment_transaction_id = isset( $_GET['txId'] ) ? $_GET['txId'] : '';
        if ( isset( $query->query_vars[self::PAYMENTS_GATEWAY_STATUS_QUERY_VAR] ) && $query->query_vars[self::PAYMENTS_GATEWAY_STATUS_QUERY_VAR] ) {
			$query->query_vars['post_type'] = WP_Groupbuy_Payment_Transaction::POST_TYPE;
			if(self::$payment_transaction_id != '') {
				WP_Groupbuy_Payment_Transaction::set_transaction_id(self::$payment_transaction_id);
				$query->query_vars['transaction_id'] = self::$payment_transaction_id;
			}
		}
	}

	// Update the global $pages array with the HTML for the page.
	public function view_payment_status( $post ) {
		if ( $post->post_type == WP_Groupbuy_Payment_Transaction::POST_TYPE ) {
			remove_filter( 'the_content', 'wpautop' );

			$panes = apply_filters( 'wg_payments_status_panes', array() );
			uasort( $panes, array( get_class(), 'sort_by_weight' ) );
			$view = self::load_view_to_string( 'payments/view', array(
				'panes' => $panes,
			) );
			global $pages;
			$pages = array( $view );
		}
	}

	// Filter 'the_title' to display the title of the page rather than the user name
	public function get_title(  $title, $post_id  ) {
		$post = &get_post( $post_id );
		if ( $post->post_type == WP_Groupbuy_Payment_Transaction::POST_TYPE ) {
			return self::__( "Your Payment status" );
		}
		return $title;
	}

	public function get_panes( array $panes) {
		$panes['payment_status'] = array(
			'weight' => 0,
			'body' => self::load_view_to_string('payments/status', array(
				'transaction_id' => self::$payment_transaction_id)),
		);
		return $panes;
	}
}

class WP_Groupbuy_Payments_Transaction_Update extends WP_Groupbuy_Controller {

	const PAYMENTS_GATEWAY_PAYMENT_TRANSACTION_UPDATE_PATH_OPTION = 'wg_payments_gateway_payment_transaction_update_path';
	const PAYMENTS_GATEWAY_PAYMENT_TRANSACTION_UPDATE_QUERY_VAR = 'wg_payments_gateway_payment_transaction_update';
	private static $payments_transaction_update_path = 'payments/payment_transaction_update';
	private static $instance;
	private static $payment_transaction_update_id = 0;

	public static function init() {
		self::$payments_transaction_update_path = get_option( self::PAYMENTS_GATEWAY_PAYMENT_TRANSACTION_UPDATE_PATH_OPTION, self::$payments_transaction_update_path );
		self::register_path_callback( self::$payments_transaction_update_path, array( get_class(), 'on_payments_gateway_payment_transaction_update_page' ), self::PAYMENTS_GATEWAY_PAYMENT_TRANSACTION_UPDATE_QUERY_VAR, 'payments/view' );
	}

	public static function register_settings_fields() {
		$page = WP_Groupbuy_UI::get_settings_page();
		$section = 'WG_URL_paths';
		// Settings
		register_setting( $page, self::PAYMENTS_GATEWAY_PAYMENT_TRANSACTION_UPDATE_PATH_OPTION );
		add_settings_field( self::PAYMENTS_GATEWAY_PAYMENT_TRANSACTION_UPDATE_PATH_OPTION, self::__( 'Paymnet gateway payment transaction update path' ), array( get_class(), 'display_payments_gateway_payment_transaction_update_path' ), $page, $section );
	}

	public static function get_url() {
		if ( self::using_permalinks() ) {
			return trailingslashit( home_url( self::$payments_path ) );
		} else {
			return add_query_arg( self::PAYMENTS_GATEWAY_PAYMENT_TRANSACTION_UPDATE_QUERY_VAR, 1, home_url() );
		}
	}

	public static function display_payments_gateway_payment_transaction_update_path() {
		echo trailingslashit( get_home_url() ) . ' <input type="text" name="' . self::PAYMENTS_GATEWAY_PAYMENT_TRANSACTION_UPDATE_PATH_OPTION . '" id="' . self::PAYMENTS_GATEWAY_PAYMENT_TRANSACTION_UPDATE_PATH_OPTION . '" value="' . esc_attr( self::$payments_transaction_update_path ) . '" size="40"/><br />';
	}

	public static function on_payments_gateway_payment_transaction_update_page() {
		self::get_instance(); // make sure the class is instantiated
	}

	private function __clone() {
		trigger_error( __CLASS__.' may not be cloned', E_USER_ERROR );
	}
	private function __sleep() {
		trigger_error( __CLASS__.' may not be serialized', E_USER_ERROR );
	}

	public static function get_instance() {

		if ( !( self::$instance && is_a( self::$instance, __CLASS__ ) ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

    private function __construct() {
//        error_log("MegaPOS Update Request Data: " . print_r($_REQUEST, true));

        self::do_not_cache(); // never cache the account pages
        self::$payment_transaction_update_id = isset( $_GET['txId'] ) ? $_GET['txId'] : '';
        $allowed_remote_address = array('127.0.0.1', '78.153.42.28', '89.142.195.145');
        $allowed_remote_range = array('213.143.80.144 - 213.143.80.159', '193.105.127.1 - 193.105.127.254');
        $user_remote_address = trim($_SERVER['REMOTE_ADDR']);
        $ip_address_checked = 0;


//        error_log("MegaPOS Update Server Data: " . print_r($_SERVER, true));
//        error_log("MegaPOS Update Transaction ID: " . self::$payment_transaction_update_id);
        //	check if IP is in range
        for($i = 0; $i < count($allowed_remote_range); $i++) {
            $ip_range = $allowed_remote_range[$i];
            list($lower, $upper) = explode(' - ', $ip_range, 2);

            $user_ip_long = ip2long($user_remote_address);
            $lower_ip_long = ip2long(trim($lower));
            $upper_ip_long = ip2long(trim($upper));

//            error_log("MegaPOS Update IP check No. {$i}: {$user_ip_long} <= {$upper_ip_long} && {$lower_ip_long} <=
//            {$user_ip_long}");
            if ($user_ip_long <= $upper_ip_long && $lower_ip_long <= $user_ip_long) {
                $ip_address_checked = 1;
                break;
            }
        }

//        error_log("MegaPOS Update IP Addrress Checked: " . $ip_address_checked);
        //	if is in range (ip_address_checked = 1) or inside list of allowed remote ip addresses
        if(in_array($user_remote_address, $allowed_remote_address) || $ip_address_checked == 1) {
            if(self::$payment_transaction_update_id != ''){
//                error_log("MegaPOS Updating Transaction with ID: " . self::$payment_transaction_update_id);
                $megaPOS = WP_Groupbuy_MegaPOS_Card::get_instance();
                if ( $megaPOS->update_transaction_status(self::$payment_transaction_update_id) ) {
                    status_header(200);
                    exit();
                }
            }
        }
        status_header( 404 );
        exit();

        //add_filter( 'template_include', array( get_class(), 'override_template' ) );
        //add_action( 'pre_get_posts', array( $this, 'edit_query' ), 10, 1 );
        //add_action( 'the_post', array( $this, 'view_payment_status' ), 10, 1 );
        //add_filter( 'the_title', array( $this, 'get_title' ), 10, 2 );
        //add_filter( 'wg_payments_status_panes', array( $this, 'get_panes' ), 0, 2 );
    }

	public static function override_template( $template ) {
		print "here";
		//break;
		if(WP_Groupbuy_Payment_Transaction::is_payment_transaction_query()) {
			$template = self::locate_template( array(
				'payments/single.php',
			), $template );
			return $template;
		}
	}

	// Edit the query on the profile edit page to select the user's account.
	public function edit_query( WP_Query $query ) {

		if ( isset( $query->query_vars[self::PAYMENTS_GATEWAY_PAYMENT_TRANSACTION_UPDATE_QUERY_VAR] ) && $query->query_vars[self::PAYMENTS_GATEWAY_PAYMENT_TRANSACTION_UPDATE_QUERY_VAR] ) {
			$query->query_vars['post_type'] = WP_Groupbuy_Payment_Transaction::POST_TYPE;
			if(self::$payment_transaction_update_id != '') {
				WP_Groupbuy_Payment_Transaction::set_transaction_id(self::$payment_transaction_update_id);
				$query->query_vars['transaction_id'] = self::$payment_transaction_update_id;
			}
		}
	}

	// Update the global $pages array with the HTML for the page.
	public function view_payment_status( $post ) {
		if ( $post->post_type == WP_Groupbuy_Payment_Transaction::POST_TYPE ) {
			remove_filter( 'the_content', 'wpautop' );

			$panes = apply_filters( 'wg_payments_status_panes', array() );
			uasort( $panes, array( get_class(), 'sort_by_weight' ) );
			$view = self::load_view_to_string( 'payments/view', array(
				'panes' => $panes,
			) );
			global $pages;
			$pages = array( $view );
		}
	}

	// Filter 'the_title' to display the title of the page rather than the user name
	public function get_title(  $title, $post_id  ) {
		$post = &get_post( $post_id );
		if ( $post->post_type == WP_Groupbuy_Payment_Transaction::POST_TYPE ) {
			return self::__( "Your Payment status" );
		}
		return $title;
	}

	public function get_panes( array $panes) {
		$panes['payment_status'] = array(
			'weight' => 0,
			'body' => self::load_view_to_string('payments/status', array(
				'transaction_id' => self::$payment_transaction_update_id)),
		);
		return $panes;
	}
}
