<?php

if ( !class_exists( 'WP_List_Table' ) ) {
	require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
}

class WP_Groupbuy_Accounts_Table extends WP_List_Table {
	protected static $post_type = WP_Groupbuy_Account::POST_TYPE;
	function __construct() {
		global $status, $page;
		// Set parent defaults
		parent::__construct( array(
				'singular' => 'account',     // singular name of the listed records
				'plural' => 'account', // plural name of the listed records
				'ajax' => false     // does this table support ajax?
			) );
	}
	function get_views() {
		$status_links = array();
		$num_posts = wp_count_posts( self::$post_type, 'readable' );
		$class = '';
		$allposts = '';
		$total_posts = array_sum( (array) $num_posts );
		// Subtract post types that are not included in the admin all list.
		foreach ( get_post_stati( array( 'show_in_admin_all_list' => false ) ) as $state )
			$total_posts -= $num_posts->$state;
		$class = empty( $_REQUEST['post_status'] ) ? ' class="current"' : '';
		$status_links['all'] = "<a href='admin.php?page=wp-groupbuy/voucher_records&tab=wp-groupbuy/account_records{$allposts}'$class>" . sprintf( _nx( 'All <span class="count">(%s)</span>', 'All <span class="count">(%s)</span>', $total_posts, 'posts' ), number_format_i18n( $total_posts ) ) . '</a>';
		foreach ( get_post_stati( array( 'show_in_admin_status_list' => true ), 'objects' ) as $status ) {
			$class = '';
			$status_name = $status->name;
			if ( empty( $num_posts->$status_name ) )
				continue;
			if ( isset( $_REQUEST['post_status'] ) && $status_name == $_REQUEST['post_status'] )
				$class = ' class="current"';
			// replace "Published" with "Complete".
			$label = str_replace( array( 'Published', 'Trash' ), array( 'Active', 'Suspended' ), translate_nooped_plural( $status->label_count, $num_posts->$status_name ) );
			$status_links[$status_name] = "<a href='admin.php?page=wp-groupbuy/voucher_records&tab=wp-groupbuy/account_records&post_status=$status_name'$class>" . sprintf( $label, number_format_i18n( $num_posts->$status_name ) ) . '</a>';
		}
		return $status_links;
	}
	function extra_tablenav( $which ) {
		?>
		<div class="alignleft actions"> <?php
		if ( 'top' == $which && !is_singular() ) {
			$this->months_dropdown( self::$post_type );
			submit_button( __( 'Filter' ), 'secondary', false, false, array( 'id' => 'post-query-submit' ) );
		} ?>
		<a href="<?php wg_accounts_report_url() ?>"  class="button"><?php wpg_e('Accounts Report') ?></a>
		<a href="<?php wg_credits_report_url() ?>"  class="button"><?php wpg_e('Credits Report') ?></a>
		</div>
<?php
	}
	function column_default( $item, $column_name ) {
		switch ( $column_name ) {
		default:
			return apply_filters( 'wg_mngt_account_column_'.$column_name, $item ); // do action for those columns that are filtered in
		}
	}
	function column_title( $item ) {
		$account_id = $item->ID;
		$account = WP_Groupbuy_Account::get_instance_by_id( $account_id );
		// Build row actions
		$actions = array(
			'payments'    => sprintf( '<a href="admin.php?page=wp-groupbuy/voucher_records&tab=wp-groupbuy/payment_records&account_id=%s">'.wpg__('Payments').'</a>', $account_id ),
			'purchases'    => sprintf( '<a href="admin.php?page=wp-groupbuy/voucher_records&tab=wp-groupbuy/purchase_records&account_id=%s">'.wpg__('Purchases').'</a>', $account_id ),
			'vouchers'    => sprintf( '<a href="admin.php?page=wp-groupbuy/voucher_records&account_id=%s">'.wpg__('Vouchers').'</a>', $account_id ),
			'gifts'    => sprintf( '<a href="admin.php?page=wp-groupbuy/voucher_records&tab=wp-groupbuy/gift_records&account_id=%s">'.wpg__('Gifts').'</a>', $account_id )
		);
		// Return the title contents
		return sprintf( '%1$s <span style="color:silver">(account&nbsp;id:%2$s)</span>%3$s',
			$item->post_title,
			$item->ID,
			$this->row_actions( $actions )
		);
	}
	function column_username( $item ) {
		$account_id = $item->ID;
		$account = WP_Groupbuy_Account::get_instance_by_id( $account_id );
		$user_id = $account->get_user_id_for_account( $account_id );
		$user = get_userdata( $user_id );
		
		$name = ( $account->is_suspended() ) ? '<span style="color:#BC0B0B">Suspended:</span> ' . $account->get_name() : $account->get_name() ;
		// Build row actions
		$suspend_text = ( $account->is_suspended() ) ? wpg__('Revert Suspension'): wpg__('Suspend');
		$actions = array(
			'edit'    => sprintf( '<a href="post.php?post=%s&action=edit">Manage</a>', $account_id ),
			'user'    => sprintf( '<a href="user-edit.php?user_id=%s">User</a>', $user_id ),
			'trash'    => '<span id="'.$account_id.'_suspend_result"></span><a href="javascript:void(0)" class="wg_suspend" id="'.$account_id.'_suspend" ref="'.$account_id.'">'.$suspend_text.'</a>'
		);
		// Return the title contents
		return sprintf( '%1$s %2$s <span style="color:silver">(user&nbsp;id:%3$s)</span>%4$s',
			get_avatar( @$user->user_email, '35' ),
			$name,
			$user_id,
			$this->row_actions( $actions )
		);
	}
	function column_address( $item ) {
		$account_id = $item->ID;
		$account = WP_Groupbuy_Account::get_instance_by_id( $account_id );
		echo wg_format_address( $account->get_address(), 'string', '<br />' );
	}
	function column_credits( $item ) {
		$account_id = $item->ID;
		$account = WP_Groupbuy_Account::get_instance_by_id( $account_id );
		$credits = $account->get_credit_balance( WP_Groupbuy_Affiliates::CREDIT_TYPE );
		if ( !$credits ) $credits = '0';
		echo $credits;
	}
	function get_columns() {
		$columns = array(
			'username' => wpg__('Name'),
			'title'  => wpg__('Account'),
			'address'  => wpg__('Address'),
			'credits'  => wpg__('Credits')
		);
		return apply_filters( 'wg_mngt_accounts_columns', $columns );
	}
	function get_sortable_columns() {
		$sortable_columns = array(
		);
		return apply_filters( 'wg_mngt_accounts_sortable_columns', $sortable_columns );
	}
	// Bulk actions are an associative array in the format
	function get_bulk_actions() {
		$actions = array();
		return apply_filters( 'wg_mngt_accounts_bulk_actions', $actions );
	}
	// Prep data.
	function prepare_items() {
		// records per page to show
		$per_page = 25;
		// Define our column headers.
		$columns = $this->get_columns();
		$hidden = array();
		$sortable = $this->get_sortable_columns();
		// Build an array to be used by the class for column headers.
		$this->_column_headers = array( $columns, $hidden, $sortable );
		$filter = ( isset( $_REQUEST['post_status'] ) ) ? $_REQUEST['post_status'] : 'all';
		$args=array(
			'post_type' => WP_Groupbuy_Account::POST_TYPE,
			'post_status' => $filter,
			'posts_per_page' => $per_page,
			'paged' => $this->get_pagenum()
		);
		// Search
		if ( isset( $_GET['search_type'] ) && $_GET['search_type'] == 's' && $_GET['search_value'] != '' ) {
			$args = array_merge( $args, array( 'p' => $_GET['search_value'] ) );
		}
		// Filter by date
		if ( isset( $_GET['m'] ) && $_GET['m'] != '' ) {
			$args = array_merge( $args, array( 'm' => $_GET['m'] ) );
		}
		$accounts = new WP_Query( $args );
		// Sorted data to the items property, where it can be used by the rest of the class.
		$this->items = apply_filters( 'wg_mngt_accounts_items', $accounts->posts );
		// Register our pagination options & calculations.
		$this->set_pagination_args( array(
				'total_items' => $accounts->found_posts,
				'per_page'  => $per_page,
				'total_pages' => $accounts->max_num_pages
			) );
	}
}
