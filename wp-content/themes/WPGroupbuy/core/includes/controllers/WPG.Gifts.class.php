<?php

add_action( 'wg_init_controllers', array( 'WP_Groupbuy_Gifts', 'init' ), 45 );

class WP_Groupbuy_Gifts extends WP_Groupbuy_Controller {
	const REDEMPTION_PATH_OPTION = 'wg_gift_redemption';
	const REDEMPTION_QUERY_VAR = 'wg_gift_redemption';
	const FORM_ACTION = 'wg_gift_redemption';
	private static $redemption_path = 'gifts';
	protected static $settings_page;
	private static $instance;

	public static function init() {
		self::register_payment_pane();
		self::register_review_pane();
		self::$redemption_path = get_option( self::REDEMPTION_PATH_OPTION, self::$redemption_path );
		add_action( 'completing_checkout', array( get_class(), 'save_recipient_for_purchase' ), 10, 1 );
		add_action( 'payment_complete', array( get_class(), 'activate_gifts_for_purchase' ), 10, 1 );
		add_action( 'admin_init', array( get_class(), 'register_settings_fields' ), 10, 0 );
		self::register_path_callback( self::$redemption_path, array( get_class(), 'on_redemption_page' ), self::REDEMPTION_QUERY_VAR, 'gifts' );

		// Admin
		self::$settings_page = self::register_settings_page( 'gift_records', self::__( 'Manage Gift Orders' ), self::__( 'Gift Orders' ), 9.1, FALSE, 'records', array( get_class(), 'display_table' ) );
		add_action( 'parse_request', array( get_class(), 'manually_resend_gift' ), 1, 0 );

		if ( !empty( $_REQUEST['gift_email'] ) ) {
			add_filter( 'wg_get_name', array( get_class(), 'wg_get_name' ), 10, 1 );
			add_filter( 'wg_login_required', array( get_class(), 'gift_login_check' ) );
			add_filter( 'wg_get_voucher_permalink', array( get_class(), 'wg_gift_view_link' ), 10, 2 );
			add_filter( 'wg_get_vouchers_url', array( get_class(), 'wg_gift_view_link' ), 10, 2 );
			add_filter( 'wg_get_voucher_used_url', array( get_class(), 'wg_gift_view_link' ), 10, 2 );
			add_filter( 'wg_get_voucher_expired_url', array( get_class(), 'wg_gift_view_link' ), 10, 2 );
			add_filter( 'wg_get_voucher_active_url', array( get_class(), 'wg_gift_view_link' ), 10, 2 );
			add_action( 'pre_get_posts', array( get_class(), 'filter_voucher_query' ), 10, 1 );
		}
	}

	public static function gift_login_check( $required ) {
		if (WP_Groupbuy_Voucher::is_voucher_query() && !empty( $_REQUEST['gift_code'] ) ) {
			$required = false;
		}
		return $required;
	}

	public static function manually_resend_gift( $gift_id = null ) {
		if ( !current_user_can( 'edit_posts' ) ) {
			return; // security check
		}
		if ( isset( $_REQUEST['resend_gift'] ) && $_REQUEST['resend_gift'] != '' ) {
			if ( wp_verify_nonce( $_REQUEST['_wpnonce'], 'resend_gift' ) ) {
				$gift_id = $_REQUEST['resend_gift'];
				$recipient = $_REQUEST['recipient'];
			}
		}
		if ( is_numeric( $gift_id ) ) {
			$gift = WP_Groupbuy_Gift::get_instance( $gift_id );
			if ( is_a( $gift, 'WP_Groupbuy_Gift' ) ) {
				if ( $recipient != '' ) {
					$gift->set_recipient( $recipient );
				}
				do_action( 'wg_gift_notification', array( 'gift' => $gift ) );
				return;
			}
		}
	}

	public static function register_settings_fields() {
		$page = WP_Groupbuy_UI::get_settings_page();
		$section = 'wg_cart_paths';

		// Settings
		register_setting( $page, self::REDEMPTION_PATH_OPTION );
		add_settings_field( self::REDEMPTION_PATH_OPTION, self::__( 'Gift Redemption Path' ), array( get_class(), 'display_path' ), $page, $section );
	}

	public static function display_path() {
		echo trailingslashit( get_home_url() ) . ' <input type="text" name="'.self::REDEMPTION_PATH_OPTION.'" id="'.self::REDEMPTION_PATH_OPTION.'" value="' . esc_attr( self::$redemption_path ) . '"  size="40" /><br />';
	}


	// Register action hooks for displaying and processing the payment page
	private static function register_payment_pane() {
		add_filter( 'wg_checkout_panes_'.WP_Groupbuy_Checkouts::PAYMENT_PAGE, array( get_class(), 'display_payment_page' ), 10, 2 );
		add_action( 'wg_checkout_action_'.WP_Groupbuy_Checkouts::PAYMENT_PAGE, array( get_class(), 'process_payment_page' ), 15, 1 );
	}

	private static function register_review_pane() {
		add_filter( 'wg_checkout_panes_'.WP_Groupbuy_Checkouts::REVIEW_PAGE, array( get_class(), 'display_review_page' ), 10, 2 );
	}

	private static function register_confirmation_pane() {
		add_filter( 'wg_checkout_panes_'.WP_Groupbuy_Checkouts::CONFIRMATION_PAGE, array( get_class(), 'display_confirmation_page' ), 10, 2 );
	}

	public static function display_payment_page( $panes, $checkout ) {
		$fields = array(
			'is_gift' => array(
				'type' => 'checkbox',
				'label' => self::__( 'Is this purchase a gift for someone?' ),
				'weight' => 0,
				'default' => ( ( isset( $checkout->cache['gift_recipient'] )&&$checkout->cache['gift_recipient'] )||isset( $_GET['gifter'] )&&$_GET['gifter'] )?TRUE:FALSE,
				'value' => 'is_gift',
			),
			'recipient' => array(
				'type' => 'text',
				'label' => self::__( "Recipient's Email Address" ),
				'weight' => 12,
				'default' => isset( $checkout->cache['gift_recipient'] )?$checkout->cache['gift_recipient']:'',
			),
			'message' => array(
				'type' => 'textarea',
				'label' => self::__( "Your Message" ),
				'weight' => 10,
				'default' => isset( $checkout->cache['gift_message'] )?$checkout->cache['gift_message']:'',
			),
            'send_gift_to_me' => array(
                'type' => 'checkbox',
                'label' => self::__( 'Send this gift to me' ),
                'weight' => 4,
                'default' => ( ( isset( $checkout->cache['gift_recipient'] )&&$checkout->cache['gift_recipient'] )||isset( $_GET['gift_to_me'] )&&$_GET['gift_to_me'] )?TRUE:FALSE,
                'value' => 'send_gift_to_me',
            ),
		);
		$fields = apply_filters( 'wg_checkout_fields_gifting', $fields );
		uasort( $fields, array( get_class(), 'sort_by_weight' ) );
		$panes['gifting'] = array(
			'weight' => 5,
			'body' => self::load_view_to_string( 'checkout/gifting', array( 'fields' => $fields ) ),
		);
		return $panes;
	}

	public static function process_payment_page( WP_Groupbuy_Checkouts $checkout ) {
		$valid = TRUE;
		if ( isset( $_POST['wg_gifting_is_gift'] ) && $_POST['wg_gifting_is_gift'] == 'is_gift' ) {

			// Get current user email
			$user = get_userdata( get_current_user_id() );
			$user_email = $user->user_email;

			// Confirm an email was added
			if ( !isset( $_POST['wg_gifting_recipient'] ) || !$_POST['wg_gifting_recipient'] ) {
				self::set_message( "Recipient's Email Address is required for gift purchases", self::MESSAGE_STATUS_ERROR );
				$valid = FALSE;
			}
			//Check email validity
			elseif ( !sanitize_email( $_POST['wg_gifting_recipient'] ) ) {
				self::set_message( "Zahtevan je veljaven prejemnikov e-naslov", self::MESSAGE_STATUS_ERROR );
				$valid = FALSE;
			}
			//Check to see if they gave the same email that's tied to their account - only if not checked SEND TO ME.
			elseif ( !isset( $_POST['wg_gifting_send_gift_to_me'] ) || !$_POST['wg_gifting_send_gift_to_me'] ) {
                if( $user_email == sanitize_email( $_POST['wg_gifting_recipient'] ) ) {
                    self::set_message( self::__( 'You deserve it but you may not gift yourself this purchase.' ), self::MESSAGE_STATUS_ERROR );
                    $valid = FALSE;
                }
			}
            else {
			    $valid = TRUE;
            }
		}
		if ( !$valid ) {
			$checkout->mark_page_incomplete( WP_Groupbuy_Checkouts::PAYMENT_PAGE );
		} elseif ( isset( $_POST['wg_gifting_is_gift'] ) && $_POST['wg_gifting_is_gift'] == 'is_gift' ) {
			$checkout->cache['gift_recipient'] = $_POST['wg_gifting_recipient'];
			$checkout->cache['gift_message'] = $_POST['wg_gifting_message'];
		}
	}


	// Display the final review pane
	public static function display_review_page( $panes, $checkout ) {
		if ( !empty($checkout->cache['gift_recipient']) ) {
			$panes['gifting'] = array(
				'weight' => 5,
				'body' => self::load_view_to_string( 'checkout/gifting-review', array( 'recipient' => $checkout->cache['gift_recipient'], 'message' => $checkout->cache['gift_message'] ) ),
			);
		}
		return $panes;
	}

	// Display the confirmation page
	public static function display_confirmation_page( $panes, $checkout ) {
		return $panes;
	}

	public static function save_recipient_for_purchase( $checkout ) {
		if ( !empty($checkout->cache['gift_recipient']) && !empty($checkout->cache['purchase_id']) ) {
			$purchase = WP_Groupbuy_Purchase::get_instance( $checkout->cache['purchase_id'] );
			$gift_id = WP_Groupbuy_Gift::new_gift( $purchase->get_id(), $checkout->cache['gift_recipient'], $checkout->cache['gift_message'] );
		}
	}

	public static function activate_gifts_for_purchase( WP_Groupbuy_Payment $payment ) {
        $purchase_id = $payment->get_purchase();
        if ($purchase_id) {
            $gift_id = WP_Groupbuy_Gift::get_gift_for_purchase( $purchase_id );
            if ( $gift_id ) {
                $gift = WP_Groupbuy_Gift::get_instance( $gift_id );
                $gift->activate();
            }
        } else {
            error_log("Purchase for payment (id=". $payment->get_id() .") not found.");
        }
	}

	public static function get_url( $gift = null ) {
		if ( $gift ) {
			$query_vars = array(
				'gift_email' => $gift->get_recipient(),
				'gift_code' => $gift->get_coupon_code(),
			);
			$url = add_query_arg( $query_vars, wg_get_vouchers_url() );
		} elseif ( self::using_permalinks() ) {
				$url = trailingslashit( home_url() ).trailingslashit( self::$redemption_path );
		} else {
			$url = add_query_arg( self::REDEMPTION_QUERY_VAR, 1, home_url() );
		}
		return $url;
	}
	public static function on_redemption_page() {
		self::get_instance();
	}

	private function __clone() {
		trigger_error( __CLASS__.' may not be cloned', E_USER_ERROR );
	}
	private function __sleep() {
		trigger_error( __CLASS__.' may not be serialized', E_USER_ERROR );
	}
	public static function get_instance() {
		if ( !( self::$instance && is_a( self::$instance, __CLASS__ ) ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	private function __construct() {
		self::do_not_cache(); // never cache the redemption page
		add_action( 'pre_get_posts', array( $this, 'edit_query' ), 10, 1 );
		add_action( 'the_post', array( $this, 'view_redemption_form' ), 10, 1 );
		add_filter( 'the_title', array( $this, 'get_title' ), 10, 2 );
	}

	public static function filter_voucher_query( $query ) {
		
		if ( !is_admin() && get_query_var('post_type') == WP_Groupbuy_Voucher::POST_TYPE && $query->is_main_query() ) {

			$email = @$_REQUEST['gift_email'];
			$code = @$_REQUEST['gift_code'];

			if ( !sanitize_email( $email ) ) {
				self::set_message( self::__( 'A valid email address is required.' ), self::MESSAGE_STATUS_ERROR );
				$valid = FALSE;
			}
			if ( !$code ) {
				self::set_message( self::__( 'A valid coupon code is required.' ), self::MESSAGE_STATUS_ERROR );
				$valid = FALSE;
			}

			if ( $code ) {

				$gift_id = WP_Groupbuy_Gift::validate_gift( $email, $code );
				
				if ( !$gift_id ) {
					self::set_message( self::__( 'Invalid code: please confirm the email address and coupon code.' ), self::MESSAGE_STATUS_ERROR );
				} else {

					$gift = WP_Groupbuy_Gift::get_instance( $gift_id );
					
					/* If Gift is still pending, then set it to be completed */
					
					if ( !$gift->get_claimed() ) {
						$gift->set_claimed();
					}

					$purchase_id = $gift->get_purchase_id();
					
					if ( empty( $purchase_id ) ) {
						return;
					}

					$query->query_vars['meta_query'] = array();
					
					$query->query_vars['meta_key'] = '_purchase_id';
					$query->query_vars['meta_value'] = $purchase_id;
				}
			}
		}
	}

	public static function wg_gift_view_link( $link, $voucher_id = 0 ) {
		if ( !empty( $_REQUEST['gift_email'] ) && !empty( $_REQUEST['gift_code'] ) ) {
			$link .= '?gift_email=' . urlencode( $_REQUEST['gift_email'] ) . '&gift_code=' . urlencode( $_REQUEST['gift_code'] );
		}

		return $link;
	}

	public static function wg_get_name( $name ) {
		if ( strlen( $name ) < 2 ) {
			$name = $_REQUEST['gift_email'];
		}
		return $name;
	}

	// Edit the query on the redemption page to select the user's account.
	public function edit_query( WP_Query $query ) {
		// we only care if this is the query to show a the edit profile form
		if ( isset( $query->query_vars[self::REDEMPTION_QUERY_VAR] ) && $query->query_vars[self::REDEMPTION_QUERY_VAR] ) {
			// use the user's account as something benign and guaranteed to exist
			$query->query_vars['post_type'] = WP_Groupbuy_Account::POST_TYPE;
			$query->query_vars['p'] = WP_Groupbuy_Account::get_account_id_for_user();
		}
	}

	// Update the global $pages array with the HTML for the page.
	public function view_redemption_form( $post ) {
		if ( $post->post_type == WP_Groupbuy_Account::POST_TYPE ) {
			remove_filter( 'the_content', 'wpautop' );
			$user = wp_get_current_user();
			$view = self::load_view_to_string( 'gift/redemption', array( 'email' => $user->user_email ) );
			global $pages;
			$pages = array( $view );
		}
	}


	// Filter 'the_title' to display the title of the page rather than the user name
	public function get_title(  $title, $post_id  ) {
		$post = get_post( $post_id );
		if ( $post->post_type == WP_Groupbuy_Account::POST_TYPE ) {
			return self::__( "Redeem a Gift Certificate" );
		}
		return $title;
	}

	public static function display_table() {
		//Create an instance of our package class...
		$wp_list_table = new WP_Groupbuy_Gifts_Table();
		//Fetch, prepare, sort, and filter our data...
		$wp_list_table->prepare_items();

?>
		<script type="text/javascript" charset="utf-8">
			jQuery(document).ready(function($){
				jQuery(".wg_resend_gift").click(function(event) {
					event.preventDefault();
						if(confirm("Are you sure?")){
							var $link = $( this ),
							gift_id = $link.attr( 'ref' ),
							recipient = $( "#"+gift_id+"_recipient_input" ).val(),
							url = $link.attr( 'href' );
							$( "#"+gift_id+"_activate" ).fadeOut('slow');
							$.post( url, { resend_gift: gift_id, recipient: recipient },
								function( data ) {
										$( "#"+gift_id+"_activate_result" ).append( '<?php self::_e( 'Resent' ) ?>' ).fadeIn();
									}
								);
						} else {
							// nothing to do.
						}
				});
				jQuery(".editable_string").click(function(event) {
					event.preventDefault();
					var gift_id = $( this ).attr( 'ref' );
					$( '#' + gift_id + '_recipient_input').show();
					$(this).hide();
				});

			});
		</script>
		<style type="text/css">
			#payment_deal_id-search-input, #purchase_id-search-input, #payment_account_id-search-input { width:5em; margin-left: 10px;}
		</style>
		<div class="wrap">
			<?php screen_icon(); ?>
			<h2 class="nav-tab-wrapper">
				<?php self::display_admin_tabs(); ?>
			</h2>

			 <?php $wp_list_table->views() ?>
			<form id="payments-filter" method="get" style="margin-top:5px;">
				<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
                                <input type="hidden" name="tab" value="<?php echo $_REQUEST['tab'] ?>" />
				<p class="search-box payment_search">
					<input style="width:100px;" type="text" id="payment_account_id-search-input" name="search_value" placeholder="<?php wpg_e( "Quick Search" ) ?>" value="<?php echo (isset($_GET['search_value']) ? $_GET['search_value'] : "" )?>">
                                        <?php
                                        $selectSearchBox = array(
                                            array("value" => "non_select", "text" => self::__( '- Select -' ), "selected" => "selected"),
                                            array("value" => "account_id", "text" => self::__( 'Account ID' ), "selected" => ""),
                                            array("value" => "deal_id", "text" => self::__( 'Deal ID' ), "selected" => ""),
                                            array("value" => "s", "text" => self::__( 'Purchase ID' ), "selected" => ""),
                                           
                                        );
                                        ?>
                                        <select name="search_type">
                                        <?php
                                        foreach($selectSearchBox as $el) {
                                             $el['selected'] = @$_GET['search_type'] == $el['value'] ? 'selected' : '';
                                             echo '<option '.$el['selected'].' value="'.$el['value'].'">'.$el['text'].'</option>';
                                        }
                                        ?>
                                        </select>
					<input type="submit" name="" id="search-submit" class="button" value="<?php self::_e( 'Search' ) ?>">
				</p>
				<?php $wp_list_table->display() ?>
			</form>
		</div>
		<?php
	}
}



if ( !class_exists( 'WP_List_Table' ) ) {
	require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
}
class WP_Groupbuy_Gifts_Table extends WP_List_Table {
	protected static $post_type = WP_Groupbuy_Gift::POST_TYPE;

	function __construct() {
		global $status, $page;

		parent::__construct( array(
				'singular' => 'gift',
				'plural' => 'gifts',
				'ajax' => false
			) );

	}

	function get_views() {

		$status_links = array();
		$num_posts = wp_count_posts( self::$post_type, 'readable' );
		$class = '';
		$allposts = '';

		$total_posts = array_sum( (array) $num_posts );

		// Subtract post types that are not included in the admin all list.
		foreach ( get_post_stati( array( 'show_in_admin_all_list' => false ) ) as $state )
			$total_posts -= $num_posts->$state;

		$class = empty( $_REQUEST['post_status'] ) ? ' class="current"' : '';
		$status_links['all'] = "<a href='admin.php?page=wp-groupbuy/voucher_records&tab=wp-groupbuy/payment_records{$allposts}'$class>" . sprintf( _nx( 'All <span class="count">(%s)</span>', 'All <span class="count">(%s)</span>', $total_posts, 'posts' ), number_format_i18n( $total_posts ) ) . '</a>';

		foreach ( get_post_stati( array( 'show_in_admin_status_list' => true ), 'objects' ) as $status ) {
			$class = '';

			$status_name = $status->name;

			if ( empty( $num_posts->$status_name ) )
				continue;

			if ( isset( $_REQUEST['post_status'] ) && $status_name == $_REQUEST['post_status'] )
				$class = ' class="current"';

			// replace "Published" with "Complete".
			$label = wpg__( 'Payment&nbsp;' ).str_replace( 'Published', 'Complete', translate_nooped_plural( $status->label_count, $num_posts->$status_name ) );
			$status_links[$status_name] = "<a href='admin.php?page=wp-groupbuy/voucher_records&tab=wp-groupbuy/gift_records&post_status=$status_name'$class>" . sprintf( $label, number_format_i18n( $num_posts->$status_name ) ) . '</a>';
		}

		return $status_links;
	}

	function extra_tablenav( $which ) {
?>
		<div class="alignleft actions">
<?php
		if ( 'top' == $which && !is_singular() ) {

			$this->months_dropdown( self::$post_type );

			submit_button( __( 'Filter' ), 'secondary', false, false, array( 'id' => 'post-query-submit' ) );
		}
?>
		</div>
<?php
	}


	// Text or HTML to be placed inside the column <td>
	function column_default( $item, $column_name ) {
		switch ( $column_name ) {
		default:
			return apply_filters( 'wg_mngt_gifts_column_'.$column_name, $item );
		}
	}


	// Text to be placed inside the column <td> (movie title only)
	function column_title( $item ) {
		$gift = WP_Groupbuy_Gift::get_instance( $item->ID );
		$purchase_id = $gift->get_purchase_id();
		$purchase = WP_Groupbuy_Purchase::get_instance( $purchase_id );
		if ( $purchase ) {
			$user_id = $purchase->get_original_user();
		} else {
			$user_id = 0;
		}
		$account_id = WP_Groupbuy_Account::get_account_id_for_user( $user_id );

		//Build row actions
		$actions = array(
			'order'    => sprintf( '<a href="admin.php?page=wp-groupbuy/voucher_records&tab=wp-groupbuy/purchase_records&s=%s">Order</a>', $purchase_id ),
			'purchaser'    => sprintf( '<a href="post.php?post=%s&action=edit">'.wpg__( 'Purchaser' ).'</a>', $account_id ),
		);

		//Return the title contents
		return sprintf( '%1$s <span style="color:silver">(order&nbsp;id:%2$s)</span>%3$s',
			$item->post_title,
			$purchase_id,
			$this->row_actions( $actions )
		);
	}

	function column_total( $item ) {
		$gift = WP_Groupbuy_Gift::get_instance( $item->ID );
		$purchase_id = $gift->get_purchase_id();
		$purchase = WP_Groupbuy_Purchase::get_instance( $purchase_id );

		if ( $purchase ) {
			wg_formatted_money( $purchase->get_total() );
		} else {
			wg_formatted_money(0);
		}
	}

	function column_deals( $item ) {
		$gift = WP_Groupbuy_Gift::get_instance( $item->ID );
		$purchase_id = $gift->get_purchase_id();
		$purchase = WP_Groupbuy_Purchase::get_instance( $purchase_id );
		$products = $purchase?$purchase->get_products():array();

		$i = 0;
                $hover = "";
                $display = "";
		foreach ( $products as $product => $item ) {
			$i++;
			// Display deal name and link
			$hover .= '<p><strong><a href="'.get_edit_post_link( $item['deal_id'] ).'">'.get_the_title( $item['deal_id'] ).'</a></strong>&nbsp;<span style="color:silver">(id:'.$item['deal_id'].')</span>';
                        $display .= '<p class="title-hover"><strong><a href="'.get_edit_post_link( $item['deal_id'] ).'">'.get_the_title( $item['deal_id'] ).'</a></strong>&nbsp;<span style="color:silver">(id:'.$item['deal_id'].')</span><br/>';
			// Build details
			$details = array(
				'Quantity' => $item['quantity'],
				'Unit Price' => wg_get_formatted_money( $item['unit_price'] ),
				'Total' => wg_get_formatted_money( $item['price'] )
			);
			// Filter to add attributes, etc.
			$details = apply_filters( 'wg_purchase_deal_column_details', $details, $item, $products );
			// display details
			foreach ( $details as $label => $value ) {
				$hover .= '<br />'.$label.': '.$value;
			}
			// Build Payment methods
			$payment_methods = array();
			foreach ( $item['payment_method'] as $method => $payment ) {
				$payment_methods[] .= $method.' &mdash; '.wg_get_formatted_money( $payment );
			}
			echo '</p>';
			if ( count( $products ) > $i ) {
				$hover .= '<span class="meta_box_block_divider"></span>';
			}
		}
                //Build row actions
		$actions = array(
			'detail'    => $hover,
		);
		//Return the title contents
		return sprintf( '%1$s %2$s', $display, $this->row_actions( $actions ) );

	}

	function column_gift( $item ) {
		$gift_id = $item->ID;
		$gift = WP_Groupbuy_Gift::get_instance( $gift_id );
		$purchase_id = $gift->get_purchase_id();
		$purchase = WP_Groupbuy_Purchase::get_instance( $purchase_id );
		echo '<p>';
		echo '<strong>'.wpg__( 'Recipient:' ).'</strong> <span class="editable_string wg_highlight" ref="'.$gift_id.'">'.$gift->get_recipient().'</span><input type="text" id="'.$gift_id.'_recipient_input" class="option_recipient cloak" value="'.$gift->get_recipient().'" />';
		echo '<br/><span style="color:silver">'.wpg__( 'code' ).': '.$gift->get_coupon_code().'</span>';
		echo '</p>';
		echo '<p><span id="'.$gift_id.'_activate_result"></span><a href="/wp-admin/edit.php?post_type=wg_purchase&resend_gift='.$gift_id.'&_wpnonce='.wp_create_nonce( 'resend_gift' ).'" class="wg_resend_gift button" id="'.$gift_id.'_activate" ref="'.$gift_id.'">Resend</a></p>';
		return;

	}

	function column_status( $item ) {
		$gift = WP_Groupbuy_Gift::get_instance( $item->ID );

		$claimed = $gift->get_claimed();
		
		if ( $claimed ) {
			$status = '<strong>'.wpg__( 'Complete' ).'</strong><br/>';
		} else {
			$status = '<strong>'.wpg__( 'Pending Claim' ).'</strong><br/>';
		}
		
		$date = $claimed ? : $item->post_date;

		$status .= '<span style="color:silver">';
		$status .= mysql2date( get_option( 'date_format' ).' - '.get_option( 'time_format' ), $date );
		$status .= '</span>';

		return $status;
	}

	function get_columns() {
		$columns = array(
			'status'  => wpg__('Status'),
			'title'  => wpg__('Order'),
			'total'  => wpg__('Totals'),
			'deals'  => wpg__('Deals'),
			'gift'  => wpg__('Manage')
		);
		return apply_filters( 'wg_mngt_gifts_columns', $columns );
	}

	function get_sortable_columns() {
		$sortable_columns = array(
		);
		return apply_filters( 'wg_mngt_gifts_sortable_columns', $sortable_columns );
	}

	function get_bulk_actions() {
		$actions = array();
		return apply_filters( 'wg_mngt_gifts_bulk_actions', $actions );
	}


	// Prep data.
	function prepare_items() {

		// records per page to show
		$per_page = 25;

		// Define our column headers.
		$columns = $this->get_columns();
		$hidden = array();
		$sortable = $this->get_sortable_columns();


		// Build an array to be used by the class for column headers.
		$this->_column_headers = array( $columns, $hidden, $sortable );

		$filter = ( isset( $_REQUEST['post_status'] ) ) ? $_REQUEST['post_status'] : 'all';
		$args=array(
			'post_type' => WP_Groupbuy_Gift::POST_TYPE,
			'posts_per_page' => $per_page,
			'paged' => $this->get_pagenum()
		);
		// Check purchases based on Deal ID
		if ( isset( $_GET['search_type'] ) && $_GET['search_type'] == 'deal_id' && $_GET['search_value'] != '' ) {

			if ( WP_Groupbuy_Deal::POST_TYPE != get_post_type( $_GET['search_value'] ) )
				return; // not a valid search

			$purchase_ids = WP_Groupbuy_Purchase::get_purchases( array( 'deal' => $_GET['search_value'] ) );

			$meta_query = array(
				'meta_query' => array(
					array(
						'key' => '_purchase',
						'value' => $purchase_ids,
						'type' => 'numeric',
						'compare' => 'IN'
					)
				) );
			$args = array_merge( $args, $meta_query );
		}
		// Check payments based on Account ID
		if ( isset( $_GET['search_type'] ) && $_GET['search_type'] == 'account_id' && $_GET['search_value'] != '' ) {

			// if ( WP_Groupbuy_Account::POST_TYPE != get_post_type( $_GET['search_value'] ) )
			// 	return; // not a valid search

			// $purchase_ids = WP_Groupbuy_Purchase::get_purchases( array( 'account' => $_GET['search_value'] ) );

			// $meta_query = array(
			// 	'meta_query' => array(
			// 		array(
			// 			'key' => '_purchase',
			// 			'value' => $purchase_ids,
			// 			'type' => 'numeric',
			// 			'compare' => 'IN'
			// 		)
			// 	) );
			// $args = array_merge( $args, $meta_query );

			$author_id = WP_Groupbuy_Account::get_user_id_for_account( $_GET['search_value'] );
			$args = array_merge( $args, array( 'author' => $author_id) );

		}

		// Search
		if ( isset( $_GET['search_type'] ) && $_GET['search_type'] == 's' && $_GET['search_value'] != '' ) {
			$args = array_merge( $args, array( 's' => $_GET['search_value'] ) );
		}
		// Filter by date
		if ( isset( $_GET['m'] ) && $_GET['m'] != '' ) {
			$args = array_merge( $args, array( 'm' => $_GET['m'] ) );
		}

		$gifts = new WP_Query( $args );

		// Sorted data to the items property, where it can be used by the rest of the class.
		$this->items = apply_filters( 'wg_mngt_gifts_items', $gifts->posts );

		$this->set_pagination_args( array(
				'total_items' => $gifts->found_posts,
				'per_page'  => $per_page,
				'total_pages' => $gifts->max_num_pages
			) );
	}

}