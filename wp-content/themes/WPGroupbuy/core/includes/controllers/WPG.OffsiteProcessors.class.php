<?php

add_action( 'wg_init_controllers', array( 'WP_Groupbuy_Offsite_Processors', 'init' ), 50 );

abstract class WP_Groupbuy_Offsite_Processors extends WP_Groupbuy_Payment_Processors {
	const CHECKOUT_ACTION = 'wg_offsite_payments';
	// Subclasses should override this to identify if they've returned from offsite processing
	public static function returned_from_offsite() {
		return FALSE;
	}
	protected function __construct() {
		parent::__construct();
		add_filter( 'wg_checkout_panes_'.WP_Groupbuy_Checkouts::PAYMENT_PAGE, array( $this, 'payment_pane' ), 10, 2 );
		add_action( 'wg_checkout_action_'.WP_Groupbuy_Checkouts::PAYMENT_PAGE, array( $this, 'processed_payment_page' ), 20, 1 );
		add_filter( 'wg_checkout_panes_'.WP_Groupbuy_Checkouts::REVIEW_PAGE, array( $this, 'review_pane' ), 10, 2 );
		add_action( 'wg_checkout_action_'.WP_Groupbuy_Checkouts::REVIEW_PAGE, array( $this, 'process_review_page' ), 20, 1 );
		add_filter( 'wg_checkout_payment_controls', array( $this, 'payment_controls' ), 10, 2 );
		WP_Groupbuy_Offsite_Processor_Handler::init();
	}
	// The payment page is unnecessary (or, rather, it's offsite)
	public function remove_payment_page( $pages ) {
		if ( $this->offsite_payment_complete() ) {
			unset( $pages[WP_Groupbuy_Checkouts::PAYMENT_PAGE] );
		}
		return $pages;
	}
	// Determine whether the user has finished with the offsite portion of the payment. If so, we can skip the payment page
	public function offsite_payment_complete() {
		return TRUE;
	}
	// Display the credit card form
	public function payment_pane( $panes, WP_Groupbuy_Checkouts $checkout ) {
		$panes['payment'] = array(
		'weight' => 100,
		'body' => self::load_view_to_string( 'checkout/credit_card', array( 'fields' => $this->payment_fields( $checkout ) ) ),
		);
		return $panes;
	}
	protected function payment_fields( $checkout = NULL ) {
		$fields = array();
		$fields = apply_filters( 'wg_offsite_payment_fields', $fields, __CLASS__, $checkout );
		$fields = apply_filters( 'wg_payment_fields', $fields, __CLASS__, $checkout );
		uasort( $fields, array( get_class(), 'sort_by_weight' ) );
		return $fields;
	}
	public function get_payment_request_total( WP_Groupbuy_Checkouts $checkout, $purchase = NULL ) {
		if ( $purchase && is_a( $purchase, 'WP_Groupbuy_Purchase' ) ) {
			$total = $purchase->get_total();
		} else {
			$cart = $checkout->get_cart();
			$total = $cart->get_total();
		}
		return apply_filters( 'wg_offsite_purchase_payment_request_total', $total, $checkout );
	}
	public function payment_controls( $controls, WP_Groupbuy_Checkouts $checkout ) {
		if ( isset( $controls['review'] ) ) {
			$controls['review'] = str_replace( self::__( 'Review' ), self::__( 'Continue' ), $controls['review'] );
		}
		return $controls;
	}
	public function processed_payment_page( WP_Groupbuy_Checkouts $checkout ) {
		if ( $checkout->is_page_complete( WP_Groupbuy_Checkouts::PAYMENT_PAGE ) ) { // Make sure to send offsite when it's okay to do so.
			do_action( 'wg_send_offsite_for_payment', $checkout );
		}
	}
	public function process_review_page( WP_Groupbuy_Checkouts $checkout ) {
		do_action( 'wg_send_offsite_for_payment_after_review', $checkout );
	}
	public function get_payment_guide() {
		return '';
	}
	// Display the review pane
	public function review_pane( $panes, WP_Groupbuy_Checkouts $checkout ) {
		$fields = array(
			'method' => array(
				'label' => self::__( 'Payment Method' ),
				'value' => $this->get_payment_method(),
				'weight' => 1,
			),
			'guide' => array(
				'label' => self::__( 'Instruction' ),
				'value' => $this->get_payment_guide(),
				'weight' => 2,
			)
		);
		$fields = apply_filters( 'wg_payment_review_fields', $fields, get_class( $this ), $checkout );
		uasort( $fields, array( get_class(), 'sort_by_weight' ) );
		$panes['payment'] = array(
			'weight' => 100,
			'body' => self::load_view_to_string( 'checkout/credit-card-review', array( 'fields' => $fields ) ),
		);
		return $panes;
	}
}
class WP_Groupbuy_Offsite_Processor_Handler extends WP_Groupbuy_Controller {
	const PAYMENT_HANDLER_OPTION = 'wg_payment_handler';
	const PAYMENT_HANDLER_QUERY_ARG = 'payment_handler';
	private static $payment_handler_path = 'payment_handler';
	private static $use_ssl = FALSE;
	private static $payment_handler = NULL;
	public static function init() {
		self::$payment_handler_path = get_option( self::PAYMENT_HANDLER_OPTION, self::$payment_handler_path );
		self::register_path_callback( self::$payment_handler_path, array( get_class(), 'on_handler_page' ), self::PAYMENT_HANDLER_QUERY_ARG );
	}
	// The URL to the checkout page
	public static function get_url() {
		if ( self::using_permalinks() ) {
			$url = trailingslashit( home_url( self::$payment_handler_path ) );
		} else {
			$url = add_query_arg( self::PAYMENT_HANDLER_QUERY_ARG, 1, home_url( '' ) );
		}
		return apply_filters( 'wg_ssl_url', $url );
	}
	private function __clone() {
		trigger_error( __CLASS__.' may not be cloned', E_USER_ERROR );
	}
	private function __sleep() {
		trigger_error( __CLASS__.' may not be serialized', E_USER_ERROR );
	}
	public static function get_instance() {
		if ( !( self::$payment_handler && is_a( self::$payment_handler, __CLASS__ ) ) ) {
			self::$payment_handler = new self();
		}
		return self::$payment_handler;
	}
	// on the checkout page
	public static function on_handler_page() {
		self::do_not_cache();
		self::get_instance(); // make sure the class is instantiated
	}
	protected function __construct() {
		$this->handle_action();
	}
	private function handle_action() {
		if ( isset( $_GET[self::PAYMENT_HANDLER_QUERY_ARG] ) ) {
			do_action( 'wg_payment_handler_'.strtolower( $_GET[self::PAYMENT_HANDLER_QUERY_ARG] ), $this );
		}
		do_action( 'wg_payment_handler', $_POST, $this );
	}
}
?>