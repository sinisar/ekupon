<?php
/**
 * Plugin Name:     WPGroupbuy Addon - PDF Vouchers
 * Description:     Generates vouchers in PDF rather than HTML format
 * Version:         1.0
 * Author:          WPG Team
 * Author URI:      http://wpgroupbuy.com
 */

if ( get_option('wg_voucher_pdf', 1) ) {

    class WPG_Vouchers_PDF {

        private static $instance;


        /**
         * Get active instance
         */
        public static function instance() {
            if( !self::$instance )
                self::$instance = new WPG_Vouchers_PDF();

            return self::$instance;
        }


        /**
         * Class constructor
         */
        public function __construct() {
            $this->includes();
            $this->init();
        }

        /**
         * Include necessary files
         */
        private function includes() {
            /*
            if( !class_exists( 'FPDF' ) )
                require_once( WG_PATH. '/includes/controllers/lib/pdf/fpdf17/fpdf.php' ); //update

            require_once( WG_PATH. '/includes/controllers/lib/pdf/fpdi/fpdi.php' );
            require_once( WG_PATH. '/includes/controllers/lib/pdf/fpdf_rotate.php' );
            */
        }


        /**
         * Run action and filter hooks
         */
        private function init() {
            // Internationalization
            add_action( 'init', array( $this, 'textdomain' ) );

            // Voucher Override
            add_action( 'wg_voucher_pre_header', array( $this, 'voucher_override' ) );
        }


        /**
         * Internationalization
         */
        public static function textdomain() {
            // Set filter for language directory
            $lang_dir = dirname( plugin_basename( __FILE__ ) ) . '/languages/'; //update
            $lang_dir = apply_filters( 'wpg_pdfv_lang_dir', $lang_dir );

            // Load translations
            load_plugin_textdomain( 'wpg_pdfv', false, $lang_dir );
        }

        /**
         * @param $property_name - name of the term where HAS function will be executed - for example custom_options_terms
         * @param $outputHTML - output html that needs to be renderd if HAS function returns TRUE
         * @param $deal_id - number of deal, for HAS function
         * @return mixed - return rendered outputHTML if successfull otherwise return emptz string
         */
        public function print_PDF_content($property_name, $outputHTML, $deal_id) {
            $functionName = 'wg_has_'.$property_name;
            if($functionName($deal_id)) {
                return $outputHTML;
            }
            return "";
        }

        /**
         * Voucher override
         */
        public function voucher_override() {
            global $wp_query;

            $voucher_id = $wp_query->query_vars['wg_voucher'];
            $deal_id = wg_get_vouchers_deal_id( $voucher_id );
            $voucher = WP_Groupbuy_Voucher::get_instance($voucher_id);
            //$template = WG_PATH . '/resources/voucher_template.pdf'; //update

            $pdfDocument = '
    <div id="PDFcontent">
        <div class="printInfo">
            <div id="infoContainer">
                <div id="leftInfo">
                    <p>
                        <span class="label right">Kupec KUPON(čk)A:</span>
                        <span class="value left">' .wg_get_purchase_billing_information_property($voucher->get_purchase_id(), null, true). '</span>

                    </p>
                    <p>
                        <span class="label right">ID kupona - št. rač.:</span>
                        <span class="value left">' .wg_get_vouchers_deal_id(). '</span>
                        <br />
                        <span class="label right">Kraj opravljene storitve:</span>
                        <span class="value left">' .wg_get_user_address()['city']. '</span>
                        <br />
                        <span class="label right">Datum izdaje računa:</span>
                        <span class="value left">' .date( current_time( 'd.m.Y')). '</span>
                    </p>
                    <p>
                        <span class="label right">Cena (brez DDV):</span>
                        <span class="value left">'. wg_get_formatted_money( wg_get_coupon_value( $deal_id ) - wg_get_coupon_value( $deal_id )*0.22 ).'</span>
                        <br />
                        <span class="label right">DDV 22%:</span>
                        <span class="value left">'. wg_get_formatted_money( wg_get_coupon_value( $deal_id )*0.22 ) .'</span>
                    <br />
                        <span class="label right">Za plačilo cena (z DDV):</span>
                        <span class="value left">'. wg_get_formatted_money(wg_get_coupon_value( $deal_id )) .'</span>
                    </p>
                </div>
                <div id="rightInfo">
                    <p>
                        <span class="label">Ponudbo lahko izkoristite:</span>
                    </p>
                    <address style="font-style: normal; ">

                        '. wg_get_merchant_name($deal_id).' <br />'.wg_get_merchant_street($deal_id).'<br />'.wg_get_merchant_zip($deal_id).' '.wg_get_merchant_city($deal_id).'<br />
                    </address>
                    <p>
                        <span class="bold right">Veljavnost kupona:</span>
                        <span class="value left">'.date( 'd.m.Y', wg_get_deal_voucher_exp_date($deal_id)).'</span>
                        <br /><br />
                        <span class="label right">VELJA KOT RAČUN!</span>
                    </p>
                </div>
            </div>
            <div id="infoContent">
                <h2 style="border-top: 1px dotted #777; padding: 20px 0 10px 0;">'.wg_get_deal_title($deal_id).' '
                .wg_get_deal_additional_description($deal_id).'</h2>
                    <ul>
                        ' .$this->print_PDF_content('custom_options_terms', '<li><strong>pogoji koriščenja:
                        </strong>' . wp_strip_all_tags(wg_get_custom_options_terms($deal_id)) .'</li>',$deal_id) .'
                        <li><strong>akcijska cena</strong>, <span style="color:#D61F8D;font-weight:bold;">ki jo plačate ponudniku znaša: '
                .wg_get_formatted_money(wg_get_deal_worth( $deal_id )).'</span></li>
                        ' .$this->print_PDF_content('custom_options_redeem', '<li><strong>veljavnost</strong>: '. wp_strip_all_tags(wg_get_custom_options_redeem($deal_id)) . '</li>', $deal_id). '
                        ' .$this->print_PDF_content('custom_options_other_terms', '<li><strong>ostalo</strong>: ' . wp_strip_all_tags(wg_get_custom_options_other_terms($deal_id)) . '</li>', $deal_id) . '
                        <li><strong>ob obisku je treba obvezno predati kupon s kodo</strong></li>
                        ' .$this->print_PDF_content('merchant_opening_hours','<li><strong>obratovalni čas: </strong>' .
                    wp_strip_all_tags(wg_get_merchant_opening_hours($deal_id)) . '</li>', $deal_id). '
                    </ul>
            </div>
        </div>
    </div>';
            include("lib/pdf/mpdf/mpdf.php");
            $mpdf = new mPDF('UTF-8', 'A4');
            $mpdf->setAutoTopMargin = 'stretch'; // Set pdf top margin to stretch to avoid content overlapping
            $mpdf->autoMarginPadding = '-10';
            $mpdf->setAutoBottomMargin = 'stretch'; // Set pdf bottom margin to stretch to avoid content overlapping
            $voucher_claim_url = wg_get_voucher_claim_url( wg_get_voucher_security_code() );
            $qrImage = "";
            if ( $voucher_claim_url ) {
                $qrImage = '<img src="http://api.qrserver.com/v1/create-qr-code/?size=80x80&data=' . urlencode(
                        $voucher_claim_url ).'"/>';

            }

            // PDF header content
            $mpdf->SetHTMLHeader('<div id="PDFHeader">
                          <div class="logo" style="float: left;display: inline-block;">
                                <img class="left" src="'. get_template_directory_uri() . "-child/style/images/logo-neg.png".'"/>
                                <div style="font-size:10px;color: #777;"><strong>E-kupon d.o.o.</strong>, Bravničarjeva ulica 13, 1000 Ljubljana</div>
                          </div>
                          <div class="couponID">
                                <div class="voucherCode" style="font-size: 20px; font-weight: bold;">
                                    #'.apply_filters('wg_voucher_code', wg_get_voucher_code( $voucher_id ),$voucher_id ).'
                                </div>
                                <div class="qrImage">
                                '.$qrImage.'
                                </div>
                          </div>
                      </div>');
            // PDF footer content
            $mpdf->SetHTMLFooter('<div id="PDFFooter">
                        <div class="notification">
                            <p>
                                Vaša koda na kuponu je popolnoma univerzalna, zato kupona zaradi lastne varnosti ne pošiljajte nikomur! Ponudnik storitve bo seznanjen z vašim imenom in univerzalno kodo
                                in ko bo izkoriščen, ponovna uporaba ni mogoča.
                                </p>
                                <p>
                                Reklamacije: Podjetje E-kupon d.o.o. - izkoristi.me v nobenem trenutku ni lastnik storitev/blaga, ki jih oglašuje na svoji spletni strani in zato ne more biti odgovoren
                                za morebitne pravne in fizične nepravilnosti posamezne storitve/blaga, kot tudi ne za drugo škodo, nepravilnosti ali pomanjkljivosti, ki morebiti lahko nastanejo
                                med uporabo storitev/blaga, ki so bile del promocije. Za le-te odgovarja izključno ponudnik, ki je lastnik posamezne storitve/blaga. Reklamacije rešuje izključno ponudnik
                                blaga/storitve v zakonskem roku.
                            </p>
                        </div>
                        <div>&copy;2017 <a href="http://www.izkoristi.me">www.izkoristi.me</a></div>
                      </div>');
            $stylesheet = file_get_contents(WG_PATH. 'includes/controllers/lib/pdf/printPDF.css'); // external css
            $mpdf->WriteHTML($stylesheet,1);
            $mpdf->WriteHTML($pdfDocument,2);
            $mpdf->Output();
            exit;
        }
    }

    /**
     * Load the plugin
     */
    function wpg_pdfv_load() {
        if( !class_exists( 'WP_Groupbuy_Controller' ) ) return;

        $wpg_pdfv = new WPG_Vouchers_PDF();
    }
    add_action( 'wg_loaded', 'wpg_pdfv_load' );
}

class WPG_Vouchers_PDF_Options {
    public static function init() {
        add_action( 'admin_init', array( get_class(), 'register_settings_fields' ), 20, 0 );
    }

    public static function register_settings_fields() {
        $page = WP_Groupbuy_UI::get_settings_page();
        $section = 'wg_general_voucher_settings';
        register_setting( $page, 'wg_voucher_pdf' );
        add_settings_field( 'wg_voucher_pdf', __( 'Enable PDF Download', 'wpgroupbuy' ), array( get_class(), 'display_voucher_pdf_download' ), $page, $section );
    }

    public static function display_voucher_pdf_download() { ?>
        <input type="checkbox" name="wg_voucher_pdf" id="wg_voucher_pdf" value="1" <?php echo checked( get_option( 'wg_voucher_pdf', 1 ) ) ?> />
        <label for="wg_voucher_pdf"><?php _e( 'Voucher will display as PDF', 'wpgroupbuy' ) ?></label>
    <?php 
    }

}

add_action( 'wg_loaded', array( 'WPG_Vouchers_PDF_Options' , 'init') );
