<?php

class WP_Groupbuy_Checkout_Notes extends WP_Groupbuy_Controller {
	const ATTRIBUTE_DATA_KEY = 'wg_att_checkout_notes';
	const CHECKOUT_NOTES_META_KEY = 'wg_checkout_notes';
	const VOUCHER_META = '_voucher_notes_meta';
	const REQUIRE_AT_CHECKOUT = 'wg_require_at_checkout_option';
	const ADD_TO_CART_FIELD = '_wg_add_to_cart_field';
	const ADD_TO_CART_FIELD_GLOBAL = 'wg_add_to_cart_field';
	private static $require;
	private static $add_field;

	public static function init() {

		self::$require = (bool)get_option( self::REQUIRE_AT_CHECKOUT, 0 );
		self::$add_field = (bool)get_option( self::ADD_TO_CART_FIELD_GLOBAL, 1 );

		// Add domain option on general options page
		add_action( 'admin_init', array( get_class(), 'register_settings_fields' ), 10, 0 );

		if ( self::$add_field ) {
			add_action( 'add_meta_boxes', array( get_class(), 'add_meta_boxes' ) );
			add_action( 'save_post', array( get_class(), 'save_meta_boxes' ), 100, 2 );
		}

		add_filter( 'wg_deal_title', array( get_class(), 'filter_deal_title' ), 10, 2 );
		add_filter( 'add_to_cart_data', array( get_class(), 'filter_add_to_cart_data' ), 10, 3 );

		// Checkout panes
		self::register_payment_pane();
		self::register_review_pane();

		add_filter( 'wg_add_to_cart_form_fields', array( get_class(), 'filter_add_to_cart_form_fields' ), 10, 2 );

		// Save notes to the checkout cache after a validation
		add_filter( 'wg_valid_process_payment_page', array( get_class(), 'process_payment_page' ), 100, 2 );
		// Save notes record for purchase
		add_action('completing_checkout', array(get_class(), 'save_notes'), 10, 1);

		// Save notes to the voucher
		add_action( 'create_voucher_for_purchase', array( get_class(), 'set_vouchers_deal_notes' ), 10, 3 );

		// Reports
		/// Purchase Report
		add_filter( 'set_deal_purchase_report_data_column', array( get_class(), 'set_deal_purchase_report_data_column' ), 10, 1 );
		add_filter( 'set_deal_purchase_report_data_records', array( get_class(), 'set_deal_purchase_report_data_records' ), 10, 1 );
		// Merchant Report
		add_filter( 'set_merchant_purchase_report_column', array( get_class(), 'set_deal_purchase_report_data_column' ), 10, 1 );
		add_filter( 'set_merchant_purchase_report_records', array( get_class(), 'set_deal_purchase_report_data_records' ), 10, 1 );
		/// Vouchers
		add_filter( 'set_deal_voucher_report_data_column', array( get_class(), 'set_deal_purchase_report_data_column' ), 10, 1 );
		add_filter( 'set_deal_voucher_report_data_records', array( get_class(), 'set_deal_purchase_report_data_records' ), 10, 1 );
		// Merchant Report
		add_filter( 'set_merchant_voucher_report_data_column', array( get_class(), 'set_deal_purchase_report_data_column' ), 10, 1 );
		add_filter( 'set_merchant_voucher_report_data_records', array( get_class(), 'set_deal_purchase_report_data_records' ), 10, 1 );

		add_filter( 'wg_mngt_purchases_columns', array( get_class(), 'wg_mngt_purchases_columns_note' ) );
		add_filter( 'wg_mngt_purchases_column_wg_checkout_notes', array( get_class(), 'wg_mngt_purchases_column_wg_checkout_notes' ) );

	}

	/**
	 * If attribute, show in a drop-down next to the add-to-cart button
	 */
	public static function filter_add_to_cart_form_fields( $fields, $deal_id ) {
		if ( !self::show_addtocart_deal_notes( $deal_id ) ) {
			return $fields;
		}
		$fields[] = '<textarea name="'.self::ATTRIBUTE_DATA_KEY.'" placeholder="'.wg__( 'Notes' ).'"></textarea>';
		return $fields;
	}


	/**
	 * Register action hooks for displaying and processing the payment page
	 */
	private static function register_payment_pane() {
		add_filter( 'wg_checkout_panes_'.WP_Groupbuy_Checkouts::PAYMENT_PAGE, array( get_class(), 'display_payment_page' ), 10, 2 );
	}

	private static function register_review_pane() {
		add_filter( 'wg_checkout_panes_'.WP_Groupbuy_Checkouts::REVIEW_PAGE, array( get_class(), 'display_review_page' ), 10, 2 );
	}

	private static function register_confirmation_pane() {
		add_filter( 'wg_checkout_panes_'.WP_Groupbuy_Checkouts::CONFIRMATION_PAGE, array( get_class(), 'display_confirmation_page' ), 10, 2 );
	}

	public static function display_payment_page( $panes, $checkout ) {
		$panes['wg_notes'] = array(
			'weight' => 100,
			'body' => self::load_view_to_string( 'checkout/notes', array( 'required' => self::$require ) ),
		);
		return $panes;
	}

	public static function process_payment_page(  $valid, WP_Groupbuy_Checkouts $checkout ) {
		if ( isset( $_POST['wg_notes'] ) && self::$require ) {
			if ( $_POST['wg_notes'] == '' ) {
				self::set_message( "A note is necessary. ", self::MESSAGE_STATUS_ERROR );
				$valid = FALSE;
			}
		}
		if ( !$valid ) {
			$checkout->mark_page_incomplete( WP_Groupbuy_Checkouts::PAYMENT_PAGE );
		} else {
			$checkout->cache['wg_notes'] = esc_textarea( $_POST['wg_notes'] );
		}
		return $valid;
	}


	/**
	 * Display the review pane with a message.
	 */
	public static function display_review_page( $panes, $checkout ) {
		$notes = $checkout->cache['wg_notes'];
		if ( $checkout->cache['wg_notes'] ) {
			$panes['wg_notes'] = array(
				'weight' => 90,
				'body' => self::load_view_to_string( 'checkout/notes-review', array( 'notes' => $notes ) ),
			);
		}
		return $panes;
	}

	public static function save_notes( WP_Groupbuy_Checkouts $checkout ) {
		if ( $checkout->cache['wg_notes'] && $checkout->cache['purchase_id'] ) {
			$purchase = WP_Groupbuy_Purchase::get_instance( $checkout->cache['purchase_id'] );
			self::set_purchase_notes( $purchase, $checkout->cache['wg_notes'] );
			do_action( 'wg_checkout_notes_save_note', $checkout->cache['wg_notes'], $purchase, $checkout );
		}
	}

	public function set_purchase_notes( WP_Groupbuy_Purchase $purchase, $notes ) {
		$purchase->save_post_meta( array(
				self::CHECKOUT_NOTES_META_KEY => $notes,
			) );
	}

	public function get_purchase_notes( WP_Groupbuy_Purchase $purchase ) {
		return $purchase->get_post_meta( self::CHECKOUT_NOTES_META_KEY );
	}

	public static function filter_add_to_cart_data( $data, $item_id, $quantity ) {
		if ( isset( $_REQUEST[self::ATTRIBUTE_DATA_KEY] ) ) {
			$attribute = WP_Groupbuy_Attribute::get_instance( $_REQUEST[WP_Groupbuy_Attributes::ATTRIBUTE_QUERY_VAR] );
			if ( $attribute ) {
				if ( $attribute->get_deal_id() == $item_id ) { // make sure we have a valid attribute
					$data[self::ATTRIBUTE_DATA_KEY] = esc_textarea( $_REQUEST[self::ATTRIBUTE_DATA_KEY] );
				}
			} else {
				$data = new WP_Error( 'invalid_selection', self::__( 'Invalid selection' ) );
			}
		}
		return $data;
	}

	public static function filter_deal_title( $title, $data ) {
		if ( !isset( $data[self::ATTRIBUTE_DATA_KEY] ) ) {
			return $title; // no note
		}
		if ( $data[self::ATTRIBUTE_DATA_KEY] != '' ) {
			$title .= ' — "'.esc_attr__( $data[self::ATTRIBUTE_DATA_KEY] ).'"';
		}
		return $title;
	}

	public function set_att_purchase_notes( WP_Groupbuy_Cart $cart, $note ) {
		$cart->save_post_meta( array(
				self::ATT_META_KEY => $note,
			) );
	}

	public function get_att_purchase_charity( WP_Groupbuy_Cart $cart ) {
		return $cart->get_post_meta( self::ATT_META_KEY );
	}

	public static function set_deal_purchase_report_data_column( $columns ) {
		$columns['notes'] = self::__( 'Customer Message' );
		$columns['details'] = self::__( 'Details' );
		return $columns;
	}

	public static function set_deal_purchase_report_data_records( $array ) {
		if ( !is_array( $array ) ) {
			return;
		}
		$new_array = array();
		foreach ( $array as $records ) {
			$items = array();
			$purchase = WP_Groupbuy_Purchase::get_instance( $records['id'] );
			$note = self::get_purchase_notes( $purchase );
			if ( !empty( $note ) ) {
				$note_record = array( 'notes' => $note );
			} else {
				$note_record = array( 'notes' => self::__( 'N/A' ) );
			}

			// Add labels
			$items = array();
			$label = array();
			if ( !empty( $records['voucher_id'] ) ) {
				$notes = wg_get_note_by_voucher_id( $records['voucher_id'] );
				if ( !empty( $notes ) ) {
					$label = array( 'details' => $notes );
				}
			} else {
				$purchase = WP_Groupbuy_Purchase::get_instance( $records['id'] );
				foreach ( $purchase->get_products() as $product => $value ) {
					if ( !empty( $value['data'][self::ATTRIBUTE_DATA_KEY] ) && $value['deal_id'] == $_GET['id'] ) {
						$items[] = $value['data'][self::ATTRIBUTE_DATA_KEY];
					}
				}
				$label = array( 'details' => implode( ', ', $items ) );
			}

			$new_array[] = array_merge( $records, $note_record, $label );
		}
		return $new_array;
	}

	public static function set_vouchers_deal_notes( $voucher_id, $purchase, $product ) {
		if ( !empty( $product['data'][self::ATTRIBUTE_DATA_KEY] ) ) {
			update_post_meta( $voucher_id, self::VOUCHER_META, $product['data'][self::ATTRIBUTE_DATA_KEY] );
		}
	}

	public function get_vouchers_deal_notes( $voucher_id ) {
		$voucher = WP_Groupbuy_Voucher::get_instance( $voucher_id );
		return $voucher->get_post_meta( self::VOUCHER_META );
	}


	public static function register_settings_fields() {
		$page = WP_Groupbuy_UI::get_settings_page();
		$section = 'wg_messaging_addon';
		add_settings_section( $section, self::__( 'Checkout Notes' ), array( get_class(), 'display_settings_section' ), $page );
		// Settings
		register_setting( $page, self::REQUIRE_AT_CHECKOUT );
		register_setting( $page, self::ADD_TO_CART_FIELD_GLOBAL );
		// Fields
		add_settings_field( self::REQUIRE_AT_CHECKOUT, self::__( 'Require Customer Message' ), array( get_class(), 'display_option' ), $page, $section );
		add_settings_field( self::ADD_TO_CART_FIELD_GLOBAL, self::__( 'Per item messaging.' ), array( get_class(), 'display_field_option' ), $page, $section );
	}

	public static function display_option() {
		echo '<input type="checkbox" name="'.self::REQUIRE_AT_CHECKOUT.'" value="1" '.checked( '1', self::$require, FALSE ).' />&nbsp;'.self::__( 'Enable checkout requirement.' );
	}

	public static function display_field_option() {
		echo '<input type="checkbox" name="'.self::ADD_TO_CART_FIELD_GLOBAL.'" value="1" '.checked( '1', self::$add_field, FALSE ).' />&nbsp;'.self::__( 'Add option on the deal edit screen to enable item level notes when an item is added to the cart.' );
	}

	public static function add_meta_boxes() {
		add_meta_box( 'wg_notes_meta_box', self::__( 'Notes' ), array( get_class(), 'show_meta_box' ), WP_Groupbuy_Deal::POST_TYPE, 'side', 'low' );
	}

	public static function show_meta_box( $post, $metabox ) {
		$deal = WP_Groupbuy_Deal::get_instance( $post->ID );
		switch ( $metabox['id'] ) {
		case 'wg_notes_meta_box':
			self::show_meta_box_notes( $deal, $post, $metabox );
			break;
		default:
			self::unknown_meta_box( $metabox['id'] );
			break;
		}
	}

	public static function save_meta_boxes( $post_id, $post ) {

		// don't do anything on autosave, auto-draft, bulk edit, or quick edit
		if ( wp_is_post_autosave( $post_id ) || $post->post_status == 'auto-draft' || defined( 'DOING_AJAX' ) || isset( $_GET['bulk_edit'] ) ) {
			return;
		}

		// only continue if it's a deal post
		if ( $post->post_type != WP_Groupbuy_Deal::POST_TYPE ) {
			return;
		}

		// save all the meta boxes
		$addtocart_deal_notes = ( isset( $_POST['wg_addtocart_deal_notes'] ) && $_POST['wg_addtocart_deal_notes'] == '1' ) ? TRUE : FALSE;
		self::set_addtocart_deal_notes( $post_id, $addtocart_deal_notes );
	}

	/**
	 * Display the membership level meta box
	 */
	public static function show_meta_box_notes( WP_Groupbuy_Deal $deal, $post, $metabox ) {

		$wg_addtocart_deal_notes = self::show_addtocart_deal_notes( $deal->get_id() );
?>
			<p>
				<label for="wg_addtocart_deal_notes"><input type="checkbox" name="wg_addtocart_deal_notes" id="wg_addtocart_deal_notes" <?php checked( $wg_addtocart_deal_notes, '1' ); ?> value="1" /> <?php self::_e( 'Add a message textarea next to the add-to-cart option, allowing items to have their own custom messages.' ); ?></label>
			</p>
		<?php
	}

	public static function set_addtocart_deal_notes( $post_id, $meta ) {
		$meta = update_post_meta( $post_id, self::ADD_TO_CART_FIELD, (bool)$meta );
		return $meta;
	}

	public static function show_addtocart_deal_notes( $post_id ) {
		$meta = get_post_meta( $post_id, self::ADD_TO_CART_FIELD, true );
		return (bool)$meta;
	}

	public static function wg_mngt_purchases_columns_note( $columns ) {
		$columns['wg_checkout_notes'] = 'Checkout Note';
		return $columns;
	} 

	public static function wg_mngt_purchases_column_wg_checkout_notes( $item ) {
		$checkout_notes = get_post_meta( $item->ID, 'wg_checkout_notes', true );
		echo $checkout_notes;
	}
}

add_action( 'wg_init_controllers', array( 'WP_Groupbuy_Checkout_Notes_Addon', 'init' ), 150 );

// Initiate the add-on
class WP_Groupbuy_Checkout_Notes_Addon extends WP_Groupbuy_Controller {

	public static function init() {
		add_filter( 'wg_addons', array( get_class(), 'wg_addon' ), 10, 1 );
	}

	public static function wg_addon( $addons ) {
		$addons['checkout_notes'] = array(
			'label' => self::__( 'Checkout Notes' ),
			'description' => self::__( 'Adds a notes field to checkout for customers.' ),
			'files' => array(
				__FILE__,
				dirname( __FILE__ ) . '/lib/notes/template-tags.php',
			),
			'callbacks' => array(
				array( 'WP_Groupbuy_Checkout_Notes', 'init' ),
			),
		);
		return $addons;
	}

}
