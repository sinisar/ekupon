<?php

add_action( 'wg_init_controllers', array( 'WP_Groupbuy_Accounts', 'init' ), 5 );

// Registration, Login, Edit, Password Recovery...
class WP_Groupbuy_Accounts extends WP_Groupbuy_Controller {
	const ACCOUNT_PATH_OPTION = 'wg_account_path';
	const ACCOUNT_QUERY_VAR = 'wg_view_account';
	const CREDIT_TYPE = 'balance';
	private static $account_path = 'account';
	public static $record_type = 'credit_history';
	private static $balance_payment_processor;
	private static $instance;
	public static function init() {		
		self::$account_path = get_option( self::ACCOUNT_PATH_OPTION, self::$account_path );
		self::register_path_callback( self::$account_path, array( get_class(), 'on_account_page' ), self::ACCOUNT_QUERY_VAR, 'account' );
		add_action( 'admin_head', array( get_class(), 'admin_style' ) );
		add_action( 'add_meta_boxes', array( get_class(), 'add_meta_boxes' ) );
		add_action( 'save_post', array( get_class(), 'save_meta_boxes' ), 10, 2 );
		add_filter( 'wg_admin_bar', array( get_class(), 'add_link_to_admin_bar' ), 10, 1 );
		add_action( 'completing_checkout', array( get_class(), 'save_contact_info_from_checkout' ), 10, 1 );
		add_action( 'admin_init', array( get_class(), 'register_settings_fields' ), 50, 0 );
		add_action( 'admin_enqueue_scripts', array( get_class(), 'enqueue_scripts' ) );
		add_filter( 'wg_account_credit_types', array( get_class(), 'register_credit_type' ), 10, 1 );
		self::$balance_payment_processor = WP_Groupbuy_Account_Balance_Payments::get_instance();
		// Admin
		self::$settings_page = self::register_settings_page( 'account_records', self::__( 'Accounts' ), self::__( 'Accounts' ), 9.2, FALSE, 'records', array( get_class(), 'display_table' ) );
		// User Admin columns
		add_filter ( 'manage_users_columns', array( get_class(), 'user_register_columns' ) );
		add_filter ( 'manage_users_custom_column', array( get_class(), 'user_column_display' ), 10, 3 );
        add_filter ( 'manage_users_sortable_columns', array( get_class(), 'user_sortable_columns' ) );


        // Init other classes, this allows for the paths to be changed
		add_action( 'init', array( get_class(), 'wp_init' ) );
		// AJAX Actions
		add_action( 'wp_ajax_nopriv_wpg_ajax_get_account',  array( get_class(), 'ajax_get_account' ), 10, 0 );
		add_action( 'wp_ajax_wpg_ajax_get_account',  array( get_class(), 'ajax_get_account' ), 10, 0 );
		add_action( 'wg_weekly_cron', array( get_class(), 'remove_duplicated_accounts' ) );
	}
	// Init all other Account classes
	public static function wp_init() {
		WP_Groupbuy_Accounts_Login::init();
		WP_Groupbuy_Accounts_Registration::init();
		WP_Groupbuy_Accounts_Edit_Profile::init();
		WP_Groupbuy_Accounts_Retrieve_Password::init();
		WP_Groupbuy_Accounts_Checkout::init();
		//	WP_Groupbuy_Accounts_Import_Users::init();
	}
	public static function enqueue_scripts() {
		wp_enqueue_script( 'wp-groupbuy-admin-account', WG_RESOURCES . 'js/account.admin.wpg.js', array( 'jquery'), WP_Groupbuy::WG_VERSION );
		$translation_array = array( 
			'creditbox' => __('Account Balance & Points', "wpgroupbuy"),
			'bplog' => __('Balance & Points log', "wpgroupbuy"),
			'manageacc' => __('Manage Account', "wpgroupbuy"),
		);
		wp_localize_script( 'wp-groupbuy-admin-account', 'object_name_acc', $translation_array );
	}
	public static function register_credit_type( $credit_types = array() ) {
		$credit_types[self::CREDIT_TYPE] = self::__( 'Account Balance' );
		return $credit_types;
	}
	public static function register_settings_fields() {
		$page = WP_Groupbuy_UI::get_settings_page();
		$section = 'WG_URL_paths';
		add_settings_section( $section, self::__( 'Change your URL Paths' ), array( get_class(), 'display_account_paths_section' ), $page );
		// Settings
		register_setting( $page, self::ACCOUNT_PATH_OPTION );
		add_settings_field( self::ACCOUNT_PATH_OPTION, self::__( 'Account Dashboard Path' ), array( get_class(), 'display_account_path' ), $page, $section );
	}
	public static function display_account_paths_section() {
		WP_Groupbuy_Controller::flush_rewrite_rules();
		echo self::__( '<h4>Account paths</h4>' );
	}
	public static function display_account_path() {
		echo trailingslashit( get_home_url() ) . ' <input type="text" name="'.self::ACCOUNT_PATH_OPTION.'" id="'.self::ACCOUNT_PATH_OPTION.'" value="' . esc_attr( self::$account_path ) . '"  size="40" /><br />';
	}
	public static function admin_style() {
		global $post;
		if ( $post && $post->post_type == WP_Groupbuy_Account::POST_TYPE ) {
		?>
			<style type="text/css">
				#minor-publishing-actions, #misc-publishing-actions, #delete-action, #submitdiv { display:none; }
				.postbox .inside, .stuffbox .inside{ padding:0}
				#wg_account_purchases #publishing-action {float: left;}
				#wg_account_purchases #major-publishing-actions {border-top: medium none; padding-left: 0;}
			</style>
		<?php
		}
	}
	public static function add_meta_boxes() {
		add_meta_box( 'wg_account_contact_info', self::__( 'Contact Info' ), array( get_class(), 'show_meta_box' ), WP_Groupbuy_Account::POST_TYPE, 'advanced', 'high' );
		add_meta_box( 'wg_account_purchases', self::__( 'Purchases' ), array( get_class(), 'show_meta_box' ), WP_Groupbuy_Account::POST_TYPE, 'advanced', 'high' );
		add_meta_box( 'wg_account_credits', self::__( 'Credits' ), array( get_class(), 'show_meta_box' ), WP_Groupbuy_Account::POST_TYPE, 'advanced', 'high' );
		add_meta_box( 'wg_account_credits_box', self::__( 'Credits & Points' ), array( get_class(), 'show_meta_box' ), WP_Groupbuy_Account::POST_TYPE, 'side', 'high' );
	}
	public static function show_meta_box( $post, $metabox ) {
		$account = WP_Groupbuy_Account::get_instance_by_id( $post->ID );
		switch ( $metabox['id'] ) {
		case 'wg_account_credits':
			self::show_meta_box_wg_account_credits( $account, $post, $metabox );
			break;
		case 'wg_account_credits_box':
			self::show_meta_box_wg_account_credits_box( $account, $post, $metabox );
			break;
		case 'wg_account_contact_info':
			self::show_meta_box_wg_account_contact_info( $account, $post, $metabox );
			break;
		case 'wg_account_purchases':
			self::show_meta_box_wg_account_purchases( $account, $post, $metabox );
			break;
		default:
			self::unknown_meta_box( $metabox['id'] );
			break;
		}
	}
	public static function save_meta_boxes( $post_id, $post ) {
		// only continue if it's an account post
		if ( $post->post_type != WP_Groupbuy_Account::POST_TYPE ) {
			return;
		}
		// don't do anything on autosave, auto-draft, bulk edit, or quick edit
		if ( wp_is_post_autosave( $post_id ) || $post->post_status == 'auto-draft' || defined( 'DOING_AJAX' ) || isset( $_GET['bulk_edit'] ) ) {
			return;
		}
		// save all the meta boxes
		$account = WP_Groupbuy_Account::get_instance_by_id( $post_id );
		if ( !is_a( $account, 'WP_Groupbuy_Account' ) ) {
			return; // The account doesn't exist
		}
		self::save_meta_box_wg_account_contact_info( $account, $post_id, $post );
		self::save_meta_box_wg_account_credits( $account, $post_id, $post );
	}
	private static function show_meta_box_wg_account_contact_info( WP_Groupbuy_Account $account, $post, $metabox ) {
		$address = $account->get_address();
		self::load_view( 'meta_boxes/account-contact-info', array(
				'first_name' => $account->get_name( 'first' ),
				'last_name' => $account->get_name( 'last' ),
				'street' => isset( $address['street'] )?$address['street']:'',
				'city' => isset( $address['city'] )?$address['city']:'',
				'zone' => isset( $address['zone'] )?$address['zone']:'',
				'postal_code' => isset( $address['postal_code'] )?$address['postal_code']:'',
				'country' => isset( $address['country'] )?$address['country']:'',
				'account_email' => $account->get_name( 'email' ),
				'account_phone' => $account->get_name( 'phone' ),
			), FALSE );
	}
	private static function save_meta_box_wg_account_contact_info( WP_Groupbuy_Account $account, $post_id, $post ) {
		$first_name = isset( $_POST['account_first_name'] ) ? $_POST['account_first_name'] : '';
		$account->set_name( 'first', $first_name );
		$last_name = isset( $_POST['account_last_name'] ) ? $_POST['account_last_name'] : '';
		$account->set_name( 'last', $last_name );
		$account_email = isset( $_POST['account_email'] ) ? $_POST['account_email'] : '';
		$account->set_name( 'email', $account_email );		
		$account_phone = isset( $_POST['account_phone'] ) ? $_POST['account_phone'] : '';
		$account->set_name( 'phone', $account_phone );		
		$address = array(
			'street' => isset( $_POST['account_street'] ) ? $_POST['account_street'] : '',
			'city' => isset( $_POST['account_city'] ) ? $_POST['account_city'] : '',
			'zone' => isset( $_POST['account_zone'] ) ? $_POST['account_zone'] : '',
			'postal_code' => isset( $_POST['account_postal_code'] ) ? $_POST['account_postal_code'] : '',
			'country' => isset( $_POST['account_country'] ) ? $_POST['account_country'] : '',
		);
		$account->set_address( $address );
	}
	private static function show_meta_box_wg_account_credits( WP_Groupbuy_Account $account, $post, $metabox ) {
		$types = self::account_credit_types();
		$credit_fields = array();
		foreach ( $types as $key => $label ) {
			$credit_fields[$key] = array(
				'balance' => $account->get_credit_balance( $key ),
				'label' => $label,
			);
		}
		$credit_types = apply_filters( 'wg_account_meta_box_credit_types', $credit_fields, $account );
		self::load_view( 'meta_boxes/account-credits', array(
				'account' => $account,
				'credit_types' => $credit_fields
			), FALSE );
	}
	
	private static function show_meta_box_wg_account_credits_box( WP_Groupbuy_Account $account, $post, $metabox ) {
		// For showing box only
	}
	private static function account_credit_types() {
		return apply_filters( 'wg_account_credit_types', array() );
	}
	private static function save_meta_box_wg_account_credits( WP_Groupbuy_Account $account, $post_id, $post ) {
		if ( isset( $_POST['account_credit_balance'] ) && is_array( $_POST['account_credit_balance'] ) ) {
			$types = array_keys( self::account_credit_types() );
			foreach ( $_POST['account_credit_balance'] as $key => $value ) {
				if ( in_array( $key, $types ) && is_numeric( $value ) ) {
					$balance = $account->get_credit_balance( $key );
					switch ( $_POST['account_credit_action'][$key] ) {
					case 'add':
						$total = $balance+$value;
						break;
					case 'deduct':
						$total = $balance-$value;
						break;
					case 'change':
						$total = $value;
						break;
					}
					$account->set_credit_balance( $total, $key );
					$data = $_POST;
					$data['adjustment_value'] = $value;
					$data['current_total'] = $total;
					$data['prior_total'] = $balance;
					do_action( 'wg_new_record', $_POST['account_credit_notes'][$key], WP_Groupbuy_Accounts::$record_type . '_' . $key, wpg__( 'Credit Adjustment' ), get_current_user_id(), $account->get_ID(), $data );
					do_action( 'wg_save_meta_box_wg_account_credits', $account, $post_id, $_POST );
				}
			}
		}
	}
	private static function show_meta_box_wg_account_purchases( WP_Groupbuy_Account $account, $post, $metabox ) {
		do_action( 'wg_account_purchases_meta_box_top', $account, $post );
		self::load_view( 'meta_boxes/account-purchases', array( 'account'=>$account ), TRUE );
		do_action( 'wg_account_purchases_meta_box_bottom', $account, $post );
	}
	// If a user's contact info isn't saved, try to get if from their billing information when checkout is finished.
	public static function save_contact_info_from_checkout( WP_Groupbuy_Checkouts $checkout ) {
		$account = WP_Groupbuy_Account::get_instance();
		$address = $account->get_address();
		$new_address = $address;
		$first_name = $account->get_name( 'first' );
		$last_name = $account->get_name( 'last' );
		$phone = $account->get_name( 'phone' );
		$email = $account->get_name( 'email' );
		if ( !$first_name && isset( $checkout->cache['billing']['first_name'] ) && $checkout->cache['billing']['first_name'] ) {
			$account->set_name( 'first', $checkout->cache['billing']['first_name'] );
		}
		if ( !$last_name && isset( $checkout->cache['billing']['last_name'] ) && $checkout->cache['billing']['last_name'] ) {
			$account->set_name( 'last', $checkout->cache['billing']['last_name'] );
		}
		if ( !$phone && isset( $checkout->cache['billing']['phone'] ) && $checkout->cache['billing']['phone'] ) {
			$account->set_name( 'phone', $checkout->cache['billing']['phone'] );
		}
		if ( !$email && isset( $checkout->cache['billing']['email'] ) && $checkout->cache['billing']['email'] ) {
			$account->set_name( 'email', $checkout->cache['billing']['email'] );
		}
		foreach ( array( 'street', 'city', 'zone', 'postal_code', 'country' ) as $key ) {
			if ( ( !isset( $address[$key] ) || !$address[$key] ) && isset( $checkout->cache['billing'][$key] ) && $checkout->cache['billing'][$key] ) {
				$new_address[$key] = $checkout->cache['billing'][$key];
			}
		}
		if ( $address != $new_address ) {
			$account->set_address( $new_address );
		}
	}
	public static function get_url() {
		if ( self::using_permalinks() ) {
			return trailingslashit( home_url( self::$account_path ) );
		} else {
			return add_query_arg( self::ACCOUNT_QUERY_VAR, 1, home_url() );
		}
	}
	public static function on_account_page() {
		self::login_required();
		self::get_instance(); // make sure the class is instantiated
	}
	public static function user_register_columns( $columns ) {
		// create a new array with account just after username, then everything else
		$new_columns = array();
		if ( isset($columns['posts']) ) {
			$new_columns['posts'] = $columns['posts'];
			unset($columns['posts']);
		}
		$new_columns['account'] = self::__( 'Account' );
        $new_columns['created_date'] = "Created date";
        //$new_columns['status'] = "Status";
		//$new_columns = array_merge($new_columns, $columns);
		$new_columns = array_merge ( $columns, $new_columns );
		return $new_columns;
	}
	public static function user_column_display( $empty='', $column_name, $id ) {
		$account = WP_Groupbuy_Account::get_instance( $id );
		if ( !$account )
			return; // return for that temp post
		switch ( $column_name ) {
		case 'account':
			$account_id = $account->get_ID();
			$user_id = $account->get_user_id_for_account( $account_id );
			$user = get_userdata( $user_id );
			$get_name = $account->get_name();
			$name = ( strlen( $get_name ) <= 1  ) ? '' : $get_name;
			//Build row actions
			$actions = array(
				'edit'    => sprintf( '<a href="post.php?post=%s&action=edit">'.self::__( 'Manage' ).'</a>', $account_id ),
				'payments'    => sprintf( '<a href="admin.php?page=wp-groupbuy/voucher_records&tab=wp-groupbuy/payment_records&account_id=%s">'.self::__( 'Payments' ).'</a>', $account_id ),
				'purchases'    => sprintf( '<a href="admin.php?page=wp-groupbuy/voucher_records&tab=wp-groupbuy/purchase_records&account_id=%s">'.self::__( 'Purchases').'</a>', $account_id ),
				'vouchers'    => sprintf( '<a href="admin.php?page=wp-groupbuy/voucher_records&account_id=%s">'.self::__( 'Vouchers').'</a>', $account_id ),
				'gifts'    => sprintf( '<a href="admin.php?page=wp-groupbuy/voucher_records&tab=wp-groupbuy/gift_records&account_id=%s">'.self::__( 'Gifts').'</a>', $account_id )
			);
			
			$wp_list_table = new WP_Groupbuy_Accounts_Table(); 
			//Return the title contents
			return sprintf( self::__( '%1$s <span style="color:silver">(account&nbsp;id:%2$s)</span> <span style="color:silver">(user&nbsp;id:%3$s)</span>%4$s' ),
				$name,
				$account_id,
				$user_id,
				$wp_list_table->row_actions( $actions ) 
			);
			break;
			/*
        case 'status':
            $user_data = get_userdata($account->get_user_id_for_account($account->get_ID()));
            return $user_data->user_status;
        */
		case 'created_date':
            $user_data = get_userdata($account->get_user_id_for_account($account->get_ID()));
            return date( "d.M.Y H:m:s", strtotime( $user_data->user_registered ) );

        default:
			// code...
			break;
		}
	}
	public static function user_sortable_columns($sortable_columns) {
        //$sortable_columns['role'] = array('role', false);
        $sortable_columns['created_date'] = 'registered';
        //$sortable_columns['user_status'] = 'status';
        return $sortable_columns;
    }

	public static function ajax_get_account() {
		$id = $_POST['id'];
		if ( !$id ) {
			return;
		}
		$user = get_userdata( $id );
		if ( is_a( $user, 'WP_User' ) ) { // Check if id is a WP User
			$account = WP_Groupbuy_Account::get_instance( $id );
		} else {
			$account = WP_Groupbuy_Account::get_instance_by_id( $id );
		}
		if ( is_a( $account, 'WP_Groupbuy_Account' ) ) {
			header( 'Content-Type: application/json' );
			$response = array(
				'account_id' => $account->get_ID(),
				'user_id' => $account->get_user_id(),
				'name' => wg_get_name( $account->get_user_id() ),
				'rewards' => $account->get_credit_balance( WP_Groupbuy_Affiliates::CREDIT_TYPE ),
				'credits' => $account->get_credit_balance( WP_Groupbuy_Accounts::CREDIT_TYPE ),
				'merchant_id' => wg_account_merchant_id( $account->get_user_id() ),
				'address' => wg_format_address( $account->get_address() )
			);
			echo json_encode( $response );
		}
		exit();
	}
	private function __clone() {
		trigger_error( __CLASS__.' may not be cloned', E_USER_ERROR );
	}
	private function __sleep() {
		trigger_error( __CLASS__.' may not be serialized', E_USER_ERROR );
	}
	public static function get_instance() {
		if ( !( self::$instance && is_a( self::$instance, __CLASS__ ) ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	private function __construct() {
		self::do_not_cache(); // never cache the account pages
		add_action( 'pre_get_posts', array( $this, 'edit_query' ), 10, 1 );
		add_action( 'the_post', array( $this, 'view_profile' ), 10, 1 );
		add_filter( 'the_title', array( $this, 'get_title' ), 10, 2 );
		add_filter( 'wg_account_view_panesmat_address', array( $this, 'get_panes' ), 10, 2 );
	}
	// Edit the query on the profile edit page to select the user's account.
	public function edit_query( WP_Query $query ) {
		// we only care if this is the query to show the account
		if ( isset( $query->query_vars[self::ACCOUNT_QUERY_VAR] ) && $query->query_vars[self::ACCOUNT_QUERY_VAR] ) {
			$query->query_vars['post_type'] = WP_Groupbuy_Account::POST_TYPE;
			$query->query_vars['p'] = WP_Groupbuy_Account::get_account_id_for_user();
		}
	}
	// Update the global $pages array with the HTML for the page.
	public function view_profile( $post ) {
		if ( $post->post_type == WP_Groupbuy_Account::POST_TYPE ) {
			remove_filter( 'the_content', 'wpautop' );
			$account = WP_Groupbuy_Account::get_instance();
			$panes = apply_filters( 'wg_account_view_panesmat_address', array(), $account );
			uasort( $panes, array( get_class(), 'sort_by_weight' ) );
			$view = self::load_view_to_string( 'account/view', array(
					'panes' => $panes,
				) );
			global $pages;
			$pages = array( $view );
		}
	}
	// Filter the array of panes for the account view page
	public function get_panes( array $panes, WP_Groupbuy_Account $account ) {
		$panes['contact'] = array(
			'weight' => 0,
			'body' => self::load_view_to_string( 'account/contact-info', array(
					'first_name' => $account->get_name( 'first' ),
					'last_name' => $account->get_name( 'last' ),
					'name' => $account->get_name(),
					'address' => $account->get_address(),
					'email' => $account->get_name( 'email' ),
					'phone' => $account->get_name( 'phone' ),
				'mailinglist' => $account->get_user_mailinglists(),
				) ),
		);
		return $panes;
	}
	// Filter 'the_title' to display the title of the page rather than the user name
	public function get_title(  $title, $post_id  ) {
		$post = get_post( $post_id );
		if ( $post->post_type == WP_Groupbuy_Account::POST_TYPE ) {
			return self::__( "Your Account" );
		}
		return $title;
	}
	public static function add_link_to_admin_bar( $items ) {
		$items[] = array(
			'id' => 'edit_accounts',
			'title' => self::__( 'Edit Accounts' ),
			'href' => wg_admin_url( 'voucher_records&tab=wp-groupbuy/account_records' ),
			'weight' => 5,
		);
		return $items;
	}
	public static function display_table() {
		//Create an instance of our package class...
		$wp_list_table = new WP_Groupbuy_Accounts_Table();
		//Fetch, prepare, sort, and filter our data...
		$wp_list_table->prepare_items();
?>
		<script type="text/javascript" charset="utf-8">
			jQuery(document).ready(function($){
				jQuery(".wg_suspend").on('click', function(event) {
					event.preventDefault();
						if( confirm( '<?php wpg_e("This will modify a users access to your site. Are you sure?") ?>' ) ) {
							var $suspend_link = $( this ),
							account_id = $suspend_link.attr( 'ref' );
							$.post( ajaxurl, { action: 'wpg_destroyer', type: 'account', id: account_id, destroyer_nonce: '<?php echo wp_create_nonce( WP_Groupbuy_Destroy::NONCE ) ?>' },
								function( data ) {
										$suspend_link.parent().html( '<?php wpg_e('Modified') ?>' );
									}
								);
						} else {
							// nothing to do.
						}
				});
			});
		</script>
		<div class="wrap">
			<?php screen_icon(); ?>
			<h2 class="nav-tab-wrapper">
				<?php self::display_admin_tabs(); ?>
			</h2>
			 <?php $wp_list_table->views() ?>
             
			<form id="payments-filter" method="get" style="margin-top:5px;">
            
				<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
                                    <input type="hidden" name="tab" value="<?php echo $_REQUEST['tab'] ?>" />
				<p class="search-box payment_search">
					<input style="width:100px;" type="text" id="payment_account_id-search-input" name="search_value" placeholder="<?php wpg_e( "Quick Search" ) ?>" value="<?php echo (isset($_GET['search_value']) ? $_GET['search_value'] : "" )?>">
                                        <?php
                                        $selectSearchBox = array(
                                            array("value" => "non_select", "text" => self::__( '- Select -' ), "selected" => "selected"),
                                            array("value" => "s", "text" => self::__( 'Account ID' ), "selected" => ""),
                                           
                                        );
                                        ?>
                                        <select name="search_type">
                                        <?php
                                        foreach($selectSearchBox as $el) {
                                             $el['selected'] = @$_GET['search_type'] == $el['value'] ? 'selected' : '';
                                             echo '<option '.$el['selected'].' value="'.$el['value'].'">'.$el['text'].'</option>';
                                        }
                                        ?>
                                        </select>
					<input type="submit" name="" id="search-submit" class="button" value="<?php self::_e( 'Search' ) ?>">
				</p>
				<?php $wp_list_table->display() ?>
                
			</form>
            
		</div>
		<?php
	}

	public static function remove_duplicated_accounts() {
		global $wpdb;
		return $count = $wpdb->query(" DELETE a.*
			FROM $wpdb->posts AS a
			   INNER JOIN (
			      SELECT post_title, MIN( id ) AS min_id
			      FROM $wpdb->posts
			      WHERE post_type = 'wg_account'
			      GROUP BY post_title
			      HAVING COUNT( * ) > 1
			   ) AS b ON b.post_title = a.post_title
			AND b.min_id <> a.id
			AND a.post_type = 'wg_account'
			");
		if ( $count ){
			wg_clean_postmeta();
		}
	}

}
