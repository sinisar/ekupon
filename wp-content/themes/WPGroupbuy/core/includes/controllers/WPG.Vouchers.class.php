<?php

add_action( 'wg_init_controllers', array( 'WP_Groupbuy_Vouchers', 'init' ), 40 );

class WP_Groupbuy_Vouchers extends WP_Groupbuy_Controller {

	const FILTER_QUERY_VAR = 'filter_wg_vouchers';
	const FILTER_EXPIRED_QUERY_VAR = 'expired';
	const FILTER_USED_QUERY_VAR = 'used';
	const FILTER_ACTIVE_QUERY_VAR = 'active';
    const FILTER_BUSINESS_QUERY_VAR = 'business';
    const FILTER_BUSINESS_ACTIVE_QUERY_VAR = 'business-active';
    const FILTER_BUSINESS_USED_QUERY_VAR = 'business-used';
    const FILTER_BUSINESS_EXPIRED_QUERY_VAR = 'business-expired';

    const VOUCHER_OPTION_EXP_PATH = 'wg_voucher_path_expired';
	const VOUCHER_OPTION_USED_PATH = 'wg_voucher_path_used';
	const VOUCHER_OPTION_ACTIVE_PATH = 'wg_voucher_path_active';
    const VOUCHER_OPTION_BUSINESS_PATH = 'wg_voucher_path_business';
    const VOUCHER_OPTION_BUSINESS_ACTIVE_PATH = 'wg_voucher_path_business-active';
    const VOUCHER_OPTION_BUSINESS_USED_PATH = 'wg_voucher_path_business-used';
    const VOUCHER_OPTION_BUSINESS_EXPIRED_PATH = 'wg_voucher_path_business-expired';

    const VOUCHER_OPTION_LOGO = 'wg_voucher_logo';
	const VOUCHER_OPTION_FINE_PRINT = 'wg_voucher_fine_print';
	const VOUCHER_OPTION_SUPPORT1 = 'wg_voucher_support_1';
	const VOUCHER_OPTION_SUPPORT2 = 'wg_voucher_support_2';
	const VOUCHER_OPTION_LEGAL = 'wg_voucher_legal';
	const VOUCHER_OPTION_PREFIX = 'wg_voucher_prefix';
	const VOUCHER_OPTION_IDS = 'wg_voucher_ids_options';
	private static $expired_path;
	private static $used_path;
	private static $active_path;
    private static $business_path;
    private static $business_active_path;
    private static $business_used_path;
    private static $business_expired_path;
	private static $voucher_logo;
	private static $voucher_fine_print;
	private static $voucher_support1;
	private static $voucher_support2;
	private static $voucher_legal;
	private static $voucher_prefix;
	private static $voucher_ids_option;

	protected static $settings_page;

	public static function init() {
		add_action( 'payment_captured', array( get_class(), 'activate_vouchers' ), 10, 2 );
		add_action( 'purchase_completed', array( get_class(), 'create_vouchers_for_purchase' ), 5, 1 );
		add_filter( 'template_include', array( get_class(), 'override_template' ) );

		self::$expired_path = get_option( self::VOUCHER_OPTION_EXP_PATH, self::FILTER_EXPIRED_QUERY_VAR );
		self::$used_path = get_option( self::VOUCHER_OPTION_USED_PATH, self::FILTER_USED_QUERY_VAR );
		self::$active_path = get_option( self::VOUCHER_OPTION_ACTIVE_PATH, self::FILTER_ACTIVE_QUERY_VAR );
        self::$business_path = get_option( self::VOUCHER_OPTION_BUSINESS_PATH, self::FILTER_BUSINESS_QUERY_VAR );
        self::$business_active_path = get_option( self::VOUCHER_OPTION_BUSINESS_ACTIVE_PATH, self::FILTER_BUSINESS_ACTIVE_QUERY_VAR );
        self::$business_used_path = get_option( self::VOUCHER_OPTION_BUSINESS_USED_PATH, self::FILTER_BUSINESS_USED_QUERY_VAR );
        self::$business_expired_path = get_option( self::VOUCHER_OPTION_BUSINESS_EXPIRED_PATH, self::FILTER_BUSINESS_EXPIRED_QUERY_VAR );

        add_filter( 'wg_rewrite_rules', array( get_class(), 'add_voucher_rewrite_rules' ), 10, 1 );
		self::register_query_var( self::FILTER_QUERY_VAR );
		add_action( 'pre_get_posts', array( get_class(), 'filter_voucher_query' ), 50, 1 );
		add_action( 'parse_query', array( get_class(), 'filter_voucher_query' ), 50, 1 );

		// Hook into WPG settings page ( after it's created )
		add_action( 'admin_init', array( get_class(), 'register_settings_fields' ), 20, 0 );

		if ( is_admin() ) {
			// Admin
			self::$settings_page = self::register_settings_page( 'voucher_records', self::__( 'Management' ), self::__( 'Management' ), 6, FALSE, 'records', array( get_class(), 'display_table' ) );
			add_action( 'parse_request', array( get_class(), 'manually_activate_vouchers' ), 1, 0 );
		}

		self::$voucher_logo = get_option( self::VOUCHER_OPTION_LOGO );
		self::$voucher_fine_print = get_option( self::VOUCHER_OPTION_FINE_PRINT );
		self::$voucher_support1 = get_option( self::VOUCHER_OPTION_SUPPORT1 );
		self::$voucher_support2 = get_option( self::VOUCHER_OPTION_SUPPORT2 );
		self::$voucher_legal = get_option( self::VOUCHER_OPTION_LEGAL );
		self::$voucher_prefix = get_option( self::VOUCHER_OPTION_PREFIX );
		self::$voucher_ids_option = get_option( self::VOUCHER_OPTION_IDS, 'random' );

		// AJAX Functions
		add_action( 'wp_ajax_wg_mark_voucher', array( get_class(), 'mark_voucher' ) );
		add_action( 'wp_ajax_nopriv_wg_mark_voucher', array( get_class(), 'mark_voucher' ) );
	}

	// A purchase has been completed. Create all the necessary vouchers and tie them to that purchase
	public static function create_vouchers_for_purchase( WP_Groupbuy_Purchase $purchase ) {
		$products = $purchase->get_products();
		$user_id = $purchase->get_user();
		$account = WP_Groupbuy_Account::get_instance( $user_id );
		foreach ( $products as $product ) {
			$deal = WP_Groupbuy_Deal::get_instance( $product['deal_id'] );
			if ( !$deal ) {
				self::set_message( sprintf( self::__( 'We experienced an error creating a voucher for deal ID %d. Please contact a site administrator for asssistance.' ), $product['deal_id'] ) );
				continue; // nothing else we can do
			}
			for ( $i = 0 ; $i < $product['quantity'] ; $i++ ) {
				$voucher_id = WP_Groupbuy_Voucher::new_voucher( $purchase->get_id(), $product['deal_id'] );
				$voucher = WP_Groupbuy_Voucher::get_instance( $voucher_id );
				$voucher->set_product_data( $product );
				$voucher->set_serial_number();
				$voucher->set_security_code();
				do_action( 'create_voucher_for_purchase', $voucher_id, $purchase, $product );
			}
		}
	}

	public static function override_template( $template ) {
		if ( WP_Groupbuy_Voucher::is_voucher_query() ) {

			// require login unless it's a validated temp access
			if ( !WP_Groupbuy_Voucher::temp_voucher_access_attempt() ) {
				self::login_required();
			}

			if ( is_single() ) {
				$template = self::locate_template( array(
						'account/voucher.php',
						'vouchers/single-voucher.php',
						'vouchers/single.php',
						'vouchers/voucher.php',
						'voucher.php',
					), $template );
			} else {
				$status = get_query_var( self::FILTER_QUERY_VAR );
				$template = self::locate_template( array(
						'account/'.$status.'-vouchers.php',
						'vouchers/'.$status.'-vouchers.php',
						'vouchers/'.$status.'.php',
						'account/vouchers.php',
						'vouchers/vouchers.php',
						'vouchers/index.php',
						'vouchers/archive.php',
						'vouchers.php',
					), $template );
			}
		}
		return $template;
	}

	// Add the rewrite rules for filtering vouchers
	public static function add_voucher_rewrite_rules( $rules ) {
		global $wp_rewrite;
		$rules[trailingslashit( WP_Groupbuy_Voucher::REWRITE_SLUG ).self::$expired_path.'(/page/?([0-9]{1,}))?/?$'] = 'index.php?post_type='.WP_Groupbuy_Voucher::POST_TYPE.'&paged='.$wp_rewrite->preg_index( 2 ).'&'.self::FILTER_QUERY_VAR.'='.self::FILTER_EXPIRED_QUERY_VAR;
		$rules[trailingslashit( WP_Groupbuy_Voucher::REWRITE_SLUG ).self::$used_path.'(/page/?([0-9]{1,}))?/?$'] = 'index.php?post_type='.WP_Groupbuy_Voucher::POST_TYPE.'&paged='.$wp_rewrite->preg_index( 2 ).'&'.self::FILTER_QUERY_VAR.'='.self::FILTER_USED_QUERY_VAR;
		$rules[trailingslashit( WP_Groupbuy_Voucher::REWRITE_SLUG ).self::$active_path.'(/page/?([0-9]{1,}))?/?$'] = 'index.php?post_type='.WP_Groupbuy_Voucher::POST_TYPE.'&paged='.$wp_rewrite->preg_index( 2 ).'&'.self::FILTER_QUERY_VAR.'='.self::FILTER_ACTIVE_QUERY_VAR;
        $rules[trailingslashit( WP_Groupbuy_Voucher::REWRITE_SLUG ).self::$business_path.'(/page/?([0-9]{1,}))?/?$'] = 'index.php?post_type='.WP_Groupbuy_Voucher::POST_TYPE.'&paged='.$wp_rewrite->preg_index( 2 ).'&'.self::FILTER_QUERY_VAR.'='.self::FILTER_BUSINESS_QUERY_VAR;
        $rules[trailingslashit( WP_Groupbuy_Voucher::REWRITE_SLUG ).self::$business_active_path.'(/page/?([0-9]{1,}))?/?$'] = 'index.php?post_type='.WP_Groupbuy_Voucher::POST_TYPE.'&paged='.$wp_rewrite->preg_index( 2 ).'&'.self::FILTER_QUERY_VAR.'='.self::FILTER_BUSINESS_ACTIVE_QUERY_VAR;
        $rules[trailingslashit( WP_Groupbuy_Voucher::REWRITE_SLUG ).self::$business_used_path.'(/page/?([0-9]{1,}))?/?$'] = 'index.php?post_type='.WP_Groupbuy_Voucher::POST_TYPE.'&paged='.$wp_rewrite->preg_index( 2 ).'&'.self::FILTER_QUERY_VAR.'='.self::FILTER_BUSINESS_USED_QUERY_VAR;
        $rules[trailingslashit( WP_Groupbuy_Voucher::REWRITE_SLUG ).self::$business_expired_path.'(/page/?([0-9]{1,}))?/?$'] = 'index.php?post_type='.WP_Groupbuy_Voucher::POST_TYPE.'&paged='.$wp_rewrite->preg_index( 2 ).'&'.self::FILTER_QUERY_VAR.'='.self::FILTER_BUSINESS_EXPIRED_QUERY_VAR;

        return $rules;

	}


	// Edit the query to remove other users vouchers
	public static function filter_voucher_query( WP_Query $query ) {
		
		if ( current_user_can("administrator") ) {
            return;
        }
		
		// error_log(print_r($query->query_vars, true));
		// we only care if this is the query for vouchers
		if ( WP_Groupbuy_Voucher::is_voucher_query( $query ) && !is_admin()  
			&& !isset( $query->query_vars['wg_bypass_filter'] ) 
			&& $query->is_main_query()
            && (isset( $query->query_vars['filter_wg_vouchers']) && strpos($query->query_vars['filter_wg_vouchers'], 'business') === false))  {
			
			$purchases = array('');

			if ( get_current_user_id() ) {
				// get all the user's purchases
				$purchases = WP_Groupbuy_Purchase::get_purchases( array(
					'user' => get_current_user_id(),
				) );
				// error_log(print_r($purchases, true));
				if(empty($purchases)) {
                    $purchases = array('');
                }
			}

			if ( !isset( $query->query_vars['meta_query'] ) || !is_array( $query->query_vars['meta_query'] ) ) {
				$query->query_vars['meta_query'] = array();
			}

			$query->query_vars['meta_query'][] = array(
				'key' => '_purchase_id',
				'value' => $purchases,
				'compare' => 'IN',
				'type' => 'NUMERIC',
			);
			// error_log(print_r($query->query_vars, true));

			switch (get_query_var( self::FILTER_QUERY_VAR )) :
				case ( self::FILTER_ACTIVE_QUERY_VAR ) :
					$query->query_vars['meta_query'][] = array(
						'key' => '_claimed',
						'compare' => 'NOT EXISTS',
					);
					break;
				case ( self::FILTER_USED_QUERY_VAR ) :
					$query->query_vars['meta_query'][] = array(
						'key' => '_claimed',
						'value' => 0,
						'compare' => '>'
					);
					break;
			endswitch;

		}
		else {
			//  url contains business - we're on business Vouchers
		    if(isset( $query->query_vars['filter_wg_vouchers'])) {
		       
                if (strpos($query->query_vars['filter_wg_vouchers'], 'business') !== false) {

                    if (!isset($query->query_vars['meta_query']) || !is_array($query->query_vars['meta_query'])) {
                        $query->query_vars['meta_query'] = array();
                    }
                    $user_deal_ids = array('');
                    
                    if (get_current_user_id()) {
                        //  load metchant_id for current logged in user
                        $user_deal_ids = array();
                        $merchant_ids = WP_Groupbuy_Merchant::find_by_meta(WP_Groupbuy_Merchant::POST_TYPE, array('_authorized_users' => get_current_user_id()));
                        $user_deal_ids = WP_Groupbuy_Deal::get_deals_by_merchant($merchant_ids[0]);
                        if(empty($user_deal_ids)) {
                            $user_deal_ids = array('');
                        }
                    }

                    $query->query_vars['meta_query'][] = array(
                        'key' => '_voucher_deal_id',
                        'value' => $user_deal_ids,
                        'compare' => 'IN',
                        'type' => 'NUMERIC',
                    );

                    switch (get_query_var(self::FILTER_QUERY_VAR)) :
                        case (self::FILTER_BUSINESS_ACTIVE_QUERY_VAR) :
                            $query->query_vars['meta_query'][] = array(

                                'key' => '_claimed',
                                'compare' => 'NOT EXISTS',
                                'value' => 'NULL'
                            );
                            break;

                    case ( self::FILTER_BUSINESS_USED_QUERY_VAR ) :
                        $query->query_vars['meta_query'][] = array(
                            'key' => '_claimed',
                            'value' => 0,
                            'compare' => '>'
                        );
                        break;

                    endswitch;

                }
            }
        }
	}

	// Get the deal IDs from a user's vouchers.
	public static function get_deal_ids( $status = NULL ) {
		$args = array(
			'post_type' => WP_Groupbuy_Voucher::POST_TYPE,
			'post_status' => 'publish',
			'posts_per_page' => -1,
			'fields' => 'ids'
		);
		$deal_ids = array();
		$status = ( NULL === $status && !get_query_var( WP_Groupbuy_Vouchers::FILTER_QUERY_VAR ) ) ? 'any' : get_query_var( WP_Groupbuy_Vouchers::FILTER_QUERY_VAR );
		foreach ( get_posts( $args ) as $voucher_id ) {
			$deal_id = get_post_meta( $voucher_id, '_voucher_deal_id', TRUE );
			if ( !in_array( $deal_id, $deal_ids ) ) {
				$claimed = get_post_meta( $voucher_id, '_claimed', TRUE );
				$exp = get_post_meta( $deal_id, '_voucher_expiration_date', TRUE );
				if ( $status == self::FILTER_USED_QUERY_VAR ) {
					if ( current_time( 'timestamp' ) < $exp || $claimed ) { // Returning expired or claimed vouchers
						$deal_ids[] = $deal_id;
					}
				}
				elseif ( $status == self::FILTER_EXPIRED_QUERY_VAR ) {
					if ( !empty( $exp ) && current_time( 'timestamp' ) > $exp ) { // Returning expired vouchers
						$deal_ids[] = $deal_id;
					}
				}
				elseif ( $status == self::FILTER_ACTIVE_QUERY_VAR ) { // return all non-expired deals if active query
					if ( !$claimed ) { // Don't included claimed vouchers
						if ( empty( $exp ) || ( !empty( $exp ) && current_time( 'timestamp' ) < $exp ) ) {
							$deal_ids[] = $deal_id;
						}
					}
				} else {
					$deal_ids[] = $deal_id;
				}
			}
		}
		return $deal_ids;
	}

	// Add the filter query var
	public function add_query_vars( $vars ) {
		array_push( $vars, self::FILTER_QUERY_VAR );
		return $vars;
	}


    public static function get_business_url() {
        return get_post_type_archive_link( WP_Groupbuy_Voucher::POST_TYPE ).self::$business_path;
    }

	public static function get_url() {
		return get_post_type_archive_link( WP_Groupbuy_Voucher::POST_TYPE );
	}

	public static function get_active_url() {
		return get_post_type_archive_link( WP_Groupbuy_Voucher::POST_TYPE ).self::$active_path;
	}

    public static function get_business_active_url() {
        return get_post_type_archive_link( WP_Groupbuy_Voucher::POST_TYPE ).self::$business_active_path;
    }

	public static function get_expired_url() {
		return get_post_type_archive_link( WP_Groupbuy_Voucher::POST_TYPE ).self::$expired_path;
	}

    public static function get_business_expired_url() {
        return get_post_type_archive_link( WP_Groupbuy_Voucher::POST_TYPE ).self::$business_expired_path;
    }

	public static function get_used_url() {
		return get_post_type_archive_link( WP_Groupbuy_Voucher::POST_TYPE ).self::$used_path;
	}

    public static function get_business_used_url() {
        return get_post_type_archive_link( WP_Groupbuy_Voucher::POST_TYPE ).self::$business_used_path;
    }

	// Activate any pending vouchers if the purchased deal is now successful
	public static function activate_vouchers( WP_Groupbuy_Payment $payment, $items_captured ) {
		$purchase_id = $payment->get_purchase();
		$purchase = WP_Groupbuy_Purchase::get_instance( $purchase_id );
		$products = $purchase->get_products();
		foreach ( $products as $product ) {
			if ( in_array( $product['deal_id'], $items_captured ) ) {
				$deal = WP_Groupbuy_Deal::get_instance( $product['deal_id'] );
				if ( $deal->is_successful() ) {
					$vouchers = WP_Groupbuy_Voucher::get_pending_vouchers( $product['deal_id'], $purchase_id );
					foreach ( $vouchers as $voucher_id ) {
						$voucher = WP_Groupbuy_Voucher::get_instance( $voucher_id );
						$voucher->activate();
					}
				}
			}
		}
	}

	public static function manually_activate_vouchers( $voucher_id = null ) {
		if ( !current_user_can( 'edit_posts' ) ) {
			return; // security check
		}
		if ( isset( $_REQUEST['activate_voucher'] ) && $_REQUEST['activate_voucher'] != '' ) {
			if ( wp_verify_nonce( $_REQUEST['_wpnonce'], 'activate_voucher' ) ) {
				$voucher_id = $_REQUEST['activate_voucher'];
			}
		}
		if ( is_numeric( $voucher_id ) ) {
			$voucher = WP_Groupbuy_Voucher::get_instance( $voucher_id );
			if ( is_a( $voucher, 'WP_Groupbuy_Voucher' ) && !$voucher->is_active() ) {
				$voucher->activate();
				return;
			}
		}
	}

	public static function mark_voucher() {
	    //  unmark_voucher => unclaimed
        //  unredeem_voucher => unredeem

		if ( isset( $_REQUEST['voucher_id'] ) && $_REQUEST['voucher_id'] ) {
			$voucher = WP_Groupbuy_Voucher::get_instance( $_REQUEST['voucher_id'] );
			// If destroying claim date
            if ( isset( $_REQUEST['unmark_voucher'] ) && $_REQUEST['unmark_voucher'] ) {
                $marked = $voucher->set_claimed_date( TRUE );
                wpg_e( 'Voucher Claim Date Removed.' );
                exit();
            }

            if ( isset( $_REQUEST['claim_voucher'] ) && $_REQUEST['claim_voucher'] ) {
                $claimed_date = $voucher->set_claimed_date(  );
                return $claimed_date;
                //wpg_e( 'Voucher Claimed' );
            }


            if ( isset( $_REQUEST['unredeem_voucher'] ) && $_REQUEST['unredeem_voucher'] ) {
                $marked = $voucher->reset_redeption_data();
                wpg_e( 'Voucher Redemption Data Removed.' );
                exit();
            }

            //  else set redeement data

            //$marked = $voucher->set_claimed_date();
			$data = array(
				'date' => date( get_option( 'date_format' ), current_time( 'timestamp', 1 ) ),
				'notes' => wpg__( 'Customer marked voucher as redeemed.' )
			);
			$voucher->set_redemption_data( $data );
            $marked = $voucher->get_redemption_data()['date'];
            print $marked;
            //return print date( get_option( 'date_format' ), $marked );
			//echo apply_filters( 'wg_ajax_mark_voucher', date( get_option( 'date_format' ), $marked ) );
			//exit();
		}
		exit();
	}

	public static function register_settings_fields() {
		$page = WP_Groupbuy_UI::get_settings_page();
		$section = 'wg_general_voucher_settings';
		add_settings_section( $section, self::__( 'Voucher Settings' ), array( get_class(), 'display_settings_section' ), $page );
		// Settings
		register_setting( $page, self::VOUCHER_OPTION_LOGO );
		register_setting( $page, self::VOUCHER_OPTION_FINE_PRINT );
		register_setting( $page, self::VOUCHER_OPTION_SUPPORT1 );
		register_setting( $page, self::VOUCHER_OPTION_SUPPORT2 );
		register_setting( $page, self::VOUCHER_OPTION_LEGAL );
		register_setting( $page, self::VOUCHER_OPTION_PREFIX );
		register_setting( $page, self::VOUCHER_OPTION_IDS );

		add_settings_field( self::VOUCHER_OPTION_LOGO, self::__( 'Logo image URL' ), array( get_class(), 'display_voucher_option_logo' ), $page, $section );
		add_settings_field( self::VOUCHER_OPTION_FINE_PRINT, self::__( 'Fine Print' ), array( get_class(), 'display_voucher_option_fine_print' ), $page, $section );
		add_settings_field( self::VOUCHER_OPTION_SUPPORT1, self::__( 'Support Contact' ), array( get_class(), 'display_voucher_option_support1' ), $page, $section );
		add_settings_field( self::VOUCHER_OPTION_SUPPORT2, self::__( 'Another Support Contact' ), array( get_class(), 'display_voucher_option_support2' ), $page, $section );
		add_settings_field( self::VOUCHER_OPTION_LEGAL, self::__( 'Legal Information' ), array( get_class(), 'display_voucher_option_legal' ), $page, $section );
		add_settings_field( self::VOUCHER_OPTION_PREFIX, self::__( 'Voucher Prefix' ), array( get_class(), 'display_voucher_option_prefix' ), $page, $section );

	}

	public static function display_voucher_option_logo() { ?>
    
    <script type="text/javascript">
			jQuery(document).ready( function($) {
				$('#upload_logo_button').click(function() {
				//tb_show('Upload a logo', 'media-upload.php?referer=wpg-upload-settings&amp;type=image&amp;TB_iframe=true&amp;post_id=0', false);
					return false;
				});
			window.send_to_editor = function(html) {
				var image_url = $('img',html).attr('src');
				//alert(html);
				$('#wg_voucher_logo').val(image_url);
				tb_remove();
				$('#upload_logo_preview img').attr('src',image_url);
				$('#submit_options_form').trigger('click');

			}
		</script>
    
    <?php
		echo '<input type="text" id="'.self::VOUCHER_OPTION_LOGO.'" name="'.self::VOUCHER_OPTION_LOGO.'" value="'.self::$voucher_logo.'" size="40">';
		echo '<img width="16" height="16" src="'.WG_RESOURCES . 'images/help.png'. '" class="help_tip" title="'.self::__( 'Copy URL of image here or click upload image button below. To remove image, just delete URL path on text field and save changes' ).'">';
                echo '<br/>';
                require_once WG_PATH.'/includes/upload.php';
                wpg_options_enqueue_scripts();
                wpg_setting_logo(self::$voucher_logo);
                wpg_setting_logo_preview(self::$voucher_logo);
	}
	public static function display_voucher_option_fine_print() {
		echo '<textarea rows="3" cols="42" name="'.self::VOUCHER_OPTION_FINE_PRINT.'" id="'.self::VOUCHER_OPTION_FINE_PRINT.'" class="tinymce" style="width:400">'.esc_textarea( self::$voucher_fine_print ).'</textarea>';
		echo '<img width="16" height="16" src="'.WG_RESOURCES . 'images/help.png'. '" class="help_tip" title="'.self::__( 'If you leave blank Fine Print field when adding Deal, this content will be replaced on voucher' ).'">';
	}
	public static function display_voucher_option_support1() {
		echo '<input type="text" name="'.self::VOUCHER_OPTION_SUPPORT1.'" value="'.self::$voucher_support1.'" size="40">';
		echo '<img width="16" height="16" src="'.WG_RESOURCES . 'images/help.png'. '" class="help_tip" title="'.self::__( 'Add contact information' ).'">';
	}
	public static function display_voucher_option_support2() {
		echo '<input type="text" name="'.self::VOUCHER_OPTION_SUPPORT2.'" value="'.self::$voucher_support2.'" size="40">';
		echo '<img width="16" height="16" src="'.WG_RESOURCES . 'images/help.png'. '" class="help_tip" title="'.self::__( 'Add another contact information if any' ).'">';
	}
	public static function display_voucher_option_legal() {
		echo '<textarea rows="3" cols="42" name="'.self::VOUCHER_OPTION_LEGAL.'" id="'.self::VOUCHER_OPTION_LEGAL.'" class="tinymce" style="width:400">'.esc_textarea( self::$voucher_legal ).'</textarea>';
		echo '<img width="16" height="16" src="'.WG_RESOURCES . 'images/help.png'. '" class="help_tip" title="'.self::__( 'Add legal information if available' ).'">';
	}
	public static function display_voucher_option_prefix() {
		echo '<input type="text" name="'.self::VOUCHER_OPTION_PREFIX.'" value="'.self::$voucher_prefix.'">';
		echo '<img width="16" height="16" src="'.WG_RESOURCES . 'images/help.png'. '" class="help_tip" title="'.self::__( 'If you want to add value before the voucher code, you can change Voucher Prefix setting.<br/>Ex: You enter <b>WPG-</b>, your voucher will be <b>WPG-gn5N-YkZY-8Tj5</b>' ).'">';
	}
	public static function display_voucher_option_ids() {
		echo '<select name="'.self::VOUCHER_OPTION_IDS.'"><option value="random" '.selected( 'random', self::$voucher_ids_option, FALSE ).'>Random</option><option value="sequantial" '.selected( 'sequantial', self::$voucher_ids_option, FALSE ).'>Sequential</option><option value="none" '.selected( 'none', self::$voucher_ids_option, FALSE ).'>None</option>';
	}

	public static function get_voucher_logo() {
		return self::$voucher_logo;
	}
	public static function get_voucher_fine_print() {
		return self::$voucher_fine_print;
	}
	public static function get_voucher_support1() {
		return self::$voucher_support1;
	}
	public static function get_voucher_support2() {
		return self::$voucher_support2;
	}
	public static function get_voucher_legal() {
		return self::$voucher_legal;
	}
	public static function get_voucher_prefix() {
		return self::$voucher_prefix;
	}
	public static function get_voucher_option_ids() {
		return self::$voucher_ids_option;
	}



	public static function display_table() {
		//Create an instance of our package class...
		$wp_list_table = new WP_Groupbuy_Vouchers_Table();
		//Fetch, prepare, sort, and filter our data...
		$wp_list_table->prepare_items();

		?>
		<script type="text/javascript" charset="utf-8">
			jQuery(document).ready(function($){
				jQuery(".wg_activate").on('click', function(event) {
					event.preventDefault();
						if( confirm( '<?php wpg_e( "Are you sure?" ) ?>' ) ){
							var $link = $( this ),
							voucher_id = $link.attr( 'ref' );
							url = $link.attr( 'href' );
							$( "#"+voucher_id+"_activate" ).fadeOut('slow');
							$.post( url, { activate_voucher: voucher_id },
								function( data ) {
										$( "#"+voucher_id+"_activate_result" ).append( '<?php self::_e( 'Activated' ) ?>' ).fadeIn();
									}
								);
						} else {
							// nothing to do.
						}
				});
				jQuery(".wg_deactivate").on('click', function(event) {
					event.preventDefault();
						if( confirm( '<?php wpg_e( "Are you sure?" ) ?>' ) ) {
							var $deactivate_button = $( this ),
							deactivate_voucher_id = $deactivate_button.attr( 'ref' );
							$( "#"+deactivate_voucher_id+"_deactivate" ).fadeOut('slow');
							$.post( ajaxurl, { action: 'wpg_deactivate_voucher', voucher_id: deactivate_voucher_id, deactivate_voucher_nonce: '<?php echo wp_create_nonce( WP_Groupbuy_Destroy::NONCE ) ?>' },
								function( data ) {
										$( "#"+deactivate_voucher_id+"_deactivate_result" ).append( '<?php self::_e( 'Deactivated' ) ?>' ).fadeIn();
									}
								);
						} else {
							// nothing to do.
						}
				});
				jQuery(".wg_destroy").on('click', function(event) {
					event.preventDefault();
						if( confirm( '<?php wpg_e( "Are you sure? You are about to permanently delete the voucher from the database and remove records of it\'s existence from the related purchase and payment(s) which cannot be reversed. This will not reverse any payments or provide a credit to the customer." ) ?>' ) ) {
							var $destroy_link = $( this ),
							destroy_voucher_id = $destroy_link.attr( 'ref' );
							$.post( ajaxurl, { action: 'wpg_destroyer', type: 'voucher', id: destroy_voucher_id, destroyer_nonce: '<?php echo wp_create_nonce( WP_Groupbuy_Destroy::NONCE ) ?>' },
								function( data ) {
										$destroy_link.parent().parent().parent().parent().fadeOut();
									}
								);
						} else {
							// nothing to do.
						}
				});
				jQuery(".wg_unclaim").on('click', function(event) {
					event.preventDefault();
						if(confirm("Are you sure?")){
							var $unclaim_button = $( this ),
							unclaim_voucher_id = $unclaim_button.attr( 'ref' );
							$( "#"+unclaim_voucher_id+"_unclaim" ).fadeOut('slow');
							$.post( ajaxurl, { action: 'wg_mark_voucher', voucher_id: unclaim_voucher_id, unmark_voucher: 1 },
								function( data ) {
										$( "#"+unclaim_voucher_id+"_unclaim_result" ).append( '<?php wpg_e( "Voucher Claimed Removed." ) ?>' ).fadeIn();
									}
								);
						} else {
							// nothing to do.
						}
				});


                jQuery(".wg_unredeem").on('click', function(event) {
                    event.preventDefault();
                    if(confirm("Are you sure?")){
                        var $unredeem_button = $( this ),
                            unredeem_voucher_id = $unredeem_button.attr( 'ref' );
                        $( "#"+unredeem_voucher_id+"_unredeem" ).fadeOut('slow');
                        $.post( ajaxurl, { action: 'wg_mark_voucher', voucher_id: unredeem_voucher_id, unredeem_voucher: 1 },
                            function( data ) {
                                $( "#"+unredeem_voucher_id+"_unredeem_result" ).append( '<?php wpg_e( "Voucher Redemption Removed." ) ?>' ).fadeIn();
                            }
                        );
                    } else {
                        // nothing to do.
                    }
                });
			});
		</script>
		<style type="text/css">
			#voucher_deal_id-search-input, #voucher_purchase_id-search-input, #voucher_account_id-search-input, #voucher_id-search-input { width:5em; margin-left: 10px;}
		</style>
		<div class="wrap">
			<?php screen_icon(); ?>
			<h2 class="nav-tab-wrapper">
				<?php self::display_admin_tabs(); ?>
			</h2>

			 <?php $wp_list_table->views() ?>
             
			<form id="payments-filter" method="get" style="margin-top:5px;">
				<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
                                <input type="hidden" name="tab" value="<?php echo @$_REQUEST['tab'] ?>" />
				<p class="search-box payment_search">
					<input style="width:150px;" type="text" id="payment_account_id-search-input" name="search_value" placeholder="<?php wpg_e( "Quick Search" ) ?>" value="<?php echo (isset($_GET['search_value']) ? $_GET['search_value'] : "" )?>">
                                        <?php
                                        $selectSearchBox = array(
                                            array("value" => "user_email", "text" => self::__( 'User email' ), "selected" => "selected"),
                                            array("value" => "account_id", "text" => self::__( 'Account ID' ), "selected" => ""),
                                            array("value" => "purchase_id", "text" => self::__( 'Order ID' ), "selected" => ""),
                                            array("value" => "deal_id", "text" => self::__( 'Deal ID' ), "selected" => ""),
                                            array("value" => "s", "text" => self::__( 'Voucher ID' ), "selected" => ""),
                                           
                                        );
                                        ?>
                                        <select name="search_type">
                                        <?php
                                        foreach($selectSearchBox as $el) {
                                             $el['selected'] = @$_GET['search_type'] == $el['value'] ? 'selected' : '';
                                             echo '<option '.$el['selected'].' value="'.$el['value'].'">'.$el['text'].'</option>';
                                        }
                                        ?>
                                        </select>
					<input type="submit" name="" id="search-submit" class="button" value="<?php self::_e( 'Search' ) ?>">
				</p>
				<?php $wp_list_table->display() ?>
			</form>
		</div>
		<?php
	}
}


if ( !class_exists( 'WP_List_Table' ) ) {
	require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
}
class WP_Groupbuy_Vouchers_Table extends WP_List_Table {
	protected static $post_type = WP_Groupbuy_Voucher::POST_TYPE;

	function __construct() {
		global $status, $page;

		//Set parent defaults
		parent::__construct( array(
				'singular' => wpg__( 'voucher' ),     // singular name of the listed records
				'plural' => wpg__( 'vouchers' ), // plural name of the listed records
				'ajax' => false     // does this table support ajax?
			) );

	}

	function get_views() {

		$status_links = array();
		$num_posts = wp_count_posts( self::$post_type, 'readable' );
		$class = '';
		$allposts = '';

		$total_posts = array_sum( (array) $num_posts );

		// Subtract post types that are not included in the admin all list.
		foreach ( get_post_stati( array( 'show_in_admin_all_list' => false ) ) as $state )
			$total_posts -= $num_posts->$state;

		$class = empty( $_REQUEST['post_status'] ) ? ' class="current"' : '';
		$status_links['all'] = "<a href='admin.php?page=wp-groupbuy/voucher_records{$allposts}'$class>" . sprintf( _nx( 'All <span class="count">(%s)</span>', 'All <span class="count">(%s)</span>', $total_posts, 'posts' ), number_format_i18n( $total_posts ) ) . '</a>';

		foreach ( get_post_stati( array( 'show_in_admin_status_list' => true ), 'objects' ) as $status ) {
			$class = '';

			$status_name = $status->name;

			if ( empty( $num_posts->$status_name ) )
				continue;

			if ( isset( $_REQUEST['post_status'] ) && $status_name == $_REQUEST['post_status'] )
				$class = ' class="current"';

			// replace "Published" with "Complete".
			$label = str_replace( array( 'Published', 'Trash' ), array( 'Active', 'Deactived' ), translate_nooped_plural( $status->label_count, $num_posts->$status_name ) );
			$status_links[$status_name] = "<a href='admin.php?page=wp-groupbuy/voucher_records&post_status=$status_name'$class>" . sprintf( $label, number_format_i18n( $num_posts->$status_name ) ) . '</a>';
		}

		return $status_links;
	}

	function extra_tablenav( $which ) {
		?>
		<div class="alignleft actions"> <?php
		if ( 'top' == $which && !is_singular() ) {

			$this->months_dropdown( self::$post_type );

			submit_button( __( 'Filter' ), 'secondary', false, false, array( 'id' => 'post-query-submit' ) );
		} ?>
		</div> <?php
	}


	// Text or HTML to be placed inside the column <td>
	function column_default( $item, $column_name ) {
		switch ( $column_name ) {
		default:
			return apply_filters( 'wg_mngt_vouchers_column_'.$column_name, $item );
		}
	}


	// Text to be placed inside the column <td> (movie title only)
	function column_title( $item ) {
		$voucher = WP_Groupbuy_Voucher::get_instance( $item->ID );
		$purchase = $voucher->get_purchase();
		if ( !is_a( $purchase, 'WP_Groupbuy_Purchase' ) ) {
			return '<p style="color:#BC0B0B">' . wpg__( 'ERROR: Order not found.' ) . '</span>';
		}
		$user_id = $purchase->get_user();
		$account = WP_Groupbuy_Account::get_instance( $user_id );
		$deal_id = $voucher->get_post_meta( '_voucher_deal_id' );


		//Build row actions
		$actions = array(
			'deal'    => sprintf( '<a href="%s">'.wpg__( 'Deal' ).'</a>', get_edit_post_link( $deal_id ) ),
			'purchase'    => sprintf( '<a href="admin.php?page=wp-groupbuy/voucher_records&tab=wp-groupbuy/purchase_records&s=%s">'.wpg__( 'Order' ).'</a>', $purchase->get_id() )
		);
		if ( $user_id == -1 ) { // gifts
			$purchaser = array(
				'purchaser'    => sprintf( '<a href="admin.php?page=wp-groupbuy/voucher_records&tab=wp-groupbuy/gift_records&s=%s">'.wpg__( 'Gift' ).'</a>', $purchase->get_id() ),
			);
		} else {
			$purchaser = array(
				'purchaser'    => sprintf( '<a href="%s">'.wpg__( 'Purchaser' ).'</a>', get_edit_post_link( $account->get_id() ) ),
			);
		}

		$actions = array_merge( $actions, $purchaser );

		//Return the title contents
		return sprintf( wpg__(  '%1$s <span style="color:silver">(voucher&nbsp;id:%2$s)</span>%3$s' ),
			get_the_title( $item->ID ),
			$item->ID,
			$this->row_actions( $actions )
		);
	}

	function column_code( $item ) {
		$voucher = WP_Groupbuy_Voucher::get_instance( $item->ID );
		echo $voucher->get_serial_number();
	}

	function column_manage( $item ) {
		$voucher_id = $item->ID;
		if ( get_post_status( $voucher_id ) != 'publish' ) {
			$activate_path = 'edit.php?post_type=wg_deal&activate_voucher='.$voucher_id.'&_wpnonce='.wp_create_nonce( 'activate_voucher' );
			echo '<p><span id="'.$voucher_id.'_activate_result"></span><a href="'.admin_url( $activate_path ).'" class="wg_activate button" id="'.$voucher_id.'_activate" ref="'.$voucher_id.'">'.wpg__( 'Activate' ).'</a></p>';
		} else {
			echo '<p><span id="'.$voucher_id.'_deactivate_result"></span><a href="javascript:void(0)" class="wg_deactivate button disabled" id="'.$voucher_id.'_deactivate" ref="'.$voucher_id.'">'.wpg__( 'Deactivate' ).'</a></p>';
		}
	}

	function column_status( $item ) {
		$voucher_id = $item->ID;
		$voucher = WP_Groupbuy_Voucher::get_instance( $voucher_id );

		$actions = array(
			'trash'    => '<span id="'.$voucher_id.'_destroy_result"></span><a href="javascript:void(0)" class="wg_destroy" id="'.$voucher_id.'_destroy" ref="'.$voucher_id.'">'.wpg__( 'Delete' ).'</a>',
		);

		$status = ucfirst( str_replace( 'publish', 'active', $item->post_status ) );
		$status .= '<br/><span style="color:silver">';
		$status .= mysql2date( get_option( 'date_format' ).' - '.get_option( 'time_format' ), $item->post_date );
		$status .= '</span>';
		$status .= $this->row_actions( $actions );
		return $status;
	}

	function column_claimed( $item ) {
		$voucher = WP_Groupbuy_Voucher::get_instance( $item->ID );
		$claim_date = $voucher->get_claimed_date();
		$status = '';
		if ( $claim_date ) {
			$status = '<p>' . date_i18n( 'd.m.Y H:i:s', $claim_date ) . '</p>';
			$status .= '<p><span id="'.$item->ID.'_unclaim_result"></span><a href="javascript:void(0)" class="wg_unclaim button disabled" id="'.$item->ID.'_unclaim" ref="'.$item->ID.'">'.wpg__( 'Remove Clamied' ).'</a></p>';
		}
		return $status;
	}

	function column_redeemed ( $item ) {
        $voucher = WP_Groupbuy_Voucher::get_instance( $item->ID );
        $redemption_data = $voucher->get_redemption_data();
        $status = '';
        
        if ( $redemption_data ) {
            $status = '<p>' . $redemption_data['date'] . '</p>';
            $status .= '<p><span id="'.$item->ID.'_unredeem_result"></span><a href="javascript:void(0)" class="wg_unredeem button disabled" id="'.$item->ID.'_unredeem" ref="'.$item->ID.'">'.wpg__( 'Remove Redemption' ).'</a></p>';
        }
        return $status;
    }

	function column_download( $item ) {
        $voucher = WP_Groupbuy_Voucher::get_instance( $item->ID );
        $status = '<p><span id="download_'.$voucher->get_id().'"><a href="'.wg_get_voucher_permalink($voucher->get_id()).'" target="_blank" id="'.$voucher->get_id().'">Open voucher</a></span></p>';
	    return $status;
    }


	function get_columns() {
		$columns = array(
			'status'  => wpg__( 'Status' ),
			'title'  => wpg__( 'Order' ),
			'code'  => wpg__( 'Code' ),
			'manage'  => wpg__( 'Manage Status' ),
			'claimed'  => wpg__( 'Claimed' ),
            'redeemed' => wpg__('Redeemed'),
            'download'  => wpg__( 'Action' )
		);
		return apply_filters( 'wg_mngt_vouchers_columns', $columns );
	}

	function get_sortable_columns() {
		$sortable_columns = array(
		);
		return apply_filters( 'wg_mngt_vouchers_sortable_columns', $sortable_columns );
	}


	// Bulk actions are an associative array in the format
	function get_bulk_actions() {
		$actions = array();
		return apply_filters( 'wg_mngt_vouchers_bulk_actions', $actions );
	}


	// Prep data.
	function prepare_items() {

		// First, lets decide how many records per page to show
		$per_page = 25;

		// Define our column headers.
		$columns = $this->get_columns();
		$hidden = array();
		$sortable = $this->get_sortable_columns();


		// Build an array to be used by the class for column headers.
		$this->_column_headers = array( $columns, $hidden, $sortable );

		$filter = ( isset( $_REQUEST['post_status'] ) ) ? $_REQUEST['post_status'] : array( 'publish', 'pending', 'draft', 'future' );
		$args=array(
			'post_type' => WP_Groupbuy_Voucher::POST_TYPE,
			'post_status' => $filter,
			'posts_per_page' => $per_page,
			'paged' => $this->get_pagenum()
		);
		if ( isset( $_GET['search_type'] ) && $_GET['search_type'] == 'purchase_id' && $_GET['search_value'] != '' ) {

			if ( WP_Groupbuy_Purchase::POST_TYPE != get_post_type( $_GET['search_value'] ) )
				return; // not a valid search

			$vouchers = WP_Groupbuy_Voucher::get_vouchers_for_purchase( $_GET['search_value'] );
			if ( empty( $vouchers ) )
				return;

			$args = array_merge( $args, array( 'post__in' => $vouchers ) );
		}
		// Check purchases based on Deal ID
		if ( isset( $_GET['search_type'] ) && $_GET['search_type'] == 'deal_id' && $_GET['search_value'] != '') {

			if ( WP_Groupbuy_Deal::POST_TYPE != get_post_type( $_GET['search_value'] ) )
				return; // not a valid search

			$vouchers = WP_Groupbuy_Voucher::get_vouchers_for_deal( $_GET['search_value'] );
			if ( empty( $vouchers ) )
				return;

			$args = array_merge( $args, array( 'post__in' => $vouchers ) );
		}
		// Check payments based on Account ID
		if ( isset( $_GET['search_type'] ) && $_GET['search_type'] == 'account_id' && $_GET['search_value'] != '' ) {
            $args = $this->search_by_account($args, $_GET['search_value']);
		}
		// Search
		if ( isset( $_GET['search_type'] ) && $_GET['search_type'] == 's' && $_GET['search_value'] != '' ) {
			$args = array_merge( $args, array( 'p' => $_GET['search_value'] ) );
		}

        //  search by user email
        if( isset($_GET['search_type']) && $_GET['search_type'] == 'user_email' && $_GET['search_value'] != '' ) {
            $user_email = $_GET['search_value'];
            $user = get_user_by( 'email', $user_email );

            if($user != null) {
                $account = WP_Groupbuy_Account::get_instance($user->ID);
                $account_id = $account->get_account_id_for_user($user->ID);
                $args = $this->search_by_account($args, $account_id);
            }

        }

		// Filter by date
		if ( isset( $_GET['m'] ) && $_GET['m'] != '' ) {
			$args = array_merge( $args, array( 'm' => $_GET['m'] ) );
		}
		$vouchers = new WP_Query( $args );

		// Sorted data to the items property, where it can be used by the rest of the class.
		$this->items = apply_filters( 'wg_mngt_vouchers_items', $vouchers->posts );

		// Register our pagination options & calculations.
		$this->set_pagination_args( array(
				'total_items' => $vouchers->found_posts,
				'per_page'  => $per_page,
				'total_pages' => $vouchers->max_num_pages
			) );
	}

	function search_by_account ( $args, $account_id ) {
        if ( WP_Groupbuy_Account::POST_TYPE != get_post_type( $account_id ) )
            return; // not a valid search

        $purchase_ids = WP_Groupbuy_Purchase::get_purchases( array( 'account' => $account_id ) );

        $meta_query = array(
            'meta_query' => array(
                array(
                    'key' => '_purchase_id',
                    'value' => $purchase_ids,
                    'type' => 'numeric',
                    'compare' => 'IN'
                )
            ) );
        $args = array_merge( $args, $meta_query );
        return $args;
    }


}


