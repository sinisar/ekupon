<?php

add_action( 'wg_init_controllers', array( 'WP_Groupbuy_Subscription_Lightbox_form', 'init' ), 125 );

// Subscription Lightbox Form
class WP_Groupbuy_Subscription_Lightbox_form extends WP_Groupbuy_Controller {
	// Leave it empty
}
class WP_Groupbuy_Subscription_Lightbox extends WP_Groupbuy_Controller {
	const BG_COLOR_OPTION = 'wg_lightbox_color_option';
	const BG_OPACITY_OPTION = 'wg_lightbox_opacity_option';
	private static $opacity;
	private static $color;
	public static function init() {
		self::$color = get_option( self::BG_COLOR_OPTION, '000000' );
		self::$opacity = get_option( self::BG_OPACITY_OPTION, 80 );
		// Redirect
		add_filter( 'wg_get_theme_custom_css', array( get_class(), 'custom_css' ) );
		add_filter( 'wp_enqueue_scripts', array( get_class(), 'enqueue' ) );
		add_filter( 'wp_footer', array( get_class(), 'footer_script' ) );
		// Options
		add_action( 'admin_init', array( get_class(), 'register_settings_fields' ), 10, 0 );
		// Ajax
		add_action( 'wp_ajax_nopriv_wg_close_lightbox', array( get_class(), 'ajax_close_lightbox' ) );
	}
	public function ajax_close_lightbox() {
		if (isset($_POST['lightbox']) && isset($_POST['lightbox']) != '') {
			$_SESSION['wg_session_location_preference'] = $_POST['lightbox'];
		}
	}
	public static function enqueue() {
		if ( !self::show_lightbox() ) {
			return;
		}
		wp_enqueue_script( 'jquery-wg-lightbox', WG_RESOURCES.'/js/jquery.simplemodal.js', array( 'jquery' ), '1.4.4', true );
		wp_enqueue_script( 'select2', WG_RESOURCES . '/js/select2/select2.js', array( 'jquery' ), WP_Groupbuy::WG_VERSION );
		wp_enqueue_style( 'select2_css', WG_RESOURCES . '/js/select2/select2.css' );
		wp_enqueue_style( 'css-wg-lightbox',  WG_RESOURCES. "/css/lightbox.css");
	}
	public static function show_lightbox() {
		$bool = TRUE;
		if ( wg_on_login_page() || wg_on_reset_password_page() || 1 == get_query_var( WP_Groupbuy_Accounts_Registration::REGISTER_QUERY_VAR ) || 1 == @$_GET['lb-bypass'] || wg_has_location_preference() || wg_has_session_location_preference() || is_user_logged_in() || wg_on_cart_page() || 1 == get_query_var( WP_Groupbuy_Checkouts::CHECKOUT_QUERY_VAR ) ) {
			$bool = FALSE;
		}
		return apply_filters( 'wg_lb_show_lightbox', $bool );
	}
	public static function footer_script() {
		if ( !self::show_lightbox() ) {
			return;
		}
		?>
		<div id="wg_lightbox_subscription_form_wrap">
		<?php echo self::subscription_form() ?>
		</div>
		<?php
		ob_start();
		?>
		<script type="text/javascript" charset="utf-8">
		
		// jQuery('#wg_lightbox_subscription_form').validate({
		// 	rules: {
		// 		email_address: {
		// 			minlength: 2,
		// 			required: true
		// 		}
		// 	},
		// 	highlight: function(element) {
		// 		jQuery(element).closest('.box_email_form').addClass('box_error');
		// 	}
		// });
		jQuery(document).ready(function() {
			jQuery("#wg_light_box").modal({
opacity:<?php echo self::$opacity ?>,
focus: false,
overlayCss: {backgroundColor:"#<?php echo self::$color ?>"}
			});
			jQuery("#continue-button").live('click', function() {
				var deal_location = jQuery("#e1 option:selected").val();
				if (deal_location != 'none') {
					jQuery("#box_location").animate({
left: '-9999px'
					}, 400);
					
					var windowsize = jQuery(window).width();
					if (windowsize < 767) {
						jQuery("#box_email").animate({
left: '0%'
						}, 400);
					}else{
						jQuery("#box_email").animate({
left: '50%'
						}, 400);
					}
				} else {
					return false;
				}
				
			});
			function validate_form(form_id) { 
				var reg1 = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
				var address1 = document.forms[form_id].elements["email_address"].value;
				if(reg1.test(address1) == false){
					jQuery('#email_address').addClass('box_error');
					// alert("Please enter your Email!");
					return false;
				} 
				return true;
			}
			jQuery("#wg_subscription").live('click', function() {
				if (validate_form("wg_lightbox_subscription_form")) {
					jQuery('#wg_lightbox_subscription_form').submit();
					// return true;
				}
			});
			jQuery("#wg_skip").live('click', function() {
				jQuery('#wg_lightbox_subscription_form').submit();
			});
			
		});
		</script>
		<?php
		$js = ob_get_clean();
		print apply_filters( 'wg_lightbox_footer_script', $js, self::$color, self::$opacity );
	}
	public static function custom_css( $css ) {
		ob_start();
		include WG_PATH . 'resources/css/lightbox.css';
		?>
		<?php
		$new_css = ob_get_clean();
		$css .= apply_filters( 'wg_lightbox_custom_css', $new_css );
		return $css;
	}
	public static function subscription_form( $show_locations = TRUE, $select_location_text = 'Select A Location:', $button_text = 'Sign Up!' ) {
		ob_start();
		include WG_PATH.'/templates/subscription/subscription-form.php';
		$view = ob_get_clean();
		return apply_filters( 'wg_lightbox_subscription_form', $view, $show_locations, $select_location_text, $button_text );
	}
	public static function register_settings_fields() {
		$page = 'wp-groupbuy/subscription';
		$section = 'wg_lightbox_settings';
		add_settings_section( $section, self::__( 'Subscription Lightbox Settings' ), array( get_class(), 'display_settings_section' ), $page );
		// Settings
		register_setting( $page, self::BG_COLOR_OPTION );
		register_setting( $page, self::BG_OPACITY_OPTION );
		// Fields
		add_settings_field( self::BG_COLOR_OPTION, self::__( 'Lightbox Background Color' ), array( get_class(), 'display_color_option' ), $page, $section );
		add_settings_field( self::BG_OPACITY_OPTION, self::__( 'Lightbox Background Opacity' ), array( get_class(), 'display_opacity_option' ), $page, $section );
	}
	public static function display_opacity_option() {
		echo '<input name="'.self::BG_OPACITY_OPTION.'" id="'.self::BG_OPACITY_OPTION.'" type="text" maxlength="100" value="'.self::$opacity.'" style="width: 3em;">%';
	}
	public static function display_color_option() {
		echo '#<input name="'.self::BG_COLOR_OPTION.'" id="'.self::BG_COLOR_OPTION.'" type="text" maxlength="6" class="color_picker" value="'.self::$color.'" style="width: 5em;">';
	}
}
?>