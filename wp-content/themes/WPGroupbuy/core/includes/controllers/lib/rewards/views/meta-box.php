<p>
	<label for="purchase_reward"><?php __('Purchase Reward:') ?> </label><input type="text" value="<?php echo $reward; ?>" name="purchase_reward" id="purchase_reward" placeholder="0" />
</p>
<p><label for="purchase_reward_qty_option"><input type="checkbox" name="purchase_reward_qty_option" id="purchase_reward_qty_option" <?php checked($qty_option, '1'); ?> value="TRUE"/> <?php _e('Give reward for each purchase.'); ?></label></p>
