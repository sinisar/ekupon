<fieldset id="wg-account-disocunt-info">
	<legend><?php self::_e('Discount Code'); ?></legend>
	<table class="discount">
		<tbody>
			<?php foreach ( $fields as $key => $data ): ?>
				<tr>
					<?php if ( $data['type'] != 'checkbox' ): ?>
						<td><?php wg_form_label($key, $data, 'discount'); ?></td>
						<td>
							<?php wg_form_field($key, $data, 'discount'); ?>
							<?php if ( $data['desc'] != '' ): ?>
								<br/><small><?php echo $data['desc']  ?></small>	
							<?php endif ?>
						</td>
					<?php else: ?>
						<td colspan="2">
							<label for="wg_discount_<?php echo $key; ?>"><?php wg_form_field($key, $data, 'discount'); ?> <?php echo $data['label']; ?></label>
						</td>
					<?php endif; ?>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</fieldset>