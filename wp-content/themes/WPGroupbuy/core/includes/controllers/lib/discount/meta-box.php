<script type="text/javascript" charset="utf-8">
	jQuery(document).ready(function($){
		jQuery('#exp').datetimepicker({minDate: 0});

		jQuery('#code').blur(function() {
			$('#ajax_check_message').removeClass().addClass('checking').text('Checking...').fadeIn();
			$.post( '<?php echo admin_url(); ?>edit.php', {
					check_coupon_code: $(this).val(),
					nonce: '<?php echo wp_create_nonce('check_coupon_code'); ?>',
					post_id: '<?php if (isset($_GET["post"])) echo $_GET["post"]; ?>'
				}, 
				function(data) {
					if ( data == 'INUSE' ) {
						$('#ajax_check_message').removeClass().addClass('error').text('Code Already Exists.').fadeIn();
					} else {
						$('#ajax_check_message').removeClass().addClass('updated').text('Valid Code.').fadeIn();
					}
				});
		});
	});
</script>
<table class="form-table">
	<tbody>
		<tr>
			<th scope="row"><label for="code"><?php self::_e('Discount') ?>:</label></th>
			<td>
				<input type="text" id="discount" name="discount" value="<?php echo $discount; ?>" size="40"  class="small-text" /> <label for="percentage"><input type="checkbox" name="percentage" id="percentage" value="1" <?php checked($percentage, TRUE); ?>> <?php wpg_e('Check if this is a percentage discount.'); ?></label>
				<br/><small><?php wpg_e('Discount applied before tax and shipping.') ?></small>
			</td>
		</tr>
		<tr>
			<th scope="row"><label for="code"><?php self::_e('Coupon Code') ?>:</label></th>
			<td>
				<input type="text" id="code" name="code" value="<?php echo $code; ?>" size="40" />
				<div id="ajax_check_message" style="display: inline-block;"></div>
			</td>
		</tr>
	</tbody>
</table>
<span class="meta_box_block_divider"></span>
<table class="form-table">
	<tbody>
		<tr>
			<th scope="row"><label for="discount"><?php self::_e('Options') ?>:</label></th>
			<td>
				<label for="combo"><input type="checkbox" name="combo" id="combo" value="1" <?php checked($combo, TRUE); ?>> <?php wpg_e('This is can be combined with other discount codes.'); ?></label>
				<br/>
				<label for="single_use"><input type="checkbox" name="single_use" id="single_use" value="1" <?php checked($single_use, TRUE); ?>> <?php wpg_e('This discount can only be used once by the same user.'); ?></label>
			</td>
		</tr>
		<tr>
			<th scope="row"><label for="exp"><?php self::_e('Expiration') ?>:</label></th>
			<td><input type="text" id="exp" name="exp" value="<?php echo date('m/d/Y G:i', $exp); ?>" size="15" /></td>
		</tr>
		<tr>

			<th scope="row"><label for="limit"><?php self::_e('Limit') ?>:</label></th>
			<td><input type="text" id="limit" name="limit" value="<?php echo $limit; ?>" size="40" class="small-text" />
				<br/><?php wpg_e('Total number of purchases this can be used.'); ?></td>
		</tr>
		<tr>

			<th scope="row"><label for="deals"><?php self::_e('Deals IDs') ?>:</label></th>
			<td><textarea rows="3" cols="40" name="deals" tabindex="510" id="deals" style="width:98%"><?php echo $deals; ?></textarea>
				<br/><small><?php wpg_e('Comma separated list. Leave blank if the discount applies to the entire cart.'); ?></small></td>
		</tr>
		<?php /*/ ?>
		<tr>
			<th scope="row"><label for="assigner"><?php self::_e('Roles') ?>:</label></th>
			<td>
				<select name="roles[]" multiple="multiple" style="height: auto;">
					<?php 
						global $wp_roles;
						foreach ( $wp_roles->roles as $role => $details ) {
							$name = translate_user_role( $details['name'] );
							$selected = ( in_array(esc_attr($role), $roles) ) ? 'selected="selected"': false ;
							echo "<option value='" . esc_attr($role) . "' ".$selected.">$name</option>";
					} ?>
				</select>
			</td>
		</tr>
		<?php /**/ ?>
	</tbody>
</table>
<span class="meta_box_block_divider"></span>
<table class="form-table">
	<tbody>
		<tr>
			<th scope="row"><label for="token"><?php self::_e('Purchases') ?>:</label></th>
			<td>
				<ul>
					<?php foreach ($purchases as $purchase_id) {
						echo '<li>'.self::__('Order #').$purchase_id.'</li>';
					} ?>
				</ul>
			</td>
		</tr>
	</tbody>
</table>