<?php
function wg_get_note_by_voucher_id( $voucher_id ) {
	$notes = WP_Groupbuy_Checkout_Notes::get_vouchers_deal_notes($voucher_id);
	return apply_filters('wg_get_note_by_voucher_id', $notes);
}