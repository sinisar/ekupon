<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit6c899d070202f9a45937579af49a771b
{
    public static $prefixLengthsPsr4 = array (
        'G' => 
        array (
            'Genkgo\\Camt\\' => 12,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Genkgo\\Camt\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
            1 => __DIR__ . '/../..' . '/tests',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit6c899d070202f9a45937579af49a771b::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit6c899d070202f9a45937579af49a771b::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
