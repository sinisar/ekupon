<?php

class WP_Groupbuy_Accounts_Login extends WP_Groupbuy_Controller {
	const LOGIN_PATH_OPTION = 'wg_account_login_path';
	const LOGIN_QUERY_VAR = 'wg_account_login';
	const FORM_ACTION = 'wg_account_login';
	private static $login_path = 'account/login';
	private static $instance;
	private static $on_login_page = FALSE;
	public static function init() {
		self::$login_path = get_option( self::LOGIN_PATH_OPTION, self::$login_path );
		self::register_path_callback( self::$login_path, array( get_class(), 'on_login_page' ), self::LOGIN_QUERY_VAR, 'account/login' );
		add_action( 'wp_loaded', array( get_class(), 'redirect_away_from_login' ) );
		add_action( 'wp_login_failed', array( get_class(), 'login_failed' ), 10, 1 );
		// Replace WP Login URIs
		add_filter( 'login_url', array( get_class(), 'login_url' ), 10, 2 );
		add_filter( 'logout_url' , array( get_class(), 'log_out_url' ), 100, 2 );
		add_action( 'admin_init' , array( get_class(), 'register_settings_fields' ), 10, 1 );
	}
	public static function register_settings_fields() {
		$page = WP_Groupbuy_UI::get_settings_page();
		$section = 'WG_URL_paths';
		// Settings
		register_setting( $page, self::LOGIN_PATH_OPTION );
		add_settings_field( self::LOGIN_PATH_OPTION, self::__( 'Account Login Path' ), array( get_class(), 'display_account_registration_path' ), $page, $section );
	}
	public static function display_account_registration_path() {
		echo trailingslashit( get_home_url() ) . ' <input type="text" name="' . self::LOGIN_PATH_OPTION . '" id="' . self::LOGIN_PATH_OPTION . '" value="' . esc_attr( self::$login_path ) . '" size="40"/><br />';
	}
	// Redirects away from the login page.
	public static function redirect_away_from_login() {
		global $pagenow;
		// check if it's part of a flash upload.
		if ( !empty( $_POST['_wpnonce'] ) || @$_GET['action'] == 'postpass' || 
			( is_ssl() ) ) 
			return;
		// always redirect away from wp-login.php but check if the user is an admin before redirecting them.
		if ( 'wp-login.php' == $pagenow || 'wp-activate.php' == $pagenow || 'wp-signup.php' == $pagenow || ( !current_user_can( 'edit_posts' ) && is_admin() && !defined( 'DOING_AJAX' ) ) ) {
			// If they're logged in, direct to the account page
			if ( is_user_logged_in() ) {
				wp_redirect( apply_filters( 'wg_redirect_away_from_login', WP_Groupbuy_Accounts::get_url() ) );
				exit();
			} else { // everyone else needs to login
				if ( !defined( 'DOING_AJAX' ) ) {
					$redirect = ( isset( $_GET['action'] ) ) ? add_query_arg( array( 'action' => $_GET['action'] ), self::get_url() ) : self::get_url() ;
					wp_redirect( apply_filters( 'wg_redirect_away_from_login', $redirect ) );
					exit();
				}
			}
		}
	}
	public static function on_login_page() {
		if ( is_ssl() ) {
			return;
		}

		self::$on_login_page = TRUE;
		// Registered users shouldn't be here. Send them elsewhere
		if ( get_current_user_id() && !self::log_out_attempt() ) {
			wp_redirect( WP_Groupbuy_Accounts::get_url(), 303 );
			exit();
		}
		if ( !empty( $_POST['wg_login'] ) && wp_verify_nonce( $_POST['wg_login'], 'wg_login_action' ) ) {
			// signin
            //  try classic login first
            $salted_password = self::generate_user_password($_POST['log'], $_POST['pwd']);
            //	print $salted_password;
            //break;
            $user = null;
            if($salted_password == null) {
                $user = wp_signon();
            }
            else {
                //	load user salt and generate password
                $credentials = array(
                    'user_login' => $_POST['log'],
                    'user_password' => $salted_password,
                );
                $user = wp_signon($credentials);
            } if (is_wp_error( $user )) {
            	$user = wp_signon();
            }

			if ( !is_wp_error( $user ) ) {
				$user_id = $user->ID;
				do_action( 'wg_user_logged_in', $user, $_REQUEST );
				if ( isset( $_POST['redirect_to'] ) && !empty( $_POST['redirect_to'] ) ) {
					$redirect_str = str_replace( home_url(), '', $_POST['redirect_to'] ); // in case the home_url is already added
					$redirect = home_url( $redirect_str );
					wp_redirect( apply_filters( 'wg_login_success_redirect', $redirect, $user_id ) );
				} else {
					wp_redirect( apply_filters( 'wg_login_success_redirect', wg_get_last_viewed_redirect_url(), $user_id ) );
				}
				exit();
			}
		} elseif ( self::log_out_attempt() ) {
			// logout
			wp_logout();
			if ( isset( $_GET['redirect_to'] ) ) {
				$redirect_to = add_query_arg( array( 'loggedout' => 'true', 'message' => 'loggedout' ), home_url( $_GET['redirect_to'] ) );
			} else {
				$redirect_to = add_query_arg( array( 'loggedout' => 'true', 'message' => 'loggedout' ), self::get_url() );
			}
			wp_redirect( $redirect_to );
			exit();
		}
		self::get_instance(); // make sure the class is instantiated
	}

	//	generate user password using password and salt
	private function generate_user_password($user_username, $user_password) {
		//	get user id from email.
		$user_id = "";
		$user = get_user_by('email', $user_username);
		if($user == null) {
			$user = get_user_by('login', $user_username);
			if($user == null) {
				echo "An error occured, no user with username: ".$user_username." found.";
				return null;
			}
			else {
				$user_id = $user->ID;
			}
		}
		else {
			$user_id = $user->ID;
		}
		//	echo "found user with id: ".$user_id;
		//	get user salt by user_username
		$user_salt = get_user_meta($user_id, '_user_salt', true);
		if(empty($user_salt)) {
			return null;
		}
		//	echo "user salt: ".$user_salt;
		//	echo "pass: ".$user_password;
		$salted_password = md5(md5($user_password).$user_salt);
		//	echo "final_pass: ".$salted_password;
		return $salted_password;
	}

	public static function get_url() {
		if ( self::using_permalinks() ) {
			return trailingslashit( home_url( trailingslashit( self::$login_path ) ) );
		} else {
			return add_query_arg( self::LOGIN_QUERY_VAR, 1, home_url() );
		}
	}
	public static function login_url( $url, $redirect ) {
		if ( strpos($redirect, 'https') !== false || is_ssl() ) {
			return $url;
		}
		$url = self::get_url();
		$redirect = apply_filters( 'wg_login_url_redirect', $redirect );
		if ( $redirect ) {
			$redirect = str_replace( home_url(), '', $redirect );
			$url = add_query_arg( 'redirect_to', $redirect, $url );
		} else {
			$redirect = str_replace( home_url(), '', WP_Groupbuy_Accounts::get_url() );
			$url = add_query_arg( 'redirect_to', $redirect, $url );
		}
		return $url;
	}
	public static function log_out_url(  $url = null, $redirect = null ) {
		$url = self::get_url();
		if ( $redirect ) {
			$redirect = str_replace( home_url(), '', $redirect );
			$url = add_query_arg( array( 'redirect_to' => $redirect, 'action' => 'logout', 'message' => 'loggedout' ), $url );
		} else {
			$url = add_query_arg( array( 'action' => 'logout', 'message' => 'loggedout' ), $url );
		}
		return $url;
	}
	public static function log_out_attempt() {
		return ( isset( $_GET['action'] ) && 'logout' == $_GET['action'] ) ? TRUE : FALSE;
	}
	public static function login_failed( $username ) {
		// recap a lot of wp-login.php
		if ( !empty( $_GET['loggedout'] ) )
			return;
		// If cookies are disabled we can't log in even with a valid user+pass
		if ( isset( $_POST['testcookie'] ) && empty( $_COOKIE[TEST_COOKIE] ) )
			$message = self::__( 'Cookies are Disabled' );
		if ( isset( $_GET['registration'] ) && 'disabled' == $_GET['registration'] )
			$message = self::__( 'Registration Disabled' );
		elseif ( isset( $_GET['checkemail'] ) && 'registered' == $_GET['checkemail'] )
			$message = self::__( 'Registered' );
		elseif ( isset( $_REQUEST['interim-login'] ) )
			$message = self::__( 'Error: Expired' );
		else
			$message = self::__( 'Username and/or Password Incorrect.' );
		$url = self::get_url();
		$url = add_query_arg( 'message', $message, self::get_url() );
		if ( isset( $_REQUEST['redirect_to'] ) ) {
			$url = add_query_arg( 'redirect_to', $_REQUEST['redirect_to'], $url );
		}
		self::set_message( $message, self::MESSAGE_STATUS_ERROR );
		wp_redirect( $url );
		exit();
	}
	private function __clone() {
		trigger_error( __CLASS__.' may not be cloned', E_USER_ERROR );
	}
	private function __sleep() {
		trigger_error( __CLASS__.' may not be serialized', E_USER_ERROR );
	}
	public static function get_instance() {
		if ( !( self::$instance && is_a( self::$instance, __CLASS__ ) ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	private function __construct() {
		self::do_not_cache(); // never cache the account pages
		$this->check_messages();
		add_action( 'pre_get_posts', array( $this, 'edit_query' ), 10, 1 );
		add_action( 'the_post', array( $this, 'view_login_form' ), 10, 1 );
		add_filter( 'the_title', array( $this, 'get_title' ), 10, 2 );
	}
	private function check_messages() {
		$messages = array(
			'test_cookie' => self::__( "Cookies are blocked or not supported by your browser. You must <a href='http://www.google.com/cookies.html'>enable cookies</a> to use this site." ),
			'loggedout' => self::__( 'You are now logged out.' ),
			'registerdisabled' => self::__( 'User registration is currently not allowed.' ),
			'registered' => self::__( 'Registration complete. Please log in' ),
			'expired' => self::__( 'Your session has expired. Please log in again.' ),
		);
		if ( isset( $_GET['message'] ) && isset( $messages[$_GET['message']] ) ) {
			self::set_message( $messages[$_GET['message']], self::MESSAGE_STATUS_ERROR );
		}
	}
	// Edit the query on the profile edit page to select the user's account.
	public function edit_query( WP_Query $query ) {
		if ( isset( $query->query_vars[self::LOGIN_QUERY_VAR] ) && $query->query_vars[self::LOGIN_QUERY_VAR] ) {
			$query->query_vars['post_type'] = WP_Groupbuy_Account::POST_TYPE;
			$query->query_vars['p'] = WP_Groupbuy_Account::get_account_id_for_user();
		}
	}
	// Update the global $pages array with the HTML for the page.
	public function view_login_form( $post ) {
		if ( $post->post_type == WP_Groupbuy_Account::POST_TYPE ) {
			remove_filter( 'the_content', 'wpautop' );
			$args = array();
			if ( isset( $_GET['redirect_to'] ) ) {
				$redirect = str_replace( home_url(), '', $_GET['redirect_to'] );
				$args['redirect'] = $redirect;
			}
			if ( isset( $_GET['action'] ) && $_GET['action'] == 'retrievepassword' ) {
				$view = self::load_view_to_string( 'account/retrievepassword', array( 'args' => $args ) );
			} else { //default
				if ( self::$on_login_page ) {
					$args['submit'] = '<input type="submit" name="submit" value="'.self::__( 'Sign In Now' ).'" class="form-submit" />';
				} else {
					$args['submit'] = '';
				}
				$view = self::load_view_to_string( 'account/login', array( 'args' => $args ) );
			}
			global $pages;
			$pages = array( $view );
		}
	}
	// Filter 'the_title' to display the title of the page rather than the user name
	public function get_title(  $title, $post_id  ) {
		$post = get_post( $post_id );
		if ( $post->post_type == WP_Groupbuy_Account::POST_TYPE ) {
			return self::__( "Login" );
		}
		return $title;
	}
}