<?php

class WP_Groupbuy_MegaPOS_Moneta extends WP_Groupbuy_MegaPOS {
    const PAYMENT_METHOD = 'MegaPOS-Moneta';
    const MEGAPOS_PAYMENT_METHOD = 'MONETA';

    protected static $instance;

    public static function get_instance() {
        if ( !( isset( self::$instance ) && is_a( self::$instance, __CLASS__ ) ) ) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function __construct() {
        parent::__construct();
        parent::setPaymentGateway(self::MEGAPOS_PAYMENT_METHOD);
    }

    public function get_payment_method() {
        return self::PAYMENT_METHOD;
    }

    public static function register() {
        self::add_payment_processor( __CLASS__, self::__( 'Moneta' ) );
    }

    // Process a payment
    public function process_payment( WP_Groupbuy_Checkouts $checkout, WP_Groupbuy_Purchase $purchase ) {
        return parent::process_MegaPOS_payment($checkout, $purchase, self::MEGAPOS_PAYMENT_METHOD);
    }

    public static function returned_from_offsite() {
        return ( isset( $_GET['back_from_megapos'] ) && ($_GET['back_from_megapos'] == self::MEGAPOS_PAYMENT_METHOD) );
    }

}
WP_Groupbuy_MegaPOS_Moneta::register();