<?php
class WP_Groupbuy_UPN_Payment extends WP_Groupbuy_Offsite_Manual_Purchasing {
    const TITLE = 'UPN Plačilo';
    const NAME = 'upn';

    protected static $data = array();

    public static function register() {
        parent::register();

        static::$data['company_name_option'] = 'wg_' . static::NAME . '_company_name';
        static::$data['company_address_option'] = 'wg_' . static::NAME . '_company_address';
        static::$data['company_account_option'] = 'wg_' . static::NAME . '_company_account';
        static::$data['company_account_bank_BIC_option'] = 'wg_' . static::NAME . '_company_account_bank_BIC';

        static::$data['company_name'] = get_option( static::$data['company_name_option'] );
        static::$data['company_address'] = get_option( static::$data['company_address_option'] );
        static::$data['company_account'] = get_option( static::$data['company_account_option'] );
        static::$data['company_account_bank_BIC'] = get_option( static::$data['company_account_bank_BIC_option'] );
    }

    public function register_settings() {
        parent::register_settings();

        $page = WP_Groupbuy_Payment_Processors::get_settings_page();
        $section = static::$data['section'];
        register_setting($page, static::$data['company_name_option']);
        register_setting($page, static::$data['company_address_option']);
        register_setting($page, static::$data['company_account_option']);
        register_setting($page, static::$data['company_account_bank_BIC_option']);
        add_settings_field(static::$data['company_name_option'], static::__('Company name'),
            array($this, 'display_upn_company_name_field'), $page, $section);
        add_settings_field(static::$data['company_address_option'], static::__('Company address'),
            array($this, 'display_upn_company_address_field'), $page, $section);
        add_settings_field(static::$data['company_account_option'], static::__('Company account'),
            array($this, 'display_upn_company_account_field'), $page, $section);
        add_settings_field(static::$data['company_account_bank_BIC_option'], static::__('Company account bank BIC'),
            array($this, 'display_upn_company_account_bank_BIC_field'), $page, $section);
    }

    public function display_upn_company_name_field() {
        echo '<input type="text" name="'.static::$data['company_name_option'].'" value="'.static::$data['company_name'].'" size="80" />';
    }

    public function display_upn_company_address_field() {
        echo '<input type="text" name="'.static::$data['company_address_option'].'" value="'.static::$data['company_address'].'" size="80" />';
    }

    public function display_upn_company_account_field() {
        echo '<input type="text" name="'.static::$data['company_account_option'].'" value="'.static::$data['company_account'].'" size="30" />';
    }

    public function display_upn_company_account_bank_BIC_field() {
        echo '<input type="text" name="'.static::$data['company_account_bank_BIC_option'].'" value="'.static::$data['company_account_bank_BIC'].'" size="30" />';
    }

    public function get_upn_payment_company_name() {
        return static::$data['company_name'];
    }

    public function get_upn_payment_company_address() {
        return static::$data['company_address'];
    }

    public function get_upn_payment_company_account() {
        return static::$data['company_account'];
    }

    public function get_upn_payment_company_account_bank_BIC() {
        return static::$data['company_account_bank_BIC'];
    }

    public function process_payment(WP_Groupbuy_Checkouts $checkout, WP_Groupbuy_Purchase $purchase)
    {
        $payment = parent::process_payment($checkout, $purchase);

        if ($payment) {
            $payment_reference = $this->calc_payment_reference( $purchase, $payment );
            // add reference to payment data
            $data = array_merge($payment->get_data(), array('payment_reference' => $payment_reference));
            $payment->set_data( $data );

            do_action( 'upn_payment_info', $purchase, $payment );
        }
        return $payment;
    }

    private function calc_payment_reference(WP_Groupbuy_Purchase $purchase, WP_Groupbuy_Payment $payment) {
//        $paymentReferenceBase = date('dm') . str_pad($payment->get_id(), 8, '0', STR_PAD_LEFT);
        // '1' in the middle is number for marking payment come from izkoristi.me
        $paymentReferenceBase = date("y") . "1" . str_pad($payment->get_id(), 9, '0', STR_PAD_LEFT);
        $paymentReferenceWithChecksum = $this->add_checksum_by_module_11($paymentReferenceBase);
        return "SI12 {$paymentReferenceWithChecksum}";
    }

    private function add_checksum_by_module_11($value) {
        $length = strlen($value);
        $sum = 0;
        for($i = 0; $i < $length; $i++) {
            $posValue = intval(substr($value, $length - 1 - $i, 1));
            $sum += ($i + 2) * $posValue;
        }
        $checkSum = 11 - ($sum % 11);
        $checkSum = ($checkSum > 10 ? '0' : $checkSum % 10);
        return $value.$checkSum;
    }

    public function complete_UPN_payment( $payment ) {
        if ( is_a( $payment, 'WP_Groupbuy_Payment' ) ) {
            if ( $payment->get_payment_method() == $this->get_payment_method() && $payment->get_status() != WP_Groupbuy_Payment::STATUS_COMPLETE ) {
                $items_to_capture = $this->items_to_capture( $payment );
                if( $items_to_capture ) {
                    $data = $payment->get_data();
                    foreach ( $items_to_capture as $deal_id => $amount ) {
                        unset( $data['uncaptured_deals'][$deal_id] );
                    }
                    do_action( 'payment_captured', $payment, array_keys( $items_to_capture ) );

                    // Set the status
                    if ( count( $data['uncaptured_deals'] ) < 1 ) {
                        $payment->set_status( WP_Groupbuy_Payment::STATUS_COMPLETE );
                        do_action( 'payment_complete', $payment );
                        return true;
                    } else {
                        $payment->set_status( WP_Groupbuy_Payment::STATUS_PARTIAL );
                    }
                }
            }
        }
        return false;
    }
}
WP_Groupbuy_UPN_Payment::register();