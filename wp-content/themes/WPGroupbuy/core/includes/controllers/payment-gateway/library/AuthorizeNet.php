<?php
/**
 * The AuthorizeNet PHP SDK
 */

define( "AUTHORIZENET_API_LOGIN_ID", get_option( WP_Groupbuy_AuthnetCIM::API_USERNAME_OPTION, '' ) );
define( "AUTHORIZENET_TRANSACTION_KEY", get_option( WP_Groupbuy_AuthnetCIM::API_PASSWORD_OPTION, '' ) );

if ( !WP_Groupbuy_AuthnetCIM::is_sandbox() ) {
	define( "AUTHORIZENET_SANDBOX", FALSE );
}
	
require dirname(__FILE__) . '/AuthorizeNet/shared/AuthorizeNetRequest.php';
require dirname(__FILE__) . '/AuthorizeNet/shared/AuthorizeNetTypes.php';
require dirname(__FILE__) . '/AuthorizeNet/shared/AuthorizeNetXMLResponse.php';
require dirname(__FILE__) . '/AuthorizeNet/shared/AuthorizeNetResponse.php';

require dirname(__FILE__) . '/AuthorizeNet/AuthorizeNetAIM.php';
require dirname(__FILE__) . '/AuthorizeNet/AuthorizeNetARB.php';
require dirname(__FILE__) . '/AuthorizeNet/AuthorizeNetSIM.php';
require dirname(__FILE__) . '/AuthorizeNet/AuthorizeNetDPM.php';
require dirname(__FILE__) . '/AuthorizeNet/AuthorizeNetTD.php';
require dirname(__FILE__) . '/AuthorizeNet/AuthorizeNetCP.php';


require dirname(__FILE__) . '/AuthorizeNet/AuthorizeNetCIM.php';

if (class_exists("SoapClient")) {
    require dirname(__FILE__) . '/AuthorizeNet/AuthorizeNetSOAP.php';
}
/**
 * Exception class for AuthorizeNet PHP SDK
 */
class AuthorizeNetException extends Exception{}