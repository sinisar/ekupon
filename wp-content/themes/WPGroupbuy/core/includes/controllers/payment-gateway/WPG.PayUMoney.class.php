<?php

/**
 * PayU Money offsite payment processor.
 */
class WPG_PayU_Money_Payments extends WP_Groupbuy_Offsite_Processors {
	
	const DEBUG = WPG_DEV; //set to WPG_DEV 
	
	const PAYMENT_METHOD = 'PayU Money';
	
	const MODE_OPTION = 'payumoney_mode';
	const PRESELECT_OPTION = 'payumoney_preselect_option';
	const MERCHANT_ID_OPTION = 'payumoney_merchant_id';
	const MERCHANT_KEY_OPTION = 'payumoney_merchant_key';
	const SALT_OPTION = 'payumoney_salt';
	
	const USER_META_TOKEN_PREFIX = 'wg_payumoney_payment_token_';
	const ORDER_META_PAYUMONEY_DATA = 'wg_payumoney_transaction_data';
	const ORDER_META_PAYUMONEY_TXNID = 'wg_payumoney_txnid';

	protected static $instance;
	private static $mode; 
	private static $api_url; 
	private static $preselect_method; 
	private static $merchantid;
	private static $merchantkey;
	private static $salt;
	
	public static function get_instance() {
		if ( !( isset( self::$instance ) && is_a( self::$instance, __CLASS__ ) ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	public function get_payment_method() {
		return self::PAYMENT_METHOD;
	}

	public static function returned_from_offsite() {
		if ( isset( $_GET['payu_money_callback'] ) ) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	protected function __construct() {
		parent::__construct();
		
		$this->mode = get_option( self::MODE_OPTION, 'live' );
		if ( $this->mode == 'live' ) {
			$this->api_url = 'https://secure.payu.in/_payment'; //live
		} else {
			$this->api_url = 'https://test.payu.in/_payment'; //test
		}
		$this->preselect_method = get_option( self::PRESELECT_OPTION, 'CC' );
		//$this->merchantid   	= get_option( self::MERCHANT_ID_OPTION, '' );
		$this->merchantkey   	= get_option( self::MERCHANT_KEY_OPTION, '' );
		$this->salt   			= get_option( self::SALT_OPTION, '' );

		add_action( 'admin_init', array( $this, 'register_settings' ), 10, 0 );

		add_filter( 'wg_valid_process_payment_page', array( $this, 'valid_process_payment_page'), 10, 2);
		add_filter( 'wg_checkout_payment_controls', array( $this, 'payment_controls' ), 20, 2 );
		add_action( 'wg_send_offsite_for_payment', array( $this, 'send_offsite' ), 10, 1 );
		add_action( 'wg_load_cart', array( $this, 'back_from_payumoney_payments' ), 10, 0 );
		add_action( 'purchase_completed', array( $this, 'complete_purchase' ), 10, 1 );
		//add_action( 'wg_manually_capture_purchase', array( $this, 'manually_capture_purchase' ), 10, 1 );
		
		// Limitations
		add_filter( 'wp_groupbuy_template_meta_boxes/deal-expiration.php', array($this, 'display_exp_meta_box'), 10);
		add_filter( 'wp_groupbuy_template_meta_boxes/deal-price.php', array($this, 'display_price_meta_box'), 10);
		add_filter( 'wp_groupbuy_template_meta_boxes/deal-limits.php', array($this, 'display_limits_meta_box'), 10);
		
		session_start();
	}
	
	public static function register() {
		self::add_payment_processor( __CLASS__, self::__( 'PayU Money' ) );
	}

	public static function public_name() {
		//return apply_filters('wpg_payment_processor_public_name', 'Verifone PayPage' );
		return self::__( 'PayU Money' );
	}
	
	//Skip Review page  during Checkout (Mark as complete)
	public static function valid_process_payment_page ($valid, $checkout) {
		//Payment page comes before review page. So if Payment page is valid, then mark Review page as valid
		if ($valid) {
			$checkout->mark_page_complete( WP_Groupbuy_Checkouts::REVIEW_PAGE );
		}
		return $valid;
	}

	/**
	 * set up the Offsite Checkout transaction and redirect there
	 */
	public function send_offsite( WP_Groupbuy_Checkouts $checkout ) {

		$cart = $checkout->get_cart();
		if ( $cart->get_total() < 0.01 ) { // for free deals.
			return;
		}

		if ( !isset( $_GET['payu_money_callback'] ) && $_REQUEST['wg_checkout_action'] == WP_Groupbuy_Checkouts::PAYMENT_PAGE ) {
			
			if ( self::DEBUG ) error_log( "------ Complete checkout - Redirecting to PayU Money offsite ------" );
			
			//Save checkout cache marking page as completed
			$checkout->save_cache_on_redirect('1');
			
			//Get payment form details
			$user_id = get_current_user_id();
			$user = get_userdata( $user_id );
			
			$filtered_total = $this->get_payment_request_total( $checkout );
			if ( $filtered_total < 0.01 ) {
				return;
			}
			
			$productinfo = self::__( 'Order from ') . get_bloginfo( 'name' );
			//$productinfo = json_encode($productinfo);

			//Hash data
			$hash_data['key']				 = $this->merchantkey;
			$hash_data['txnid'] 			 = substr(hash('sha256', mt_rand() . microtime()), 0, 20); // Unique alphanumeric Transaction ID
			$hash_data['amount'] 			 = $filtered_total;
			$hash_data['productinfo'] 		 = $productinfo;
			$hash_data['firstname']			 = $checkout->cache['billing']['first_name'];
			$hash_data['email'] 			 = $user->user_email;
			$hash_data['hash'] 				 = $this->calculate_hash_before_transaction($hash_data);
			
			// save txnid to session 
			$_SESSION['WG_PAYUMONEY_TXNID'] = $hash_data['txnid'];
			
			//Save payumoney transaction data
			$transaction_data = array( 
				'user_id' => get_current_user_id(), 
				'txnid' => $hash_data['txnid'],
				'total' => $filtered_total
				);
			update_user_meta(get_current_user_id(), self::USER_META_TOKEN_PREFIX.$hash_data['txnid'], $transaction_data);
			
			$cancel_url = add_query_arg( array('wg_payumoney_cancel' => 1), WP_Groupbuy_Carts::get_url());
			$success_url = add_query_arg( array('wg_checkout_action' => 'confirmation'), WP_Groupbuy_Checkouts::get_url());
		
			// PayU Args
			$payu_money_args = array(
	
				// Merchant details
				'key'					=> $this->merchantkey,
				'surl'					=> add_query_arg( array('payu_money_callback' => 1), $success_url ),
				'furl'					=> add_query_arg(array('payu_money_callback' => 1, 'payu_money_status' => 'failed'), $cancel_url ),
				'curl'					=> add_query_arg(array('payu_money_callback' => 1, 'payu_money_status' => 'cancelled'), $cancel_url),
	
				// Customer details
				'firstname'				=> $checkout->cache['billing']['first_name'],
				'lastname'				=> $checkout->cache['billing']['last_name'],
				'email'					=> $user->user_email,
				'address1'				=> $checkout->cache['billing']['street'],
				'address2'				=> '',
				'city'					=> $checkout->cache['billing']['city'],
				'state'					=> $checkout->cache['billing']['zone'],
				'zipcode'				=> $checkout->cache['billing']['postal_code'],
				'country'				=> $checkout->cache['billing']['country'],
				'phone' 	           	=> '55512345123',
	
				// Item details
				'productinfo'		    => $productinfo,
				'amount'			   	=> $filtered_total,
	
				// Pre-selection of the payment method tab
				'pg'			   	    => $this->preselect_method
	
			);
	
			$payuform = '';
	
			foreach( $payu_money_args as $key => $value ) {
				if( $value ) {
					$payuform .= '<input type="hidden" name="' . $key . '" value="' . $value . '" />' . "\n";
				}
			}
	
			$payuform .= '<input type="hidden" name="txnid" value="' . $hash_data['txnid'] . '" />' . "\n";
			$payuform .= '<input type="hidden" name="hash" value="' . $hash_data['hash'] . '" />' . "\n";
			$payuform .= '<input type="hidden" name="service_provider" value="payu_paisa" />' . "\n"; //PayU Money requires this service_provider = payu_paisa
	
			// The form
			echo '<p>'.self::__('Preparing your order...').'</p>';
			echo '<form action="' . $this->api_url . '" method="POST" name="payform" id="payform">';
			echo $payuform;
			echo "
			<noscript>
			<br> &nbsp; <input type='submit' name='submit' value='".self::__('Click here to complete Payment via PayU Money')."'>
			</noscript>
			";
			echo '</form>';
			echo "
			<script type='text/javascript'>
			document.getElementById('payform').submit(); 
			</script>
			";
			
			exit();
		}
	}

	/**
	 * Oon the checkout page, just back from PayU.
	 */
	 
	public function back_from_payumoney_payments() {
		if ( isset( $_GET['payu_money_callback'] ) ) {
			
			if ( self::DEBUG ) error_log('PayU Money - payu_money_callback POST: '.print_r($_POST, true));
			
			$bln_payment_success = FALSE;
		
			//Check status
			$payu = self::get_instance();
			$transaction = $payu->check_transaction_status();
			$transaction_status = $transaction['status'];
			
			if ( $transaction_status == 'success' ) {
				$bln_payment_success = TRUE;
				//Set action
				$_REQUEST['wg_checkout_action'] = 'back_from_payumoney_payments';
				
				//Save data to session
				$_SESSION['WG_PAYUMONEY_TXN_DATA'] = $transaction;
				
			} elseif ( $transaction_status == 'pending' ) {
				$bln_payment_success = TRUE;
				//Set action
				$_REQUEST['wg_checkout_action'] = 'back_from_payumoney_payments';
			} elseif ( $transaction_status == 'failure' ) {
				//failure
			}
			
			if ( self::DEBUG ) error_log('payumoney - payu_money_callback transaction_status: '.print_r($transaction_status, true));
			
			//Should we not continue showing the success page?
			if ( $bln_payment_success != TRUE ) {
				
				self::set_message( self::__('Payment failed.'), self::MESSAGE_STATUS_ERROR );
				
				//Clear txn from session
				unset( $_SESSION['WG_PAYUMONEY_TXNID'] );
				
				$cancel_url = add_query_arg( array('wg_payumoney_cancel' => 1, 'txnid' => $_POST['txnid']), WP_Groupbuy_Carts::get_url());
				wp_redirect( $cancel_url );
				exit();
			}
		} 
	}
	
	public function check_transaction_status() {
		
		$salt = $this->salt;

		if(!empty($_POST)) {
			foreach($_POST as $key => $value) {
				$txnRs[$key] = htmlentities($value, ENT_QUOTES);
           }
         } else {
	         die('No transaction data was passed!');
         }

         $txnid = $txnRs['txnid'];


		/* Checking hash / true or false */
		if($this->check_hash_after_transaction($salt, $txnRs)) {
			
			return $txnRs;

		} else {
			die('Hash incorrect!');
		}

	} // End check_transaction_status()
	
	public function calculate_hash_before_transaction($hash_data) {

		$hash_sequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|||||";
		$hash_vars_seq = explode('|', $hash_sequence);
		$hash_string = '';

		foreach($hash_vars_seq as $hash_var) {
		  $hash_string .= isset($hash_data[$hash_var]) ? $hash_data[$hash_var] : '';
		  $hash_string .= '|';
		}

		$hash_string .= $this->salt;
		$hash_data['hash'] = strtolower(hash('sha512', $hash_string));

		return $hash_data['hash'];

	} // End calculate_hash_before_transaction()
	

	function check_hash_after_transaction($salt, $txnRs) {

		$hash_sequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
		$hash_vars_seq = explode('|', $hash_sequence);
		//generation of hash after transaction is = salt + status + reverse order of variables
		$hash_vars_seq = array_reverse($hash_vars_seq);

		$merc_hash_string = $salt . '|' . $txnRs['status'];

		foreach ($hash_vars_seq as $merc_hash_var) {
			$merc_hash_string .= '|';
			$merc_hash_string .= isset($txnRs[$merc_hash_var]) ? $txnRs[$merc_hash_var] : '';
		}

		$merc_hash = strtolower(hash('sha512', $merc_hash_string));

		/* The hash is valid */
		if($merc_hash == $txnRs['hash']) {
			return true;
		} else {
			return false;
		}

	} // End check_hash_after_transaction()
	
	/*
	//Not used with payumoney
	function payu_money_transaction_verification($txnid) {

		$verification_liveurl	= 'https://info.payu.in/merchant/postservice';
		$verification_testurl	= 'https://test.payu.in/merchant/postservice';

		$host = $verification_liveurl;
		if ( $this->mode == 'test' ) { $host = $verification_testurl; }

		$hash_data['key'] = $this->merchantkey;
		$hash_data['command'] = 'verify_payment';
		$hash_data['var1'] =  $txnid;
		$hash_data['hash'] = $this->calculate_hash_before_verification($hash_data);

		// Call the PayU, and verify the status
		$response = $this->send_request($host, $hash_data);

		$response = unserialize($response);

		return $response['transaction_details'][$txnid]['status'];

	} // End payu_money_transaction_verification()
	
	function payu_money_transaction_verification_data($txnid) {

		$verification_liveurl	= 'https://info.payu.in/merchant/postservice';
		$verification_testurl	= 'https://test.payu.in/merchant/postservice';

		$host = $verification_liveurl;
		if ( $this->mode == 'test' ) { $host = $verification_testurl; }

		$hash_data['key'] = $this->merchantkey;
		$hash_data['command'] = 'verify_payment';
		$hash_data['var1'] =  $txnid;
		$hash_data['hash'] = $this->calculate_hash_before_verification($hash_data);

		// Call the PayU, and verify the status
		$response = $this->send_request($host, $hash_data);

		$response = unserialize($response);

		return $response['transaction_details'];

	} // End payu_money_transaction_verification_data()
	
	
	function calculate_hash_before_verification($hash_data) {

		$hash_sequence = "key|command|var1";
		$hash_vars_seq = explode('|', $hash_sequence);
		$hash_string = '';

		foreach($hash_vars_seq as $hash_var) {
		  $hash_string .= isset($hash_data[$hash_var]) ? $hash_data[$hash_var] : '';
		  $hash_string .= '|';
		}

		$hash_string .= $this->salt;
		$hash_data['hash'] = strtolower(hash('sha512', $hash_string));

		return $hash_data['hash'];

	} // End calculate_hash_before_verification()
	*/
	
	function send_request($host, $data) {

		$response = wp_remote_post($host, array(
                    'method' => 'POST',
                    'body' => $data,
                    'timeout' => 70,
                    'sslverify' => false
                    ));

        if (is_wp_error($response))
            throw new Exception(self::__('There was a problem connecting to the payment gateway.'));

        if (empty($response['body']))
            throw new Exception(self::__('Empty PayU response.'));

        $parsed_response = $response['body'];

        return $parsed_response;

	} // End send_request()
	

	/**
	 * Process a payment
	 */
	public function process_payment( WP_Groupbuy_Checkouts $checkout, WP_Groupbuy_Purchase $purchase ) {
		
		if ( self::DEBUG ) error_log('wg_payumoney running process_payment: session txnid: '.$_SESSION['WG_PAYUMONEY_TXNID']);
		
		if ( $purchase->get_total( self::get_payment_method() ) < 0.01 ) {
			$payments = WP_Groupbuy_Payment::get_payments_for_purchase( $purchase->get_id() );
			foreach ( $payments as $payment_id ) {
				$payment = WP_Groupbuy_Payment::get_instance( $payment_id );
				return $payment;
			}
		}
		
		//Get txnid from user session
		$txnid = $_SESSION['WG_PAYUMONEY_TXNID'];
		$transaction = $_SESSION['WG_PAYUMONEY_TXN_DATA'];
		if ( !$txnid || !$transaction || $transaction['txnid'] != $txnid ) {
			return FALSE;
		}
		
		//Save txnid to order
		update_post_meta( $purchase->get_id(), self::ORDER_META_PAYUMONEY_TXNID, $txnid);
		
		//Check status
		if( $transaction['status'] == 'success' || $transaction['status'] == 'pending') {
			//OK
			
			//Save data to order
			update_post_meta( $purchase->get_id(), self::ORDER_META_PAYUMONEY_DATA, $transaction);
			
		} else {
			if ( self::DEBUG ) error_log('payu_money_transaction_verification_data txnid: '.$txnid.' | status: '.$transaction['status'].' | data: '.print_r($transaction, true));
			return FALSE;	
		}
		
		do_action( 'wg_log', __CLASS__ . '::' . __FUNCTION__ . ' - PayUMoney Payment Response (Raw)', $payumoney_payment );

		// create loop of deals for the payment post
		$deal_info = array();
		foreach ( $purchase->get_products() as $item ) {
			if ( isset( $item['payment_method'][self::get_payment_method()] ) ) {
				if ( !isset( $deal_info[$item['deal_id']] ) ) {
					$deal_info[$item['deal_id']] = array();
				}
				$deal_info[$item['deal_id']][] = $item;
			}
		}
		$shipping_address = array();
		if ( isset( $checkout->cache['shipping'] ) ) {
			$shipping_address['first_name'] = $checkout->cache['shipping']['first_name'];
			$shipping_address['last_name'] = $checkout->cache['shipping']['last_name'];
			$shipping_address['street'] = $checkout->cache['shipping']['street'];
			$shipping_address['city'] = $checkout->cache['shipping']['city'];
			$shipping_address['zone'] = $checkout->cache['shipping']['zone'];
			$shipping_address['postal_code'] = $checkout->cache['shipping']['postal_code'];
			$shipping_address['country'] = $checkout->cache['shipping']['country'];
		}
		
		// create new payment
		$payment_id = WP_Groupbuy_Payment::new_payment( array(
				'payment_method' => self::get_payment_method(),
				'purchase' => $purchase->get_id(),
				'amount' => $purchase->get_total(self::get_payment_method()),
				'data' => array(
					'api_response' => $transaction,
					'uncaptured_deals' => $deal_info
				),
				// 'transaction_id' => $response[], // TODO set the transaction ID
				'deals' => $deal_info,
				'shipping_address' => $shipping_address,
			), WP_Groupbuy_Payment::STATUS_AUTHORIZED );
		if ( !$payment_id ) {
			return FALSE;
		}
		$payment = WP_Groupbuy_Payment::get_instance( $payment_id );
		do_action( 'payment_authorized', $payment );
		
		// complete payment
		/*
		$items_captured = array(); // Creating simple array of items that are captured
		foreach ( $purchase->get_products() as $item ) {
			$items_captured[] = $item['deal_id'];
		}
		do_action( 'payment_captured', $payment, $items_captured );
		$payment->set_status( WP_Groupbuy_Payment::STATUS_COMPLETE );
		do_action( 'payment_complete', $payment );
		*/

		return $payment;
	}
	
	/**
	 * Complete the purchase after the process_payment action, otherwise vouchers will not be activated.
	 */
	public function complete_purchase( WP_Groupbuy_Purchase $purchase ) {
		
		if ( self::DEBUG ) error_log('payumoney complete_purchase, for order id: '.$purchase->get_id().'. session txnid: '.$_SESSION['WG_PAYUMONEY_TXNID']);
		
		
		//Get txkid from user session
		$txnid = $_SESSION['WG_PAYUMONEY_TXNID'];
		if ( !$txnid ) {
			return FALSE;
		}
		
		//Check for payment data 
		$payu_transaction = get_post_meta( $purchase->get_id(), self::ORDER_META_PAYUMONEY_DATA, TRUE);
		if ( self::DEBUG ) error_log('payumoney complete_purchase - session transaction data: '.print_r($payu_transaction, true));
		if ( !$payu_transaction || $payu_transaction['txnid'] != $txnid ) {
			if ( self::DEBUG ) error_log('payumoney complete_purchase - session transaction data: '.print_r($payu_transaction, true));
			unset($_SESSION['WG_PAYUMONEY_TXNID']);
			unset($_SESSION['WG_PAYUMONEY_TXN_DATA']);
			return FALSE;
		} 
		
		//if already not complete, then lets check now
		$set_payment_status = WP_Groupbuy_Payment::STATUS_AUTHORIZED;
		if ( $payu_transaction['status'] == 'success' ) {
			//completed
			$set_payment_status = WP_Groupbuy_Payment::STATUS_COMPLETE;
			self::wpg_handle_completed_payment( $purchase->get_id() );
		} elseif ($payu_transaction['status'] == 'pending' ) {
			//or pending (such as bank transfer)
			$set_payment_status = WP_Groupbuy_Payment::STATUS_PENDING;
			self::wpg_handle_pending_payment( $purchase->get_id() );
		} else {
			unset($_SESSION['WG_PAYUMONEY_TXNID']);
			unset($_SESSION['WG_PAYUMONEY_TXN_DATA']);
			return FALSE;
		}
		
		unset($_SESSION['WG_PAYUMONEY_TXNID']);
		unset($_SESSION['WG_PAYUMONEY_TXN_DATA']);
	}
	
	public function wpg_handle_completed_payment($purchase_id) {
		
		$purchase = WP_Groupbuy_Purchase::get_instance($purchase_id);
		
		$items_captured = array(); // Creating simple array of items that are captured
		foreach ( $purchase->get_products() as $item ) {
			$items_captured[] = $item['deal_id'];
		}
		$payments = WP_Groupbuy_Payment::get_payments_for_purchase($purchase->get_id());
		foreach ( $payments as $payment_id ) {
			$payment = WP_Groupbuy_Payment::get_instance($payment_id);
			
			// mark the items as captured
			$data = $payment->get_data();
			foreach ( $items_captured as $deal_id ) {
				unset( $data['uncaptured_deals'][$deal_id] );
			}
			$payment->set_data( $data );
			
			do_action('payment_captured', $payment, $items_captured);
			do_action('payment_complete', $payment);
			$payment->set_status(WP_Groupbuy_Payment::STATUS_COMPLETE);
		}
	}
	
	public function wpg_handle_pending_payment($purchase_id) {
		
		$purchase = WP_Groupbuy_Purchase::get_instance($purchase_id);
		
		$payments = WP_Groupbuy_Payment::get_payments_for_purchase($purchase->get_id());
		foreach ( $payments as $payment_id ) {
			$payment = WP_Groupbuy_Payment::get_instance($payment_id);
			
			$payment->set_status(WP_Groupbuy_Payment::STATUS_PENDING);
		}
	}
	
	public function manually_capture_purchase( WP_Groupbuy_Payment $payment ) {
		// is this the right payment processor? does the payment still need processing?
		if ( $payment->get_payment_method() == self::get_payment_method() && $payment->get_status() != WP_Groupbuy_Payment::STATUS_COMPLETE ) {
			$data = $payment->get_data();
			
			$purchase = WP_Groupbuy_Purchase::get_instance( $payment->get_purchase() );
			
			//Get payu payment data 
			$txnid = get_post_meta( $purchase->get_id(), self::ORDER_META_PAYUMONEY_TXNID, TRUE);
			$payu_transaction = get_post_meta( $purchase->get_id(), self::ORDER_META_PAYUMONEY_DATA, TRUE);
			
			if ( self::DEBUG ) error_log('payumoney manually_capture_purchase, for payment id: '.$payment->get_id().'. payu_transaction: '.print_r($payu_transaction, true));
			
			if ( !$payu_transaction || !isset($payu_transaction['txnid']) ) {
				return FALSE;
			} 
		
			//Check status
			$payu = self::get_instance();
			$payu_transaction = $payu->payu_money_transaction_verification_data($txnid);
			if( $payu_transaction['status'] == 'success' ) {
				//OK
				
				//Save data to order
				update_post_meta( $purchase->get_id(), self::ORDER_META_PAYUMONEY_DATA, $payu_transaction);
				
				if ( self::DEBUG ) error_log('payumoney manually_capture_purchase status: '.$payu_transaction['status'].' | data: '.print_r($payu_transaction, true));
				
			} else {
				if ( self::DEBUG ) error_log('payumoney manually_capture_purchase status: '.$payu_transaction['status'].' | data: '.print_r($payu_transaction, true));
				return FALSE;	
			}
		}
	}
	
	public function register_settings() {
		$page = WP_Groupbuy_Payment_Processors::get_settings_page();
		$section = 'payumoney_settings';
		add_settings_section($section, self::__('Payu Money Settings'), array($this, 'display_settings_section'), $page);
		
		register_setting($page, self::MODE_OPTION);
		//register_setting($page, self::MERCHANT_ID_OPTION);
		register_setting($page, self::MERCHANT_KEY_OPTION);
		register_setting($page, self::SALT_OPTION);
		register_setting($page, self::PRESELECT_OPTION);
		
		add_settings_field(self::MODE_OPTION, self::__('Mode'), array($this, 'display_mode_field'), $page, $section);
		//add_settings_field(self::MERCHANT_ID_OPTION, self::__('Merchant ID'), array($this, 'display_merchant_id_field'), $page, $section);
		add_settings_field(self::MERCHANT_KEY_OPTION, self::__('Merchant Key'), array($this, 'display_merchant_key_field'), $page, $section);
		add_settings_field(self::SALT_OPTION, self::__('Salt'), array($this, 'display_salt_field'), $page, $section);
		add_settings_field(self::PRESELECT_OPTION, self::__('Preselect Payment Tab'), array($this, 'display_preselect_field'), $page, $section);
		
	}
	
	public function display_settings_section() {
		echo '<strong>Specify your PayU Money Merchant ID and Salt credentials.</strong><br>';
	}
	
	public function display_mode_field() {
		echo '<label><input type="radio" name="'.self::MODE_OPTION.'" value="live" '.checked( 'live', $this->mode, FALSE ).'/> '.self::__( 'Live' ).'</label><br />';
		echo '<label><input type="radio" name="'.self::MODE_OPTION.'" value="test" '.checked( 'test', $this->mode, FALSE ).'/> '.self::__( 'Test/Sandbox' ).'</label>';
	}

	public function display_merchant_id_field() {
		echo '<input type="text" name="'.self::MERCHANT_ID_OPTION.'" value="'.$this->merchantid.'" size="30" />';
	}
	
	public function display_merchant_key_field() {
		echo '<input type="text" name="'.self::MERCHANT_KEY_OPTION.'" value="'.$this->merchantkey.'" size="30" />';
	}

	public function display_salt_field() {
		echo '<input type="text" name="'.self::SALT_OPTION.'" value="'.$this->salt.'" size="30" />';
	}
	
	public function display_preselect_field() {
		echo '<select name="'.self::PRESELECT_OPTION.'">
						<option value="">-- None preselected --</option>
						<option value="CC" '.selected($this->preselect_method, 'CC', FALSE).'>'.self::__( 'Credit Card' ).'</option>
						<option value="DC" '.selected($this->preselect_method, 'DC', FALSE).'>'.self::__( 'Debit Card' ).'</option>
						<option value="NB" '.selected($this->preselect_method, 'NB', FALSE).'>'.self::__( 'Net Banking' ).'</option>
						<option value="EMI" '.selected($this->preselect_method, 'EMI', FALSE).'>'.self::__( 'EMI' ).'</option>
						<option value="COD" '.selected($this->preselect_method, 'COD', FALSE).'>'.self::__( 'COD' ).'</option>
					</select>';
	}

	public function payment_controls( $controls, WP_Groupbuy_Checkouts $checkout ) {
		
		if ( isset($controls['review']) ) {
			$controls['review'] = str_replace( 'value="'.self::__('Review').'"', 'value="'.self::__('Checkout').'"', $controls['review']);
		}
		return $controls;
	}

	/**
	 * Grabs error messages from a PayUMoney response and displays them to the user
	 */
	private function set_error_messages( $message, $display = TRUE ) {
		if ( $display ) {
			self::set_message( $message, self::MESSAGE_STATUS_ERROR );
		} else {
			error_log( "error message from PayUMoney: " . print_r( $message, true ) );
		}
	}
	
	public function display_exp_meta_box()
	{
		return WG_PATH . '/includes/controllers/payment-gateway/meta-boxes/exp-only.php';
	}

	public function display_price_meta_box()
	{
		return WG_PATH . '/includes/controllers/payment-gateway/meta-boxes/no-dynamic-price.php';
	}

	public function display_limits_meta_box()
	{
		return WG_PATH . '/includes/controllers/payment-gateway/meta-boxes/no-tipping.php';
	}
}
WPG_PayU_Money_Payments::register();