<?php

class WPG_Braintree_Payments extends WP_Groupbuy_Credit_Card_Processors {

	const MODE_OPTION = 'wg_braintree_mode';
	const MERCHANT_ID_OPTION = 'wg_braintree_merchant_id';
	const PUBLIC_KEY_OPTION = 'wg_braintree_public_key';
	const PRIVATE_KEY_OPTION = 'wg_braintree_private_key';
	const MODE_LIVE = 'production';
	const MODE_TEST = 'sandbox';
	const PAYMENT_METHOD = 'Credit (Braintree Payments)';
	
	protected static $instance;
	private $mode = '';
	private $merchant_id = '';
	private $public_key = '';
	private $private_key = '';

	public static function get_instance() {
		if ( !( isset( self::$instance ) && is_a( self::$instance, __CLASS__ ) ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	public function get_payment_method() {
		return self::PAYMENT_METHOD;
	}

	protected function __construct() {

		parent::__construct();
		$this->mode = get_option( self::MODE_OPTION, self::MODE_TEST );
		$this->merchant_id = get_option( self::MERCHANT_ID_OPTION, '' );
		$this->public_key = get_option( self::PUBLIC_KEY_OPTION, '' );
		$this->private_key = get_option( self::PRIVATE_KEY_OPTION, '' );

		add_action( 'admin_init', array( $this, 'register_settings' ), 10, 0 );
		add_action( 'purchase_completed', array( $this, 'complete_purchase' ), 10, 1 );

		// Limitations
		add_filter( 'wp_groupbuy_template_meta_boxes/deal-expiration.php', array( $this, 'display_exp_meta_box' ), 10 );
		add_filter( 'wp_groupbuy_template_meta_boxes/deal-price.php', array( $this, 'display_price_meta_box' ), 10 );
		add_filter( 'wp_groupbuy_template_meta_boxes/deal-limits.php', array( $this, 'display_limits_meta_box' ), 10 );
		
		
	}

	public static function register() {
		self::add_payment_processor( __CLASS__, self::__( 'Braintree' ) );
	}
	
	/**
	 * Run the transaction
	 */
	private function do_braintree_transaction( WP_Groupbuy_Checkouts $checkout, WP_Groupbuy_Purchase $purchase ) {
		$user = get_userdata( $purchase->get_user() );

		require_once 'library/Braintree.php';
		
		Braintree_Configuration::environment($this->mode);
		Braintree_Configuration::merchantId($this->merchant_id);
		Braintree_Configuration::publicKey($this->public_key);
		Braintree_Configuration::privateKey($this->private_key);
		
		//Payment data
		$payment_data = array(
			'amount' => wg_get_number_format( $purchase->get_total( $this->get_payment_method() ) ),
			'orderId' => $purchase->get_id(),
			//'merchantAccountId' => 'a_merchant_account_id',
			'creditCard' => array(
				'number' => $this->cc_cache['cc_number'],
				'expirationMonth' => $this->cc_cache['cc_expiration_month'],
				'expirationYear' => $this->cc_cache['cc_expiration_year'],
				'cardholderName' => $this->cc_cache['cc_name'],
				'cvv' => $this->cc_cache['cc_cvv']
			));
		
		$payment_data['customer'] = array(
			'firstName' => $checkout->cache['billing']['first_name'],
			'lastName' => $checkout->cache['billing']['last_name'],
			//'company' => 'Braintree',
			'phone' => $checkout->cache['billing']['phone'],
			//'fax' => '810-888-1234',
			//'website' => 'http://www.domain.com',
			'email' => $user->user_email
		);
		$payment_data['billing'] = array(
			'firstName' => $checkout->cache['billing']['first_name'],
			'lastName' => $checkout->cache['billing']['last_name'],
			//'company' => 'Braintree',
			'streetAddress' => $checkout->cache['billing']['street'],
			//'extendedAddress' => 'Suite 403',
			'locality' => $checkout->cache['billing']['city'],
			'region' => $checkout->cache['billing']['zone'],
			'postalCode' => $checkout->cache['billing']['postal_code'],
			'countryCodeAlpha2' => $checkout->cache['billing']['country']
		);
		if ( isset( $checkout->cache['shipping'] ) ) {
			$payment_data['shipping'] = array(
				'firstName' => $checkout->cache['shipping']['first_name'],
				'lastName' => $checkout->cache['shipping']['last_name'],
				//'company' => 'Braintree',
				'streetAddress' => $checkout->cache['shipping']['street'],
				//'extendedAddress' => 'Suite 403',
				'locality' => $checkout->cache['shipping']['city'],
				'region' => $checkout->cache['shipping']['zone'],
				'postalCode' => $checkout->cache['shipping']['postal_code'],
				'countryCodeAlpha2' => $checkout->cache['shipping']['country']
			);
		}
		
		$payment_data['options'] = array(
			'submitForSettlement' => true
		);
		
		
		//Submit transaction
		$result = Braintree_Transaction::sale($payment_data);
		
		if ($result->success) {
			//print_r("success!: " . $result->transaction->id);
			return TRUE;
			
		} else if ($result->transaction) {
			
			//print_r("Error processing transaction:");
			//print_r("\n  message: " . $result->message);
			//print_r("\n  code: " . $result->transaction->processorResponseCode);
			//print_r("\n  text: " . $result->transaction->processorResponseText);
			
			self::set_error_messages( $result->message );
			return FALSE;
		} else {
			
			//print_r("Message: " . $result->message);
			//print_r("\nValidation errors: \n");
			//print_r($result->errors->deepAll());
			
			self::set_error_messages( $result->message );
			return FALSE;
		}
	}

	/**
	 * Process a payment
	 */
	public function process_payment( WP_Groupbuy_Checkouts $checkout, WP_Groupbuy_Purchase $purchase ) {
		if ( $purchase->get_total( $this->get_payment_method() ) < 0.01 ) {
			$payments = WP_Groupbuy_Payment::get_payments_for_purchase( $purchase->get_id() );
			foreach ( $payments as $payment_id ) {
				$payment = WP_Groupbuy_Payment::get_instance( $payment_id );
				return $payment;
			}
		}

		$braintree = $this->do_braintree_transaction( $checkout, $purchase );

		if ( self::DEBUG ) error_log( '----------Response----------' . print_r( $braintree, TRUE ) );
		
		if ( FALSE === $braintree ) {
			return FALSE;
		}

		/*
		 * Purchase since payment was successful above.
		 */
		$deal_info = array(); // creating purchased products array for payment below
		foreach ( $purchase->get_products() as $item ) {
			if ( isset( $item['payment_method'][$this->get_payment_method()] ) ) {
				if ( !isset( $deal_info[$item['deal_id']] ) ) {
					$deal_info[$item['deal_id']] = array();
				}
				$deal_info[$item['deal_id']][] = $item;
			}
		}
		if ( isset( $checkout->cache['shipping'] ) ) {
			$shipping_address = array();
			$shipping_address['first_name'] = $checkout->cache['shipping']['first_name'];
			$shipping_address['last_name'] = $checkout->cache['shipping']['last_name'];
			$shipping_address['street'] = $checkout->cache['shipping']['street'];
			$shipping_address['city'] = $checkout->cache['shipping']['city'];
			$shipping_address['zone'] = $checkout->cache['shipping']['zone'];
			$shipping_address['postal_code'] = $checkout->cache['shipping']['postal_code'];
			$shipping_address['country'] = $checkout->cache['shipping']['country'];
		}
		$payment_id = WP_Groupbuy_Payment::new_payment( array(
				'payment_method' => $this->get_payment_method(),
				'purchase' => $purchase->get_id(),
				'amount' => $purchase->get_total( $this->get_payment_method() ),
				'data' => array(
					'api_response' => $braintree,
					'masked_cc_number' => $this->mask_card_number( $this->cc_cache['cc_number'] ), // save for possible credits later
				),
				'deals' => $deal_info,
				'shipping_address' => $shipping_address,
			), WP_Groupbuy_Payment::STATUS_AUTHORIZED );
		if ( !$payment_id ) {
			return FALSE;
		}
		$payment = WP_Groupbuy_Payment::get_instance( $payment_id );
		do_action( 'payment_authorized', $payment );
		return $payment;
	}

	/**
	 * Complete the purchase
	 */
	public function complete_purchase( WP_Groupbuy_Purchase $purchase ) {
		$items_captured = array(); // Creating simple array of items that are captured
		foreach ( $purchase->get_products() as $item ) {
			$items_captured[] = $item['deal_id'];
		}
		$payments = WP_Groupbuy_Payment::get_payments_for_purchase( $purchase->get_id() );
		foreach ( $payments as $payment_id ) {
			$payment = WP_Groupbuy_Payment::get_instance( $payment_id );
			do_action( 'payment_captured', $payment, $items_captured );
			do_action( 'payment_complete', $payment );
			$payment->set_status( WP_Groupbuy_Payment::STATUS_COMPLETE );
		}
	}


	/**
	 * Grabs error messages
	 */
	private function set_error_messages( $response, $display = TRUE ) {
		if ( $display ) {
			self::set_message( $response, self::MESSAGE_STATUS_ERROR );
		} else {
			error_log( $response );
		}
	}


	private function convert_money_to_cents( $value ) {
		// strip out commas
		$value = preg_replace( "/\,/i", "", $value );
		// strip out all but numbers, dash, and dot
		$value = preg_replace( "/([^0-9\.\-])/i", "", $value );
		// make sure we are dealing with a proper number now, no +.4393 or 3...304 or 76.5895,94
		if ( !is_numeric( $value ) ) {
			return 0.00;
		}
		// convert to a float explicitly
		$value = (float)$value;
		return round( $value, 2 )*100;
	}

	public function register_settings() {
		$page = WP_Groupbuy_Payment_Processors::get_settings_page();
		$section = 'wg_braintree_settings';
		add_settings_section( $section, self::__( 'Braintree Payments' ), array( $this, 'display_settings_section' ), $page );
		register_setting( $page, self::MODE_OPTION );
		register_setting( $page, self::MERCHANT_ID_OPTION );
		register_setting( $page, self::PUBLIC_KEY_OPTION );
		register_setting( $page, self::PRIVATE_KEY_OPTION );
		add_settings_field( self::MODE_OPTION, self::__( 'Mode' ), array( $this, 'display_mode_field' ), $page, $section );
		add_settings_field( self::MERCHANT_ID_OPTION, self::__( 'Merchant ID' ), array( $this, 'display_merchant_id_field' ), $page, $section );
		add_settings_field( self::PUBLIC_KEY_OPTION, self::__( 'Public Key' ), array( $this, 'display_public_key_field' ), $page, $section );
		add_settings_field( self::PRIVATE_KEY_OPTION, self::__( 'Private Key' ), array( $this, 'display_private_key_field' ), $page, $section );
	}

	public function display_mode_field() {
		echo '<label><input type="radio" name="'.self::MODE_OPTION.'" value="'.self::MODE_LIVE.'" '.checked( self::MODE_LIVE, $this->mode, FALSE ).'/> '.self::__( 'Live' ).'</label><br />';
		echo '<label><input type="radio" name="'.self::MODE_OPTION.'" value="'.self::MODE_TEST.'" '.checked( self::MODE_TEST, $this->mode, FALSE ).'/> '.self::__( 'Sandbox' ).'</label>';
	}
	
	public function display_merchant_id_field() {
		echo '<input type="text" name="'.self::MERCHANT_ID_OPTION.'" value="'.$this->merchant_id.'" size="80" />';
	}
	
	public function display_public_key_field() {
		echo '<input type="text" name="'.self::PUBLIC_KEY_OPTION.'" value="'.$this->public_key.'" size="80" />';
	}
	
	public function display_private_key_field() {
		echo '<input type="text" name="'.self::PRIVATE_KEY_OPTION.'" value="'.$this->private_key.'" size="80" />';
	}
	
	public function display_exp_meta_box() {
		return WG_PATH . '/includes/controllers/payment-gateway/meta-boxes/exp-only.php';
	}

	public function display_price_meta_box() {
		return WG_PATH . '/includes/controllers/payment-gateway/meta-boxes/no-dynamic-price.php';
	}
	
	public function display_limits_meta_box() {
		return WG_PATH . '/includes/controllers/payment-gateway/meta-boxes/no-tipping.php';
	}
}
WPG_Braintree_Payments::register();


	
