<?php

class WP_Groupbuy_Discount_Payments extends WP_Groupbuy_Payment_Processors {
	const PAYMENT_METHOD = 'Discounts';
	protected static $instance;
	
	public static function get_instance() {
		if ( !(isset(self::$instance) && is_a(self::$instance, __CLASS__)) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	protected function __construct() {
		parent::__construct();
		
		add_filter('wg_payment_fields', array($this, 'payment_fields'), 10, 3 );
		add_action('wg_checkout_action_'.WP_Groupbuy_Checkouts::PAYMENT_PAGE, array( $this, 'process_payment_page'), 19, 1);
		
		add_filter('wg_set_nvp_data', array($this, 'filter_paypal_nvp_data'), 15, 3);
		add_filter('wg_paypal_ec_nvp_data', array($this, 'filter_paypal_nvp_data'), 15, 3);
		
		remove_action('wg_new_purchase', array($this, 'register_as_payment_method'), 10, 1);
		add_action('wg_new_purchase', array($this, 'register_as_payment_method'), 4, 2);
		add_action('processing_payment', array($this, 'process_payment'), 10, 2);
		add_action('wg_processing_cart', array($this, 'reset_checkout_discount_cache'), 10, 2);

		add_action('purchase_completed', array($this, 'capture_purchase'), 10, 1);
		add_action('wg_cron', array($this, 'capture_pending_payments'));

		add_filter('wg_item_to_capture_total', array($this, 'filter_item_to_capture_total'), 10, 3);
	}

	/**
	 * Register as the payment method for each unpaid-for item in the purchase
	 */
	public function register_as_payment_method( $purchase, $args = array() ) {
		$checkout = $args['checkout'];
		if ( isset($checkout->cache[WG_Discount::CART_META_DISCOUNT]) && $checkout->cache[WG_Discount::CART_META_DISCOUNT] ) {
			$discount_to_use = $checkout->cache[WG_Discount::CART_META_DISCOUNT];
			$items = $purchase->get_products();
			foreach ( $items as $key => $item ) {
				$remaining = $item['price'];
				foreach ( $item['payment_method'] as $processor => $amount ) {
					$remaining -= $amount;
				}
				if ( $remaining >= 0.01 ) {
					$extras = $purchase->get_item_shipping($item)+$purchase->get_item_tax($item);
					if ( $remaining+$extras <= $discount_to_use ) {
						$items[$key]['payment_method'][$this->get_payment_method()] = $remaining;
						$discount_to_use -= $remaining+$extras;
					} else {
						$ratio = @($discount_to_use/($item['price']+$extras));
						$items[$key]['payment_method'][$this->get_payment_method()] = $ratio*$item['price'];
						$discount_to_use = 0;
					}
				}
				if ( $discount_to_use < 0.01 ) {
					break;
				}
			}
			$purchase->set_products($items);
		}
	}

	public function get_payment_method() {
		return self::PAYMENT_METHOD;
	}

	/**
	 * When making an offsite payment request, filter the total requested to account
	 * for any affiliate discounts being used
	 */
	public function filter_payment_request_total( $total, WP_Groupbuy_Checkouts $checkout ) {
		if ( self::DEBUG ) error_log( "total before: " . print_r( $total, true ) );
		if ( self::DEBUG ) error_log( "cache: " . print_r( $checkout->cache, true ) );
		if ( isset($checkout->cache[WG_Discount::CART_META_DISCOUNT]) && $checkout->cache[WG_Discount::CART_META_DISCOUNT] ) {
			$discount_to_use = $checkout->cache[WG_Discount::CART_META_DISCOUNT];
			if ( self::DEBUG ) error_log( "discount_to_use: " . print_r( $discount_to_use, true ) );
			$total = max($total - $discount_to_use, 0); // can't use discount to get free money
		}
		if ( self::DEBUG ) error_log( "total: " . print_r( $total, true ) );
		return $total;
	}
	
	public function filter_item_to_capture_total( $total, $raw_total, $item = array() ) {
		if ( isset($item['payment_method'][$this->get_payment_method()]) ) {
			return $raw_total;
		}
		return $total;
		
	}
	public function process_payment( WP_Groupbuy_Checkouts $checkout, WP_Groupbuy_Purchase $purchase ) {
		$discount_to_use = @$checkout->cache[WG_Discount::CART_META_DISCOUNT];
		
		$deal_info = array();
		foreach ( $purchase->get_products() as $item ) {
			if ( isset($item['payment_method'][$this->get_payment_method()]) ) {
				if ( !isset($deal_info[$item['deal_id']]) ) {
					$deal_info[$item['deal_id']] = array();
				}
				$deal_info[$item['deal_id']][] = $item;
			}
		}
		if ( !$deal_info ) {
			return FALSE; // no deals, no payment
		}

		if ( isset($checkout->cache['shipping']) ) {
			$shipping_address = array();
			$shipping_address['first_name'] = $checkout->cache['shipping']['first_name'];
			$shipping_address['last_name'] = $checkout->cache['shipping']['last_name'];
			$shipping_address['street'] = $checkout->cache['shipping']['street'];
			$shipping_address['city'] = $checkout->cache['shipping']['city'];
			$shipping_address['zone'] = $checkout->cache['shipping']['zone'];
			$shipping_address['postal_code'] = $checkout->cache['shipping']['postal_code'];
			$shipping_address['country'] = $checkout->cache['shipping']['country'];
		}
		$payment_id = WP_Groupbuy_Payment::new_payment(array(
			'payment_method' => $this->get_payment_method(),
			'purchase' => $purchase->get_id(),
			'amount' => $discount_to_use,
			'data' => array(
				'uncaptured_deals' => $deal_info,
				'applied_coupons' => $checkout->cache[WG_Discount::CART_META_CODES],
			),
			'deals' => $deal_info,
			'shipping_address' => $shipping_address,
		), WP_Groupbuy_Payment::STATUS_AUTHORIZED);
		if ( !$payment_id ) {
			return FALSE;
		}
		$payment = WP_Groupbuy_Payment::get_instance($payment_id);
		do_action('payment_authorized', $payment);
		
		return $payment;
	}

	public static function register() {
		// don't do anything
	}


	public function payment_fields( $fields, $payment_processor_class, $checkout ) {
		$cart = WP_Groupbuy_Cart::get_instance();
		$discount = isset($checkout->cache[WG_Discount::CART_META_DISCOUNT])?$checkout->cache[WG_Discount::CART_META_DISCOUNT]:'';
		$fields['coupon_discounts'] = array(
			'type' => 'text',
			'weight' => -5,
			'label' => self::__('Discount'),
			'description' => self::__('Enter a discount code to apply it to this purchase.'),
			'size' => 10,
			'required' => FALSE,
			'default' => '',
		);
		if ( isset($checkout->cache[WG_Discount::CART_META_DISCOUNT]) && $checkout->cache[WG_Discount::CART_META_DISCOUNT] >= 0.01 ) {
			if ( $total <= $checkout->cache[WG_Discount::CART_META_DISCOUNT] ) {
				foreach ( array_keys($fields) as $key ) {
					$fields[$key]['required'] = FALSE;
				}
			}
		}
		return $fields;
	}

	public static function filter_paypal_nvp_data( $nvpData, WP_Groupbuy_Checkouts $checkout, $i ) {
		if ( !empty($checkout->cache[WG_Discount::CART_META_CODES]) ) {
			foreach ($checkout->cache[WG_Discount::CART_META_CODES] as $code => $deducted) {
				$discount = WG_Discount::get_instance_by_code($code);
				$nvpData['L_PAYMENTREQUEST_0_NAME'.$i] = $discount->get_title();
				$nvpData['L_PAYMENTREQUEST_0_AMT'.$i] = wg_get_number_format( -$deducted);
				$nvpData['L_PAYMENTREQUEST_0_QTY'.$i] = '1';
				$i++;
			}
			$cart = $checkout->get_cart();
			$filtered_total = WP_Groupbuy_Offsite_Processors::get_payment_request_total($checkout);
			$shipping = $cart->get_shipping_total();
			$tax = $cart->get_tax_total();
			$nvpData['PAYMENTREQUEST_0_ITEMAMT'] = wg_get_number_format($filtered_total-($shipping+$tax));
			
		}
		return $nvpData;
	}



	public function process_payment_page( WP_Groupbuy_Checkouts $checkout ) {
		if ( isset($_POST['wg_credit_coupon_discounts']) && $_POST['wg_credit_coupon_discounts'] ) {
			
			$code = $_POST['wg_credit_coupon_discounts'];
			$applied_codes = WG_Discount::get_cart_discounts();
			
			// If the discount is already applied they're most likely resubmitting the checkout page.
			if ( array_key_exists($code,$applied_codes)) {
				// return now and allow for the checkout process to continue.
				return;

				// self::set_message('Discount already applied.', self::MESSAGE_STATUS_ERROR);
				// $checkout->mark_page_incomplete(WP_Groupbuy_Checkouts::PAYMENT_PAGE);
				// return;
			}
			
			// Get Discount Object
			$discount = WG_Discount::get_instance_by_code($code);
			
			// Verify the discount before attempting to get the discount total
			if ( !is_a($discount,'WG_Discount') ) {
				self::set_message('Unknown discount code. ERROR: 42', self::MESSAGE_STATUS_ERROR);
				$fail = TRUE;
			} elseif ( $discount->is_expired() ) {
				self::set_message('Expired discount code.', self::MESSAGE_STATUS_ERROR);
				$fail = TRUE;
			} elseif ( $discount->get_limit() <= count($discount->get_purchases()) ) {
				self::set_message('Sorry. This discount has reached a limit.', self::MESSAGE_STATUS_ERROR);
				$fail = TRUE;
			} elseif ( $discount->is_single_use() && $discount->user_used() ) {
				self::set_message('Sorry but you cannot use this discount more than once.', self::MESSAGE_STATUS_ERROR);
				$fail = TRUE;
			}
			if ( $fail ) {
				$checkout->mark_page_incomplete(WP_Groupbuy_Checkouts::PAYMENT_PAGE);
				return;
			}

			// New discount
			$cart = WP_Groupbuy_Cart::get_instance();
			$discount_to_use = WG_Discounts::get_discount($discount,$cart);
			$unfilter_discount = $discount_to_use;
			$existing_discount = $checkout->cache[WG_Discount::CART_META_DISCOUNT]; // Existing discounts applied

			// Determine messaging
			if ( $discount_to_use ) {
				$discount_to_use = (float)$discount_to_use;
				if ( $discount_to_use < 0.01 ) {
					self::set_message('Bummer! Discount attempted would provide no value to your cart.', self::MESSAGE_STATUS_ERROR);
				} else {
					self::set_message(sprintf('Successfully applied %s discount.',$discount->get_title()));
				}
			} else {
				self::set_message('Discount cannot be applied to this cart.', self::MESSAGE_STATUS_ERROR);
				$checkout->mark_page_incomplete(WP_Groupbuy_Checkouts::PAYMENT_PAGE);
				return;
			}

			// Possibly chain discounts together since we know the user is attempting to add another discount at this point.
			if ( $existing_discount ) { 
				$combo = TRUE;

				// Check to make sure the newly added discount can be combo'd
				if ( !$discount->is_combo() ) {
					// Removed all previously applied discounts.
					unset($checkout->cache[WG_Discount::CART_META_CODES]);
					$combo = FALSE;
				} else {
					// Loop through all the applied coupons and make sure they can be combo'd
					foreach ($applied_codes as $applied_code => $deducted ) { 
						$applied_discount = WG_Discount::get_instance_by_code($applied_code);
						if ( !$applied_discount->is_combo() ) {
							unset($checkout->cache[WG_Discount::CART_META_CODES][$applied_code]); // Removed the previously applied discount.
							$combo = FALSE;
						}
					}
				}
				if ( $combo ) {
					$discount_to_use += $existing_discount;
				}
			}
			
			$total = $cart->get_total();
			if ( $discount_to_use > $total ) { // in case the discount is more than the total of the cart
				$discount_to_use = $total;
			}

			$checkout->cache[WG_Discount::CART_META_CODES][$code] = $unfilter_discount;
			$checkout->cache[WG_Discount::CART_META_DISCOUNT] = $discount_to_use;

			if( WPG_DEV ) error_log( "cache: " . print_r( $checkout->cache, true ) );

			// Since a discount is applied the checkout page needs to be refreshed.
			$checkout->mark_page_incomplete(WP_Groupbuy_Checkouts::PAYMENT_PAGE);
			return;
		}
	}

	public static function reset_checkout_discount_cache() {
		// Reset the cache so discounts are not persistent beyond the checkout page
		global $blog_id;
		$checkout_cache = get_user_meta(get_current_user_id(), $blog_id.'_'.WP_Groupbuy_Checkouts::CACHE_META_KEY, TRUE);
		if ( is_array( $checkout_cache ) ) {
			unset($checkout_cache[WG_Discount::CART_META_CODES]);
			unset($checkout_cache[WG_Discount::CART_META_DISCOUNT]);
		}
		update_user_meta(get_current_user_id(), $blog_id.'_'.WP_Groupbuy_Checkouts::CACHE_META_KEY, $checkout_cache);
	}

	/**
	 * Capture a pre-authorized payment
	 */
	public function capture_purchase( WP_Groupbuy_Purchase $purchase ) {
		$payments = WP_Groupbuy_Payment::get_payments_for_purchase($purchase->get_id());
		foreach ( $payments as $payment_id ) {
			$payment = WP_Groupbuy_Payment::get_instance($payment_id);
			$this->capture_payment($payment);
		}
	}

	/**
	 * Try to capture all pending payments
	 */
	public function capture_pending_payments() {
		$payments = WP_Groupbuy_Payment::get_pending_payments();
		foreach ( $payments as $payment_id ) {
			$payment = WP_Groupbuy_Payment::get_instance($payment_id);
			$this->capture_payment($payment);
		}
	}


	public function capture_payment( WP_Groupbuy_Payment $payment ) {
		// is this the right payment processor? does the payment still need processing?
		if ( $payment->get_payment_method() == $this->get_payment_method() && $payment->get_status() != WP_Groupbuy_Payment::STATUS_COMPLETE ) {
			
			$data = $payment->get_data();
			$items_to_capture = $this->items_to_capture($payment);

			if ( $items_to_capture ) {
				$finishes_payment = (count($items_to_capture) < count($data['uncaptured_deals']))?FALSE:TRUE;

				foreach ( $items_to_capture as $deal_id => $amount ) {
					unset($data['uncaptured_deals'][$deal_id]);
				}

				$payment->set_data($data);
				do_action('payment_captured', $payment, array_keys($items_to_capture));

				// Add the purchase_id to each discount
				if ( isset($data['applied_coupons']) && count($data['applied_coupons']) > 0 ) {
					foreach ($data['applied_coupons'] as $code => $deducted) {
						$discount = WG_Discount::get_instance_by_code($code);
						$discount->add_purchase($payment->get_purchase());
					}
				}

				if ( $finishes_payment ) {
					$payment->set_status(WP_Groupbuy_Payment::STATUS_COMPLETE);
					do_action('payment_complete', $payment);
				} else {
					$payment->set_status(WP_Groupbuy_Payment::STATUS_PARTIAL);
				}
			}
		}

	}
}
