<?php
class WP_Groupbuy_Offsite_Manual_Purchasing extends WP_Groupbuy_Offsite_Processors {
	const TITLE = 'Offline Payment';
	const NAME = 'offsite_purchasing';
	protected static $instance;
	protected static $data = array();
	
	public static function get_instance() {
		if ( !(isset(static::$instance) && is_a(static::$instance, get_called_class())) ) {
			static::$instance = new static();
		}
		return static::$instance;
	}
	
	protected function __construct() {
		parent::__construct();
		add_action('admin_init', array($this, 'register_settings'), 10, 0);
		add_filter('wg_checkout_payment_controls', array($this, 'payment_controls'), 20, 2);
	}

	public static function register() {
		// var_dump(get_called_class(), static::TITLE);
		static::add_payment_processor(get_called_class(), static::__(static::TITLE));

		static::$data['section'] = 'wg_' . static::NAME . '_settings';
		static::$data['button_option'] = 'wg_' . static::NAME . '_button';
		static::$data['guide_option'] = 'wg_' . static::NAME . '_guide'; 
		static::$data['button'] = get_option( static::$data['button_option'] );
		static::$data['guide'] = get_option( static::$data['guide_option'] );

		$active_processor = static::get_payment_processor();
		if ( is_a( $active_processor, 'WP_Groupbuy_Hybrid_Payment_Processor' ) || 
			get_class($active_processor) === get_called_class() ) {
			add_action( 'show_payment_guide', array( get_called_class(), 'show_payment_guide' ) );
		}
		
	}
	
	public function get_payment_method() {
		return static::__(static::TITLE);
	}

	public function get_payment_guide() {
		return static::$data['guide'];
	}
	/**
	* Process a payment
	*/
	public function process_payment( WP_Groupbuy_Checkouts $checkout, WP_Groupbuy_Purchase $purchase ) {
		if ( $purchase->get_total($this->get_payment_method()) < 0.01 ) {
			$payments = WP_Groupbuy_Payment::get_payments_for_purchase($purchase->get_id());
			foreach ( $payments as $payment_id ) {
				$payment = WP_Groupbuy_Payment::get_instance($payment_id);
				return $payment;
			}
		}
		// create loop of deals for the payment post
		$deal_info = array();
		foreach ( $purchase->get_products() as $item ) {
			if ( isset($item['payment_method'][$this->get_payment_method()]) ) {
				if ( !isset($deal_info[$item['deal_id']]) ) {
					$deal_info[$item['deal_id']] = array();
				}
				$deal_info[$item['deal_id']][] = $item;
			}
		}
		// create new payment
		$payment_id = WP_Groupbuy_Payment::new_payment(array(
			'payment_method' => $this->get_payment_method(),
			'purchase' => $purchase->get_id(),
			'amount' => $purchase->get_total($this->get_payment_method()),
			'data' => array(
                'api_response' => static::__(static::TITLE),
                'uncaptured_deals' => $deal_info
			),
			// 'transaction_id' => $response[], // set the transaction ID
			'deals' => $deal_info,
		), WP_Groupbuy_Payment::STATUS_PENDING);
		if ( !$payment_id ) {
			return FALSE;
		}
		$payment = WP_Groupbuy_Payment::get_instance($payment_id);
		do_action('payment_pending', $payment);
		return $payment;
	}
	
	public function register_settings() {
		$page = WP_Groupbuy_Payment_Processors::get_settings_page();
		$section = static::$data['section'];
		add_settings_section($section, $this->get_payment_method(), array($this, 'display_settings_section'), $page);
		register_setting($page, static::$data['button_option']);
		register_setting($page, static::$data['guide_option']);
		add_settings_field(static::$data['button_option'], static::__('Checkout Button Image'), array($this, 'display_background_field'), $page, $section);
		add_settings_field(static::$data['guide_option'], static::__('How to make payment'), array($this, 'display_guide_field'), $page, $section);
	}

	public function display_background_field() { ?>
		
		<script type="text/javascript">
		
		jQuery(document).ready( function($) {
			
			function <?php echo static::$data['button_option'] ?>_upload(html) {
				var image_url = $('img',html).attr('src');
				//alert(html);
				$('#<?php echo static::$data['button_option'] ?>').val(image_url);
				tb_remove();
				$('#<?php echo static::$data['button_option'] ?>_preview img').attr('src',image_url);
				window.send_to_editor = window.old_send_to_editor;
			}

			$('#<?php echo static::$data['button_option'] ?>_upload').click(function(e) {
				formfield = 'wpg_checkout_button';
				tb_show('Upload <?php echo static::TITLE ?> Button Background Image', 'media-upload.php?referer=wpg-upload-settings&amp;type=image&amp;TB_iframe=true&amp;post_id=0', false);
				window.old_send_to_editor = window.send_to_editor;
				window.send_to_editor = <?php echo static::$data['button_option'] ?>_upload;
				return false;
			});
			// Tooltips
			jQuery(".tips, .help_tip").tipTip({
				'attribute' : 'data-tip',
				'fadeIn' : 50,
				'fadeOut' : 50,
				'delay' : 200
			});
		});
		</script>
		
		<input type="text" id="<?php echo static::$data['button_option'] ?>" name="<?php echo static::$data['button_option'] ?>" value="<?php echo static::$data['button'] ?>" />
		<input id="<?php echo static::$data['button_option'] ?>_upload" type="button" class="button" value="<?php _e( 'Upload Image', 'wpgroupbuy' ); ?>" />
		<img width="16" height="16" src="<?php echo WG_URL ?>/resources/images/help.png" class="help_tip" data-tip="<?php echo printf(static::__( 'Button image for %s gateway. Size shoud be 116x36px.' ), static::TITLE) ?>">
		<div id="<?php echo static::$data['button_option'] ?>_preview">
			<img style="max-width:100%; padding-top:5px;" src="<?php echo esc_url( static::$data['button'] ); ?>" />
		</div>
		<?php
		require_once WG_PATH.'/includes/upload.php';
		wpg_options_enqueue_scripts();

	}
	
	public function display_guide_field() {
		echo '<textarea rows="8" cols="50" name="'.static::$data['guide_option'].'" />'.static::$data['guide'].'</textarea>';
		echo '<img width="16" height="16" src="'.WG_RESOURCES . 'images/help.png'. '" class="help_tip" title="'.static::__( 'How to make payment offine information. You can use HTML tag for this content.' ).'">';
	}
	
	public function payment_controls( $controls, WP_Groupbuy_Checkouts $checkout ) {
		if ( isset($controls['review']) ) {
			if ( !empty(static::$data['button']) && static::$data['button'] != '' ) {
				$style = 'style="width: 156px!important; height: 36px; background-image: url('.static::$data['button'].'); background-position: center; border: none; cursor: pointer; text-shadow: 1px 1px black; margin-top: 12px;"';
			}
			$controls['review'] = str_replace( 'value="'.static::__('Review').'"', $style . ' value="'.static::__(static::TITLE).'"', $controls['review']);			
		}
		return $controls;
	}

	public static function show_payment_guide() {
		$class = get_called_class();
	 ?>
		<div id="<?php echo $class ?>" class="payment_guide"><?php echo $class::$data['guide'] ?></div>
	<?php
	}
}

WP_Groupbuy_Offsite_Manual_Purchasing::register();

class WP_Groupbuy_Bank_Payment extends WP_Groupbuy_Offsite_Manual_Purchasing {
	const TITLE = 'Bank Transfer';
	const NAME = 'bank';
	protected static $data = array();
}

WP_Groupbuy_Bank_Payment::register();

class WP_Groupbuy_COD_Payment extends WP_Groupbuy_Offsite_Manual_Purchasing {
	const TITLE = 'Cash on Delivery';
	const NAME = 'cod';
	protected static $data = array();
}

WP_Groupbuy_COD_Payment::register();

class WP_Groupbuy_Cheque_Payment extends WP_Groupbuy_Offsite_Manual_Purchasing {
	const TITLE = 'Cheque';
	const NAME = 'cheque';
	protected static $data = array();
}

WP_Groupbuy_Cheque_Payment::register();

