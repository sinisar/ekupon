<?php

class WP_Groupbuy_Hybrid_Payment_Processor extends WP_Groupbuy_Payment_Processors {
	const PAYMENT_METHOD = 'TBD';
	const ENABLED_PROCESSORS_OPTION = 'wg_hybrid_processors';

	private static $instance;
	private $current_payment_processor = NULL;

	public static function get_instance() {
		if ( !( isset( self::$instance ) && is_a( self::$instance, __CLASS__ ) ) ) {
			self::$instance = new self();
		}

		if ( self::$instance->current_payment_processor ) {
			return self::$instance->current_payment_processor;
		}

		return self::$instance;
	}

	protected function __construct() {
		// don't want to call the parent constructor, we would just have to undo it
		add_action( 'admin_init', array( $this, 'register_settings' ), 10, 0 );
		add_action( 'wg_processing_cart', array( $this, 'load_enabled_processors' ), 10, 0 );
		add_action( self::CRON_HOOK, array( $this, 'load_enabled_processors' ), -100, 0 );
		add_filter( 'wg_payment_fields', array( $this, 'payment_fields' ), 10, 3 );
		add_filter( 'wg_checkout_payment_controls', array( $this, 'payment_controls' ), 10, 2 );
		add_filter( 'wpg_checkout_panes', array( $this, 'payment_method_cache_pane' ), 10, 2 );


		// always load all enabled processors on admin pages
		if ( is_admin() ) {
			$this->load_enabled_processors();
		}

		add_action( 'wg_load_cart', array( $this, 'load_payment_processor_for_checkout' ), 0, 2 );

	}

	public function get_payment_method() {
		return self::PAYMENT_METHOD;
	}

	public function payment_fields( $fields, $payment_processor_class, $checkout ) {
		$enabled = $this->enabled_processors();
		if ( count($enabled) > 1 ) {
			$fields['payment_method'] = array(
				'type' => 'radios',
				'weight' => 0,
				'label' => self::__( 'Payment Method' ),
				'required' => TRUE,
				'options' => array(),
				'default' => '',
			);
			$registered = self::get_registered_processors();
			foreach ( $enabled as $class ) {
				if ( self::is_cc_processor($class) ) {
					$fields['payment_method']['default'] = 'credit';
					if ( is_callable( array( $class, 'accepted_cards' ) ) ) {
						$accepted_cards = call_user_func( array($class, 'accepted_cards') );
					} else {
						$accepted_cards = 0;
					}
					$fields['payment_method']['options']['credit'] = self::load_view_to_string( 'checkout/credit-card-option', array( 'accepted_cards' => $accepted_cards, 'class' => $class, 'registered' => $registered ) );
				} elseif ( class_exists( $class ) ) {
					if ( is_callable( array( $class, 'checkout_icon' ) ) ) {
						$label = call_user_func( array($class, 'checkout_icon') );
					} elseif ( is_callable( array( $class, 'public_name' ) ) ) {
						$label = call_user_func( array($class, 'public_name') );
					} else {
						$label = $registered[$class];
					}
					$label = apply_filters('wpg_payment_processor_public_name', $label);

					$fields['payment_method']['options'][$class] = $label;
					if ( empty($fields['payment_method']['default']) ) {
						$fields['payment_method']['default'] = $class;
					}
				}
			}
		}
		return $fields;
	}

	public function payment_controls( $controls, WP_Groupbuy_Checkouts $checkout ) {
		if ( FALSE && isset( $controls['review'] ) ) {
			$controls['review'] = str_replace( self::__( 'Review' ), self::__( 'Continue' ), $controls['review'] );
		}
		return $controls;
	}

	// Enabled processors
	public function load_enabled_processors() {
		$current = $this->enabled_processors();
		foreach ( $current as $class ) {
			$this->load_processor($class);
		}
	}

	public function payment_method_cache_pane( $panes, WP_Groupbuy_Checkouts $checkout ) {
		if ( $this->current_payment_processor ) {
			$data = array(
				'type' => 'hidden',
				'value' => esc_attr( get_class($this->current_payment_processor) ),
			);
			if ( self::is_cc_processor($data['value']) ) {
				$data['value'] = 'credit';
			}
			$panes['payment_method_cache'] = array(
				'weight' => 0,
				'body' => wg_get_form_field( 'payment_method_cache', $data, 'credit' )
			);
		}
		return $panes;
	}

	private function load_processor( $class ) {
		if ( class_exists($class) ) {
			$processor = call_user_func(array($class, 'get_instance'));
			return $processor;
		}
		return NULL;
	}

	public function enabled_processors() {
		$enabled = get_option(self::ENABLED_PROCESSORS_OPTION, array());
		if ( !is_array($enabled) ) { $enabled = array(); }
		return array_filter( $enabled );
	}

	// On checkout page, need to load one of the payment processors (but not both)
	public function load_payment_processor_for_checkout( WP_Groupbuy_Checkouts $checkout, WP_Groupbuy_Cart $cart ) {
		// if the decision's already been made
		if ( !empty($this->current_payment_processor) ) {
			return;
		}

		// if only one is enabled, the decision is easy
		$enabled = $this->enabled_processors();
		if ( count($enabled) == 1 ) {
			$this->current_payment_processor = $this->load_processor($enabled[0]);
			return;
		}

		// multiple are enabled, we have to make a decision

		// if the user chose to use one on the checkout page
		if ( isset( $_POST['wg_credit_payment_method'] ) || isset($_POST['wg_credit_payment_method_cache']) ) {
			$method = isset( $_POST['wg_credit_payment_method'] )?$_POST['wg_credit_payment_method']:$_POST['wg_credit_payment_method_cache'];
			if ( $method == 'credit' ) {
				foreach ( $enabled as $class ) {
					if ( self::is_cc_processor($class) ) {
						$this->current_payment_processor = $this->load_processor($class);
						return;
					}
				}
			} elseif ( in_array($method, $enabled) ) {
				$this->current_payment_processor = $this->load_processor($method);
				return;
			}
		}

		// check for tokens sent back by offsite processors
		foreach ( $enabled as $class ) {
			if ( !self::is_cc_processor($class) && class_exists( $class )) {
				if ( call_user_func(array($class, 'returned_from_offsite')) ) {
					$this->current_payment_processor = $this->load_processor($class);
					return;
				}
			}
		}

		// default to the enabled onsite processor (if there is one)
		foreach ( $enabled as $class ) {
			if ( self::is_cc_processor($class) ) {
				$this->current_payment_processor = $this->load_processor($class);
				return;
			}
		}

		// default to the selected default offsite processor (if there is one)
		$this->current_payment_processor = $this->load_processor($enabled[0]);
	}

	public static function register() {
		self::add_payment_processor( __CLASS__, self::__( 'Choose multiple methods' ) );
	}

	// Add options to choose which payment processors to enable
	public function register_settings() {
		$page = WP_Groupbuy_Payment_Processors::get_settings_page();
		$section = 'wg_hybrid_settings';
		add_settings_section( $section, self::__( 'Payment Processors' ), array( $this, 'display_settings_section' ), $page );
		register_setting( $page, self::ENABLED_PROCESSORS_OPTION );
		add_settings_field( self::ENABLED_PROCESSORS_OPTION, self::__( 'Payment Methods' ), array( $this, 'display_payment_methods_field' ), $page, $section );
	}

	public function display_payment_methods_field() {
		$offsite = self::get_registered_processors('offsite');
		$credit = self::get_registered_processors('credit');
		$enabled = $this->enabled_processors();

		if ( $offsite ) {
			printf( '<h4>%s</h4>', self::__('Offsite Processors') );
			foreach ( $offsite as $class => $label ) {
				printf('<p><label><input type="checkbox" name="%s[]" value="%s" %s /> %s</label></p>', self::ENABLED_PROCESSORS_OPTION, esc_attr($class), checked(TRUE, in_array($class, $enabled), FALSE), esc_html($label));
			}
		}
		if ( $credit ) {
			printf( '<h4>%s</h4>', self::__('Credit Card Processors') );
			printf( '<p><select name="%s[]">', self::ENABLED_PROCESSORS_OPTION );
			printf( '<option value="">%s</option>', self::__('-- None --') );
			foreach ( $credit as $class => $label ) {
				printf('<option value="%s" %s /> %s</option>', esc_attr($class), selected(TRUE, in_array($class, $enabled), FALSE), esc_html($label));
			}
			echo '</select>';
		}
	}

	public function process_payment( WP_Groupbuy_Checkouts $checkout, WP_Groupbuy_Purchase $purchase ) {
		return FALSE;
	}

	public function verify_recurring_payment( WP_Groupbuy_Payment $payment ) {
		$method = $payment->get_payment_method();
		$this->load_enabled_processors();
		$enabled = $this->enabled_processors();
		foreach ( $enabled as $class ) {
			$processor = $this->load_processor($class);
			if ( $processor && $processor->get_payment_method() == $method ) {
				$processor->verify_recurring_payment($payment);
				break;
			}
		}

	}
}
WP_Groupbuy_Hybrid_Payment_Processor::register();
