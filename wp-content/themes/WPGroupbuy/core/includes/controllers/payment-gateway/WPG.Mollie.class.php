<?php

/**
 * Mollie offsite payment processor.
 */
class WPG_Mollie_Payments extends WP_Groupbuy_Offsite_Processors {
	
	const DEBUG = WPG_DEV; //set to WPG_DEV
	
	const API_KEY_OPTION = 'wg_mollie_payments_key';
	const LOCALE_OPTION = 'wg_mollie_payments_locale';
	const METHOD_OPTION = 'wg_mollie_payments_method';
	const PAYMENT_METHOD = 'Mollie Payments';
	const USER_META_TOKEN_PREFIX = 'wg_mollie_payment_token_';
	const ORDER_META_MOLLIE = 'wg_mollie_data';

	protected static $instance;
	
	private static $token; //custom local token 
	private static $api_key; //"test_dHar4XY7LxsDOtmnkVtjNVWXLSlXsM"
	private static $locale;
	private static $method;
	
	public static function get_instance() {
		if ( !( isset( self::$instance ) && is_a( self::$instance, __CLASS__ ) ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	public function get_payment_method() {
		return self::PAYMENT_METHOD;
	}

	public static function returned_from_offsite() {
		if ( isset( $_GET['wg_mollie_return'] ) && isset( $_GET['token'] ) && isset( $_GET['payer_uid'] ) ) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	protected function __construct() {
		parent::__construct();
		self::$api_key = get_option( self::API_KEY_OPTION, '' );
		self::$locale = get_option( self::LOCALE_OPTION, '');
		self::$method = get_option( self::METHOD_OPTION, '');

		add_action( 'admin_init', array( $this, 'register_settings' ), 10, 0 );

		add_filter( 'wg_valid_process_payment_page', array( $this, 'valid_process_payment_page'), 10, 2);
		add_filter( 'wg_checkout_payment_controls', array( $this, 'payment_controls' ), 20, 2 );
		add_action( 'wg_send_offsite_for_payment', array( $this, 'send_offsite' ), 10, 1 );
		add_action( 'wg_load_cart', array( $this, 'back_from_mollie_payments' ), 10, 0 );
		add_action( 'purchase_completed', array( $this, 'complete_purchase' ), 10, 1 );
		
		add_action( 'wg_manually_capture_purchase', array( $this, 'manually_capture_purchase' ), 10, 1 );
		
		//Add webhook listener
		add_action( 'init', array( $this, 'webhook_listener' ), 0, 0 );
		
		// Limitations
		add_filter( 'wp_groupbuy_template_meta_boxes/deal-expiration.php', array( $this, 'display_exp_meta_box' ), 10 );
		add_filter( 'wp_groupbuy_template_meta_boxes/deal-price.php', array( $this, 'display_price_meta_box' ), 10 );
		add_filter( 'wp_groupbuy_template_meta_boxes/deal-limits.php', array( $this, 'display_limits_meta_box' ), 10 );
	}

	public static function register() {
		self::add_payment_processor( __CLASS__, self::__( 'Mollie Payments' ) );
	}

	public static function public_name() {
		return self::__( 'Mollie Payments' );
	}
	
	//Skip Review page  during Checkout (Mark as complete)
	public static function valid_process_payment_page ($valid, $checkout) {
		//Payment page comes before review page. So if Payment page is valid, then mark Review page as valid
		if ($valid) {
			$checkout->mark_page_complete( WP_Groupbuy_Checkouts::REVIEW_PAGE );
		}
		return $valid;
	}

	/**
	 * set up the Offsite Checkout transaction and redirect there
	 */
	public function send_offsite( WP_Groupbuy_Checkouts $checkout ) {

		$cart = $checkout->get_cart();
		if ( $cart->get_total() < 0.01 ) { // for free deals.
			return;
		}

		if ( !isset( $_GET['token'] ) && $_REQUEST['wg_checkout_action'] == WP_Groupbuy_Checkouts::PAYMENT_PAGE ) {
			
			//Initialize Mollie API
			require_once dirname(__FILE__) . "/library/Mollie/API/Autoloader.php";
			$mollie = new Mollie_API_Client;
			$mollie->setApiKey(self::$api_key);
			
			$user = get_userdata( get_current_user_id() );
			$filtered_total = $this->get_payment_request_total( $checkout );
			if ( $filtered_total < 0.01 ) {
				return;
			}
			
			$create_token = get_current_user_id().'-'.$cart->get_ID().'-'.time();
			
			//Set return url
			$return_url = add_query_arg( array('wg_mollie_return' => 1, 'token' => $create_token, 'payer_uid' => get_current_user_id() ), WP_Groupbuy_Checkouts::get_url());
			$webhook_url = add_query_arg( array('wg_mollie_listener' => 1, 'token' => $create_token, 'payer_uid' => get_current_user_id() ), WP_Groupbuy_Checkouts::get_url());
			
			$mollieData = array();
			$mollieData = array(
				"amount"       => wg_get_number_format( $filtered_total ),
				"description"  => get_option( 'blogname' ),
				"redirectUrl"  => $return_url,
				"webhookUrl"   => $webhook_url,
				"method"  	   => self::$method,
				"locale"  	   => self::$locale,
				"metadata"     => array(
					"token" => $create_token,
					"user_id" => get_current_user_id(),
				)
			);
			
			//$mollieData['consumerName'] = $checkout->cache['billing']['first_name'].' '.$checkout->cache['billing']['last_name'];
			$mollieData['billingEmail'] = $user->user_email;
			
			if ( isset( $checkout->cache['billing'] ) ) {
				$mollieData['billingAddress'] = $checkout->cache['billing']['street'];
				$mollieData['billingCity'] = $checkout->cache['billing']['city'];
				$mollieData['billingRegion'] = $checkout->cache['billing']['zone'];
				$mollieData['billingPostal'] = $checkout->cache['billing']['postal_code'];
				$mollieData['billingCountry'] = $checkout->cache['billing']['country'];
			}

			if ( isset( $checkout->cache['shipping'] ) ) {
				//$mollieData['consumerName'] = $checkout->cache['shipping']['first_name'].' '.$checkout->cache['shipping']['last_name'];
				$mollieData['shippingAddress'] = $checkout->cache['shipping']['street'];
				$mollieData['shippingCity'] = $checkout->cache['shipping']['city'];
				$mollieData['shippingRegion'] = $checkout->cache['shipping']['zone'];
				$mollieData['shippingPostal'] = $checkout->cache['shipping']['postal_code'];
				$mollieData['shippingCountry'] = $checkout->cache['shipping']['country'];
			}
			
			$mollie_payment = $mollie->payments->create($mollieData);


			if ( $mollie_payment->id ) {
				$_SESSION['WG_MOLLIE_TOKEN'] = $create_token; // save token to session (needed?)
				//self::$token = $create_token; // save token
				$save_payment_data_array = array('id' => $mollie_payment->id, 'status' => $mollie_payment->status);
				update_user_meta(get_current_user_id(), self::USER_META_TOKEN_PREFIX.$create_token, $save_payment_data_array);
				
				wp_redirect ( $mollie_payment->getPaymentUrl() );
				exit();
			
			} else {
				self::set_error_messages( self::__('Payment failed') );
				//self::set_error_messages( print_r($mollie_payment, true) );
				wp_redirect( WP_Groupbuy_Carts::get_url(), 303 );
				exit();
			}
		}
	}

	/**
	 * Store the token and payer ID that Mollie gives us
	 */
	public function back_from_mollie_payments() {
		if ( isset( $_GET['wg_mollie_return'] ) && isset( $_GET['token'] ) && isset( $_GET['payer_uid'] ) ) {
			$bln_payment_success = FALSE;
			$token_payment_data = get_user_meta( (int)$_GET['payer_uid'], self::USER_META_TOKEN_PREFIX.$_GET['token'], TRUE);
			if ( $token_payment_data ) {
				if ( $token_payment_data['status'] == 'paid' || $token_payment_data['status'] == 'paidout' ) {
					$bln_payment_success = TRUE;
					//Set action
					$_REQUEST['wg_checkout_action'] = 'back_from_mollie_payments';
				}
			}
			
			//If not valid yet, Check with api call
			if ( $bln_payment_success != TRUE && $token_payment_data['id']) {
				$result = self::validate_mollie_payment($_GET['payer_uid'], $_GET['token'], $token_payment_data['id'] );
				if ( $result ) {
					$bln_payment_success = TRUE;
					//Set action
					$_REQUEST['wg_checkout_action'] = 'back_from_mollie_payments';
				}
			}
			
			if ( self::DEBUG ) error_log('back_from_mollie_payments result. payment data. '.print_r($token_payment_data, true));
			
			if ( $bln_payment_success != TRUE ) {
				$cancel_url = add_query_arg( array('wg_mollie_cancel' => 1, 'token' => $_GET['token'], 'payer_uid' => $_GET['payer_uid'] ), WP_Groupbuy_Carts::get_url());
				wp_redirect( $cancel_url );
				exit();
			}
		} 
		/*elseif ( !isset( $_REQUEST['wg_checkout_action'] ) ) {
			// this is a new checkout. clear the token so we don't give things away for free
			if ( self::DEBUG ) error_log('back_from_mollie_payments: unsetting session token. '.$_SESSION['WG_MOLLIE_TOKEN']);
			unset( $_SESSION['WG_MOLLIE_TOKEN'] );
		}
		*/
	}
	
	public function webhook_listener() {
		if ( isset( $_GET['wg_mollie_listener'] ) && isset( $_GET['token'] ) && isset( $_GET['payer_uid'] ) ) {
			
			if ( self::DEBUG ) error_log('wg_mollie_listener for token: '.$_GET['token'].' POST: '.print_r($_POST, true));
			
			$mollie_payment_status = self::validate_mollie_payment($_GET['payer_uid'], $_GET['token'], $_POST["id"]);
			
			//If webhook and there is a wpg_order_id, then this is a follow up webhook (for a pending order)
			if ( $mollie_payment_status == 'paid' || $mollie_payment_status == 'paidout' ) {
				if ( self::DEBUG ) error_log('wg_mollie_listener (FOLLOW UP WEBHOOK) for token: '.$_GET['token'].' POST: '.print_r($_POST, true));
				$token_payment_data = get_user_meta( (int)$_GET['payer_uid'], self::USER_META_TOKEN_PREFIX.$_GET['token'], TRUE);
				if ( $token_payment_data['wpg_order_id'] ) {
					if ( self::DEBUG ) error_log('wg_mollie_listener (FOLLOW UP WEBHOOK) - mollie payment status: '.$mollie_payment_status.' - completing payment for order id: '.$token_payment_data['wpg_order_id'] );
					self::wpg_handle_completed_payment ( (int)$token_payment_data['wpg_order_id'] );
				}
			}
			
			//Don't continue page load
			exit();
		}
	}
	
	public function validate_mollie_payment($user_id, $token, $mollie_payment_id) {
		// Initialize Mollie API
		require_once dirname(__FILE__) . "/library/Mollie/API/Autoloader.php";
		$mollie = new Mollie_API_Client;
		$mollie->setApiKey(self::$api_key);
		
		$token_payment_data = get_user_meta( (int)$user_id, self::USER_META_TOKEN_PREFIX.$token, TRUE);
		
		//U pdate payment status (if exists, we have a valid one)
		if ( $token_payment_data ) {
			
			$payment = $mollie->payments->get($mollie_payment_id); //webhook sends mollie payment ID in POST
			
			if ( $payment->id ) {
				$save_token_payment_data = array('id' => $payment->id, 'status' => $payment->status);
				update_user_meta((int)$_GET['payer_uid'], self::USER_META_TOKEN_PREFIX.$_GET['token'], $save_token_payment_data);
				return $payment->status;
			} else {
				if ( self::DEBUG ) error_log('mollie web listener error: no valid payment found. '.print_r($payment, true));
				if ( self::DEBUG ) error_log('mollie web listener error: dump of POST. '.print_r($_POST, true));
			}
		}
		return FALSE;
	}

	/**
	 * Process a payment
	 */
	public function process_payment( WP_Groupbuy_Checkouts $checkout, WP_Groupbuy_Purchase $purchase ) {
		
		if ( self::DEBUG ) error_log('wg_mollie running process_payment: session token: '.$_SESSION['WG_MOLLIE_TOKEN']);
		
		if ( $purchase->get_total( self::get_payment_method() ) < 0.01 ) {
			$payments = WP_Groupbuy_Payment::get_payments_for_purchase( $purchase->get_id() );
			foreach ( $payments as $payment_id ) {
				$payment = WP_Groupbuy_Payment::get_instance( $payment_id );
				return $payment;
			}
		}

		//Initialize Mollie API
		require_once dirname(__FILE__) . "/library/Mollie/API/Autoloader.php";
		$mollie = new Mollie_API_Client;
		$mollie->setApiKey(self::$api_key);
		
		//Get token from user session
		$current_token = $_SESSION['WG_MOLLIE_TOKEN'];
		if ( !$current_token ) {
			return FALSE;
		}
		
		//Check for payment status (perhaps updated from webhook already)
		$token_payment_data = get_user_meta( get_current_user_id(), self::USER_META_TOKEN_PREFIX.$current_token, TRUE);
		
		//Update payment status (if exists, we have a valid one)
		if ( !$token_payment_data || !$token_payment_data['id'] ) {
			return FALSE;
		} else {
			
			$bln_create_wpg_payment = FALSE;
			
			$mollie_payment = $mollie->payments->get( $token_payment_data['id'] );
			
			//if already not complete, then lets check now
			if ( $mollie_payment->status == 'paid' || $mollie_payment->status == 'paidout' || $mollie_payment->status == 'open' || $mollie_payment->status == 'pending' ) {
				//completed, or pending (such as bank transfer)
				$bln_create_wpg_payment = TRUE;
			} else {
				$bln_create_wpg_payment = FALSE;
			} 
			
			//Update mollie status, only if valid lookup
			if ( isset($mollie_payment->id) && isset($mollie_payment->status) ) {
				$token_payment_data['id'] = $mollie_payment->id;
				$token_payment_data['status'] = $mollie_payment->status;
			}
			//Update payment data with order ID
			$token_payment_data['wpg_order_id'] = $purchase->get_id();
			update_user_meta( get_current_user_id(), self::USER_META_TOKEN_PREFIX.$current_token, $token_payment_data);
			update_user_meta( $purchase->get_id(), self::ORDER_META_MOLLIE, array('token' => $current_token, 'data' => $token_payment_data));
			
			if ( $bln_create_wpg_payment != TRUE ) {
				if ( self::DEBUG ) error_log('mollie process_payment error: no valid payment found. '.print_r($mollie_payment, true));
				if ( self::DEBUG ) error_log('mollie process_payment error: dump of token_payment_data. '.print_r($token_payment_data, true));
				return FALSE;
			} 
		}

		do_action( 'wg_log', __CLASS__ . '::' . __FUNCTION__ . ' - Mollie Payment Response (Raw)', $mollie_payment );

		// create loop of deals for the payment post
		$deal_info = array();
		foreach ( $purchase->get_products() as $item ) {
			if ( isset( $item['payment_method'][self::get_payment_method()] ) ) {
				if ( !isset( $deal_info[$item['deal_id']] ) ) {
					$deal_info[$item['deal_id']] = array();
				}
				$deal_info[$item['deal_id']][] = $item;
			}
		}
		$shipping_address = array();
		if ( isset( $checkout->cache['shipping'] ) ) {
			$shipping_address['first_name'] = $checkout->cache['shipping']['first_name'];
			$shipping_address['last_name'] = $checkout->cache['shipping']['last_name'];
			$shipping_address['street'] = $checkout->cache['shipping']['street'];
			$shipping_address['city'] = $checkout->cache['shipping']['city'];
			$shipping_address['zone'] = $checkout->cache['shipping']['zone'];
			$shipping_address['postal_code'] = $checkout->cache['shipping']['postal_code'];
			$shipping_address['country'] = $checkout->cache['shipping']['country'];
		}
		// create new payment
		$payment_id = WP_Groupbuy_Payment::new_payment( array(
				'payment_method' => self::get_payment_method(),
				'purchase' => $purchase->get_id(),
				'amount' => $mollie_payment->amount,
				'data' => array(
					'api_response' => array('id' => $mollie_payment->id, 'status' => $mollie_payment->status, 'method' => $mollie_payment->method, 'mode' => $mollie_payment->mode, 'amount' => $mollie_payment->amount, 'paidDatetime' => $mollie_payment->paidDatetime, 'metadata' => $mollie_payment->metadata ),
					'uncaptured_deals' => $deal_info
				),
				// 'transaction_id' => $response[], // TODO set the transaction ID
				'deals' => $deal_info,
				'shipping_address' => $shipping_address,
			), WP_Groupbuy_Payment::STATUS_AUTHORIZED );
		if ( !$payment_id ) {
			return FALSE;
		}
		$payment = WP_Groupbuy_Payment::get_instance( $payment_id );
		do_action( 'payment_authorized', $payment );
		
		// complete payment
		/*
		$items_captured = array(); // Creating simple array of items that are captured
		foreach ( $purchase->get_products() as $item ) {
			$items_captured[] = $item['deal_id'];
		}
		do_action( 'payment_captured', $payment, $items_captured );
		$payment->set_status( WP_Groupbuy_Payment::STATUS_COMPLETE );
		do_action( 'payment_complete', $payment );
		*/

		return $payment;
	}
	
	/**
	 * Complete the purchase after the process_payment action, otherwise vouchers will not be activated.
	 */
	public function complete_purchase( WP_Groupbuy_Purchase $purchase ) {
		
		//Get token from user session
		$current_token = $_SESSION['WG_MOLLIE_TOKEN'];
		if ( !$current_token ) {
			return FALSE;
		}
		
		//Check for payment status (perhaps updated from webhook already)
		$token_payment_data = get_user_meta( get_current_user_id(), self::USER_META_TOKEN_PREFIX.$current_token, TRUE);
		
		//Update payment status (if exists, we have a valid one)
		if ( !$token_payment_data || !$token_payment_data['id'] ) {
			unset($_SESSION['WG_MOLLIE_TOKEN']);
			return FALSE;
		} 
		
		//if already not complete, then lets check now
		$set_payment_status = WP_Groupbuy_Payment::STATUS_AUTHORIZED;
		if ( $token_payment_data['status'] == 'paid' || $token_payment_data['status'] == 'paidout' ) {
			//completed
			$set_payment_status = WP_Groupbuy_Payment::STATUS_COMPLETE;
			self::wpg_handle_completed_payment( $purchase->get_id() );
		} elseif ($token_payment_data['status'] == 'open' || $token_payment_data['status'] == 'pending' ) {
			//or pending (such as bank transfer)
			$set_payment_status = WP_Groupbuy_Payment::STATUS_PENDING;
			self::wpg_handle_pending_payment( $purchase->get_id() );
		} else {
			unset($_SESSION['WG_MOLLIE_TOKEN']);
			return FALSE;
		}
		
		unset($_SESSION['WG_MOLLIE_TOKEN']);
	}
	
	public function wpg_handle_completed_payment($purchase_id) {
		
		$purchase = WP_Groupbuy_Purchase::get_instance($purchase_id);
		
		$items_captured = array(); // Creating simple array of items that are captured
		foreach ( $purchase->get_products() as $item ) {
			$items_captured[] = $item['deal_id'];
		}
		$payments = WP_Groupbuy_Payment::get_payments_for_purchase($purchase->get_id());
		foreach ( $payments as $payment_id ) {
			$payment = WP_Groupbuy_Payment::get_instance($payment_id);
			
			// mark the items as captured
			$data = $payment->get_data();
			foreach ( $items_captured as $deal_id ) {
				unset( $data['uncaptured_deals'][$deal_id] );
			}
			$payment->set_data( $data );
			
			do_action('payment_captured', $payment, $items_captured);
			do_action('payment_complete', $payment);
			$payment->set_status(WP_Groupbuy_Payment::STATUS_COMPLETE);
		}
	}
	
	public function wpg_handle_pending_payment($purchase_id) {
		
		$purchase = WP_Groupbuy_Purchase::get_instance($purchase_id);
		
		$payments = WP_Groupbuy_Payment::get_payments_for_purchase($purchase->get_id());
		foreach ( $payments as $payment_id ) {
			$payment = WP_Groupbuy_Payment::get_instance($payment_id);
			
			$payment->set_status(WP_Groupbuy_Payment::STATUS_PENDING);
		}
	}
	
	public function manually_capture_purchase( WP_Groupbuy_Payment $payment ) {
		// is this the right payment processor? does the payment still need processing?
		if ( $payment->get_payment_method() == self::get_payment_method() && $payment->get_status() != WP_Groupbuy_Payment::STATUS_COMPLETE ) {
			$data = $payment->get_data();
			
			$purchase = WP_Groupbuy_Purchase::get_instance( $payment->get_purchase() );
			$purchase_mollie = get_user_meta( $purchase->get_id(), self::ORDER_META_MOLLIE, TRUE);
			
			$current_token = $purchase_mollie['token'];
			$token_payment_data = $purchase_mollie['data'];
			
			if ( self::DEBUG ) error_log('mollie manually_capture_purchase, for payment id: '.$payment->get_id().'. purchase_mollie_data: '.print_r($purchase_mollie, true));
			
			if ( !$current_token || !$token_payment_data['id'] ) {
				return FALSE;
			}
				
			//Initialize Mollie API
			require_once dirname(__FILE__) . "/library/Mollie/API/Autoloader.php";
			$mollie = new Mollie_API_Client;
			$mollie->setApiKey(self::$api_key);
			
			$mollie_payment = $mollie->payments->get( $token_payment_data['id'] );
				
			//if already not complete, then lets check now
			if ( $mollie_payment->status == 'paid' || $mollie_payment->status == 'paidout' ) {
				//completed, or pending (such as bank transfer)
				if ( self::DEBUG ) error_log('mollie manually_capture_purchase, for payment id: '.$payment->get_id().' - result: COMPLETING ORDER. mollie payment status: '.$mollie_payment->status);
				return TRUE;
			} else {
				if ( self::DEBUG ) error_log('mollie manually_capture_purchase, for payment id: '.$payment->get_id().' - result: NOT COMPLETING ORDER. mollie payment status: '.$mollie_payment->status);
				return FALSE;
			} 
		}
	}
	
	public function register_settings() {
		$page = WP_Groupbuy_Payment_Processors::get_settings_page();
		$section = 'wg_mollie_settings';
		add_settings_section( $section, self::__( 'Mollie Payments' ), array( $this, 'display_settingstion' ), $page );
	
		register_setting( $page, self::API_KEY_OPTION );
		register_setting( $page, self::LOCALE_OPTION );
		register_setting( $page, self::METHOD_OPTION );
		add_settings_field( self::API_KEY_OPTION, self::__( 'API Key' ), array( get_class(), 'display_api_key_field' ), $page, $section );
		add_settings_field( self::LOCALE_OPTION, self::__( 'Locale' ), array( get_class(), 'display_locale_field' ), $page, $section );
		add_settings_field( self::METHOD_OPTION, self::__( 'Method' ), array( get_class(), 'display_method_field' ), $page, $section );
	}

	public function display_api_key_field() {
		echo '<input type="text" name="'.self::API_KEY_OPTION.'" value="'.self::$api_key.'" size="50" />'.self::__( '<br><em>Use a "Test/Sandbox" API key to set to Test mode, or use a "Live" API key to set to Live mode.').'';
	}
	
	public function display_locale_field() {
		echo '<select name="'.self::LOCALE_OPTION.'">
						<option value="" '.selected(self::$locale, '', FALSE).'>'.self::__( 'Choose location').'</option>
						<option value="nl" '.selected(self::$locale, 'nl', FALSE).'>NL</option>
						<option value="fr" '.selected(self::$locale, 'fr', FALSE).'>FR</option>
						<option value="de" '.selected(self::$locale, 'de', FALSE).'>DE</option>
						<option value="es" '.selected(self::$locale, 'es', FALSE).'>ES</option>
						<option value="en" '.selected(self::$locale, 'en', FALSE).'>EN</option>
					</select>';
	}
	public function display_method_field() {
		echo '<select name="'.self::METHOD_OPTION.'">
						<option value="" '.selected(self::$method, '', FALSE).'>'.self::__( 'All methods').'</option>
						<option value="ideal" '.selected(self::$method, 'ideal', FALSE).'>'.self::__( 'iDeal').'</option>
						<option value="creditcard" '.selected(self::$method, 'creditcard', FALSE).'>'.self::__( 'Credit Card').'</option>
						<option value="mistercash" '.selected(self::$method, 'mistercash', FALSE).'>'.self::__( 'Mister Cash').'</option>
						<option value="sofort" '.selected(self::$method, 'sofort', FALSE).'>'.self::__( 'Sofort').'</option>
						<option value="banktransfer" '.selected(self::$method, 'banktransfer', FALSE).'>'.self::__( 'Bank Transfer').'</option>
						<option value="bitcoin" '.selected(self::$method, 'bitcoin', FALSE).'>'.self::__( 'Bitcoin').'</option>
						<option value="paypal" '.selected(self::$method, 'paypal', FALSE).'>'.self::__( 'PayPal').'</option>
						<option value="paysafecard" '.selected(self::$method, 'paysafecard', FALSE).'>'.self::__( 'PaySafeCard').'</option>
					</select>';
	}

	public function payment_controls( $controls, WP_Groupbuy_Checkouts $checkout ) {
		
		if ( isset($controls['review']) ) {
			$controls['review'] = str_replace( 'value="'.self::__('Review').'"', 'value="'.self::__('Purchase').'"', $controls['review']);
		}
		return $controls;
	}

	/**
	 * Grabs error messages from a Mollie response and displays them to the user
	 */
	private function set_error_messages( $message, $display = TRUE ) {
		if ( $display ) {
			self::set_message( $message, self::MESSAGE_STATUS_ERROR );
		} else {
			error_log( "error message from mollie: " . print_r( $message, true ) );
		}
	}
	
	public function display_exp_meta_box() {
		return WG_PATH . '/includes/controllers/payment-gateway/meta-boxes/exp-only.php';
	}

	public function display_price_meta_box() {
		return WG_PATH . '/includes/controllers/payment-gateway/meta-boxes/no-dynamic-price.php';
	}
	
	public function display_limits_meta_box() {
		return WG_PATH . '/includes/controllers/payment-gateway/meta-boxes/no-tipping.php';
	}
}
WPG_Mollie_Payments::register();