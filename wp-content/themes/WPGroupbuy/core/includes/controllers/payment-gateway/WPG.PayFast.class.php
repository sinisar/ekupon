<?php
class WP_Groupbuy_Payfast extends WP_Groupbuy_Offsite_Processors {
	const API_ENDPOINT_SANDBOX = 'sandbox.payfast.co.za';
	const API_ENDPOINT_LIVE = 'www.payfast.co.za';
	const API_REDIRECT_SANDBOX = 'https://sandbox.payfast.co.za/eng/process';
	const API_REDIRECT_LIVE = 'https://www.payfast.co.za/eng/process';
	const MODE_TEST = 'sandbox';
	const MODE_LIVE = 'live';
	const API_MODE_OPTION = 'wg_payfast_mode';
	const CANCEL_URL_OPTION = 'wg_payfast_cancel_url';
	const RETURN_URL_OPTION = 'wg_payfast_return_url';
	const API_MERCH_ID_OPTION = 'wg_payfast_merchant_id';
	const API_MERCH_KEY_OPTION = 'wg_payfast_merchant_key';
	const API_PDT_KEY_OPTION = 'wg_payfast_pdt_key';
	const CURRENCY_CODE_OPTION = 'wg_payfast_currency';
	const PAYMENT_METHOD = 'PayFast';
	const TOKEN_KEY = 'wg_token_key';
	const USE_PROXY = FALSE;
	protected static $instance;
	private static $response_data;
	private $cancel_url = '';
	private $return_url = '';
	private $merchant_id = '';
	private $merchant_key = '';
	private $pdt_key = '';
	private $currency_code = '';

	public static function get_instance() {
		if ( !(isset(self::$instance) && is_a(self::$instance, __CLASS__)) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	private static function get_api_url() {
		$api_mode = get_option(self::API_MODE_OPTION, self::MODE_TEST);
		if ( $api_mode == self::MODE_LIVE ) {
			return self::API_ENDPOINT_LIVE;
		} else {
			return self::API_ENDPOINT_SANDBOX;
		}
	}

	private function get_redirect_url() {
		if ( $this->api_mode == self::MODE_LIVE ) {
			return self::API_REDIRECT_LIVE;
		} else {
			return self::API_REDIRECT_SANDBOX;
		}
	}

	public function get_payment_method() {
		return self::PAYMENT_METHOD;
	}

	public static function returned_from_offsite() {
		return ( isset($_GET['pt']) && !empty($_GET['pt']) );
	}

	protected function __construct() {
		parent::__construct();
		$this->merchant_id = get_option(self::API_MERCH_ID_OPTION, '');
		$this->merchant_key = get_option(self::API_MERCH_KEY_OPTION, '');
		$this->pdt_key = get_option(self::API_PDT_KEY_OPTION, '');
		$this->api_mode = get_option(self::API_MODE_OPTION, self::MODE_TEST);
		$this->cancel_url = get_option(self::CANCEL_URL_OPTION, WP_Groupbuy_Accounts::get_url());
		$this->return_url = WP_Groupbuy_Checkouts::get_url();
		$this->currency_code = get_option(self::CURRENCY_CODE_OPTION, 'ZAR');
		add_action('admin_init', array($this, 'register_settings'), 10, 0);
		
		// Remove the review page since it's at payfast
		add_filter('wg_checkout_pages', array($this, 'remove_review_page'));
		
		// Send offsite
		add_action('wg_send_offsite_for_payment', array($this,'send_offsite'), 10, 1);
		// Handle the return of user from pxpay
		add_action('wg_load_cart', array($this,'back_from_payfast'), 10, 0);
		// Complete Purchase
		add_action('purchase_completed', array($this, 'complete_purchase'), 10, 1);
		
		// Change button
		add_filter('wg_checkout_payment_controls', array($this, 'payment_controls'), 20, 2);
		
		// Limitations
		add_filter( 'wp_groupbuy_template_meta_boxes/deal-expiration.php', array($this, 'display_exp_meta_box'), 10);
		add_filter( 'wp_groupbuy_template_meta_boxes/deal-price.php', array($this, 'display_price_meta_box'), 10);
		add_filter( 'wp_groupbuy_template_meta_boxes/deal-limits.php', array($this, 'display_limits_meta_box'), 10);
	}

	public static function checkout_icon() {
		return 'Payfast';
	}

	/**
	 * The review page is unnecessary
	 */
	public function remove_review_page( $pages ) {
		unset($pages[WP_Groupbuy_Checkouts::REVIEW_PAGE]);
		return $pages;
	}

	public static function register() {
		self::add_payment_processor(__CLASS__, self::__('PayFast'));
	}

	/**
	 * Instead of redirecting to the WPG checkout page,
	 * set up the PxPay Request and redirect
	 */
	public function send_offsite( WP_Groupbuy_Checkouts $checkout ) {
		$cart = $checkout->get_cart();
		if ( $cart->get_total() < 0.01 ) { // for free deals.
			return;
		}

		if ( $_REQUEST['wg_checkout_action'] == WP_Groupbuy_Checkouts::PAYMENT_PAGE ) {
			
			$user = get_userdata(get_current_user_id());
			$account = WP_Groupbuy_Account::get_instance(get_current_user_id());
			
			$filtered_total = $this->get_payment_request_total($checkout);
			if ( $filtered_total < 0.01 ) {
				return array();
			}
			
			$line_items = array();
			if ( $filtered_total < $cart->get_total() ) {
				$line_items[] .=  self::__('Credits Applied to Total:') . sprintf('%0.2f', $cart->get_subtotal() + $filtered_total - $cart->get_total());
			}
			
			
			foreach ( $cart->get_items() as $key => $item ) {
				$deal = WP_Groupbuy_Deal::get_instance($item['deal_id']);
				$line_items[] .= 'Item: '.$deal->get_title($item['data']).' Quantity: '.$item['quantity'].' Price: '.sprintf('%0.2f', $deal->get_price()).' Item #: '.$item['deal_id'];
			}
			
			$payfast_url = add_query_arg(
				array(
					// Merchant details
					'merchant_id' => $this->merchant_id,
					'merchant_key' => $this->merchant_key,
					'return_url' => $this->return_url,
					'cancel_url' => $this->cancel_url,
					'notify_url' => WP_Groupbuy_Offsite_Processor_Handler::get_url(),
	
					// Item details
					'item_name' => urlencode(get_bloginfo('name')),
					'item_description' => urlencode(implode(', ', $line_items)),
					
					// Order Details
					'amount' => sprintf('%0.2f', $filtered_total),
					
					// Customer Details
					'name_first' => urlencode($account->get_name('first_name')),
					'name_last' => urlencode($account->get_name('last_name')),
					'email_address' => urlencode($user->user_email),
				),
			self::get_redirect_url() );
			
			if ( self::DEBUG ) {
				error_log('----------Payfast Redirect----------');
				error_log( print_r($payfast_url, TRUE) );
				error_log( print_r(wp_parse_args($payfast_url),TRUE) );
			}
			// This is where you redirect
			wp_redirect ( $payfast_url );
			exit();
		
		}
		
	}

	/**
	 * Process a payment
	 */
	public function process_payment( WP_Groupbuy_Checkouts $checkout, WP_Groupbuy_Purchase $purchase ) {

		if ( $purchase->get_total(self::get_payment_method()) < 0.01 ) {
			$payments = WP_Groupbuy_Payment::get_payments_for_purchase($purchase->get_id());
			foreach ( $payments as $payment_id ) {
				$payment = WP_Groupbuy_Payment::get_instance($payment_id);
				return $payment;
			}
		}

		// create loop of deals for the payment post
		$deal_info = array();
		foreach ( $purchase->get_products() as $item ) {
			if ( isset($item['payment_method'][self::get_payment_method()]) ) {
				if ( !isset($deal_info[$item['deal_id']]) ) {
					$deal_info[$item['deal_id']] = array();
				}
				$deal_info[$item['deal_id']][] = $item;
			}
		}

		// create new payment
		$payment_id = WP_Groupbuy_Payment::new_payment(array(
			'payment_method' => self::get_payment_method(),
			'purchase' => $purchase->get_id(),
			'amount' => $purchase->get_total(self::get_payment_method()),
			'data' => array(
				'api_response' => self::$response_data,
				'uncaptured_deals' => $deal_info
			),
			'deals' => $deal_info,
		), WP_Groupbuy_Payment::STATUS_COMPLETE);
		if ( !$payment_id ) {
			return FALSE;
		}

		// send data back to complete_checkout
		$payment = WP_Groupbuy_Payment::get_instance($payment_id);
		do_action( 'payment_authorized', $payment);
		return $payment;

	}

	/**
	 * Complete the purchase after the process_payment action, otherwise vouchers will not be activated.
	 */
	public function complete_purchase( WP_Groupbuy_Purchase $purchase ) {
		$items_captured = array(); // Creating simple array of items that are captured
		foreach ( $purchase->get_products() as $item ) {
			$items_captured[] = $item['deal_id'];
		}
		$payments = WP_Groupbuy_Payment::get_payments_for_purchase($purchase->get_id());
		foreach ( $payments as $payment_id ) {
			$payment = WP_Groupbuy_Payment::get_instance($payment_id);
			do_action('payment_captured', $payment, $items_captured);
			do_action('payment_complete', $payment);
			$payment->set_status(WP_Groupbuy_Payment::STATUS_COMPLETE);
		}
	}

	/**
	* Handle a received IPN.
	*/
	public function back_from_payfast() {
		do_action('payment_handle_payfast', $itn);
		if ( self::returned_from_offsite() ) {
			if ( self::pdt_validation($_GET['pt']) ) {
				$_REQUEST['wg_checkout_action'] = 'back_from_payfast';
			}
		}
	}
	
	public static function pdt_validation( $pt ) {
		$req = 'pt='.$pt.'&at='.get_option(self::API_PDT_KEY_OPTION);
		$host = self::get_api_url();
		// Construct Header
		$header = "POST /eng/query/fetch HTTP/1.0\r\n";
		$header .= 'Host: '. $host ."\r\n";
		$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
		$header .= 'Content-Length: '. strlen( $req ) ."\r\n\r\n";
 
		// Connect to server
		$socket = fsockopen( 'ssl://' . $host, 443, $errno, $errstr, 10 );
		if ( $socket ) {
			// Send command to server
			fputs( $socket, $header . $req );
	 
			// Read the response from the server
			$response = '';
			$header_done = false;
	 
			while( !feof( $socket ) ) {
				$line = fgets( $socket, 1024 );
	 
				// Check if we are finished reading the header yet
				if( strcmp( $line, "\r\n" ) == 0 ) {
					// read the header
					$header_done = true;
				}
				// If header has been processed
				elseif ( $header_done ) {
					// Read the main response
					$response .= $line;
				}
			}
			// Parse the returned data
			$lines = explode( "\n", $response );
			// If the transaction was successful
			if( strcmp( trim( $lines[0] ), 'SUCCESS' ) == 0 ) {
				$data = array();	
				// Process the reponse into an associative array of data
				for( $i = 1; $i < count( $lines ); $i++ ) {
					list( $key, $val ) = explode( "=", $lines[$i] );
					$data[urldecode( $key )] = stripslashes( urldecode( $val ) );
				}
				self::$response_data = $data;
				return TRUE;
			} elseif ( strcmp( $result, 'FAIL' ) == 0 ) { // If the transaction was NOT successful
				return FALSE;
			}
		}
	}
	
	public function payment_controls( $controls, WP_Groupbuy_Checkouts $checkout ) {
		
		if ( isset($controls['review']) ) {
			$style = 'style="box-shadow: none;-moz-box-shadow: none;-webkit-box-shadow: none; display: block; width: 100px; height: 60px; background-color: transparent; background-image: url(https://www.payfast.co.za/images/buttons/buynow_basic_logo.gif); background-position: 0 0; background-repeat: no-repeat; padding: 42px 0 0 0; border: none; cursor: pointer; text-indent: -9000px; margin-top: 12px;"';
			$controls['review'] = str_replace( 'value="'.self::__( 'Review' ).'"', $style . ' value="'.self::__( 'Payfast' ).'"', $controls['review'] );
		}

		return $controls;
	}

	private function get_currency_code() {
		return apply_filters('wg_payfast_currency_code', $this->currency_code);
	}

	public function register_settings() {
		$page = WP_Groupbuy_Payment_Processors::get_settings_page();
		$section = 'wg_paypalwpp_settings';
		add_settings_section($section, self::__('Payfast Adaptive Payments'), array($this, 'display_settings_section'), $page);
		register_setting($page, self::API_MODE_OPTION);
		register_setting($page, self::API_MERCH_ID_OPTION);
		register_setting($page, self::API_MERCH_KEY_OPTION);
		register_setting($page, self::API_PDT_KEY_OPTION);
		register_setting($page, self::CURRENCY_CODE_OPTION);
		register_setting($page, self::RETURN_URL_OPTION);
		register_setting($page, self::CANCEL_URL_OPTION);
		add_settings_field(self::API_MODE_OPTION, self::__('Mode'), array($this, 'display_api_mode_field'), $page, $section);
		add_settings_field(self::API_MERCH_ID_OPTION, self::__('Merchant ID'), array($this, 'display_api_username_field'), $page, $section);
		add_settings_field(self::API_MERCH_KEY_OPTION, self::__('Merchant Key'), array($this, 'display_api_password_field'), $page, $section);
		add_settings_field(self::API_PDT_KEY_OPTION, self::__('PDT Key'), array($this, 'display_api_key_field'), $page, $section);
		add_settings_field(self::CURRENCY_CODE_OPTION, self::__('Currency Code'), array($this, 'display_currency_code_field'), $page, $section);
		add_settings_field(self::RETURN_URL_OPTION, self::__('Return URL'), array($this, 'display_return_field'), $page, $section);
		add_settings_field(self::CANCEL_URL_OPTION, self::__('Cancel URL'), array($this, 'display_cancel_field'), $page, $section);
	}

	public function display_api_username_field() {
		echo '<input type="text" name="'.self::API_MERCH_ID_OPTION.'" value="'.$this->merchant_id.'" size="80" />';
	}

	public function display_api_password_field() {
		echo '<input type="text" name="'.self::API_MERCH_KEY_OPTION.'" value="'.$this->merchant_key.'" size="80" />';
	}

	public function display_api_key_field() {
		echo '<input type="text" name="'.self::API_PDT_KEY_OPTION.'" value="'.$this->pdt_key.'" size="80" />';
	}

	public function display_api_mode_field() {
		echo '<label><input type="radio" name="'.self::API_MODE_OPTION.'" value="'.self::MODE_LIVE.'" '.checked(self::MODE_LIVE, $this->api_mode, FALSE).'/> '.self::__('Live').'</label><br />';
		echo '<label><input type="radio" name="'.self::API_MODE_OPTION.'" value="'.self::MODE_TEST.'" '.checked(self::MODE_TEST, $this->api_mode, FALSE).'/> '.self::__('Sandbox').'</label>';
	}

	public function display_currency_code_field() {
		echo '<input type="text" name="'.self::CURRENCY_CODE_OPTION.'" value="'.$this->currency_code.'" size="5" />';
	}

	public function display_return_field() {
		echo '<input type="text" disabled="disabled" name="'.self::RETURN_URL_OPTION.'" value="'.$this->return_url.'" size="80" class="disabled"/>';
	}

	public function display_cancel_field() {
		echo '<input type="text" name="'.self::CANCEL_URL_OPTION.'" value="'.$this->cancel_url.'" size="80" />';
	}

	public function review_controls()
	{
		echo '<div class="checkout-controls">
				<input type="hidden" name="" value="'.self::CHECKOUT_ACTION.'">
				<input class="form-submit submit checkout_next_step" type="submit" value="'.self::__('PayFast').'" name="wg_checkout_button" />
			</div>';
	}

	/**
	 * Get error messages from a PayFast response and displays them to the user
	 */
	private function set_error_messages( $message, $display = TRUE ) {
		if ( $display ) {
			self::set_message($message, self::MESSAGE_STATUS_ERROR);
		} else {
			error_log($message);
		}
	}

	public function display_exp_meta_box()
	{
		return WG_PATH . '/includes/controllers/payment-gateway/meta-boxes/exp-only.php';
	}

	public function display_price_meta_box()
	{
		return WG_PATH . '/includes/controllers/payment-gateway/meta-boxes/no-dynamic-price.php';
	}

	public function display_limits_meta_box()
	{
		return WG_PATH . '/includes/controllers/payment-gateway/meta-boxes/no-tipping.php';
	}
}
WP_Groupbuy_Payfast::register();