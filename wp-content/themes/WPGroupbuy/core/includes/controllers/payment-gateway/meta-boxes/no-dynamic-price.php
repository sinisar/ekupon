<div class="wg_meta_column float_left" style="float:none;">
		<p>
			<label for="redirect_url"><strong><?php wpg_e( 'Redirect URL (for Buy Now button)' ); ?>:</strong></label>
			<input id="redirect_url" type="text" size="38" value="<?php echo $redirect_url; ?>" name="redirect_url" /> <img width="16" height="16" src="<?php echo (WG_RESOURCES . 'images/help.png')?>" class="help_tip" title="<?php wpg_e('If you want to redirect user to other URL when click Buy Now button, enter URL here.' ) ?>">
		</p>
		
	</div>
	<span class="meta_box_block_divider" style="margin-top: 10px; margin-bottom: 10px;"></span>
<p>
	<label for="deal_base_price"><strong><?php self::_e( 'Price' ); ?>:</strong></label>
	&nbsp;
	<?php wg_currency_symbol();  ?><input id="deal_base_price" type="text" size="5" value="<?php echo $price; ?>" name="deal_base_price" />
</p>

<span class="meta_box_block_divider"></span>
<?php do_action( 'wg_meta_box_deal_price_left', $price, $dynamic_price, $shipping, $shippable, $shipping_dyn, $shipping_mode, $tax, $taxable, $taxrate ) ?>

<?php do_action( 'wg_meta_box_deal_price_right', $price, $dynamic_price, $shipping, $shippable, $shipping_dyn, $shipping_mode, $tax, $taxable, $taxrate ) ?>

<?php do_action( 'wg_meta_box_deal_price', $price, $dynamic_price, $shipping, $shippable, $shipping_dyn, $shipping_mode, $tax, $taxable, $taxrate ) ?>
