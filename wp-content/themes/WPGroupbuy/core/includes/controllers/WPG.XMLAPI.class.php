<?php
/**
 * Created by PhpStorm.
 * User: sinisa
 * Date: 02/07/16
 * Time: 17:41
 * Edited: 24/10/16
 * Time: 21:27
 */

add_action( 'wg_init_classes', array( 'WP_Groupbuy_XMLAPI', 'init' ), 56 );

class WP_Groupbuy_XMLAPI extends WP_Groupbuy_Controller
{
    const API_BASE_PATH = 'wpg/api.xml';
    const TIMEOUT = 120;
    const MAX_ITEMS_IN_FEED = 100;
    protected static $private_key = '';

    public static function init() {
        add_action('wg_router_generate_routes', array(get_class(), 'register_path_callbacks'), 10, 1);
    }

    /**
     * Setup all the API callbacks
     */
    public static function register_path_callbacks($router)
    {
        $routes = array(
            // Deals
            'wg-xmlapi-all-deals' => array(
                'path' => self::API_BASE_PATH . '/allDeals$',
                'page_callback' => array(__CLASS__, 'get_all_deals'),
                'title' => self::__('All Deals'),
                'template' => FALSE,
            ),
        );

        foreach ($routes as $id => $route) {
            $router->add_route($id, $route);
        }
    }

    public function get_all_deals() {
        $title = get_bloginfo('title');
        $description = get_bloginfo('description');
        $link = site_url();
        $xml_response = '';
        $xml_header = '<?xml version="1.0" encoding="UTF-8"?>';
        $xml_response .= $xml_header . '<channel>' . self::get_xml_title($title) . self::get_xml_description($description) . self::get_xml_link($link);
        $xml_items = array();
		
		$args = array(
            'post_type' => WP_Groupbuy_Deal::POST_TYPE,
            'post_status' => 'publish', // need to know posts that were deleted
            'suppress_filters' => TRUE, // so we can filter the post modified date later
        );
        if ( isset( $_REQUEST['max'] ) && (int)$_REQUEST['max'] < self::MAX_ITEMS_IN_FEED ) {
            $args['posts_per_page'] = (int)$_REQUEST['max'];
        } else {
            $args['posts_per_page'] = self::MAX_ITEMS_IN_FEED;
        }
        if ( isset( $_REQUEST['location'] ) && $_REQUEST['location'] ) {
            $locations = explode( ',', $_REQUEST['location'] );
            foreach ( $locations as $location_id ) {
                $children = get_term_children( $location_id, WP_Groupbuy_Deal::LOCATION_TAXONOMY );
                $locations = array_merge( $locations, $children );
            }
            $args['tax_query']['relation'] = 'AND';
            $args['tax_query'][] = array(
                'taxonomy' => WP_Groupbuy_Deal::LOCATION_TAXONOMY,
                'field' => 'id',
                'terms' => $locations,
                'operator' => 'IN',
            );
        }
        if ( isset( $_REQUEST['category'] ) && $_REQUEST['category'] ) {
            $categories = explode( ',', $_REQUEST['category'] );
            foreach ( $categories as $category_id ) {
                $children = get_term_children( $category_id, WP_Groupbuy_Deal::CAT_TAXONOMY );
                $categories = array_merge( $categories, $children );
            }
            $args['tax_query']['relation'] = 'AND'; // in case it wasn't set earlier
            $args['tax_query'][] = array(
                'taxonomy' => WP_Groupbuy_Deal::CAT_TAXONOMY,
                'field' => 'id',
                'terms' => $categories,
                'operator' => 'IN',
            );
        }
        if ( isset( $_REQUEST['expired'] ) ) {
            // expired shows expired deals,
            // anything other argument will show non expired.
            $compare = ( $_REQUEST['expired'] == 'expired' ) ? 'BETWEEN' : 'NOT BETWEEN' ;
            $args['meta_query'][] = array(
                'key' => '_expiration_date',
                'value' => array( 0, current_time( 'timestamp' ) ),
                'compare' => $compare
            );
        }
		
        // Get posts
        $posts = get_posts( $args );
        //error_reporting(E_ALL);
		$output = array();
        foreach ( $posts as $post ) {
			$post_id = $post->ID;
            $output[ $post_id ] = self::build_deal( $post_id );
			if ($post_id == 16122) {
				//print_r($output[ $post_id ]); exit;
			}
			$xml_item = self::create_xml_item($output[ $post_id ]);
            $xml_items[] = $xml_item;
        }
		
        $items = '';

        foreach($xml_items as $xml_item) {
            $items .= '<item>'.$xml_item.'</item>';
        }

        $xml_response .=$items;
        $xml_response .='</channel>';
        header( 'Content-type: text/xml' );
        echo $xml_response;
        exit();
    }

    public function build_deal( $deal_id ) {
        if ( get_post_type( $deal_id ) != WP_Groupbuy_Deal::POST_TYPE )
            return;
        $deal = WP_Groupbuy_Deal::get_instance( $deal_id );
        $post = get_post( $deal_id );
        $item = array(
            'id' => $deal_id,
            'title' => $deal->get_title(),
            'slug' => $post->post_name,
            'post_content' => apply_filters( 'the_content', $post->post_content ),
            'post_date' => $post->post_date,
            'post_date_gmt' => $post->post_date_gmt,
            'post_status' => $post->post_status,
            'post_modified' => $post->post_modified,
            'post_modified_gmt' => $post->post_modified_gmt,
            'guid' => $post->guid,
            'url' => get_permalink( $deal_id ),
            'thumb_url' => wp_get_attachment_thumb_url( get_post_thumbnail_id( $deal_id ) ),
            'images' => WP_Groupbuy_API::get_attachments( $deal_id, 'image' ),
            'attachments' => WP_Groupbuy_API::get_attachments( $deal_id, 'file' ),
            'status' => $deal->get_status(),
            'amount_saved' => $deal->get_amount_saved(), // string
            'capture_before_expiration' => $deal->capture_before_expiration(), // bool
            'dynamic_price' => $deal->get_dynamic_price(), // array
            'expiration_date' => $deal->get_expiration_date(), // int
            'fine_print' => $deal->get_fine_print(), // string
            'highlights' => $deal->get_highlights(), // string
            'max_purchases' => $deal->get_max_purchases_per_user(), // int
            'max_purchases_per_user' => $deal->get_max_purchases(), // int
            'merchant_id' => $deal->get_merchant_id(), // int
            'min_purchases' => $deal->get_min_purchases(), // int
            'number_of_purchases' => $deal->get_number_of_purchases(), // int
            'remaining_allowed_purchases' => $deal->get_remaining_allowed_purchases(), // int
            'remaining_required_purchases' => $deal->get_remaining_required_purchases(), // int
            'taxable' => $deal->get_taxable(), // bool
            'shippable' => $deal->get_shipping(), // string
            'rss_excerpt' => $deal->get_rss_excerpt(), // string
            'fullPrice' => $deal->get_price(), // float
            'discountPrice' => $deal->get_value(), // string
            'couponPrice' => $deal->get_coupon_value()[0],
            'voucher_expiration_date' => $deal->get_voucher_expiration_date(), // string
            'voucher_how_to_use' => $deal->get_voucher_how_to_use(), // string
            'voucher_locations' => $deal->get_voucher_locations(), // array
            'voucher_logo' => $deal->get_voucher_logo(), // int
            'voucher_map' => $deal->get_voucher_map(), // string
            'locations' => get_the_terms( $deal_id, wg_get_deal_location_tax() ),
            'categories' => get_the_terms( $deal_id, wg_get_deal_cat_slug() ),
            'tags' => get_the_terms( $deal_id, wg_get_deal_tag_slug() ),
        );

        if ( function_exists('wg_deal_has_attributes') && wg_deal_has_attributes( $deal_id ) ) {
            foreach ( WP_Groupbuy_Attribute::get_attributes( $deal_id, 'post' ) as $post ) {
                $attribute_id = $post->ID;
                $attribute = WP_Groupbuy_Attribute::get_instance( $attribute_id );
                $item['attributes'][$attribute_id] = array(
                    'title' => $post->post_title,
                    'description' => $attribute->get_description(),
                    'max_purchases' => $attribute->get_max_purchases(),
                    'remaining_purchases' => $attribute->remaining_purchases(),
                    'categories' => $attribute->get_categories(),
                    'sku' => $attribute->get_sku(),
                    'price' => $attribute->get_price(),
                    'post_date' => $post->post_date,
                    'post_date_gmt' => $post->post_date_gmt,
                    'post_status' => $post->post_status,
                    'post_modified' => $post->post_modified,
                    'post_modified_gmt' => $post->post_modified_gmt
                );
            }
        }
        return $item;
    }

    private function get_xml_title($title_text) {
        return '<title>'.$title_text.'</title>';
    }


    private function get_xml_description($description) {
        return '<description>'.$description.'</description>';
    }


    private function get_xml_link($link) {
        return '<link>'.$link.'</link>';
    }

    private function create_xml_item($deal) {
        $output = '';
        $id = '<id>'.$deal['id'].'</id>';
        $category = '<category>'. (self::concat_categories($deal['categories'])).'</category>';
        $url = '<url>'.$deal['url'].'</url>';
        $title = '<title>'.$deal['title'].'</title>';
        $desc = '<description>'. str_replace("&nbsp;", '', $deal['post_content']).'</description>';
        $image = '<image>'.$deal['images']['url'].'</image>';
        $pledgeURL = '<pledgeURL>' . $deal['url'] . '</pledgeURL>';
        $initialAmount = '<initialAmmount>'. $deal['min_purchases'] .'</initialAmmount>';
        $availableAmount = '<availableAmmount>' . $deal['max_purchases'] .'</availableAmmount>';
        $offerStartTS = '<offerStartTS>'. $deal['post_date'] .'</offerStartTS>';
        $offerStopTS = '<offerStopTS>'. date('Y-m-d H:m:s', $deal['expiration_date']) . '</offerStopTS>';
        $fullPrice = '<fullPrice>'. number_format($deal['fullPrice'], 2) .'</fullPrice>';
        $discountPrice = '<discountPrice>'. number_format($deal['discountPrice'], 2) .'</discountPrice>';
        $prepaid = '<prepaid>'. number_format($deal['couponPrice'], 2) .'</prepaid>';
        $offerOpen = '<offerOpen>'. ($deal['status'] == 'open' ? 'true' : 'false' ) .'</offerOpen>';

        //  location
        $location = '';
        if($deal['merchant_id'] != '') {
            $location = self::create_xml_location($deal);
        }

        $output .= $id.
                    $category.
                    $url.
                    $title.
                    $desc.
                    $image.
                    $pledgeURL.
                    $initialAmount.
                    $availableAmount.
                    $offerStartTS.
                    $offerStopTS.
                    $fullPrice.
                    $discountPrice.
                    $prepaid.
                    $offerOpen.
                    $location;
        return $output;
    }

    private function concat_categories($categories) {
        $concat_categories = '';
        foreach($categories as $category) {
            $concat_categories .= $category->name . ",";
        }
        return rtrim($concat_categories, ',');
    }

    private function create_xml_location($deal) {
		//print $deal['id'].',';
        $merchant = WP_Groupbuy_Merchant::get_instance($deal['merchant_id']);
        $city = '<city>'. $merchant->get_contact_city() .'</city>';
        $zip = '<ZIP>'. $merchant->get_contact_postal_code() .'</ZIP>';
        $address = '<address>'. $merchant->get_contact_street() .'</address>';
        $location = '<location>' . $city. $zip. $address.'</location>';

        return $location;
    }

}