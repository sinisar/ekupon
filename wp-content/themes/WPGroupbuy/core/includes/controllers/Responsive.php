<?php

add_action( 'wg_init_controllers', array( 'WPG_Responsive', 'init' ), 91 );

class WPG_Responsive extends WP_Groupbuy_Controller {

	public static function init() {
		add_action( 'admin_init', array( get_called_class(), 'register_settings_fields' ), 20, 0 );
		add_action( 'wp_enqueue_scripts', array( get_called_class(), 'enqueue_scripts' ) );

		add_shortcode('responsive', array( get_called_class(), 'make_responsive') );
		add_shortcode('visibility', array( get_called_class(), 'responsive_visibility' ) );
	}

	public static function enqueue_scripts() {
		if ( get_option('enable_responsive', 1 ) ){
			wp_enqueue_style( 'wpg_responsive', get_template_directory_uri().'/style/responsive.css', null, wg_ptheme_current_version() );
			wp_enqueue_style( 'media_queries_style_menu', get_template_directory_uri().'/style/jquery.sidr.dark.css', null, wg_ptheme_current_version() );
		}
	}

	public static function register_settings_fields() {
		$page = WP_Groupbuy_Theme_UI::get_settings_page();

		add_settings_field( 'enable_responsive', self::__( 'Enable Responsive' ), array( get_class(), 'display_enable_reponsive' ), $page );
		register_setting( $page, 'enable_responsive' );

	}

	public static function display_enable_reponsive() {
		$active = get_option( 'enable_responsive', '0' );
		echo '<label name="'.'enable_responsive'.'"><input type="checkbox" value="1" name="'.'enable_responsive'.'" '.checked( '1', $active, false ).' /> </label>';
		echo '<img width="16" height="16" src="'.WG_RESOURCES . 'images/help.png'. '" class="help_tip" title="'.self::__( 'Check to enable responsive version that supports mobile devices' ).'">';
	}


	/* Responsive Images */

	function make_responsive( $atts, $content = null ) {
	    extract( shortcode_atts(array(), $atts) );
		return '<span class="responsive">' . do_shortcode($content) . '</span>';
	}

	/* Responsive Visibility */

	public function responsive_visibility( $atts, $content = null) {
		extract( shortcode_atts( array( 'show' => 'desktop' ), $atts ) );
		return '<div class="visibility-' . $show . '">' . do_shortcode($content) . '</div>';
	}
}

