<?php

add_action( 'wg_init_controllers', array( 'WP_Groupbuy_Purchases', 'init' ), 60 );

class WP_Groupbuy_Purchases extends WP_Groupbuy_Controller {
	const ORDER_LU_OPTION = 'wg_order_lookup_path';
	const AUTH_FORM_INPUT = 'order_billing_city';
	const AUTH_FORM_ID_INPUT = 'order_id';
	const NONCE_ID = 'wg_order_lookup_nonce';
	private static $lookup_path = 'order-tracking';

	public static function init() {
		self::$lookup_path = get_option( self::ORDER_LU_OPTION, self::$lookup_path );

		// Wrapper Template
		add_filter( 'template_include', array( get_class(), 'override_template' ) );
		// Modify Content for purchase template
		add_action( 'the_post', array( get_class(), 'purchase_content' ), 10, 1 );
		add_filter( 'the_title', array( get_class(), 'get_title' ), 10, 2 );

		// Order Tracking
		add_action( 'wg_router_generate_routes', array( get_class(), 'register_path_callback' ), 10, 1 );
		add_action( 'admin_init', array( get_class(), 'register_settings_fields' ), 50, 1 );

		// Admin
		self::$settings_page = self::register_settings_page( 'purchase_records', self::__( 'Orders' ), self::__( 'Purchases' ), 8.1, FALSE, 'records', array( get_class(), 'display_table' ) );
	}

	// Override template for the purchase post type
	public static function override_template( $template ) {
		$post_type = get_query_var( 'post_type' );
		if ( $post_type == WP_Groupbuy_Purchase::POST_TYPE ) {
			if ( is_single() ) {
				$template = self::locate_template( array(
						'account/single-purchase.php',
						'account/single-order.php',
						'order/single.php',
						'orders/single.php',
						'purchase/single.php',
						'purchases/single.php',
						'order.php',
						'purchase.php',
						'account.php'
					), $template );
			}
		}
		return $template;
	}

	// Update the global $pages array with the HTML for the current checkout page
	public static function purchase_content( $post ) {
		if ( $post->post_type == WP_Groupbuy_Purchase::POST_TYPE && is_single() ) {
			$purchase_id = $post->ID;
			$purchase = WP_Groupbuy_Purchase::get_instance( $purchase_id );
			// Remove content filter
			remove_filter( 'the_content', 'wpautop' );

			if ( self::authorized_user( $purchase_id ) ) {
				$args = array(
					'order_number' => $purchase_id,
					'tax' => $purchase->get_tax_total(),
					'shipping' => $purchase->get_shipping_total(),
					'total' => $purchase->get_total(),
					'products' => $purchase->get_products()
				);
				$view = self::load_view_to_string( 'purchase/order', $args );
			}
			else { // show the authentication form
				$view = self::lookup_page( TRUE );
			}
			// Display content
			global $pages;
			$pages = array( $view );
		}
	}

	// Filter 'the_title' to display the title of the current page, purchase or lookup
	public static function get_title( $title = '', $post_id = 0 ) {
		if ( WP_Groupbuy_Purchase::POST_TYPE == get_post_type( $post_id ) ) {
			if ( is_single() ) {
				$filtered = self::__( 'Order Tracking' );
				if ( self::authorized_user( $post_id ) ) {
					$filtered .= ': '.str_replace( 'Order ', '', $title );
				}
				return $filtered;
			}
		}
		return $title;
	}

	// Tracking view
	public function lookup_page( $return = FALSE ) {
		$args = array( 'action' => self::get_url(), 'nonce_id' => self::NONCE_ID, 'city_option_name' => self::AUTH_FORM_INPUT, 'order_option_name' => self::AUTH_FORM_ID_INPUT );
		remove_filter( 'the_content', 'wpautop' );
		if ( !$return ) {
			self::load_view( 'purchase/order-tracking', $args );
		} else {
			return self::load_view_to_string( 'purchase/order-tracking', $args );
		}
	}

	// Check to see if the user has access to view the purchase content.
	public static function authorized_user( $purchase_id, $user_id = 0 ) {
		$return = FALSE;
		if ( !$user_id ) {
			$user_id = get_current_user_id();
		}
		if ( WP_Groupbuy_Purchase::POST_TYPE != get_post_type( $purchase_id ) ) {
			return FALSE;
		}
		$purchase = WP_Groupbuy_Purchase::get_instance( $purchase_id );
		$purchase_user_id = $purchase->get_user();
		// If logged in or manually checked
		if ( $user_id ) {
			// Purchaser or admin
			if ( ( $user_id == $purchase_user_id ) || current_user_can( 'manage_options' ) ) {
				$return = TRUE;
			}
		}
		// Submitted form
		if ( // handle both submissions and $_GET variables from a redirect
			( isset( $_POST['wg_order_lookup_'.self::AUTH_FORM_INPUT] ) && $_POST['wg_order_lookup_'.self::AUTH_FORM_INPUT] != '' ) ||
			( isset( $_REQUEST[self::AUTH_FORM_INPUT] ) && $_REQUEST[self::AUTH_FORM_INPUT] != '' )
		) {
			// submitted form and has a matching billing city
			$account = WP_Groupbuy_Account::get_instance( $purchase_user_id );
			$address = $account ? $account->get_address() : array( 'city' => '' );
			$query = ( isset( $_REQUEST[self::AUTH_FORM_INPUT] ) ) ? $_REQUEST[self::AUTH_FORM_INPUT] : $_POST['wg_order_lookup_'.self::AUTH_FORM_INPUT] ;
			if ( strtolower( $address['city'] ) == strtolower( $query ) ) {
				$return = strtolower( $address['city'] );
			}
		}
		return apply_filters( 'wg_purchase_view_authorized_user', $return, $purchase_id, $user_id );
	}

	// Register the path callback for the order lookup page
	public static function register_path_callback( $router, $callback = '', $query_var = '', $view = NULL ) {
		$path = str_replace( '/', '-', self::$lookup_path );
		$args = array(
			'path' => self::$lookup_path,
			'title' => self::__( 'Order Tracking' ),
			'page_callback' => array( get_class(), 'lookup_page' ),
			'access_callback' => array( get_class(), 'process_form' ),
			'template' => array(
				self::get_template_path().'/'.str_replace( '/', '-', self::$lookup_path ).'.php',
				self::get_template_path().'/order.php', // theme override
				WG_PATH.'/templates/public/order.php', // default
			),
		);
		$router->add_route( 'wg_show_order_lookup', $args );
	}

	public function process_form() {
		$message = FALSE;
		if ( isset( $_POST['wg_order_lookup_'.self::AUTH_FORM_ID_INPUT] ) && $_POST['wg_order_lookup_'.self::AUTH_FORM_ID_INPUT] ) {
			if ( !wp_verify_nonce( $_POST['_wpnonce'], self::NONCE_ID ) ) {
				$message = self::__( 'Invalid Submission Attempt' );
			}
			else {
				$purchase_id = $_POST['wg_order_lookup_'.self::AUTH_FORM_ID_INPUT];
				if ( WP_Groupbuy_Purchase::POST_TYPE == get_post_type( $purchase_id ) ) {
					if ( $billing_auth = self::authorized_user( $purchase_id ) ) {
						$url = add_query_arg( array( self::AUTH_FORM_INPUT => $billing_auth ), get_permalink( $purchase_id ) );
						wp_redirect( $url );
						exit();
					} else {
						$message = self::__( 'Invalid Billing City' );
					}
				} else {
					$message = self::__( 'Order ID not found' );
				}
			}
		}
		if ( $message ) {
			self::set_message( $message );
		}
		return TRUE;
	}

	// The URL to the cart page
	public static function get_url() {
		if ( self::using_permalinks() ) {
			return trailingslashit( home_url() ).trailingslashit( self::$lookup_path );
		} else {
			$router = WG_Router::get_instance();
			return $router->get_url( 'wg_show_order_lookup' );
		}
	}

	public static function register_settings_fields() {
		$page = WP_Groupbuy_UI::get_settings_page();
		$section = 'wg_cart_paths';

		// Settings
		register_setting( $page, self::ORDER_LU_OPTION );
		add_settings_field( self::ORDER_LU_OPTION, self::__( 'Order Tracking Path' ), array( get_class(), 'display_path_option' ), $page, $section );
	}

	public static function display_path_option() {
		echo trailingslashit( get_home_url() ) . ' <input type="text" name="' . self::ORDER_LU_OPTION . '" id="' . self::ORDER_LU_OPTION . '" value="' . esc_attr( self::$lookup_path ) . '" size="40"/><br />';
	}


	public static function display_table() {
		//Create an instance of our package class...
		$wp_list_table = new WP_Groupbuy_Purchases_Table();
		//Fetch, prepare, sort, and filter our data...
		$wp_list_table->prepare_items();

		?>
		<script type="text/javascript" charset="utf-8">
			jQuery(document).ready(function($){
				jQuery(".wg_destroy").on('click', function(event) {
					event.preventDefault();
						if( confirm( '<?php wpg_e( "This will permanently trash the purchase and its associated voucher(s) and payment(s) which cannot be reversed. This will not reverse any payments or provide a credit to the customer. Are you sure?" ) ?>' ) ) {
							var $destroy_link = $( this ),
							destroy_purchase_id = $destroy_link.attr( 'ref' );
							$.post( ajaxurl, { action: 'wpg_destroyer', type: 'purchase', id: destroy_purchase_id, destroyer_nonce: '<?php echo wp_create_nonce( WP_Groupbuy_Destroy::NONCE ) ?>' },
								function( data ) {
										$destroy_link.parent().parent().parent().parent().fadeOut();
									}
								);
						} else {
							// nothing to do.
						}
				});
			});
		</script>
		<style type="text/css">
			#payment_deal_id-search-input, #purchase_id-search-input, #payment_account_id-search-input { width:5em; margin-left: 10px;}
		</style>
		<div class="wrap">
			<?php screen_icon(); ?>
			<h2 class="nav-tab-wrapper">
				<?php self::display_admin_tabs(); ?>
			</h2>

			 <?php $wp_list_table->views() ?>
			<form id="payments-filter" method="get" style="margin-top:5px;">
				<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
                                <input type="hidden" name="tab" value="<?php echo $_REQUEST['tab'] ?>" />
				<p class="search-box payment_search">
					<input style="width:200px;" type="text" id="payment_account_id-search-input" name="search_value" placeholder="<?php wpg_e( "Quick Search" ) ?>" value="<?php echo (isset($_GET['search_value']) ? $_GET['search_value'] : "" )?>">
                                        <?php
                                        $selectSearchBox = array(
                                            array("value" => "non_select", "text" => self::__( '- Select -' ), "selected" => "selected"),
                                            array("value" => "account_id", "text" => self::__( 'Account ID' ), "selected" => ""),
                                            array("value" => "email", "text" => self::__( 'User email' ), "selected" => ""),
                                            array("value" => "user_name", "text" => self::__( 'User fist and last name' ), "selected" => ""),
                                            array("value" => "deal_id", "text" => self::__( 'Deal ID' ), "selected" => ""),
                                            array("value" => "s", "text" => self::__( 'Order ID' ), "selected" => ""),
                                           
                                        );
                                        ?>
                                        <select name="search_type">
                                        <?php
                                        foreach($selectSearchBox as $el) {
                                             $el['selected'] = @$_GET['search_type'] == $el['value'] ? 'selected' : '';
                                             echo '<option '.$el['selected'].' value="'.$el['value'].'">'.$el['text'].'</option>';
                                        }
                                        ?>
                                        </select>
					<input type="submit" name="" id="search-submit" class="button" value="<?php self::_e( 'Search' ) ?>">
				</p>
				<?php $wp_list_table->display() ?>
			</form>
		</div>
		<?php
	}

	private function __clone() {
		trigger_error( __CLASS__.' may not be cloned', E_USER_ERROR );
	}
	private function __sleep() {
		trigger_error( __CLASS__.' may not be serialized', E_USER_ERROR );
	}
	public static function get_instance() {
		if ( !( self::$instance && is_a( self::$instance, __CLASS__ ) ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	private function __construct() {}
}


if ( !class_exists( 'WP_List_Table' ) ) {
	require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
}
class WP_Groupbuy_Purchases_Table extends WP_List_Table {
	protected static $post_type = WP_Groupbuy_Purchase::POST_TYPE;

	function __construct() {
		global $status, $page;

		//Set parent defaults
		parent::__construct( array(
				'singular' => 'order',
				'plural' => 'orders',
				'ajax' => false
			) );

	}

	function get_views() {

		$status_links = array();
		$num_posts = wp_count_posts( self::$post_type, 'readable' );
		$class = '';
		$allposts = '';

		$total_posts = array_sum( (array) $num_posts );

		// Subtract post types that are not included in the admin all list.
		foreach ( get_post_stati( array( 'show_in_admin_all_list' => false ) ) as $state )
			$total_posts -= $num_posts->$state;

		$class = empty( $_REQUEST['post_status'] ) ? ' class="current"' : '';
		$status_links['all'] = "<a href='admin.php?page=wp-groupbuy/voucher_records&tab=wp-groupbuy/purchase_records{$allposts}'$class>" . sprintf( _nx( 'All <span class="count">(%s)</span>', 'All <span class="count">(%s)</span>', $total_posts, 'posts' ), number_format_i18n( $total_posts ) ) . '</a>';

		foreach ( get_post_stati( array( 'show_in_admin_status_list' => true ), 'objects' ) as $status ) {
			$class = '';

			$status_name = $status->name;

			if ( empty( $num_posts->$status_name ) )
				continue;

			if ( isset( $_REQUEST['post_status'] ) && $status_name == $_REQUEST['post_status'] )
				$class = ' class="current"';

			// replace "Published" with "Complete".
			$label = str_replace( 'Published', 'Complete', translate_nooped_plural( $status->label_count, $num_posts->$status_name ) );
			$status_links[$status_name] = "<a href='admin.php?page=wp-groupbuy/voucher_records&tab=wp-groupbuy/purchase_records&post_status=$status_name'$class>" . sprintf( $label, number_format_i18n( $num_posts->$status_name ) ) . '</a>';
		}

		return $status_links;
	}

	function extra_tablenav( $which ) {
		?>
		<div class="alignleft actions"> <?php
		if ( 'top' == $which && !is_singular() ) {

			$this->months_dropdown( self::$post_type );

			do_action( 'wg_mngt_purchases_extra_tablenav' );

			submit_button( __( 'Filter' ), 'secondary', false, false, array( 'id' => 'post-query-submit' ) );
		} ?>
		</div> <?php
	}


	// Text or HTML to be placed inside the column <td>
	function column_default( $item, $column_name ) {
		switch ( $column_name ) {
		default:
			return apply_filters( 'wg_mngt_purchases_column_'.$column_name, $item ); // do action for those columns that are filtered in
		}
	}


	// Text to be placed inside the column <td> (movie title only)
	function column_title( $item ) {
		$purchase = WP_Groupbuy_Purchase::get_instance( $item->ID );
		$user_id = $purchase->get_original_user();
		$account_id = WP_Groupbuy_Account::get_account_id_for_user( $user_id );

		//Build row actions
		$actions = array(
			'payment'    => sprintf( '<a href="admin.php?page=wp-groupbuy/voucher_records&tab=wp-groupbuy/payment_records&purchase_id=%s">Payments</a>', $item->ID ),
			'purchaser'    => sprintf( '<a href="post.php?post=%s&action=edit">'.wpg__( 'Purchaser' ).'</a>', $account_id ),
		);

		//Return the title contents
		return sprintf( '%1$s <span style="color:silver">(order&nbsp;id:%2$s)</span>%3$s',
			$item->post_title,
			$item->ID,
			$this->row_actions( $actions )
		);
	}

	function column_total( $item ) {
		$purchase = WP_Groupbuy_Purchase::get_instance( $item->ID );
		wg_formatted_money( $purchase->get_total() );
	}

	function column_deals( $item ) {
		$purchase = WP_Groupbuy_Purchase::get_instance( $item->ID );
		$products = $purchase->get_products();

		$i = 0;
                $hover = "";
                $display = "";
		foreach ( $products as $product => $item ) {
			$i++;
			if ( !isset( $item['deal_id'] ) ) {
				continue;
			}
			// Display deal name and link
			$hover .= '<p><strong><a href="'.get_edit_post_link( $item['deal_id'] ).'">'.get_the_title( $item['deal_id'] ).'</a></strong>&nbsp;<span style="color:silver">(id:'.$item['deal_id'].')</span>';
                        $display .= '<p class="title-hover"><strong><a href="'.get_edit_post_link( $item['deal_id'] ).'">'.get_the_title( $item['deal_id'] ).'</a></strong>&nbsp;<span style="color:silver">(id:'.$item['deal_id'].')</span><br/>';
			// Build details
			$details = array(
				__('Quantity') => $item['quantity'],
				__('Unit Price') => wg_get_formatted_money( $item['unit_price'] ),
				__('Total') => wg_get_formatted_money( $item['price'] )
			);
			// Filter to add attributes, etc.
			$details = apply_filters( 'wg_purchase_deal_column_details', $details, $item, $products );
			// display details
			foreach ( $details as $label => $value ) {
				$hover .=  '<br />'.$label.': '.$value;
			}
			// Build Payment methods
			$payment_methods = array();
			if ( !empty( $item['payment_method'] ) ) {
				foreach ( $item['payment_method'] as $method => $payment ) {
					$payment_methods[] .= $method.' &mdash; '.wg_get_formatted_money( $payment );
				}
			}
			$hover .=  '</p>';
			if ( count( $products ) > $i ) {
				$hover .=  '<span class="meta_box_block_divider"></span>';
			}
		}
                //Build row actions
		$actions = array(
			'detail'    => $hover,
		);
		//Return the title contents
		return sprintf( '%1$s %2$s', $display, $this->row_actions( $actions ) );

	}

	function column_payments( $item ) {
		$payments = WP_Groupbuy_Payment::get_payments_for_purchase( $item->ID );
		foreach ( $payments as $payment_id ) {
			$payment = WP_Groupbuy_Payment::get_instance( $payment_id );
			$method = $payment->get_payment_method();
			//Return the title contents
			return sprintf( '<a href="admin.php?page=wp-groupbuy/voucher_records&tab=wp-groupbuy/payment_records&s=%2$s">%1$s</a> <span style="color:silver">(payment&nbsp;id:%2$s)</span>',
				$method,
				$payment_id
			);
		}
	}

	function column_ip_address( $item ) {
		$purchase = WP_Groupbuy_Purchase::get_instance( $item->ID );
		print $purchase->get_user_ip();
	}

	function column_status( $item ) {
		$purchase_id = $item->ID;
		$purchase = WP_Groupbuy_Purchase::get_instance( $item->ID );

		$actions = array(
			'trash'    => '<span id="'.$purchase_id.'_destroy_result"></span><a href="javascript:void(0)" class="wg_destroy" id="'.$purchase_id.'_destroy" ref="'.$purchase_id.'">'.wpg__( 'Delete Purchase' ).'</a>',
		);
		
		$status = '<span class="status">' . ucfirst( str_replace( 'publish', 'complete', $item->post_status ) ) . '</span>';
		$status .= '<br/><span style="color:silver">';
		$status .= mysql2date( get_option( 'date_format' ).' - '.get_option( 'time_format' ), $item->post_date );
		$status .= '</span>';
		$status .= $this->row_actions( $actions );
		return $status;
	}

    function column_customer( $item ) {
        $purchase_id = $item->ID;
        $purchase = WP_Groupbuy_Purchase::get_instance( $item->ID );
        $user_id = $purchase->get_user();
        if($user_id != -1) {
            $account_id = WP_Groupbuy_Account::get_account_id_for_user($user_id);
            $account = WP_Groupbuy_Account::get_instance($user_id);

            $user_fist_last_name = $account->get_name();
            if ($user_fist_last_name != null) {
                //  load email
                $user_email = $account->get_name('email');
                if ($user_email == null) {
                    $wp_user = get_user_by('id', $user_id);
                    if ($wp_user != null) {
                        $user_email = $wp_user->user_email;
                    }
                }
            }
            return '<a href="post.php?post=' . $account_id . '&action=edit">' . $user_fist_last_name . '<br>' . $user_email . '</a>';
        }
        else {
            return 'Unknown person, gift maybe ?';
        }
    }

    function get_columns() {
		$columns = array(
			'status'  => wpg__( 'Status' ),
			'title'  => wpg__( 'Order' ),
			'total'  => wpg__( 'Total' ),
			'deals'  => wpg__( 'Deals' ),
			'customer' => wpg__( 'Customer' ),
			'payments'  => wpg__( 'Payments' ),
			'ip_address'  => wpg__( 'IP Address' )
		);
		return apply_filters( 'wg_mngt_purchases_columns', $columns );
	}

	function get_sortable_columns() {
		$sortable_columns = array(
		);
		return apply_filters( 'wg_mngt_purchases_sortable_columns', $sortable_columns );
	}


	// Bulk actions are an associative array in the format
	function get_bulk_actions() {
		$actions = array();
		return apply_filters( 'wg_mngt_purchases_bulk_actions', $actions );
	}


	// Prep data.
	function prepare_items() {

		// records per page to show
		$per_page = 25;

		// Define our column headers.
		$columns = $this->get_columns();
		$hidden = array();
		$sortable = $this->get_sortable_columns();


		// Build an array to be used by the class for column headers.
		$this->_column_headers = array( $columns, $hidden, $sortable );

		$filter = ( isset( $_REQUEST['post_status'] ) ) ? $_REQUEST['post_status'] : 'all';
		$args=array(
			'post_type' => WP_Groupbuy_Purchase::POST_TYPE,
			'post_status' => $filter,
			'posts_per_page' => $per_page,
			'paged' => $this->get_pagenum()
		);
		// Check purchases based on Deal ID
		if (isset( $_GET['search_type'] ) && $_GET['search_type'] == 'deal_id' && $_GET['search_value'] != '' ) {

			if ( WP_Groupbuy_Deal::POST_TYPE != get_post_type( $_GET['search_value'] ) )
				return; // not a valid search

			$purchase_ids = WP_Groupbuy_Purchase::get_purchases( array( 'deal' => $_GET['search_value'] ) );

			$posts_in = array(
				'post__in' => $purchase_ids
			);
			$args = array_merge( $args, $posts_in );
		}
		// Check payments based on Account ID
		if ( isset( $_GET['search_type'] ) && $_GET['search_type'] == 'account_id' && $_GET['search_value'] != ''  ) {

			if ( WP_Groupbuy_Account::POST_TYPE != get_post_type( $_GET['search_value'] ) )
				return; // not a valid search

			$purchase_ids = WP_Groupbuy_Purchase::get_purchases( array( 'account' => $_GET['search_value'] ) );
			$meta_query = array(
				'post__in' => $purchase_ids,
			);
			$args = array_merge( $args, $meta_query );
			print "args inside account";
			print_r($args);
		}

        // Check purchases based on username or email
        if (isset( $_GET['search_type'] ) && ($_GET['search_type'] == 'email' ||  $_GET['search_type'] == 'user_name' ) && $_GET['search_value'] != '' ) {
            if($_GET['search_type'] == 'email') {
                $user = get_user_by( "email", $_GET['search_value']);
                if($user != null) {
                    $account_id = WP_Groupbuy_Account::get_account_id_for_user( $user->ID );
                    $purchase_ids = WP_Groupbuy_Purchase::get_purchases(array('account' => $account_id));
                    if(empty($purchase_ids)) {
                        return;
                    }
                }
                else {
                    return;
                }
            }
            else {
		        if($_GET['search_type'] == 'user_name') {
                    $user_fist_name = "";
                    $user_last_name = "";
                    if(strpos($_GET['search_value'], ' ' ) !== false) {
                        $user_names = explode(" ", $_GET['search_value']);
                        if(count($user_names) > 0) {
                            $user_fist_name = $user_names[0];
                            $user_last_name= $user_names[1];
                        }
                    }
                    else {
                        $user_fist_name = $_GET['search_value'];
                    }

                    $account = WP_Groupbuy_Account::get_account_id_for_user_name($user_fist_name, $user_last_name);
                    if($account == 0) {
                        $user_fist_name = "";
                        $user_last_name = $_GET['search_value'];
                        $account = WP_Groupbuy_Account::get_account_id_for_user_name($user_fist_name, $user_last_name);
                    }

                    $purchase_ids = array();

                    foreach ($account as $key => $value) {
                        $purchase_ids = array_merge($purchase_ids, WP_Groupbuy_Purchase::get_purchases(array('account' => $value)));
                    }

                    if(empty($purchase_ids)) {
                        return;
                    }
		        }

            }


            $posts_in = array(
                'post__in' => $purchase_ids
            );
            $args = array_merge( $args, $posts_in );
        }

		// Search
		if ( isset( $_GET['search_type'] ) && $_GET['search_type'] == 's' && $_GET['search_value'] != '' ) {
			$args = array_merge( $args, array( 's' => $_GET['search_value'] ) );
		}
		// Filter by date
		if ( isset( $_GET['m'] ) && $_GET['m'] != '' ) {
			$args = array_merge( $args, array( 'm' => $_GET['m'] ) );
		}
		$purchases = new WP_Query( $args );

		// Sorted data to the items property, where it can be used by the rest of the class.
		$this->items = apply_filters( 'wg_mngt_purchases_items', $purchases->posts );

		// Register our pagination options & calculations.
		$this->set_pagination_args( array(
				'total_items' => $purchases->found_posts,
				'per_page'  => $per_page,
				'total_pages' => $purchases->max_num_pages
			) );
	}
}
