<?php
add_action( 'wg_init_controllers', array( 'WP_Groupbuy_MerchantVouchersPaymentsReport', 'init' ), 55 );

class WP_Groupbuy_MerchantVouchersPaymentsReport extends WP_Groupbuy_Controller
{

    const MERCHANT_VOUCHERS_PAYMENTS_REPORT_PATH_OPTION = 'wg_merchant_vouchers_payments_report_path';
    const MERCHANT_VOUCHERS_PAYMENTS_REPORT_QUERY_VAR = 'wg_merchant_vouchers_payments_report';
    const MERCHANT_VOUCHERS_PAYMENTS_REPORT_BASE = 'merchant_vouchers_payments_report';
    private static $merchant_vouchers_payments_report = self::MERCHANT_VOUCHERS_PAYMENTS_REPORT_BASE;
    protected static $settings_page;
    private static $instance;

    public static function init()
    {
        if (!empty($_POST['mytheme_export_csv'])) {
            //print " inside export operation";

            header("Content-type: application/force-download");
            header('Content-Disposition: inline; filename="merchant_voucher_payments.csv"');

            //$wp_list_table = new WP_Groupbuy_MerchantVouchersPaymentsReport_Table();

            /*
            $wp_list_table = new WP_Groupbuy_MerchantVouchersPaymentsReport_Table();
            //Fetch, prepare, sort, and filter our data...
            $wp_list_table->prepare_items();

            echo $wp_list_table->display();
            */
            $args = array(
                'post_type' => WP_Groupbuy_Voucher::POST_TYPE,
            );

            if(isset($_GET['s']) && $_GET['s'] != '') {
                $merchant_id = $_GET['s'];
                $merchant_deal_ids = WP_Groupbuy_Deal::get_deals_by_merchant($merchant_id);
                $meta_query = array(
                    'meta_query' => array(
                        array(
                            'key' => '_voucher_deal_id',
                            'value' => $merchant_deal_ids,
                            'type' => 'numeric',
                            'compare' => 'IN'
                        )
                    )
                );
                $args = array_merge( $args, $meta_query );
            }

            $merchantVourchers = new WP_Query($args);
/*
            print_r($merchantVourchers->posts);
            exit();
*/
            $csv_headers = "Voucher ID, Deal ID, Customer, Voucher exp. date, Coupon value, Deal provision, Payment date, Claim date, Payment method, Voucher status \r\n";
            echo $csv_headers;
            foreach ( $merchantVourchers->posts as $merchantVoucher) {
                $voucher_id = $merchantVoucher->ID;

                $voucher = WP_Groupbuy_Voucher::get_instance($voucher_id);

                $deal_id = $voucher->get_post_meta('_voucher_deal_id');
                //$customer = $wp_list_table->column_customer($voucher_id);
                $customer = "";
                $purchase = $voucher->get_purchase();
                $purchase_id = $voucher->get_purchase_id();
                $gift_id = WP_Groupbuy_Gift::get_gift_for_purchase($purchase_id);
                $account_id = $purchase->get_account_id();
                $account = WP_Groupbuy_Account::get_instance_by_id($account_id);
                $returnValue = $account_id . " - " . $account->get_name();
                if ($gift_id > 0) {
                    $gift = WP_Groupbuy_Gift::get_instance($gift_id);
                    $customer = $returnValue . " (" . $gift->get_recipient() . ")";
                }

                $customer = $returnValue;

                $voucher_date = date('d.m.Y H:i:s', $voucher->get_expiration_date());

                $deal = WP_Groupbuy_Deal::get_instance($deal_id);
                $deal_value = 0;
                $deal_coupon_value = $deal->get_coupon_value();
                if (count($deal_coupon_value) == 1) {
                    if (!empty($deal_coupon_value[0])) {
                        $deal_value = $deal_coupon_value[0] . " " . wg_get_currency_symbol();
                    }
                } else {
                    if (!empty($deal_coupon_value)) {
                        $deal_value = $deal_coupon_value . " " . wg_get_currency_symbol();
                    }
                }

                $deal_provision = number_format((float)($deal->get_provision_value()/100) * $deal_value, 2, '.', '.') . " " . wg_get_currency_symbol();
                //return number_format((float)($deal->get_provision_value()/100) * $coupon_value, 2, '.', '.') . " " . wg_get_currency_symbol();


                $payment_date =  mysql2date( get_option( 'date_format' ).' - '.get_option( 'time_format' ), $merchantVoucher->post_date );

                $claim_date_return = "";
                $claim_date = $voucher->get_claimed_date();
                if($claim_date) {
                    $claim_date_return = date_i18n('d.m.Y H:i:s', $claim_date);
                } else {
                    $claim_date_return = "Not yet claimed.";
                }

                $payment_method = "";
                if(count($voucher->get_product_data()['payment_method']) >= 1) {
                    $payment_method = key($voucher->get_product_data()['payment_method']);
                }

                $payment_status = ucfirst( str_replace( 'publish', 'active', $merchantVoucher->post_status ) );

                echo $voucher_id .", ". $deal_id .", ". $customer .", ". $voucher_date .", ". $deal_value .", ". $deal_provision . ", ". $payment_date . ", ". $claim_date_return . ", " . $payment_method . ", " . $payment_status . "\r\n";
            }
            exit();
        }
        self::$settings_page = self::register_settings_page(self::MERCHANT_VOUCHERS_PAYMENTS_REPORT_BASE, self::__('Merchant vouchers payment report'), self::__('Merchant voucher payment reports'), 11, FALSE, 'records', array(get_class(), "display_table"));
        self::$merchant_vouchers_payments_report = get_option(self::MERCHANT_VOUCHERS_PAYMENTS_REPORT_PATH_OPTION, self::$merchant_vouchers_payments_report);
        self::register_path_callback(self::$merchant_vouchers_payments_report, array(get_class(), 'on_payment_importer_page'), self::MERCHANT_VOUCHERS_PAYMENTS_REPORT_QUERY_VAR, self::MERCHANT_VOUCHERS_PAYMENTS_REPORT_BASE);
    }

    public function display_table()
    {

        //Create an instance of our package class...
        $wp_list_table = new WP_Groupbuy_MerchantVouchersPaymentsReport_Table();
        //Fetch, prepare, sort, and filter our data...
        $wp_list_table->prepare_items();

        ?>


        <style type="text/css">
            #payment_deal_id-search-input, #purchase_id-search-input, #payment_account_id-search-input {
                width: 5em;
                margin-left: 10px;
                float: left;
            }
        </style>
        <div class="wrap">
            <?php screen_icon(); ?>
            <h2 class="nav-tab-wrapper">
                <?php self::display_admin_tabs(); ?>
            </h2>

            <?php $wp_list_table->views() ?>
            <form id="payments-filter" method="get" style="margin-top:5px;">
                <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>"/>
                <input type="hidden" name="tab" value="<?php echo $_REQUEST['tab'] ?>"/>
                <?php $wp_list_table->display() ?>
            </form>
            <script type="text/javascript">
                jQuery(document).ready( function($)
                {
                    $('.tablenav.top .clear, .tablenav.bottom .clear').before('<form action="#" method="POST"><input type="hidden" id="mytheme_export_csv" name="mytheme_export_csv" value="1" /><input class="button button-primary user_export_button" style="margin-top:3px;" type="submit" value="<?php esc_attr_e('Export All as CSV', 'mytheme');?>" /></form>');
                });
            </script>
        </div>
        <?php
        add_action('admin_init', 'export_csv'); //you can use admin_init as well

    }
    function export_csv()
    {
        if (!empty($_POST['mytheme_export_csv'])) {
            print " inside export operation";
            break;
        }
    }

}

if ( !class_exists( 'WP_List_Table' ) ) {
    require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
}
class WP_Groupbuy_MerchantVouchersPaymentsReport_Table extends WP_List_Table
{

    function get_bulk_actions() {
        $actions = array(
            'delete' => __( 'Delete' , 'visual-form-builder'),
            'export-all' => __( 'Export All' , 'visual-form-builder'),
            'export-selected' => __( 'Export Selected' , 'visual-form-builder')
        );

        return $actions;
    }
    function process_bulk_action()
    {
        $entry_id = (is_array($_REQUEST['entry'])) ? $_REQUEST['entry'] : array($_REQUEST['entry']);

        if ('delete' === $this->current_action()) {
            global $wpdb;

            foreach ($entry_id as $id) {
                $id = absint($id);
                $wpdb->query("DELETE FROM $this->entries_table_name WHERE entries_id = $id");
            }
        }
    }

    protected static $post_type = WP_Groupbuy_Gift::POST_TYPE;

    function __construct()
    {
        global $status, $page;

        parent::__construct(array(
            'singular' => 'voucher',
            'plural' => 'vouchers',
            'ajax' => false
        ));

    }

    function get_views()
    {

        $status_links = array();
        $num_posts = wp_count_posts(self::$post_type, 'readable');
        $class = '';
        $allposts = '';

        $total_posts = array_sum((array)$num_posts);

        // Subtract post types that are not included in the admin all list.
        foreach (get_post_stati(array('show_in_admin_all_list' => false)) as $state)
            $total_posts -= $num_posts->$state;

        $class = empty($_REQUEST['post_status']) ? ' class="current"' : '';
        $status_links['all'] = "<a href='admin.php?page=wp-groupbuy/voucher_records&tab=wp-groupbuy/payment_records{$allposts}'$class>" . sprintf(_nx('All <span class="count">(%s)</span>', 'All <span class="count">(%s)</span>', $total_posts, 'posts'), number_format_i18n($total_posts)) . '</a>';

        foreach (get_post_stati(array('show_in_admin_status_list' => true), 'objects') as $status) {
            $class = '';

            $status_name = $status->name;

            if (empty($num_posts->$status_name))
                continue;

            if (isset($_REQUEST['post_status']) && $status_name == $_REQUEST['post_status'])
                $class = ' class="current"';

            // replace "Published" with "Complete".
            $label = wpg__('Payment&nbsp;') . str_replace('Published', 'Complete', translate_nooped_plural($status->label_count, $num_posts->$status_name));
            $status_links[$status_name] = "<a href='admin.php?page=wp-groupbuy/voucher_records&tab=wp-groupbuy/gift_records&post_status=$status_name'$class>" . sprintf($label, number_format_i18n($num_posts->$status_name)) . '</a>';
        }

        return $status_links;
    }

    function extra_tablenav($which)
    {
        ?>
        <div class="alignleft actions">
            <?php
            if ('top' == $which && !is_singular()) {

                $this->months_dropdown(self::$post_type);

                submit_button(__('Filter'), 'secondary', false, false, array('id' => 'post-query-submit'));
            }
            ?>
        </div>
        <?php
    }


    // Text or HTML to be placed inside the column <td>
    function column_default($item, $column_name)
    {
        switch ($column_name) {
            default:
                return apply_filters('wg_mngt_voucher_report_column_' . $column_name, $item);
        }
    }


    function column_cb($item)
    {
        //  parent::column_cb($item); // TODO: Change the autogenerated stub
        $checkbox ='<input type="checkbox" name="extra" />';
        return  $checkbox;
    }

    // Text to be placed inside the column <td> (movie title only)
    function column_voucher_id($item)
    {
        //print "item: ";
        //print_r($item);
        //break;
        //$this->display_table1();
        return "$item->ID";

        $gift = WP_Groupbuy_Gift::get_instance($item->ID);
        $purchase_id = $gift->get_purchase_id();
        $purchase = WP_Groupbuy_Purchase::get_instance($purchase_id);
        if ($purchase) {
            $user_id = $purchase->get_original_user();
        } else {
            $user_id = 0;
        }
        $account_id = WP_Groupbuy_Account::get_account_id_for_user($user_id);

        //Build row actions
        $actions = array(
            'order' => sprintf('<a href="admin.php?page=wp-groupbuy/voucher_records&tab=wp-groupbuy/purchase_records&s=%s">Order</a>', $purchase_id),
            'purchaser' => sprintf('<a href="post.php?post=%s&action=edit">' . wpg__('Purchaser') . '</a>', $account_id),
        );

        //Return the title contents
        return sprintf('%1$s <span style="color:silver">(order&nbsp;id:%2$s)</span>%3$s',
            $item->post_title,
            $purchase_id,
            $this->row_actions($actions)
        );
    }

    function column_deal_id($item)
    {
        //  return "$item->post_title";


        $voucher = WP_Groupbuy_Voucher::get_instance($item->ID);
        //  $purchase_id = $gift->get_purchase_id();
        //  $purchase = WP_Groupbuy_Purchase::get_instance($purchase_id);
    /*
        if ($purchase) {
            wg_formatted_money($purchase->get_total());
        } else {
            wg_formatted_money(0);
        }
    */
        //$dealObject = $deal->get_deal();
        $deal_id = $voucher->get_post_meta( '_voucher_deal_id' );
        return $deal_id;
        //return $deal->get_deal()->get_id();
    }

    function column_customer($item) {
        $voucher = WP_Groupbuy_Voucher::get_instance($item->ID);
        $purchase = $voucher->get_purchase();
        $purchase_id = $voucher->get_purchase_id();
        $gift_id = WP_Groupbuy_Gift::get_gift_for_purchase($purchase_id);
        $account_id = $purchase->get_account_id();
        $account = WP_Groupbuy_Account::get_instance_by_id($account_id);
        $returnValue = $account_id . " - ".$account->get_name();
        if($gift_id > 0) {
            $gift = WP_Groupbuy_Gift::get_instance( $gift_id );
            return $returnValue. " (". $gift->get_recipient() .")";
        }
        return $returnValue;
    }

    function column_voucher_date($item) {
        $voucher = WP_Groupbuy_Voucher::get_instance($item->ID);
        return date('d.m.Y H:i:s', $voucher->get_expiration_date());
    }

    function column_deal_coupon_value($item) {
        $voucher = WP_Groupbuy_Voucher::get_instance($item->ID);
        $deal_id = $voucher->get_post_meta( '_voucher_deal_id' );
        $deal = WP_Groupbuy_Deal::get_instance($deal_id);
        $deal_coupon_value = $deal->get_coupon_value();
        if(count($deal_coupon_value) == 1) {
            if(!empty($deal_coupon_value[0])) {
                return $deal_coupon_value[0]." ".wg_get_currency_symbol();
            }
        }
        if(!empty($deal_coupon_value)) {
            return $deal_coupon_value . " " . wg_get_currency_symbol();
        }
    }

    function column_deal_provision($item){
        $voucher = WP_Groupbuy_Voucher::get_instance($item->ID);
        $deal_id = $voucher->get_post_meta( '_voucher_deal_id' );
        $deal = WP_Groupbuy_Deal::get_instance($deal_id);
        $deal_coupon_value = $deal->get_coupon_value();
        $coupon_value = 0;
        if(count($deal_coupon_value) == 1) {
            $coupon_value = $deal_coupon_value[0];
        }
        return number_format((float)($deal->get_provision_value()/100) * $coupon_value, 2, '.', '.') . " " . wg_get_currency_symbol();
        //return ($deal->get_provision_value()/100) * coupon_value();
        //return "";
    }

    function column_payment_date($item)
    {
        //return "$item->post_title";
/*
        $gift = WP_Groupbuy_Gift::get_instance($item->ID);
        $purchase_id = $gift->get_purchase_id();
        $purchase = WP_Groupbuy_Purchase::get_instance($purchase_id);
        $products = $purchase ? $purchase->get_products() : array();

        $i = 0;
        $hover = "";
        $display = "";
        foreach ($products as $product => $item) {
            $i++;
            // Display deal name and link
            $hover .= '<p><strong><a href="' . get_edit_post_link($item['deal_id']) . '">' . get_the_title($item['deal_id']) . '</a></strong>&nbsp;<span style="color:silver">(id:' . $item['deal_id'] . ')</span>';
            $display .= '<p class="title-hover"><strong><a href="' . get_edit_post_link($item['deal_id']) . '">' . get_the_title($item['deal_id']) . '</a></strong>&nbsp;<span style="color:silver">(id:' . $item['deal_id'] . ')</span><br/>';
            // Build details
            $details = array(
                'Quantity' => $item['quantity'],
                'Unit Price' => wg_get_formatted_money($item['unit_price']),
                'Total' => wg_get_formatted_money($item['price'])
            );
            // Filter to add attributes, etc.
            $details = apply_filters('wg_purchase_deal_column_details', $details, $item, $products);
            // display details
            foreach ($details as $label => $value) {
                $hover .= '<br />' . $label . ': ' . $value;
            }
            // Build Payment methods
            $payment_methods = array();
            foreach ($item['payment_method'] as $method => $payment) {
                $payment_methods[] .= $method . ' &mdash; ' . wg_get_formatted_money($payment);
            }
            echo '</p>';
            if (count($products) > $i) {
                $hover .= '<span class="meta_box_block_divider"></span>';
            }
        }
        //Build row actions
        $actions = array(
            'detail' => $hover,
        );
        //Return the title contents
        return sprintf('%1$s %2$s', $display, $this->row_actions($actions));
*/

        return mysql2date( get_option( 'date_format' ).' - '.get_option( 'time_format' ), $item->post_date );
        /*
        $voucher = WP_Groupbuy_Voucher::get_instance($item->ID);
        //  print_r($voucher->get_deal());
        $deal = $voucher->get_deal();
        //print_r($deal);
        //print($deal->get_the_post_author());
        $deal_id = $voucher->get_post_meta( '_voucher_deal_id' );
        //print($deal_id);
        return get_the_modified_date("d.m.Y H:i:s", $deal_id);
        //return $deal->post_modified();
        */
    }

    function column_claim_date($item) {
        //return "aaaa";
        $voucher = WP_Groupbuy_Voucher::get_instance($item->ID);

        $claim_date = $voucher->get_claimed_date();
        if($claim_date) {
            return date_i18n('d.m.Y H:i:s', $claim_date);
        } else {
            return "Not yet claimed.";
        }
    }

    function column_payment_method($item)
    {
        $voucher = WP_Groupbuy_Voucher::get_instance($item->ID);

        if(count($voucher->get_product_data()['payment_method']) >= 1) {
            return key($voucher->get_product_data()['payment_method']);
        }
/*
        print_r(key());

        return $voucher['payment_method'];
        return "$item->post_title";

        $gift_id = $item->ID;
        $gift = WP_Groupbuy_Gift::get_instance($gift_id);
        $purchase_id = $gift->get_purchase_id();
        $purchase = WP_Groupbuy_Purchase::get_instance($purchase_id);
        echo '<p>';
        echo '<strong>' . wpg__('Recipient:') . '</strong> <span class="editable_string wg_highlight" ref="' . $gift_id . '">' . $gift->get_recipient() . '</span><input type="text" id="' . $gift_id . '_recipient_input" class="option_recipient cloak" value="' . $gift->get_recipient() . '" />';
        echo '<br/><span style="color:silver">' . wpg__('code') . ': ' . $gift->get_coupon_code() . '</span>';
        echo '</p>';
        echo '<p><span id="' . $gift_id . '_activate_result"></span><a href="/wp-admin/edit.php?post_type=wg_purchase&resend_gift=' . $gift_id . '&_wpnonce=' . wp_create_nonce('resend_gift') . '" class="wg_resend_gift button" id="' . $gift_id . '_activate" ref="' . $gift_id . '">Resend</a></p>';
        return;
*/
    }

    function column_payment_status($item)
    {
        return ucfirst( str_replace( 'publish', 'active', $item->post_status ) );
        /*
        return "$item->post_title";


        $gift = WP_Groupbuy_Gift::get_instance($item->ID);

        $claimed = $gift->get_claimed();

        if ($claimed) {
            $status = '<strong>' . wpg__('Complete') . '</strong><br/>';
        } else {
            $status = '<strong>' . wpg__('Pending Claim') . '</strong><br/>';
        }

        $date = $claimed ?: $item->post_date;

        $status .= '<span style="color:silver">';
        $status .= mysql2date(get_option('date_format') . ' - ' . get_option('time_format'), $date);
        $status .= '</span>';

        return $status;
        */
    }

    function column_manage($item)
    {
        return "Set payed";
    }

    function get_columns()
    {
        $columns = array(
            'cb' => '&lt;input type="checkbox" />',
            'voucher_id' => wpg__('Voucher ID'),
            'deal_id' => wpg__('Deal ID'),
            'customer' => wpg__('Customer'),
            'voucher_date' => wpg__('Voucher exp. date'),
            //  deal's coupon value
            'deal_coupon_value' => wpg__('Coupon value'),
            //  provision
            'deal_provision' => wpg__('Deal provision'),
            'payment_date' => wpg__('Payment date'),
            'claim_date' => wpg__('Claim date'),
            'payment_method' => wpg__('Payment method'),
            'payment_status' => wpg__('Voucher status'),
            //  TODO: export status -> oznaci da je ze placan na knof

            'manage' => wpg__('Manage')
        );
        return apply_filters('wg_mngt_voucher_reports_columns', $columns);
    }

    function get_sortable_columns()
    {
        $sortable_columns = array();
        return apply_filters('wg_mngt_voucher_reports_sortable_columns', $sortable_columns);
    }
/*
    function get_bulk_actions()
    {
        $actions = array();
        return apply_filters('wg_mngt_voucher_reports_bulk_actions', $actions);
    }
*/

    // Prep data.
    function prepare_items()
    {
        // records per page to show
        $per_page = 25;

        // Define our column headers.
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();

        // Build an array to be used by the class for column headers.
        $this->_column_headers = array($columns, $hidden, $sortable);
        $filter = (isset($_REQUEST['post_status'])) ? $_REQUEST['post_status'] : 'all';
        $args = array(
        'post_type' => WP_Groupbuy_Voucher::POST_TYPE,
        'posts_per_page' => $per_page,
        'paged' => $this->get_pagenum()
    );
        /*
        // Check purchases based on Deal ID
        if (isset($_GET['search_type']) && $_GET['search_type'] == 'deal_id' && $_GET['search_value'] != '') {

            if (WP_Groupbuy_Deal::POST_TYPE != get_post_type($_GET['search_value']))
                return; // not a valid search

            $purchase_ids = WP_Groupbuy_Purchase::get_purchases(array('deal' => $_GET['search_value']));

            $meta_query = array(
                'meta_query' => array(
                    array(
                        'key' => '_purchase',
                        'value' => $purchase_ids,
                        'type' => 'numeric',
                        'compare' => 'IN'
                    )
                ));
            $args = array_merge($args, $meta_query);
        }*/
        /*
        // Check payments based on Account ID
        if (isset($_GET['search_type']) && $_GET['search_type'] == 'account_id' && $_GET['search_value'] != '') {

            // if ( WP_Groupbuy_Account::POST_TYPE != get_post_type( $_GET['search_value'] ) )
            // 	return; // not a valid search

            // $purchase_ids = WP_Groupbuy_Purchase::get_purchases( array( 'account' => $_GET['search_value'] ) );

            // $meta_query = array(
            // 	'meta_query' => array(
            // 		array(
            // 			'key' => '_purchase',
            // 			'value' => $purchase_ids,
            // 			'type' => 'numeric',
            // 			'compare' => 'IN'
            // 		)
            // 	) );
            // $args = array_merge( $args, $meta_query );

            $author_id = WP_Groupbuy_Account::get_user_id_for_account($_GET['search_value']);
            $args = array_merge($args, array('author' => $author_id));

        }
        */
        //if ( WP_Groupbuy_Merchant::POST_TYPE != get_post_type( $_GET['search_value'] ) )
        // 	return; // not a valid search
/*
        // Search
        if (isset($_GET['search_type']) && $_GET['search_type'] == 's' && $_GET['search_value'] != '') {
            $args = array_merge($args, array('s' => $_GET['search_value']));
        }
        // Filter by date
        if (isset($_GET['m']) && $_GET['m'] != '') {
            $args = array_merge($args, array('m' => $_GET['m']));
        }
        //print "args: ";
        //print_r($args);
        //break;
*/

        //  TODO: load vouchers per selected merchant - merchant is author of deal -> get deal ids for specific merchant, and load vouchers from those deals.

        if(isset($_GET['s']) && $_GET['s'] != '') {
            $merchant_id = $_GET['s'];
            $merchant_deal_ids = WP_Groupbuy_Deal::get_deals_by_merchant($merchant_id);
            $meta_query = array(
                'meta_query' => array(
                    array(
                 		'key' => '_voucher_deal_id',
                        'value' => $merchant_deal_ids,
                        'type' => 'numeric',
                        'compare' => 'IN'
                    )
                )
            );
            $args = array_merge( $args, $meta_query );
        }

        $merchantVourchers = new WP_Query($args);

        //  load all payments for each

        //print_r($voucherPayments);

        // Sorted data to the items property, where it can be used by the rest of the class.
        $this->items = apply_filters('wg_mngt_voucher_reports_items', $merchantVourchers->posts);

        $this->set_pagination_args(array(
            'total_items' => $merchantVourchers->found_posts,
            'per_page' => $per_page,
            'total_pages' => $merchantVourchers->max_num_pages
        ));
        $this->process_bulk_action();

    }
}
?>