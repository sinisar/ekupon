<?php

add_action( 'wg_init_controllers', array( 'WP_Groupbuy_Registration_Captcha', 'init' ), 150 );

class WP_Groupbuy_Registration_Captcha extends WP_Groupbuy_Controller {

	public static function init() {
		// Hook this plugin into the WPG add-ons controller
		add_filter( 'wg_addons', array( get_class(), 'wg_addon' ), 10, 1 );
	}

	public static function wg_addon( $addons ) {
		$addons['registration_captcha'] = array(
			'label' => self::__( 'Captcha for registration page' ),
			'description' => self::__( 'Add Captcha to the Registration page.' ),
			'files' => array(
				__FILE__
			),
			'callbacks' => array(
				array( 'Registration_Captcha', 'init' ),
			),
		);
		return $addons;
	}
}

class Registration_Captcha extends WP_Groupbuy_Controller {

	const SOURCE = 'wg_account_fields_source';
	const RECAPTCHA_OPTION = 'wg_recaptcha_key';
	const RECAPTCHA_PRIVATE_OPTION = 'wg_recaptcha_private_key';
	const RECAPTCHA_ONPASS_RESET_OPTION = 'wg_recaptcha_on_pr';
	private static $public_key;
	private static $secret_key;
	private static $add_to_reset_password_page = TRUE;


	public static function init() {

		self::$public_key = get_option( self::RECAPTCHA_OPTION, '' );
		self::$secret_key = get_option( self::RECAPTCHA_PRIVATE_OPTION, '' );
		self::$add_to_reset_password_page = get_option( self::RECAPTCHA_ONPASS_RESET_OPTION, 1 );

		// registration hooks
		add_filter( 'wg_account_registration_panes', array( get_class(), 'get_registration_panes' ), 100 );
		add_filter( 'wg_validate_account_registration', array( get_class(), 'validate_account_fields' ), 10, 4 );

		if ( self::$add_to_reset_password_page ) {
			// registration hooks
			add_action( 'lostpassword_form', array( get_class(), 'lostpassword_form_captcha' ) );
			add_action( 'parse_request', array( get_class(), 'validate_for_password_reset' ), 0 );
			add_filter( 'wg_validate_password_reset', array( get_class(), 'validate_account_fields' ), 10, 4 );
		}
		// Options
		add_action( 'admin_init', array( get_class(), 'register_settings_fields' ), 10, 0 );

	}

	/**
	 * Add the default pane to the account edit form
	 */
	public function get_registration_panes( array $panes ) {

		$recaptcha_html = self::recaptcha_get_html();
		// $recaptcha_field_html = self::load_view_to_string( 'account/register-recaptcha', array( 'recaptcha_html' => $recaptcha_html ) );

		$panes['captcha'] = array(
			'weight' => 100,
			'body' => $recaptcha_html,
		);

		return $panes;
	}

	public function lostpassword_form_captcha() {
		echo '<tr><td>' . self::recaptcha_get_html() . '</td></tr>';
	}

	public static function recaptcha_get_html ()
	{
		if ( !self::$public_key ) {
			$recaptcha_html = "To use reCAPTCHA you must get an API key from <a href='https://www.google.com/recaptcha/admin/create'>https://www.google.com/recaptcha/admin/create</a>";
		} else {		
			$recaptcha_html = '<div class="g-recaptcha" data-sitekey="'.self::$public_key.'"></div><script src="https://www.google.com/recaptcha/api.js"></script>';
		}

		return $recaptcha_html;
	}

	public static function recaptcha_verification() {

	  $response = isset( $_POST['g-recaptcha-response'] ) ? esc_attr( $_POST['g-recaptcha-response'] ) : '';

	  $remote_ip = $_SERVER["REMOTE_ADDR"];

	  // make a GET request to the Google reCAPTCHA Server
	  $request = wp_remote_get(
	    'https://www.google.com/recaptcha/api/siteverify?secret=' . self::$secret_key . '&response=' . $response . '&remoteip=' . $remote_ip
	  );

	  // get the request response body
	  $response_body = wp_remote_retrieve_body( $request );

	  $result = json_decode( $response_body, true );

	  return $result['success'];
	}

	/**
	 * Validate the form submitted
	 */
	public function validate_account_fields( $errors, $username, $email_address, $post ) {
		$success = self::recaptcha_verification();
		if ( !$success ) {
			$errors[] = 'The reCAPTCHA wasn’t entered correctly. Go back and try it again.';
			return $errors;
		}
	}

	public static function validate_for_password_reset() {
		if ( isset( $_POST['user_login'] ) && !empty( $_POST['user_login'] ) ) {
			$success = self::recaptcha_verification();

			if ( !$success ) {
				wg_set_message( wg__('The reCAPTCHA wasn’t entered correctly. Try again.'), 'error' );
				wp_redirect( add_query_arg( array( 'message' => 'captcha_error' ), WP_Groupbuy_Accounts_Retrieve_Password::get_url() ) );
				exit();
			}
		}
	}

	public static function register_settings_fields() {
		$page = WP_Groupbuy_UI::get_settings_page();
		$section = 'wg_basic_rewards_settings';
		add_settings_section( $section, self::__( 'reCaptcha Settings' ), array( get_class(), 'display_settings_section' ), $page );
		// Settings
		register_setting( $page, self::RECAPTCHA_OPTION );
		register_setting( $page, self::RECAPTCHA_PRIVATE_OPTION );
		register_setting( $page, self::RECAPTCHA_ONPASS_RESET_OPTION );
		// Fields
		add_settings_field( self::RECAPTCHA_OPTION, self::__( 'Public Key' ), array( get_class(), 'display_key' ), $page, $section );
		add_settings_field( self::RECAPTCHA_PRIVATE_OPTION, self::__( 'Private Key' ), array( get_class(), 'display_private_key' ), $page, $section );
		add_settings_field( self::RECAPTCHA_ONPASS_RESET_OPTION, self::__( 'Show on reset password page' ), array( get_class(), 'display_on_pr' ), $page, $section );
	}
	public function display_settings_section() {
		printf( self::__( '<a href="%s" target="_blank">Signup</a> and create your public and private keys.' ), 'https://www.google.com/recaptcha/intro/index.html' );
	}

	public static function display_key() {
		echo '<input type="text" name="'.self::RECAPTCHA_OPTION.'" value="'.self::$public_key.'" />';
	}

	public static function display_private_key() {
		echo '<input type="text" name="'.self::RECAPTCHA_PRIVATE_OPTION.'" value="'.self::$secret_key.'" />';
	}

	public static function display_on_pr() {
		echo '<select name="'.self::RECAPTCHA_ONPASS_RESET_OPTION.'"><option value="1" '.selected( self::$add_to_reset_password_page, '1', FALSE ).'>Yes</option><option value="0" '.selected( self::$add_to_reset_password_page, '0', FALSE ).'>No</option></select>';
	}

}
