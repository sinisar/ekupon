<?php

add_action( 'wg_init_controllers', array( 'WP_Groupbuy_Help', 'init' ), 115 );

class WP_Groupbuy_Help extends WP_Groupbuy_Controller {

	public static function init() {

		add_action( 'wg_settings_page_sub_heading_wp-groupbuy', array( get_class(), 'display_dashboard_section' ), 10, 0 );

		// Menus and help
		if ( version_compare( get_bloginfo( 'version' ), '3.3', '>=' ) ) {
			add_action( 'admin_bar_menu', array( get_class(), 'wp_admin_bar_options' ), 62 );
			add_action( 'admin_menu', array( get_class(), 'admin_menu' ) );
		}
	}

	public function display_dashboard_section() {
		if ( $_GET['page'] == self::TEXT_DOMAIN ) {
			print self::load_view( 'dashboard/dashboard.php', array() );
		}
	}

	public static function admin_menu() {
		// Mixed
		add_action( 'load-edit.php', array( get_class(), 'help_section' ) );
		add_action( 'load-post.php', array( get_class(), 'help_section' ) );
		add_action( 'load-post-new.php', array( get_class(), 'help_section' ) );

		// Edit screen
		add_action( 'load-edit.php', array( get_class(), 'help_section_edit' ) );

		// Edit or add a deal
		add_action( 'load-post.php', array( get_class(), 'help_section_edit_post' ) );
		add_action( 'load-post-new.php', array( get_class(), 'help_section_edit_post' ) );

		// Option Pages
		/*foreach ( self::get_admin_pages() as $page => $data ) {
			add_action( 'load-'.$data['hook'], array( get_class(), 'options_help_section' ), 50 );
		}*/
	}

	public static function options_help_section() {
		$screen = get_current_screen();
		$page = str_replace( 'wp-groupbuy_page_wp-groupbuy/', '', $screen->id );

		$screen->add_help_tab( array(
				'id'      => 'general-options-questions',
				'title'   => self::__( 'Question about WPG' ),
				'content' =>
				'<p><strong>' . self::__( 'Do you have a question about WPGroupbuy Theme & Plugin?' ) . '</strong></p>' .
				'<p>' . sprintf( self::__( 'Visit our <a href="%s">support page</a> to find an answer if you get any trouble in using Theme & Plugin.' ), 'http://wpgroupbuy.com/support/' ) . '</p>'.
				'<p>' . sprintf( self::__( 'If you have any question, you can <a href="%s">contact us</a> by email.' ), 'http://wpgroupbuy.com/contact/' ) . '</p>'
			) );
		$screen->add_help_tab( array(
				'id'      => 'general-options-critical', 
				'title'   => self::__( 'Getting Error' ),
				'content' =>
				'<p><strong>' . self::__( 'Are you facing a new error that WPG don\'t know?' ) . '</strong></p>' .
				'<p>' . sprintf( self::__( 'Please <a href="%s" target="_blank">send us</a> more information about error, bug you got. Describe how you get this troube?' ), 'http://wpgroupbuy.com/contact/' ) . '</p>'
			) );
		$screen->add_help_tab( array(
				'id'      => 'general-options-customizations', 
				'title'   => self::__( 'Customizations' ),
				'content' =>
				'<p><strong>' . self::__( 'Do you want special features or change your site?' ) . '</strong></p>' .
				'<p>' . sprintf( self::__( 'You should never edit WPG plugins or themes directly.' ) ) . '</p>'.
				'<p>' . sprintf( self::__( 'WPG Team provides custom development services for you. <a href="%s" target="_blank">Contact us</a> and send all your request.' ), 'http://wpgroupbuy.com/contact/' ) . '</p>'
			) );
		$screen->set_help_sidebar(
			'<p><strong>' . __( 'For more information:' ) . '</strong></p>' .
			'<p>' . self::__( '<a href="http://wpgroupbuy.com/support/documents/" target="_blank">Documentation</a>' ) . '</p>' .
			'<p>' . self::__( '<a href="http://wpgroupbuy.com/contact/" target="_blank">Contact WPG</a>' ) . '</p>'
		);
	}

	public static function help_section() {
		$screen = get_current_screen();
		$post_id = isset( $_GET['post'] ) ? (int)$_GET['post'] : FALSE;
		if ( $post_id ) {
			$post_type = get_post_type( $post_id );
		} else {
			$post_type = ( isset( $_REQUEST['post_type'] ) && post_type_exists( $_REQUEST['post_type'] ) ) ? $_REQUEST['post_type'] : null ;
		}
		// Deals
		if ( $post_type == WP_Groupbuy_Deal::POST_TYPE ) {
			$screen->add_help_tab( array(
					'id'      => 'deal-help', 
					'title'   => self::__( 'Deal Management' ),
					'content' =>
					'<p><strong>' . self::__( 'Adding a new deal.' ) . '</strong></p>' .
					'<p>' . sprintf( self::__( 'Create a new deal by <a href="%s">clicking here</a>.' ), 'post-new.php?post_type=wg_deal' ) . '</p>' .
					'<p><strong>' . self::__( 'Deleting Deals:' ) . '</strong></p>' .
					'<p>' . self::__( 'You can get error with users\' account pages after deleting completed deals, so very careful about this. Keep backup your site frequently.' ) . '</p>'
				) );

			$screen->set_help_sidebar(
				'<p><strong>' . __( 'For more information:' ) . '</strong></p>' .
				'<p>' . self::__( '<a href="http://wpgroupbuy.com/support/documents/" target="_blank">Documentation</a>' ) . '</p>' .
				'<p>' . self::__( '<a href="http://wpgroupbuy.com/contact/" target="_blank">Contact WPG</a>' ) . '</p>'
			);
		}
		// Merchants
		elseif ( wg_merchant_enabled() && $post_type == WP_Groupbuy_Merchant::POST_TYPE ) {
			$screen->add_help_tab( array(
					'id'      => 'merchant-help',
					'title'   => self::__( 'Merchant Center' ),
					'content' =>
					'<p><strong>' . self::__( 'What is a Merchant?' ) . '</strong></p>' .
					'<p>' . self::__( 'Merchants are businesses offer deals for people. They could be restaurants, fashion shops... They signup an account on your website, submit deals.' ) . '</p>' 
				) );
			$screen->add_help_tab( array(
					'id'      => 'merchant-help-assign',
					'title'   => self::__( 'Assign Users' ),
					'content' =>
					'<p><strong>' . self::__( 'How to assign a normal user to a merchant account?' ) . '</strong></p>' .
					'<p>' . self::__( 'You have the ability to assign a normal user to a merchant account. Edit a Merchant account, from the bottom, you can see the assign tool, then choose a user to make a Merchant account.' ) . '</p>'
				) );
			$screen->set_help_sidebar(
				'<p><strong>' . __( 'For more information:' ) . '</strong></p>' .
				'<p>' . self::__( '<a href="http://wpgroupbuy.com/support/documents/" target="_blank">Documentation</a>' ) . '</p>' .
				'<p>' . self::__( '<a href="http://wpgroupbuy.com/contact/" target="_blank">Contact WPG</a>' ) . '</p>'
			);
		}
	}

	public static function help_section_edit() {
		$screen = get_current_screen();
		$post_id = isset( $_GET['post'] ) ? (int)$_GET['post'] : FALSE;
		if ( $post_id ) {
			$post_type = get_post_type( $post_id );
		} else {
			$post_type = ( isset( $_REQUEST['post_type'] ) && post_type_exists( $_REQUEST['post_type'] ) ) ? $_REQUEST['post_type'] : null ;
		}

		// Deals
		if ( $post_type == WP_Groupbuy_Deal::POST_TYPE ) {
			$screen->add_help_tab( array(
					'id'      => 'deal-help-reports',
					'title'   => self::__( 'Deal Reports' ),
					'content' =>
					'<p><strong>' . self::__( 'Purchase Report' ) . '</strong>' .
					': ' . self::__( 'View purchases summary. Download report as a CSV file.' ) . '</p>' .
					'<p><strong>' . self::__( 'Voucher Report' ) . '</strong>' .
					': ' . self::__( 'View vouchers summary. Download report as a CSV file.' ) . '</p>',
				) );
		}
		// Accounts
		elseif ( $post_type == WP_Groupbuy_Account::POST_TYPE ) {
		}
	}

	public static function help_section_edit_post() {
		$screen = get_current_screen();
		$post_id = isset( $_GET['post'] ) ? (int)$_GET['post'] : FALSE;
		if ( $post_id ) {
			$post_type = get_post_type( $post_id );
		} else {
			$post_type = ( isset( $_REQUEST['post_type'] ) && post_type_exists( $_REQUEST['post_type'] ) ) ? $_REQUEST['post_type'] : null ;
		}

		// Deals
		if ( $post_type == WP_Groupbuy_Deal::POST_TYPE ) {

			$screen->add_help_tab( array(
					'id'      => 'deal-help-info',
					'title'   => __( 'Posting a Deal' ),
					'content' =>
					'<p>' . self::__( '<strong>Required content</strong>: You must add title, main content (don\'t use More tag). Deal\'s Feature image, Highlight, Fine Print, Voucher information also required to make site work property.' ) . '</p>'.
					'<p>' . self::__( '<strong>Expiration Date</strong>: Remember to set Expiration Date. If you want to create a comming soon deal, release in next few days or next week or so, choose a furture day then set Publish post time as a furture day, not immediately' ) . '</p>'.
					'<p>' . self::__( '<strong>Location & Category</strong>: Check for locations, categories which you want to show Deal in' ) . '</p>'.
					'<p>' . self::__( '<strong>Featured Image</strong>: should be a JPG or PNG image in 475x475 px minimum.' ) . '</p>'.
					'<p>' . __( '<strong>Discussion</strong> - Turn on/off comment for deal.' ) . '</p>'					
				) );
			$screen->add_help_tab( array(
					'id'      => 'deal-help-pricing',
					'title'   => self::__( 'Pricing' ),
					'content' =>
					'<p>' . self::__( '<strong>Deal Pricing</strong>: Price you want to sell deal, enter number only.' ) . '</p>' .
					'<p>' . self::__( '<strong>Dynamic Pricing</strong>: allow you give discount for buyer, buy more and get cheaper price.' ) . '</p>' .
					'<p>' . sprintf( self::__( '<strong>Tax</strong>: You can change Tax rates and options <a href="%s">here</a>.' ), admin_url( 'admin.php?page=wp-groupbuy/wg_settings&tab=wp-groupbuy/wg_tax_settings' ) ) . '</p>' .
					'<p>' . sprintf( self::__( '<strong>Shipping Fee</strong>: You can change Shipping rates and options <a href="%s">here</a>.' ), admin_url( 'admin.php?page=wp-groupbuy/wg_settings&tab=wp-groupbuy/wg_shipping_settings' ) ) . '</p>',
				) );
			$screen->add_help_tab( array(
					'id'      => 'deal-help-shortcode',
					'title'   => __( 'Shortcode' ),
					'content' =>
					'<p><strong>' . self::__( 'Using Shortcode' ) . '</strong></p>'.
					'<p>' . self::__( 'You can click to Shortcode button on Editing bar to add shortcode. Make sure you review the shortcode, remove unused tag to make it work. Shortcode buttons are available to use on main content only. It doesn\'t word with Deal\'s Highlight, Fine Print, Voucher information field.' ) . '</p>'
				) );
		}

		// Accounts
		elseif ( $post_type == WP_Groupbuy_Account::POST_TYPE ) {
			$screen->add_help_tab( array(
					'id'      => 'account-help-info', 
					'title'   => self::__( 'Contact Info' ),
					'content' =>
					'<p><strong>' . self::__( 'Contact Information Edit.' ) . '</strong></p>' .
					'<p>' . self::__( 'Review and change the account\'s contact information.' ) . '</p>'

				) );
			$screen->add_help_tab( array(
					'id'      => 'account-help-purchase', 
					'title'   => self::__( 'Purchase Management' ),
					'content' =>
					'<p><strong>' . self::__( 'Purchase Management and History.' ) . '</strong></p>' .
					'<p>' . self::__( 'Manually add a purchase to the account. Review the purchase history of the account.' ) . '</p>'

				) );
			$screen->add_help_tab( array(
					'id'      => 'account-help-credits', 
					'title'   => self::__( 'Credit Management' ),
					'content' =>
					'<p><strong>' . self::__( 'Credits.' ) . '</strong></p>' .
					'<p>' . self::__( 'Manage the user\'s credits with the ability to add notes for future reference.' ) . '</p>'
				) );
		}
	}

	// Admin Bar menu
	public static function wp_admin_bar_options( WP_Admin_Bar $wp_admin_bar ) {

		if ( !current_user_can( 'manage_options' ) )
			return;

		$menu_items = apply_filters( 'wg_admin_bar', array() );
		$sub_menu_items = apply_filters( 'wg_admin_bar_sub_items', array() );

		$wp_admin_bar->add_node( array(
				'id' => self::MENU_ID,
				'parent' => false,
				'title' => 'WPG Menu',
				'href' => admin_url( 'edit.php?post_type='.WP_Groupbuy_Deal::POST_TYPE )
			) );

		uasort( $menu_items, array( get_class(), 'sort_by_weight' ) );
		foreach ( $menu_items as $item ) {
			$wp_admin_bar->add_node( array(
					'parent' => self::MENU_ID,
					'id' => $item['id'],
					'title' => self::__($item['title']),
					'href' => $item['href'],
				) );
		}

		$wp_admin_bar->add_group( array(
				'parent' => self::MENU_ID,
				'id'     => self::MENU_ID.'_options',
				'meta'   => array( 'class' => 'ab-sub-secondary' ),
			) );

		$admin_pages = self::get_admin_pages();
		uasort( $admin_pages, array( get_class(), 'sort_by_weight' ) );
		foreach ( $admin_pages as $page => $data ) {
			$sub_menu_items[] = array(
				'parent' => self::MENU_ID.'_options',
				'id' => $page,
				'title' => self::__(str_replace( 'Settings', '', $data['menu_title'] )),
				'href' => admin_url( 'admin.php?page='.$page ),
				'weight' => $data['weight'],
			);
		}
	}
}