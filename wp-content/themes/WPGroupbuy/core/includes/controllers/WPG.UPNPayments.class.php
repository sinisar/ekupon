<?php

add_action( 'wg_init_controllers', array( 'WP_Groupbuy_UPN_Payments', 'init' ), 65 );

class WP_Groupbuy_UPN_Payments extends WP_Groupbuy_Controller {

    const UPN_PAYMENTS_PATH_OPTION = 'wg_upn_payments_path';
    const UPN_PAYMENTS_QUERY_VAR = 'wg_view_upn_payments';
    public static $baseTabPath = 'admin.php?page=wp-groupbuy/voucher_records&tab=wp-groupbuy/';
    private static $upn_payments_path = 'upn_payments';
    private static $instance;
    protected static $settings_page;

    private $id; //id of importing file

    public static function init() {
        self::$settings_page = self::register_settings_page( 'upn_payments_importer', self::__( 'Import UPN payments' ), self::__( 'UPN payments' ), 10, FALSE, 'records', array(get_class(), "display_importer"));
        self::$upn_payments_path = get_option( self::UPN_PAYMENTS_PATH_OPTION, self::$upn_payments_path );
        self::register_path_callback( self::$upn_payments_path, array( get_class(), 'on_payment_importer_page' ), self::UPN_PAYMENTS_QUERY_VAR, 'upn_payments' );
//        add_action( 'init', array( get_class(), 'wp_init' ) );
    }

    public static function get_tab_base_path() {
        return self::$baseTabPath . 'upn_payments_importer';
    }

    public static function display_importer() {
        $upnImporter = new WPG_UPNPaymentImporter(self::get_tab_base_path());
        wg_head_script_variables();
        ?>
        <div class="wrap">
            <h2 class="nav-tab-wrapper">
                <?php self::display_admin_tabs(); ?>
            </h2>
            <?php $upnImporter->dispatch(); ?>
        </div>
    <?php
    }

    public static function on_payment_importer_page() {
        self::get_instance();
    }

    public static function get_instance() {
        if ( !( self::$instance && is_a( self::$instance, __CLASS__ ) ) ) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct() {
        self::do_not_cache();
    }
}

if ( !class_exists( 'WP_Importer' ) ) {
    require_once ABSPATH . 'wp-admin/includes/class-wp-importer.php';
}

//error_reporting(E_ALL);
require_once ('lib/Money/vendor/autoload.php');
require_once ('lib/IBAN/vendor/autoload.php');
require_once ('lib/CAMTparser/vendor/autoload.php');

use Genkgo\Camt\Config;
use Genkgo\Camt\Reader;
use Genkgo\Camt\Camt052;
use Genkgo\Camt\DTO;
use Money\Formatter;
use Money\Currencies;

class WPG_UPNPaymentImporter extends WP_Importer {

    private $importId;
    private $baseImporterPath;
    private $payments = array();

    function __construct($basePath) {
        $this->baseImporterPath = $basePath;
    }

    function dispatch() {
        $this->header();

        $step = empty( $_GET['step'] ) ? 0 : (int) $_GET['step'];
        switch ( $step ) {
            case 0:
                $this->instructions();
                break;
            case 1:
                check_admin_referer( 'import-upload' );
                if ( $this->handle_upload() ) {
                    $this->import_confirmation();
                }
                break;
            case 2:
                check_admin_referer( 'upn_payments' );
                $this->importId = (int) $_POST['import_id'];
                $file = get_attached_file( $this->importId );
                set_time_limit(0);
                $this->import( $file );
                break;
        }

        $this->footer();
    }

    // Display import page title
    function header() {
        echo '<div class="wrap">';
        screen_icon();
        echo '<h2>' . __( 'UPN Payments Importer') . '</h2>';
    }

    function footer() {
        echo '</div>';
    }

    function instructions() {
        echo '<div class="narrow">';
        echo '<p>'.__( 'Upload your ISO 20022 CAMT XML file and we&#8217;ll import the payments.')
            .'</p>';
        echo '<p>'.__( 'Choose a XML file to upload, then click Upload file and import.').'</p>';
        wp_import_upload_form( $this->baseImporterPath .'&amp;step=1' );
        echo '</div>';
    }

    function handle_upload() {
        $file = wp_import_handle_upload();

        if ( isset( $file['error'] ) ) {
            echo '<p><strong>' . __( 'Sorry, there has been an error.') . '</strong><br />';
            echo esc_html( $file['error'] ) . '</p>';
            return false;
        } else if ( ! file_exists( $file['file'] ) ) {
            echo '<p><strong>' . __('Sorry, there has been an error.') . '</strong><br />';
            printf( __( 'The import file could not be found at <code>%s</code>. It is likely that this was caused by a permissions problem.'), esc_html( $file['file'] ) );
            echo '</p>';
            return false;
        }

        $this->importId = (int) $file['id'];
        $import_data = $this->parse( $file['file'] );
        if ( is_wp_error( $import_data ) ) {
            echo '<p><strong>' . __( 'Sorry, there has been an error.' ) . '</strong><br />';
            echo esc_html( $import_data->get_error_message() ) . '</p>';
            return false;
        }

        $this->payments = $import_data;

        return true;
    }

    function import_confirmation() {
        ?>
        <form action="<?php echo admin_url( $this->baseImporterPath .'&amp;step=2' ); ?>" method="post">
            <?php wp_nonce_field( 'upn_payments' ); ?>
            <input type="hidden" name="import_id" value="<?php echo $this->importId; ?>" />

            <?php if ( ! empty( $this->payments ) ) : ?>
                <h3><?php _e( 'UPN Payments Import Confirmation', 'UPNPayments-importer' ); ?></h3>
                <p><?php _e( 'Please, review all payments listed below, and confirm their importing with Submit button.', 'UPNPayments-importer' ); ?></p>
                <ol id="transactions">
                    <?php foreach ( $this->payments as $transaction ) : ?>
                        <li>
                            <?php
                                _e( 'UPN Payment:', 'UPNPayments-importer' );
                                echo ' <strong>' . esc_html( $transaction['debtorName'] );
                                echo ' (' . esc_html( $transaction['debtorAddress'] ) . ')';
                                echo '</strong><br />';
                            ?>
                            <div style="margin-left:18px">
                                <?php
                                    _e( 'IBAN:', 'UPNPayments-importer' );
                                    echo ' '. esc_html( $transaction['IBAN'] ) .'</br>';
                                    _e( 'Amount:', 'UPNPayments-importer' );
                                    echo ' '. esc_html( $transaction['amount'] .' '. $transaction['currency'] ) .'</br>';
                                    _e( 'Booking date:', 'UPNPayments-importer' );
                                    echo ' '. esc_html( $transaction['bookingDate'] ) .'</br>';
                                    _e( 'Value date:', 'UPNPayments-importer' );
                                    echo ' '. esc_html( $transaction['valueDate'] ) .'</br>';
                                    _e( 'Reference:', 'UPNPayments-importer' );
                                    echo ' '. esc_html( $transaction['reference'] ) .'</br>';
                                ?>
                            </div>
                       </li>
                    <?php endforeach; ?>
                </ol>
            <?php endif; ?>
            <p class="submit"><input type="submit" class="button" value="<?php esc_attr_e( 'Submit', 'UPNPayments-importer' ); ?>" /></p>
        </form>
    <?php
    }

    function import( $file ) {
        add_filter( 'http_request_timeout', array( &$this, 'get_http_request_timeout' ) );

        $this->import_start( $file );

        wp_suspend_cache_invalidation( true );
        $this->process_payments();
        wp_suspend_cache_invalidation( false );

        $this->import_end();
    }

    function get_http_request_timeout() {
        return 60;
    }

    function validate_reference( $reference ) {
        $reference = preg_replace( '/\s+/', '', $reference );

        if ( "SI12" != substr($reference, 0, 4) ) {
            return false;
        }

        $reference = preg_replace( '/[^0-9]/', '', substr($reference, 4) );
        if ( strlen($reference) != 13 ) {
            return false;
        }

        $paymentID = intval( substr( $reference, 4, -1) );
        return $paymentID;
    }

    function import_start( $file ) {
        if ( ! is_file($file) ) {
            echo '<p><strong>' . __( 'Sorry, there has been an error.', 'UPNPayments-importer' ) . '</strong><br />';
            echo __( 'The file does not exist, please try again.', 'UPNPayments-importer' ) . '</p>';
            $this->footer();
            die();
        }

        $import_data = $this->parse( $file );

        if ( is_wp_error( $import_data ) ) {
            echo '<p><strong>' . __( 'Sorry, there has been an error.', 'UPNPayments-importer' ) . '</strong><br />';
            echo esc_html( $import_data->get_error_message() ) . '</p>';
            $this->footer();
            die();
        }

        $this->payments = $import_data;

        do_action( 'import_start' );
    }

    function import_end() {
        wp_import_cleanup( $this->importId );

        wp_cache_flush();

        echo '<p>' . __( 'All done.', 'UPNPayments-importer' ) . ' <a href="' . admin_url() . '">' . __( 'Have fun!', 'UPNPayments-importer' ) . '</a>' . '</p>';

        do_action( 'import_end' );
    }

    function parse( $file ) {
        $config = new Config();
        $config->addMessageFormat(new Camt052\MessageFormat\V02());
        if (!$config) {
            return new WP_Error( 'CAMT_Parser_Configuration_Error', __('There was an error occured on CAMT
                configuration setup!', 'UPNPayments-importer'), "Config doesn't exist.");
        }
        $reader = new Reader($config);
        if (!$reader) {
            return new WP_Error( 'CAMT_Parser_Reader_Error', __('There was an error occured on CAMT
                reader setup!', 'UPNPayments-importer'), "Reader doesn't exist.");
        }
        try {
            $message = $reader->readFile($file);
        } catch (\Genkgo\Camt\Exception\ReaderException $ex) {
            return new WP_Error( 'CAMT_Parser_Reading_Error', __('There was an error occured on CAMT
                parsing!', 'UPNPayments-importer'), $ex);
        }

        $statements = $message->getRecords();
        if (empty($statements)) {
            return new WP_Error( 'CAMT_Parser_No_Statements_Error', __('There was an error occured on CAMT
                parsing result!', 'UPNPayments-importer'), "No statements read");
        }
        $result = array();
        $errors = array();
        $decimalMoneyFormatter = new Formatter\DecimalMoneyFormatter(new Currencies\ISOCurrencies());
        foreach ($statements as $statement) {
            $entries = $statement->getEntries();
            foreach($entries as $entry) {
                if ($entry->getAmount()->isPositive()) {
                    if (isset($entry->getTransactionDetails()[0])) {
                        $transDetails = $entry->getTransactionDetails()[0];
                        if (isset($transDetails)) {
                            $data = array(
                                "amount" => $decimalMoneyFormatter->format($entry->getAmount()),
                                "currency" => $entry->getAmount()->getCurrency()->getCode(),
                                "bookingDate" => $entry->getBookingDate()->format('Y-m-d'),
                                "valueDate" => $entry->getValueDate()->format('Y-m-d'),
                                "reference" => (string)$transDetails->getRemittanceInformation()
                                        ->getCreditorReferenceInformation()->getRef()
                            );
                            foreach($transDetails->getRelatedParties() as $relatedParty) {
                                $relatedPartyObject = $relatedParty->getRelatedPartyType();
                                if ( ($relatedPartyObject != null) &&
                                     is_a($relatedPartyObject, DTO\Debtor::class) ){
                                    $debtor = $relatedPartyObject;
                                    $data["debtorName"] = $debtor->getName();
                                    $data["debtorAddress"] = implode(", ", $debtor->getAddress()->getAddressLines());

                                    if ($relatedParty->getAccount() != null) {
                                        $ibanAccount = $relatedParty->getAccount();
                                        $data["IBAN"] = $ibanAccount->getIban()->getIban();
                                    }
                                }
                            }
                            $transactionID = $transDetails->getReferences()[0]->getTransactionId();
                            if( !isset($data['debtorName']) ) {
                                $errors[$transactionID][] = "Debtor name doesn't set!";
                            }
                            if( !isset($data['debtorAddress']) ) {
                                $errors[$transactionID][] = "Debtor address doesn't set!";
                            }
                            if( !isset($data['IBAN']) ) {
                                $errors[$transactionID][] = "IBAN doesn't set!";
                            }
                            $result[] = $data;
                        }
                    }
                }
            }
//            echo "Transactions: <pre>"; print_r($result); echo "</pre>";
        }
        if ( !empty($errors) ) {
            $errMsg = implode(";\r\n", array_map(
                function ($v, $k) { return $k ." - ". implode(', ', $v); },
                $errors,
                array_keys($errors))
            );
            return new WP_Error( 'CAMT_Parser_Data_Recognition_Error', __('There was an error occured on CAMT
                data recognition!', 'UPNPayments-importer'), $errMsg);
        }
        return $result;
    }

    function process_payments() {
        if ( empty( $this->payments ) )
            return;

        echo '<div>';
        foreach ( $this->payments as $payment ) {
            $payment_id = $this->validate_reference( $payment['reference'] );
            if ($payment_id) {
                echo $payment['reference'] . ' - ' . $payment_id . '</br>';

                $payment = WP_Groupbuy_Payment::get_instance( $payment_id );
                if ( $payment ) {
                    $UPNpaymentProcessor = WP_Groupbuy_UPN_Payment::get_instance();

                    if ($UPNpaymentProcessor->complete_UPN_payment( $payment ) ) {
                        echo " -> DA</br>";
                    } else {
                        echo " -> NI uspelo</br>";
                    }
                } else {
                    echo " -> NE obstaja</br>";
                }
            }
        }
        echo '</div>';

        unset( $this->payments );
    }
}