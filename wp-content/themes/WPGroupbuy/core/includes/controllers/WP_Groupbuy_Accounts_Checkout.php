<?php

class WP_Groupbuy_Accounts_Checkout extends WP_Groupbuy_Controller {
	private static $instance;
	const GUEST_PURCHASE_USER_FLAG = 'guest_purchase_user_flag';
	public static function init() {
		self::get_instance();
	}
	private function __clone() {
		trigger_error( __CLASS__.' may not be cloned', E_USER_ERROR );
	}
	private function __sleep() {
		trigger_error( __CLASS__.' may not be serialized', E_USER_ERROR );
	}
	public static function get_instance() {
		if ( !( self::$instance && is_a( self::$instance, __CLASS__ ) ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	private function __construct() {
		add_filter( 'wg_checkout_panes_'.WP_Groupbuy_Checkouts::PAYMENT_PAGE, array( $this, 'display_checkout_registration_form' ), 20, 2 );
		add_action( 'wg_checkout_action_'.WP_Groupbuy_Checkouts::PAYMENT_PAGE, array( $this, 'process_checkout_registration_form' ), 0, 1 );
		add_filter( 'wg_valid_process_payment_page', array( $this, 'validate_payment_page' ), 10, 2 );
		add_filter( 'wg_checkout_account_registration_panes', array( $this, 'filter_registration_panes' ) );
		add_filter( 'wg_account_register_contact_info_fields', array( $this, 'filter_contact_fields_on_checkout_registration' ) );
		add_filter( 'wg_account_register_user_fields', array( $this, 'registration_fields' ), 10, 1 );
		add_action( 'checkout_completed', array( $this, 'checkout_complete' ), 10, 3 );
	}
	public function display_checkout_registration_form( $panes, $checkout ) {
		// Registered users shouldn't see the
		if ( get_current_user_id() ) {
			return $panes;
		}
		if ( !get_option( 'users_can_register' ) ) {
			$error_new_pane['error'] = array(
				'weight' => 1,
				'body' => wpg__( 'Contact an Administrator, registrations are disabled.' ),
			);
			return $error_new_pane;
		}
		if ( !get_current_user_id() ) {
			$args = array();
			if ( get_option( 'users_can_register' ) ) {
				$args['registration_form'] = $this->get_registration_form();
			}
			$args['login_form'] = $this->get_login_form();
			$panes['account'] = array(
				'weight' => 1,
				'body' => self::load_view_to_string( 'checkout/login-or-register', $args ),
			);

			if ( !get_option( 'wg_guest_checkout' ) ) {
				// var_dump($panes); die;
				unset( $panes['billing'] );
				unset( $panes['gifting'] );
				unset( $panes['payment'] );
			}
		}
		return $panes;
	}
	public function registration_fields( $fields = array() ) {
		if ( wg_on_checkout_page() ) {
			// $fields['guest_purchase'] = array(
			// 	'weight' => -10,
			// 	'label' => self::__( 'Guest Purchase' ),
			// 	'type' => 'checkbox',
			// 	'required' => FALSE,
			// 	'value' => 1,
			// );
			unset($fields['password2']);
		}
		return $fields;
	}
	private function get_registration_form() {
		$registration = WP_Groupbuy_Accounts_Registration::get_instance();
		$panes = apply_filters( 'wg_account_registration_panes', array() );
		$panes = apply_filters( 'wg_checkout_account_registration_panes', $panes );
		uasort( $panes, array( get_class(), 'sort_by_weight' ) );
		$args = apply_filters( 'wg_checkout_account_registration_args', array() );
		$view = self::load_view_to_string( 'checkout/register', array( 'panes'=>$panes, 'args' => $args ) );
		return $view;
	}
	public function filter_registration_panes( $panes ) {
		$unregistered_checkout_panes = apply_filters( 'unregistered_registration_checkout_panes', array( 'mc_subs', 'contact_info' ) );
		foreach ( $unregistered_checkout_panes as $pane_key ) {
			unset( $panes[$pane_key] );
		}
		return $panes;
	}
	public function filter_contact_fields_on_checkout_registration( $fields ) {
		if ( isset( $_POST['wg_login_or_register'] ) ) {
			return array();
		}
		return $fields;
	}
	private function get_login_form() {
		$args = apply_filters( 'wg_checkout_account_login_args', array() );
		$view = self::load_view_to_string( 'checkout/login', array( 'args' => $args ) );
		return $view;
	}
	// Hook into the payment process page and check to see if the the user is trying to login or register. Check to see if the user has selected guest checkout as well.
	public function process_checkout_registration_form( WP_Groupbuy_Checkouts $checkout ) {
		if ( !isset( $_POST['wg_login_or_register'] ) ) {
			return;
		}
		if ( $_POST['log'] != '' ) {
			$login = WP_Groupbuy_Accounts_Login::get_instance(); // make sure the class is instantiated
			$user = wp_signon();
			wp_set_current_user( $user->ID );
			if ( !$user || !$user->ID ) {
				self::set_message( self::__( 'Login unsuccessful. Please try again.' ), self::MESSAGE_STATUS_ERROR );
			}
		} else {
			// Guest Checkout
			if ( isset( $_POST['wg_user_guest_purchase'] ) && $_POST['wg_user_guest_purchase'] ) {
				$cart = &$checkout->get_cart();
				$cart_id = $cart->get_id();
				// User
				$user_login = $email = $_POST['wg_billing_email'];

				// Check if user exists
				if ( $user_id = username_exists( $user_login ) ) {
					// $user_id already set
				} else {
					$password = wp_generate_password();
					// Create user
					$user_id = WP_Groupbuy_Accounts_Registration::create_user( $user_login, $email, $password, $_POST );
					update_user_meta( $user_id, self::GUEST_PURCHASE_USER_FLAG, 1 );
				}
				if ( $user_id ) {
					$user = wp_signon(
						array(
							'user_login' => $user_login,
							'user_password' => $password,
							'remember' => false
						), false );
					wp_set_current_user( $user->ID );
				}
			}
			else { // Registration
				WP_Groupbuy_Accounts_Registration::get_instance(); // instantiating should process the form
				$user = wp_get_current_user();
				if ( $user && $user->ID ) {
					self::set_message( self::__( 'Registration complete. Please continue with your purchase.' ) );
				}
			}
		}
		// Don't validate billing fields
		add_filter( 'wg_valid_process_payment_page_fields', '__return_false');
		// mark checkout incomplete
		add_filter( 'wg_valid_process_payment_page', '__return_false');
	}
	// After checkout is complete check whether the purchase was made by a temp account and update that WP User with the Purchase ID, log the user out and change the guest purchase flag
	public static function checkout_complete( WP_Groupbuy_Checkouts $checkout, WP_Groupbuy_Payment $payment, WP_Groupbuy_Purchase $purchase ) {
		$user_id = get_current_user_id();
		if ( get_user_meta( $user_id, self::GUEST_PURCHASE_USER_FLAG, TRUE ) == 1 ) { // If the user is flagged as a guest user
			global $wpdb;
			$purchase_id = $purchase->get_id();
			$wpdb->query( $wpdb->prepare(  "UPDATE $wpdb->users SET user_login = %s WHERE ID = %s", $purchase_id, $user_id ) );
			// Logout
	 		wp_logout();
	 		update_user_meta( $user_id, self::GUEST_PURCHASE_USER_FLAG, $purchase_id );
		}
	}
	// Delete temporary users without a purchase
	public static function delete_temp_users() {
		// get users with self::GUEST_PURCHASE_USER_FLAG set to 1
		// check registration date and delete if older than x days
	}
	public function validate_payment_page( $valid, $checkout ) {
		$user = wp_get_current_user();
		if ( !$user || !$user->ID ) {
			return FALSE;
		}
		return $valid;
	}
}
