<?php

abstract class WP_Groupbuy_MegaPOS extends WP_Groupbuy_Offsite_Processors {

    const MEGAPOS_STATUS_URL = '/payments/status/';
	const MEGAPOS_UPDATE_URL = '/payments/payment_transaction_update/';

    const MEGAPOS_LANGUAGE_ID = 'SLV';
	const MEGAPOS_CURRENCY = 'EUR';
	const MEGAPOS_ORDER_TRANSACTION_TYPE = 'ORDER';
	const MEGAPOS_PURCHASE_TRANSACTION_TYPE = 'PURCHASE';

	const MODE_TEST = 'test';
	const MODE_LIVE = 'izkoristi';
    const STORE_ID = 'izkoristi';
	const API_MODE_OPTION = 'wg_megapos_mode';

    private $api_mode = self::MODE_TEST;
    private $soap_client;

    private $currentGateway;

	public function __construct() {
        parent::__construct();
		$this->api_mode = get_option( self::API_MODE_OPTION, self::MODE_TEST );

        $this->initSoapClient();

        add_action( 'admin_init', array( $this, 'register_settings' ), 10, 0 );

        // Send offsite and handle the return
        add_action( 'wg_send_offsite_for_payment_after_review', array( $this, 'send_offsite' ), 10, 1 );
        add_action( 'wg_load_cart', array( $this, 'back_from_megapos' ), 10, 0 );

		add_action( 'purchase_completed', array( $this, 'complete_purchase' ), 10, 1 );
	}

    protected function setPaymentGateway( $gateway ) {
        $this->currentGateway = $gateway;
    }


    private function initSoapClient() {
        $soapData = array('local_cert' => $this->getCertFile());
        if ($this->api_mode == self::MODE_TEST) {
            $soapData['cache_wsdl'] = WSDL_CACHE_NONE;
            $soapData['trace'] = 1;
        }
        $this->soap_client = new SoapClient($this->getWSDLFile(), $soapData);
    }

    private function getCertFile() {
        return dirname(__FILE__).'/payment-gateway/library/megapos/izkoristi/izkoristi_php.pem';
    }

    private function getWSDLFile() {
        return dirname(__FILE__).'/payment-gateway/library/megapos/megapos_v3_'.$this->api_mode.'.wdsl';
    }


    private function log_error_message( $method, $msg, $addData = NULL) {
        $debugMsg = "ERROR: ". $method .": ". $msg. ($addData ? "[". print_r($addData, true) ."]" : "");
        $display = FALSE;
        if ($this->api_mode == self::MODE_TEST) {
            $display = TRUE;
        }

        $this->log_message($debugMsg, self::MESSAGE_STATUS_ERROR, $display);
    }

    private function log_info_message( $method, $msg, $addData = NULL) {
        $debugMsg = "INFO: ". $method .": ". $msg. ($addData ? "[". print_r($addData, true) ."]" : "");
        $display = FALSE;
        if ($this->api_mode == self::MODE_TEST) {
            $display = TRUE;
        }

        $this->log_message($debugMsg, self::MESSAGE_STATUS_INFO, $display);
    }

    private function log_message( $message, $msg_type, $display = TRUE ) {
		// If you want to display messages on page, set $display = TRUE
		$display = FALSE;
        if ( $display ) {
            self::set_message( $message, $msg_type );
        } else {
            error_log( "Message from MegaPOS ({$msg_type}): " . print_r( $message, true ) );
        }
    }

    protected function get_transaction_status($transactionId) {
        $transactionModel = new WP_Groupbuy_MegaPOS_Transaction($transactionId);
        if ($transactionModel) {
            return $transactionModel->getStatus();
        } else {
            $this->log_error_message(__METHOD__, "Transaction with ID: {$transactionId} doesn't exists.");
        }
        return false;
    }

    protected function get_transaction($transactionId) {
        $transactionModel = new WP_Groupbuy_MegaPOS_Transaction($transactionId);
        if ($transactionModel) {
            return $transactionModel;
        } else {
            $this->log_error_message(__METHOD__, "Transaction with ID: {$transactionId} doesn't exists.");
        }
        return false;
    }

    private function client_load_transaction($transactionId) {
        try {
            $requestData = $this->prepare_id_data($transactionId);
            return $this->soap_client->load( $requestData );
        } catch (Exception $e) {
            $this->log_error_message(__METHOD__, "Error on reading transaction from soap client.", $e->getMessage());
        }
        return false;
    }

    public function update_transaction_status($transactionId) {
        $this->log_info_message(__METHOD__, "MegaPOS Updating TransactionID: {$transactionId}");
        $transactionClientData = $this->client_load_transaction($transactionId);
        $this->log_info_message(__METHOD__, "MegaPOS processing update with client transaction Data.", $transactionClientData);
        if ( $transactionClientData &&
            isset($transactionClientData->{'active-state'}) &&
            isset($transactionClientData->{'active-state'}->type) ) {
            $active_state_type = $transactionClientData->{'active-state'}->type;

            $transactionModel = new WP_Groupbuy_MegaPOS_Transaction($transactionId);
            if ($transactionModel) {
                if ($transactionModel->update_status($active_state_type)) {
                    return true;
                }
            }
        } else {
            $this->log_error_message(__METHOD__, "Transaction with ID: {$transactionId} doesn't exists.");
        }
        return false;
    }

    private function list_payment_gateways() {
        try {
            return $this->soap_client->listGateways(self::STORE_ID);
        } catch (Exception $e) {
            $this->log_error_message(__METHOD__, "Error on reading payment gateways from saop client.", $e->getMessage());
        }
        return false;
    }

    private function get_gateway_info($gatewayName) {
        $paymentGateways = $this->list_payment_gateways();
        if ($paymentGateways) {
            if (is_array($paymentGateways->{'item'})) {
                foreach ($paymentGateways->item as $payGateway) {
                    if ($payGateway->name === $gatewayName) {
                        return $payGateway;
                    }
                }
            } elseif (is_object($paymentGateways->{'item'})) {
                $payGateway = $paymentGateways->{'item'};
                if ($payGateway->name === $gatewayName) {
                    return $payGateway;
                }
            }
        }
        $this->log_error_message(__METHOD__, "Selected payment gateway '{$gatewayName}' doesn't registered in system.", $paymentGateways);
        return false;
    }

    private function get_transaction_type($gateway) {
        if( $gateway->{'supports-order'} ) {
            return self::MEGAPOS_ORDER_TRANSACTION_TYPE;
        } elseif( $gateway->{'supports-purchase'} ) {
            return self::MEGAPOS_PURCHASE_TRANSACTION_TYPE;
        }
        $this->log_error_message(__METHOD__, "Payment gateway doesn't support no of available transaction types.", $gateway);
        return false;
    }

    private function generate_transaction_ID($gateway) {
        return substr($gateway, 0, 4).mt_rand(10000, 99999).uniqid();
    }

    private function prepare_init_data(WP_Groupbuy_Checkouts $checkout, $totalAmount, $gatewayName) {
        $user = get_userdata( get_current_user_id() );
        $gateway = $this->get_gateway_info($gatewayName);
        if( $gateway ) {
            return array(
                'store-id'          => self::STORE_ID,
                'transaction-id'    => $this->generate_transaction_ID($gatewayName),
                'amount'            => number_format($totalAmount, 2),
                'currency'          => self::MEGAPOS_CURRENCY,
                'status-url'        => get_site_url().self::MEGAPOS_STATUS_URL,
                'update-url'        => get_site_url().self::MEGAPOS_UPDATE_URL,
                'gateway-id'        => $gateway->id,
                'transaction-type'  => $this->get_transaction_type($gateway),
                'language'          => self::MEGAPOS_LANGUAGE_ID,
                'customer-name'     => is_a($user, 'WP_User') ? $user->user_firstname : '',
                'customer-surname'  => is_a($user, 'WP_User') ? $user->user_lastname : '',
                'additional-info'   => is_a($user, 'WP_User') ? json_encode(
                                                                    array(
                                                                        'displayName' => $user->display_name,
                                                                        'niceName' => $user->user_nicename,
                                                                        'login' => $user->user_login
                                                                    )
                                                                ) : '',
                'email'             => is_a($user, 'WP_User') ? $user->user_email : '',
            );
        } else {
            $this->log_error_message(__METHOD__, "Selected payment gateway '{$gatewayName}' doesn't registered in system.", $gateway);
            return FALSE;
        }
    }

    private function prepare_id_data($transactionId) {
        return array(
            'store-id'       => self::STORE_ID,
            'transaction-id' => $transactionId
        );
    }

    protected function transaction_init($init_data, $gateway) {
        global $wpdb;
        try {
            $infoData = array(
                'status'        => 'INITIALIZING',
                'gateway'       => $gateway,
                'customer-ip'   => $_SERVER['REMOTE_ADDR']
            );
            $transactionModel = new WP_Groupbuy_MegaPOS_Transaction();
            if ($transactionModel->save(array_merge($init_data, $infoData))) {
                $result = $this->soap_client->init($init_data);
                $redirect_url = $result->{'active-state'}->result;
                return $redirect_url;
            } else {
                $this->log_error_message(__METHOD__, "Error occured on saving transaction into DB.", $transactionModel);
            }
        }
        catch (Exception $e) {
            $this->log_error_message(__METHOD__, "Error occured on transaction initialization.", $e->getMessage());
        }
        return FALSE;
    }

    public function transaction_cancel($transactionId) {
        try {
            $result = $this->soap_client->cancelTransaction($this->prepare_id_data($transactionId));

            $transactionModel = new WP_Groupbuy_MegaPOS_Transaction($transactionId);

            $result_type = $result->{'active-state'}->type;
            if ($result_type == 'CANCELLED') {
                if ($transactionModel->update_status($result_type)) {
                    return true;
                } else {
                    $this->log_error_message(__METHOD__, "Error occured on saving transaction new status.", array($transactionId,
                        $result_type));
                }
            } else {
                $this->log_error_message(__METHOD__, "Status of canceled transaction is invalid.", array($transactionId, $result));
            }
        } catch (Exception $e) {
            $this->log_error_message(__METHOD__, "Error occured on canceling transaction.", $e->getMessage());
        }
        return false;
    }

    public function transaction_process($transactionId) {
        try {
            $this->log_info_message(__METHOD__, "Starting transaction processing...", array($transactionId));
            if( $transaction = new WP_Groupbuy_MegaPOS_Transaction($transactionId) ) {
                if( ($transaction->getTransactionType() == 'ORDER') && ($transaction->getStatus() == 'INITIALIZED') ) {
                    $amount_data['amount'] = $transaction->getAmount();
                    $amount_data['currency'] = $transaction->getCurrency();
                    $amount_data_merged = array_merge($this->prepare_id_data($transactionId), $amount_data);

                    $result = $this->soap_client->processTransaction($amount_data_merged);
                    $result_type = $result->{'active-state'}->type;
                    if ($result_type == 'PROCESSED'){
                        if ($transaction->update_status($result_type)) {
                            return TRUE;
                        } else {
                            $this->log_error_message(__METHOD__, "Error occured on transaction status updating.",
                                array($transactionId, $transaction, $amount_data_merged, $result));
                        }
                    } else {
                        $this->log_error_message(__METHOD__, "Status of processed transaction is invalid.",
                            array($transactionId, $transaction, $amount_data_merged, $result));
                    }
                } elseif( ($transaction->getTransactionType() == 'PURCHASE') &&
                          ( ($transaction->getStatus() == 'PROCESSED') ||
                            ($transaction->getStatus() == 'AWAITING-CONFIRMATION') ) ) {
                    return TRUE;
                } else {
                    $this->log_error_message(__METHOD__, "Transaction has invalid status.",
                        array($transactionId, $transaction));
                }
            } else {
                $this->log_error_message(__METHOD__, "Can't load transaction with ID: {$transactionId}.");
            }
        } catch (Exception $e) {
            $this->log_error_message(__METHOD__, "Error occured on transaction processing.", $e->getMessage());
        }
        return FALSE;
    }

	public function register_settings() {
		$page = WP_Groupbuy_Payment_Processors::get_settings_page();
		$section = 'wg_megapos_settings';
		add_settings_section( $section, self::__( 'MegaPOS' ), array( $this, 'display_settings_section' ), $page );
		register_setting( $page, self::API_MODE_OPTION );
		add_settings_field( self::API_MODE_OPTION, self::__( 'Choose mode' ), array( $this, 'display_api_mode_field' ), $page, $section );
	}

	public function display_api_mode_field() {
		echo '<label><input type="radio" name="'.self::API_MODE_OPTION.'" value="'.self::MODE_LIVE.'" '.checked(
                self::MODE_LIVE, $this->api_mode, FALSE ).'/> '.self::__( 'Live' ).'</label><br />';
		echo '<label><input type="radio" name="'.self::API_MODE_OPTION.'" value="'.self::MODE_TEST.'" '.checked(
                self::MODE_TEST, $this->api_mode, FALSE ).'/> '.self::__( 'Test' ).'</label>';
	}

    protected function process_MegaPOS_payment(WP_Groupbuy_Checkouts $checkout, WP_Groupbuy_Purchase $purchase, $gateway) {
        $filtered_total = self::get_payment_request_total( $checkout );
        if ( $filtered_total < 0.01 ) {
            $payments = WP_Groupbuy_Payment::get_payments_for_purchase( $purchase->get_id() );
            foreach ( $payments as $payment_id ) {
                $payment = WP_Groupbuy_Payment::get_instance( $payment_id );
                return $payment;
            }
        }

//        $user = get_userdata( get_current_user_id() );
        $shipping_address = NULL;
        if ( isset( $checkout->cache['shipping'] ) ) {
            $shipping_address = array();
            $shipping_address['first_name'] = $checkout->cache['shipping']['first_name'];
            $shipping_address['last_name'] = $checkout->cache['shipping']['last_name'];
            $shipping_address['street'] = $checkout->cache['shipping']['street'];
            $shipping_address['city'] = $checkout->cache['shipping']['city'];
            $shipping_address['zone'] = $checkout->cache['shipping']['zone'];
            $shipping_address['postal_code'] = $checkout->cache['shipping']['postal_code'];
            $shipping_address['country'] = $checkout->cache['shipping']['country'];
        }

        // PAYMENT SAVE
        // create loop of deals for the payment post
        $deal_info = array();
        foreach ( $purchase->get_products() as $item ) {
            if ( isset( $item['payment_method'][$this->get_payment_method()] ) ) {
                if ( !isset( $deal_info[$item['deal_id']] ) ) {
                    $deal_info[$item['deal_id']] = array();
                }
                $deal_info[$item['deal_id']][] = $item;
            }
        }

        // create new payment
        $payment_id = WP_Groupbuy_Payment::new_payment(
            array(
                'payment_method' => $this->get_payment_method(),
                'purchase' => $purchase->get_id(),
                'amount' => $filtered_total,
                'data' => array(
                        'transaction_id' => $checkout->cache['transaction_id'],
                        'uncaptured_deals' => $deal_info
                ),
                'deals' => $deal_info,
                'shipping_address' => $shipping_address,
            ),
            WP_Groupbuy_Payment::STATUS_AUTHORIZED );
        if ( !$payment_id ) {
            return FALSE;
        }
        $payment = WP_Groupbuy_Payment::get_instance( $payment_id );
        do_action( 'payment_authorized', $payment );

        return $payment;
    }

    public function send_offsite( WP_Groupbuy_Checkouts $checkout ) {
//        $user = get_userdata( get_current_user_id() );
        $filtered_total = self::get_payment_request_total( $checkout );
        if ( $filtered_total < 0.01 ) {
            return FALSE;
        }

        // block if not in payment way
        if( $_REQUEST['wg_checkout_action'] == WP_Groupbuy_Checkouts::REVIEW_PAGE ) {
            // TRANSACTION INIT
            $init_data = $this->prepare_init_data($checkout, $filtered_total, $this->currentGateway);
            if ( self::DEBUG ) {
                $this->log_info_message(__METHOD__, "MegaPOS init data.", $init_data);
            }
            if (!$init_data) {
                $this->log_error_message(__METHOD__, "Preparing of transaction init data failed.");
                return FALSE;
            }
            // save transaction ID for later payment completing, when it should be checked
            $checkout->cache['transaction_id'] = $init_data['transaction-id'];

            $redirect_url = $this->transaction_init($init_data, $this->currentGateway);
            $this->log_info_message(__METHOD__, "MegaPOS redirect URL: {$redirect_url}");

            if ( $redirect_url ) {
                wp_redirect ( $redirect_url );
                exit();
            } else {
                $this->log_error_message(__METHOD__, "Invalid MegaPOS redirect URL:  {$redirect_url}");
                return FALSE;
            }
        }
    }

    public function back_from_megapos() {
        if ( self::returned_from_offsite() ) {
            // let the checkout know that this isn't a fresh start
            $_REQUEST['wg_checkout_action'] = 'back_from_megapos';
        } elseif ( !isset( $_REQUEST['wg_checkout_action'] ) ) {
            // this is a new checkout.
        }
    }

    public function complete_purchase( WP_Groupbuy_Purchase $purchase ) {
        $this->log_info_message(__METHOD__, "Start to complete purchase: {$purchase->get_id()}.");
        if (self::DEBUG) {
            $this->log_info_message(__METHOD__, "Completing purchase.", $purchase);
        }
        $payments = WP_Groupbuy_Payment::get_payments_for_purchase( $purchase->get_id() );
        foreach ( $payments as $payment_id ) {
            $payment = WP_Groupbuy_Payment::get_instance( $payment_id );
            $this->log_info_message(__METHOD__, "Start to complete payment: {$payment->get_id()}.");
            if (self::DEBUG) {
                $this->log_info_message(__METHOD__, "Completing payment.", $payment);
            }

            $this->log_info_message(__METHOD__, "Payment method: {$payment->get_payment_method()}, payment status: {$payment->get_status()}.");
            if ( $payment->get_payment_method() == $this->get_payment_method() && $payment->get_status() != WP_Groupbuy_Payment::STATUS_COMPLETE ) {

                $items_to_capture = $this->items_to_capture( $payment );
                $this->log_info_message(__METHOD__, "Items to capture for payment: {$payment->get_id()}.", $items_to_capture);

                if( $items_to_capture ) {
                    $data = $payment->get_data();

                    // read data for transaction processing
                    $transactionId = $data['transaction_id'];
                    $transactionStatus = WP_Groupbuy_Payment_Transaction::get_transaction_status($transactionId);
                    $this->log_info_message(__METHOD__, "Starting processing transaction '{$transactionId}' with
                        status '{$transactionStatus}'.");

                    if( WP_Groupbuy_Payment_Transaction::get_transaction_final_status($transactionStatus) ===
                        WP_Groupbuy_Payment_Transaction::WPG_MEGAPOS_TRANSACTION_FINAL_STATUS_SUCCESS ) {
                        if( $this->transaction_process($transactionId) ) {
                            $statusAfterProcess = WP_Groupbuy_Payment_Transaction::get_transaction_status($transactionId);
                            if ( ($statusAfterProcess === WP_Groupbuy_MegaPOS_Transaction::TRANS_STATUS_PROCESSED) ||
                                 ($statusAfterProcess === WP_Groupbuy_MegaPOS_Transaction::TRANS_STATUS_AWAITING_CONFIRMATION) ) {
                                // remove deal items from payment uncaptured items
                                foreach ( $items_to_capture as $deal_id => $amount ) {
                                    unset( $data['uncaptured_deals'][$deal_id] );
                                }
                                do_action( 'payment_captured', $payment, array_keys( $items_to_capture ) );

                                // Set the status
                                if ( count( $data['uncaptured_deals'] ) < 1 ) {
                                    $payment->set_status( WP_Groupbuy_Payment::STATUS_COMPLETE );
                                    do_action( 'payment_complete', $payment );
                                    $this->log_info_message(__METHOD__, "Completing payment finished: {$payment->get_id()}");
                                } else {
                                    $payment->set_status( WP_Groupbuy_Payment::STATUS_PARTIAL );
                                }
                            } else {
                                $this->log_error_message(__METHOD__, "Transaction status after processing is invalid
                                for completing payment.", array($transactionId, $statusAfterProcess));
                            }
                        } else {
                            $this->log_error_message(__METHOD__, "Processing of transaction '{$transactionId}' failed.
                                Payment '{$payment_id} will be skipped.", array($transactionId));
                        }
                    } else {
                        $this->log_error_message(__METHOD__, "Current status of transaction is invalid for processing.",
                            array($transactionId, $transactionStatus));
                    }
                } else {
                    $this->log_error_message(__METHOD__, "No items to capture for payment: {$payment->get_id()}.",
                        $items_to_capture);
                }
                $this->log_info_message(__METHOD__, "Completing purchase finished: {$purchase->get_id()}");
            }
        }
    }

    public function process_payment( WP_Groupbuy_Checkouts $checkout, WP_Groupbuy_Purchase $purchase ) {

    }
}