<?php

add_action( 'wg_init_controllers', array( 'WP_Groupbuy_Shipping_Fees', 'init' ), 130 );

class WP_Groupbuy_Shipping_Fees extends WP_Groupbuy_Controller {
	const OPTION_FLAT = 'wg_enable_flat_rate';
	const FLAT_MODE = 'flat-rate-cart';
	const MODE_NAME = 'Flat Rate Cart';
	private static $enable;
	private static $rates;
	private static $include_shipping = FALSE;

	public static function init() {
		// Options
		add_action( 'admin_init', array( get_class(), 'register_settings_fields' ), 40, 0 );
		self::$enable = get_option( self::OPTION_FLAT, 'TRUE' );
		self::$rates = get_option( WP_Groupbuy_Core_Shipping::SHIPPING_RATES );

		// Filter Cart
		// add_filter( 'wg_shipping_cart_shipping_total_bool', array( get_class(), 'filter_cart_shipping_total_bool' ), 10, 3 );
		add_filter( 'wg_shipping_cart_shipping_total', array( get_class(), 'filter_cart_shipping_total' ), 10, 3 );

		// Filter Purchase
		add_filter( 'wg_shipping_purchase_shipping_total', array( get_class(), 'filter_purchase_total' ), 10, 5 );
		add_filter( 'wg_shipping_purchase_item_shipping', array( get_class(), 'filter_purchase_item_shipping' ), 10, 5 );

	}

	public static function flat_rate_enabled() {
		if ( self::$enable !== 'TRUE' ) {
			return FALSE;
		}
		return TRUE;
	}

	public static function filter_cart_shipping_total_bool( $bool, $shipping_total, $cart, $local = null ) {
		if ( is_a( $shipping_total, 'WP_Groupbuy_Cart' ) ) {
			$local = $cart;
			$cart = $shipping_total;
			$shipping_total = 0;
		}

		$total = self::filter_cart_shipping_total( 0, $cart, $local );
		if ( $total > 0.01 ) {
			return TRUE;
		}
		return $bool;
	}

	public static function filter_cart_shipping_total( $filtered_shipping_total, WP_Groupbuy_Cart $cart, $local = null, $bool_return = FALSE ) {
		if ( !self::flat_rate_enabled() ) {
			return $filtered_shipping_total;
		}
		$use_flat_rate = FALSE;
		$shipping_total = 0;
		if ( WP_Groupbuy_Core_Shipping::shipping_local_enabled() && NULL === $local ) {
			$account = WP_Groupbuy_Account::get_instance();
			$address = $account->get_ship_address();
			if ( empty( $address ) ) {
				$address = $account->get_address();
			}
			if ( !empty( $address ) ) {
				$local = array(
					'zone' => $address['zone'],
					'country' => $address['country'],
				);
			}
		}

		$deals_totaled = WP_Groupbuy_Core_Shipping::deal_quantity( $cart->get_items() );
		foreach ( $deals_totaled as $deal_id => $qty ) {
			$deal = WP_Groupbuy_Deal::get_instance( $deal_id );
			if ( is_a( $deal, 'WP_Groupbuy_Deal' ) ) {
				$mode = $deal->get_shipping_mode();
				if ( $mode == self::FLAT_MODE ) {
					$use_flat_rate = ( WP_Groupbuy_Core_Shipping::shipping_local_enabled() ) ? self::local_check( $mode, $local ) : TRUE ;
					$deal_shipping = WP_Groupbuy_Core_Shipping::get_shipping( $deal, $qty, $local );
					if ( $deal_shipping > $shipping_total ) {
						$shipping_total = $deal_shipping;
					}
				}

			}
		}
		if ( $bool_return ) {
			return $use_flat_rate;
		}
		return ( $use_flat_rate ) ? $shipping_total : $filtered_shipping_total;
	}

	public static function local_check( $mode, $local ) {
		$match = FALSE;
		foreach ( self::$rates as $rate_id => $data ) {
			if ( NULL != $local && WP_Groupbuy_Core_Shipping::shipping_local_enabled() ) {
				if ( $mode == $data['mode'] ) {
					if ( !empty( $data['zones'] ) && !empty( $data['regions'] )  ) {
						if ( in_array( $local['zone'], $data['zones'] ) && in_array( $local['country'], $data['regions'] ) ) {
							$match = TRUE;
							break;
						}
					}
					elseif ( !empty( $data['zones'] ) && !$matched_zone ) {
						if ( in_array( $local['zone'], $data['zones'] ) ) {
							$match = TRUE;
							if ( count( $data['zones'] ) == 1 ) {
								break;
							}
							$matched_zone = TRUE;
						}
					}
					elseif ( !empty( $data['regions'] ) && !$matched_zone && !$matched_region ) {
						if ( in_array( $local['country'], $data['regions'] ) ) {
							$match = TRUE;
							$matched_region = TRUE;
						}
					}
				}
			}
		}
		return $match;
	}

	public static function filter_purchase_total( $filtered_shipping_total, WP_Groupbuy_Purchase $purchase, $payment_method= NULL, $local= NULL, $distribute = TRUE ) {

		if ( !self::flat_rate_enabled() ) {
			return $filtered_shipping_total;
		}

		$use_flat_rate = FALSE;
		$distribute_total = 0;
		$shipping_total = 0;
		foreach ( $purchase->get_products() as $item ) {
			if ( isset( $item['payment_method'][$payment_method] ) ) {
				$distribute_total += $item['quantity'];
				$deal = WP_Groupbuy_Deal::get_instance( $item['deal_id'] );
				if ( is_a( $deal, 'WP_Groupbuy_Deal' ) ) {
					$mode = $deal->get_shipping_mode();
					if ( $mode === self::FLAT_MODE ) {
						$use_flat_rate = TRUE;
						$deal_shipping = WP_Groupbuy_Core_Shipping::get_shipping( $deal, 1, $local );
						if ( $deal_shipping > $shipping_total ) {
							$shipping_total = $deal_shipping;
						}
					}
				}
			}
		}

		if ( $use_flat_rate ) {
			if ( $distribute_total ) {
				return $shipping_total/$distribute_total;
			}
			return $shipping_total;
		}
		return $filtered_shipping_total;
	} 

	public static function filter_purchase_item_shipping( $filtered_shipping_total, WP_Groupbuy_Purchase $purchase, $item, $local= NULL, $distribute = TRUE ) {

		if ( !self::flat_rate_enabled() ) {
			return $filtered_shipping_total;
		}

		$use_flat_rate = FALSE;
		$distribute_total = 0;
		$shipping_total = 0;
		foreach ( $purchase->get_products() as $item ) {
			$distribute_total += $item['quantity'];
			$deal = WP_Groupbuy_Deal::get_instance( $item['deal_id'] );
			if ( is_a( $deal, 'WP_Groupbuy_Deal' ) ) {
				$mode = $deal->get_shipping_mode();
				if ( $mode === self::FLAT_MODE ) {
					$use_flat_rate = TRUE;
					$deal_shipping = WP_Groupbuy_Core_Shipping::get_shipping( $deal, 1, $local );
					if ( $deal_shipping > $shipping_total ) {
						$shipping_total = $deal_shipping;
					}
				}
			}
		}

		if ( $use_flat_rate ) {
			if ( $distribute_total ) {
				return $shipping_total/$distribute_total;
			}
			return $shipping_total;
		}
		return $filtered_shipping_total;

	}

	final protected function __clone() {
		trigger_error( __CLASS__.' may not be cloned', E_USER_ERROR );
	}

	final protected function __sleep() {
		trigger_error( __CLASS__.' may not be serialized', E_USER_ERROR );
	}

	protected function __construct() {}


	public static function register_settings_fields() {
		$page = 'wp-groupbuy/wg_shipping_settings';
		$section = 'wg_shipping';

		// Settings
		register_setting( $page, self::OPTION_FLAT, array( get_class(), 'save_local_option' ) );

		add_settings_field( self::OPTION_FLAT, self::__( 'Shopping Cart based' ), array( get_class(), 'display_enable_flat_rate' ), $page, $section );
	}

	public static function display_enable_flat_rate() {
		echo '<lable><input type="checkbox" name="'.self::OPTION_FLAT.'" value="TRUE" '.checked( 'TRUE', self::$enable, FALSE ).'>'.self::__( 'Enable shipping fee per cart.' ).'</label>';

	}

	public static function save_local_option( $flat_shipping ) {
		if ( !isset( $_POST[self::OPTION_FLAT] ) )
			return $flat_shipping;
		if ( !isset( $_POST[WP_Groupbuy_Core_Shipping::SHIPPING_OPTION] ) || ( isset( $_POST[self::OPTION_FLAT] ) && $_POST[WP_Groupbuy_Core_Shipping::SHIPPING_OPTION] != 'TRUE' ) ) {
			$flat_shipping = 'FALSE';
		} else {
			$modes = get_option( WP_Groupbuy_Core_Shipping::SHIPPING_MODES );
			if ( !strpos( $modes, self::MODE_NAME ) ) {
				update_option( WP_Groupbuy_Core_Shipping::SHIPPING_MODES, $modes."\n".self::MODE_NAME, true );
			}
		}
		return $flat_shipping;
	}

}
