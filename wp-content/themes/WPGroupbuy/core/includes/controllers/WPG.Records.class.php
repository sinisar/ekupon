<?php

add_action( 'wg_init_controllers', array( 'WP_Groupbuy_Records', 'init' ), 75 );

class WP_Groupbuy_Records extends WP_Groupbuy_Controller {

	private static $instance;

	public static function init() {
		add_action( 'wg_new_record', array( get_class(), 'new_record' ), 10, 6 );
	}

	public static function affiliate_record( $account, $payment_id, $credits, $type ) {
		$account_id = $account->get_ID();
		$balance = $account->get_credit_balance( $type );
		$data = array();
		$data['account_id'] = $account_id;
		$data['payment_id'] = $payment_id;
		$data['credits'] = $credits;
		$data['type'] = $type;
		$data['current_total_'.$type] = $credits;
		$data['prior_total_'.$type] = $balance;
		$data['adjustment_value'] = $credits;
		$data['current_total'] = $balance;
		$data['prior_total'] = $balance-$credits;
		WP_Groupbuy_Records::new_record( sprintf( self::__( '%s Credit Applied' ), ucfirst( $type ) ), WP_Groupbuy_Accounts::$record_type . '_' . $type, sprintf( self::__( '%s Credit Applied' ), ucfirst( $type ) ), 1, $account_id, $data );
	}

	public static function new_record( $message, $type = 'mixed', $title = '', $author = 1, $associate_id = -1, $data = array() ) {
		return WP_Groupbuy_Record::new_record( $message, $type, $title, $author, $associate_id, $data );
	}

	private function __clone() {
		trigger_error( __CLASS__.' may not be cloned', E_USER_ERROR );
	}
	private function __sleep() {
		trigger_error( __CLASS__.' may not be serialized', E_USER_ERROR );
	}
	public static function get_instance() {
		if ( !( self::$instance && is_a( self::$instance, __CLASS__ ) ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	private function __construct() {
	}

	public function sort_callback( $a, $b ) {
		if ( $a == $b ) {
			return 0;
		}
		return ( $a < $b ) ? 1 : -1;
	}

}
