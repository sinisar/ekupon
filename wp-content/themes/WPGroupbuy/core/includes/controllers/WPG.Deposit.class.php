<?php
	
/**
 * Deposit function
 */
class WP_Groupbuy_Deposit extends WP_Groupbuy_Controller {

	public static function init() {
		add_filter('wg_addons', array(get_class(),'wg_addon'), 10, 1);
	}

	public static function wg_addon( $addons ) {
		$addons['cashback_rewards'] = array(
			'label' => self::__('Rewards Purchase'),
			'description' => self::__('Allows for a deal to include rewards points (aka credits) as part of the purchase.'),
			'files' => array(
				__FILE__,
			),
			'callbacks' => array(
				array('WP_Groupbuy_Credit_Purchase', 'init'),
			),
			'active' => TRUE,
		);
		return $addons;
	}

}

class WP_Groupbuy_Credit_Purchase extends WP_Groupbuy_Controller {

	private static $meta_keys = array(
		'reward' => '_wpg_credit_purchase_option', // bool
		// 'qty_option' => '_wpg_credit_purchase_option_quanity', // int
	);
	
	public static function init() {

		// Hook into purchases and provide credits based on deal
		add_action( 'payment_complete', array( get_class(), 'apply_rewards' ), 10, 1 ); // Do the dirty work
		
		
		// Meta Boxes
		add_action( 'add_meta_boxes', array(get_class(), 'add_meta_boxes'));
		add_action( 'save_post', array( get_class(), 'save_meta_boxes' ), 10, 2 );
	}

	/**
	 * Give credits to deserving users when a purchase is completed
	 */
	public static function apply_rewards( WP_Groupbuy_Payment $payment ) {
		$account = $payment->get_account();

		$credits = 0;
		$purchase_id = $payment->get_purchase();
		$purchase = WP_Groupbuy_Purchase::get_instance($purchase_id);
		$products = $purchase->get_products();
		foreach ( $products as $product ) {
			$deal_id = (int) $product['deal_id'];
			$deal = WP_Groupbuy_Deal::get_instance( $deal_id );
			$reward = self::get_reward($deal);
			// $multiple = self::get_reward_qty_option($deal); // unnecessary since the admin can limit the purchase quantity to a single user.
			
			if ( $reward >= 0.01 ) {
				$calc_reward = $reward*$product['quantity'];
				$credits += $calc_reward;
			}
		}
		$account->add_credit($credits, WP_Groupbuy_Affiliates::CREDIT_TYPE);
		self::reward_record($account,$payment->get_ID(),$credits,WP_Groupbuy_Affiliates::CREDIT_TYPE);
		do_action('wg_apply_credit_purchase',$account,$payment,$credits,WP_Groupbuy_Affiliates::CREDIT_TYPE);

	}

	public static function reward_record( $account, $payment_id, $credits, $type ) {
		$account_id = $account->get_ID();
		$balance = $account->get_credit_balance($type);
		$data = array();
		$data['account_id'] = $account_id;
		$data['payment_id'] = $payment_id;
		$data['credits'] = $credits;
		$data['type'] = $type;
		$data['current_total_'.$type] = $balance;
		$data['change_'.$type] = $credits;
		WP_Groupbuy_Records::new_record(self::__('Rewards Purchase'), WP_Groupbuy_Accounts::$record_type, self::__('Credits/Rewards Purchase'), 1, $account_id, $data);
	}

	public static function add_meta_boxes() {
		add_meta_box('credit_purchase', self::__('Rewards Purchase'), array(get_class(), 'show_meta_boxes'), WP_Groupbuy_Deal::POST_TYPE, 'side');
	}

	public static function show_meta_boxes( $post, $metabox ) {
		switch ( $metabox['id'] ) {
			case 'credit_purchase':
				$deal = WP_Groupbuy_Deal::get_instance($post->ID);
				self::show_meta_box($deal, $post, $metabox);
				break;
			default:
				self::unknown_meta_box($metabox['id']);
				break;
		}
	}

	private static function show_meta_box( WP_Groupbuy_Deal $deal, $post, $metabox ) {
		$reward = self::get_reward($deal);
		// $qty_option = self::get_reward_qty_option($deal);
		include WG_PATH.'/templates/meta_boxes/account-deposit.php';
	}
	
	public static function save_meta_boxes( $post_id, $post ) {
		// only continue if it's an account post
		if ( $post->post_type != WP_Groupbuy_Deal::POST_TYPE ) {
			return;
		}
		// don't do anything on autosave, auto-draft, bulk edit, or quick edit
		if ( wp_is_post_autosave( $post_id ) || $post->post_status == 'auto-draft' || defined('DOING_AJAX') || isset($_GET['bulk_edit']) ) {
			return;
		}
		// save all the meta boxes
		$deal = WP_Groupbuy_Deal::get_instance($post_id);
		if ( !is_a($deal, 'WP_Groupbuy_Deal') ) {
			return; // The account doesn't exist
		}
		self::save_meta_box($deal, $post_id, $post);
	}

	private static function save_meta_box( WP_Groupbuy_Deal $deal, $post_id, $post ) {
		
		/*/
		self::set_reward_qty_option($deal, 0);
		if ( isset($_POST['credit_purchase_qty_option']) && 'TRUE' == $_POST['credit_purchase_qty_option'] ) {
			self::set_reward_qty_option($deal, 1);
		}
		/**/

		if ( isset($_POST['purchase_reward']) && (int)$_POST['purchase_reward'] > 0 ) {
			$reward = (int)$_POST['purchase_reward'];
			self::set_reward($deal, $reward);
		}
		
	}


	public function get_reward( WP_Groupbuy_Deal $deal ) {
		return $deal->get_post_meta(self::$meta_keys['reward']);
	}
	public function set_reward( WP_Groupbuy_Deal $deal, $reward ) {
		return $deal->save_post_meta(array(self::$meta_keys['reward'] => $reward));
	}

	/*/
	public function get_reward_qty_option( WP_Groupbuy_Deal $deal ) {
		return $deal->get_post_meta(self::$meta_keys['qty_option']);
	}
	public function set_reward_qty_option( WP_Groupbuy_Deal $deal, $qty_option ) {
		return $deal->save_post_meta(array(self::$meta_keys['qty_option'] => $qty_option));
	}
	/**/
}