<?php

class WP_Groupbuy_Accounts_Import_Users extends WP_Groupbuy_Controller
{
    const IMPORT_USERS_PATH_OPTION = 'wg_account_import_users_path';
    const IMPORT_USERS_QUERY_VAR = 'wg_account_import_users';
    const FORM_ACTION = 'wg_account_import_users';
    private static $import_users_path = 'account/import_users';
    private static $instance;
    private static $default_mailing_list;
    private static $defaul_log_file_path;

    public static function init()
    {
        //echo "Inside init import users <br/>";
        self::$import_users_path = get_option(self::IMPORT_USERS_PATH_OPTION, self::$import_users_path);
        self::register_path_callback(self::$import_users_path, array(get_class(), 'on_import_users_page'), self::IMPORT_USERS_QUERY_VAR, 'account/import_users');
        add_action('admin_init', array(get_class(), 'register_settings_fields'), 10, 1);
    }

    public static function register_settings_fields()
    {
        $page = WP_Groupbuy_UI::get_settings_page();
        $section = 'WG_URL_paths';
        // Settings
        register_setting($page, self::IMPORT_USERS_PATH_OPTION);
        add_settings_field(self::IMPORT_USERS_PATH_OPTION, self::__('Account Import User Path'), array(get_class(), 'display_account_import_users_path'), $page, $section);
    }

    public static function display_account_import_users_path()
    {
        echo trailingslashit(get_home_url()) . ' <input type="text" name="' . self::IMPORT_USERS_PATH_OPTION . '" id="' . self::IMPORT_USERS_PATH_OPTION . '" value="' . esc_attr(self::$import_users_path) . '" size="40"/><br />';
    }


    public static function on_import_users_page()
    {
        self::login_required();
        self::get_instance(); // make sure the class is instantiated
    }

    private function __clone()
    {
        trigger_error(__CLASS__ . ' may not be cloned', E_USER_ERROR);
    }

    private function __sleep()
    {
        trigger_error(__CLASS__ . ' may not be serialized', E_USER_ERROR);
    }

    public static function get_instance()
    {
        if (!(self::$instance && is_a(self::$instance, __CLASS__))) {
            self::$instance = new self();
        }
        return self::$instance;
    }


    private function __construct()
    {
        //echo "inside __construct.<br/>";
        self::do_not_cache(); // never cache the account pages
        add_filter('wg_validate_account_edit_form', array($this, 'validate_account_fields'), 0, 2);
        if (isset($_POST['wg_account_action']) && $_POST['wg_account_action'] == self::FORM_ACTION) {
            $this->process_form_submission();
        }
        add_action('pre_get_posts', array($this, 'edit_query'), 10, 1);
        add_action('the_post', array($this, 'view_profile_form'), 10, 1);
        add_filter('the_title', array($this, 'get_title'), 10, 2);
        add_filter('wg_account_import_users_panes', array($this, 'get_panes'), 0, 2);
    }


    public function edit_query(WP_Query $query)
    {
        if (isset($query->query_vars[self::IMPORT_USERS_QUERY_VAR]) && $query->query_vars[self::IMPORT_USERS_QUERY_VAR]) {
            $query->query_vars['post_type'] = WP_Groupbuy_Account::POST_TYPE;
            $query->query_vars['p'] = WP_Groupbuy_Account::get_account_id_for_user();
        }
    }

    //	helper function for logging purposes - enable error_log in php.ini
    function log_message($message)
    {
        //	TODO: +2h
        if (isset(self::$defaul_log_file_path)) {
            $pre_message = "[";
            $pre_message .= date('d-m-Y H:i:s');
            $pre_message .= "] - ";
            if (strpos($message, '\n') !== false) {
                $post_message = "";
            } else {
                $post_message = "\n";
            }
            $log_message = $pre_message . $message . $post_message;
            error_log($log_message, 3, self::$defaul_log_file_path);
        }
    }

    private function process_form_submission()
    {
        //global $default_mailing_list;
        $import_file = isset($_POST['wg_import_users_import_file']) ? $_POST['wg_import_users_import_file'] : '';
        $nr_of_records_from = isset($_POST['wg_import_users_nr_of_records_from']) ? $_POST['wg_import_users_nr_of_records_from'] : 0;
        $nr_of_records_to = isset($_POST['wg_import_users_nr_of_records_to']) ? $_POST['wg_import_users_nr_of_records_to'] : 0;
        //echo "from: ".$nr_of_records_from;
        //echo "to: ".$nr_of_records_to;

        self::$default_mailing_list = isset($_POST['wg_import_users_mailinglists']) ? $_POST['wg_import_users_mailinglists'] : array();
        self::$defaul_log_file_path = isset($_POST['wg_import_users_log_file_path']) ? $_POST['wg_import_users_log_file_path'] : '';

        //$this->log_message("mytestmsg");
        //  echo "default mailinglists: ";
        //  print_r(self::$default_mailing_list);
        //echo "<br/>";
        //echo "import users file path: ";
        //print_r(self::$defaul_log_file_path);

        //	echo "import_file:" .$import_file."<br/>";
        //echo "inside process form submission.";
        $counter_success = 0;
        $counter_failed = 0;
        $processed_items_counter = 0;
        if ($this->has_files_to_upload('wg_import_users_import_file')) {
            //	echo "file for upload;";
            if (isset($_FILES['wg_import_users_import_file'])) {
                //	upload the file:
                //$file = wp_upload_bits($_FILES['wg_import_users_import_file']['name'], null, @file_get_contents($_FILES['wg_import_users_import_file']['tmp_name']));
                //echo "file:" .$file['file']."<br/>";
                //  $file = array("file" => "/Applications/XAMPP/xamppfiles/htdocs/izkoristime/wp-content/uploads/import_users.txt");
                //  $file = array("file" => "/Applications/XAMPP/xamppfiles/htdocs/izkoristime/users_data/izkme_export_v4/output/generated_selected_users.txt");
                $file = array("file" => "/var/www/vhosts/izkoristi.me/httpdocs/wp-content/upgrade/import_users/merged_users.txt");
                $file_handle = fopen($file['file'], "r");

                while (!feof($file_handle)) {
                    $line = fgets($file_handle);
                    //echo "line: ".$line ."<br/>";
                    //$this->log_message("Starting with import of: " . $line);
                    //break;
                    if ($processed_items_counter >= $nr_of_records_from && $processed_items_counter <= $nr_of_records_to) {
                        $result = $this->import_user($line);
                        if ($result) {
                            $counter_success += 1;
                        } else {
                            $counter_failed += 1;
                        }
                        if ($processed_items_counter % 100 == 0) {
                            $this->log_message("Processed: " . $processed_items_counter . " users.");
                        }
                    }
                    $processed_items_counter += 1;

                }
                echo "Successfully inserted: " . $counter_success . " new users.<br/>";
                echo "Failed with insertion: " . $counter_failed . " of old users.";
                fclose($file_handle);
                //print_r($file);
            }

        } else {
            echo "not ...";
        }

        //  break;

    }

    function has_files_to_upload($id)
    {
        return (!empty($_FILES)) && isset($_FILES[$id]);
    }

    private function import_user($exiting_user_string)
    {
        $log_message = "";
        $existing_user_array = explode(';;', $exiting_user_string);
        //print_r($existing_user_array);
        //break;
        if (count($existing_user_array) < 15 && count($existing_user_array) > 22) {
            $log_exiting_user_array = print_r($existing_user_array, true);
            $log_message .= "Data corupted: " . $log_exiting_user_array;
            $this->log_message($log_message);

            return false;
        }
        //$existing_user_array = $this->clean_apostrophes($existing_user_array);

        //echo "printing existing array: ";
        //print_r($existing_user_array);
        //echo "end <br/>";
        //break;
        //echo "<br>";
        //break;
        // Array (
        // [0] => 'branka.tocnoto@gmail.com'
        // [1] => '872fc58a0eb06286917d89cddd587f0a'
        // [2] => 'Ki#'
        // [3] => NULL
        // [4] => 'administrator'
        // [5] => 1324815064
        // [6] => 'Branka gmail'
        // [7] => 'TOčnoTO'
        // [8] => NULL
        // [9] => 1
        // [10] => 'Erjavčeva 8a 1233 DOB'
        // [11] => NULL
        // [12] => 1 )

        $user_email = $existing_user_array[1];
        $user_data_array = array(
            "email" => $existing_user_array[1],
            "password" => $existing_user_array[2],
            "role" => $this->get_user_mapped_role($existing_user_array[5]),
            "created" => $existing_user_array[6],
        );

        $user_meta_array = array(
            '_user_salt' => $existing_user_array[3],
            'first_name' => $existing_user_array[7],
            'last_name' => (string)$existing_user_array[8],
        );

        //	TODO: role => im_capabilities - by default => subscriber usermeta->im_capabilities
        $user_postmeta_array = array(
            "email" => $existing_user_array[1],
            //'role' => $existing_user_array[5],
            'firstName' => $existing_user_array[7],
            'lastName' => $existing_user_array[8],
            'addrress' => $existing_user_array[11],
            'city' => $existing_user_array[12],
        );

        //  13995;;info@zavodgrai.si;;cb7c7a9ee6db2edf505a164204d5c7a8;;k3#;;0;;company;;1474016516;;Zavod Grai - Te��aj ru����ine;;;;0;;1.0;;;;;;0;;
        //  Zavod Grai - Tečaj ruščine;;http://www.zavodgrai.si/;;4;;1000;;Dimičeva ulica 13;;1;;

        /*
            'contact_title' => '_contact_title',    => 15
            'contact_name' => '_contact_name',  => 15
            'contact_street' => '_contact_street',  => 19
            'contact_city' => '_contact_city',  => 19 TODO: from post_code to city ?
            'contact_state' => '_contact_state',
            'contact_postal_code' => '_contact_postal_code',    => 19
            'contact_country' => '_contact_country',
            'contact_phone' => '_contact_phone',
            'website' => '_website',    => 16
            'facebook' => '_facebook',
            'twitter' => '_twitter',
            'opening_hours' => '_opening_hours',
            'map_width' => '_map_width',
            'map_height' => '_map_weight',
            'map_zoom' => '_map_zoom',
            'map_type' => '_map_type',
            'map_info_window' => '_map_info_window',
         */
    $user_merchant_array = array();
    if(count($existing_user_array) >= 15) {
        //  merchant extra data
        $merchant_contact_title = "";
        if($existing_user_array[15] !="") {
            $merchant_contact_title = $existing_user_array[15];
        }

        $merchant_contact_name = "";
        if($existing_user_array[15] !="") {
            $merchant_contact_name = $existing_user_array[15];
        }

        $merchant_contact_street = "";
        if($existing_user_array[19] !="") {
            $merchant_contact_street = $existing_user_array[19];
        }

        $merchant_contact_postal_code = "";
        if($existing_user_array[18] !="") {
            $merchant_contact_postal_code = $existing_user_array[18];
        }

        $merchant_website = "";
        if($existing_user_array[16] !="") {
            $merchant_website = $existing_user_array[16];
        }

        $user_merchant_array = array(
            'contact_title' => $merchant_contact_title,
            'contact_name' => $merchant_contact_name,
            'contact_street' => $merchant_contact_street,
            'contact_city' => "",
            'contact_state' => "",
            'contact_postal_code' => $merchant_contact_postal_code,
            'contact_country' => "",
            'contact_phone' => "",
            'website' => $merchant_website,
            'facebook' => "",
            'twitter' => "",
            'opening_hours' => "",
            'map_width' => "",
            'map_height' => "",
            'map_zoom' => "",
            'map_type' => "",
            'map_info_window' => "",
        );
    }

        //	create user
        $user_id = $this->create_user($user_data_array);
        //echo $user_id." aaa";
        if ($user_id == null) {
            $log_user_data_array = print_r($user_data_array, true);
            $log_message .= "User, with data: " . $log_user_data_array . " NOT created;";
            $this->log_message($log_message);

            return false;
        }

        //	create options (salt)
        $create_user_meta = $this->create_user_meta($user_id, $user_meta_array);
        if (!$create_user_meta) {
            $log_user_meta_array = print_r($user_meta_array, true);
            $log_message .= "Failed while creating user metadata for user: " . $user_id . " and metadata_array: " . $log_user_meta_array . ";";
            $this->log_message($log_message);

            return false;
        }

        //	create user postmeta data
        $create_user_postmeta = $this->create_user_postmeta($user_id, $user_postmeta_array);
        if (!$create_user_postmeta) {
            $log_user_postmeta_array = print_r($user_postmeta_array, true);
            $log_message .= "Failed while creating user postmeta data for user: " . $user_id . " and postmeta_array: " . $log_user_postmeta_array . ";";
            $this->log_message($log_message);

            return false;
        }

        //	create default subscription
        //print "before creation: ";
        //print_r(self::$default_mailing_list);
        $create_user_subscription = $this->create_default_subscription($user_id, $user_email);
        if (!$create_user_subscription) {
            $log_message .= "Failed while creating user default subscription for user: " . $user_id . " and mailing lists: " . print_r(self::$default_mailing_list, true) . ";";
            $this->log_message($log_message);

            return false;
        }

        // create merchant if role = company
        if($existing_user_array[5] == "company") {
            $create_merchant = $this->create_merchant($user_id, $user_merchant_array);
            if (!$create_merchant) {
                $log_message .= "Failed to create merchant for user: " . $user_id . " with data: " . print_r($user_merchant_array, true) . ";";
                $this->log_message($log_message);

                return false;
            }
        }

        //  $log_message .= "User with id: ".$user_id." created successfully.";
        //  $this->log_message($log_message);
        //break;
        return true;

        //break;
        //	create post
        //	create post_meta
        /*
        (
        `id` => im_user.id
        `email`, => im_user.email
        `password`, => im_user.password
        `salt`, => NULL
        `changePassword`, => NULL
        `role`, => im_usermeta.im_capabilities (default = subscriber)
        `created`, => im_user.user_registrated
        `firstName`, => im_usermeta._first_name
        `lastName`,=> im_usermeta._last_name
        `avatar`, => NULL
        `timeZone`, => NULL
        `address`, => im_postmeta._address
        `city`, => im_postmeta._address
        `newsletter` => im_wpmlsubscribers
                   => im_wpmlsubscriberslists (default = osrednjeslovenska)
        )
*/
    }

    //	role mapper from old portal to new
    private function get_user_mapped_role($import_user_role)
    {
        if ($import_user_role == 'user') {
            return 'subscriber';
        }
        if ($import_user_role == 'company') {
            return 'merchant';
        }
    }

    //	remove all apostrophes befor import process starts
    private function clean_apostrophes($existing_user_array)
    {
        $cleaned_array = array();
        foreach ($existing_user_array as $exisiting_user_item) {
            $item = str_replace("'", "", $exisiting_user_item);
            $item = str_replace("\"", "", $item);
            $cleaned_array[] = $item;
        }
        return $cleaned_array;
    }

    //	create wp_user from input data
    private function create_user($user_data_array)
    {
        //echo "inside";
        // TODO: fix FUCKING nickname, display_name !
        $username = "";
        $email_parts = explode("@", $user_data_array['email']);
        if (count($email_parts) > 0) {
            $username = $email_parts[0];
        }
        if ($username == "") {
            $this->log_message("An error occured while parsing username from mail: " . $user_data_array['email']);
            return null;
        }
        $userdata = array(
            'user_email' => $user_data_array['email'],
            'user_login' => $user_data_array['email'],
            'user_nicename' => $username,
            'display_name' => $username,
            'user_pass' => $user_data_array['password'],
            'user_registrated' => $user_data_array['created'],
            'role' => $user_data_array['role']
        );
        //print_r($userdata);
        //echo "before create;";
        $user_id = wp_insert_user($userdata);
        //echo "postcreate; ".$user_id;

        //On success
        if (!is_wp_error($user_id)) {
            //echo "user with id: ".$user_id."created<br/>";
            return $user_id;
        } else {
            //echo "userid: ".$user_id;
            //echo "user not created ";
            //print_r($userdata);
            //echo "<br/>";
        }
        return null;
    }

    //	create additional usermeta from input data
    private function create_user_meta($user_id, $user_meta_array)
    {
        $meta_created = 0;
        foreach ($user_meta_array as $meta_key => $meta_value) {
            //	update existing (auto created meta keys)
            //if ($meta_key == 'first_name' || $meta_key == 'last_name') {
                if($meta_value != '') {
                    //print "user meta for: ". $meta_key. " and value: ".$meta_value. " :" . get_user_meta($user_id, $meta_key, true) . " end. <br>";
                    //if (get_user_meta($user_id, $meta_key, true) == null) {
                        if(update_user_meta($user_id, $meta_key, $meta_value) == false) {
                            $this->log_message("An error occured while adding user meta for key: ".$meta_key. " and value: ". $meta_value);
                        }
                        //else {
                         //   print "added new meta<br>";
                        //}
                    //} else {
                     //   update_user_meta($user_id, $meta_key, $meta_value);
                     //   print "updated<br>";
                    //}
                }
                /*
                else {
                    print "meta for key: ".$meta_key." not set.<br>";
                }
            } else { // else add new ones ...
        update_user_meta($user_id, $meta_key, $meta_value);
            }
*/
            $meta_created += 1;
        }
        if ($meta_created == count($user_meta_array)) {
            return true;
        }
        return false;
    }

    //	create additional postmeta  from input data
    private function create_user_postmeta($user_id, $user_postmeta_array)
    {
        // Set contact info for the new account
        $account = WP_Groupbuy_Account::get_instance($user_id);
        if (is_a($account, 'WP_Groupbuy_Account')) {
            $first_name = isset($user_postmeta_array['firstName']) ? $user_postmeta_array['firstName'] : '';
            $account->set_name('first', $first_name);

            $last_name = isset($user_postmeta_array['lastName']) ? $user_postmeta_array['lastName'] : '';
            $account->set_name('last', $last_name);

            $account_email = isset($user_postmeta_array['email']) ? $user_postmeta_array['email'] : '';
            $account->set_name('email', $account_email);
            $address = array(
                'street' => isset($user_postmeta_array['address']) ? $user_postmeta_array['address'] : '',
                'city' => isset($user_postmeta_array['city']) ? $user_postmeta_array['city'] : '',
            );
            $account->set_address($address);
            return true;
        }

        return false;
    }

    //	add user do default mailinglists
    private function create_default_subscription($user_id, $user_email)
    {
        /// TODO: load list of default mailinglists from admin console for FB and twitter or FORM.
        //echo "default mailinglists: <br/>";
        //  print_r(self::$default_mailing_list);

        $nr_of_subscribers = 0;
        $nr_of_subscription_list = 0;
        $nr_of_subscriptions = 0;

        
        //  one subscriber, multiple lists ...
        $data = array();
        $data['registered'] = 'Y';
        $data['user_id'] = $user_id;
        $data['email'] = $user_email;

        $subscribers = new wpmlSubscriber($data);
        //echo "subscriber: ";
        $subscription_result = $subscribers->save($data, false, false);
        //echo "subscriber result: ".$subscription_result;
        if ($subscription_result) {
            $nr_of_subscribers += 1;
        }
        //  print_r($subscribers->get_by_email($user_email));
        //echo "<br/>";
        $subscriber = $subscribers->get_by_email($user_email);
        //print "aaaaaa<br>";
        $subscriber_id = $subscriber[0]->id;

        foreach (self::$default_mailing_list as $nr => $id) {
            
            $data = array();
            $data['active'] = 'Y';
            $data['list_id'] = $id;
            $data['subscriber_id'] = $subscriber_id;

            $subscribersList = new wpmlSubscribersList($data);
            $subscribersList_result = $subscribersList->save($data, true, false);
            if ($subscribersList_result == true) {
                $nr_of_subscription_list += 1;
            }
            //echo "$nr_of_subscription_list: ".$nr_of_subscription_list;

            $nr_of_subscriptions += 1;
            //echo "subscriberList_result: ".$nr_of_subscriptions;
        }

        if ($nr_of_subscriptions == count(self::$default_mailing_list)) {
            //print "Count match!<br>";
            //if ($nr_of_subscribers == $nr_of_subscription_list) {
                if ($nr_of_subscriptions == $nr_of_subscription_list) {
                    return true;
                }
                /*
                else {
                    print "nr_of_subscribers $nr_of_subscribers and nr_of_subscriptions $nr_of_subscriptions don't match.";
                }*/
            //}
            //else {
            //    print "nr_of_subscribers $nr_of_subscribers and nr_of_subscription_list $nr_of_subscription_list don't match.";
            //}
        }
        /*
        else {
            print "nr_of_subscribers $nr_of_subscribers and nr of default mailing list don't match.";

        }
*/
        return false;
    }

    public function create_merchant($user_id, $user_merchant_array) {
        //  create new post type = Merchant, with defautl values for post title an description
        $merchant_contact_title = isset($user_merchant_array['contact_title'])? $user_merchant_array['contact_title'] : 'Merchant'.$user_id;
        $post_id = wp_insert_post( array(
            'post_status' => 'publish',
            'post_type' => WP_Groupbuy_Merchant::POST_TYPE,
            'post_title' => $merchant_contact_title,
            'post_content' => $merchant_contact_title.' (merchant for user: '.$user_id.')'
        ) );
        $merchant = WP_Groupbuy_Merchant::get_instance( $post_id );
        if (is_a($merchant, 'WP_Groupbuy_Merchant')) {
/*
            'contact_title' => $merchant_contact_title,
            'contact_name' => $merchant_contact_name,
            'contact_street' => $merchant_contact_street,
            'contact_postal_code' => $merchant_contact_postal_code,
            'website' => $merchant_website,
 */
            $merchant->set_contact_title($merchant_contact_title);

            $merchant_contact_name = isset($user_merchant_array['contact_name'])? $user_merchant_array['contact_name'] : '';
            $merchant->set_contact_name($merchant_contact_name);

            $merchant_contact_street = isset($user_merchant_array['contact_street'])? $user_merchant_array['contact_street'] : '';
            $merchant->set_contact_street($merchant_contact_street);

            $merchant_contact_postal_code = isset($user_merchant_array['contact_postal_code'])? $user_merchant_array['contact_postal_code'] : '';
            $merchant->set_contact_postal_code($merchant_contact_postal_code);

            $merchant_website = isset($user_merchant_array['website'])? $user_merchant_array['website'] : '';
            $merchant->set_website($merchant_website);

            //  authorize user
            $merchant->authorize_user($user_id);

            return true;
        }
        else {
            print "Not an instance of Merchant.";
        }


            return false;
    }


    public function get_panes(array $panes)
    {

        $panes['import_users'] = array(
            'weight' => 0,
            'body' => self::load_view_to_string('account/import_users', array('fields' => $this->account_import_users_fields())),
        );
        $panes['controls'] = array(
            'weight' => 1000,
            'body' => self::load_view_to_string('account/import-controls', array()),
        );
        return $panes;
    }


    private function account_import_users_fields()
    {
        $fields = array(
            'import_file' => array(
                'weight' => 0,
                'label' => self::__('User file: '),
                'type' => 'file',
                'required' => TRUE,
                'default' => '',
            ),
            'nr_of_records_from' => array(
                'weight' => 10,
                'label' => self::__('Records from:'),
                'type' => 'text',
                'required' => TRUE,
                'default' => '',
            ),
            'nr_of_records_to' => array(
                'weight' => 20,
                'label' => self::__('Records to:'),
                'type' => 'text',
                'required' => TRUE,
                'default' => '',
            ),
            'mailinglists[]' => array(
                'weight' => 30,
                'label' => self::__('Mailing lists'),
                'type' => 'checkboxes',
                'options' => get_all_mailinglist_as_array(),
            ),
            'log_file_path' => array(
                'weight' => 40,
                'label' => self::__('Log file path:'),
                'type' => 'text',
                'required' => TRUE,
                'default' => '',
            ),
        );
        $fields = apply_filters('wg_account_import_users_fields', $fields);
        uasort($fields, array(get_class(), 'sort_by_weight'));
        return $fields;
    }

    public static function get_url()
    {
        if (self::using_permalinks()) {
            return trailingslashit(home_url()) . trailingslashit(self::$import_users_path);
        } else {
            return add_query_arg(self::IMPORT_USERS_QUERY_VAR, 1, home_url());
        }
    }


    // Update the global $pages array with the HTML for the page.
    public function view_profile_form($post)
    {
        if ($post->post_type == WP_Groupbuy_Account::POST_TYPE) {
            remove_filter('the_content', 'wpautop');
            $account = WP_Groupbuy_Account::get_instance();
            $panes = apply_filters('wg_account_import_users_panes', array(), $account);
            uasort($panes, array(get_class(), 'sort_by_weight'));
            $view = self::load_view_to_string('account/import', array(
                'panes' => $panes,
            ));
            global $pages;
            $pages = array($view);
        }
    }

    // Filter 'the_title' to display the title of the page rather than the user name
    public function get_title($title, $post_id)
    {
        $post = &get_post($post_id);
        if ($post->post_type == WP_Groupbuy_Account::POST_TYPE) {
            $user = wp_get_current_user();
            if ($user->display_name) {
                $name = $user->display_name;
            } else {
                $name = $user->user_login;
            }
            return sprintf(self::__('Importing users'), $name);
        }
        return $title;
    }
}