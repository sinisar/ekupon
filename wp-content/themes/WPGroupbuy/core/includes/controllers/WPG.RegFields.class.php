<?php

add_action( 'wg_init_controllers', array( 'WP_Groupbuy_Registration_Fields', 'init' ), 135 );

class WP_Groupbuy_Registration_Fields extends WP_Groupbuy_Controller {
	public static function init() {
		add_filter('wg_addons', array(get_class(),'wg_addon'), 10, 1);
	}
	public static function wg_addon( $addons ) {
		$addons['registration_fields'] = array(
		'label' => self::__('Additional Registration Field'),
		'description' => self::__( 'Add your own field to registration field. It\'s for developer only. Please check wiki website to get more information about how to add more fields.' ),
		'files' => array(
		__FILE__,
		dirname( __FILE__ ) . '/lib/registration/template_tags.php',
		),
		'callbacks' => array(
		array('Registration_Fields', 'init'),
		),
		);
		return $addons;
	}
}
class Registration_Fields extends WP_Groupbuy_Controller {
	
	// List all of your field IDs here as constants
	const SOURCE = 'wg_account_fields_source';
	public static function init() {
		
		// registration hooks
		add_filter('wg_account_registration_panes', array(get_class(), 'get_registration_panes'),100);
		add_filter('wg_validate_account_registration', array(get_class(), 'validate_account_fields'), 10, 4);
		add_action('wg_registration', array(get_class(), 'process_registration'), 50, 5);
		
		// Add the options to the account edit screens
		add_filter('wg_account_edit_panes', array(get_class(), 'get_edit_fields'), 0, 2);
		add_action('wg_process_account_edit_form',array(get_class(), 'process_edit_account'));
		
		// Hook into the reports
		add_filter('set_deal_purchase_report_data_column', array(get_class(), 'reports_columns'), 10, 2);
		add_filter('set_merchant_purchase_report_column', array(get_class(), 'reports_columns'), 10, 2);
		add_filter('set_accounts_report_data_column', array(get_class(), 'reports_columns'), 10, 2);
		add_filter('wg_deal_purchase_record_item', array(get_class(), 'reports_record'), 10, 3);
		add_filter('wg_merch_purchase_record_item', array(get_class(), 'reports_record'), 10, 3);
		add_filter('wg_accounts_record_item', array(get_class(), 'reports_account_record'), 10, 3);
		
	}
	
	/**
	* Add the report coloumns.
	*/
	public function reports_columns( $columns )
	{
		// Add as many as you want with their own key that will be used later.
		$columns['rf_referred_by'] = self::__('Referred By');
		return $columns;
	}
	
	/**
	* Add the report record for deal purchase and merchant report.
	*/
	public function reports_record( $array, $purchase, $account )
	{
		if ( !is_a($account,'WP_Groupbuy_Account')) {
			return $array;
		}
		// Add as many as you want with their own matching key from the reports_column
		$array['rf_referred_by'] = get_post_meta( $account->get_ID(), '_'.self::SOURCE, TRUE );
		return $array;
	}
	
	/**
	* Add the report record for account report
	*/
	public function reports_account_record( $array, $account )
	{
		// Add as many as you want with their own matching key from the reports_column
		$array['rf_referred_by'] = get_post_meta( $account->get_ID(), '_'.self::SOURCE, TRUE );
		return $array;
	}
	
	/**
	* Hook into the process registration action
	*/
	public function process_registration( $user = null, $user_login = null, $user_email = null, $password = null, $post = null ) {
		$account = WP_Groupbuy_Account::get_instance($user->ID);
		self::process_form($account);
	}
	
	/**
	* Hook into the process edit account action
	*/
	public static function process_edit_account( WP_Groupbuy_Account $account ) {
		// using the single callback below
		self::process_form($account);
	}
	
	/**
	* Process the form submission and save the meta
	*/
	public static function process_form( WP_Groupbuy_Account $account ) {
		// Copy all of the new fields below, copy the below if it's a basic field.
		if ( isset($_POST[self::SOURCE]) && $_POST[self::SOURCE] != '' ) {
			delete_post_meta( $account->get_ID(), '_'.self::SOURCE );
			add_post_meta( $account->get_ID(), '_'.self::SOURCE, $_POST[self::SOURCE] );
		}
		// Below is a commented out process to uploaded images
		/*/
		if ( !empty($_FILES[self::UPLOAD]) ) {
			// Set the uploaded field as an attachment
			self::set_attachement( $account->get_ID(), $_FILES );
		}
		/*/
	}
	
	/**
	* Add a file as a post attachment.
	*/
	public static function set_attachement( $post_id, $files ) {
		if (!function_exists('wp_generate_attachment_metadata')){
			require_once(ABSPATH . 'wp-admin' . '/includes/image.php');
			require_once(ABSPATH . 'wp-admin' . '/includes/file.php');
			require_once(ABSPATH . 'wp-admin' . '/includes/media.php');
		}
		foreach ($files as $file => $array) {
			if ($files[$file]['error'] !== UPLOAD_ERR_OK) {
				self::set_message('upload error : ' . $files[$file]['error']);
			}
			$attach_id = media_handle_upload( $file, $post_id );
		}
		// Make it a thumbnail while we're at it.
		if ($attach_id > 0){
			update_post_meta($post_id,'_thumbnail_id',$attach_id);
		}
		return $attach_id;
	}
	
	/**
	* Validate the form submitted
	*/
	public function validate_account_fields( $errors, $username, $email_address, $post ) {
		// If the field is required it should 
		if ( isset($post[self::SOURCE]) && $post[self::SOURCE] == '' ) {
			$errors[] = self::__('"Referred By" is required.');
		}
		return $errors;
	}
	/**
	* Add the default pane to the account edit form
	*/
	public function get_registration_panes( array $panes ) {
		$panes['custom_fields'] = array(
		'weight' => 10,
		'body' => self::rf_load_view_string('panes', array( 'fields' => self::fields() )),
		);
		return $panes;
	}
	
	/**
	* Add the fields to the registration form
	*/
	private function fields( $account = NULL ) {
		$fields = array(
		'source' => array(
		'weight' => 0, // sort order
		'label' => self::__('Referred By'), // the label of the field
		'type' => 'text', // type of field (e.g. text, textarea, checkbox, etc. )
		'required' => TRUE, // If this is false then don't validate the post in validate_account_fields
		),
		// add new fields here within the current array.
		);
		$fields = apply_filters('custom_registration_fields', $fields);
		return $fields;
	}
	/**
	* Add the default pane to the account edit form
	*/
	public function get_edit_fields( array $panes, WP_Groupbuy_Account $account ) {
		$panes['custom'] = array(
		'weight' => 99,
		'body' => self::rf_load_view_string('panes', array( 'fields' => self::edit_fields($account) )),
		);
		return $panes;
	}
	
	/**
	* Add the fields to the account form
	*/
	private function edit_fields( $account = NULL ) {
		$fields = array(
		'source' => array(
		'weight' => 0, // sort order
		'label' => self::__('Referred By'), // the label of the field
		'type' => 'text', // type of field (e.g. text, textarea, checkbox, etc. )
		'required' => TRUE, // If this is false then don't validate the post in validate_account_fields
		'default' => $custom // the default value
		),
		// add new fields here within the current array.
		);
		uasort($fields, array(get_class(), 'sort_by_weight'));
		$fields = apply_filters('invite_only_fields', $fields);
		return $fields;
	}
	
	/**
	* return a view as a string.
	*/
	private static function rf_load_view_string( $path, $args ) {
		ob_start();
		if (!empty($args)) extract($args);
		@include ('registration/views/'.$path.'.php');
		return ob_get_clean();
	}
}