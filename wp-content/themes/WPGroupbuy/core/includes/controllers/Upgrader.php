<?php
class WPG_Upgrader {

	protected $name;
	
	protected $message;

	protected static $instances = array();
	protected static $current_version;
	protected static $versions = array(
		2.2,
		2.3,
	);

	public function __construct() {
		$this->name = '<b>' . str_replace( '_', ' ', get_called_class() ) . '</b> ';
		/* Manual upgrade */
		if ( self::is_upgrading() ) {
			add_action( 'init', array( $this, 'hooks' ) );
			add_action( 'wg_upgrade', array( $this, 'upgrade' ) );
			add_action( 'wg_after_upgrade', array( $this, 'after_upgrade' ) );
			add_action( 'admin_notices', array( $this, 'after_upgrade_notice' ) );
		}

		$this->message = $this->name . __('successfully upgraded to new version.', 'wpgroupbuy' );

		self::$current_version = get_option( 'wg_current_version', 0 );

		wg_require_dir( WG_PATH.'/includes/upgraders/' );

		/* Auto upgrade */
		// Future
	}

	public function hooks() {
	}

	public function upgrade() {
		foreach ( self::$versions as $version ) {
			if ( $version > self::$current_version ) {
				$method_name = 'upgrade_to_' . str_replace( array( '.', ',' ), '_', $version );
				if ( method_exists( $this, $method_name ) ) {
					add_action( 'wg_upgrade_to_' . $version, array( $this, $method_name ) );
				}
			}
		}
	}

	public function after_upgrade() {
	}

	public function after_upgrade_notice() { ?>
		<div class="updated"><p><?php echo $this->message ?></p></div>
		<?php
	}

	public static function get_instance() {
		$class = get_called_class();
		if ( empty(static::$instances[$class]) ) {
			static::$instances[$class] = new static();
		}

		return static::$instances[$class];
	}

	public static function is_upgrading() {
		return !empty( $_REQUEST['wg_upgrade'] ) && is_admin();
	}

	public static function init() {
		$wp_upgrader = static::get_instance();
	}

	public static function upgrade_action() {
		do_action( 'wg_upgrade' );
		foreach ( self::$versions as $version ) {
			if ( $version > self::$current_version ) {
				do_action( 'wg_upgrade_to_' . $version );
			}
		}
		do_action( 'wg_after_upgrade' );
	}

	/* Clean up post meta table by removing orphaned records that has no valid post_id */

	public static function clean_postmeta() {
		global $wpdb;
		return $wpdb->query("DELETE FROM $wpdb->postmeta WHERE `post_id` NOT IN ( SELECT ID FROM $wpdb->posts );");
	}

}

add_action('wg_loaded', function() {
	WPG_Upgrader::init();
	if ( WPG_Upgrader::is_upgrading() ) {
		add_action( 'init', array( 'WPG_Upgrader', 'upgrade_action' ) );
	}
});
