<?php 

add_action( 'wg_init_controllers', array( 'WP_Groupbuy_Increase_Sale', 'init' ), 140 );

class WP_Groupbuy_Increase_Sale extends WP_Groupbuy_Controller {
	
	private static $instance;

	public static function init() {
		add_filter('wg_addons', array(get_class(),'wg_addon'), 10, 1);
	}
	public static function get_instance() {
		if ( !(self::$instance && is_a(self::$instance, __CLASS__)) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	public static function wg_addon( $addons ) {
		$addons['wg_increase_sale'] = array(
			'label' => self::__('Increases the total number of purchases shown for a deal'),
			'description' => self::__( 'It doesn\'t affect minimum purchases, purchase or voucher reports.' ),
			'files' => array(
				__FILE__,
			),
			'callbacks' => array(
				array('WPG_IncreaseSale', 'init'),
			),
		);
		return $addons;
	}

}

class WPG_IncreaseSale extends WP_Groupbuy_Controller {

	private static $meta_keys = array(
		'number_sales' => '_wpg_additional_sales', // int
		//'custom_field' => '_wpg_custom_field' // string
	);

	public static function init() {
		
		// Meta Boxes
		add_action( 'add_meta_boxes', array(get_class(), 'add_meta_boxes'));
		add_action( 'save_post', array( get_class(), 'save_meta_boxes' ), 10, 2 );
		
		//Show on front end
		add_filter('wg_get_number_of_purchases', array(get_class(), 'filter_add_to_number_of_purchases'), 11, 1); 
		
	}
	
	public static function filter_add_to_number_of_purchases( $sales ) {
		if ( !is_admin() ) {
			
			global $post;
			$post_id = $post->ID;
			
			$deal = WP_Groupbuy_Deal::get_instance( $post_id );
			if ( is_a( $deal, 'WP_Groupbuy_Deal' ) ) {
			
				$add_number_sales = 0;
				$deal = WP_Groupbuy_Deal::get_instance($post_id);
				if ($deal) {
					$add_number_sales = (int)self::get_number_sales($deal);
					$sales = $sales + $add_number_sales;
				}
			}
		}
		return $sales;
	}
	
	
	public static function add_to_number_of_purchases( $post_id ) {
		if ( !$post_id ) {
			global $post;
			$post_id = $post->ID;
		}
		
		if ( $post_id ) {
			$add_number_sales = 0;
			$deal = WP_Groupbuy_Deal::get_instance($post_id);
			if ($deal) {
				$add_number_sales = (int)self::get_number_sales($deal);
				return $add_number_sales;
			}
		}
		return 0;
	}
	
	public static function add_meta_boxes() {
		add_meta_box('wpg-increase-sale-fields', self::__('Increasing sale number'), array(get_class(), 'show_meta_boxes'), WP_Groupbuy_Deal::POST_TYPE, 'side', 'high');
	}
	
	
	public static function get_number_sales( WP_Groupbuy_Deal $deal ) {
		return $deal->get_post_meta(self::$meta_keys['number_sales']);
	}
	public static function set_number_sales( WP_Groupbuy_Deal $deal, $input_value ) {
		$input_value = (int)$input_value;
		return $deal->save_post_meta(array(self::$meta_keys['number_sales'] => $input_value));
	}

	public static function show_meta_boxes( $post, $metabox ) {
		$deal = WP_Groupbuy_Deal::get_instance($post->ID);
		switch ( $metabox['id'] ) {
			case 'wpg-increase-sale-fields':
				self::show_meta_box($deal, $post, $metabox);
				break;
			default:
				self::unknown_meta_box($metabox['id']);
				break;
		}
	}

	private static function show_meta_box( WP_Groupbuy_Deal $deal, $post, $metabox ) {
		$number_sales = self::get_number_sales($deal);
		
		?>
            
            <p>
                <label for="<?php echo self::$meta_keys['number_sales']; ?>"><?php self::_e('Enter number you want to add to deal\'s purchases'); ?>:</label><br>
                <input type="text" name="<?php echo self::$meta_keys['number_sales']; ?>" value="<?php echo $number_sales ?>" id="<?php echo self::$meta_keys['number_sales']; ?>" class="medium-text " size="10">
            </p>
        
        <?php
	}
	
	public static function save_meta_boxes( $post_id, $post ) {
		// only continue if it's an account post
		if ( $post->post_type != WP_Groupbuy_Deal::POST_TYPE ) {
			return;
		}
		// don't do anything on autosave, auto-draft, bulk edit, or quick edit
		if ( wp_is_post_autosave( $post_id ) || $post->post_status == 'auto-draft' || defined('DOING_AJAX') || isset($_GET['bulk_edit']) ) {
			return;
		}
		// save all the meta boxes
		$deal = WP_Groupbuy_Deal::get_instance($post_id);
		if ( !is_a($deal, 'WP_Groupbuy_Deal') ) {
			return; // The account doesn't exist
		}
		if (empty($_POST)) {
			return;
		}
		self::save_meta_box($deal, $post_id, $post);
	}


	private static function save_meta_box( WP_Groupbuy_Deal $deal, $post_id, $post ) {

		if ( isset($_POST[self::$meta_keys['number_sales']]) ) {
			self::set_number_sales($deal, $_POST[self::$meta_keys['number_sales']]);
		}
	
	}
	
}