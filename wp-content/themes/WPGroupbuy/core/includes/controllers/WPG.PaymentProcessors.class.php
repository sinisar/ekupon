<?php

add_action( 'wg_init_controllers', array( 'WP_Groupbuy_Payment_Processors', 'init' ), 55 );

abstract class WP_Groupbuy_Payment_Processors extends WP_Groupbuy_Controller {
	const PAYMENT_PROCESSOR_OPTION = 'wg_payment_processor';
	const CURRENCY_SYMBOL_OPTION = 'wg_currency_symbol';
	const MONEY_FORMAT_OPTION = 'wg_money_format';
	const CREDIT_TYPE_EXCHANGE_RATES = 'wg_credit_exchange_rates';
	private static $payment_processor;
	private static $active_payment_processor_class;
	protected static $settings_page;
	private static $potential_processors = array();
	private static $currency_symbol;
	private static $money_format;

	final public static function init() {
		self::$settings_page = self::register_settings_page( 'payment', self::__( 'Payment Gateway' ), self::__( 'Payment Gateway' ), 15, FALSE, 'general' );
		add_action( 'admin_init', array( get_class(), 'register_settings_fields' ), 10, 0 );
		self::get_payment_processor();
		self::$currency_symbol = get_option( self::CURRENCY_SYMBOL_OPTION, '$' );
		self::$money_format = get_option( self::MONEY_FORMAT_OPTION, '%0.2f' );
	}

	// Get an instance of the active payment processor
	public static function get_payment_processor() {
		self::$active_payment_processor_class = get_option( self::PAYMENT_PROCESSOR_OPTION, 'WP_Groupbuy_Paypal_WPP' );
		if ( class_exists( self::$active_payment_processor_class ) ) {
			self::$payment_processor = call_user_func( array( self::$active_payment_processor_class, 'get_instance' ) );
			return self::$payment_processor;
		} else {
			return NULL;
		}
	}

	// The ID of the payment settings page
	public static function get_settings_page() {
		return self::$settings_page;
	}

	final protected function __clone() {
		trigger_error( __CLASS__.' may not be cloned', E_USER_ERROR );
	}

	final protected function __sleep() {
		trigger_error( __CLASS__.' may not be serialized', E_USER_ERROR );
	}

	public static function get_instance() {}

	protected function __construct() {
		add_action( 'wg_new_purchase', array( $this, 'register_as_payment_method' ), 10, 1 );
	}

	// Process a payment
	public abstract function process_payment( WP_Groupbuy_Checkouts $checkout, WP_Groupbuy_Purchase $purchase );

	// Subclasses have to register to be listed as payment options
	public  static function register() {}

	// Generate a list of months
	public static function get_month_options() {
		$months = array(
			1 => self::__( '01 - Januar' ),
			2 => self::__( '02 - Februar' ),
			3 => self::__( '03 - Marec' ),
			4 => self::__( '04 - April' ),
			5 => self::__( '05 - Maj' ),
			6 => self::__( '06 - Junij' ),
			7 => self::__( '07 - Julij' ),
			8 => self::__( '08 - Avgust' ),
			9 => self::__( '09 - September' ),
			10 => self::__( '10 - Oktober' ),
			11 => self::__( '11 - November' ),
			12 => self::__( '12 - December' ),
		);
		return apply_filters( 'wg_payment_month_options', $months );
	}

	// Generate an array of years, starting with the current year, with keys matching values
	public static function get_year_options( $number = 10 ) {
		$this_year = (int)date( 'Y' );
		$years = array();
		for ( $i = 0 ; $i < $number ; $i++ ) {
			$years[$this_year+$i] = $this_year+$i;
		}
		return apply_filters( 'wg_payment_year_options', $years );
	}


	// Remove the payments page from the list of completed checkout pages
	protected function invalidate_checkout( WP_Groupbuy_Checkouts $checkout ) {
		$checkout->mark_page_incomplete( WP_Groupbuy_Checkouts::PAYMENT_PAGE );
	}

	public static function register_settings_fields() {
		register_setting( self::$settings_page, self::PAYMENT_PROCESSOR_OPTION );
		register_setting( self::$settings_page, self::CURRENCY_SYMBOL_OPTION );
		register_setting( self::$settings_page, self::CREDIT_TYPE_EXCHANGE_RATES, array( get_class(), 'save_exchange_rates' )  );
		add_settings_field( self::PAYMENT_PROCESSOR_OPTION, self::__( 'Choose Payment Method' ), array( get_class(), 'display_payment_processor_selection' ), self::$settings_page );
		add_settings_field( self::CURRENCY_SYMBOL_OPTION, self::__( 'Currency Symbol' ), array( get_class(), 'display_currency_symbol_selection' ), self::$settings_page );
		add_settings_field( self::CREDIT_TYPE_EXCHANGE_RATES, self::__( 'Credit Exchange Rates' ), array( get_class(), 'display_credit_type_exchange' ), self::$settings_page );
	}

	public static function display_payment_processor_selection() {
		echo '<select name="'.self::PAYMENT_PROCESSOR_OPTION.'">';
		foreach ( self::$potential_processors as $class => $label ) {
			echo '<option value="'.$class.'" '.selected( self::$active_payment_processor_class, $class ).'>'.$label.'</option>';
		}
		echo '</select>';
	}

	public static function get_registered_processors( $filter = '' ) {
		$processors = self::$potential_processors;
		switch ( $filter ) {
			case 'offsite':
				foreach ( $processors as $class => $label ) {
					if ( !self::is_offsite_processor($class) ) {
						unset($processors[$class]);
					}
				}
				break;
			case 'credit':
				foreach ( $processors as $class => $label ) {
					if ( !self::is_cc_processor($class) ) {
						unset($processors[$class]);
					}
				}
				break;
			default:
				break; // do not filter
		}
		return $processors;
	}

	public static function is_cc_processor( $class ) {
		return is_subclass_of($class, 'WP_Groupbuy_Credit_Card_Processors');
	}

	public static function is_offsite_processor( $class ) {
		return is_subclass_of($class, 'WP_Groupbuy_Offsite_Processors');
	}

	public static function display_currency_symbol_selection() {
		echo '<input type="text" name="'.self::CURRENCY_SYMBOL_OPTION.'" value="'.self::$currency_symbol.'" size="5" />';
		echo '<img width="16" height="16" src="'.WG_RESOURCES . 'images/help.png'. '" class="help_tip" title="'.self::__( 'Use a % before your currency symbol (%&pound;) to show the symbol after the value' ).'">';
	}

	public static function display_credit_type_exchange() {
		$types = apply_filters( 'wg_account_credit_types', array() );
		foreach ( $types as $type => $name ) {
			?>
			<p>

				<em><?php echo $name ?>:</em> <input type="number" name="<?php echo self::CREDIT_TYPE_EXCHANGE_RATES.'_'.$type ?>" value="<?php echo self::get_credit_exchange_rate( $type ) ?>" class="small-text"> <?php printf( self::__('credit equals %s for purchases.'), wg_get_formatted_money(1,FALSE) ) ?>
			</p>
			<?php
		}
		echo '<img width="16" height="16" src="'.WG_RESOURCES . 'images/help.png'. '" class="help_tip" title="'.self::__( 'Changing a ratio does not change your customers/accounts credit balance. It is not recommended to change the \'Account Balance\' ratio' ).'">';
	}

	public static function save_exchange_rates( $value = null ) {
		$types = apply_filters( 'wg_account_credit_types', array() );
		foreach ( $types as $type => $name ) {
			$key = self::CREDIT_TYPE_EXCHANGE_RATES . '_' . $type;
			if ( isset( $_POST[ $key ] ) ) {
				update_option( $key, $_POST[ $key ], true );
			}
		}
	}

	public static function get_credit_exchange_rate( $credit_type ) {
		return get_option( self::CREDIT_TYPE_EXCHANGE_RATES . '_' . $credit_type, 1 );
	}

	final protected static function add_payment_processor( $class, $label ) {
		self::$potential_processors[$class] = $label;
	}

	public static function get_currency_symbol() {
		return self::$currency_symbol;
	}

	public abstract function get_payment_method();

	// Register as the payment method for each unpaid-for item in the purchase
	public function register_as_payment_method( $purchase, $args = array() ) {
		$items = $purchase->get_products();
		foreach ( $items as $key => $item ) {
			$remaining = $item['price'];
			foreach ( $item['payment_method'] as $processor => $amount ) {
				$remaining -= $amount;
			}
			if ( $remaining >= 0.01 || $item['price'] == 0 ) { // leave a bit of room for floating point arithmetic
				$items[$key]['payment_method'][$this->get_payment_method()] = $remaining;
			}
		}
		$purchase->set_products( $items );
	}


	// Determine which items in a purchase for which the Payment is ready to capture funds
	protected function items_to_capture( WP_Groupbuy_Payment $payment, $release_payment = FALSE ) {
		$data = $payment->get_data();
		$items_to_capture = array();
		$purchase = WP_Groupbuy_Purchase::get_instance( $payment->get_purchase() );
		if ( !is_a( $purchase, 'WP_Groupbuy_Purchase' ) ) return; // nothing to do if the object is not a purchase
		foreach ( $purchase->get_products() as $item ) {
			// is this payment processor used for this item (in case of mixed payment for a purchase)?
			if ( isset( $item['payment_method'][$this->get_payment_method()] ) ) {
				// do we still need to capture this payment
				if ( isset( $data['uncaptured_deals'][$item['deal_id']] ) && count( $data['uncaptured_deals'][$item['deal_id']] ) > 0 ) {
					$deal = WP_Groupbuy_Deal::get_instance( $item['deal_id'] );
					if ( is_a( $deal, 'WP_Groupbuy_Deal' ) ) {
						// is the deal successful OR is this returning payments for release?
						if ( $deal->is_successful() || ( $release_payment && !$deal->is_successful() && $deal->is_expired() ) ) {
							// how much do we need to capture for this deal
							$items_to_capture[$item['deal_id']] = 0;
							foreach ( $data['uncaptured_deals'][$item['deal_id']] as $item ) {
								// if the deal has a dynamic price, or simply the admin changed the price.
								$subtotal = $deal->get_payment_price( $item['quantity'], $item['data'] )*$item['quantity']; // don't forget about quantity purchases and attributes.
								$tax = $purchase->get_item_tax( $item );
								$shipping = $purchase->get_item_shipping( $item );

								// only capture the portion handled by this payment method
								$ratio = 1;
								if ( $item['payment_method'][$this->get_payment_method()] != $item['price'] && $item['price'] > 0 ) {
									$ratio = $item['payment_method'][$this->get_payment_method()] / $item['price'];
								}

								$total = ( $subtotal + $tax + $shipping ) * $ratio;

								$items_to_capture[$item['deal_id']] += apply_filters( 'wg_item_to_capture_total', number_format( floatval( $total ), 2, '.', '' ), $total, $item, $release_payment ); // Make sure it's a number others can use, otherwise gateways will complain about x.00000001.
							}
						}
					}

				}
			}
		}
		return apply_filters( 'wg_pp_items_to_capture', $items_to_capture, $this, $payment );
	}

	// Check if a recurring payment is still active with the payment processor
	public function verify_recurring_payment( WP_Groupbuy_Payment $payment ) {
		
	}

	// Cancel a recurring payment
	public function cancel_recurring_payment( WP_Groupbuy_Payment $payment ) {
		$payment->set_status( WP_Groupbuy_Payment::STATUS_CANCELLED );
		// it's up to the individual payment processor to handle any other details
	}

	public function get_checkout_local( WP_Groupbuy_Checkouts $checkout, WP_Groupbuy_Purchase $purchase, $billing = FALSE ) {
		$local = array(
			'zone' => $checkout->cache['billing']['zone'],
			'country' => $checkout->cache['billing']['country'],
		);
		if ( !$billing && isset( $checkout->cache['shipping'] ) ) {
			$local = array(
				'zone' => $checkout->cache['shipping']['zone'],
				'country' => $checkout->cache['shipping']['country'],
			);
		}
		if ( empty( $local['zone'] ) || empty( $local['country'] ) ) {
			$user_id = $purchase->get_user();
			$account = WP_Groupbuy_Account::get_instance( $user_id );
			$address = $account->get_address();
			if ( !empty( $address ) ) {
				$account_local = array(
					'zone' => $address['zone'],
					'country' => $address['country'],
				);
			}
			$local = wp_parse_args( $local, $account_local );
		}
		return $local;
	}

	public static function get_credit_types() {
		$types = array(
			self::CREDIT_TYPE => self::__( 'Account Balance' ),
		);
		return apply_filters( 'wg_account_credit_types', $types );
	}
}
