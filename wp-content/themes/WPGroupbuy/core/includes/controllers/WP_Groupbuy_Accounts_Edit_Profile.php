<?php

class WP_Groupbuy_Accounts_Edit_Profile extends WP_Groupbuy_Controller {
	const EDIT_PROFILE_PATH_OPTION = 'wg_account_edit_profile_path';
	const EDIT_PROFILE_QUERY_VAR = 'wg_account_edit';
	const FORM_ACTION = 'wg_account_edit';
	private static $edit_profile_path = 'account/edit';
	private static $instance;
	public static function init() {
		self::$edit_profile_path = get_option( self::EDIT_PROFILE_PATH_OPTION, self::$edit_profile_path );
		self::register_path_callback( self::$edit_profile_path, array( get_class(), 'on_edit_profile_page' ), self::EDIT_PROFILE_QUERY_VAR, 'account/edit' );
		add_action( 'admin_init', array( get_class(), 'register_settings_fields' ), 10, 1 );
	}
	public static function register_settings_fields() {
		$page = WP_Groupbuy_UI::get_settings_page();
		$section = 'WG_URL_paths';
		// Settings
		register_setting( $page, self::EDIT_PROFILE_PATH_OPTION );
		add_settings_field( self::EDIT_PROFILE_PATH_OPTION, self::__( 'Account Profile Edit Path' ), array( get_class(), 'display_account_edit_path' ), $page, $section );
	}
	public static function display_account_edit_path() {
		echo trailingslashit( get_home_url() ) . ' <input type="text" name="' . self::EDIT_PROFILE_PATH_OPTION . '" id="' . self::EDIT_PROFILE_PATH_OPTION . '" value="' . esc_attr( self::$edit_profile_path ) . '" size="40"/><br />';
	}
	public static function on_edit_profile_page() {
		self::login_required();
		self::get_instance(); // make sure the class is instantiated
	}
	public static function get_url() {
		if ( self::using_permalinks() ) {
			return trailingslashit( home_url() ).trailingslashit( self::$edit_profile_path );
		} else {
			return add_query_arg( self::EDIT_PROFILE_QUERY_VAR, 1, home_url() );
		}
	}
	private function __clone() {
		trigger_error( __CLASS__.' may not be cloned', E_USER_ERROR );
	}
	private function __sleep() {
		trigger_error( __CLASS__.' may not be serialized', E_USER_ERROR );
	}
	public static function get_instance() {
		if ( !( self::$instance && is_a( self::$instance, __CLASS__ ) ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	private function __construct() {
		self::do_not_cache(); // never cache the account pages
		add_filter( 'wg_validate_account_edit_form', array( $this, 'validate_account_fields' ), 0, 2 );
		if ( isset( $_POST['wg_account_action'] ) && $_POST['wg_account_action'] == self::FORM_ACTION ) {
			$this->process_form_submission();
		}
		add_action( 'pre_get_posts', array( $this, 'edit_query' ), 10, 1 );
		add_action( 'the_post', array( $this, 'view_profile_form' ), 10, 1 );
		add_filter( 'the_title', array( $this, 'get_title' ), 10, 2 );
		add_filter( 'wg_account_edit_panes', array( $this, 'get_panes' ), 0, 2 );
	}
	// Edit the query on the profile edit page to select the user's account.
	public function edit_query( WP_Query $query ) {
		if ( isset( $query->query_vars[self::EDIT_PROFILE_QUERY_VAR] ) && $query->query_vars[self::EDIT_PROFILE_QUERY_VAR] ) {
			$query->query_vars['post_type'] = WP_Groupbuy_Account::POST_TYPE;
			$query->query_vars['p'] = WP_Groupbuy_Account::get_account_id_for_user();
		}
	}
	private function process_form_submission() {
		$errors = array();
		$account = WP_Groupbuy_Account::get_instance();
		$user = $account->get_user();
		$errors = apply_filters( 'wg_validate_account_edit_form', $errors, $account );
		$user_email = apply_filters( 'user_registration_email', $_POST['wg_account_email'] );
		if ( !$errors && ( $user->user_email != $user_email || $_POST['wg_account_password'] ) ) {
			$_POST['email'] = $user_email;
			if ( $_POST['wg_account_password'] ) {
				$_POST['pass1'] = $_POST['wg_account_password'];
				$_POST['pass2'] = $_POST['wg_account_password_confirm'];
				//	missing nickname on changing password
				$_POST['nickname'] = $user_email;
			}
			require_once ABSPATH . 'wp-admin/includes/admin.php';
			$password_errors = edit_user( $account->get_user_id() );
			if ( is_wp_error( $password_errors ) ) {
				$errors = $password_errors->get_error_messages();
			}
		}
		if ( $errors ) {
			foreach ( $errors as $error ) {
				self::set_message( $error, self::MESSAGE_STATUS_ERROR );
			}
			return FALSE;
		}
		$first_name = isset( $_POST['wg_contact_first_name'] ) ? $_POST['wg_contact_first_name'] : '';
		$account->set_name( 'first', $first_name );
		$last_name = isset( $_POST['wg_contact_last_name'] ) ? $_POST['wg_contact_last_name'] : '';
		$account->set_name( 'last', $last_name );
		$account_phone = isset( $_POST['wg_contact_phone'] ) ? $_POST['wg_contact_phone'] : '';
		$account->set_name( 'phone', $account_phone );
		$account_email = isset( $_POST['wg_contact_email'] ) ? $_POST['wg_contact_email'] : '';
		$account->set_name( 'email', $account_email );
			
		$address = array(
			'street' => isset( $_POST['wg_contact_street'] ) ? $_POST['wg_contact_street'] : '',
			'city' => isset( $_POST['wg_contact_city'] ) ? $_POST['wg_contact_city'] : '',
			'zone' => isset( $_POST['wg_contact_zone'] ) ? $_POST['wg_contact_zone'] : '',
			'postal_code' => isset( $_POST['wg_contact_postal_code'] ) ? $_POST['wg_contact_postal_code'] : '',
			'country' => isset( $_POST['wg_contact_country'] ) ? $_POST['wg_contact_country'] : '',
			
		);
		//	load malinglists from POST
		$mailinglists = array();
		if(isset($_POST['wg_mailinglist_mailinglists'])) {
			$mailinglists = $_POST['wg_mailinglist_mailinglists'];
		}

		$account->set_address( $address );
		//	save mailinglists
		$account->set_mailinglist( $mailinglists );

		do_action( 'wg_process_account_edit_form', $account );
		self::set_message( self::__( 'Account updated' ) );
		wp_redirect( WP_Groupbuy_Accounts::get_url(), 303 );
		exit;
	}
	public function validate_account_fields( $errors, $account ) {
		$user = $account->get_user();
		$user_email = apply_filters( 'user_registration_email', $_POST['wg_account_email'] );
		// Check the e-mail address
		if ( $user_email == '' ) {
			$errors[] = 'Please type your e-mail address.';
		} elseif ( ! is_email( $user_email ) ) {
			$errors[] = 'The email address isn&#8217;t correct.';
		} elseif ( $user_email != $user->user_email && email_exists( $user_email ) ) {
			$errors[] = 'This email is already registered, please choose another one.';
		}
		if ( $_POST['wg_account_password'] && !$_POST['wg_account_password_confirm'] ) {
			$errors[] = 'Please confirm your password.';
		} elseif ( $_POST['wg_account_password'] != $_POST['wg_account_password_confirm'] ) {
			$errors[] = 'The passwords you entered to not match.';
		}
		return $errors;
	}
	// Add the default pane to the account edit form
	public function get_panes( array $panes, WP_Groupbuy_Account $account ) {
		$panes['account'] = array(
			'weight' => 0,
			'body' => self::load_view_to_string( 'account/edit-account-info', array( 'fields' => $this->account_info_fields( $account ) ) ),
		);
		$panes['contact'] = array(
			'weight' => 1,
			'body' => self::load_view_to_string( 'account/edit-contact-info', array( 'fields' => $this->contact_info_fields( $account ) ) ),
		);
		$panes['mailinglist'] = array(
			'weight' => 2,
			'body' => self::load_view_to_string('account/edit-mailinglist-info', array( 'fields' => $this->mailinglist_info_fields( $account ) ) ),
		);
		$panes['controls'] = array(
			'weight' => 1000,
			'body' => self::load_view_to_string( 'account/edit-controls', array() ),
		);
		return $panes;
	}
	private function account_info_fields( $account = NULL ) {
		if ( !$account ) {
			$account = WP_Groupbuy_Account::get_instance();
		}
		$user = $account->get_user();
		$fields = array(
			'email' => array(
				'weight' => 0,
				'label' => self::__( 'Email Address' ),
				'type' => 'text',
				'required' => TRUE,
				'default' => $user->user_email,
			),
			'password' => array(
				'weight' => 10,
				'label' => self::__( 'Password' ),
				'type' => 'password',
				'required' => FALSE,
				'default' => '',
			),
			'password_confirm' => array(
				'weight' => 10.01,
				'label' => self::__( 'Confirm Password' ),
				'type' => 'password',
				'required' => FALSE,
				'default' => '',
			),
		);
		$fields = apply_filters( 'wg_account_edit_account_fields', $fields );
		uasort( $fields, array( get_class(), 'sort_by_weight' ) );
		return $fields;
	}
	private function contact_info_fields( $account = NULL ) {
		$fields = $this->get_standard_address_fields( $account );
		$fields = apply_filters( 'wg_account_edit_contact_fields', $fields );
		foreach ( $fields as $key => $value ) { // Remove all the required fields since we don't validate any of it anyway.
			if ( $value['required'] ) {
				unset( $fields[$key]['required'] );
			}
		}
		uasort( $fields, array( get_class(), 'sort_by_weight' ) );
		return $fields;
	}

	//
	private function mailinglist_info_fields($account = NULL) {
		if ( !$account ) {
			$account = WP_Groupbuy_Account::get_instance();
		}

		$fields['mailinglists[]'] = array(
			'weight' => 20,
			'label' => self::__('Mailing lists'),
			'type' => 'checkboxes',
			'options' => get_all_mailinglist_as_array(array('conditions' => array('privatelist' => 'N'))),
		);
		return $fields;
	}

	// Update the global $pages array with the HTML for the page.
	public function view_profile_form( $post ) {
		if ( $post->post_type == WP_Groupbuy_Account::POST_TYPE ) {
			remove_filter( 'the_content', 'wpautop' );
			$account = WP_Groupbuy_Account::get_instance();
			$panes = apply_filters( 'wg_account_edit_panes', array(), $account );
			uasort( $panes, array( get_class(), 'sort_by_weight' ) );
			$view = self::load_view_to_string( 'account/edit', array(
					'panes' => $panes,
				) );
			global $pages;
			$pages = array( $view );
		}
	}
	// Filter 'the_title' to display the title of the page rather than the user name
	public function get_title(  $title, $post_id  ) {
		$post = get_post( $post_id );
		if ( $post->post_type == WP_Groupbuy_Account::POST_TYPE ) {
			$user = wp_get_current_user();
			if ( $user->display_name ) {
				$name = $user->display_name;
			} else {
				$name = $user->user_login;
			}
			return sprintf( self::__( 'Editing %s&rsquo;s Profile' ), $name );
		}
		return $title;
	}
}
