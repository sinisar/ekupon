<?php

add_action( 'wg_init_controllers', array( 'WP_Groupbuy_UI', 'init' ), 90 );

class WP_Groupbuy_UI extends WP_Groupbuy_Controller {
	const TODAYSDEAL_PATH_OPTION = 'wg_todaysdeal_path';
	const REMOVE_EXPIRED_DEALS = 'wg_remove_expired';
	const COUNTRIES_OPTION = 'wg_countries_filter';
	const STATES_OPTION = 'wg_states_filter';
	public static $todays_deal_path;
	public static $remove_expired;
	protected static $settings_page;
	protected static $int_settings_page;
	protected static $countries;
	protected static $states;
	private static $instance;

	final public static function init() {
		self::$settings_page = self::register_settings_page( 'wg_settings', self::__( 'Change Options & Settings of your site' ), self::__( 'Settings' ), 10, FALSE, 'general' );

		add_action( 'admin_init', array( get_class(), 'register_settings_fields' ), 10, 0 );
		add_action( 'admin_init', array( get_class(), 'register_int_settings_fields' ), 100, 0 );
		self::$todays_deal_path = trailingslashit( get_option( self::TODAYSDEAL_PATH_OPTION, 'todays-deal' ) );
		self::$remove_expired = get_option( self::REMOVE_EXPIRED_DEALS, FALSE );
		self::$countries = get_option( self::COUNTRIES_OPTION, FALSE );
		self::$states = get_option( self::STATES_OPTION, FALSE );

		// Callback
		self::register_path_callback( self::$todays_deal_path, array( get_class(), 'todays_deal' ), self::TODAYSDEAL_PATH_OPTION );

		self::get_instance();

		add_action( 'init', array( get_class(), 'register_resources' ) );
		add_action( 'wp_enqueue_scripts', array( get_class(), 'frontend_enqueue' ) );
		add_action( 'admin_enqueue_scripts', array( get_class(), 'admin_enqueue' ) );

		// Widgets
		add_action( 'widgets_init', create_function( '', 'return register_widget("WPGroupbuy_FinePrint");' ) );
		add_action( 'widgets_init', create_function( '', 'return register_widget("WPGroupbuy_Highlights");' ) );
		add_action( 'widgets_init', create_function( '', 'return register_widget("WPGroupbuy_Locations");' ) );
		add_action( 'widgets_init', create_function( '', 'return register_widget("WPGroupbuy_Location");' ) );
		add_action( 'widgets_init', create_function( '', 'return register_widget("WPGroupbuy_Categories");' ) );
		add_action( 'widgets_init', create_function( '', 'return register_widget("WPGroupbuy_Tags");' ) );
		add_action( 'widgets_init', create_function( '', 'return register_widget("WPGroupbuy_RecentDeals");' ) );
		add_action( 'widgets_init', create_function( '', 'return register_widget("WPGroupbuy_RelatedDeals");' ) );
		add_action( 'widgets_init', create_function( '', 'return register_widget("WPGroupbuy_Share_and_Earn");' ) );
		add_action( 'widgets_init', create_function( '', 'return register_widget("WPGroupbuy_UpcomingDeals");' ) );
        add_action( 'widgets_init', create_function( '', 'return register_widget("WPGroupbuy_HomepageBanners");' ) );

		add_action( 'admin_head', array( get_class(), 'admin_footer' ) );
		if ( version_compare( get_bloginfo( 'version' ), '3.3', '<=' ) ) {
			add_filter( 'contextual_help', array( get_class(), 'admin_contextual_help' ), 10, 2 );
		}

	}

	public static function admin_footer() {
		echo '<style type="text/css">';
		echo '#icon-edit.icon32-posts-wg_deal { background: url('.WG_URL.'/resources/images/deals-big.png) no-repeat 0 0; }';
		echo '#icon-edit.icon32-posts-wg_merchant { background: url('.WG_URL.'/resources/images/merchant-big.png) no-repeat 0 0; }';
		echo '</style>';
	}

	public static function admin_contextual_help( $text, $screen ) {
		
	}

	public static function register_resources() {
		// Datepicker and misc.
		wp_register_script( 'wp-groupbuy-admin', WG_RESOURCES . 'js/admin.wpg.js', array( 'jquery', 'jquery-ui-draggable' ), WP_Groupbuy::WG_VERSION );
		// Validation plugin and misc
		wp_register_script( 'wp-groupbuy-jquery', WG_RESOURCES . 'js/jquery.public.wpg.js', array( 'jquery' ), WP_Groupbuy::WG_VERSION );
		// Select2
		wp_register_script( 'select2', WG_RESOURCES . 'js/select2/select2.js', array( 'jquery' ), WP_Groupbuy::WG_VERSION );

		// CSS
		wp_register_style( 'wp-groupbuy-admin-css', WG_RESOURCES . 'css/wpg.styles.css' );
		wp_register_style( 'select2_css', WG_RESOURCES . 'js/select2/select2.css' );
	}

	public static function frontend_enqueue() {
		wp_enqueue_script( 'wp-groupbuy-jquery' );
	}

	public static function admin_enqueue() {
		wp_enqueue_script( 'wpg-admin-tiptip', WG_RESOURCES . 'js/jquery.tipTip.min.js', array('jquery') );
		wp_enqueue_script( 'wp-groupbuy-admin' );
		//wp_enqueue_script( 'wp-groupbuy-admin-marketplace' );
		wp_enqueue_script( 'select2' );
		wp_enqueue_style( 'wp-groupbuy-admin-css' );
		wp_enqueue_style( 'select2_css' );
	}

	// The ID of the payment settings page
	public static function get_settings_page() {
		return self::$settings_page;
	}

	final protected function __clone() {
		trigger_error( __CLASS__.' may not be cloned', E_USER_ERROR );
	}

	final protected function __sleep() {
		trigger_error( __CLASS__.' may not be serialized', E_USER_ERROR );
	}

	public static function get_instance() {
		if ( !( self::$instance && is_a( self::$instance, __CLASS__ ) ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}


	protected function __construct() {
		if ( 'FALSE' != self::$remove_expired ) {
			add_filter( 'pre_get_posts', array( get_class(), 'remove_expired_deals' ), 10, 1 );
		}
		if ( 'false' != self::$countries && !empty( self::$countries ) ) {
			add_filter( 'wg_country_options', array( get_class(), 'get_countries_option' ), 10, 2 );
		}
		if ( 'false' != self::$states && !empty( self::$states ) ) {
			add_filter( 'wg_state_options', array( get_class(), 'get_states_option' ), 10, 2 );
		}
	}

	// Remove expired deals from loop
	public static function remove_expired_deals( &$query ) {
		$is_front_page = ( $query->get( 'page_id' ) == get_option( 'page_on_front' ) );
		
		if ( ( $is_front_page || is_tax() || is_archive() ) && ( $is_front_page || !is_page() ) 
			&& !is_single() && !is_admin() && !wg_on_voucher_page() ) {
			if ( isset($query->query_vars['post_type']) && $query->query_vars['post_type'] == wg_get_deal_post_type()  ) {
				$query->set( 'meta_query', array(
						array(
							'key' => '_expiration_date',
							'value' => array( 0, current_time( 'timestamp' ) ),
							'compare' => 'NOT BETWEEN'
						)
					) );
			}
			if ( self::$remove_expired == 'ALL' ) {
				if (
					isset( $query->query[wg_get_deal_location_tax()] ) ||
					isset( $query->query[wg_get_deal_cat_slug()] ) ||
					isset( $query->query[wg_get_deal_tag_slug()] )
				) {
					$query->set( 'meta_query', array(
							array(
								'key' => '_expiration_date',
								'value' => array( 0, current_time( 'timestamp' ) ),
								'compare' => 'NOT BETWEEN'
							)
						) );
				}
			}
		}
	}

	public function todays_deal() {
		wp_redirect( wg_get_latest_deal_link() );
		exit();
	}

	public static function register_settings_fields() {
		$page = self::$settings_page;
		$section = 'wg_general_settings';
		add_settings_section( $section, self::__( 'General Options' ), array( get_class(), 'display_settings_section' ), $page );
		register_setting( self::$settings_page, self::TODAYSDEAL_PATH_OPTION );
		register_setting( self::$settings_page, self::REMOVE_EXPIRED_DEALS );
		add_settings_field( self::REMOVE_EXPIRED_DEALS, self::__( 'How do you want to show Expired Deals?' ), array( get_class(), 'display_option_remove_expired' ), $page, $section );
	}

	public static function register_int_settings_fields() {
		$page = self::$settings_page;
		$int_section = 'wg_internationalization_settings';
	}

	public static function display_option_todays_deal() {
		echo site_url().'/<input type="text" name="'.self::TODAYSDEAL_PATH_OPTION.'" value="'.self::$todays_deal_path.'">';
	}

	public static function display_option_remove_expired() {
		echo '<label><input type="radio" name="'.self::REMOVE_EXPIRED_DEALS.'" value="TRUE" '.checked( 'TRUE', self::$remove_expired, FALSE ).'/> '.self::__( 'Remove the expired deals from the main deals page.' ).'</label><br />';
		echo '<label><input type="radio" name="'.self::REMOVE_EXPIRED_DEALS.'" value="ALL" '.checked( 'ALL', self::$remove_expired, FALSE ).'/> '.self::__( 'Remove the expired deals from location, tags, category page.' ).'</label><br />';
		echo '<label><input type="radio" name="'.self::REMOVE_EXPIRED_DEALS.'" value="FALSE" '.checked( 'FALSE', self::$remove_expired, FALSE ).'/> '.self::__( 'Show expired deals. ' ).'</label><br />';
	}

	public static function display_internationalization_section() {
		echo '<p>'.self::_e( 'Select the states and countries/proviences you would like in your forms.' ).'</p>';

	}

	public static function display_option_states_title() {
		$title = '<strong>'.self::__( 'States' ).'</strong>';
		$title .= '<p>'.self::__( 'Additional states can be added by hooking into the <code>wg_state_options</code> filter.' ).'</p>';
		return $title;
	}

	public static function display_option_states() {
		echo '<div class="wg_state_options">';
		echo '<select name="'.self::STATES_OPTION.'[]" class="select2" multiple="multiple">';
		foreach ( parent::$grouped_states as $group => $states ) {
			echo '<optgroup label="'.$group.'">';
			foreach ($states as $key => $name) {
				$selected = ( in_array( $name, self::$states[$group] ) || empty( self::$states ) ) ? 'selected="selected"' : null ;
				echo '<option value="'.$key.'" '.$selected.'>&nbsp;'.$name.'</option>';
			}
			echo '</optgroup>';
		}
		echo '</select>';
		echo '</div>';
	}

	public static function display_option_countries_title() {
		$title = '<strong>'.self::__( 'Countries' ).'</strong>';
		$title .= '<p>'.self::__( 'Additional countries can be added by hooking into the <code>wg_country_options</code> filter.' ).'</p>';
		$title .= '<p>'.self::__( 'Note: Some payment processors country support is limited. For example, Paypal Pro only accepts <a href="https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_country_codes">these countries</a>.' ).'</p>';
		return $title;
	}

	public static function display_option_countries() {
		echo '<div class="wg_country_options">';
		echo '<select name="'.self::COUNTRIES_OPTION.'[]" class="select2" multiple="multiple">';
		foreach ( parent::$countries as $key => $name ) {
			$selected = ( in_array( $name, self::$countries ) || empty( self::$countries ) ) ? 'selected="selected"' : null ;
			echo '<option value="'.$name.'" '.$selected.'>&nbsp;'.$name.'</option>';
		}
		echo '</select>';
		echo '</div>';
	}

	public static function save_states( $selected ) {
		$sanitized_options = array();
		foreach ( parent::$grouped_states as $group => $states ) {
			$sanitized_options[$group] = array();
			foreach ($states as $key => $name) {
				if ( in_array( $key, $selected ) ) {
					$sanitized_options[$group][$key] = $name;
				}
			}
			// Unset the empty groups
			if ( empty( $sanitized_options[$group] ) ) {
				unset( $sanitized_options[$group] );
			}
		}
		return $sanitized_options;
	}

	public static function save_countries( $options ) {
		$sanitized_options = array();
		foreach ( parent::$countries  as $key => $name ) {
			if ( in_array( $name, $options ) ) {
				$sanitized_options[$key] = $name;
			}
		}
		return $sanitized_options;
	}

	public static function get_states_option( $states = array(), $args = array() ) {
		if ( isset( $args['include_option_none'] ) && $args['include_option_none'] ) {
			$states = array( '' => $args['include_option_none'] ) + self::$states;
		}
		return $states;
	}

	public static function get_countries_option( $countries = array(), $args = array() ) {
		if ( isset( $args['include_option_none'] ) && $args['include_option_none'] ) {
			$countries = array( '' => $args['include_option_none'] ) + self::$countries;
		}
		return $countries;
	}
}

// Fine print Widget
class WPGroupbuy_FinePrint extends WP_Widget {

	function WPGroupbuy_FinePrint() {
		$widget_ops = array( 'description' => wpg__( 'Can only be used on the Deal Page.' ) );
		parent::__construct( false, $name = wpg__( 'WPG - Deal Fine Print' ), $widget_ops );
	}

	function widget( $args, $instance ) {
		do_action( 'pre_fine_print', $args, $instance );
		extract( $args );
		$title = apply_filters( 'widget_title', @$instance['title'] );
		$post_type = get_query_var( 'post_type' );
		if ( is_single() && $post_type == wg_get_deal_post_type() ) {
			echo $before_widget;
			ob_start();
			if ( !empty( $title ) ) { echo $before_title . $title . $after_title; };
?>
				<div class="deal-widget-inner"><?php wg_fine_print(); ?></div>
				<?php

			$view = ob_get_clean();
			print apply_filters( 'wg_fine_print_widget', $view );
			echo $after_widget;
		}
		do_action( 'post_fine_print', $args, $instance );
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		return $instance;
	}

	function form( $instance ) {
		$title = esc_attr( @$instance['title'] );
?>
            <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php wpg_e( 'Title:' ); ?> <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></label></p>
        <?php
	}

}

// Highlights Widget
class WPGroupbuy_Highlights extends WP_Widget {

	function WPGroupbuy_Highlights() {
		$widget_ops = array( 'description' => wpg__( 'Can only be used on the Deal Page.' ) );
		parent::__construct( false, $name = wpg__( 'WPG - Deal Highlights' ), $widget_ops );
	}

	function widget( $args, $instance ) {
		do_action( 'pre_highlights', $args, $instance );
		extract( $args );
		$title = apply_filters( 'widget_title', @$instance['title'] );
		$post_type = get_query_var( 'post_type' );
		if ( is_single() && $post_type == wg_get_deal_post_type() ) {
			echo $before_widget;
			ob_start();
			if ( !empty( $title ) ) { echo $before_title . $title . $after_title; };
?>
				<div class="deal-widget-inner"><?php wg_highlights(); ?></div>
				<?php

			$view = ob_get_clean();
			print apply_filters( 'wg_highlights_widget', $view );
			echo $after_widget;
		}
		do_action( 'post_highlights', $args, $instance );
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		return $instance;
	}

	function form( $instance ) {
		$title = esc_attr( @$instance['title'] );
?>
            <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php wpg_e( 'Title:' ); ?> <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></label></p>
        <?php
	}

}

// Locations Widget
class WPGroupbuy_Locations extends WP_Widget {

	function WPGroupbuy_Locations() {
		$widget_ops = array( 'description' => wpg__( 'Can only be used on the Deal Page.' ) );
		parent::__construct( false, $name = wpg__( 'WPG - Locations' ), $widget_ops );
	}

	function widget( $args, $instance ) {
		do_action( 'pre_locations', $args, $instance );
		extract( $args );

		$title = apply_filters( 'widget_title', @$instance['title'] );
		$post_type = get_query_var( 'post_type' );
		if ( is_single() && $post_type == wg_get_deal_post_type() ) {
			echo $before_widget;
			ob_start();
			if ( !empty( $title ) ) { echo $before_title . $title . $after_title; };
?>
				<div class="deal-widget-inner"><?php wg_deal_locations(); ?></div>
				<?php

			$view = ob_get_clean();
			print apply_filters( 'wg_locations_widget', $view );
			echo $after_widget;
		}
		do_action( 'post_locations', $args, $instance );
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		return $instance;
	}

	function form( $instance ) {
		$title = esc_attr( @$instance['title'] );
?>
            <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php wpg_e( 'Title:' ); ?> <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></label></p>
        <?php
	}

}

// WPG Categories Widget
class WPGroupbuy_Categories extends WP_Widget {

	function WPGroupbuy_Categories() {
		$widget_ops = array( 'description' => wpg__( 'Can only be used on the Deal Page.' ) );
		parent::__construct( false, $name = wpg__( 'WPG - Categories' ), $widget_ops );
	}

	function widget( $args, $instance ) {
		do_action( 'pre_categories', $args, $instance );
		extract( $args );

		$title = apply_filters( 'widget_title', @$instance['title'] );
		$post_type = get_query_var( 'post_type' );
		if ( is_single() && $post_type == wg_get_deal_post_type() ) {
			echo $before_widget;
			ob_start();
			if ( !empty( $title ) ) { echo $before_title . $title . $after_title; };
?>
				<div class="deal-widget-inner"><?php wg_deal_categories(); ?></div>
				<?php

			$view = ob_get_clean();
			print apply_filters( 'wg_categories_widget', $view );
			echo $after_widget;
		}
		do_action( 'post_categories', $args, $instance );
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		return $instance;
	}

	function form( $instance ) {
		$title = esc_attr( @$instance['title'] );
?>
            <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php wpg_e( 'Title:' ); ?> <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></label></p>
        <?php
	}

}

// Tags Widget
class WPGroupbuy_Tags extends WP_Widget {

	function WPGroupbuy_Tags() {
		$widget_ops = array( 'description' => wpg__( 'Can only be used on the Deal Page.' ) );
		parent::__construct( false, $name = wpg__( 'WPG - Tags' ), $widget_ops );
	}

	function widget( $args, $instance ) {
		do_action( 'pre_tags', $args, $instance );
		extract( $args );

		$title = apply_filters( 'widget_title', @$instance['title'] );
		$post_type = get_query_var( 'post_type' );
		if ( is_single() && $post_type == wg_get_deal_post_type() ) {
			echo $before_widget;
			ob_start();
			if ( !empty( $title ) ) { echo $before_title . $title . $after_title; };
?>
				<div class="deal-widget-inner"><?php wg_deal_tags( ); ?></div>
				<?php

			$view = ob_get_clean();
			print apply_filters( 'wg_tags_widget', $view );
			echo $after_widget;
		}
		do_action( 'post_tags', $args, $instance );
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		return $instance;
	}

	function form( $instance ) {
		$title = esc_attr( @$instance['title'] );
?>
            <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php wpg_e( 'Title:' ); ?> <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></label></p>
        <?php
	}

}

// Location Widget
class WPGroupbuy_Location extends WP_Widget {

	function WPGroupbuy_Location() {
		$widget_ops = array( 'description' => wpg__( 'Can only be used on the Deal Page.' ) );
		parent::__construct( false, $name = wpg__( 'WPG - Map' ), $widget_ops );
	}

	function widget( $args, $instance ) {
		do_action( 'pre_location', $args, $instance );
		extract( $args );
		$title = apply_filters( 'widget_title', @$instance['title'] );

		$post_type = get_query_var( 'post_type' );
		if ( is_single() && $post_type == wg_get_deal_post_type() ) {
			echo $before_widget;
			ob_start();
			if ( !empty( $title ) ) { echo $before_title . $title. $after_title; };
?>
				<div class="deal-widget-inner"><?php wg_map(); ?></div>
				<?php

			$view = ob_get_clean();
			print apply_filters( 'wg_location_widget', $view );
			echo $after_widget;
		}
		do_action( 'post_location', $args, $instance );
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		return $instance;
	}

	function form( $instance ) {
		$title = esc_attr( @$instance['title'] );
?>
            <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php wpg_e( 'Title:' ); ?> <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></label></p>
        <?php
	}

}

// Affiliate Widget
class WPGroupbuy_Share_and_Earn extends WP_Widget {

	function WPGroupbuy_Share_and_Earn() {
		$widget_ops = array( 'description' => wpg__( 'Display a affiliate widget.' ) );
		parent::__construct( false, $name = wpg__( 'WPG - Affiliate Link' ), $widget_ops );
	}

	function widget( $args, $instance ) {
		do_action( 'pre_share_and_earn', $args, $instance );
		extract( $args );
		$title = apply_filters( 'widget_title', @$instance['title'] );
		$buynow = @$instance['buynow'] ? : wpg__('Buy Now');

		echo $before_widget;
		if ( !empty( $title ) ) { echo $before_title . $title. $after_title; };

		if ( is_single() && get_post_type( get_the_ID() ) == wg_get_deal_post_type() ):
			WP_Groupbuy_Controller::load_view( 'widgets/share-earn.php', array( 'buynow'=>$buynow ) );
		endif;

		echo $after_widget;
		do_action( 'post_share_and_earn', $args, $instance );
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		return $instance;
	}

	function form( $instance ) {
		$title = esc_attr( @$instance['title'] );
?>
            <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php wpg_e( 'Title:' ); ?> <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></label></p>
        <?php
	}

}

// Recent Deals Widget
class WPGroupbuy_RecentDeals extends WP_Widget {

	function WPGroupbuy_RecentDeals() {
		$widget_ops = array( 'description' => wpg__( 'Display of recent deals.' ) );
		parent::__construct( false, $name = wpg__( 'WPG - Recent Deals' ), $widget_ops );
	}

	function widget( $args, $instance ) {
		do_action( 'pre_recent_deals', $args, $instance );
		global $wg, $wp_query;
		$temp = null;
		extract( $args );

		$title = apply_filters( 'widget_title', @$instance['title'] );
		$buynow = @$instance['buynow'] ? : wpg__('Buy Now');
		$deals = apply_filters( 'wg_recent_deals_widget_show', @$instance['deals'] );
		$count = 1;
		$deal_query= null;
		$args=array(
			'post_type' => wg_get_deal_post_type(),
			'post_status' => 'publish',
			'meta_query' => array(
				array(
					'key' => '_expiration_date',
					'value' => array( 0, current_time( 'timestamp' ) ),
					'compare' => 'NOT BETWEEN'
				) ),
			'posts_per_page' => $deals,
		);

		if ( is_single() ) {
			$args['post_not_in'] = array( $wp_query->post->ID );
		}


		$deal_query = new WP_Query( $args );
		if ( $deal_query->have_posts() ) {
			echo $before_widget;
			echo $before_title . $title . $after_title;
			while ( $deal_query->have_posts() ) : $deal_query->the_post();

			WP_Groupbuy_Controller::load_view( 'widgets/recent-deals.php', array( 'buynow'=>$buynow ) );

			endwhile;
			echo $after_widget;
		}
		$deal_query = null; $deal_query = $temp;
		wp_reset_query();
		do_action( 'post_recent_deals', $args, $instance );
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['buynow'] = strip_tags( $new_instance['buynow'] );
		$instance['deals'] = strip_tags( $new_instance['deals'] );
		$instance['show_expired'] = strip_tags( $new_instance['show_expired'] );
		return $instance;
	}

	function form( $instance ) {
		$title = esc_attr( @$instance['title'] );
		$buynow = esc_attr( @$instance['buynow'] );
		$deals = esc_attr( @$instance['deals'] );
		$show_expired = esc_attr( @$instance['show_expired'] );
?>
            <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php wpg_e( 'Title:' ); ?> <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></label></p>
            <p><label for="<?php echo $this->get_field_id( 'buynow' ); ?>"><?php wpg_e( 'Buy now link text:' ); ?> <input class="widefat" id="<?php echo $this->get_field_id( 'buynow' ); ?>" name="<?php echo $this->get_field_name( 'buynow' ); ?>" type="text" value="<?php echo $buynow; ?>" /></label></p>
            <p><label for="<?php echo $this->get_field_id( 'deals' ); ?>"><?php wpg_e( 'Number of deals to display:' ); ?>
            	<select id="<?php echo $this->get_field_id( 'deals' ); ?>" name="<?php echo $this->get_field_name( 'deals' ); ?>">
					<option value="1">1</option>
					<option value="2"<?php if ( $deals=="2" ) {echo ' selected="selected"';} ?>>2</option>
					<option value="3"<?php if ( $deals=="3" ) {echo ' selected="selected"';} ?>>3</option>
					<option value="4"<?php if ( $deals=="4" ) {echo ' selected="selected"';} ?>>4</option>
					<option value="5"<?php if ( $deals=="5" ) {echo ' selected="selected"';} ?>>5</option>
					<option value="10"<?php if ( $deals=="10" ) {echo ' selected="selected"';} ?>>10</option>
					<option value="15"<?php if ( $deals=="15" ) {echo ' selected="selected"';} ?>>15</option>
					<option value="-1"<?php if ( $deals=="-1" ) {echo ' selected="selected"';} ?>>All</option>
				 </select>
            </label></p>
        <?php
	}
}

// Related Deals Widget
class WPGroupbuy_RelatedDeals extends WP_Widget {

	function WPGroupbuy_RelatedDeals() {
		$widget_ops = array( 'description' => wpg__( 'Display of related deals.' ) );
		parent::__construct( false, $name = wpg__( 'WPG - Related Deals' ), $widget_ops );
	}

	function widget( $args, $instance ) {
		do_action( 'pre_related_deals', $args, $instance );
		global $wp_query, $post;

		extract( $args );
		$title = apply_filters( 'widget_title', @$instance['title'] );
		$buynow = @$instance['buynow'] ? : wpg__('Buy Now');
		$qty = @$instance['deals'];
		$location = '';

		if ( isset( $_COOKIE[ 'wg_location_preference' ] ) && $_COOKIE[ 'wg_location_preference' ] != '' ) {
			$location = $_COOKIE[ 'wg_location_preference' ];
		}
		if ( $location == '' ) {
			$locations = array();
			$terms = get_the_terms( $post->ID, wg_get_deal_location_tax() );
			if ( is_array( $terms ) ) {
				foreach ( $terms as $term ) {
					$locations[] = $term->slug;
				}
			}
			$location = $locations[0];
		}
		if ( $location != '' ) {
			$locations = array();
			$args = array(
				'post_type' => wg_get_deal_post_type(),
				'post_status' => 'publish',
				wg_get_deal_location_tax() => apply_filters( 'wg_related_deals_widget_location', $location, $locations ),
				'meta_query' => array(
					array(
						'key' => '_expiration_date',
						'value' => array( 0, current_time( 'timestamp' ) ),
						'compare' => 'NOT BETWEEN'
					) ),
				'posts_per_page' => $qty
			);
			if ( is_single() ) {
				$args = array_merge( $args, array( 'post__not_in' => array( $wp_query->post->ID ) ) );
			}

			$related_deal_query = new WP_Query( apply_filters( 'wg_related_deals_widget_args', $args) );
			if ( $related_deal_query->have_posts() ) {
				echo $before_widget;
				echo $before_title . $title . $after_title;
					while ( $related_deal_query->have_posts() ) : $related_deal_query->the_post();

						WP_Groupbuy_Controller::load_view( 'widgets/related-deals.php', array( 'buynow'=>$buynow ) );

					endwhile;
				echo $after_widget;
			}
			wp_reset_query();
			do_action( 'post_related_deals', $args, $instance );
		}
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['buynow'] = strip_tags( $new_instance['buynow'] );
		$instance['deals'] = strip_tags( $new_instance['deals'] );
		return $instance;
	}

	function form( $instance ) {
		$title = esc_attr( @$instance['title'] );
		$buynow = esc_attr( @$instance['buynow'] );
		$deals = esc_attr( @$instance['deals'] );
?>
            <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php wpg_e( 'Title:' ); ?> <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></label></p>
            <p><label for="<?php echo $this->get_field_id( 'buynow' ); ?>"><?php wpg_e( 'Buy now link text:' ); ?> <input class="widefat" id="<?php echo $this->get_field_id( 'buynow' ); ?>" name="<?php echo $this->get_field_name( 'buynow' ); ?>" type="text" value="<?php echo $buynow; ?>" /></label></p>
            <p><label for="<?php echo $this->get_field_id( 'deals' ); ?>"><?php wpg_e( 'Number of deals to display:' ); ?>
            	<select id="<?php echo $this->get_field_id( 'deals' ); ?>" name="<?php echo $this->get_field_name( 'deals' ); ?>">
					<option value="1">1</option>
					<option value="2"<?php if ( $deals=="2" ) {echo ' selected="selected"';} ?>>2</option>
					<option value="3"<?php if ( $deals=="3" ) {echo ' selected="selected"';} ?>>3</option>
					<option value="4"<?php if ( $deals=="4" ) {echo ' selected="selected"';} ?>>4</option>
					<option value="5"<?php if ( $deals=="5" ) {echo ' selected="selected"';} ?>>5</option>
					<option value="10"<?php if ( $deals=="10" ) {echo ' selected="selected"';} ?>>10</option>
					<option value="15"<?php if ( $deals=="15" ) {echo ' selected="selected"';} ?>>15</option>
					<option value="-1"<?php if ( $deals=="-1" ) {echo ' selected="selected"';} ?>>All</option>
				 </select>
            </label></p>
        <?php
	}

}


// Upcoming Deals Widget
class WPGroupbuy_UpcomingDeals extends WP_Widget {

	function WPGroupbuy_UpcomingDeals() {
		$widget_ops = array('title' => 'Upcoming deals','DisplayDate' => '','DisplayContent' => '');
		parent::__construct( false, $name = wpg__( 'WPG - Upcoming Deals' ), $widget_ops );
	}

	function widget( $args, $instance ) {
		
		extract( $args );
		extract( $instance );

		$query = new WP_Query(array('post_type' => 'wg_deal', 'post_status' => 'future', 'showposts' => 5 ));
		
		if ($query->have_posts()) {
			echo $before_widget;
			echo $before_title . $title . $after_title;
			
			while ($query->have_posts()) : $query->the_post();
				WP_Groupbuy_Controller::load_view( 'widgets/upcoming-deals.php', array( ) );
			endwhile;
			
			echo $after_widget;
		}
	}
	
	function form( $instance ) { //FIX
		/*$title = esc_attr( @$instance['title'] );
		$DisplayDate = esc_attr( @$instance['DisplayDate'] );
		$DisplayContent = esc_attr( @$instance['DisplayContent'] );
		$options = get_option("UCDL");*/
		
		$options = array('title' => 'Upcoming deals','DisplayDate' => '','DisplayContent' => '');
	
		if ($_POST['UCDL-Submit'])
		{
			$options['title'] = htmlspecialchars($_POST['UCDL-WidgetTitle']);
			$options['DisplayDate'] = $_POST['UCDL-WidgetDate'];
			$options['DisplayContent'] = $_POST['UCDL-WidgetContent'];		
			update_option("UCDL", $options);
		}
	
		?>
		<p><label for="UCDL-WidgetTitle">Title: 
                <input type="text" id="UCDL-WidgetTitle" name="UCDL-WidgetTitle" value="<?php echo $options['title'];?>"/>
            </label></p>
        <p><label for="UCDL-WidgetDate">Show date:
                <input type="checkbox" id="UCDL-WidgetDate" name="UCDL-WidgetDate" <?php if($options['DisplayDate']==true){ echo 'checked=checked';} ?> value="true" />
            </label></p>
        <p><label for="UCDL-WidgetContent">Show content:
                <input type="checkbox" id="UCDL-WidgetContent" name="UCDL-WidgetContent" <?php if($options['DisplayContent']==true){ echo 'checked=checked';} ?> value="true" />
            </label></p>
        <p><input type="hidden" id="UCDL-Submit" name="UCDL-Submit" value="1" /></p>
	<?php
	}
}

//  homepage banners on left and right side
class WPGroupbuy_HomepageBanners extends WP_Widget {
    function WPGroupbuy_HomepageBanners() {
        $widget_ops = array('bannerRightURL' => '',
                            'bannerRightText' => '');
        parent::__construct( false, $name = wpg__( 'WPG - Homepage Banner URLs' ), $widget_ops );
    }

    function widget( $args, $instance ) {

        $hpbarray = get_option('HPB');

        echo $before_widget;
        $bannerRight = '';

        
        if(!empty($hpbarray['bannerRightURL']) && !empty($hpbarray['bannerRightText'])) {
            $bannerRight = '<div class="link-prevzem"> <a href="'. $hpbarray['bannerRightURL'] .'" target="_blank">' .$hpbarray['bannerRightText']. '</a></div>';
        }

        echo $bannerRight;

        echo $after_widget;
    }

    function form( $instance ) {

        $options = array('bannerURL' => '');

        
        $bannerRightURL = esc_attr( @$instance['bannerRightURL'] );
        $bannerRightText = esc_attr( @$instance['bannerRightText'] );

        if ($_POST['HPB-Submit'])
        {
            $options['bannerRightURL'] = $_POST['homepage-banner-right-url'];
            $options['bannerRightText'] = $_POST['homepage-banner-right-text'];
            update_option("HPB", $options);
        } else {
            $hpbarray = get_option('HPB');
            if(!empty($hpbarray)) {
                $options['bannerRightURL'] = $hpbarray['bannerRightURL'];
                $options['bannerRightText'] = $hpbarray['bannerRightText'];
            }
        }

        ?>
        <p>
            <label for="homepage-banner-url">Homepage Right URL:
                <input type="text" id="homepage-banner-url" name="homepage-banner-right-url" value="<?php echo $options['bannerRightURL'];?>" size="55"/>
            </label>
        </p>
        <p>
            <label for="homepage-banner-text">Homepage Right Text:
                <input type="text" id="homepage-banner-text" name="homepage-banner-right-text" value="<?php echo $options['bannerRightText'];?>" size="55"/>
            </label>
        </p>
        <p><input type="hidden" id="HPB-Submit" name="HPB-Submit" value="1" /></p>
        <?php
    }
}