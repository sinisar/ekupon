<?php

class WP_Groupbuy_Accounts_Registration extends WP_Groupbuy_Controller {
	const REGISTER_PATH_OPTION = 'wg_account_register_path';
	const MINIMAL_REGISTRATION_OPTION = 'wg_minimal_registration';
	const REGISTER_QUERY_VAR = 'wg_account_register';
	const FORM_ACTION = 'wg_account_register';
	private static $register_path = 'account/register';
	private static $minimal_registration;
	private static $instance;
	private static $on_registration_page = FALSE;
	public static function init() {
		add_action( 'admin_init', array( get_class(), 'register_settings_fields' ), 10, 1 );
		self::$register_path = get_option( self::REGISTER_PATH_OPTION, self::$register_path );
		self::$minimal_registration = get_option( self::MINIMAL_REGISTRATION_OPTION, 'FALSE' );
		self::register_path_callback( self::$register_path, array( get_class(), 'on_registration_page' ), self::REGISTER_QUERY_VAR, 'account/register' );
	}
	public static function register_settings_fields() {
		$page = WP_Groupbuy_UI::get_settings_page();
		$section = 'WG_URL_paths';
		// Settings
		register_setting( $page, self::REGISTER_PATH_OPTION );
		// Fields
		add_settings_field( self::REGISTER_PATH_OPTION, self::__( 'Account Registration Path' ), array( get_class(), 'display_account_register_path' ), $page, $section );
		$section = 'wg_registration_settings';
		add_settings_section( $section, self::__( ' ' ), array( get_class(), 'display_settings_section' ), $page );
		// Settings
		register_setting( $page, self::MINIMAL_REGISTRATION_OPTION );
		// Fields
		add_settings_field( self::MINIMAL_REGISTRATION_OPTION, self::__( 'Registration Form' ), array( get_class(), 'display_registration_mini_option' ), $page, $section );
	}
	public static function display_registration_mini_option() {
		echo '<label><input type="radio" name="'.self::MINIMAL_REGISTRATION_OPTION.'" value="FALSE" '.checked( 'FALSE', self::$minimal_registration, FALSE ).'/> '.self::__( 'Full Registration form with all contact fields' ).'</label><br />';
		echo '<label><input type="radio" name="'.self::MINIMAL_REGISTRATION_OPTION.'" value="TRUE" '.checked( 'TRUE', self::$minimal_registration, FALSE ).'/> '.self::__( 'Minimal Registration form with Username, E-Mail and Password.' ).'</label><br />';
	}
	public static function display_account_register_path() {
		echo trailingslashit( get_home_url() ) . ' <input type="text" name="' . self::REGISTER_PATH_OPTION . '" id="' . self::REGISTER_PATH_OPTION . '" value="' . esc_attr( self::$register_path ) . '" size="40"/><br />';
	}
	public static function on_registration_page() {
		self::$on_registration_page = TRUE;
		// Registered users shouldn't be here. Send them elsewhere
		if ( get_current_user_id() ) {
			wp_redirect( WP_Groupbuy_Accounts::get_url(), 303 );
			exit();
		}
		if ( !get_option( 'users_can_register' ) ) {
			self::set_message( self::__( 'Registration Disabled.' ), self::MESSAGE_STATUS_ERROR );
			wp_redirect( add_query_arg( array( 'registration' => 'disabled' ), home_url() ) );
			exit();
		}
		self::get_instance(); // make sure the class is instantiated
	}
	public static function get_url() {
		if ( self::using_permalinks() ) {
			return trailingslashit( home_url() ).trailingslashit( self::$register_path );
		} else {
			return add_query_arg( self::REGISTER_QUERY_VAR, 1, home_url() );
		}
	}
	private function __clone() {
		trigger_error( __CLASS__.' may not be cloned', E_USER_ERROR );
	}
	private function __sleep() {
		trigger_error( __CLASS__.' may not be serialized', E_USER_ERROR );
	}
	public static function get_instance() {
		if ( !( self::$instance && is_a( self::$instance, __CLASS__ ) ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	private function __construct() {
		self::do_not_cache(); // never cache the account pages
		if ( isset( $_POST['wg_account_action'] ) && $_POST['wg_account_action'] == self::FORM_ACTION ) {
			$this->process_form_submission();
		}
		add_action( 'pre_get_posts', array( $this, 'edit_query' ), 10, 1 );
		add_action( 'the_post', array( $this, 'view_registration_form' ), 10, 1 );
		add_filter( 'the_title', array( $this, 'get_title' ), 10, 2 );
		add_filter( 'wg_account_registration_panes', array( $this, 'get_panes' ), 0, 1 );
	}
	private function process_form_submission() {
		$errors = array();
		$email_address = isset( $_POST['wg_user_email'] )?$_POST['wg_user_email']:'';
		$username = isset( $_POST['wg_user_login'] )?$_POST['wg_user_login']:$email_address;
		$password = isset( $_POST['wg_user_password'] )?$_POST['wg_user_password']:'';
		$password2 = isset( $_POST['wg_user_password2'] )?$_POST['wg_user_password2']:'';
		$mailing_list_array_ids = isset( $_POST['wg_user_mailinglists'] )?$_POST['wg_user_mailinglists']:array();

		$errors = array_merge( $errors, $this->validate_user_fields( $username, $email_address, $password, $password2, $mailing_list_array_ids ) );
		if ( self::$minimal_registration == 'FALSE' ) {
			$errors = array_merge( $errors, $this->validate_contact_info_fields( $_POST ) );
		}
		$errors = apply_filters( 'wg_validate_account_registration', $errors, $username, $email_address, $_POST );
		if ( $errors ) {
			foreach ( $errors as $error ) {
				self::set_message( $error, self::MESSAGE_STATUS_ERROR );
			}
			return FALSE;
		} else {
			$sanitized_user_login = sanitize_user( $username );
			$user_email = apply_filters( 'user_registration_email', $email_address );
			$password = isset( $_POST['wg_user_password'] )?$_POST['wg_user_password']:'';
			$mailing_list_array_ids = isset( $_POST['wg_user_mailinglists'] )?$_POST['wg_user_mailinglists']:array();
			//	create new user, and add additional info to posts and postsmeta !
			$user_id = $this->create_user( $sanitized_user_login, $user_email, $password, $_POST );
			if ( $user_id ) {
				$user = wp_signon(
					array(
						'user_login' => $sanitized_user_login,
						'user_password' => $password,
						'remember' => false
					), false );
				//	create subscriptions
				//print "before running into deep shit: ";
				$this->add_subscriber($user_id, $user_email, $sanitized_user_login, $mailing_list_array_ids);
				//break;
				do_action( 'wg_registration', $user, $sanitized_user_login, $user_email, $password, $_POST );
				if ( self::$on_registration_page ) {
					if ( isset( $_REQUEST['redirect_to'] ) && !empty( $_REQUEST['redirect_to'] ) ) {
						$redirect = str_replace( home_url(), '', $_REQUEST['redirect_to'] ); // in case the home_url is already added
						$url = home_url( $redirect );
					} else {
						$url = wg_get_last_viewed_redirect_url();
					}
					wp_redirect( apply_filters( 'wg_registration_redirect', $url ), 303 );
					exit();
				} else {
					wp_set_current_user( $user->ID );
				}
			}
		}
	}
	private function validate_user_fields( $username, $email_address, $password, $password2, $mailing_list_array_ids ) {
		$errors = new WP_Error();
		if ( is_multisite() && WG_IS_AUTHORIZED_WPMU_SITE ) {
			$validation = wpmu_validate_user_signup( $username, $email_address );
			if ( $validation['errors']->get_error_code() ) {
				$errors = apply_filters( 'registration_errors_mu', $validation['errors'] );
			}
		} else { // Single-site install, so we don't have the wpmu functions
			// This is mostly just copied from register_new_user() in wp-login.php
			$sanitized_user_login = sanitize_user( $username );
			$user_email = apply_filters( 'user_registration_email', $email_address );
			if ( $password2 == '' )
				$password2 = $password;
			// check Password
			if ( $password == '' || $password2 == '' ) {
				$errors->add( 'empty_password', __( 'Please enter a password.' ) );
			} elseif ( $password != $password2 ) {
				$errors->add( 'password_mismatch', __( 'Passwords did not match.' ) );
			}
			// Check the username
			if ( $sanitized_user_login == '' ) {
				$errors->add( 'empty_username', __( 'Please enter a username.' ) );
			} elseif ( ! validate_username( $username ) ) {
				$errors->add( 'invalid_username', __( 'This username is invalid because it uses illegal characters. Please enter a valid username.' ) );
				$sanitized_user_login = '';
			} elseif ( username_exists( $sanitized_user_login ) ) {
				$errors->add( 'username_exists', __( 'This username is already registered, please choose another one.' ) );
			}
			// Check the e-mail address
			if ( $user_email == '' ) {
				$errors->add( 'empty_email', __( 'Please type your e-mail address.' ) );
			} elseif ( ! is_email( $user_email ) ) {
				$errors->add( 'invalid_email', __( 'The email address isn&#8217;t correct.' ) );
				$user_email = '';
			} elseif ( email_exists( $user_email ) ) {
				$errors->add( 'email_exists', __( 'This email is already registered, please choose another one.' ) );
			}

			// Check the mailing lists
			if(count($mailing_list_array_ids) == 0) {
				$errors->add( 'empty_mailinglist', __( 'Please check at least one mailinglist.' ) );
			}

			do_action( 'register_post', $sanitized_user_login, $user_email, $password, $password2, $mailing_list_array_ids, $errors );
			$errors = apply_filters( 'registration_errors', $errors, $sanitized_user_login, $user_email, $password, $password2, $mailing_list_array_ids );
		}
		if ( $errors->get_error_code() ) {
			return $errors->get_error_messages();
		} else {
			return array();
		}
	}
	private function validate_contact_info_fields( $submitted ) {
		$errors = array();
		$fields = $this->contact_info_fields();
		foreach ( $fields as $key => $data ) {
			if ( isset( $data['required'] ) && $data['required'] && !( isset( $submitted['wg_contact_'.$key] ) && $submitted['wg_contact_'.$key] != '' ) ) {
				$errors[] = sprintf( self::__( '"%s" field is required.' ), $data['label'] );
			}
		}
		return $errors;
	}
	public function create_user( $username, $email_address, $password = '', $submitted = array() ) {

		$submitted = array_merge( $submitted, $_POST );

		$password = ( $password != '' ) ? $password: wp_generate_password( 12, false );
		$username = ( !empty( $username ) ) ? $username : $email_address;

		$first_name = @$submitted['wg_contact_first_name'] ? : @$submitted['wg_billing_first_name'];
		$last_name = @$submitted['wg_contact_last_name'] ? : @$submitted['wg_billing_last_name'];
		$account_phone = @$submitted['wg_contact_phone'] ? : @$submitted['wg_billing_phone'];
		$account_email = @$submitted['wg_contact_email'] ? : @$submitted['wg_billing_email'];
		
		$userdata = array(
		    'user_login'  =>  $username,
		    'user_email'    =>  $email_address,
		    'user_pass'   =>  $password,
		    'first_name' => $first_name,
		    'last_name' => $last_name,
		);

		$user_id = wp_insert_user( $userdata );
		if ( !$user_id || is_wp_error( $user_id ) ) {
			self::set_message( self::__( $user_id->get_error_message() ) );
			return FALSE;
		}
		// Set contact info for the new account
		$account = WP_Groupbuy_Account::get_instance( $user_id );
		if ( is_a( $account, 'WP_Groupbuy_Account' ) ) {
			$account->set_name( 'first', $first_name );
			$account->set_name( 'last', $last_name );
			$account->set_name( 'phone', $account_phone );
			$account->set_name( 'email', $account_email );
			$address = array(
				'street' => @$submitted['wg_contact_street'] ? : $submitted['wg_billing_street'],
				'city' => @$submitted['wg_contact_city'] ? : $submitted['wg_billing_city'],
				'zone' => @$submitted['wg_contact_zone'] ? : $submitted['wg_billing_zone'],
				'postal_code' => @$submitted['wg_contact_postal_code'] ? : $submitted['wg_billing_postal_code'],
				'country' => @$submitted['wg_contact_country'] ? : $submitted['wg_billing_country'],
			);
			$account->set_address( $address );
		}
		wp_new_user_notification( $user_id );
		do_action( 'wg_account_created', $user_id, $_POST, $account );
		return $user_id;
	}
	// add user to mailing list when registrating
	public function add_subscriber($user_id, $user_email, $sanitized_user_login, $mailing_list_array_ids) {
		//	subscribe user
		$data = array();
		$data['registered'] ='Y';
		$data['user_id'] = $user_id;
		$data['email'] = $user_email;
		$data['username'] = $sanitized_user_login;

		$subscriber = new wpmlSubscriber($data);
		$subscriber->save($data, TRUE);
        //  load subscriber id not user id
        $subscriber = $subscriber->get_by_email($user_email);
        $subscriber_id = $subscriber[0]->id;



		//	subsribers mailinglists
		foreach($mailing_list_array_ids as $nr => $id) {
			$data = array();
			$data['list_id']= $id;
			$data['subscriber_id'] = $subscriber_id;
			$data['active'] = 'Y';

			$subscribersList = new wpmlSubscribersList($data);
			$subscribersList->save($data, TRUE, FALSE);

		}
		/*
        print "user_id: ".$user_id;
        print "subscriber_id: ".$subscriber_id;
        print_r($subscriber);
        break;
		*/
	}
	// Edit the query on the registration page to select the user's account.
	public function edit_query( WP_Query $query ) {
		if ( $query->is_main_query() && isset( $query->query_vars[self::REGISTER_QUERY_VAR] ) && $query->query_vars[self::REGISTER_QUERY_VAR] ) {
			$query->query_vars['post_type'] = WP_Groupbuy_Account::POST_TYPE;
			$query->query_vars['p'] = WP_Groupbuy_Account::get_account_id_for_user();
		}
	}
	// Update the global $pages array with the HTML for the page.
	public function view_registration_form( $post ) {
		if ( $post->post_type == WP_Groupbuy_Account::POST_TYPE ) {
			remove_filter( 'the_content', 'wpautop' );
			$panes = apply_filters( 'wg_account_registration_panes', array() );
			uasort( $panes, array( get_class(), 'sort_by_weight' ) );
			$args = array();
			if ( isset( $_GET['redirect_to'] ) ) {
				$redirect = str_replace( home_url(), '', $_GET['redirect_to'] );
				$args['redirect'] = $redirect;
			}
			$view = self::load_view_to_string( 'account/register', array( 'panes'=>$panes, 'args' => $args ) );
			global $pages;
			$pages = array( $view );
		}
	}
	public static function get_registration_form() {
		$registration = WP_Groupbuy_Accounts_Registration::get_instance(); // make sure the class is instantiated
		$panes = apply_filters( 'wg_account_registration_panes', array() );
		uasort( $panes, array( get_class(), 'sort_by_weight' ) );
		$args = array();
		if ( isset( $_GET['redirect_to'] ) ) {
			$redirect = str_replace( home_url(), '', $_GET['redirect_to'] );
			$args['redirect'] = $redirect;
		}
		return self::load_view_to_string( 'account/register', array( 'panes'=>$panes, 'args' => $args ) );
	}
	// Get the panes for the registration page
	public function get_panes( array $panes ) {
		$panes['user'] = array(
			'weight' => 0,
			'body' => $this->user_pane(),
		);
		if ( self::$minimal_registration == 'FALSE' ) {
			$panes['contact_info'] = array(
				'weight' => 10,
				'body' => $this->contact_info_pane(),
			);
		}
		$panes['controls'] = array(
			'weight' => 100,
			'body' => $this->load_view_to_string( 'account/register-controls', array() ),
		);
		return $panes;
	}
	private function user_pane() {
		return $this->load_view_to_string( 'account/register-user', array( 'fields' => $this->user_info_fields() ) );
	}
	private function user_info_fields() {
		$fields = array();
		$fields['login'] = array(
			'weight' => 0,
			'label' => self::__( 'Username' ),
			'type' => 'text',
			'required' => TRUE,
		);
		$fields['email'] = array(
			'weight' => 5,
			'label' => self::__( 'Email Address' ),
			'type' => 'text',
			'required' => TRUE,
		);
		$fields['password'] = array(
			'weight' => 10,
			'label' => self::__( 'Password' ),
			'type' => 'password',
			'required' => TRUE,
		);
		$fields['password2'] = array(
			'weight' => 15,
			'label' => self::__( 'Confirm Password' ),
			'type' => 'password',
			'required' => TRUE,
		);
        $fields['mailinglists[]'] = array(
            'weight' => 20,
            'label' => self::__('Mailing lists'),
            'type' => 'checkboxes',
            'options' => get_all_mailinglist_as_array(array('conditions' => array('privatelist' => 'N'))),
            'required' => TRUE,
        );
        //  print "mailinglis:";
		//  print_r(get_all_mailinglist_as_array());

		$fields = apply_filters( 'wg_account_register_user_fields', $fields );
		uasort( $fields, array( get_class(), 'sort_by_weight' ) );
		return $fields;
	}
	private function contact_info_pane() {
		return $this->load_view_to_string( 'account/register-contact-info', array( 'fields' => $this->contact_info_fields() ) );
	}
	private function contact_info_fields() {
		$fields = $this->get_standard_address_fields();
		$fields = apply_filters( 'wg_account_register_contact_info_fields', $fields );
		uasort( $fields, array( get_class(), 'sort_by_weight' ) );
		return $fields;
	}
	// Filter 'the_title' to display the title of the page rather than the user name
	public function get_title(  $title, $post_id  ) {
		$post = get_post( $post_id );
		if ( $post->post_type == WP_Groupbuy_Account::POST_TYPE ) {
			return self::__( "Register" );
		}
		return $title;
	}
}
