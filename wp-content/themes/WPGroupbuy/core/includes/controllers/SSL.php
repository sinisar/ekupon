<?php

add_action( 'wg_init_controllers', array( 'WPG_SSL', 'init' ), 21 );

class WPG_SSL extends WP_Groupbuy_Controller {
	private static $use_ssl = false;

	public static function init() {
		self::$use_ssl = get_option( 'wg_checkout_use_ssl', false );
		add_filter( 'wpg_require_ssl', array( get_class(), 'require_ssl_on_checkout_pages' ), 10, 2 );
		add_filter( 'wg_ssl_url', array( get_class(), 'url_ssl_replace' ) );
		add_action( 'admin_init', array( get_class(), 'register_settings_fields' ), 20, 0 );
	}

	// The URL to the checkout page
	public static function url_ssl_replace( $url ) {
		if ( self::$use_ssl ) {
			$url = str_replace('http://', 'https://', $url);
		}
		return $url;
	}

	public static function require_ssl_on_checkout_pages( $required, WP $wp ) {
		if ( self::$use_ssl && isset( $wp->query_vars[WP_Groupbuy_Checkouts::CHECKOUT_QUERY_VAR] ) && $wp->query_vars[WP_Groupbuy_Checkouts::CHECKOUT_QUERY_VAR] ) {
			return TRUE;
		}
		return $required;
	}

	public static function register_settings_fields() {
		register_setting( WP_Groupbuy_Payment_Processors::get_settings_page(), 'wg_checkout_use_ssl' );
		add_settings_field( 'wg_checkout_use_ssl', self::__( 'Enable SSL (Secure Socket Layer)' ), array( get_class(), 'display_use_ssl_option' ), WP_Groupbuy_Payment_Processors::get_settings_page() );
	}

	public static function display_use_ssl_option() {
		printf( '<label><input type="checkbox" value="1" name="%s" id="%s" %s /> %s</label>', 'wg_checkout_use_ssl', 'wg_checkout_use_ssl', checked( self::$use_ssl, TRUE, FALSE ), __( 'Enable' ) );
		echo '<img width="16" height="16" src="'.WG_RESOURCES . 'images/help.png'. '" class="help_tip" title="'.self::__( 'Purchases will be managed in a more secured way with SSL (requires purchase of an SSL certificate if this option is in disabled state). SSL is highly recommended for production sites accepting Credit Card' ).'">';
	}
}
