<?php

class WP_Groupbuy_Accounts_Retrieve_Password extends WP_Groupbuy_Controller {
	const RP_PATH_OPTION = 'wg_account_rp_path';
	const RP_QUERY_VAR = 'wg_account_rp';
	private static $rp_path = 'account/retrievepassword';
	private static $instance;
	public static function init() {
		self::$rp_path = get_option( self::RP_PATH_OPTION, self::$rp_path );
		self::register_path_callback( self::$rp_path, array( get_class(), 'on_rp_page' ), self::RP_QUERY_VAR, 'account/retrievepassword' );
		// Replace WP Login URIs
		add_filter( 'lostpassword_url', array( get_class(), 'get_url' ), 10, 2 );
		add_action( 'admin_init', array( get_class(), 'register_settings_fields' ), 10, 1 );
		add_action( 'parse_request', array( get_class(), 'check_messages' ) );
	}
	public static function register_settings_fields() {
		$page = WP_Groupbuy_UI::get_settings_page();
		$section = 'WG_URL_paths';
		// Settings
		register_setting( $page, self::RP_PATH_OPTION );
		add_settings_field( self::RP_PATH_OPTION, self::__( 'Forgot Password Path' ), array( get_class(), 'display_account_login_path' ), $page, $section );
	}
	public static function display_account_login_path() {
		echo trailingslashit( get_home_url() ) . ' <input type="text" name="' . self::RP_PATH_OPTION . '" id="' . self::RP_PATH_OPTION . '" value="' . esc_attr( self::$rp_path ) . '" size="40"/><br />';
	}
	public static function on_rp_page() {
		// Registered users shouldn't be here. Send them elsewhere
		if ( get_current_user_id() && !self::log_out_attempt() ) {
			wp_redirect( WP_Groupbuy_Accounts::get_url(), 303 );
			exit();
		}
		if ( isset( $_GET['key'] ) ) {
			if ( self::reset_password( $_GET['key'] ) ) {
				self::set_message( __( 'Password Reset Successful.' ), self::MESSAGE_STATUS_INFO );
				wp_redirect( add_query_arg( array( 'checkemail' => 'newpass', 'message' => 'newpass' ), WP_Groupbuy_Accounts_Edit_Profile::get_url() ) );
				exit();
			}
			wp_redirect( add_query_arg( array( 'error' => 'invalidkey', 'action' => 'lostpassword', 'message' => 'invalidkey' ), self::get_url() ) );
			exit();
		}
		elseif ( 'POST' == $_SERVER['REQUEST_METHOD'] ) {
			$message = self::retrieve_password();
			if ( $message == 'newpass' ) {
				wp_redirect( add_query_arg( array( 'message' => $message ), WP_Groupbuy_Accounts_Login::get_url() ) );
			} else {
				wp_redirect( add_query_arg( array( 'message' => $message ), self::get_url() ) );
			}
			exit();
		}
		self::get_instance(); // make sure the class is instantiated
	}
	public static function log_out_attempt() {
		return ( isset( $_GET['action'] ) && 'logout' == $_GET['action'] ) ? TRUE : FALSE;
	}
	public static function get_url() {
		if ( self::using_permalinks() ) {
			return trailingslashit( home_url() ).trailingslashit( self::$rp_path );
		} else {
			return add_query_arg( self::RP_QUERY_VAR, 1, home_url() );
		}
	}
	// Handles sending password retrieval email to user.
	private static function retrieve_password() {
		global $wpdb;
		if ( empty( $_POST['user_login'] ) ) {
			return 'blank';
		}
		if ( strpos( $_POST['user_login'], '@' ) ) {
			$user_data = get_user_by( 'email', trim( $_POST['user_login'] ) );
			if ( empty( $user_data ) )
				return 'incorrect';
		}
		if ( !$user_data || empty( $user_data ) ) {
			$login = trim( $_POST['user_login'] );
			$user_data = get_userdatabylogin( $login );
		}
		if ( !$user_data ) {
			return 'incorrect';
		}
		// redefining user_login ensures we return the right case in the email
		$user_login = $user_data->user_login;
		$user_email = $user_data->user_email;
		//	if user has salt, delete ir !
		$user_salt = get_user_meta($user_data->ID, '_user_salt', true);
        //print "user_salt: ".$user_salt;
        if(!empty($user_salt)) {
            //  reset salt
            delete_user_meta($user_data->ID, '_user_salt', '');
            //print "reset: ". get_user_meta($user_data->ID, '_user_salt', true);
        }


		do_action( 'retrieve_password', $user_login );
		$allow = apply_filters( 'allow_password_reset', TRUE, $user_data->ID );
		if ( !$allow ) {
			return 'notallowed';
		}
		$key = $wpdb->get_var( $wpdb->prepare( "SELECT user_activation_key FROM $wpdb->users WHERE user_login = %s", $user_login ) );
		if ( empty( $key ) ) {
			// Generate something random for a key...
			$key = wp_generate_password( 20, false );
			do_action( 'wg_retrieve_password_key', $user_login, $key );
			// Now insert the new md5 key into the db
			$wpdb->update( $wpdb->users, array( 'user_activation_key' => $key ), array( 'user_login' => $user_login ) );
		}
		$data = array(
			'key' => $key,
			'user' => $user_data
		);
		do_action( 'wg_retrieve_password_notification', $data );
		return 'newpass';
	}
	// Handles resetting the user's password.
	private static function reset_password( $key ) {
		global $wpdb;
		$key = preg_replace( '/[^a-z0-9]/i', '', $key );
		if ( empty( $key ) || is_array( $key ) )
			return false;
		$user = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $wpdb->users WHERE user_activation_key = %s", $key ) );
		if ( empty( $user ) )
			return false;
		// Generate random password w/o those special_chars that we all hate
		$new_pass = wp_generate_password( 8, false );
		do_action( 'password_reset', $user, $new_pass );
		wp_set_password( $new_pass, $user->ID );
		$user = wp_signon(
			array(
				'user_login' => $user->user_login,
				'user_password' => $new_pass,
				'remember' => false
			), false );
		$data = array(
			'user' => $user,
			'new_pass' => $new_pass
		);
		do_action( 'wg_password_reset_notification', $data );
		wp_password_change_notification( $user );
		return true;
	}
	private function __clone() {
		trigger_error( __CLASS__.' may not be cloned', E_USER_ERROR );
	}
	private function __sleep() {
		trigger_error( __CLASS__.' may not be serialized', E_USER_ERROR );
	}
	public static function get_instance() {
		if ( !( self::$instance && is_a( self::$instance, __CLASS__ ) ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	private function __construct() {
		self::do_not_cache(); // never cache the account pages
		add_action( 'pre_get_posts', array( $this, 'edit_query' ), 10, 1 );
		add_action( 'the_post', array( $this, 'view_rp_form' ), 10, 1 );
		add_filter( 'the_title', array( $this, 'get_title' ), 10, 2 );
	}
	public static function check_messages() {
		$messages = array(
			'test_cookie' => self::__( "Cookies are blocked or not supported by your browser. You must <a href='http://www.google.com/cookies.html'>enable cookies</a> to use this site." ),
			'confirm' => self::__( 'Check your e-mail for the password reset link.' ),
			'newpass' => self::__( 'Check your e-mail for your new password.' ),
			'incorrect' => self::__( 'Your username or email is incorrect' ),
			'notallowed' => self::__( 'Password reset is not allowed.' ),
			'blank' => self::__( 'Your username or email is incorrect' ),
			'invalidkey' => self::__( 'Invalid Password Reset Key.' ),
		);
		if ( isset( $_GET['message'] ) && isset( $messages[$_GET['message']] ) ) {
			self::set_message( $messages[$_GET['message']], self::MESSAGE_STATUS_ERROR );
		}
	}
	// Edit the query on the reset password page
	public function edit_query( WP_Query $query ) {
		if ( isset( $query->query_vars[self::RP_QUERY_VAR] ) && $query->query_vars[self::RP_QUERY_VAR] ) {
			$query->query_vars['post_type'] = WP_Groupbuy_Account::POST_TYPE;
			$query->query_vars['p'] = WP_Groupbuy_Account::get_account_id_for_user();
		}
	}
	// Update the global $pages array with the HTML for the page.
	public function view_rp_form( $post ) {
		if ( $post->post_type == WP_Groupbuy_Account::POST_TYPE ) {
			remove_filter( 'the_content', 'wpautop' );
			$args = array();
			if ( isset( $_GET['redirect_to'] ) ) {
				$redirect = str_replace( home_url(), '', $_GET['redirect_to'] );
				$args['redirect'] = $redirect;
			}
			$view = self::load_view_to_string( 'account/retrievepassword', array( 'args' => $args ) );
			global $pages;
			$pages = array( $view );
		}
	}
	// Filter 'the_title' to display the title of the page rather than the user name
	public function get_title(  $title, $post_id  ) {
		$post = get_post( $post_id );
		if ( $post->post_type == WP_Groupbuy_Account::POST_TYPE ) {
			return self::__( "Retrieve Password" );
		}
		return $title;
	}
}