<?php

add_action( 'wg_init_controllers', array( 'WP_Groupbuy_Addons', 'init' ), 105 );

class WP_Groupbuy_Addons extends WP_Groupbuy_Controller {
	const ADDONS_SETTING = 'wg_enabled_addons';
	protected static $settings_page;
	protected static $marketplace_settings_page;
	private static $addons = array();

	// Declare all of the addons available
	private static function addon_definitions() {
		if ( self::$addons ) {
			return self::$addons;
		}
		self::$addons['attributes'] = array(
			'label' => self::__( 'Deal Attributes' ),
            'description' => self::__( 'Add attributes (ex: color, size) that customers can choose when buying deals.' ),
            'default' => true,
			'files' => array(
				WG_PATH.'/includes/classes/WPG.Attribute.class.php',
				WG_PATH.'/includes/controllers/WPG.Attributes.class.php',
				WG_PATH.'/templates/template-tags/attributes.php',
			),
			'callbacks' => array(
				array( 'WP_Groupbuy_Attribute', 'init' ),
				array( 'WP_Groupbuy_Attributes', 'init' ),
			),
		);
		self::$addons['dynamic_attribute_selection'] = array(
			'label' => self::__( 'Dynamic Attribute Selection' ),
            'description' => self::__( 'Alow client select attributes (ex: \'size\' and \'color\') easily.' ),
            'default' => true,
			'files' => array(),
			'callbacks' => array(
				array( 'WP_Groupbuy_Attributes', 'activate_dynamic_category_selection' ),
			),
		);
		self::$addons['fulfillment'] = array(
			'label' => self::__( 'Order Fulfillment and Inventory Management.' ),
            'description' => self::__( 'Manage order status and low inventory notifications.' ),
			'files' => array(
				WG_PATH.'/includes/controllers/WPG.Fulfillment.class.php',
				WG_PATH.'/templates/template-tags/fulfillment.php',
			),
			'callbacks' => array(
				array( 'WP_Groupbuy_Fulfillment', 'init' ),
			),
		);
		self::$addons['query_optimization'] = array(
			'label' => self::__( 'MySQL Query Optimization' ),
            'description' => self::__( 'Enable this option to make database queries more efficient by adding additional tables.' ),
			'files' => array(
				WG_PATH.'/includes/controllers/WPG.QueryOptimization.class.php',
			),
			'callbacks' => array(
				array( 'WP_Groupbuy_Query_Optimization', 'init' ),
			),
		);
		
		self::$addons = apply_filters( 'wg_addons', self::$addons );
		return self::$addons;
	}

	// Actions and settings page
	public static function init() {
		self::$settings_page = self::register_settings_page( 'wg_addons', self::__( 'WPG Tools' ), self::__( 'WPG Tools' ), 10000, FALSE, 'addons' );
		add_action( 'init', array( get_class(), 'load_enabled_addons' ), -1, 0 );
		add_action( 'admin_init', array( get_class(), 'register_settings_fields' ), 20, 0 );
		add_action( 'wg_options_shop', array( get_class(), 'shop_view' ), 20 );
	}

	// addon enabled?
	public static function is_enabled( $addon ) {
		$enabled = get_option( self::ADDONS_SETTING, array() );
		if ( isset( $enabled[$addon] ) ) {
			if ( $enabled[$addon] ) {
				return TRUE;
			} 
		} else {
			return !empty( self::$addons[$addon]['default'] );
		}
		return FALSE;
	}

	// Load enabled Addons
	public static function load_enabled_addons() {
		$addons = self::addon_definitions();
		foreach ( $addons as $key => $addon ) {
			if ( self::is_enabled( $key ) ) {
				self::load_addon( $key );
			}
		}
	}

	// Load an addon
	private static function load_addon( $key ) {
		$addons = self::addon_definitions();
		if ( !isset( $addons[$key] ) || !is_array( $addons[$key]['files'] ) ) {
			return;
		}
		foreach ( $addons[$key]['files'] as $file_path ) {
			require_once $file_path;
		}
		foreach ( (array)$addons[$key]['callbacks'] as $callback ) {
			if ( is_callable( $callback ) ) {
				call_user_func( $callback );
			}
		}
	}


	// Inject plugin update information
	public static function site_transient_update_plugins( $trans ) {

		if ( !is_admin() )
			return $trans;

		if ( empty( $trans->checked ) )
			return $trans;

		foreach ( $trans->checked as $plugin => $version ) {

			// get addon data
			$token = basename($plugin, ".php");
			$data = self::get_addon_data( $token );

			if ( $data ) {

				// Add addon upgrade data
				if ( version_compare( $version, $data['new_version'], '<' ) ) {
					$trans->response[$plugin]->url = $data['url'];
					$trans->response[$plugin]->slug = $data['slug'];
					$trans->response[$plugin]->package = $data['download_url'];
					$trans->response[$plugin]->new_version = $data['new_version'];
					$trans->response[$plugin]->id = '0';
				}
			}
		}

		return $trans;
	}

	public static function get_addon_data( $token = 'pull', $query_args = array(), $fresh = FALSE ) {

		$transient_key = 'wpg_' . substr( md5( serialize( $query_args ) ), -60 );

		$addon_data = get_site_transient( $transient_key ); // Look for transient cache
		if ( empty( $addon_data ) || $fresh ) {
			if ( WPG_DEV ) error_log( "get_addon_data not cached: " . print_r( TRUE, true ) );
			$addon_data = self::api_get( 'pull', $query_args, $fresh );
			if ( !$addon_data ) {
				return NULL;
			}
			// Set a transient to cache
			if ( WPG_DEV ) error_log( "set transient: " . print_r( $fresh, true ) );
			set_site_transient( $transient_key, $addon_data, 60*60*24 ); // 60*60*24
		}
		if ( $token != 'pull' ) {
			if ( isset( $addon_data[$token] ) ) {
				return $addon_data[$token];
			}
			return FALSE;
		}
		return $addon_data;
	}

	public static function get_addon_info( $addon = '', $fresh = FALSE ) {

		$transient_key = 'wpg_' . substr( md5( serialize( $addon ) ), -60 );

		$addon_data = get_site_transient( $transient_key ); // Look for transient cache

		if ( !$addon_info || $fresh ) {
			if ( WPG_DEV ) error_log( "get_addon_info not cached: " . print_r( TRUE, true ) );
			$addon_data = self::api_get( 'info', null, $fresh );
			if ( !$addon_data ) {
				return NULL;
			}
			// Set a transient to cache
			set_site_transient( $transient_key, $addon_info, 60*60*24 );
		}

		if ( $addon != '' ) {
			if ( isset( $addon_data[$addon] ) ) {
				return $addon_data[$addon];
			}
			return FALSE;
		}
		return $addon_data;
	}

	public static function get_category_data() {
		$transient_key = 'wpg_addon_categories';
		$categories = get_site_transient( $transient_key ); // Look for transient cache
		if ( is_array(!$categories) ) {
			if ( WPG_DEV ) error_log( "cats not cached: " . print_r( TRUE, true ) );
			$categories = array();
			$category_data = self::api_get( 'categories' );

			// build a useful
			foreach ( $category_data as $tax_key => $tax_value ) {
				if ( !isset( $categories[$tax_key] ) && $tax_value['count'] > 3 ) {
					$categories[$tax_key] = $tax_value;
					$categories[$tax_key]['weight'] = 1000-$tax_value['count']; // set the weight for sorting
				}
			}
			uasort( $categories, array( get_class(), 'sort_by_weight' ) );

			if ( !$categories ) {
				return NULL;
			}
			// Set a transient to cache
			set_site_transient( $transient_key, $categories, 60*60*24 );
		}

		return $categories;
	}

	// Add-ons

	function get_installed_version( $addon_type, $token ) {
		if ( !is_admin() ) return FALSE;

		// Get array of plugins or themes
		$items = ( 'theme' == $addon_type ) ? self::get_themes() : self::get_plugins() ;

		// Return the Version based on token
		if ( isset( $items[$token]['Version'] ) ) {
			return $items[$token]['Version'];
		}

		return FALSE;
	}

	// Utility Functions
	
	public static function get_themes() {
		// Themes are keyed by theme name instead of their directory name
		$themes = wp_get_themes();
		$wp_themes = array();
		foreach ( $themes as $theme ) {
			$key = $theme->get_stylesheet();
			$wp_themes[ $key ] = $theme;
		}
		return $wp_themes;
	}

	public static function get_themes_dep() {
		$themes = wp_get_themes();
		$wp_themes = array();

		foreach ( $themes as $theme ) {
			$name = $theme->get('Name');
			if ( isset( $wp_themes[ $name ] ) )
				$wp_themes[ $name . '/' . $theme->get_stylesheet() ] = $theme;
			else
				$wp_themes[ $name ] = $theme;
		}

		return $wp_themes;
	}

	public static function get_plugins() {
		return get_plugins();
	}

	private static function api_args() {
		$args['sslverify'] = false;
		$args['timeout'] = 30;

		$args['headers'] = array(
			'X_WPG_SITE_URL' => home_url(),
			'Referer' => self::current_url(),
			'User-Agent' => 'WordPress/' . get_bloginfo( 'version' ) . 'WPGVERSION/' .WP_Groupbuy::WG_VERSION
		);

		return $args;
	}

	public static function current_url() {
		$port = ( $_SERVER['SERVER_PORT'] != '80' ) ? ':' . $_SERVER['SERVER_PORT'] : '';
		return sprintf( 'http%s://%s%s%s', is_ssl(), $_SERVER['SERVER_NAME'], $port, $_SERVER['REQUEST_URI'] );
	}

	// Get Settings page
	public static function get_settings_page() {
		return self::$settings_page;
	}

	// Register options
	public static function register_settings_fields() {
		if ( !self::addon_definitions() ) {
			return; // nothing to register
		}
		$page = self::$settings_page;
		$section = 'wg__addons_settings';
		add_settings_section( $section, self::__( 'Add-ons' ), array( get_class(), 'display_settings_section' ), $page );
		// Settings
		register_setting( $page, self::ADDONS_SETTING );

		add_settings_field( self::ADDONS_SETTING, self::__( 'Enable Addons' ), array( get_class(), 'display_addons_options' ), $page, $section );
	}

	// Display all the addons for selection
	public static function display_addons_options() {
		$addons= self::addon_definitions();
		foreach ( $addons as $key => $details ) {
			$description = strpos( @$details['description'], '<img' ) !== false ? @$details['description'] : '<img width="16" height="16" src="'.WG_RESOURCES . 'images/help.png'. '" class="help_tip" title="' . esc_attr( $details['description'] ) . '" />';

			printf( '<input type="hidden" name="%s[%s]" value="%s"/>', self::ADDONS_SETTING, $key, false );
			printf( '<label><input type="checkbox" name="%s[%s]" value="%s" %s /> %s</label>%s<br/>', self::ADDONS_SETTING, $key, true, checked( true, self::is_enabled( $key ), false ), $details['label'], $description );
		}
	}
}
