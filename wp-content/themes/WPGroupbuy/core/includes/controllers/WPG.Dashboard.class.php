<?php

add_action( 'wg_init_classes', array( 'WP_Groupbuy_Dashboard', 'init' ), 10 );

class WP_Groupbuy_Dashboard extends WP_Groupbuy_Controller {
	protected static $settings_page;

	public static function init() {
            self::$settings_page = self::register_settings_page( 'dashboard', self::__( 'Overview' ), self::__( 'Overview' ), 9, FALSE, 'dashboard');
	}

}