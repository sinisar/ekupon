<?php
function wpg_options_setup() {
	global $pagenow;
	if ('media-upload.php' == $pagenow || 'async-upload.php' == $pagenow) {
		// Now we'll replace the 'Insert into Post Button inside Thickbox' 
		add_filter( 'gettext', 'replace_thickbox_text' , 1, 2 );
	}
}
add_action( 'admin_init', 'wpg_options_setup' );

function replace_thickbox_text($translated_text, $text ) {	
	if ( 'Insert into Post' == $text ) {
		$referer = strpos( wp_get_referer(), 'wpg-upload-settings' );
		if ( $referer != '' ) {
			return __('I choose this!', 'wpgroupbuy' );
		}
	}

	return $translated_text;
}

function delete_image( $image_url ) {
	global $wpdb;
	
	// We need to get the image's meta ID..
	$query = "SELECT ID FROM wp_posts where guid = '" . esc_url($image_url) . "' AND post_type = 'attachment'";  
	$results = $wpdb -> get_results($query);

	// And delete them (if more than one attachment is in the Library
	foreach ( $results as $row ) {
		wp_delete_attachment( $row -> ID );
	}	
}

/********************* JAVASCRIPT ******************************/

function wpg_options_enqueue_scripts() {

	wp_enqueue_script('jquery');

	wp_enqueue_script('thickbox');
	wp_enqueue_style('thickbox');

	wp_enqueue_script('media-upload');
	wp_enqueue_script('wpg-upload');
	
}

add_action('admin_enqueue_scripts', 'wpg_options_enqueue_scripts');


function wpg_setting_logo_preview($url_logo) {
	?>
	<div id="upload_logo_preview">
		<img style="max-width:100%; padding-top:5px;" src="<?php echo esc_url( $url_logo ); ?>" />
	</div>
	<?php
}

function wpg_setting_logo($url_logo) {
	?>
		<input id="upload_logo_button" type="button" class="button" value="<?php _e( 'Upload Image', 'wpgroupbuy' ); ?>" />
	<?php
}

function wpg_setting_payment_preview($url_payment) {
	?>
	<div id="upload_payment_preview">
		<img style="max-width:100%; padding-top:5px;" src="<?php echo esc_url( $url_payment ); ?>" />
	</div>
	<?php
}

function wpg_setting_payment($url_payment) {
	?>
		<input id="upload_payment_button" type="button" class="button" value="<?php _e( 'Upload Image', 'wpgroupbuy' ); ?>" />
	<?php
}

function wpg_setting_locations_preview($url_locations) {
	?>
	<img id="upload_locations_preview" name="upload_locations_preview" style="max-width:100%; padding-top:5px;" src="<?php echo esc_url( $url_locations ); ?>" />
	<?php
}

function wpg_setting_locations($url_locations) {
	?>
		<input style="width: 15%; margin-right: 30%;" id="upload_locations_button" type="button" class="button upload_image_button" value="<?php _e( 'Upload Image', 'wpgroupbuy' ); ?>" />
	<?php
}

function wpg_setting_logo_image_url_preview($logo_image_url) {
	?>
	<img id="upload_logo_image_url_preview" name="upload_logo_image_url_preview" style="max-width:100%; padding-top:5px;" src="<?php echo esc_url( $logo_image_url ); ?>" />
	<?php
}

function wpg_setting_logo_image_url($logo_image_url) {
	?>
		<input style="width: 15%; margin-right: 30%;" id="upload_logo_image_url_button" type="button" class="button upload_image_button" value="<?php _e( 'Upload Image', 'wpgroupbuy' ); ?>" />
	<?php
}

?>