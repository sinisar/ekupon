<?php

add_action( 'wg_init_classes', array( 'WP_Groupbuy_Deal', 'init' ), 5 );

class WP_Groupbuy_Deal extends WP_Groupbuy_Post_Type {
	const POST_TYPE = 'wg_deal';
	const REWRITE_SLUG = 'deals';
	const LOCATION_TAXONOMY = 'wg_location';
	const CAT_TAXONOMY = 'wg_category';
	const TAG_TAXONOMY = 'wg_tag';

	const DEAL_STATUS_OPEN = 'open';
	const DEAL_STATUS_PENDING = 'pending';
	const DEAL_STATUS_CLOSED = 'closed';
	const DEAL_STATUS_UNKNOWN = 'unknown';

	const NO_EXPIRATION_DATE = -1;
	const NO_MAXIMUM = -1;
	const MAX_LOCATIONS = 5;

	private static $instances = array();

	private static $meta_keys = array(
		'amount_saved' => '_amount_saved',
        'show_amount_saved' => '_show_amount_saved',
		'capture_before_expiration' => '_capture_before_expiration', 
		'dynamic_price' => '_dynamic_price', 
		'expiration_date' => '_expiration_date', 
		'fine_print' => '_fine_print', 
		'highlights' => '_highlights', 
		'max_purchases' => '_max_purchases', 
		'max_purchases_per_user' => '_max_purchases_per_user', 
		'merchant_id' => '_merchant_id', 
		'min_purchases' => '_min_purchases', 
		'number_of_purchases' => '_number_of_purchases', 
		'redirect_url' => '_redirect_url', 
		'price' => '_base_price', 
		'coupon_value' => '_coupon_value',
        'provision_value' => '_provision_value',
		'preview_key' => '_preview_private_key', 
		'tax' => '_tax', 
		'taxable' => '_taxable', 
		'taxmode' => '_taxmode', 
		'shipping' => '_shipping', 
		'shippable' => '_shippable', 
		'shipping_dyn_price' => '_shipping_dyn_price', 
		'shipping_mode' => '_shipping_mode', 
		'rss_excerpt' => '_rss_excerpt', 
		'used_voucher_serials' => '_used_voucher_serials', 
		'value' => '_value', 
		'voucher_expiration_date' => '_voucher_expiration_date', 
		'voucher_how_to_use' => '_voucher_how_to_use', 
		'voucher_id_prefix' => '_voucher_id_prefix',
		'voucher_locations' => '_voucher_locations', 
		'voucher_logo' => '_voucher_logo', 
		'voucher_map' => '_voucher_map', 
		'voucher_serial_numbers' => '_voucher_serial_numbers', 
		'custom_options_payment_type' => '_custom_options_payment_type',
		'custom_options_terms' => '_custom_options_terms',
		'custom_options_redeem' => '_custom_options_redeem',
		'custom_options_other_terms' => '_custom_options_other_terms',
		'custom_options_need_help' => '_custom_options_need_help',
        'custom_options_voucher_extra_text' => '_custom_options_voucher_extra_text',
        'description' => 'description',
	);


	public static function init() {
		// register Deal post type
		$post_type_args = array(
			'has_archive' => TRUE,
			'menu_position' => 4,
			'rewrite' => array(
				'slug' => self::REWRITE_SLUG,
				'with_front' => TRUE,
				'hierarchical' => TRUE
			),
			'supports' => array( 'title', 'editor', 'thumbnail', 'comments', 'custom-fields', 'revisions' ),
			'menu_icon' => WG_RESOURCES . 'images/deals.png',
			// 'hierarchical' => TRUE
		);
		self::register_post_type( self::POST_TYPE, self::__( 'Deal' ), self::__( 'Deals' ), $post_type_args );
		

		// register Locations taxonomy
		$singular = __('Location');
		$plural = __('Locations');
		$taxonomy_args = array(
			'rewrite' => array(
				'slug' => 'locations',
				'with_front' => TRUE,
				'hierarchical' => TRUE,
			),
		);
		self::register_taxonomy( self::LOCATION_TAXONOMY, array( self::POST_TYPE ), $singular, $plural, $taxonomy_args );

		// register Locations taxonomy
		$singular = __('Category');
		$plural = __('Categories');
		$taxonomy_args = array(
			'rewrite' => array(
				'slug' => 'deal-categories',
				'with_front' => TRUE,
				'hierarchical' => TRUE,
			),
		);
		self::register_taxonomy( self::CAT_TAXONOMY, array( self::POST_TYPE ), $singular, $plural, $taxonomy_args );

		// register Locations taxonomy
		$singular = __('Tag');
		$plural = __('Tags');
		$taxonomy_args = array(
			'hierarchical' => FALSE,
			'rewrite' => array(
				'slug' => 'deal-tags',
				'with_front' => TRUE,
				'hierarchical' => FALSE,
			),
		);
		self::register_taxonomy( self::TAG_TAXONOMY, array( self::POST_TYPE ), $singular, $plural, $taxonomy_args );
	}

	protected function __construct( $id ) {
		parent::__construct( $id );
	}

	public static function get_instance( $id = 0 ) {
		if ( !$id ) {
			return NULL;
		}
		if ( !isset( self::$instances[$id] ) || !self::$instances[$id] instanceof self ) {
			self::$instances[$id] = new self( $id );
		}
		if ( self::$instances[$id]->post->post_type != self::POST_TYPE ) {
			return NULL;
		}
		return self::$instances[$id];
	}

	public static function is_deal_query() {
		$post_type = get_query_var( 'post_type' );
		if ( $post_type == self::POST_TYPE ) {
			return TRUE;
		}
		return FALSE;
	}

	public static function is_deal_tax_query() {
		$taxonomy = get_query_var( 'taxonomy' );
		if ( $taxonomy == self::LOCATION_TAXONOMY || $taxonomy == self::CAT_TAXONOMY || $taxonomy == self::TAG_TAXONOMY ) {
			return TRUE;
		}
		return FALSE;
	}

	// Get the post object associated with this deal
	public function get_deal() {
		return $this->get_post();
	}

	public function get_redirect_url() {
		$redirect_url = $this->get_post_meta( self::$meta_keys['redirect_url'] );
		if ( empty( $redirect_url ) ) return;
		return $redirect_url;
	}

	public function set_redirect_url( $redirect_url ) {
		$this->save_post_meta( array(
				self::$meta_keys['redirect_url'] => $redirect_url,
			) );
	}

	// Get the current price of the deal, which may be influenced by the number of successful purchases to date
	public function get_price( $qty = NULL, $data = array() ) {
		if ( is_null( $qty ) ) {
			$qty = $this->get_number_of_purchases();
		}

		$price = apply_filters( 'wg_get_deal_price_meta', $this->get_post_meta( self::$meta_keys['price'] ), $this, $qty, $data );
		if ( 0 == $qty ) {
			$price = apply_filters( 'wg_deal_price', $price, $this, $qty, $data );
			return $price;
		}

		$dynamic_price = $this->get_dynamic_price();
		$max_qty_found = 0;
		if ( !empty( $dynamic_price ) ) {
			foreach ( $dynamic_price as $qty_required => $new_price ) {
				if ( $qty >= $qty_required && $qty_required > $max_qty_found ) {
					$price = $new_price;
					$max_qty_found = $qty_required;
				}
			}
		}

		return apply_filters( 'wg_deal_get_price', $price, $this, $qty, $data );
	}

	public function get_cart_price($qty = NULL, $data = array() ) {
        if ( is_null( $qty ) ) {
            $qty = $this->get_number_of_purchases();
        }

        $price = apply_filters( 'wg_get_deal_price_meta', $this->get_coupon_price($qty, $data), $this, $qty, $data );
        if ( 0 == $qty ) {
            $price = apply_filters( 'wg_deal_price', $price, $this, $qty, $data );
            return $price;
        }

        $dynamic_price = $this->get_dynamic_price();
        $max_qty_found = 0;
        if ( !empty( $dynamic_price ) ) {
            foreach ( $dynamic_price as $qty_required => $new_price ) {
                if ( $qty >= $qty_required && $qty_required > $max_qty_found ) {
                    $price = $new_price;
                    $max_qty_found = $qty_required;
                }
            }
        }
        return apply_filters( 'wg_deal_get_price', $price, $this, $qty, $data );

    }

    public function get_coupon_price($qty = NULL, $data = array()) {

        if ( is_null( $qty ) ) {
            $qty = $this->get_number_of_purchases();
        }
        $price = apply_filters( 'wg_get_deal_price_meta', $this->get_coupon_value(), $this, $qty, $data );
        if(count($price)>0) {
            $price = $price[0];
        }

        if ( 0 == $qty ) {
            $price = apply_filters( 'wg_deal_price', $price, $this, $qty, $data );
            return $price;
        }

        $dynamic_price = $this->get_dynamic_price();
        $max_qty_found = 0;
        if ( !empty( $dynamic_price ) ) {
            foreach ( $dynamic_price as $qty_required => $new_price ) {
                if ( $qty >= $qty_required && $qty_required > $max_qty_found ) {
                    $price = $new_price;
                    $max_qty_found = $qty_required;
                }
            }
        }
        return apply_filters( 'wg_deal_get_price', $price, $this, $qty, $data );

    }

	//  coupon_value
    public function get_coupon_value() {
        $coupon_value = $this->get_post_meta( self::$meta_keys['coupon_value'] );
        if ( empty( $coupon_value ) ) return;
        return (array)$coupon_value;
    }

    public function set_coupon_value($coupon_value) {
        $this->save_post_meta( array(
            self::$meta_keys['coupon_value'] => $coupon_value,
        ) );
    }

    //  provision_value
    public function get_provision_value() {
        $provision_value = $this->get_post_meta( self::$meta_keys['provision_value'] );
        if ( empty( $provision_value ) ) return 0;
        return $provision_value;
    }

    public function set_provision_value($provision_value) {
        $this->save_post_meta( array(
            self::$meta_keys['provision_value'] => $provision_value,
        ) );
    }

    public function get_dynamic_price() {
		$dynamic_price = $this->get_post_meta( self::$meta_keys['dynamic_price'] );
		if ( empty( $dynamic_price ) ) return;
		return (array)$dynamic_price;
	}

	public function has_dynamic_price() {
		$dynamic_price = $this->get_dynamic_price();
		return count( $dynamic_price ) > 0;
	}

	public function get_payment_price( $qty = NULL, $data = array() ) {
		return apply_filters( 'wg_get_payment_price', $this->get_coupon_price( $qty, $data ), $this, $qty, $data );
	}

	public function set_prices( $prices ) {
		$base = 0;
		$coupon = 0;
		$dynamic = array();
		foreach ( $prices as $qty => $price ) {
			if ( $qty == 0 ) {
				$base = $price;
			} else {
				if($qty == 1) {
					$coupon = $price;
				}
				else {
				$dynamic[$qty] = $price;
				}
			}
		}
		$this->save_post_meta( array(
				self::$meta_keys['price'] => $base,
				self::$meta_keys['coupon_value'] => $coupon,
				self::$meta_keys['dynamic_price'] => $dynamic,
			) );
	}

	// Get a list of every successful Purchase of this deal
	public function get_purchases() {
		$purchase_ids = WP_Groupbuy_Purchase::get_purchases( array( 'deal' => $this->ID ) );

		$purchases = array();
		foreach ( $purchase_ids as $purchase_id ) {
			$purchases[] = WP_Groupbuy_Purchase::get_instance( $purchase_id );
		}

		return $purchases;
	}

	// Get a list of successful Purchases by a given account
	public function get_purchases_by_account( $account_id ) {
		$purchase_ids = WP_Groupbuy_Purchase::get_purchases( array( 'deal' => $this->ID, 'account' => $account_id ) );
		return $purchase_ids;
	}

	public function get_number_of_purchases( $recalculate = false, $count_pending = true ) {
		$number_of_purchases = $this->get_post_meta( self::$meta_keys['number_of_purchases'] );
		if ( $recalculate || empty( $number_of_purchases ) ) {
			$purchases = $this->get_purchases();
			$number_of_purchases = 0;
			foreach ( $purchases as $purchase ) {
				if ( $purchase->is_complete() || ( $count_pending && $purchase->is_pending() ) ) {
					$purchase_quantity = $purchase->get_product_quantity( $this->ID );
					$number_of_purchases += $purchase_quantity;
				}
			}

			$number_of_purchases = apply_filters( 'wg_deal_number_of_purchases', $number_of_purchases, $this );
			$this->save_post_meta( array(
					self::$meta_keys['number_of_purchases'] => $number_of_purchases
				) );
		}
		return (int)$number_of_purchases;
	}

	// Get the unix timestamp indicating when the deal expires
	public function get_expiration_date() {
		$date = (int)$this->get_post_meta( self::$meta_keys['expiration_date'] );
		if ( $date == 0 ) {
			$date = current_time( 'timestamp' ) + 24*60*60;
		}
		return $date;
	}

	public function is_expired() {
		if ( $this->never_expires() ) {
			return FALSE;
		}
		if ( current_time( 'timestamp' ) > $this->get_expiration_date() ) {
			return TRUE;
		}
		return FALSE;
	}

	public function never_expires() {
		return $this->get_expiration_date() == self::NO_EXPIRATION_DATE;
	}

	// Get the list of deals that expired since $timestamp
	public static function get_expired_deals( $timestamp = 0 ) {
		global $wpdb;
		$sql = "SELECT post_id FROM {$wpdb->postmeta} WHERE meta_key=%s AND meta_value>%d AND meta_value<%d";
		$query = $wpdb->prepare( $sql, self::$meta_keys['expiration_date'], $timestamp, current_time( 'timestamp' ) );
		return (array)$wpdb->get_col( $query );
	}

	// Determine the status of the deal
	public function get_status() {
		if ( $this->is_closed() ) {
			return self::DEAL_STATUS_CLOSED;
		} elseif ( $this->is_pending() ) {
			return self::DEAL_STATUS_PENDING;
		} elseif ( $this->is_open() ) {
			return self::DEAL_STATUS_OPEN;
		} else {
			return self::DEAL_STATUS_UNKNOWN;
		}
	}

	public function is_open() {
		if ( ( get_post_status($this->ID) == 'publish' || get_post_status($this->ID) == 'private' ) && !$this->is_closed() ) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function is_pending() {
		if ( get_post_status($this->ID) == 'pending' ) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function is_closed() {
		if ( $this->is_expired() ) {
			return TRUE;
		} elseif ( $this->is_sold_out() ) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function set_shipping( $shipping ) {
		$this->save_post_meta( array(
				self::$meta_keys['shipping'] => $shipping,
			) );
		return $shipping;
	}

	public function get_shipping_meta() {
		$shipping = $this->get_post_meta( self::$meta_keys['shipping'] );
		return $shipping;
	}

	public function get_shipping( $qty = NULL, $local = NULL ) {
		$shipping = $this->get_post_meta( self::$meta_keys['shipping'] );
		// shipping class
		return apply_filters( 'wg_deal_get_shipping', $shipping, $this, $qty, $local );
	}

	public function set_shippable( $shippable ) {
		$this->save_post_meta( array(
				self::$meta_keys['shippable'] => $shippable,
			) );
		return $shippable;
	}

	public function get_shippable() {
		$shippable = $this->get_post_meta( self::$meta_keys['shippable'] );
		return $shippable;
	}

	public function set_shipping_dyn_price( $shipping_dyn_price ) {
		$this->save_post_meta( array(
				self::$meta_keys['shipping_dyn_price'] => $shipping_dyn_price,
			) );
		return $shipping_dyn_price;
	}

	public function get_shipping_dyn_price() {
		$shipping_dyn_price = $this->get_post_meta( self::$meta_keys['shipping_dyn_price'] );
		return $shipping_dyn_price;
	}

	public function set_shipping_mode( $shipping_mode ) {
		$this->save_post_meta( array(
				self::$meta_keys['shipping_mode'] => $shipping_mode,
			) );
		return $shipping_mode;
	}

	public function get_shipping_mode( $local = NULL ) {
		$shipping_mode = $this->get_post_meta( self::$meta_keys['shipping_mode'] );
		return $shipping_mode;
	}

	public function set_tax( $tax ) {
		if ( is_numeric( $tax ) && $tax >= 1 ) {
			$tax = $tax/100;
		}
		$this->save_post_meta( array(
				self::$meta_keys['tax'] => $tax,
			) );
		return $tax;
	}

	public function get_tax( $local = NULL ) {
		$mode = $this->get_tax_mode();
		if ( is_int( $mode ) ) {
			return $mode;
		}
		return WP_Groupbuy_Core_Tax::get_rate( $mode, $local );
	}

	public function get_tax_mode( $local = NULL ) {
		$tax = $this->get_post_meta( self::$meta_keys['tax'] );
		if ( is_numeric( $tax ) ) {
			return (int)$tax;
		}
		return $tax;
	}

	public function set_taxable( $bool ) {
		$this->save_post_meta( array(
				self::$meta_keys['taxable'] => $bool,
			) );
		return $bool;
	}

	public function get_taxable( $local = NULL ) {
		$tax = $this->get_post_meta( self::$meta_keys['taxable'] );
		return $tax;
	}


	public function get_calc_tax( $qty = NULL, $item_data = NULL, $local = NULL ) {
		$tax = WP_Groupbuy_Core_Tax::get_calc_tax( $this, $qty, $item_data, $local );
		return $tax;
	}

	// Set the unix timestamp indicating when the deal expires
	public function set_expiration_date( $timestamp ) {
		$this->save_post_meta( array(
				self::$meta_keys['expiration_date'] => $timestamp,
			) );
		return $timestamp;
	}

	// The maximum number of purchases allowed for this deal
	public function get_max_purchases() {
		return $this->get_post_meta( self::$meta_keys['max_purchases'] );
	}

	// Set a new value for the maximum number of purchases
	public function set_max_purchases( $qty ) {
		$this->save_post_meta( array(
				self::$meta_keys['max_purchases'] => $qty,
			) );
		return $qty;
	}

	// The number of allowed purchase remaining
	public function get_remaining_allowed_purchases() {
		$max = $this->get_max_purchases();
		if ( $max == self::NO_MAXIMUM || empty( $max ) ) {
			return self::NO_MAXIMUM;
		}
		return $max - $this->get_number_of_purchases();
	}

	// sold out
	public function is_sold_out() {
		$max = $this->get_max_purchases();
		if ( $max == self::NO_MAXIMUM || empty( $max ) ) {
			return FALSE;
		}
		return $this->get_remaining_allowed_purchases() < 1;
	}

	//The minimum number of purchases for this to be a successful deal
	public function get_min_purchases() {
	    return 0;
		//	return $this->get_post_meta( self::$meta_keys['min_purchases'] );
	}

	// Set a new value for the minimum number of purchases
	public function set_min_purchases( $qty ) {
		$this->save_post_meta( array(
				self::$meta_keys['min_purchases'] => $qty,
			) );
		return $qty;
	}

	// The number of purchases still needed for a successful deal
	public function get_remaining_required_purchases() {
		$remaining = max( 0, $this->get_min_purchases() - $this->get_number_of_purchases() );
		return apply_filters( 'get_remaining_required_purchases', $remaining );
	}

	public function is_successful() {
//		if ( !$this->capture_before_expiration() && !$this->is_expired() ) {
        if ( $this->is_expired() ) {
			return apply_filters( 'wg_deal_is_successful', FALSE, $this );
		}
		$remaining = $this->get_remaining_required_purchases();
		if ( $remaining < 1 ) {
			return apply_filters( 'wg_deal_is_successful', TRUE, $this );
		}
		return apply_filters( 'wg_deal_is_successful', FALSE, $this );
	}

	// The maximum quantity of this deal that a single user can Purchase
	public function get_max_purchases_per_user() {
		return $this->get_post_meta( self::$meta_keys['max_purchases_per_user'] );
	}
	
	// Set a new value for the maximum number of purchases per user
	public function set_max_purchases_per_user( $qty ) {
		$this->save_post_meta( array(
				self::$meta_keys['max_purchases_per_user'] => $qty
			) );
		return $qty;
	}

	// The ID of the merchant associated with this deal
	public function get_merchant_id() {
		return $this->get_post_meta( self::$meta_keys['merchant_id'] );
	}

	// Set a new value for the merchant ID
	public function set_merchant_id( $id ) {
		$this->save_post_meta( array(
				self::$meta_keys['merchant_id'] => $id
			) );
		return $id;
	}

	// The merchant associated with this deal
	public function get_merchant() {
		$id = $this->get_merchant_id();
		return WP_Groupbuy_Merchant::get_instance( $id );
	}

	public function get_title( $data = array() ) {
		return apply_filters( 'wg_deal_title', get_the_title( $this->ID ), $data );
	}

	public function get_slug() {
		$post = get_post( $this->ID );
		return $post->post_name;
	}

	public function get_value() {
		$value = $this->get_post_meta( self::$meta_keys['value'] );
		return $value;
	}

	public function set_value( $value ) {
		$this->save_post_meta( array(
				self::$meta_keys['value'] => $value
			) );
		return $value;
	}

	public function get_amount_saved() {
        $deal_coupon_value = $this->get_coupon_value()[0];
        $price = $this->get_price();
        $amount_saved = 0;
        if($price > 0) {
            $amount_saved = number_format(((1 - ($deal_coupon_value) / $price)) * 100, 0);
        }
		return $amount_saved;
	}

	public function set_amount_saved( $amount_saved ) {
		$this->save_post_meta( array(
				self::$meta_keys['amount_saved'] => $amount_saved
			) );
		return $amount_saved;
	}

    public function get_show_amount_saved() {
        $show_amount_saved = $this->get_post_meta( self::$meta_keys['show_amount_saved']);
        //  if record doesn't exist, returm TRUE by default else return stored value => 1/0
        if($show_amount_saved == NULL) {
            return 1;
        }
        return $show_amount_saved;
    }

    public function set_show_amount_saved( $show_amount_saved ) {
        $this->save_post_meta( array(
            self::$meta_keys['show_amount_saved'] => $show_amount_saved
        ) );
        return $show_amount_saved;
    }

	public function get_highlights() {
		$highlights = $this->get_post_meta( self::$meta_keys['highlights'] );
		return $highlights;
	}

	public function set_highlights( $highlights ) {
		$this->save_post_meta( array(
				self::$meta_keys['highlights'] => $highlights
			) );
		return $highlights;
	}

	public function get_fine_print() {
		$fine_print = $this->get_post_meta( self::$meta_keys['fine_print'] );
		return $fine_print;
	}

	public function set_fine_print( $fine_print ) {
		$this->save_post_meta( array(
				self::$meta_keys['fine_print'] => $fine_print
			) );
		return $fine_print;
	}

	public function get_rss_excerpt() {
		$rss_excerpt = $this->get_post_meta( self::$meta_keys['rss_excerpt'] );
		return $rss_excerpt;
	}

	public function set_rss_excerpt( $rss_excerpt ) {
		$this->save_post_meta( array(
				self::$meta_keys['rss_excerpt'] => $rss_excerpt
			) );
		return $rss_excerpt;
	}

	public function get_preview_key() {
		$preview_key = $this->get_post_meta( self::$meta_keys['preview_key'] );
		return $preview_key;
	}

	public function set_preview_key( $preview_key ) {
		$this->save_post_meta( array(
				self::$meta_keys['preview_key'] => $preview_key
			) );
		return $preview_key;
	}

	// Expiration date for this deal's voucher
	public function get_voucher_expiration_date() {
		$voucher_expiration_date = $this->get_post_meta( self::$meta_keys['voucher_expiration_date'] );
		return $voucher_expiration_date;
	}

	// The expiration date for this deal's voucher
	public function set_voucher_expiration_date( $date ) {
		$this->save_post_meta( array(
				self::$meta_keys['voucher_expiration_date'] => strtotime( $date )
			) );
		return $date;
	}

	// Instructions on how to use this deal's voucher
	public function get_voucher_how_to_use() {
		$voucher_how_to_use = $this->get_post_meta( self::$meta_keys['voucher_how_to_use'] );
		return $voucher_how_to_use;
	}

	// The instructions for how to use this deal's voucher
	public function set_voucher_how_to_use( $instructions ) {
		$this->save_post_meta( array(
				self::$meta_keys['voucher_how_to_use'] => $instructions
			) );
		return $instructions;
	}

	// Prefix for this deal's voucher ID
	public function get_voucher_id_prefix( $fallback = FALSE ) {
		$voucher_id_prefix = $this->get_post_meta( self::$meta_keys['voucher_id_prefix'] );
		if ( $fallback && !$voucher_id_prefix ) {
			$voucher_id_prefix = WP_Groupbuy_Vouchers::get_voucher_prefix();
		}
		return $voucher_id_prefix;
	}

	// The string with which to prefix this deal's voucher IDs
	public function set_voucher_id_prefix( $prefix ) {
		$this->save_post_meta( array(
				self::$meta_keys['voucher_id_prefix'] => $prefix
			) );
		return $prefix;
	}

	// Locations where this deal's voucher may be used
	public function get_voucher_locations() {
		$voucher_locations = $this->get_post_meta( self::$meta_keys['voucher_locations'] );
		if ( is_null( $voucher_locations ) ) {

			$voucher_locations = array();
			while ( count( $voucher_locations ) < self::MAX_LOCATIONS ) {
				$voucher_locations[] = '';
			}
		}
		return (array)$voucher_locations;
	}

	// Locations where this deal's voucher may be used
	public function set_voucher_locations( $locations ) {
		$this->save_post_meta( array(
				self::$meta_keys['voucher_locations'] => $locations
			) );
		return $locations;
	}

	// Location of the logo for this deal's voucher
	public function get_voucher_logo() {
		$voucher_logo = $this->get_post_meta( self::$meta_keys['voucher_logo'] );
		return $voucher_logo;
	}

	// Location of the logo for this deal's voucher
	public function set_voucher_logo( $path ) {
		$this->save_post_meta( array(
				self::$meta_keys['voucher_logo'] => $path
			) );
		return $path;
	}

	// Google maps iframe code for this deal's voucher
	public function get_voucher_map() {
		$voucher_map = $this->get_post_meta( self::$meta_keys['voucher_map'] );
		return $voucher_map;
	}

	public function set_voucher_map( $map ) {
		$this->save_post_meta( array(
				self::$meta_keys['voucher_map'] => $map
			) );
		return $map;
	}

	// Comma separated list serial numbers for this deal's voucher
	public function get_voucher_serial_numbers() {
		$voucher_serial_numbers = (array)$this->get_post_meta( self::$meta_keys['voucher_serial_numbers'] );
		return $voucher_serial_numbers;
	}

	// List serial numbers for this deal's voucher
	public function set_voucher_serial_numbers( array $numbers ) {
		$this->save_post_meta( array(
				self::$meta_keys['voucher_serial_numbers'] => $numbers
			) );
		return $numbers;
	}

	public function get_next_serial() {
		$serials = $this->get_voucher_serial_numbers();
		if ( !$serials ) {
			return FALSE;
		}
		$used = $this->get_post_meta( self::$meta_keys['used_voucher_serials'] );
		$remain = array_diff( $serials, $used );
		$serial = count( $remain ) ? current( $remain ) : false;
		return $serial;
	}

	public function mark_serial_used( $new ) {
		$serials = $this->get_post_meta( self::$meta_keys['used_voucher_serials'] );
		if ( !is_array( $serials ) ) {
			$serials = array();
		}
		$serials[] = $new;
		$this->save_post_meta( array(
				self::$meta_keys['used_voucher_serials'] => $serials
			) );
	}

	// Add a file as a post attachment.
	public function set_attachement( $files, $key = '' ) {
		if ( !function_exists( 'wp_generate_attachment_metadata' ) ) {
			require_once ABSPATH . 'wp-admin' . '/includes/image.php';
			require_once ABSPATH . 'wp-admin' . '/includes/file.php';
			require_once ABSPATH . 'wp-admin' . '/includes/media.php';
		}

		foreach ( $files as $file => $array ) {
			if ( $files[$file]['error'] !== UPLOAD_ERR_OK ) {

			}
			if ( $key !== '' ) {
				if ( $key == $file  ) {
					$attach_id = media_handle_upload( $file, $this->ID );
				}
			}
			else {
				$attach_id = media_handle_upload( $file, $this->ID );
			}

		}
 		// Make it a thumbnail
		if ( !is_wp_error($attach_id) && $attach_id > 0 ) {
			update_post_meta( $this->ID, '_thumbnail_id', $attach_id );
		}
		return $attach_id;
	}

	// The merchant to look for
	public static function get_deals_by_merchant( $merchant_id ) {
		$deal_ids = self::find_by_meta( self::POST_TYPE, array( self::$meta_keys['merchant_id'] => $merchant_id ) );
		return $deal_ids;
	}

	// payments will be captured before deal expiration
	public function capture_before_expiration() {
		return (bool)$this->get_post_meta( self::$meta_keys['capture_before_expiration'] );
	}

	public function set_capture_before_expiration( $status ) {
		$this->save_post_meta( array(
				self::$meta_keys['capture_before_expiration'] => (bool)$status,
			) );
	}

	//	Custom options getters and setters
	//	get custom payment types for deal
	public function get_custom_options_payment_type() {
		return $this->get_post_meta( self::$meta_keys['custom_options_payment_type'] );
	}
	//	set custom payment type for deal
	public function set_custom_options_payment_type( $custom_options_payment_type) {
		$this->save_post_meta( array(
			self::$meta_keys['custom_options_payment_type'] => $custom_options_payment_type,
		) );
		return $custom_options_payment_type;
	}

	//	get custom options terms for deal
	public function get_custom_options_terms() {
		return $this->get_post_meta( self::$meta_keys['custom_options_terms'] );
	}
	// set custom options terms for deal
	public function set_custom_options_terms( $custom_options_terms ) {
		$this->save_post_meta( array(
			self::$meta_keys['custom_options_terms'] => $custom_options_terms,
		) );
		return $custom_options_terms;
	}

	//	get custom options redeems
	public function get_custom_options_redeem() {
		return $this->get_post_meta( self::$meta_keys['custom_options_redeem'] );
	}
	//	set custom options deal redeem
	public function set_custom_options_redeem($custom_options_redeem) {
		$this->save_post_meta( array(
			self::$meta_keys['custom_options_redeem'] => $custom_options_redeem,
		) );
		return $custom_options_redeem;
	}

	//	get custom options other term
	public function get_custom_options_other_terms() {
		return $this->get_post_meta( self::$meta_keys['custom_options_other_terms'] );
	}
	//	set custom options deal's other terms
	public function set_custom_options_other_terms($custom_options_other_terms) {
		$this->save_post_meta( array(
			self::$meta_keys['custom_options_other_terms'] => $custom_options_other_terms,
		) );
		return $custom_options_other_terms;
	}

	//	get custom options help info
	public function get_custom_options_need_help() {
		return $this->get_post_meta( self::$meta_keys['custom_options_need_help'] );
	}
	//	set custom options deal's help info
	public function set_custom_options_need_help($custom_options_need_help) {
		$this->save_post_meta( array(
			self::$meta_keys['custom_options_need_help'] => $custom_options_need_help,
		) );
		return $custom_options_need_help;
	}

    //	get custom options voucher extra text
    public function get_custom_options_voucher_extra_text() {
        return $this->get_post_meta( self::$meta_keys['custom_options_voucher_extra_text'] );
    }
    //	set custom options voucher extra text
    public function set_custom_options_voucher_extra_text($custom_options_voucher_extra_text) {
        $this->save_post_meta( array(
            self::$meta_keys['custom_options_voucher_extra_text'] => $custom_options_voucher_extra_text,
        ) );
        return $custom_options_voucher_extra_text;
    }

	public function get_additional_title_description() {
		return $this->get_post_meta( self::$meta_keys['description'] );
	}
	//	set custom options deal's help info
	public function set_additional_title_description($additional_title_description) {
		$this->save_post_meta( array(
			self::$meta_keys['description'] => $additional_title_description,
		) );
		return $additional_title_description;
	}


}
