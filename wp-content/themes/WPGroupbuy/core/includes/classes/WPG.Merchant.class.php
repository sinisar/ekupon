<?php

add_action('wg_init_classes', array( 'WP_Groupbuy_Merchant', 'init' ), 60 );

class WP_Groupbuy_Merchant extends WP_Groupbuy_Post_Type {
	const POST_TYPE = 'wg_merchant';
	const REWRITE_SLUG = 'business';
	const MERCHANT_TYPE_TAXONOMY = 'wg_merchant_type';
	const MERCHANT_TAG_TAXONOMY = 'wg_merchant_tags';
	const MERCHANT_TYPE_TAX_SLUG = 'business-type';
	const MERCHANT_TAG_TAX_SLUG = 'business-tags';

	private static $instances = array();

	private static $meta_keys = array(
		'authorized_users' => '_authorized_users', 
		'contact_title' => '_contact_title', 
		'contact_name' => '_contact_name', 
		'contact_street' => '_contact_street', 
		'contact_city' => '_contact_city', 
		'contact_state' => '_contact_state', 
		'contact_postal_code' => '_contact_postal_code', 
		'contact_country' => '_contact_country', 
		'contact_phone' => '_contact_phone', 
		'website' => '_website', 
		'facebook' => '_facebook', 
		'twitter' => '_twitter',
        'opening_hours' => '_opening_hours',
		'map_width' => '_map_width',
		'map_height' => '_map_weight',
		'map_zoom' => '_map_zoom',
		'map_type' => '_map_type',
		'map_info_window' => '_map_info_window',
	);


	public static function init() {
		$post_type_args = array(
			'menu_position' => 4,
			'has_archive' => TRUE,
			'rewrite' => array(
				'slug' => self::REWRITE_SLUG,
				'with_front' => FALSE,
			),
			'supports' => array( 'title', 'editor', 'thumbnail', 'comments', 'custom-fields', 'revisions' ),
			'menu_icon' => WG_RESOURCES . 'images/merchant.png'
		);
		self::register_post_type( self::POST_TYPE, self::__('Merchant'), self::__('Merchants'), $post_type_args );

		// register Business Type taxonomy
		$singular = self::__('Merchant Type' );
		$plural = self::__('Merchant Types' );
		$taxonomy_args = array(
			'rewrite' => array(
				'slug' => self::MERCHANT_TYPE_TAX_SLUG,
				'with_front' => FALSE,
				'hierarchical' => TRUE,
			),
		);
		self::register_taxonomy( self::MERCHANT_TYPE_TAXONOMY, array( self::POST_TYPE ), $singular, $plural, $taxonomy_args );

		// register Business Tag taxonomy
		$singular = self::__('Merchant Tag' );
		$plural = self::__('Merchant Tags' );
		$taxonomy_args = array(
			'hierarchical' => FALSE,
			'rewrite' => array(
				'slug' => self::MERCHANT_TAG_TAX_SLUG,
				'with_front' => FALSE,
				'hierarchical' => FALSE,
			),
		);
		//self::register_taxonomy( self::MERCHANT_TAG_TAXONOMY, array( self::POST_TYPE ), $singular, $plural, $taxonomy_args );
	}


	protected function __construct( $id ) {
		parent::__construct( $id );
	}

	public static function get_instance( $id = 0 ) {
		if ( !$id ) {
			return NULL;
		}
		if ( !isset( self::$instances[$id] ) || !self::$instances[$id] instanceof self ) {
			self::$instances[$id] = new self( $id );
		}
		if ( self::$instances[$id]->post->post_type != self::POST_TYPE ) {
			return NULL;
		}
		return self::$instances[$id];
	}

	public static function get_merchant_object( $id = 0 ) {
		if ( !$id ) {
			return NULL;
		}
		$type = get_post_type( $id );

		if ( $type == self::POST_TYPE ) {
			$merchant = self::get_instance( $id );
		} elseif ( $type = WP_Groupbuy_Deal::POST_TYPE ) {
			$deal = WP_Groupbuy_Deal::get_instance( $id );
			$merchant = $deal->get_merchant();
		} else { return NULL; }

		return $merchant;
	}

	public static function get_merchant_id_for_user( $user_id = 0 ) {
		if ( !$user_id ) {
			$user_id = (int)get_current_user_id();
		}
		$merchant_ids = self::find_by_meta( self::POST_TYPE, array( self::$meta_keys['authorized_users'] => $user_id ) );
		if ( empty( $merchant_ids ) ) {
			$account_id = self::blank_merchant();
		} else {
			$account_id = $merchant_ids[0];
		}
		return $account_id;
	}

	public static function blank_merchant() {
		global $wpdb;
		$blank_merchant_id = $wpdb->get_var( $wpdb->prepare( "SELECT ID from {$wpdb->posts} WHERE post_type = %s and post_title = %s", self::POST_TYPE, self::__( 'Blank Merchant' ) ) );
		if ( $blank_merchant_id ) {
			return $blank_merchant_id;
		}
		//	added - by default, always return user id=2 -> it must exist with role merchant !
		else {
		    return 2;
        }

		$post = array(
			'post_title' => self::__( 'Blank Merchant' ),
			'post_name' => self::__( 'blank-merchant' ),
			'post_status' => 'publish',
			'post_type' => self::POST_TYPE
		);
		$id = wp_insert_post( $post );
		return $id;
	}

	// the current query is for the merchant post type
	public static function is_merchant_query() {
		$post_type = get_query_var( 'post_type' );
		if ( $post_type == self::POST_TYPE ) {
			return TRUE;
		}
		return FALSE;
	}

	// the current query is for the merchant post type
	public static function is_merchant_tax_query() {
		$taxonomy = get_query_var( 'taxonomy' );
		if ( $taxonomy == self::MERCHANT_TYPE_TAXONOMY || $taxonomy == self::MERCHANT_TAG_TAXONOMY ) {
			return TRUE;
		}
		return FALSE;
	}

	public static function get_merchant_id($merchant) {
	    //  print "inside get_merchant_id.";
        //  print_r($merchant);
        //  print "post_post_author: ".$merchant->post->post_author;
        return $merchant->post->post_author;
    }

	// Can the given user edit the Merchant's contact info, or draft deals for this merchant
	public function is_user_authorized( $user_id ) {
		$authorized_users = $this->get_authorized_users();
		if ( empty( $authorized_users ) ) return;
		return in_array( $user_id, $authorized_users );
	}

	// Get a list of all users who are authorized to edit this Merchant
	public function get_authorized_users() {
		$authorized_users = $this->get_post_meta( self::$meta_keys['authorized_users'], FALSE );
		if ( empty( $authorized_users ) ) {
			$authorized_users = array();
		}
		return $authorized_users;
	}

	// Add a user to the list of authorized users
	public function authorize_user( $user_id ) {
		$user = get_userdata( $user_id );
        if ( !is_a( $user, 'WP_User' ) ) {
            $account = WP_Groupbuy_Account::get_instance_by_id( $user_id );
            if ( $account ) {
                $user_id = $account->get_user_id_for_account( $account->get_ID() );
            }
        }
		if ( $user_id && !$this->is_user_authorized( $user_id ) ) {
			update_post_meta( $this->ID, self::$meta_keys['authorized_users'], $user_id );
            //print "auth_user_id: ".$user_id;
		}
	}

	// Remove a user from the list of authorized users
	public function unauthorize_user( $user_id ) {
		if ( $this->is_user_authorized( $user_id ) ) {
			$this->delete_post_meta( array(
					self::$meta_keys['authorized_users'] => $user_id
				) );
		}
	}

	public function get_deal_ids( ) {
		$deal_ids = WP_Groupbuy_Deal::get_deals_by_merchant( $this->ID );
		return $deal_ids;
	}

	public function get_active_deal_ids() {
        $active_deal_ids = array();
        $deal_ids = WP_Groupbuy_Deal::get_deals_by_merchant($this->ID);

        foreach ($deal_ids as $deal_id) {
            $deal = WP_Groupbuy_Deal::get_instance( $deal_id );
            if(!$deal->is_expired()) {
                $active_deal_ids[] = $deal_id;
            }
        }
        return $active_deal_ids;
    }

	// Get a list of deals associated with this merchant
	public function get_deals( $status = NULL ) {
		$deal_ids = self::get_deal_ids();
		$deals = array();
		foreach ( $deal_ids as $deal_id ) {
			$deal = WP_Groupbuy_Deal::get_instance( $deal_id );
			if ( is_null( $status ) || $deal->get_status() == $status ) {
				$deals[] = $deal;
			}
		}
		return $deals;
	}

	public static function get_url() {
		if ( wg_using_permalinks() ) {
			$url = trailingslashit( home_url() ).trailingslashit( self::REWRITE_SLUG );
		} else {
			$url = add_query_arg( self::REWRITE_SLUG, 1, home_url() );
		}
		return $url;
	}

	public function get_user_id() {
        $contact_name = $this->get_post_meta( self::$meta_keys['authorized_users'] );
        return $contact_name;
    }

/*
    public function set_user_id( $user_id ) {
        $this->save_post_meta( array(
            self::$meta_keys['authorized_users'] => $user_id
        ) );
        return $user_id;
    }
*/

    public function get_contact_name() {
		$contact_name = $this->get_post_meta( self::$meta_keys['contact_name'] );
		return $contact_name;
	}

	public function set_contact_name( $contact_name ) {
		$this->save_post_meta( array(
				self::$meta_keys['contact_name'] => $contact_name
			) );
		return $contact_name;
	}

	public function get_contact_title() {
		$contact_title = $this->get_post_meta( self::$meta_keys['contact_title'] );
		return $contact_title;
	}

	public function set_contact_title( $contact_title ) {
		$this->save_post_meta( array(
				self::$meta_keys['contact_title'] => $contact_title
			) );
		return $contact_title;
	}

	public function get_contact_street() {
		$contact_street = $this->get_post_meta( self::$meta_keys['contact_street'] );
		return $contact_street;
	}

	public function set_contact_street( $contact_street ) {
		$this->save_post_meta( array(
				self::$meta_keys['contact_street'] => $contact_street
			) );
		return $contact_street;
	}

	public function get_contact_city() {
		$contact_city = $this->get_post_meta( self::$meta_keys['contact_city'] );
		return $contact_city;
	}

	public function set_contact_city( $contact_city ) {
		$this->save_post_meta( array(
				self::$meta_keys['contact_city'] => $contact_city
			) );
		return $contact_city;
	}

	public function get_contact_state() {
		$contact_state = $this->get_post_meta( self::$meta_keys['contact_state'] );
		return $contact_state;
	}

	public function set_contact_state( $contact_state ) {
		$this->save_post_meta( array(
				self::$meta_keys['contact_state'] => $contact_state
			) );
		return $contact_state;
	}

	public function get_contact_postal_code() {
		$contact_postal_code = $this->get_post_meta( self::$meta_keys['contact_postal_code'] );
		return $contact_postal_code;
	}

	public function set_contact_postal_code( $contact_postal_code ) {
		$this->save_post_meta( array(
				self::$meta_keys['contact_postal_code'] => $contact_postal_code
			) );
		return $contact_postal_code;
	}

	public function get_contact_country() {
		$contact_country = $this->get_post_meta( self::$meta_keys['contact_country'] );
		return $contact_country;
	}

	public function set_contact_country( $contact_country ) {
		$this->save_post_meta( array(
				self::$meta_keys['contact_country'] => $contact_country
			) );
		return $contact_country;
	}

	public function get_contact_phone() {
		$contact_phone = $this->get_post_meta( self::$meta_keys['contact_phone'] );
		return $contact_phone;
	}

	public function set_contact_phone( $contact_phone ) {
		$this->save_post_meta( array(
				self::$meta_keys['contact_phone'] => $contact_phone
			) );
		return $contact_phone;
	}

	public function get_website() {
		$website = $this->get_post_meta( self::$meta_keys['website'] );
		return $website;
	}

	public function set_website( $website ) {
		$this->save_post_meta( array(
				self::$meta_keys['website'] => $website
			) );
		return $website;
	}

	public function get_facebook() {
		$facebook = $this->get_post_meta( self::$meta_keys['facebook'] );
		return $facebook;
	}

	public function set_facebook( $facebook ) {
		$this->save_post_meta( array(
				self::$meta_keys['facebook'] => $facebook
			) );
		return $facebook;
	}

	public function get_twitter() {
		$twitter = $this->get_post_meta( self::$meta_keys['twitter'] );
		return $twitter;
	}

	public function set_twitter( $twitter ) {
		$this->save_post_meta( array(
				self::$meta_keys['twitter'] => $twitter
			) );
		return $twitter;
	}

    public function get_opening_hours() {
        $opening_hours = $this->get_post_meta( self::$meta_keys['opening_hours'] );
        return $opening_hours;
    }

    public function set_opening_hours( $opening_hours ) {
        $this->save_post_meta( array(
            self::$meta_keys['opening_hours'] => $opening_hours
        ) );
        return $opening_hours;
    }

	// Add a file as a post attachment.
	public function set_attachement( $files, $key = '' ) {
		if ( !function_exists( 'wp_generate_attachment_metadata' ) ) {
			require_once ABSPATH . 'wp-admin' . '/includes/image.php';
			require_once ABSPATH . 'wp-admin' . '/includes/file.php';
			require_once ABSPATH . 'wp-admin' . '/includes/media.php';
		}

		foreach ( $files as $file => $array ) {
			if ( $files[$file]['error'] !== UPLOAD_ERR_OK ) {

			}
			if ( $key !== '' ) {
				if ( $key == $file  ) {
					$attach_id = media_handle_upload( $file, $this->ID );
				}
			}
			else {
				$attach_id = media_handle_upload( $file, $this->ID );
			}
		}
		// thumbnail
		if ( !is_wp_error($attach_id) && $attach_id > 0 ) {
			update_post_meta( $this->ID, '_thumbnail_id', $attach_id );
		}
		return $attach_id;
	}


	// List of IDs for deals created by this merchant
	public static function get_merchants_by_account( $user_id ) {
		$merchants = self::find_by_meta( self::POST_TYPE, array( self::$meta_keys['authorized_users'] => $user_id ) );
		return $merchants;
	}

	//	merchant google map width
	public function get_map_width() {
		$map_width = $this->get_post_meta( self::$meta_keys['map_width'] );
		return $map_width;
	}

	public function set_map_width( $map_width ) {
		$this->save_post_meta( array(
			self::$meta_keys['map_width'] => $map_width
		) );
		return $map_width;
	}

	//	merchant google map height
	public function get_map_height() {
		$map_height = $this->get_post_meta( self::$meta_keys['map_height'] );
		return $map_height;
	}

	public function set_map_height( $map_height ) {
		$this->save_post_meta( array(
			self::$meta_keys['map_height'] => $map_height
		) );
		return $map_height;
	}

//	merchant google map zoom
	public function get_map_zoom() {
		$map_zoom = $this->get_post_meta( self::$meta_keys['map_zoom'] );
		return $map_zoom;
	}

	public function set_map_zoom( $map_zoom ) {
		$this->save_post_meta( array(
			self::$meta_keys['map_zoom'] => $map_zoom
		) );
		return $map_zoom;
	}

//	merchant google map type
	public function get_map_type() {
		$map_type = $this->get_post_meta( self::$meta_keys['map_type'] );
		return $map_type;
	}

	public function set_map_type( $map_type ) {
		$this->save_post_meta( array(
			self::$meta_keys['map_type'] => $map_type
		) );
		return $map_type;
	}

//	merchant google map info_window
	public function get_map_info_window() {
		$map_info_window = $this->get_post_meta( self::$meta_keys['map_info_window'] );
		if($map_info_window == '') {
			$map_info_window = "no";
		}
		return $map_info_window;
	}

	public function set_map_info_window( $map_info_window ) {
		$this->save_post_meta( array(
			self::$meta_keys['map_info_window'] => $map_info_window
		) );
		return $map_info_window;
	}

}
