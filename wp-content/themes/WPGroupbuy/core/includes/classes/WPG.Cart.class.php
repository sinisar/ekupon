<?php

add_action( 'wg_init_classes', array( 'WP_Groupbuy_Cart', 'init' ), 20 );

class WP_Groupbuy_Cart  {
	// All the items in the cart
	protected $products = array();
	
	protected function __construct() {
	}

	public function get_id() {
		return session_id();
	}

	public function get_products() {
		return $this->products;
	}

	// Adds an item to the cart
	public function add_item( $product_id, $quantity = 1, $data = NULL ) {
		do_action( 'wg_cart_add_item', $this, $product_id, $quantity, $data );
		$new_quantity = $this->quantity_allowed( $product_id, $quantity, $data );
		if ( $new_quantity ) {
			$this->set_quantity( $product_id, $new_quantity, $data );
			return TRUE;
		}
		return FALSE;
	}

	// Set the item to the quantity given
	public function set_quantity( $product_id, $quantity, $data = NULL ) {
		$found = FALSE;
		do_action( 'wg_cart_set_quantity', $this, $product_id, $quantity, $data );
		foreach ( $this->products as $index => $product ) {
			if ( $product['deal_id'] == $product_id && $data == $product['data'] ) {
				$found = TRUE;
				$allowed = $this->quantity_allowed( $product_id, $quantity, $data );
				$new_quantity = ( $quantity>$allowed ) ? $allowed : $quantity;
				$this->products[$index]['quantity'] = $new_quantity;
				break;
			}
		}
		if ( !$found ) {
			$this->products[] = array(
				'deal_id' => $product_id,
				'quantity' => $quantity,
				'data' => $data,
			);
		}

		do_action( 'wg_cart_set_quantity_save', $this, $product_id, $quantity, $data );
		$this->save();
	}

	// Allowed quantity.
	public function quantity_allowed( $product_id, $quantity = 1, $data = NULL ) {
		$account = WP_Groupbuy_Account::get_instance( get_current_user_id() );
		$allowed = $account->can_purchase( $product_id, $data ); // how many the user is allowed to have
		$total_quantity = $this->get_quantity( $product_id, $data ); // how many are currently in the cart
		if ( $allowed != WP_Groupbuy_Account::NO_MAXIMUM && $allowed < $total_quantity + $quantity ) { // we have too many
			$subtract = $total_quantity+$quantity - $allowed;
			$new_quantity = $quantity + $this->get_quantity( $product_id, $data ) - $subtract; // take out as many as necessary to make this legal
		} else {
			$new_quantity = $quantity + $this->get_quantity( $product_id, $data ); // add in the new quantity
		}
		return apply_filters( 'cart_quantity_allowed', $new_quantity, $product_id, $data );
	}

	// Get the current quantity of the given item in the cart
	public function get_quantity( $product_id, $data, $ignore_data = FALSE ) {
		$qty = 0;
		foreach ( $this->products as $product ) {
			if ( $product['deal_id'] == $product_id && ( $ignore_data || $data == $product['data'] ) ) {
				$qty += $product['quantity'];
			}
		}
		return $qty;
	}

	// Removes all instances of a given item from the cart. If $data is not NULL, only removes items with matching data.
	public function remove_item( $product_id, $data = NULL ) {
		foreach ( $this->products as $index => $product ) {
			if ( $product['deal_id'] == $product_id && ( is_null( $data ) || $data == $product['data'] ) ) {
				unset( $this->products[$index] );
			}
		}
		$this->save();
	}

	// Removes all items from the given cart
	public function empty_cart() {
		$this->products = array();
		$this->save();
	}

	public function get_items() {
		return apply_filters( 'wg_cart_get_items', $this->products, $this );
	}

	public function item_count() {
		$count = ( $this->is_empty() ) ? 0 : count( $this->products ) ;
		return apply_filters( 'wg_cart_item_count', $count, $this );
	}

	// Saves the cart to the database
	private function save() {
		$_SESSION['wg_cart'] = maybe_serialize( $this );
	}

	public function is_empty() {
		if ( count( $this->products ) > 0 ) {
			return FALSE;
		}
		return TRUE;
	}

	public function has_products() {
		return !$this->is_empty();
	}

	public function has_extras() {
		$bool = apply_filters( 'wg_cart_extras', $this );
		return $bool;
	}

	public function is_shippable() {
		return $this->has_extras();
	}

	// Get the subtotal of all the items in the cart
	public function get_subtotal() {
		$subtotal = 0.0;
		foreach ( $this->products as $item ) {
			$deal = WP_Groupbuy_Deal::get_instance( $item['deal_id'] );
			if ( is_object( $deal ) ) {
				$price = $deal->get_payment_price( $item['quantity'], $item['data'] ) * $item['quantity'];
				$subtotal += $price;
			}
		}
		return $subtotal;
	}

	// Get the subtotal of all the items in the cart
	public function get_shipping_total() {
		return WP_Groupbuy_Core_Shipping::cart_shipping_total( $this );
	}

	// Get the subtotal of all the items in the cart
	public function get_tax_total() {
		return WP_Groupbuy_Core_Tax::cart_tax_total( $this );
	}

	// Get the total of all the items in the cart, plus any line items
	public function get_total() {
		$total = $this->get_subtotal();
		return apply_filters( 'wg_cart_get_total', $total, $this );
	}

	public static function get_instance() {
		$cart = maybe_unserialize(@$_SESSION['wg_cart']);
		if ( empty( $cart ) || !is_a( $cart, get_class() ) ) {
			$cart = new static();
			$cart->save();
		}
		return $cart;
	}

	public static function init() {
	}
}
