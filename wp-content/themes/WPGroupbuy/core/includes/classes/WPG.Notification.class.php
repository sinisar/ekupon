<?php

add_action( 'wg_init_classes', array( 'WP_Groupbuy_Notification', 'init' ), 30 );

class WP_Groupbuy_Notification extends WP_Groupbuy_Post_Type {
	const POST_TYPE = 'wg_notification';
	const REWRITE_SLUG = 'notifications';
	private static $instances = array();
	private static $meta_keys = array(
		'disabled' => '_disabled',
	);


	public static function init() {
		// register Notification post type
		$post_type_args = array(
			'public' => FALSE,
			'has_archive' => FALSE,
			'show_ui' => true,
			'show_in_menu' => 'wp-groupbuy/merchants',
			'supports' => array( 'title', 'editor', 'revisions' ),
			'rewrite' => array(
				'slug' => self::REWRITE_SLUG,
				'with_front' => FALSE,
			),
		);
		self::register_post_type( self::POST_TYPE, 'Notification', 'Notifications', $post_type_args );
	}

	protected function __construct( $id ) {
		parent::__construct( $id );
	}

	public static function get_instance( $id = 0 ) {
		if ( !$id ) {
			return NULL;
		}
		if ( !isset( self::$instances[$id] ) || !self::$instances[$id] instanceof self ) {
			self::$instances[$id] = new self( $id );
		}
		if ( self::$instances[$id]->post->post_type != self::POST_TYPE ) {
			return NULL;
		}
		return self::$instances[$id];
	}

	public function is_disabled() {
		$disabled = $this->get_post_meta( self::$meta_keys['disabled'] );
		if ( 'TRUE' == $disabled ) {
			return TRUE;
		}
		return;
	}

	public function get_disabled() {
		$disabled = $this->get_post_meta( self::$meta_keys['disabled'] );
		return $disabled;
	}

	public function set_disabled( $disabled ) {
		$this->save_post_meta( array(
				self::$meta_keys['disabled'] => $disabled
			) );
		return $disabled;
	}
}
