<?php

class WG_Discount extends WP_Groupbuy_Post_Type {

	const POST_TYPE = 'discount_codes';
	const CART_META_CODES = 'wg_applied_coupons';
	const CART_META_DISCOUNT = 'wg_coupon_discount_total';
	const NO_EXPIRATION_DATE = -1;
	private static $instances = array();

	public static $meta_keys = array(
		'code' => '_code', // string || the code
		'discount' => '_discount', // string || the discount
		'percentage' => '_is_percentage', // bool
		'deals' => '_deal_requirements', // array || deals the code can be used on
		'exp' => '_expiration', // int/time
		'purchases' => '_purchase_ids', // array || all the purchases that used the code
		'roles' => '_user_roles', // array || roles that the user must be in
		'single_use' => '_single_use', // bool
		'limit' => '_use_limit', // int/time
		'combo' => '_can_be_combined', // bool
	); // A list of meta keys this class cares about. Try to keep them in alphabetical order.


	public static function init() {
		WG_Discounts::init();
		$post_type_args = array(
			'public' => FALSE,
			'rewrite' => FALSE,
			'has_archive' => FALSE,
			'supports' => array('title'),
			);
		self::register_post_type(self::POST_TYPE, 'Discount', 'Discounts', $post_type_args);
	}

	protected function __construct( $id ) {
		parent::__construct($id);
	}

	/**
	 * return WP_Groupbuy_Discount
	 */
	public static function get_instance( $id = 0 ) {
		if ( !$id ) {
			return NULL;
		}
		if ( !isset(self::$instances[$id]) || !self::$instances[$id] instanceof self ) {
			self::$instances[$id] = new self($id);
		}
		if ( self::$instances[$id]->post->post_type != self::POST_TYPE ) {
			return NULL;
		}
		return self::$instances[$id];
	}

	/**
	 * return array List of IDs for discounts with this code
	 */
	public static function get_instance_by_code( $code ) {
		$discounts = self::find_by_meta( self::POST_TYPE, array( self::$meta_keys['code'] => $code ) );
		$discount_id = array_pop($discounts);
		$instance = self::get_instance($discount_id);
		return $instance;
	}

	public function get_code() {
		$code = $this->get_post_meta(self::$meta_keys['code']);
		if ( empty($code) || $code = '' ) {
			$code = $this->set_code();
		}
		return $this->get_post_meta(self::$meta_keys['code']);
	}


	public function get_title( $data = array() ) {
		return apply_filters('wg_discount_title', get_the_title($this->ID), $data);
	}

	/**
	 * Coupon code generator
	 */
	public function set_code( $code = null ) {
		if ( null == $code ) {
	 		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
	 		for ($i = 0; $i < 8; $i++) {
				$random .= $characters[mt_rand(0, strlen($characters))];
			}
			//$random = wp_generate_password(8, FALSE, FALSE);
			$code = implode('-', str_split($random, 4));
		}
		$this->save_post_meta(array(self::$meta_keys['code'] => $code));
		return $code;
	}

	public function get_deals() {
		return $this->get_post_meta(self::$meta_keys['deals'], TRUE);
	}

	public function set_deals( $deals = array() ) {
		$this->save_post_meta(array(
			self::$meta_keys['deals'] => $deals,
		));
		return $deals;
	}

	/**
	 * check expired
	 */
	public function is_expired() {
		if ( $this->never_expires() ) {
			return FALSE;
		}
		if ( current_time('timestamp') > (int)$this->get_exp() ) {
			return TRUE;
		}
		return FALSE;
	}

	public function get_exp() {
		return (int)$this->get_post_meta(self::$meta_keys['exp']);
	}

	public function set_exp( $exp = -1 ) {
		$this->save_post_meta(array(
			self::$meta_keys['exp'] => $exp,
		));
		return $exp;
	}

	public function never_expires() {
		return $this->get_exp() == self::NO_EXPIRATION_DATE;
	}

	public function get_purchases() {
		$meta = $this->get_post_meta(self::$meta_keys['purchases']);
		$purchases = ( !is_array( $meta ) ) ? array() : $meta ;
		return $purchases;
	}

	public function add_purchase( $purchase_id ) {
		$purchases = $this->get_post_meta(self::$meta_keys['purchases']);
		if ( !is_array($purchases) ) {
			$purchases = array();
		}
		$purchases[] = $purchase_id;
		$purchases = $this->save_post_meta( array(
			self::$meta_keys['purchases'] => $purchases,
		) );
		return $purchases;
	}

	public function get_roles() {
		return $this->get_post_meta(self::$meta_keys['roles']);
	}

	public function set_roles( $roles = array() ) {
		$this->save_post_meta(array(
			self::$meta_keys['roles'] => $roles,
		));
		return $roles;
	}

	public function get_limit() {
		return $this->get_post_meta(self::$meta_keys['limit']);
	}

	public function set_limit( $limit = 100 ) {
		$this->save_post_meta(array(
			self::$meta_keys['limit'] => $limit,
		));
		return $limit;
	}

	public function get_discount() {
		return $this->get_post_meta(self::$meta_keys['discount']);
	}

	public function set_discount( $discount = 0 ) {
		$this->save_post_meta(array(
			self::$meta_keys['discount'] => $discount,
		));
		return $discount;
	}

	public function is_single_use() {
		return $this->get_post_meta(self::$meta_keys['single_use']);
	}

	public function set_single_use( $single_use = TRUE ) {
		$this->save_post_meta(array(
			self::$meta_keys['single_use'] => $single_use,
		));
		return $single_use;
	}

	public function is_combo() {
		return $this->get_post_meta(self::$meta_keys['combo']);
	}

	public function set_combo( $combo = FALSE ) {
		$this->save_post_meta(array(
			self::$meta_keys['combo'] => $combo,
		));
		return $combo;
	}

	public function is_percentage() {
		return $this->get_post_meta(self::$meta_keys['percentage']);
	}

	public function set_percentage( $percentage = FALSE ) {
		$this->save_post_meta(array(
			self::$meta_keys['percentage'] => $percentage,
		));
		return $percentage;
	}

	public static function get_cart_discount_total() {
		global $blog_id;
		$cache = get_user_meta(get_current_user_id(), $blog_id.'_'.WP_Groupbuy_Checkouts::CACHE_META_KEY, TRUE);
		return @$cache[self::CART_META_DISCOUNT];
	}

	public function update_cart_discount( $discount_total ) {
		global $blog_id;
		$cache = get_user_meta(get_current_user_id(), $blog_id.'_'.WP_Groupbuy_Checkouts::CACHE_META_KEY, TRUE);
		if ( !is_array($cache) ) {
			$cache = array();
		}
		$cache[self::CART_META_DISCOUNT] = $discount_total;
		update_user_meta(get_current_user_id(), $blog_id.'_'.WP_Groupbuy_Checkouts::CACHE_META_KEY, $cache);
		return $cache;
	}

	public function get_cart_discounts() {
		global $blog_id;
		$cache = get_user_meta(get_current_user_id(), $blog_id.'_'.WP_Groupbuy_Checkouts::CACHE_META_KEY, TRUE);
		$discounts = $cache[self::CART_META_CODES];
		if ( !is_array($discounts) ) {
			$discounts = array();
		}
		return $discounts;
	}

	public function add_cart_discount( $discount_code, $discount_total ) {
		global $blog_id;
		$cache = get_user_meta(get_current_user_id(), $blog_id.'_'.WP_Groupbuy_Checkouts::CACHE_META_KEY, TRUE);
		if ( !is_array($cache) ) {
			$cache = array();
		}
		$cache[self::CART_META_CODES][$discount_code] = (float)$discount_total;
		update_user_meta(get_current_user_id(), $blog_id.'_'.WP_Groupbuy_Checkouts::CACHE_META_KEY, $cache);
		return $cache;
	}

	public function user_used( $user_id = NULL ) {
		if ( NULL === $user_id ) {
			$user_id = get_current_user_id();
		}
		// Purchases this discount has been used with
		$purchases = $this->get_purchases();
		// All the user's purchases
		$user_purchases = self::find_by_meta( WP_Groupbuy_Purchase::POST_TYPE, array( '_user_id' => $user_id ));
		return count(array_intersect($purchases, $user_purchases)); // Count the matches from two via diff
	}

	/**
	 * Transition the invite's status from pending to publish
	 */
	public function activate() {
		$this->post->post_status = 'publish';
		$this->save_post();
		do_action('discount_activated', $this);
	}

	public function is_active() {
		return $this->post->post_status == 'publish';
	}

	/**
	 * Find all Discounts associated with a Deal
	 * return array List of IDs of Discounts associated with the given purchase
	 */
	public static function get_discounts_by_deal( $deal_id ) {
		$discount_ids = self::find_by_meta( self::POST_TYPE, array( self::$meta_keys['deals'] => $deal_id ) );
		return $discount_ids;
	}

	/**
	 * Find all Discounts associated with a Code
	 * return array List of IDs of Discounts associated with the given purchase
	 */
	public static function get_discounts_by_code( $code ) {
		$args = array(
			'post_type' => self::POST_TYPE,
			'fields' => 'ids',
			'post_status' => array( 'publish', 'pending', 'future', 'private' ), // no drafts
			'meta_query' => array(
				array(
					'key' => self::$meta_keys['code'],
					'value' => $code
				)
			)
		 );
		$query = new WP_Query( $args );
		return $query->posts;
	}
}