<?php
/**
 * Created by PhpStorm.
 * User: sinisa
 * Date: 25/06/16
 * Time: 18:00
 */

class WP_Groupbuy_Payment_Transaction extends WP_Groupbuy_Post_Type
{
    const POST_TYPE = 'wg_payment_trans';
    const REWRITE_SLUG = 'payments_trans';
    const STATUS_COMPLETE = 'publish'; // payment has been authorized and fully captured
    const STATUS_CANCELLED = 'cancelled'; // a recurring payment has been cancelled
    private static $instances = array();
    private static $transaction_id = 0;

    const WPG_MEGAPOS_TRANSACTION_FINAL_STATUS_SUCCESS = 'SUCCESS';
    const WPG_MEGAPOS_TRANSACTION_FINAL_STATUS_INITIALIZING = 'INITIALIZING';
    const WPG_MEGAPOS_TRANSACTION_FINAL_STATUS_ERROR = 'ERROR';

    public static function init()
    {
        $post_type_args = array(
            'public' => FALSE,
            'show_ui' => TRUE,
            'show_in_menu' => 'wp-groupbuy',
            'rewrite' => FALSE,
            'has_archive' => FALSE,
            'supports' => array('title'),
        );
        self::register_post_type(self::POST_TYPE, 'PaymentTransaction', 'PaymentTransactions', $post_type_args);
        self::register_post_statuses();
    }

    private static function register_post_statuses() {
        $statuses = array(
            self::STATUS_CANCELLED => self::__('Cancelled'),
        );
        foreach ( $statuses as $status => $label ) {
            register_post_status( $status, array(
                'label' => $label,
                'exclude_from_search' => FALSE,
            ));
        }
    }

    public function __construct( $id ) {
        parent::__construct( $id );
    }

    public static function get_instance( $id = 0 ) {
        if ( !$id ) {
            return NULL;
        }
        if ( !isset( self::$instances[$id] ) || !self::$instances[$id] instanceof self ) {
            self::$instances[$id] = new self( $id );
        }
        if ( self::$instances[$id]->post->post_type != self::POST_TYPE ) {
            return NULL;
        }
        return self::$instances[$id];
    }

    public static function get_transaction_id() {
        return self::$transaction_id;
    }

    public static function set_transaction_id( $transaction_id) {
        self::$transaction_id = $transaction_id;
    }

    public static function get_transaction_status($transactionId) {
        $megaPOSTransaction = new WP_Groupbuy_MegaPOS_Transaction($transactionId);
        return $megaPOSTransaction->getStatus();
    }

    public static function get_transaction_payment_gateway($transactionId) {
        $megaPOSTransaction = new WP_Groupbuy_MegaPOS_Transaction($transactionId);
        return $megaPOSTransaction->getGateway();
    }

    private static function is_transaction_status_completed($transaction_status){
        return in_array($transaction_status,
            array(
                WP_Groupbuy_MegaPOS_Transaction::TRANS_STATUS_PROCESSED,
                WP_Groupbuy_MegaPOS_Transaction::TRANS_STATUS_INITIALIZED,
                WP_Groupbuy_MegaPOS_Transaction::TRANS_STATUS_AWAITING_CONFIRMATION
            )
        );
    }

    public static function get_transaction_final_status($transaction_status) {
        if ( self::is_transaction_status_completed($transaction_status) ) {
            return self::WPG_MEGAPOS_TRANSACTION_FINAL_STATUS_SUCCESS;
        }
        elseif ($transaction_status == WP_Groupbuy_MegaPOS_Transaction::TRANS_STATUS_INITIALIZING ) {
            return self::WPG_MEGAPOS_TRANSACTION_FINAL_STATUS_INITIALIZING;
        }
        else {
            return self::WPG_MEGAPOS_TRANSACTION_FINAL_STATUS_ERROR;
        }
    }

    // the current query is for the merchant post type
    public static function is_payment_transaction_query() {
        $post_type = get_query_var( 'post_type' );
        if ( $post_type == self::POST_TYPE ) {
            return TRUE;
        }
        return FALSE;
    }
}