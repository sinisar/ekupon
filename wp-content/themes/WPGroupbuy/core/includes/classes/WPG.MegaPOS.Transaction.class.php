<?php

class WP_Groupbuy_MegaPOS_Transaction extends WP_Groupbuy_Model {

    private $table = 'megapos_transactions';

    private $transactionId;
    private $amount;
    private $customerName;
    private $customerSurname;
    private $email;
    private $language;
    private $transactionType;
    private $gateway;
    private $gatewayId;
    private $currency;
    private $status;
    private $storeId;
    private $additionalData;
    private $statusURL;
    private $updateURL;
    private $customerIP;

    const TRANS_STATUS_INITIALIZING = "INITIALIZING";
    const TRANS_STATUS_INITIALIZED = "INITIALIZED";
    const TRANS_STATUS_PROCESSED = "PROCESSED";
    const TRANS_STATUS_AWAITING_CONFIRMATION = "AWAITING-CONFIRMATION";
    const TRANS_STATUS_FAILED = "FAILED";
    const TRANS_STATUS_CANCELLED = "CANCELLED";

    public function getStatus() {
        return $this->status;
    }

    public function getGateway() {
        return $this->gateway;
    }

    public function getTransactionType() {
        return $this->transactionType;
    }

    public function getAmount() {
        return $this->amount;
    }

    public function getCurrency() {
        return $this->currency;
    }

    public function __construct($transactionId = NULL) {
        if (!is_null($transactionId)) {
            $this->transactionId = $transactionId;
            $this->load_transaction();
        }
    }

    protected function load_transaction() {
        if ( $transactionData = $this->get_transaction_by_id($this->transactionId) ) {
            $this->apply_data_array_to_object($transactionData);
            return TRUE;
        } else {
            return FALSE;
        }
    }

    private function get_table_full_name() {
        global $wpdb;

        return $wpdb->prefix . $this->table;
    }

    private function get_transaction_by_id($transactionId) {
        global $wpdb;

        $sql = "SELECT * FROM ". $this->get_table_full_name() ." WHERE transactionId = %s";
        return $wpdb->get_row($wpdb->prepare($sql, $transactionId), ARRAY_A);
    }

    public function save($transactionData) {
        $this->convert_input_data_to_object($transactionData);
        return $this->crate_transaction();
    }

    public function update($transactionData) {
        $this->convert_input_data_to_object($transactionData);
        return $this->update_transaction();
    }

    protected function crate_transaction() {
        global $wpdb;

        return $wpdb->insert($this->get_table_full_name(), $this->get_data_as_array());
    }

    protected function update_transaction() {
        global $wpdb;

        return $wpdb->update($this->get_table_full_name(), $this->get_data_as_array(),
            array('transactionId' => $this->transactionId));
    }

    public function update_status($status) {
        global $wpdb;

        return $wpdb->update($this->get_table_full_name(), array('status' => $status),
            array('transactionId' => $this->transactionId));
    }

    private function apply_data_array_to_object($dataArray) {
        $this->transactionId = $dataArray['transactionId'];
        $this->transactionType = $dataArray['transactionType'];
        $this->gateway = $dataArray['gateway'];
        $this->gatewayId = $dataArray['gatewayId'];
        $this->storeId = $dataArray['storeId'];
        $this->amount = $dataArray['amount'];
        $this->currency = $dataArray['currency'];
        $this->status = $dataArray['status'];
        $this->customerName = $dataArray['customerName'];
        $this->customerSurname = $dataArray['customerSurname'];
        $this->email = $dataArray['email'];
        $this->language = $dataArray['language'];
        $this->customerIP = $dataArray['customerIP'];
        $this->additionalData = $dataArray['additionalData'];
        $this->statusURL = $dataArray['statusURL'];
        $this->updateURL = $dataArray['updateURL'];
    }

    public function get_data_as_array() {
        return array(
            'transactionId' => $this->transactionId,
            'transactionType' => $this->transactionType,
            'gateway' => $this->gateway,
            'gatewayId' => $this->gatewayId,
            'storeId' => $this->storeId,
            'amount' => $this->amount,
            'currency' => $this->currency,
            'status' => $this->status,
            'customerName' => $this->customerName,
            'customerSurname' => $this->customerSurname,
            'email' => $this->email,
            'language' => $this->language,
            'customerIP' => $this->customerIP,
            'additionalData' => $this->additionalData,
            'statusURL' => $this->statusURL,
            'updateURL' => $this->updateURL
        );
    }

    private function convert_input_data_to_object($data) {
        $this->transactionId = $data['transaction-id'];
        $this->transactionType = $data['transaction-type'];
        $this->gateway = $data['gateway'];
        $this->gatewayId = $data['gateway-id'];
        $this->storeId = $data['store-id'];
        $this->amount = $data['amount'];
        $this->currency = $data['currency'];
        $this->status = $data['status'];
        $this->customerName = $data['customer-name'];
        $this->customerSurname = $data['customer-surname'];
        $this->email = $data['email'];
        $this->language = $data['language'];
        $this->customerIP = $data['customer-ip'];
        $this->additionalData = $data['additional-info'];
        $this->statusURL = $data['status-url'];
        $this->updateURL = $data['update-url'];
    }

} 