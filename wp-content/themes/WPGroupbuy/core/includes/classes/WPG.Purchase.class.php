<?php

add_action( 'wg_init_classes', array( 'WP_Groupbuy_Purchase', 'init' ), 40 );

class WP_Groupbuy_Purchase extends WP_Groupbuy_Post_Type {
	const POST_TYPE = 'wg_purchase';
	const REWRITE_SLUG = 'purchase';
	const NO_USER = -1;
	private static $instances = array();


	protected static $meta_keys = array(
		'account_id' => '_account_id', 
		'auth_key' => '_auth_key', 
		'deal_id' => '_deal_id',
		'gateway_data' => '_gateway_data', 
		'original_owner' => '_original_owner', 
		'products' => '_products', 
		'shipping' => '_shipping', 
		'shipping_local' => '_shipping_address', 
		'subtotal' => '_subtotal',
		'tax' => '_tax',
		'total' => '_total',
		'user_id' => '_user_id', 
		'user_ip' => '_user_ip',
        'billing_information' => '_billing_information',
	); 

	// All the items that were purchased
	protected $products = array();

	public static function init() {
		$post_type_args = array(
			'show_ui' => FALSE,
			'show_in_menu' => 'wp-groupbuy',
			'rewrite' => array(
				'slug' => self::REWRITE_SLUG,
				'with_front' => FALSE,
			),
			'has_archive' => FALSE,
			'supports' => array( 'title' ),
		);
		self::register_post_type( self::POST_TYPE, 'Purchase', 'Purchases', $post_type_args );
	}

	protected function __construct( $id ) {
		parent::__construct( $id );
	}


	// Update with fresh data from the database
	protected function refresh() {
		parent::refresh();
		$this->products = (array)$this->get_post_meta( self::$meta_keys['products'] );
	}

	public static function get_instance( $id = 0 ) {
		if ( !$id ) {
			return new WP_Groupbuy_Purchase();
		}
		if ( !isset( self::$instances[$id] ) || !self::$instances[$id] instanceof self ) {
			self::$instances[$id] = new self( $id );
		}
		if ( !self::$instances[$id]->post || self::$instances[$id]->post->post_type != self::POST_TYPE ) {
			return NULL;
		}
		return self::$instances[$id];
	}


	public static function new_purchase( $args = array() ) {
		$default = array(
			'post_title' => sprintf( self::__( 'Order %s' ), $title_suffix ),
			'post_status' => 'pending',
			'post_type' => self::POST_TYPE,
		);
		$id = wp_insert_post( $default );
		if ( is_wp_error( $id ) ) {
			return 0;
		}
		$purchase = self::get_instance( $id );
		$purchase->set_title( sprintf( self::__( 'Order #%d' ), $id ) );
		if ( isset( $args['user'] ) && is_numeric( $args['user'] ) ) {
			$purchase->set_account_id( WP_Groupbuy_Account::get_account_id_for_user( (int)$args['user'] ) );
			//  TODO: check, if using gift, no user is set ...
			$purchase->set_user( (int)$args['user'] );
			$purchase->set_original_user( (int)$args['user'] );
		}
		if ( isset( $args['cart'] ) && is_a( $args['cart'], 'WP_Groupbuy_Cart' ) ) {
			$items = $args['cart']->get_items();
			foreach ( $items as $key => $item ) {
				$deal = WP_Groupbuy_Deal::get_instance( $item['deal_id'] );
				if ( is_object( $deal ) ) {
					$price = $deal->get_coupon_price( NULL, $item['data'] )*$item['quantity'];
					$items[$key]['price'] = $price;
					$items[$key]['unit_price'] = $deal->get_coupon_price( NULL, $item['data'] );
					$items[$key]['payment_method'] = array();
				}
			}
			$purchase->set_products( $items );
			$purchase->set_subtotal( $args['cart']->get_subtotal() );
			$purchase->set_tax_total( $args['cart']->get_tax_total() );
			$purchase->set_shipping_total( $args['cart']->get_shipping_total() );
			$purchase->set_total( $args['cart']->get_total() );
		} elseif ( isset( $args['items'] ) && is_array( $args['items'] ) ) {
			$purchase->set_products( $args['items'] );
			$price = 0;
			foreach ( $args['items'] as $item ) {
				$price += $item['price'];
			}
			$purchase->set_shipping_total( 0 );
			$purchase->set_tax_total( 0 );
			$purchase->set_subtotal( $price );
			$purchase->set_total( $price );
		} else {
			$purchase->set_products( array() );
		}
        $purchase->set_billing_information($args['checkout']->cache['billing']);
		$purchase->set_user_ip();
		if ( self::DEBUG ) error_log( "new purchase: " . print_r( $purchase, true ) );
		if ( self::DEBUG ) error_log( "debug_backtrace: " . print_r( debug_backtrace(), true ) );
		do_action( 'wg_new_purchase', $purchase, $args );
		if ( self::DEBUG ) error_log( "new purchase after action: " . print_r( $purchase, true ) );
		return $id;
	}

	public static function delete_purchase( $id ) {
		$post_type = get_post_type( $id );
		if ( $post_type == self::POST_TYPE ) {
			do_action( 'deleting_purchase', $id );
			wp_delete_post( $id, TRUE );
			if ( isset( self::$instances[$id] ) ) {
				unset( self::$instances[$id] );
			}
			do_action( 'deleted_purchase', $id );
		}
	}

	public function complete() {
		if ( $this->is_settled() ) {
			do_action( 'completing_purchase', $this );
			$this->post->post_status = 'publish';
			$this->save_post();
			$this->clear_caches();
			do_action( 'purchase_completed', $this );
		}
	}

	public function get_products() {
		return $this->products;
	}

	public function set_products( $products ) {
		$this->products = $products;
		$this->save_post_meta( array(
				self::$meta_keys['products'] => $products,
			) );
		delete_post_meta( $this->ID, self::$meta_keys['deal_id'] );
		foreach ( $this->products as $product ) {
			add_post_meta( $this->ID, self::$meta_keys['deal_id'], $product['deal_id'] );
		}
	}

	// Set the user ID for this purchase
	public function set_user( $user_id ) {
		$this->save_post_meta( array(
				self::$meta_keys['user_id'] => $user_id,
			) );
	}

	// The ID of the user who made this purchase
	public function get_user() {
		return (int)$this->get_post_meta( self::$meta_keys['user_id'] );
	}

	public function set_account_id( $account_id ) {
		$this->save_post_meta( array(
				self::$meta_keys['account_id'] => $account_id
			) );
	}

	public function get_account_id( $reset = FALSE ) {
		$account_id = $this->get_post_meta( self::$meta_keys['account_id'] );
		if ( $reset || empty( $account_id ) || !WP_Groupbuy_Account::is_account( $account_id ) ) {
			$user_id = $this->get_user();
			$account_id = WP_Groupbuy_Account::get_account_id_for_user( $user_id );
			$this->set_account_id( $account_id );
		}
		return $account_id;
	}

	public function get_subtotal( $payment_method = NULL ) {
		if ( $payment_method === NULL ) {
			return $this->get_post_meta( self::$meta_keys['subtotal'] );
		} else {
			$total = 0;
			foreach ( $this->products as $item ) {
				if ( isset( $item['payment_method'][$payment_method] ) ) {
					$total += $item['payment_method'][$payment_method];
				}
			}
			return apply_filters( 'wpg_get_subtotal_purchase', $total, $this, $payment_method );
		}
	}

	public function set_subtotal( $subtotal ) {
		$this->save_post_meta( array(
				self::$meta_keys['subtotal'] => $subtotal
			) );
	}

	public function get_item_tax( $item ) {
		return WP_Groupbuy_Core_Tax::purchase_item_tax( $this, $item );
	}

	public function get_tax_total( $payment_method = NULL, $local = NULL ) {
		if ( $payment_method === NULL ) {
			return $this->get_post_meta( self::$meta_keys['tax'] );
		} else {
			return WP_Groupbuy_Core_Tax::purchase_tax_total( $this, $payment_method, $local );
		}
	}

	public function set_tax_total( $tax ) {
		$this->save_post_meta( array(
				self::$meta_keys['tax'] => $tax
			) );
	}

	// Shipping total for the purchase, calculated if items have multiple payment methods.
	public function get_item_shipping( $item, $local = NULL, $distribute = TRUE ) {
		return WP_Groupbuy_Core_Shipping::purchase_item_shipping( $this, $item, $local, $distribute );
	}

	// Set the shipping local
	public function get_shipping_local() {
		return $this->get_post_meta( self::$meta_keys['shipping_local'] );
	}

	public function set_shipping_local( $local ) {
		$this->save_post_meta( array(
				self::$meta_keys['shipping_local'] => $local
			) );
	}

	// Shipping total for the purchase, calculated if items have multiple payment methods.
	public function get_shipping_total( $payment_method = NULL, $local = NULL ) {
		if ( $payment_method === NULL ) {
			return $this->get_post_meta( self::$meta_keys['shipping'] );
		} else {
			return WP_Groupbuy_Core_Shipping::purchase_shipping_total( $this, $payment_method, $local );
		}
	}

	public function set_shipping_total( $shipping ) {
		$this->save_post_meta( array(
				self::$meta_keys['shipping'] => $shipping
			) );
	}

	public function set_total( $total ) {
		$this->save_post_meta( array(
				self::$meta_keys['total'] => $total
			) );
	}

    public function get_billing_information() {
        return $this->get_post_meta( self::$meta_keys['billing_information'] );
    }

    public function set_billing_information( $billing_information ) {
        $this->save_post_meta( array(
            self::$meta_keys['billing_information'] => $billing_information
        ) );
    }

	public function get_total( $payment_method = NULL, $local = NULL, $local_billing = NULL ) {
		if ( $payment_method === NULL ) {
			return $this->get_post_meta( self::$meta_keys['total'] );
		} else {
			$total = $this->get_subtotal( $payment_method );
			$total += $this->get_shipping_total( $payment_method, $local );
			$total += $this->get_tax_total( $payment_method, $local_billing );
			return apply_filters( 'wg_purchase_get_total', $total, $this, $payment_method, $local, $local_billing );
		}
	}

	// The IDs of the payments that paid for this purchase
	public function get_payments() {
		return WP_Groupbuy_Payment::get_payments_for_purchase( $this->ID );
	}

	// The IDs of all vouchers associated with this purchase
	public function get_vouchers() {
		return WP_Groupbuy_Voucher::get_vouchers_for_purchase( $this->ID );
	}

	// Get a list of Purchase IDs, filtered by $args
	public static function get_purchases( $args = array(), $meta = array() ) {
		if ( isset( $args['deal'] ) ) {
			if ( is_array( $args['deal'] ) ) {
				$purchase_ids = array();
				foreach ( $args['deal'] as $deal_id ) {
					$meta[self::$meta_keys['deal_id']] = $deal_id;
					$purchase_ids = array_merge( $purchase_ids, self::find_by_meta( self::POST_TYPE, $meta ) );
				}
				return $purchase_ids;
			} else {
				$meta[self::$meta_keys['deal_id']] = $args['deal'];
			}
		}

		if ( isset( $args['account'] ) || isset( $args['user'] ) ) {
			$meta[self::$meta_keys['user_id']] = isset( $args['user'] ) ? (int)$args['user'] : WP_Groupbuy_Account::get_user_id_for_account( (int)$args['account'] );
		}

		$purchase_ids = self::find_by_meta( self::POST_TYPE, $meta );
		return $purchase_ids;
	}

	// Get the total quantity of a product in this purchase
	public function get_product_quantity( $product_id, $data = NULL ) {
		$quantity = 0;
		if ( !is_array( $this->products ) ) {
			return 0;
		}
		foreach ( $this->products as $product ) {
			if ( $product['deal_id'] == $product_id && ( is_null( $data ) || $product['data'] == $data ) ) {
				if ( is_null( $data ) ) {
					$quantity += $product['quantity'];
				} elseif ( $product['data'] && is_array( $product['data'] ) && is_array( $data ) ) {
					$match = TRUE;
					foreach ( $data as $key => $value ) {
						if ( !( isset( $product['data'][$key] ) && $product['data'][$key] == $value ) ) {
							$match = FALSE;
						}
					}
					if ( $match ) {
						$quantity += $product['quantity'];
					}
				}

			}
		}
		return $quantity;
	}

	// Get the total quantity of a product in this purchase
	public function get_product_price( $product_id, $data = NULL ) {
		$price = 0;
		if ( !is_array( $this->products ) ) {
			return 0;
		}
		foreach ( $this->products as $product ) {
			if ( $product['deal_id'] == $product_id && ( is_null( $data ) || $product['data'] == $data ) ) {
				$price += $product['price'];
			}
		}
		return $price;
	}

	// Get the total quantity of a product in this purchase
	public function get_product_unit_price( $product_id, $data = NULL ) {
		$price = 0;
		if ( !is_array( $this->products ) ) {
			return 0;
		}
		foreach ( $this->products as $product ) {
			if ( $product['deal_id'] == $product_id && ( is_null( $data ) || $product['data'] == $data ) ) {
				$price = $product['unit_price'];
			}
		}
		return $price;
	}

	public function set_original_user( $user_id ) {
		$this->save_post_meta( array(
				self::$meta_keys['original_owner'] => $user_id,
			) );
	}

	public function get_original_user() {
		return (int)$this->get_post_meta( self::$meta_keys['original_owner'] );
	}

	public function set_auth_key( $auth_key ) {
		$this->save_post_meta( array(
				self::$meta_keys['auth_key'] => $auth_key,
			) );
	}

	public function get_auth_key() {
		return $this->get_post_meta( self::$meta_keys['auth_key'] );
	}

	public function set_gateway_data( $gateway_data ) {
		$this->save_post_meta( array(
				self::$meta_keys['gateway_data'] => $gateway_data,
			) );
	}

	public function get_gateway_data() {
		return $this->get_post_meta( self::$meta_keys['gateway_data'] );
	}

	public function set_user_ip() {
		$this->save_post_meta( array(
				self::$meta_keys['user_ip'] => $_SERVER['REMOTE_ADDR']
			) );
	}

	public function get_user_ip() {
		return $this->get_post_meta( self::$meta_keys['user_ip'] );
	}

	public function is_complete() {
		return $this->post->post_status == 'publish';
	}

	public function is_pending() {
		return $this->post->post_status == 'pending';
	}

	public function is_settled() {
		return $this->post->post_status != 'unsettled';
	}

	public function set_unsettled_status() {
		$this->post->post_status = 'unsettled';
		$this->save_post();
	}

	public function set_pending() {
		$this->post->post_status = 'pending';
		$this->save_post();
	}

	public static function get_purchase_by_key( $key = null ) {
		if ( null == $key ) return;
		$purchase_ids = self::find_by_meta( self::POST_TYPE, array( self::$meta_keys['auth_key'] => $key ) );
		return $purchase_ids[0];
	}

	// cache
	private function clear_caches() {
		$products = $this->get_products();
		foreach ( $products as $product ) {
			WP_Groupbuy_Controller::clear_post_cache($product['deal_id']);
		}
	}

}
