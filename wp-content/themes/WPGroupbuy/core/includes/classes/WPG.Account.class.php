<?php

add_action( 'wg_init_classes', array( 'WP_Groupbuy_Account', 'init' ), 15 );

class WP_Groupbuy_Account extends WP_Groupbuy_Post_Type {
	const POST_TYPE = 'wg_account';
	const SUSPEND = 'suspended';
	const REWRITE_SLUG = 'account';
	const CREDIT_TYPE_DEFAULT = 'default';
	const NO_MAXIMUM = -1;

	const CACHE_PREFIX = 'wpg_account';
	const CACHE_GROUP = 'wpg_account';

	protected $user_id;
	private static $instances;

	private static $meta_keys = array(
		'user_id' => '_user_id',
		'first_name' => '_first_name',
		'last_name' => '_last_name',
		'account_email' => '_account_email',
		'account_phone' => '_account_phone',
		'address' => '_address',
		'ship_address' => '_ship_address',
		'data' => '_data',
	);

	public static function init() {
		$post_type_args = array(
			'has_archive' => FALSE,
			'public' => FALSE,
			'publicly_queryable' => FALSE,
			'exclude_from_search' => TRUE,
			'show_ui' => true,
			'show_in_menu' => 'wp-groupbuy/accounts',
			'supports' => array( null ),
			'rewrite' => FALSE,
		);

		self::register_post_type( self::POST_TYPE, 'Account', 'Accounts', $post_type_args );

		add_action( 'pre_get_posts', array( get_class(), 'filter_dummy' ), 10, 1 );

		add_action( 'update_post_meta', array( get_class(), 'maybe_clear_id_cache' ), 10, 4 );
		add_action( 'added_post_meta', array( get_class(), 'maybe_clear_id_cache' ), 10, 4 );
		add_action( 'delete_post_meta', array( get_class(), 'maybe_clear_id_cache' ), 10, 4 );
	}

	protected function __construct( $user_id, $account_id = 0 ) {
		$this->user_id = $user_id;
	
		if ( !$account_id ) {
			$account_id = self::get_account_id_for_user( $user_id );	

		}
		parent::__construct( $account_id );
	}


	public static function get_instance( $user_id = 0, $account_id = 0  ) {
		if ( $user_id == -1 ) {
			return FALSE;
		}
		if ( !$user_id ) {
			$user_id = get_current_user_id();
		}
		if ( !$account_id ) {
			if ( !$account_id = self::get_account_id_for_user( $user_id ) ) {
				return NULL;
			}
		}
		if ( !isset( self::$instances[$account_id] ) || !self::$instances[$account_id] instanceof self ) {
			self::$instances[$account_id] = new self( $user_id, $account_id );
		}
		if ( self::$instances[$account_id]->post->post_type != self::POST_TYPE ) {
			return NULL;
		}
		return self::$instances[$account_id];
	}

	public static function get_instance_by_id( $account_id = 0 ) {
		$user_id = self::get_user_id_for_account( $account_id );
		if ( !$user_id ) {
			return self::get_instance( 0, 0 );
		}
		return self::get_instance( $user_id, $account_id );
	}

	public static function get_account_id_for_user( $user_id = 0 ) {

		if ( $user_id == -1 ) {
			return FALSE;
		}
		if ( !is_numeric( $user_id ) ) {
			$user_id = 0;
		}
		if ( !$user_id ) {
			$user_id = (int)get_current_user_id();
		}

		if ( $account_id = self::get_id_cache( $user_id ) ) {

			return $account_id;
		}

		$account_ids = self::find_by_meta( self::POST_TYPE, array( self::$meta_keys['user_id'] => $user_id ) );

		if ( empty( $account_ids ) ) {
			$account_id = self::new_account( $user_id );
		} else {
			$account_id = $account_ids[0];
		}

		self::set_id_cache( $user_id, $account_id );

		return $account_id;
	}

	public static function get_account_id_for_user_name($user_first_name, $user_last_name) {

	    $search_meta_key = array();
	    if($user_first_name != "") {
            $search_meta_key = array(self::$meta_keys['first_name'] => $user_first_name);
            if($user_last_name !=  "") {
	            $search_meta_key = array_merge($search_meta_key, array(self::$meta_keys['last_name'] => $user_last_name));
            }
        }
        else {
	        if($user_last_name != null) {
                $search_meta_key = array(self::$meta_keys['last_name'] => $user_last_name);
            }
        }

        if(!empty($search_meta_key)) {
            $account_ids = self::find_by_meta( self::POST_TYPE, $search_meta_key );
        }
        if ( !empty( $account_ids ) ) {
            return $account_ids;
        }
        else {
            return 0;
        }
    }

	public static function get_user_id_for_account( $account_id ) {
		if ( isset( self::$instances[$account_id] ) ) {
			return self::$instances[$account_id]->user_id;
		} else {
			$user_id = get_post_meta( $account_id, self::$meta_keys['user_id'], TRUE );
			return $user_id;
		}
	}

	// Create a new account
	private static function new_account( $user_id ) {
		if ( $user_id ) {
			$user = get_userdata( $user_id );
			if ( !is_a( $user, 'WP_User' ) ) {
				return NULL;
			}
			$title = $user->user_login;
			if ( $user->user_nicename ) {
				$title = $user->user_nicename;
			}
			$post_data = array(
				'post_title' => $title,
				'post_name' => $user->user_login,
				'post_status' => 'publish',
				'post_type' => self::POST_TYPE,
			);
		} else {
			$post_data = array(
				'post_title' => self::__( 'Anonymous' ),
				'post_status' => 'publish',
				'post_type' => self::POST_TYPE,
			);
		}

		// #138 Fix duplicated post issue
		$post = get_page_by_title( $post_data['post_title'], OBJECT, self::POST_TYPE );
				
		if ( $post ) {
			$id = $post->ID;
		} else {
			$id = wp_insert_post( $post_data );
			if ( !is_wp_error( $id ) ) {
				if ( is_a( $user_id, 'WP_User' ) ) {
					$user_id = $user_id->ID;
				}
				update_post_meta( $id, self::$meta_keys['user_id'], $user_id );
			}
		}

		return $id;
	}

	public static function maybe_clear_id_cache( $meta_id, $object_id, $meta_key, $meta_value ) {
		if ( $meta_key == self::$meta_keys['user_id'] && get_post_type($object_id) == self::POST_TYPE ) {
			if ( $meta_value ) { // this might be empty on a delete_post_meta
				self::clear_id_cache($meta_value);
			}
			$old_value = get_post_meta($object_id, $meta_key);
			if ( $old_value != $meta_value ) { // this is probably the same on added_post_meta
				self::clear_id_cache($old_value);
			}
		}
	}

	// Get the account ID cache for the user
	private static function get_id_cache( $user_id ) {
		return (int)wp_cache_get(self::CACHE_PREFIX.'_id_'.$user_id, self::CACHE_GROUP);
	}

	// Set the account ID cache for the user
	private static function set_id_cache( $user_id, $account_id ) {
		wp_cache_set(self::CACHE_PREFIX.'_id_'.$user_id, (int)$account_id, self::CACHE_GROUP);
	}

	// Delete the account ID cache for the user
	private static function clear_id_cache( $user_id ) {
		wp_cache_delete(self::CACHE_PREFIX.'_id_'.$user_id, self::CACHE_GROUP);
	}

	// Edit the query on the account edit page to remove the dummy post
	public static function filter_dummy( WP_Query $query ) {
		if ( is_admin() ) {
			global $wpdb;
			if ( isset( $query->query_vars['post_type'] ) && self::POST_TYPE == $query->query_vars['post_type'] ) {
				$blank_account_id = $wpdb->get_var( $wpdb->prepare( "SELECT ID from {$wpdb->posts} WHERE post_type = %s and post_title = %s", self::POST_TYPE, self::__( 'Anonymous' ) ) );
				if ( isset( $query->query_vars['post__not_in'] ) && !empty( $query->query_vars['post__not_in'] ) ) {
					$query->query_vars['post__not_in'][] = $blank_account_id;
				} else {
					$query->query_vars['post__not_in'] = array( $blank_account_id );
				}
			}
		}
	}

	// Get the user's balance for the given credit type
	public function get_credit_balance( $credit_type = self::CREDIT_TYPE_DEFAULT ) {
		$balance = $this->get_post_meta( "_credit_balance_".$credit_type );
		if ( is_null( $balance ) || $balance < 0.01 ) {
			$balance = 0;
		}
		return $balance;
	}

	// Set the user's balance for the given credit type
	public function set_credit_balance( $balance, $credit_type = self::CREDIT_TYPE_DEFAULT ) {
		$this->save_post_meta( array(
				"_credit_balance_".$credit_type => $balance
			) );
	}

	// Add to the user's balance for the given credit type
	public function add_credit( $qty, $credit_type = self::CREDIT_TYPE_DEFAULT ) {
		$balance = $this->get_credit_balance( $credit_type );
		$balance = $balance + $qty;
		$this->set_credit_balance( $balance, $credit_type );
		return $balance;
	}

	// Deduct from the user's balance for the given credit type
	public function deduct_credit( $qty, $credit_type = self::CREDIT_TYPE_DEFAULT ) {
		$balance = $this->get_credit_balance( $credit_type );
		$total = $balance - $qty;
		if ( $total < 0.01 ) {
			$total = 0;
		}
		$this->set_credit_balance( $total, $credit_type );
	}

	// Move credits to a holding area so they can't be spent
	public function reserve_credit( $qty, $credit_type = self::CREDIT_TYPE_DEFAULT ) {
		$this->deduct_credit( $qty, $credit_type );
		return $this->add_credit( $qty, $credit_type.'_reserved' );
	}

	// Move credits out of the holding area
	public function restore_credit( $qty, $credit_type = self::CREDIT_TYPE_DEFAULT ) {
		$this->deduct_credit( $qty, $credit_type.'_reserved' );
		return $this->add_credit( $qty, $credit_type );
	}

	public function can_purchase( $deal_id, $data = array() ) {
		$qty = self::NO_MAXIMUM;
		$deal = WP_Groupbuy_Deal::get_instance( $deal_id );
		if ( !is_a( $deal, 'WP_Groupbuy_Deal' ) ) {
			return 0;
		}
		if ( $deal->is_closed() ) {
			return 0;
		}
		$max_purchases_per_user = $deal->get_max_purchases_per_user();
		if ( $max_purchases_per_user >= 0 ) {
			$total_purchased = 0;
			$purchases = $deal->get_purchases_by_account( $this->ID );
			if ( $purchases ) {
				foreach ( $purchases as $purchase_id ) {
					$purchase = WP_Groupbuy_Purchase::get_instance( $purchase_id );
					$number_purchased = $purchase->get_product_quantity( $deal_id );
					$total_purchased += $number_purchased;
				}
			}
			$qty = $max_purchases_per_user - $total_purchased;
		}
		$remaining = $deal->get_remaining_allowed_purchases();
		if ( $remaining >= 0 && ( $remaining < $qty || $qty == self::NO_MAXIMUM ) ) {
			$qty = $remaining;
		}

        //  by default enable purchases: if min req. purchases = 0; maximumx purchases = NotSet; maximum purchases per user: NotSet;
        //  skip checking of checking successfull deal
		if($deal->get_max_purchases() == self::NO_MAXIMUM && $max_purchases_per_user == self::NO_MAXIMUM && $deal->get_min_purchases() == 0) {
		    $qty = 1000;
        }

		$qty = apply_filters( 'account_can_purchase', $qty, $deal_id, $data, $this );
		return $qty;
	}

	public function get_name( $component = NULL ) {
		$first = $this->get_post_meta( self::$meta_keys['first_name'] );
		$last = $this->get_post_meta( self::$meta_keys['last_name'] );
		$email = $this->get_post_meta( self::$meta_keys['account_email'] );
		$phone = $this->get_post_meta( self::$meta_keys['account_phone'] );
		switch ( $component ) {
		case 'first':
			return $first;
		case 'last':
			return $last;
		case 'email':
			return $email;
		case 'phone':
			return $phone;
		default:
			return $first.' '.$last;
		}
	}

	public function set_name( $component = NULL, $value ) {
		switch ( $component ) {
		case 'first':
			$this->save_post_meta( array( self::$meta_keys['first_name'] => $value ) );
			break;
		case 'last':
			$this->save_post_meta( array( self::$meta_keys['last_name'] => $value ) );
			break;
		case 'email':
			$this->save_post_meta( array( self::$meta_keys['account_email'] => $value ) );
			break;
		case 'phone':
			$this->save_post_meta( array( self::$meta_keys['account_phone'] => $value ) );
			break;
		}
	}

	public function get_data() {
		return $this->get_post_meta( self::$meta_keys['data'] );
	}

	public function set_data( $address ) {
		return $this->save_post_meta( array( self::$meta_keys['data'] => $address ) );
	}

	public function get_address() {
		return $this->get_post_meta( self::$meta_keys['address'] );
	}

	public function set_address( $address ) {
		return $this->save_post_meta( array( self::$meta_keys['address'] => $address ) );
	}

	public function get_ship_address() {
		return $this->get_post_meta( self::$meta_keys['ship_address'] );
	}

	public function set_ship_address( $ship_address ) {
		return $this->save_post_meta( array( self::$meta_keys['ship_address'] => $ship_address ) );
	}

	public function get_user_id() {
		$user_id = $this->user_id;
		if ( is_a( $user_id, 'WP_User' ) ) {
			$user_id = $user_id->ID;
			update_post_meta( $this->get_id(), self::$meta_keys['user_id'], $user_id );
		}
		return $user_id;
	}

	public function get_user() {
		$user = new WP_User( $this->user_id );
		return $user;
	}

	public static function is_account( $account_id ) {
		if ( is_object( $account_id ) ) {
			if ( $account_id instanceof self ) {
				return TRUE;
			}
		}
		if ( self::POST_TYPE === get_post_type( $account_id ) ) {
			return TRUE;
		}
		return FALSE;
	}

	// Suspend an account
	public function suspend() {
		$this->post->post_status = self::SUSPEND;
		$this->save_post();
		do_action( 'account_suspended', $this );
	}

	public function unsuspend() {
		$this->post->post_status = 'publish';
		$this->save_post();
		do_action( 'account_unsuspended', $this );
	}

	public function is_suspended() {
		return $this->post->post_status == self::SUSPEND;
	}

//	returns array of user mailinglists - an array with id => title
	public function get_user_mailinglists_ids() {
        $current_user = wp_get_current_user();
        //  load user email from ID
        $user_email = $current_user->user_email;

        //  load subscriber
        $data = array();
        $data['registered'] = 'Y';
        $data['user_id'] = $current_user;
        $data['email'] = $user_email;
        $subscribers = new wpmlSubscriber($data);

        //  load subscriber email
        $subscriber = $subscribers->get_by_email($user_email);
        $subscriber_id = $subscriber[0]->id;

		//	find all subscribed newsletter lists
		$subscribersList = new wpmlSubscribersList($data);
		$user_subscriberList = $subscribersList->find_all( array('subscriber_id' => $subscriber_id));
		if(count($user_subscriberList) == 0) {
			return array();
		}
		//	add list ids to array
		$mailingList = array();
		foreach($user_subscriberList as $listItem) {
			$mailingList[] = $listItem->list_id;
		}
		//	get list of nr=>id
		return $mailingList;
	}

	public function get_user_mailinglists() {
		$mailingList = $this->get_user_mailinglists_ids();

		//	get list of id=>title mailinglists
		return get_mailinglist_array_from_ids($mailingList);
	}

	//	update users mailinglists
	public function set_mailinglist($mailinglists_array) {
		$current_user = wp_get_current_user();
		//  load user email from ID
        $user_email = $current_user->user_email;

        //  load subscriber
        $data = array();
        $data['registered'] = 'Y';
        $data['user_id'] = $current_user->ID;
        $data['email'] = $user_email;
        $subscribers = new wpmlSubscriber($data);

        //  load subscriber email
        $subscriber = $subscribers->get_by_email($user_email);
        $subscriber_id = $subscriber[0]->id;

		$data = array();
		$data['subscriber_id'] = $subscriber_id;

		//	find all subscribed newsletter lists for current user
		$subscribersList = new wpmlSubscribersList($data);
		$user_subscriberList = $subscribersList->find_all( $data );

		$db_list_ids = array();
		$data = array();
		$data['subscriber_id'] = $subscriber_id;

		//	check values from DB
		foreach($user_subscriberList as $listItem) {
			//	parse list_id values from DB object
			$db_list_ids[] = $listItem->list_id;
			//	list_id not in $_POST => remove it
			if(!in_array($listItem->list_id, $mailinglists_array)) {
				$data['list_id']= $listItem->list_id;
				$subscribersList->delete_all($data);
			}
		}

		$data = array();
		$data['subscriber_id'] = $subscriber_id;

		//	check values from $_POST
		foreach($mailinglists_array as $listKey => $listValue) {
			//	$listValue not in DB, add it
			if(!in_array($listValue, $db_list_ids)) {
				$data['list_id']= $listValue;
                $data['active'] = 'Y';
				$subscribersList->save($data, TRUE, FALSE);
			}
		}

		$data = array();
		$data['subscriber_id'] = $subscriber_id;

		//	count all list_ids from DB and compare them ...
		$user_subscriberList = $subscribersList->count( $data );
		if(count($mailinglists_array) == $user_subscriberList) {
			return true;
		}
		else {
			return false;
		}
	}

}
