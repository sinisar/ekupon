<?php
class WPG_Gifts_Upgrader extends WPG_Upgrader {
	public function upgrade_to_2_3() {
		$posts = get_posts(
			array(
				'post_type' => 'wg_gift',
				'post_status' => 'publish',
				'posts_per_page' => 1000,
			)
		);

		$count = 0;
		foreach ( $posts as $post ) {
			$gift = WP_Groupbuy_Gift::get_instance( $post->ID );

			$purchase = $gift->get_purchase();

			if ( $purchase ) {
				// var_dump($post->post_date , $post->post_modified);
				if ( $purchase->get_user() < 0 && $post->post_date == $post->post_modified ) {
					$gift->set_pending();
					$count ++;
				}
			}
		}

		$this->message = $this->name . sprintf( __('successfully marked %d gifts as Pending', 'wpgroupbuy'), $count);
	}
}

WPG_Gifts_Upgrader::init();
