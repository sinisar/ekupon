<?php
class WPG_Accounts_Upgrader extends WPG_Upgrader {

	public function upgrade_to_2_3() {
		global $wpdb;

		$count = WP_Groupbuy_Accounts::remove_duplicated_accounts();

		if ( $count ){
			self::clean_postmeta();
		}

		$this->message = $this->name . sprintf( __('successfully deleted %d duplicated account records from DB', 'wpgroupbuy'), $count);
	}
	
}

WPG_Accounts_Upgrader::init();
