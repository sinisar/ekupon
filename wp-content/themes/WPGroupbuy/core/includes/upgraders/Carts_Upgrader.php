<?php
class WPG_Carts_Upgrader extends WPG_Upgrader {

	public function upgrade_to_2_3() {
		global $wpdb;

		$count = $wpdb->query("DELETE FROM $wpdb->posts WHERE post_type = 'wg_cart'");
		if ( $count ){
			$wpdb->query("DELETE FROM $wpdb->postmeta WHERE `post_id` NOT IN ( SELECT ID FROM $wpdb->posts );");
		}

		$this->message = $this->name . sprintf( __('successfully deleted %d cart records from DB', 'wpgroupbuy'), $count);
	}
	
}

WPG_Carts_Upgrader::init();
