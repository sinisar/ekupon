<?php get_header(); ?>
<!-- ACCOUNT -->

<div class="main">
  <div class="mainPadding"> 
    <!-- LEFT CONTENT -->
    <div class="leftMainBox">
      <div class="sell1 mTop10">
      <?php if ( is_user_logged_in() ): ?>
        <div class="profileCus">
          <?php get_template_part( 'wpg-framework/account/topnav' ); ?>
          <?php get_template_part( 'wpg-framework/account/nav' ); ?>
        </div>
        <?php endif; ?>
        
        <div class="main_viewprofile">
          <div class="">
            <div class="profileContainer">
              
                <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
                <?php the_content(); ?>
                <?php endwhile; // end of the loop. ?>
              
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END LEFT CONTENT --> 
    
    <!-- RIGHT SIDE -->
    
    <div class="rightMainBox">
      <div>
        <div class="mBottom15 mTop10"> 
          <?php get_template_part( 'wpg-framework/inc/account-sidebar' ); ?>
          <?php dynamic_sidebar( 'account-sidebar' ); ?>
          
        </div>
      </div>
      <div></div>
    </div>
    <!-- END RIGHT SIDE -->
    <div class="c"></div>
  </div>
</div>
<!-- END ACCOUNT -->

<?php get_footer(); ?>
