<?php get_header();?>

<!-- ORDER LOOKUP -->
<div class="main">
  <div class="mainPadding"> 
    <!-- LEFT CONTENT -->
    <div class="leftMainBox">
      <div class="sell1 mTop10">
        <div class="profileCus">
          <div class="profileHeaderTop">
            <div class="fl profileHeaderTitle">
              <?php the_title() ?>
            </div>
            <div class="c"></div>
          </div>
        </div>
        
        <div class="main_viewprofile">
          <div class="main-boxcontFull m0">
            <div class="profileContainer">
              <div id="wpg_checkout" class="pTop10">
                <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				  <?php the_content(); ?>
                  <?php endwhile; ?>
                  <div class="c"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END LEFT CONTENT --> 
    
    <!-- RIGHT SIDE -->
    <div class="rightMainBox">
      <div>
        <div class="mBottom15 mTop10"> 
          <?php dynamic_sidebar( 'page-sidebar' ); ?>
          
        </div>
      </div>
      <div></div>
    </div>
    <!-- END RIGHT SIDE -->
    <div class="c"></div>
  </div>
</div>
<!-- END ORDER LOOKUP -->

<?php get_footer(); ?>
