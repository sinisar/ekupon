<?php

// Get the featured content for the deal
if ( !function_exists( 'wg_get_featured_content' ) ) {
	function wg_get_featured_content( $deal_id = null ) {
		if ( null === $deal_id ) {
			global $post;
			$deal_id = $post->ID;
		}
		$content = WP_Groupbuy_Featured_Content::get_featured_content( $deal_id );
		return apply_filters( 'wg_get_featured_content', do_shortcode( $content ) );
	}
}

// Print it
if ( !function_exists( 'wg_featured_content' ) ) {
	function wg_featured_content( $deal_id = null ) {
		echo apply_filters( 'wg_featured_content', wg_get_featured_content( $deal_id ) );
	}
}

// Featured content available or not
if ( !function_exists( 'wg_has_featured_content' ) ) {
	function wg_has_featured_content( $deal_id = null ) {
		$content = wg_get_featured_content( $deal_id );
		$has = ( $content == '' ) ? FALSE : TRUE ;
		return apply_filters( 'wg_has_featured_content', $has );

	}
}
