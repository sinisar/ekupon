<?php do_action( 'wg_meta_box_deal_theme_meta_pre' ) ?>
<p>
	<p><label for="featured_content"><strong><?php self::_e( 'Featured Content' ) ?></strong> <img width="16" height="16" src="<?php echo (WG_RESOURCES . 'images/help.png')?>" class="help_tip" title="<?php wpg_e( 'Replace deal thumbnail area with this featured content. You can use HTML code or you can install image slideshow plugin and insert shortcodes here. Remember to set size image 475x475 px' ); ?>"></label></p>
	<textarea rows="3" cols="40" name="featured_content" style="width:98%"><?php echo $featured_content; ?></textarea>
</p>
<?php do_action( 'wg_meta_box_deal_theme_meta' ) ?>
