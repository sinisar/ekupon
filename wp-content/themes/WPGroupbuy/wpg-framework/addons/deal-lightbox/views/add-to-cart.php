<div class="v6BuyNow">
	<?php if ( wg_is_sold_out() ) : ?>
		<a href="#"><div class="retinaiconbox soldout_btn"><span class="retinaicon-shopping-cart"></span> <span class="buynow_text"><?php wpg_e('Sold out') ?></span></div></a>
	<?php elseif ( wg_deal_availability() && !wg_is_deal_complete() ): ?>
		<a href="<?php wg_add_to_cart_url() ?>"><div class="retinaiconbox buynow_btn"><span class="retinaicon-shopping-cart"></span> <span class="buynow_text"><?php wpg_e('Buy now') ?></span></div></a>
	<?php else : ?>
		<a href="#"><div class="retinaiconbox soldout_btn"><span class="retinaicon-shopping-cart"></span> <span class="buynow_text"><?php wpg_e('Unavailable') ?></span></div></a>
	<?php endif ?>
</div>