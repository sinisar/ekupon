<div id="lightbox_attributes_wrap">
	<div id="wg_deal_light_box" class="shadow top bottom cloak">
		<form action="<?php echo wg_get_cart_url() ?>" id="wg_deal_options" method="get" class="clearfix">
			<div id="wg_lb_options" class="col-md-8">
				<header>
					<?php _e( 'Choose your deal', 'wpgroupbuy' ) ?>: 
				</header>
				<div class="wg_lb_content">
			<?php 
			if ( function_exists( 'wg_deal_has_attributes' ) && wg_deal_has_attributes() ) : ?>
				<?php global $post;
				$deal_id =  $post->ID;
				$attributes = WP_Groupbuy_Attribute::get_attributes( $deal_id, 'object' );

				$count = 0;
				?>

				<input type="hidden" name="add_to_cart" value="<?php echo $deal_id ?>" />
				<?php 
				foreach ( $attributes as $attribute ) :
					$title = $attribute->get_title();

					$price = wg_get_formatted_money( $attribute->the_price() );
					if ( $attribute->get_max_purchases() == WP_Groupbuy_Attribute::NO_MAXIMUM ) {
						$remaining = wpg__( 'No max limit' );
					} else {
						$remain_purchases = ( int ) $attribute->remaining_purchases();
						$remaining = $remain_purchases > 0 ? $remain_purchases . ' ' . wpg__( 'remaining' ) : wpg__('Sold out');
					}

					$deal_url = add_query_arg( array( 'attribute_id' => $attribute->get_id() ), WP_Groupbuy_Carts::add_to_cart_url( $deal_id ) );

					$disabled = '';
					if ( $attribute->get_max_purchases() == WP_Groupbuy_Attribute::NO_MAXIMUM || $attribute->remaining_purchases() > 0 ) {
						$count++;
					} else {
						$disabled = 'disabled';
					}

					$full_desc = $attribute->get_description();
					$short_desc = wp_trim_words( $full_desc, 20 );

					$checked = ( $count == 1 && !$disabled ) ? 'checked' : ''; ?>

					<label class="deal_item <?php echo $disabled ?>">
						<div class="row">
							<div class="col-xs-9">
								<div class="deal_title"><input class="option_id" type="radio" name="attribute_id" value="<?php echo $attribute->get_id()?>" <?php echo $disabled ?> <?php echo $checked ?> /> <?php echo $title ?></div>
								<div class="deal_short_desc hidden-xs"><?php echo $short_desc ?></div>
								<div class="deal_full_desc hidden"><?php echo $full_desc ?></div>
							</div>
							<div class="col-md-3">
								<div class="deal_price"><?php echo $price ?></div>
								<div class="deal_limit hidden-xs"><?php echo $remaining ?></div>
							</div>
						</div>
					</label>
				<?php endforeach; ?>
			<?php endif; ?>
				</div>
			</div>
			<div id="wg_lb_selected" class="col-md-4">
				<h4><?php wpg_e( 'Your price:' ); ?></h4>
				<div class="wg_lb_content">
					<div class="row">
						<div class="deal_price col-xs-6"></div>
						<div class="col-xs-6 hidden-md hidden-lg"><button type="submit" class="button v6BuyNow"><div class="retinaiconbox buynow_btn"><span class="retinaicon-shopping-cart"></span> <span class="buynow_text"><?php wpg_e( 'Buy now' ); ?></span></div></button>
						</div>
					</div>
					<div class="deal_full_desc"></div>
					<div class="deal_fields row">
						<div class="value col-xs-4"></div>
						<div class="discount col-xs-4"></div>
						<div class="save col-xs-4"></div>
					</div>
				</div>
				<footer>
					<div class="countdown">
						<?php wg_deal_countdown(true); ?>
						<noscript>
						<?php wg_get_deal_end_date(); ?>
						</noscript>
					</div>
					<button type="submit" class="button v6BuyNow hidden-sm hidden-xs"><div class="retinaiconbox buynow_btn"><span class="retinaicon-shopping-cart"></span> <span class="buynow_text"><?php wpg_e( 'Buy now' ); ?></span></div></button>
				</footer>
			</div>
		</form>
	</div>
</div>