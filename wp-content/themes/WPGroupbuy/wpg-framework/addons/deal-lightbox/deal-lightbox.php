<?php
// NOTE: Lightbox settings will use WP_Groupbuy_Subscription_Lightbox if present -if not, it will show Lightbox settings.

define ( 'ATTR_LIGHTBOX_URL', get_template_directory_uri() . '/wpg-framework/addons/deal-lightbox/' );

add_filter( 'wg_addons', 'wg_deal_lighbox_addon', 10, 1 );

function wg_deal_lighbox_addon( $addons ) {
	$addons['deal_lighbox'] = array( 'label' => __( 'Deal Lightbox', 'wpgroubuy' ),
		'description' => __( 'Adds Attributes Lightbox to display all options/variations when users are purchasing a Deal with attributes.', 'wpgroubuy' ),
		'default' => true,
		'files' => array( __FILE__, ),
		'callbacks' => array( array( 'WPG_Deal_Lightbox', 'init' ), ), );
	return $addons;
}

class WPG_Deal_Lightbox extends WP_Groupbuy_Controller {
	
	const DEBUG = WPG_DEV; //WPG_DEV;
	private static $opacity;
	private static $color;
	private static $id_onclick;
	
	public static function init() {
		
		self::$color = get_option( 'deal_lightbox_color', '000000' );
		self::$opacity = get_option( 'deal_lightbox_opacity', 60 );
		self::$id_onclick = get_option( 'deal_lightbox_id_onclick', 'form.add-to-cart input[type="submit"]' );
		
		// Options
		add_action( 'admin_init', array( get_class(), 'register_settings_fields' ), 10, 0 );
		
		add_action( 'wp_enqueue_scripts', array( get_class(), 'deal_scripts_enqueue' ) );
		add_filter( 'wp_groupbuy_template_add-to-cart.php', array( get_class(), 'filter_template_add_to_cart' ) ) ;
		add_filter( 'wp_footer', array( get_class(), 'deal_lightbox_footer_script' ) );
	
		remove_filter( 'wg_add_to_cart_form_fields', array( 'WP_Groupbuy_Attributes', 'filter_add_to_cart_add_category_selection' ), 10, 2 );
		remove_filter( 'wg_add_to_cart_form_fields', array( 'WP_Groupbuy_Attributes', 'filter_add_to_cart_form_fields' ), 10, 2 );
		
		
	}
	
	public static function deal_scripts_enqueue() {
		wp_enqueue_style( 'deal-lightbox', ATTR_LIGHTBOX_URL . 'assets/deal-lightbox.css' );
		wp_enqueue_script( 'jquery-sf-attr-lightbox',ATTR_LIGHTBOX_URL.'assets/lightbox.jquery.js',array( 'jquery' ), '1', true );
	}
	
	public static function show_deal_lightbox( $post_id = null ) {
		if ( !$post_id ) {
			global $post;
			$post_id = $post->ID;
		}
		$deal = WP_Groupbuy_Deal::get_instance( $post_id );
		if ( !is_a( $deal, 'WP_Groupbuy_Deal' ) ) {
			return FALSE;
		}
		//Show?
		if ( class_exists( 'WP_Groupbuy_Attribute' ) ) {
			$attributes = WP_Groupbuy_Attribute::get_attributes( $post_id, 'id' );
			if ( !empty( $attributes ) ) {
				//	if ( self::DEBUG ) error_log( "deal_lighbox: show_deal_lightbox = TRUE" ); 
				return TRUE;
			}
		}
		//	if ( self::DEBUG ) error_log( "deal_lighbox: show_deal_lightbox = FALSE" ); 
		return FALSE;
	}
	
	public static function deal_lightbox_footer_script() {
		if ( !self::show_deal_lightbox() ) return;
		$post_id = get_the_ID();
		//Get selector for trigger
		$selector = ( self::$id_onclick ) ? html_entity_decode( self::$id_onclick ) : 'form.add-to-cart input[type="submit"]';
		//Get color
		$lb_color = ( stripos( self::$color, '#' ) === false ) ? '#'.self::$color : self::$color;
		include( 'views/deal-lightbox.php' ); ?>

		<script type="text/javascript" charset="utf-8">
			jQuery(document).ready(function($) {
				$selector = $('<?php echo $selector ?>');

				//Trigger with custom selector
				$selector.click( function ( e ) {
					e.preventDefault();  
					sfTriggerAttrLb();
				});

				//Also, trigger with Buy Now button
				jQuery('.v6BuyNow a, .purchases a').click(function (e) {
					e.preventDefault();  
					sfTriggerAttrLb();
					
				});

				$(document).on('click', '#simplemodal-overlay', function () {
					$.modal.close();
				});
				
				$('.deal_item .option_id').change(function() {
					showDealDetails();
				});

				showDealDetails();

				function showDealDetails() {
					var $option_id = $('.deal_item .option_id:checked');
					var $item = $option_id.parents('.deal_item');
					var price = $item.find('.deal_price').text();
					$('#wg_lb_selected .deal_price').text(price);
					$('#wg_lb_selected .deal_full_desc').text($item.find('.deal_full_desc').text());
				}

				function countdown_<?php echo $post_id ?>(){
					var date = new Date ('<?php echo wg_get_deal_end_date( "F j\, Y H:i:s", $post_id ); ?>' );
					$timer = jQuery(document).find('.countdown-timer-<?php echo $post_id ?>');
					$timer.countdown('destroy').countdown( {
						until: date, 
						timezone: '<?php echo get_option( 'gmt_offset' ); ?>',
						compact: true, 
						format: 'DHMS', 
						description: ''
					})
				}

				function sfTriggerAttrLb() {
					jQuery("#lightbox_attributes_wrap").modal( {
						opacity:<?php echo self::$opacity ?>,
						focus: false,
						overlayCss: {backgroundColor:"<?php echo $lb_color; ?>"},
						open: countdown_<?php echo $post_id ?>()
					});
				}

			});
		</script>
		<?php
	}
	
		
	//Display settings for Lightbox color/opacity settings if needed
	public static function register_settings_fields() {
		$page = WP_Groupbuy_UI::get_settings_page();
		$section = 'deal_lightbox_settings';
		add_settings_section( $section, self::__( 'Attribute Lightbox' ), array( get_class(), 'display_settings_section' ), $page );
		// Settings
		register_setting( $page, 'deal_lightbox_color' );
		register_setting( $page, 'deal_lightbox_opacity' );
		// Fields
		add_settings_field( 'deal_lightbox_color', self::__( 'Lightbox Background Color' ), array( get_class(), 'display_color' ), $page, $section );
		add_settings_field( 'deal_lightbox_opacity', self::__( 'Lightbox Background Opacity' ), array( get_class(), 'display_opacity' ), $page, $section );
		
		//Attribute lightbox settings
		register_setting( $page, 'deal_lightbox_id_onclick' );
		register_setting( $page, 'deal_lightbox_disable_wpg_fields' );
		add_settings_field( 'deal_lightbox_id_onclick', self::__( 'Custom ID/Class Selector to Trigger Lightbox' ), array( get_class(), 'display_id_onclick' ), $page, $section );
		
	}

	public static function display_opacity() {
		echo '<input name="'.'deal_lightbox_opacity'.'" id="'.'deal_lightbox_opacity'.'" type="text" maxlength="100" value="'.self::$opacity.'" style="width: 3em;">%';
	}

	public static function display_color() {
		echo '#<input name="'.'deal_lightbox_color'.'" id="'.'deal_lightbox_color'.'" type="text" maxlength="6" class="color_picker" value="'.self::$color.'" style="width: 5em;">';
	}
	
	
	public static function display_id_onclick() {
		echo '<input name="'.'deal_lightbox_id_onclick'.'" id="'.'deal_lightbox_id_onclick'.'" type="text" maxlength="255" class="text" value="'.htmlentities( self::$id_onclick, ENT_QUOTES ).'" style="width: 20em;">
		<br><span><em>'.self::__( 'Set the custom ID or Class selector that will be used to trigger/display the Attribute Lightbox. Use this if the Buy Button has been customized. The default value is: form.add-to-cart input[type="submit"]' ).'</em></span>';
	}

	public static function filter_template_add_to_cart( $file ) {
		return locate_template( '/wpg-framework/addons/deal-lightbox/views/add-to-cart.php' );
	}

}