<?php

if ( class_exists( 'WP_Groupbuy_Theme_UI' ) && !class_exists( 'WP_Groupbuy_Advanced_Thumbs' ) ) {

	include 'template-tags.php';

	class WP_Groupbuy_Advanced_Thumbs extends WP_Groupbuy_Theme_UI {
		const ACTIVATED = 'wg_adv_thumbs';
		const QUALITY = 'wg_adv_thumbs_quality';
		const ZC = 'wg_adv_thumbs_sc';
		const ALIGN = 'wg_adv_thumbs_align';
		const SHARPEN = 'wg_adv_thumbs_sharpen';
		const COLOR = 'wg_adv_thumbs_cc';
		private static $instance;
		protected static $theme_settings_page;
		private static $version = '1.1';
		public static $documentation = 'http://wpgroupbuy.com/support/documents/';
		private static $active;
		private static $quality;
		private static $align;
		private static $zc;
		private static $sharpen;
		private static $cc;

		private function __construct() {
			self::$active = get_option( self::ACTIVATED, '0' );
			self::$quality = get_option( self::QUALITY, '89' );
			self::$align = get_option( self::ALIGN, '0' );
			self::$sharpen = get_option( self::SHARPEN, '0' );
			self::$zc = get_option( self::ZC, '2' );
			self::$cc = get_option( self::COLOR, '#FFFFFF' );
			add_action( 'admin_init', array( get_class(), 'register_settings_fields' ), 10, 0 );

			if ( self::$active == '1' ) {
				// reset the image_sizes
				add_image_size( 'wpg_deal_thumbnail', 656, 399, false );
				add_image_size( 'wpg_loop_thumb', 208, 120, true );
				add_image_size( 'wpg_widget_thumb', 60, 60, true );
				add_image_size( 'wpg_merchant_loop', 150, null, true );
				add_image_size( 'wpg_merchant_thumb', 255, null, false );
				add_image_size( 'wpg_voucher_thumb', 255, 220, false );
				add_image_size( 'merchant_post_thumb', 160, 100, true );
				add_image_size( 'wpg_700x400', 700, 400, false );
				add_image_size( 'wpg_300x180', 300, 180, true );
				add_image_size( 'wpg_250x110', 250, 110, true );
				add_image_size( 'wpg_150w', 150, null, true );
				add_image_size( 'wpg_100x100', 100, 100, false );
				add_image_size( 'wpg_200x150', 200, 150, false );
				add_image_size( 'wpg_160x100', 160, 100, true );
				// Do the work
				add_filter( 'image_downsize', array( get_class(), 'filter_image_downsize' ), 10, 3 );
			}
		}

		public static function init() {
			self::get_instance();
			if ( version_compare( get_bloginfo( 'version' ), '3.3', '>=' ) ) { // 3.3. only
				add_action( 'load-wp-groupbuy_page_wp-groupbuy/theme_options', 45 );
			}
		}

		public function filter_image_downsize( $bool, $id, $size ) {
			global $_wp_additional_image_sizes;

			if ( is_array( $_wp_additional_image_sizes ) && ( is_array( $size ) || array_key_exists( $size, $_wp_additional_image_sizes ) ) ) {

				$src = wp_get_attachment_image_src( $id, 'full' );
				$src = $src[0];

				if ( is_array( $size ) ) {
					$w = $size[0];
					$h = $size[1];
				} else {
					$w = $_wp_additional_image_sizes[$size]['width'];
					$h = $_wp_additional_image_sizes[$size]['height'];
				}

				$url = add_query_arg(
					array(
						'src' => $src,
						'w' => $w,
						'h' => $h,
						'zc' => self::$zc,
						's' => self::$sharpen,
						'a' => self::$align,
						'q' => self::$quality,
						'cc' => str_replace( '#', '', self::$cc),
					),
					get_bloginfo( 'template_url' ) . '/wpg-framework/addons/timthumb/timthumb.php' );

				return array( $url, $w, $h, false );
			}
		}

		public static function register_settings_fields() {
			$page = parent::$theme_settings_page;
			$section = 'wg_theme_thumbs';
			add_settings_section( $section, self::__( 'Thumbnails' ), array( get_class(), 'display_section' ), $page );
			register_setting( $page, self::ACTIVATED );
			register_setting( $page, self::QUALITY );
			register_setting( $page, self::ZC );
			register_setting( $page, self::ALIGN );
			register_setting( $page, self::SHARPEN );
			register_setting( $page, self::COLOR );
			add_settings_field( self::ACTIVATED, self::__( 'Use TimThumb resizing' ), array( get_class(), 'display_option' ), $page, $section );
			add_settings_field( self::QUALITY, self::__( 'Resize Quality' ), array( get_class(), 'display_option_quality' ), $page, $section );
			add_settings_field( self::ZC, self::__( 'Zoom Crop' ), array( get_class(), 'display_option_zc' ), $page, $section );
			add_settings_field( self::ALIGN, self::__( 'Crop Alignment' ), array( get_class(), 'display_option_align' ), $page, $section );
			add_settings_field( self::SHARPEN, self::__( 'Sharpen' ), array( get_class(), 'display_option_sharpen' ), $page, $section );
			add_settings_field( self::COLOR, self::__( 'Crop Background Color' ), array( get_class(), 'display_option_background_color' ), $page, $section );
		}

		public static function display_option() {
			echo '<label name="'.self::ACTIVATED.'"><input type="checkbox" value="1" name="'.self::ACTIVATED.'" '.checked( '1', self::$active, false ).' /> '.self::__( 'Enable' ).'</label>';
			echo '<img width="16" height="16" src="'.WG_URL . '/resources/images/help.png'. '" class="help_tip" data-tip="'.self::__( 'Enabling this will allow your thumbnails to be cropped and resized by Timbthumb' ).'">';
		}

		public static function display_option_quality() {
			echo '<input type="text" maxlength="3" class="small-text" name="'.self::QUALITY.'" value="'.self::$quality.'" />';
			echo '<img width="16" height="16" src="'.WG_URL . '/resources/images/help.png'. '" class="help_tip" data-tip="'.self::__( 'The allowed values are between 0 and 100' ).'">';
		}

		public static function display_option_zc() {
			echo '<input type="text" maxlength="2" class="small-text" name="'.self::ZC.'" value="'.self::$zc.'" />';
			echo '<img width="16" height="16" src="'.WG_URL . '/resources/images/help.png'. '" class="help_tip" data-tip="'.self::__( 'Use below value:<br/>0: Resize to Fit specified dimensions (no cropping)<br/>1: Crop and resize to best fit the dimensions (default behaviour)<br/>2: Resize proportionally to fit entire image into specified dimensions, and add borders if required.<br/>3: Resize proportionally adjusting size of scaled image so there are no borders gaps.' ).'">';
			echo '<br/><span class="description">'.self::__( 'For more information, <a href="http://www.binarymoon.co.uk/demo/timthumb-zoom-crop/" target="_blank">click here</a>.').'</span>';
		}

		public static function display_option_align() {
			echo '<input type="text" maxlength="2" class="small-text" name="'.self::ALIGN.'" value="'.self::$align.'" />';
			echo '<img width="16" height="16" src="'.WG_URL . '/resources/images/help.png'. '" class="help_tip" data-tip="'.self::__( 'Use below value:<br/>Leaving blank or using a \'0\' will force the alignment to the center<br/><strong>c</strong> : position in the center (this is the default)<br/><strong>t</strong> : align top<br/><strong>tr</strong> : align top right<br/><strong>tl</strong> : align top left<br/><strong>b</strong> : align bottom<br/><strong>br</strong> : align bottom right<br/><strong>bl</strong> : align bottom left<br/><strong>l</strong> : align left</br><strong>r</strong> : align right.' ).'">';
		}

		public static function display_option_sharpen() {
			echo '<input type="text" maxlength="1" class="small-text" name="'.self::SHARPEN.'" value="'.self::$sharpen.'" />';
			echo '<img width="16" height="16" src="'.WG_URL . '/resources/images/help.png'. '" class="help_tip" data-tip="'.self::__( 'Use below value:<br/>0: The image no sharpening<br/>1: to sharpen the photo' ).'">';
		}

		public static function display_option_background_color() {
			echo '<input id="background_color" type="text" maxlength="6" class="color_picker" name="'.self::COLOR.'" value="'.self::$cc.'" style="width:5em;"/>';
			echo '<img width="16" height="16" src="'.WG_URL . '/resources/images/help.png'. '" class="help_tip" data-tip="'.self::__( 'Change background color. Most used when changing the zoom and crop settings, which in turn can add borders to the image. Use hexadecimal colour value, ex: #FFFFFF' ).'">';
		}

		private function __clone() {
			trigger_error( __CLASS__.' may not be cloned', E_USER_ERROR );
		}
		private function __sleep() {
			trigger_error( __CLASS__.' may not be serialized', E_USER_ERROR );
		}
		public static function get_instance() {
			if ( !( self::$instance && is_a( self::$instance, __CLASS__ ) ) ) {
				self::$instance = new self();
			}
			return self::$instance;
		}

	}
}
add_action( 'init', array( 'WP_Groupbuy_Advanced_Thumbs', 'init' )  );