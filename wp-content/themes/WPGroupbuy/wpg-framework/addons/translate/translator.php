<?php
if ( class_exists( 'WP_Groupbuy_Controller' ) ) {

	include 'template-tags.php';

	class WP_Groupbuy_Translator extends WP_Groupbuy_Controller {
		const TRANSLATION_OPTION = 'wg_translations';
		private static $translations = array();
		protected static $settings_page;
		private static $instance;

		public static function init() {
			self::$settings_page = self::register_settings_page( 'translation', self::__( 'Quick Translator' ), self::__( 'Quick Translator' ), 11000, 'theme', 'addons' );
			add_action( 'admin_init', array( get_class(), 'register_settings_fields' ), 10, 0 );
			self::$translations = get_option( self::TRANSLATION_OPTION );
			self::translate_strings();

			if ( version_compare( get_bloginfo( 'version' ), '3.2.99', '>=' ) ) { // 3.3. only
				add_action( 'load-wp-groupbuy_page_wp-groupbuy/translation', array( get_class(), 'options_help_section' ), 45 );
			}
		}

		public static function options_help_section() {
			
		}

		public static function translate_strings() {
			if ( !empty( self::$translations ) ) {
				foreach ( self::$translations as $string => $translation ) {
					add_filter( 'wg_string_'.sanitize_title( $string ), create_function( '', 'return "'.$translation.'";' ) );
				}
			}
		}

		/**
		 *
		 *
		 * @static
		 * @return string The ID of the subscription settings page
		 */
		public static function get_settings_page() {
			return self::$settings_page;
		}

		/*
		 * Singleton Design Pattern
		 * ------------------------------------------------------------- */
		private function __clone() {
			trigger_error( __CLASS__.' may not be cloned', E_USER_ERROR );
		}
		private function __sleep() {
			trigger_error( __CLASS__.' may not be serialized', E_USER_ERROR );
		}
		public static function get_instance() {
			if ( !( self::$instance && is_a( self::$instance, __CLASS__ ) ) ) {
				self::$instance = new self();
			}
			return self::$instance;
		}

		private function __construct() {
		}

		public static function register_settings_fields() {
			register_setting( self::$settings_page, self::TRANSLATION_OPTION );
			add_settings_section( self::TRANSLATION_OPTION, '', array( get_class(), 'display_translator_selection' ), self::$settings_page );
		}

		public static function display_translator_selection() {
?>
			<style type="text/css" media="screen">
				#translations th {text-align: left !important;}
				#translations tr td {padding: 5px 15px 5px 10px;}
				#translations input {width: 220px;}
				#translator tr:hover {background-color: #f3f3f3;}
			</style>
			<script type="text/javascript">
				jQuery(document).ready(function($){
					// Initialize a counter var
					inputCount = 0;
					// Now let's say you click the "add text box" button
					jQuery('#add_translation').click(function(){
						var original = jQuery('#translator_original_text').val();
						var translation = jQuery('#translator_translation_text').val();
						var row = '<tr id="dyn_trans_'+inputCount+'"><td>'+original+'</td><td><input id="dyn_trans_' + inputCount + '" type="text" name="wg_translations['+original+']" value="'+translation+'"></td><td><a id="delete_dyn_trans_'+inputCount+'" class="delete-dyn_trans button hide-if-no-js" onclick="jQuery(this).parent().parent().remove();">Delete</a></td></tr>';
						inputCount++;
						if ( original.length != 0 & translation.length != 0 ) {
							jQuery(' '+row+' ').appendTo('#translator');
						};
					});
					jQuery('.delete-dyn_trans').live('click', function(){
						jQuery(this).parent().parent().remove();
					});
				});
			</script>
			<table id="translations">
				<thead>
					<tr>
						<th><h3><?php self::_e( 'Original Text:' ); ?></h3></th>
						<th><h3><?php self::_e( 'Translated Text:' ); ?></h3></th>
					</tr>
				</thead>

				<tbody id="translator">
					<tr>
						<td>
							<input type="text" name="translator_original_text" id="translator_original_text" />
						</td>
						<td>
							<input type="text" name="translator_translation_text" id="translator_translation_text" class="tiny-text" />
						</td>
						<td>
							<a id="add_translation" class="add-dyn-cost button hide-if-no-js"><?php self::_e( 'Add' ); ?></a>
						</td>
					</tr>
					<?php
			if ( !empty( self::$translations ) ) {
				foreach ( self::$translations as $string => $translation ) {
?>
									<tr>
										<td>
											<?php echo stripslashes( $string ); ?>
										</td>
										<td>
											<input type="text" name="<?php echo self::TRANSLATION_OPTION; ?>[<?php echo stripslashes( $string ); ?>]" value="<?php echo stripslashes( $translation ); ?>">
										</td>
										<td>
											<a id="delete_dyn_trans" class="delete-dyn_trans button hide-if-no-js">Delete</a>
										</td>
									</tr>
								<?php
				}
			}
?>
				</tbody>
			</table>
		<?php
		}
	}
}
add_action( 'init', array( 'WP_Groupbuy_Translator', 'init' )  );
