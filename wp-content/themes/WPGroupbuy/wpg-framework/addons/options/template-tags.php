<?php
// Template based

//  Get header logo
if ( !function_exists( 'wg_get_header_logo' ) ) {
	function wg_get_header_logo( $location = null, $term_logo_url = null ) {
		global $wp_query;
		$postID = $wp_query->post->ID;
		$current_location = ( wg_get_current_location_extended() != '' ) ? wg_get_current_location_extended() : FALSE ;

		if ( FALSE != $current_location ) {
			$terms = get_the_terms( $postID, wg_get_deal_location_tax() );
			if ( $terms ) {
				foreach ( $terms as $term ) {
					if ( $current_location == $term->name ) {
						$term_logo_url = get_metadata( 'location_terms', $term->term_id, 'logo_image_url', TRUE );
						break;
					}
				}
			}
		}
		if ( isset( $term_logo_url ) && $term_logo_url != '' ) {
			$img_url = $term_logo_url;
		} elseif ( wg_get_theme_header_logo() != '' ) {
			$img_url = wg_get_theme_header_logo();
		} else {
			$img_url = get_bloginfo( 'template_directory' ) . "/style/images/logo.png";
			$system_img_url = str_replace( get_site_url(), $_SERVER['DOCUMENT_ROOT'], $img_url );
			if ( !file_exists( $system_img_url )) {
				$img_url = get_bloginfo( 'template_directory' ) . "/style/images/logo.png";	
			}
		}
		return apply_filters( 'wg_get_header_logo', $img_url, wg_get_theme_header_logo(), $term_logo_url, $location );
	}
}

// Print header location URL
if ( !function_exists( 'wg_header_logo' ) ) {
	function wg_header_logo( $location = null ) {
		echo apply_filters( 'wg_header_logo', wg_get_header_logo( $location ), $location );
	}
}

// Return the theme header logo option
if ( !function_exists( 'wg_get_theme_header_logo' ) ) {
	function wg_get_theme_header_logo() {
		return apply_filters( 'wg_get_theme_header_logo', get_option( WP_Groupbuy_Theme_UI::HEADER_LOGO_OPTION ) );
	}
}

// Echo the theme header logo option
if ( !function_exists( 'wg_theme_header_logo' ) ) {
	function wg_theme_header_logo() {
		echo apply_filters( 'wg_theme_header_logo', wg_get_theme_header_logo() );
	}
}

// Return the theme featured category option
if ( !function_exists( 'wg_get_featured_cat' ) ) {
	function wg_get_featured_cat() {
		return apply_filters( 'wg_get_featured_cat', get_option( WP_Groupbuy_Theme_UI::FEATURED_ID ) );
	}
}

// Echo the theme featured category option
if ( !function_exists( 'wg_featured_cat' ) ) {
	function wg_featured_cat() {
		echo apply_filters( 'wg_featured_cat', wg_get_featured_cat() );
	}
}

// Returns the user avatar. Filtered by the facebook integration and uses gravatar as a fallback
if ( !function_exists( 'wg_gravatar' ) ) {
	function wg_gravatar( $size = 18, $user_id = 0, $default = null ) {
		if ( !$user_id ) {
			$user_id = get_current_user_id();
		}
		echo apply_filters( 'wg_gravatar', get_avatar( $user_id, $size, $default ), $user_id, $size, $default );

	}
}

// Theme scripts and stylesheet options

// Return force login option
if ( !function_exists( 'wg_force_login_option' ) ) {
	function wg_force_login_option() {
		return get_option( WP_Groupbuy_Theme_UI::FORCE_LOGIN, 'false' );
	}
}

// Return flavor option
if ( !function_exists( 'wg_get_theme_flavor' ) ) {
	function wg_get_theme_flavor() {
		return apply_filters( 'wg_get_theme_flavor', get_option( WP_Groupbuy_Theme_UI::FLAVOR_OPTION_OPTION ) );
	}
}

// Echo the flavor option
if ( !function_exists( 'wg_theme_flavor' ) ) {
	function wg_theme_flavor() {
		echo apply_filters( 'wg_theme_flavor', wg_get_theme_flavor() );
	}
}

// Return custom css option
if ( !function_exists( 'wg_get_theme_custom_css' ) ) {
	function wg_get_theme_custom_css() {
		return apply_filters( 'wg_get_theme_custom_css', get_option( WP_Groupbuy_Theme_UI::CUSTOM_CSS_OPTION ) );
	}
}

// Echo the custom CSS option
if ( !function_exists( 'wg_theme_custom_css' ) ) {
	function wg_theme_custom_css() {
		echo apply_filters( 'wg_theme_custom_css', wg_get_theme_custom_css() );
	}
}

// Footer scripts option
if ( !function_exists( 'wg_get_theme_footer_scripts' ) ) {
	function wg_get_theme_footer_scripts() {
		return apply_filters( 'wg_get_theme_footer_scripts', get_option( WP_Groupbuy_Theme_UI::FOOTER_SCRIPT_OPTION ) );
	}
}

// Prints footer script options
if ( !function_exists( 'wg_theme_footer_scripts' ) ) {
	function wg_theme_footer_scripts() {
		echo apply_filters( 'wg_theme_footer_scripts', wg_get_theme_footer_scripts() );
	}
}

// Content Related

// Function to print the current deals viewed title
if ( !function_exists( 'wg_deals_index_title' ) ) {
	function wg_deals_index_title() {
		if ( is_tax() ) {
			$location = get_query_var( wg_get_deal_location_tax() );
			$category = get_query_var( 'wg_category' );
			$tags = get_query_var( 'wg_tag' );
			if ( !empty( $location ) ) {
				$term = get_term_by( 'slug', $location, wg_get_deal_location_tax() );
			} elseif ( !empty( $category ) ) {
				$term = get_term_by( 'slug', $category, wg_get_deal_cat_slug() );
			} elseif ( !empty( $tags ) ) {
				$term = get_term_by( 'slug', $tags, wg_get_deal_tag_slug() );
			}
			$title = wpg__( 'Deals for ' ) . $term->name;
		}
		if ( empty( $title ) ) {
			$title = wpg__( 'Current Deals' );
		}
		echo apply_filters( 'wg_deals_index_title', $title );
	}
}

// Links
// Returns the feed link for the current viewed location
if ( !function_exists( 'wpg_feed_link' ) ) {
	function wpg_feed_link( $context = '' ) {
		if ( empty( $context ) ) {
			$context = $_COOKIE[ 'wg_location_preference' ];
		}
		if ( !empty( $context ) && !is_home() && !is_front_page() ) {
			if ( wg_using_permalinks() ) {
				global $wp_rewrite;
				$rewrite_prestructure = $wp_rewrite->front;
				if ( !empty( $rewrite_prestructure ) ) {
					return site_url( $rewrite_prestructure . 'deals/' . $context . '/feed/' );
				}
				$feed_link = wg_get_deals_link() . '/feed/';
			} else {
				$feed_link = add_query_arg( array( 'deals' => $context, 'post_type' => wg_get_deal_post_type() ), get_bloginfo( 'rss2_url' ) );
			}
		} else {
			$feed_link = get_bloginfo( 'rss2_url' );
		}
		return apply_filters( 'wpg_feed_link', $feed_link, $context );
	}
}

// Return twitter option
if ( !function_exists( 'wg_get_theme_twitter' ) ) {
	function wg_get_theme_twitter() {
		return apply_filters( 'wg_get_theme_twitter', get_option( WP_Groupbuy_Theme_UI::TWITTER_OPTION ) );
	}
}

// print the twitter url option
if ( !function_exists( 'wg_theme_twitter' ) ) {
	function wg_theme_twitter() {
		echo apply_filters( 'wg_theme_twitter', wg_get_theme_twitter() );
	}
}

// Return facebook url option
if ( !function_exists( 'wg_get_theme_facebook' ) ) {
	function wg_get_theme_facebook() {
		return apply_filters( 'wg_get_theme_facebook', get_option( WP_Groupbuy_Theme_UI::TWITTER_OPTION ) );
	}
}

// Print the facebook options option
if ( !function_exists( 'wg_theme_facebook' ) ) {
	function wg_theme_facebook() {
		echo apply_filters( 'wg_theme_facebook', wg_get_theme_facebook() );
	}
}

// Locations

/**
 * Returns the current location for the menu
 */

if ( !function_exists( 'wg_get_current_location_extended' ) ) {
	function wg_get_current_location_extended( $return = 'name', $single = TRUE ) {
		static $current_location;

		$return = ( $return == 'id' ) ? 'term_id' : strtolower( $return ) ;

		$value = wpg__( 'Choose city' );

		if ( !isset( $current_location ) ) {

			if ( !empty( $_GET['location'] ) ) {
				$current_location = get_term_by( 'slug', $_GET['location'], wg_get_deal_location_tax() );
			} elseif ( is_tax('wg_location') ) {
				$current_location = get_queried_object();
			} elseif ( !empty($_COOKIE[ 'wg_location_preference' ]) ) {
				$current_location = get_term_by( 'slug', $_COOKIE[ 'wg_location_preference' ], wg_get_deal_location_tax() );
			} elseif ( is_single() && $single ) {
				$terms = get_the_terms( get_the_ID(), wg_get_location_tax_slug() );
				if ( $terms ) {
					foreach ( $terms as $term ) {
						if ( wg_get_current_location_extended( 'term_id', FALSE ) == $term->term_id ) {
							$current_location = $term;
							break;
						}
					}
				}
			}
		}

		if ( is_object($current_location) ) {
			$value = $current_location->$return;
		}
		return apply_filters( 'wg_get_current_location_extended', $value, $return );
	}
}

// Prints the current location for the menu
if ( !function_exists( 'wg_current_location_extended' ) ) {
	function wg_current_location_extended( $return = 'name' ) {

		$get_current_location = wg_get_current_location_extended( $return );

		$current_location = ( $get_current_location != '' ) ? $get_current_location : wpg__( 'Choose city' ) ;
		echo apply_filters( 'wg_current_location_extended', $current_location );
	}
}

// Used on the homepage to redirect the user to their selected location.
if ( !function_exists( 'location_redirect' ) ) {
	function location_redirect( $location_slug = null ) {
		if ( null === $location_slug && isset( $location_cookie ) ) {
			$location_slug = $_COOKIE[ 'wg_location_preference' ];
		}
		if ( isset( $location_slug ) ) {
			if ( wg_using_permalinks() ) {
				$redirect = site_url() . '/deals/' . $location_slug;
			} else {
				$redirect = add_query_arg( array( wg_get_deal_tag_slug() => $location_slug ), get_site_url() );
			}
			wp_redirect( apply_filters( 'location_redirection', $redirect, $location_slug ) );
			exit();
		}
	}
}