<?php
if ( class_exists( 'WP_Groupbuy_Controller' ) ) {

	include 'template-tags.php';

	class WP_Groupbuy_Theme_UI extends WP_Groupbuy_Controller {
		const CUSTOM_CSS_OPTION = 'wg_custom_css';
		const HEADER_LOGO_OPTION = 'wg_theme_header_logo';
		const FOOTER_SCRIPT_OPTION = 'wg_theme_footer_script';
		const CUSTOM_CSS_VAR = 'wg_custom_css';
		const TWITTER_OPTION = 'wg_twitter';
		const FACEBOOK_OPTION = 'wg_facebook';
		const NO_DEALS_CONTENT = 'wg_nodeal_content';
		const ENABLE_RTL = 'enable_rtl';
		const ENABLE_MERCHANT_EDIT = 'wg_enable_merchant_edit';
		const TOS_PAGE_ID = 'wg_tos_page';
		const PP_PAGE_ID = 'wg_pp_page';
		const FEATURED_ID = 'wg_fea_cat';
		const FORCE_LOGIN = 'wg_force_login';
		const FLAVOR_ARRAY = 'wg_flavor_array2';
		const CUSTOMIZER_OPTIONS_PREFIX = 'wg_customizer_options_';
		const CONVERT_THEME_CUSTOMIZATIONS = 'wg_has_converted_theme_options_for_customizer';
		const CUSTOMIZER_RESET_QUARY_ARG = 'wg_customizer_reset';
		private static $custom_css_path = 'flavor/css';
		private static $instance;
		protected static $theme_settings_page;
		protected static $theme_color_settings_page;
		protected static $flavor;
		protected static $custom_css;
		protected static $header_logo;
		protected static $footer_scripts;
		protected static $twitter;
		protected static $facebook;
		protected static $nodeal_pageid;
		protected static $tos_pageid;
		protected static $pp_pageid;
		protected static $featured_cat;
		protected static $force_login;
		protected static $customizer_options;
		protected static $deprecated_registered_colors;
		protected static $font_sizes = array(
			'0.1em' => '0.1em',
			'0.2em' => '0.2em',
			'0.4em' => '0.4em',
			'0.6em' => '0.6em',
			'0.8em' => '0.8em',
			'1em' => '1em',
			'1.1em' => '1.1em',
			'1.2em' => '1.2em',
			'1.3em' => '1.3em',
			'1.4em' => '1.4em',
			'1.5em' => '1.5em',
			'1.6em' => '1.6em',
			'1.7em' => '1.7em',
			'1.8em' => '1.8em',
			'1.9em' => '1.9em',
			'2em' => '2em',
			'2.5em' => '2.5em',
			'3em' => '3em',
			'3.5em' => '3.5em',
			'4em' => '4em',
			'5em' => '5em',
		);
		protected static $font_faces = array(
			'Arial' => array( 'font-family' => 'Arial, Helvetica, Geneva, sans-serif', 'font-name' => 'Arial' ),
			'Arial Black' => array( 'font-family' => '"Arial Black", Arial, Helvetica, Geneva, sans-serif', 'font-name' => 'Arial Black' ),
			'Impact' => array( 'font-family' => '"Impact", Arial, Helvetica, Geneva, sans-serif', 'font-name' => 'Impact' ),
			'Tahoma' => array( 'font-family' => 'Tahoma, Arial, sans-serif', 'font-name' => 'Tahoma' ),
			'Verdana' => array( 'font-family' => 'Verdana, Arial, sans-serif', 'font-name' => 'Verdana' ),
			'Georgia' => array( 'font-family' => 'Georgia, "Times New Roman", Times, serif', 'font-name' => 'Georgia' ),
			'Helvetica Neue' => array( 'font-family' => '"Helvetica Neue", Arial, Helvetica, Geneva, sans-serif', 'font-name' => 'Helvetica Neue' ),
			'Trebuchet MS' => array( 'font-family' => 'Trebuchet MS, "Times New Roman", Times, serif', 'font-name' => 'Trebuchet MS' ),
			'Garamond' => array( 'font-family' => 'Garamond, "Times New Roman", Times, serif', 'font-name' => 'Garamond' ),
			'Times New Roman' => array( 'font-family' => '"Times New Roman", Times, serif', 'font-name' => 'Times New Roman' ),
			'Lucida Grande' => array( 'font-family' => 'font-family: "Lucida Grande", Lucida, Verdana, sans-serif', 'font-name' => 'Lucida Grande' ),
			'Courier New' => array( 'font-family' => '"Courier New", Courier, mono', 'font-name' => 'Courier New' ),
			'web-fonts' => array( 'font-family' => 'Arial, Helvetica, Geneva, sans-serif', 'font-name' => '==Google Web Fonts==' ),
		);



		public static function init() {
			self::get_instance();

			self::$theme_settings_page = self::register_settings_page( 'theme_options', sprintf( self::__( '%s Options' ), WPG_THEME_NAME ), self::__( 'Theme Options' ), 9999, FALSE, 'theme' );
			add_action( 'admin_init', array( get_class(), 'register_settings_fields' ), 10, 0 );
			self::register_path_callback( self::$custom_css_path, array( get_class(), 'custom_css' ), self::CUSTOM_CSS_VAR );
			add_filter( 'wpg_no_ssl_redirect', array( get_class(), 'ssl_redirect' ), 10 );

			global $pagenow;
			if ( is_admin() && isset( $_GET['activated'] ) && $pagenow == 'themes.php' ) {
				self::create_location_table();
			}
			self::setup_location_table(); // This is persistent.

			self::$custom_css = get_option( self::CUSTOM_CSS_OPTION );
			self::$header_logo = get_option( self::HEADER_LOGO_OPTION );
			self::$footer_scripts = get_option( self::FOOTER_SCRIPT_OPTION );
			self::$twitter = get_option( self::TWITTER_OPTION );
			self::$facebook = get_option( self::FACEBOOK_OPTION );
			self::$nodeal_pageid = get_option( self::NO_DEALS_CONTENT, '0' );
			self::$tos_pageid = get_option( self::TOS_PAGE_ID, '0' );
            define('TOS_PAGEID',get_option( self::TOS_PAGE_ID, '0' ));
			self::$pp_pageid = get_option( self::PP_PAGE_ID, '0' );
            define('PP_PAGEID',get_option( self::PP_PAGE_ID, '0' ));
			self::$featured_cat = get_option( self::FEATURED_ID, '0' );
			self::$force_login = get_option( self::FORCE_LOGIN, 'false' );
			self::$deprecated_registered_colors = get_option( self::FLAVOR_ARRAY, array() );
			self::$customizer_options = get_option( self::CUSTOMIZER_OPTIONS_PREFIX . WPG_THEME_SLUG, self::get_registrations() );

			// Theme Customizer
			if ( version_compare( get_bloginfo( 'version' ), '3.4', '>=' ) ) {
				add_action( 'customize_register', array( get_class(), 'theme_customizer' ) );
				// Reset Customizer options
				add_action( 'admin_init', array( get_class(), 'reset_customizer_options' ) );
				add_action( 'wp_ajax_wpg_reset_customizer_options',  array( get_class(), 'reset_customizer_options' ), 10, 0 );
				add_action( 'admin_init', array( get_class(), 'wg_converted_theme_customizer' ) );
				add_action( 'admin_init', array( get_class(), 'customizer_redirect' ), 100 );
			}

			// Menus & help
			add_action( 'load-wp-groupbuy_page_wp-groupbuy/theme_options', array( get_class(), 'options_help_section' ), 45 );
			add_action( 'load-wp-groupbuy_page_wp-groupbuy/theme_color_options', array( get_class(), 'options_help_section' ), 45 );
			// Defaults
			add_action( 'load-wp-groupbuy_page_wp-groupbuy/translation', array( get_class(), 'options_help_section_defaults' ), 50 );
			add_action( 'load-wp-groupbuy_page_wp-groupbuy/subscription', array( get_class(), 'options_help_section_defaults' ), 50 );
			add_action( 'load-wp-groupbuy_page_wp-groupbuy/theme_options', array( get_class(), 'options_help_section_defaults' ), 50 );
			add_action( 'load-wp-groupbuy_page_wp-groupbuy/theme_color_options', array( get_class(), 'options_help_section_defaults' ), 50 );
		}

		private function __construct() {
			// Flavor
			add_action( 'parse_request', array( get_class(), 'flavor_css' ) );
			// Footer Scripts
			add_action( 'wp_footer', array( get_class(), 'wpg_footer_scripts' ), 100 );
			// Location Flavor
			add_action( wg_get_location_tax_slug().'_edit_form_fields', array( $this, 'location_input_metabox' ), 10, 2 );
			add_action( 'admin_enqueue_scripts', array( get_class(), 'iris_admin_enqueue_scripts' ) );
			add_action( 'edited_terms', array( get_class(), 'save_location_meta_data' ) );
			add_action( 'wp_head', array( get_class(), 'location_css' ) );
			add_filter( 'theme_mod_background_image', array( get_class(), 'background_image_filter' ) , 10, 1 );
			add_filter( 'wg_account_registration_panes', array( get_class(), 'display_tos_message' ), 100 );
		}

		public static function get_settings_page() {
			return self::$theme_settings_page;
		}

		// Theme Customizer
		public static function get_registrations( $reset = FALSE ) {
			$current = get_option( self::CUSTOMIZER_OPTIONS_PREFIX . WPG_THEME_SLUG );
			if ( $current && !$reset )
				return $current;

			self::color_registration_merge();
			update_option( self::CUSTOMIZER_OPTIONS_PREFIX . WPG_THEME_SLUG, self::$customizer_options, true );
			return self::$customizer_options;
		}

		public static function color_registration_merge() {
			foreach ( wg_custom_color_registrations() as $key => $values ) { 
				$colors = wg_custom_color_registrations();
				foreach ( $colors[$key]['rules'] as $rule => $value ) { 
					if ( strstr( $rule, 'color' ) || strstr( $rule, 'background' ) ) {
						self::$customizer_options[$key.'-'.$rule] = '#'.$value;
					} else {
						self::$customizer_options[$key.'-'.$rule] = $value;
					}
				}
			}
			return self::$customizer_options;
		}
                

		public static function reset_customizer_options() {
                    $start = '';

			if ( ( isset( $_REQUEST['action'] ) && $_REQUEST['action'] == self::CUSTOMIZER_RESET_QUARY_ARG ) || $start ) {
				self::get_registrations( TRUE );
				if ( !defined( 'DOING_AJAX' ) ) {
					wp_redirect( remove_query_arg( 'action' ) );
				}
				exit();
			}
		}

		public static function wg_converted_theme_customizer() {
			if ( get_option( self::CONVERT_THEME_CUSTOMIZATIONS, FALSE ) )
				return;

			foreach ( wg_custom_color_registrations() as $key => $values ) { 
				$colors = wp_parse_args( self::$deprecated_registered_colors, wg_custom_color_registrations() ); // merge the registered options and previous options.
				foreach ( $colors[$key]['rules'] as $rule => $value ) { 
					if ( strstr( $rule, 'color' ) || strstr( $rule, 'background' ) ) {
						self::$customizer_options[$key.'-'.$rule] = '#'.$value;
					} else {
						self::$customizer_options[$key.'-'.$rule] = $value;
					}
				}
			}
			update_option( self::CUSTOMIZER_OPTIONS_PREFIX . WPG_THEME_SLUG, self::$customizer_options, true );
			update_option( self::CONVERT_THEME_CUSTOMIZATIONS, 1, true ); // Set the option so this doesn't run again.
		}

		public static function theme_customizer( $wp_customize ) {

			$wp_customize->add_section( 'wpg_theme_color_schemer', array(
					'title'          => wpg__( 'WPG Color Styling' ),
					'priority'       => 35,
				) );

			$wp_customize->add_section( 'wpg_theme_font_schemer', array(
					'title'          => wpg__( 'WPG Font Styling' ),
					'priority'       => 37,
				) );

			$wp_customize->add_section( 'wpg_theme_other_schemer', array(
					'title'          => wpg__( 'WPG Misc. Theme Styling' ),
					'priority'       => 39,
				) );
			$count = 0;
			foreach ( wg_custom_color_registrations() as $key => $values ) { 
				$colors = wg_custom_color_registrations(); 
				foreach ( $colors[$key]['rules'] as $rule => $default_value ) { 

					$count ++;
					$slug = $key.'-'.$rule;
					$option_name = self::CUSTOMIZER_OPTIONS_PREFIX . WPG_THEME_SLUG."[".$slug."]";
					$value = ( isset( self::$customizer_options[$slug] ) && self::$customizer_options[$slug] ) ? self::$customizer_options[$slug] : $default_value ;

					if ( strstr( $rule, 'color' ) || strstr( $rule, 'background' ) ) {
						$wp_customize->add_setting( $option_name, array(
								'default'           => $value,
								'type'              => 'option',
								'capability'        => 'edit_theme_options',
								'transport'         => 'postMessage',
							) );

						switch ( $rule ) {
						case 'background':
						case 'background-color':
						case 'background-important':
							$type = wpg__( 'Background Color' );
							break;
						case 'color':
						case 'color-important':
							$type = wpg__( 'Text Color' );
							break;
						default:
							$type = wpg__( 'Color' );
							break;
						}

						$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, $slug, array(
									'label'   => $colors[$key]['name'] . ' &mdash; ' . $type,
									'section' => 'wpg_theme_color_schemer',
									'settings'   => $option_name,
									'priority' => $count
								) ) );
					}
					elseif ( strstr( $rule, 'font-family' ) ) {

						$wp_customize->add_setting( $option_name, array(
								'default'           => $value,
								'type'              => 'option',
								'capability'        => 'edit_theme_options',
								'transport'         => 'postMessage',
							) );

						$fonts = array();
						foreach ( self::get_fonts() as $font_id => $font ) {
							$fonts[sanitize_title_with_dashes( $font['font-name'] )] = $font['font-name'];
						}

						$wp_customize->add_control( $slug, array(
								'label'   => 'Font Family &mdash; ' . $colors[$key]['name'],
								'section' => 'wpg_theme_font_schemer',
								'type'    => 'select',
								'choices'    => $fonts,
								'settings'   => $option_name,
								'priority' => $count
							) );

					}
					elseif ( strstr( $rule, 'font-size' ) ) {

						$wp_customize->add_setting( $option_name, array(
								'default'           => $value,
								'type'              => 'option',
								'capability'        => 'edit_theme_options',
								'transport'         => 'postMessage',
							) );

						$wp_customize->add_control( $slug, array(
								'label'   => 'Font Size &mdash; ' . $colors[$key]['name'],
								'section' => 'wpg_theme_font_schemer',
								'type'    => 'select',
								'choices'    => self::$font_sizes,
								'settings'   => $option_name,
								'priority' => $count
							) );
					}
					elseif ( strstr( $rule, 'border-radius' ) ) {

						$wp_customize->add_setting( $option_name, array(
								'default'           => $value,
								'type'              => 'option',
								'capability'        => 'edit_theme_options',
								'transport'         => 'postMessage',
							) );

						$wp_customize->add_control( $slug, array(
								'label'   => $colors[$key]['name'] . ' &mdash; Border Radius',
								'section' => 'wpg_theme_other_schemer',
								'type'    => 'select',
								'choices'    => self::$font_sizes,
								'settings'   => $option_name,
								'priority' => $count
							) );
					}
					else {

						$wp_customize->add_setting( $option_name, array(
								'default'           => $value,
								'type'              => 'option',
								'capability'        => 'edit_theme_options',
								'transport'         => 'postMessage',
							) );

						$wp_customize->add_control( $slug, array(
									'label'   => $colors[$key]['name'],
									'section' => 'wpg_theme_other_schemer',
									'settings'   => $option_name,
									'type'    => 'input',
									'priority' => $count
								) );
					}
				}
			}

			if ( $wp_customize->is_preview() && !is_admin() )
				add_action( 'wp_footer', array( get_class(), 'customizer_preview_js' ), 21 );
		}

		public static function customizer_preview_js() {
			echo '<script type="text/javascript">';
			echo '( function( $ ) {';
			foreach ( wg_custom_color_registrations() as $key => $values ) { 
				$colors = wg_custom_color_registrations();
				foreach ( $colors[$key]['rules'] as $rule => $value ) { 
					
					$slug = $key.'-'.$rule;
					$option_name = self::CUSTOMIZER_OPTIONS_PREFIX . WPG_THEME_SLUG."[".$slug."]";

					echo 'wp.customize( "'.$option_name.'", function( value ) {';
						echo 'value.bind( function( to ) {';
							echo "$('".$values['selectors']."').css('".$rule."', to ? to : '' );";

						echo '});';
					echo '});';
				}
			}
			echo '} )( jQuery )';
			echo '</script>';
		}

		// Flavor CSS
		public static function get_flavor_css() {
			$output = '';
			$imports = '';
			foreach ( wg_custom_color_registrations() as $key => $values ) { 
				$registered_rules = wg_custom_color_registrations(); 
				foreach ( $registered_rules[$key]['rules'] as $rule => $default_value ) { 
					
					$slug = $key.'-'.$rule;
					$value = ( isset( self::$customizer_options[$slug] ) && self::$customizer_options[$slug] ) ? self::$customizer_options[$slug] : $default_value ;

					$added = array(); // don't repeat already imported font faces
					$important = '';

					// Handle Fonts differently
					if ( !in_array( $rule, $added ) && in_array( $rule, array( 'font-family', 'font-family-important' ) ) ) {

						$all_fonts = self::get_fonts();
						foreach ( $all_fonts as $fonts_key => $fonts_value ) {
							$font = sanitize_title_with_dashes( $fonts_value['font-name'] );
							if ( !in_array( $rule, $added ) && $value == $font ) {
								$added[] = $rule;
								if ( in_array( $value, array( 'arial', 'arial-black', 'impact', 'tahoma', 'verdana', 'georgia', 'helvetica-neue', 'trebuchet-ms', 'garamond', 'times-new-roman', 'lucida-grande', 'courier-new' ) ) ) {
									$output .= $values['selectors']." { font-family: ".$fonts_value['font-family'].$important."; } \n";
								} else {
									$imports .= "@import url('https://fonts.googleapis.com/css?family=".$fonts_value['font-name'].":r,b');\n";
									$output .= $values['selectors'].' {';
									$output .= 'font-family: "'.$fonts_value['font-name'].'", '.$all_fonts['web-fonts']['font-family'].$important.';';
									$output .= "} \n";
								}
								unset( $registered_rules[$key] );
							}
						}
					}
				}
				if ( isset( $registered_rules[$key] ) ) {
					$output .= $values['selectors'].' {';
					foreach ( $registered_rules[$key]['rules'] as $rule => $default_value ) {

						$slug = $key.'-'.$rule;
						$value = ( isset( self::$customizer_options[$slug] ) && self::$customizer_options[$slug] ) ? self::$customizer_options[$slug] : $default_value ;

						if ( strpos( $rule, '-important' ) !== FALSE ) {
							$rule = str_replace( '-important', '', $rule );
							$value = $value.' !important';
						}
						if ( !in_array( $rule, array( 'font-family', 'font-family-important' ) ) ) {
							$output .= $rule.': '.$value.'; ';
						}
					}
					$output .= "} \n";
				}
			}
			print $imports;
			print $output;
		}

		// Redirect from the default customize.php
		public static function customizer_redirect() {
			global $pagenow;
			if ( $pagenow == 'customize.php' && !isset( $_GET['url'] ) ) {
				$redirect = add_query_arg( array( 'url' => wg_get_deals_link() ), wp_customize_url() );
				wp_redirect( $redirect );
				exit;
			}
		}

		// Help
		public static function options_help_section() {
			// Empty
		}
		
		// Enqueue
		public static function custom_css( $wp ) {
			header( 'Content-type: text/css' );
			do_action( 'wg_custom_css' );
			if ( function_exists( 'wg_custom_color_registrations' ) ) {
				self::get_flavor_css();
			}
			wg_theme_custom_css();
			do_action( 'wg_custom_css_after' );
			exit();
		}

		public static function flavor_css() {
			wp_enqueue_style( 'custom_css', self::get_css_url(), array( 'template_style' ) );
		}

		// Enqueue color selection on the theme options page only
		public static function iris_admin_enqueue_scripts( $hook ) {
			if( 'wp-groupbuy_page_wp-groupbuy/theme_options' == $hook || 'edit-tags.php' == $hook ) {
				wp_enqueue_script( 'wp-color-picker' );
				wp_enqueue_style( 'wp-color-picker' );
				wp_enqueue_script( 'wg_colorpicker_load', get_bloginfo( 'template_directory' ) . '/wpg-framework/addons/options/js/jquery.scripts.js', array( 'jquery', 'wp-color-picker' ) );
			}
		}

		// Create Save meta data
		public static function location_css() {
			$term_id = wg_get_current_location_extended( 'id' );
			$background_color = self::location_background_color( $term_id );
			$background_image_url = self::location_background_image( $term_id );
			$background_image_repeat = self::location_background_image_repeat( $term_id );

			$background_css = '';
			if ( $background_color ) {
				$background_css .= "body { background-color:$background_color; }";
			}
			if ( $background_image_url ) {
				$background_css .= "body { background-image:url($background_image_url); background-repeat:".$background_image_repeat."; }";
			}
			$css = '<style type="text/css">'.$background_css.'</style>';
			echo apply_filters( 'wg_location_css', $css , $term_id );
		}

		// Utility
		public static function wpg_footer_scripts() {
			wg_theme_footer_scripts();
		}

		public static function get_fonts( $cache_reset = FALSE ) {
			$cache_key = 'wg_theme_fonts_cache';
			if ( !$cache_reset ) {
				$cache = get_transient( $cache_key );
				if ( !empty( $cache ) ) {
					return apply_filters( 'wg_theme_get_fonts', $cache );
				}
			}
			$fonts_seraliazed = wp_remote_get( 'http://phat-reaction.com/googlefonts.php?format=php' );
			$font_array = unserialize( wp_remote_retrieve_body( $fonts_seraliazed ) );
			if ( empty( $font_array ) ) {
				$json_array = file_get_contents( get_template_directory() . '/wpg-framework/addons/options/cache/google-fonts.php' );
				$font_array = unserialize( $json_array );
			}
			$fonts = wp_parse_args( $font_array, self::$font_faces );
			set_transient( $cache_key, $fonts, 60*60*24*7 ); // cache for a week
			return apply_filters( 'wg_theme_get_fonts', $fonts );
		}
		
		public static function display_tos_message( array $panes ) {
			$content = '';
			if ( self::$tos_pageid ) {
				if ( self::$pp_pageid ) {
					$content = '<p align="center">'.sprintf( self::__( 'By registering you agree to the <a href="%s">Terms and Conditions</a> and <a href="%s">Privacy Policy</a>.' ), get_permalink( self::$tos_pageid ), get_permalink( self::$pp_pageid ) ).'</p>';
				} else {
					$content = '<p align="center">'.sprintf( self::__( 'By registering you agree to the <a href="%s">Terms and Conditions</a>.' ), get_permalink( self::$tos_pageid ) ).'</p>';
				}
			}

			$panes['tos'] = array(
				'weight' => 101,
				'body' => apply_filters( 'wg_display_tos_message', $content ),
			);
			return $panes;
		}


		// URLs
		public static function get_css_url() {
			if ( self::using_permalinks() ) {
				return home_url( trailingslashit( self::$custom_css_path ), is_ssl()?'https':NULL );
			} else {
				return add_query_arg( array( self::CUSTOM_CSS_VAR => 1 ), home_url( '', is_ssl()?'https':NULL ) );
			}
		}

		// Redirect when viewing the flavor CSS on SSL
		public static function ssl_redirect() {
			global $wp;
			if ( is_ssl() && isset( $wp->query_vars[self::CUSTOM_CSS_VAR] ) && $wp->query_vars[self::CUSTOM_CSS_VAR] ) {
				return FALSE;
			}
			return TRUE;
		}

		// https
		public static function background_image_filter( $url ) {
			if ( !is_string( $url ) ) return $url;
			$background_image_url = self::location_background_image();
			if ( !empty( $background_image_url ) ) {
				$url = $background_image_url;
			}
			if ( is_ssl() ) {
				$url = str_replace( 'http://', 'https://', $url );
			} else {
				$url = str_replace( 'https://', 'http://', $url );
			}
			return $url;
		}


		public static function location_background_image( $term_id = NULL ) {
			if ( NULL === $term_id ) {
				$term_id = wg_get_current_location_extended( 'id' );
			}
			$background_image_url = get_metadata( 'location_terms', $term_id, 'background_image_url', TRUE );
			return apply_filters( 'wg_location_background_image', $background_image_url, $term_id );
		}

		public static function location_background_color( $term_id = NULL ) {
			if ( NULL === $term_id ) {
				$term_id = wg_get_current_location_extended( 'id' );
			}
			$background_color = get_metadata( 'location_terms', $term_id, 'background_color', TRUE );
			return apply_filters( 'location_background_color', $background_color, $term_id );
		}

		public static function location_background_image_repeat( $term_id = NULL ) {
			if ( NULL === $term_id ) {
				$term_id = wg_get_current_location_extended( 'id' );
			}
			$background_image_repeat = get_metadata( 'location_terms', $term_id, 'background_image_repeat', TRUE );
			return apply_filters( 'location_background_image_repeat', $background_image_repeat, $term_id );
		}

		// WPDB
		public static function setup_location_table() {
			global $wpdb;
			$type = "location_terms";
			$table_name = $wpdb->prefix . $type . 'meta';
			$variable_name = $type . 'meta';
			$wpdb->$variable_name = $table_name;
		}

		// Add table for location terms.
		public static function create_location_table() {
			global $wpdb;
			$type = "location_terms";
			$table_name = $wpdb->prefix . $type . 'meta';

			if ( !empty ( $wpdb->charset ) )
				$charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}";
			if ( !empty ( $wpdb->collate ) )
				$charset_collate .= " COLLATE {$wpdb->collate}";

			$sql = "CREATE TABLE IF NOT EXISTS {$table_name} (
				meta_id bigint(20) NOT NULL AUTO_INCREMENT,
				{$type}_id bigint(20) NOT NULL default 0,

				meta_key varchar(255) DEFAULT NULL,
				meta_value longtext DEFAULT NULL,

				UNIQUE KEY meta_id (meta_id)
			) {$charset_collate};";

			require_once ABSPATH . 'wp-admin/includes/upgrade.php';
			dbDelta( $sql );
		}

		private function __clone() {
			trigger_error( __CLASS__.' may not be cloned', E_USER_ERROR );
		}
		private function __sleep() {
			trigger_error( __CLASS__.' may not be serialized', E_USER_ERROR );
		}
		public static function get_instance() {
			if ( !( self::$instance && is_a( self::$instance, __CLASS__ ) ) ) {
				self::$instance = new self();
			}
			return self::$instance;
		}

		public static function register_settings_fields() {

			$page = WP_Groupbuy_UI::get_settings_page();
			$reg_section = 'wg_registration_settings';
			register_setting( $page, self::TOS_PAGE_ID );
			register_setting( $page, self::PP_PAGE_ID );
			register_setting( $page, self::FEATURED_ID );
			register_setting( $page, self::ENABLE_MERCHANT_EDIT );
			register_setting( $page, 'wg_enable_merchant' );
			

			add_settings_field( self::TOS_PAGE_ID, self::__( 'Terms and Conditions Page' ), array( get_class(), 'display_tos' ), $page, $reg_section );
			add_settings_field( self::PP_PAGE_ID, self::__( 'Privacy Policy Page' ), array( get_class(), 'display_pp' ), $page, $reg_section );
			add_settings_field( self::FEATURED_ID, self::__( 'Featured Deals Category' ), array( get_class(), 'display_featured_cat' ), $page, $reg_section );


			$section = 'wg_general_settings';
			register_setting( $page, self::FORCE_LOGIN );
			add_settings_field( self::FORCE_LOGIN, self::__( 'How do you want to show Deals?' ), array( get_class(), 'display_option_force_login' ), $page, $section );

			// options
			add_settings_field( self::FOOTER_SCRIPT_OPTION, self::__( 'Footer Scripts' ), array( get_class(), 'display_footer_script' ), self::$theme_settings_page );
			add_settings_field( self::NO_DEALS_CONTENT, self::__( 'Empty Deals Content' ), array( get_class(), 'display_nodeals' ), self::$theme_settings_page );
			add_settings_field( self::ENABLE_RTL, self::__( 'Enable RTL' ), array( get_class(), 'display_enable_rtl' ), self::$theme_settings_page );
			
			//theme styling
			$section = 'wg_theme_styling';
			add_settings_section( $section, self::__( 'WPGroupbuy Theme Customizations' ), array( get_class(), 'display_style_section' ), self::$theme_settings_page );

			register_setting( self::$theme_settings_page, self::FOOTER_SCRIPT_OPTION );
			register_setting( self::$theme_settings_page, self::TWITTER_OPTION );
			register_setting( self::$theme_settings_page, self::FACEBOOK_OPTION );
			register_setting( self::$theme_settings_page, self::NO_DEALS_CONTENT );
			register_setting( self::$theme_settings_page, self::ENABLE_RTL );
			register_setting( self::$theme_settings_page, self::ENABLE_MERCHANT_EDIT );
			register_setting( self::$theme_settings_page, self::HEADER_LOGO_OPTION );
			register_setting( self::$theme_settings_page, self::CUSTOM_CSS_OPTION );

			add_settings_field( 'display_customization', self::__( 'Theme Customization' ), array( get_class(), 'display_customization' ), self::$theme_settings_page, $section );
			add_settings_field( self::CUSTOM_CSS_OPTION, self::__( 'Custom CSS' ), array( get_class(), 'display_css_textarea' ), self::$theme_settings_page, $section );
			add_settings_field( self::HEADER_LOGO_OPTION, self::__( 'Website Logo' ), array( get_class(), 'display_header_logo' ), self::$theme_settings_page, $section );
			add_settings_field( 'display_background_image', self::__( 'Background Image' ), array( get_class(), 'display_background_image' ), self::$theme_settings_page, $section );
		}

		public static function display_customization() {
			self::_e( sprintf( 'Change color, style with <a href="%s" target="_blank">theme customizer</a>.', wp_customize_url() ) );
			?>
				<p>
				<script type="text/javascript">
					jQuery(document).ready( function($) {
                                            
                                        
						var $button = $('#reset_customizer');
						var $span = $('#reset_customizer_ajax');
						
						$button.click(function(event) {
							$span.fadeOut();
							event.preventDefault();
							$.ajax({
								type: 'POST',
								dataType: 'json',
								url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
								data: {
									action: '<?php echo self::CUSTOMIZER_RESET_QUARY_ARG ?>'
								},
								success: function(data) {
									$span.empty().fadeIn().append('<?php wpg_e('Settings Reset') ?>');
								}
							});
						});
                                                $('#upload_logo_button').click(function() {
                                                    tb_show('Upload a logo', 'media-upload.php?referer=wpg-upload-settings&amp;type=image&amp;TB_iframe=true&amp;post_id=0', false);
                                                    return false;
                                            });

                                            window.send_to_editor = function(html) {
                                                    var image_url = $('img',html).attr('src');
                                                    //alert(html);
                                                    $('#website_logo').val(image_url);
                                                    tb_remove();
                                                    $('#upload_logo_preview img').attr('src',image_url);

                                                    $('#submit_options_form').trigger('click');
                                                    // $('#uploaded_logo').val('uploaded');

                                            }
                                            // Tooltips
                                            jQuery(".tips, .help_tip").tipTip({
                                                'attribute' : 'title',
                                                'fadeIn' : 50,
                                                'fadeOut' : 50,
                                                'delay' : 200
                                            });
					});
				</script>
				<?php self::_e( sprintf( '<a href="%s" id="reset_customizer" class="button-secondary">Reset Customizer</a>&nbsp;<span id="reset_customizer_ajax"></span>', add_query_arg( array( 'action' => self::CUSTOMIZER_RESET_QUARY_ARG ) ) ) ); ?>
				</p>
			<?php
			
		}

		public static function display_background_image() {
			$background = 'themes.php?page=custom-background';
			$location = 'edit-tags.php?taxonomy=wg_location&post_type=wg_deal';
			self::_e( sprintf( 'Change the website background <a target="_blank" href="%s">here</a>.', $background ) );
			echo "<br/>";
			self::_e( sprintf( 'Change the location specific backgrounds by editing <a target="_blank" href="%s">locations</a>.', $location ) );
		}

		public static function display_css_textarea() {
			do_action('wg_theme_options_display_css_textarea');
			echo '<textarea rows="5" cols="40" name="'.self::CUSTOM_CSS_OPTION.'">'.self::$custom_css.'</textarea>';
			echo '<img width="16" height="16" src="'.WG_RESOURCES . 'images/help.png'. '" class="help_tip" title="'.self::__( 'Add your custom CSS Style here' ).'">';
		}

		public static function display_header_logo() {
			echo '<input id="website_logo" type="text" class="regular-text" name="'.self::HEADER_LOGO_OPTION.'" value="'.self::$header_logo.'" />';
			echo '<img width="16" height="16" src="'.WG_RESOURCES . 'images/help.png'. '" class="help_tip" title="'.self::__( 'Copy URL of image here or click upload image button below. To remove image, just delete URL path on text field and save changes' ).'">';
                        require_once WG_PATH.'/includes/upload.php';
                        echo '<br/><br/>';
                        wpg_options_enqueue_scripts();
                        wpg_setting_logo(self::$header_logo);
                        wpg_setting_logo_preview(self::$header_logo);
                         
		}

		public static function display_footer_script() {
			echo '<textarea rows="5" cols="40" name="'.self::FOOTER_SCRIPT_OPTION.'">'.self::$footer_scripts.'</textarea>';
			echo '<img width="16" height="16" src="'.WG_RESOURCES . 'images/help.png'. '" class="help_tip" title="'.self::__( 'You can add Google Analytics, custom javascript here' ).'">';
		}

		public static function display_twitter() {
			echo '<input type="text" class="regular-text" name="'.self::TWITTER_OPTION.'" value="'.self::$twitter.'" />';
		}

		public static function display_facebook() {
			echo '<input type="text" class="regular-text" name="'.self::FACEBOOK_OPTION.'" value="'.self::$facebook.'" />';
		}

		public static function display_nodeals() {
			wp_dropdown_pages( array( 'name' => self::NO_DEALS_CONTENT, 'echo' => 1, 'show_option_none' => self::__( '-- Select --' ), 'option_none_value' => '0', 'selected' => self::$nodeal_pageid ) );
			echo '<img width="16" height="16" src="'.WG_RESOURCES . 'images/help.png'. '" class="help_tip" title="'.self::__( 'Redirect empty deals page to a page you choose above' ).'">';
		}
                
		public static function display_enable_rtl() {
			$active = get_option( self::ENABLE_RTL, '0' );
			echo '<label name="'.self::ENABLE_RTL.'"><input type="checkbox" value="1" name="'.self::ENABLE_RTL.'" '.checked( '1', $active, false ).' /> </label>';
			echo '<img width="16" height="16" src="'.WG_RESOURCES . 'images/help.png'. '" class="help_tip" title="'.self::__( 'Check to enable Right-to-Left language support' ).'">';
		}
		
		public static function display_tos() {
			wp_dropdown_pages( array( 'name' => self::TOS_PAGE_ID, 'echo' => 1, 'show_option_none' => self::__( '-- Select --' ), 'option_none_value' => '0', 'selected' => self::$tos_pageid ) );
		}

		public static function display_pp() {
			wp_dropdown_pages( array( 'name' => self::PP_PAGE_ID, 'echo' => 1, 'show_option_none' => self::__( '-- Select --' ), 'option_none_value' => '0', 'selected' => self::$pp_pageid ) );
		}
		
		public static function display_featured_cat() {
			wp_dropdown_categories( array( 'show_option_none' => self::__( '-- Select --' ), 'echo' => 1, 'selected' => self::$featured_cat, 'name' => self::FEATURED_ID, 'hide_empty' => 0, 'taxonomy' => 'wg_category' ) );
		}

		public static function color_section() {
			echo "<p>";
			printf( self::__( 'Customize the theme by changing the colors below. Customize the background color and image via <a href="%s">Appearance > Background</a>.' ), admin_url( '/themes.php?page=custom-background' ) );
			echo "</p>";
		}

		// Create Input field in deal (location) taxonomy add and edit.
		public static function location_input_metabox( $tag ) {
			$background_color = get_metadata( 'location_terms', $tag->term_id, 'background_color', TRUE );
			$background_image_url = get_metadata( 'location_terms', $tag->term_id, 'background_image_url', TRUE );
			$background_image_repeat = get_metadata( 'location_terms', $tag->term_id, 'background_image_repeat', TRUE );
			$background_image_repeat = ( $background_image_repeat ) ? $background_image_repeat : "repeat-x";
			$logo_image_url = get_metadata( 'location_terms', $tag->term_id, 'logo_image_url', TRUE );
?>
					</tbody>
				</table>
				<h3><?php wpg_e( 'Custom Flavor' ) ?></h3>
				<script type="text/javascript">
					jQuery(document).ready(function($) {
						var $formfield, formfield, $preview;
						jQuery('.upload_image_button').click(function() {
							$formfield = $(this).prev();
							$preview = $(this).next('img');
							formfield = $formfield.attr('name');
						tb_show( '', 'media-upload.php?type=image&amp;TB_iframe=true' );
						return false;
						});
						window.send_to_editor = function(html){
							fileurl = $('img',html).attr('src');
							$formfield.val(fileurl);
							tb_remove();
							$preview.attr('src',fileurl);
							
						}
						// Tooltips
						jQuery(".tips, .help_tip").tipTip({
							'attribute' : 'title',
							'fadeIn' : 50,
							'fadeOut' : 50,
							'delay' : 200
						});

					});					
				</script>

				<table class="form-table">
					<tbody>
						<tr class="form-field">
							<th scope="row" valign="top"><label for="background_color"><?php wpg_e( 'Background Color' ) ?></label></th>
							<td><input type="text" class="color_picker w5e" value="<?php echo $background_color; ?>" id="background_color" name="background_color"/></td>
						</tr>
						<tr class="form-field">
							<th scope="row" valign="top"><label for="background_image_url"><?php wpg_e( 'Background Image URL' ) ?></label></th>
							<td>
								<input style="width: 50%;" type="text" size="40" value="<?php echo $background_image_url; ?>" id="background_image_url" name="background_image_url" />
								
								<?php
									require_once WG_PATH.'/includes/upload.php';
							        wpg_options_enqueue_scripts();
							        wpg_setting_locations($background_image_url);
							        wpg_setting_locations_preview($background_image_url);
								?>
							</td>
						</tr>
						<tr class="form-field">
							<th scope="row" valign="top"><label for="background_image_repeat"><?php wpg_e( 'Background Image Repeat' ) ?></label></th>
							<td>
								<input type="radio" value="repeat" <?php if ( $background_image_repeat=="repeat" ) {echo "checked ";} ?>id="title" name="background_image_repeat" style="width:auto" /> <label for="title"><?php wpg_e( 'Tile' ) ?></label>&nbsp;&nbsp;
								<input type="radio" value="repeat-x" <?php if ( $background_image_repeat=="repeat-x" ) {echo "checked ";} ?>id="Horizontal" name="background_image_repeat" style="width:auto"/> <label for="Horizontal"><?php wpg_e( 'Horizontal' ) ?></label>&nbsp;&nbsp;
								<input type="radio" value="repeat-y" <?php if ( $background_image_repeat=="repeat-y" ) {echo "checked ";} ?>id="Vertical" name="background_image_repeat" style="width:auto"/> <label for="Vertical"><?php wpg_e( 'Vertical' ) ?></label>&nbsp;&nbsp;
								<input type="radio" value="no-repeat" <?php if ( $background_image_repeat=="no-repeat" ) {echo "checked ";} ?>id="None" name="background_image_repeat" style="width:auto"/> <label for="None"><?php wpg_e( 'None' ) ?></label>
							</td>
						</tr>
						<tr class="form-field">
							<th scope="row" valign="top"><label for="logo_image_url"><?php wpg_e( 'Logo Image URL' ) ?></label></th>
							<td>
								<input style="width: 50%;"  type="text" size="40" value="<?php echo $logo_image_url; ?>" id="logo_image_url" name="logo_image_url" />
								
								<?php
									require_once WG_PATH.'/includes/upload.php';
							        wpg_options_enqueue_scripts();
							        wpg_setting_logo_image_url($logo_image_url);
							        wpg_setting_logo_image_url_preview($logo_image_url);
								?>
							</td>
						</tr>
					</tbody>
				</table>
			<?php

				
			}

		// Create Save meta data to table.
		public static function save_location_meta_data( $term_id ) {
			if ( isset( $_POST['background_color'] ) ) {
				$background_color = esc_attr( $_POST['background_color'] );
				update_metadata( 'location_terms', $term_id, 'background_color', $background_color );
			}
			if ( isset( $_POST['background_image_url'] ) ) {
				$background_image_url = esc_attr( $_POST['background_image_url'] );
				update_metadata( 'location_terms', $term_id, 'background_image_url', $background_image_url );
			}
			if ( isset( $_POST['background_image_repeat'] ) ) {
				$background_image_repeat = esc_attr( $_POST['background_image_repeat'] );
				update_metadata( 'location_terms', $term_id, 'background_image_repeat', $background_image_repeat );
			}
			if ( isset( $_POST['logo_image_url'] ) ) {
				$logo_image_url = esc_attr( $_POST['logo_image_url'] );
				update_metadata( 'location_terms', $term_id, 'logo_image_url', $logo_image_url );
			}
		}

		public static function display_option_force_login() {
			echo '<label><input type="radio" name="'.self::FORCE_LOGIN.'" value="false" '.checked( 'false' , self::$force_login, FALSE ).'/> '.self::__( 'Show deals for everyone, guest can buy deals.' ).'</label><br /	>';
			echo '<label><input type="radio" name="'.self::FORCE_LOGIN.'" value="subscriptions" '.checked( 'subscriptions', self::$force_login, FALSE ).'/> '.self::__( 'Visitor can see the deal but they need to login to get more details.' ).'</label><br />';
			echo '<label><input type="radio" name="'.self::FORCE_LOGIN.'" value="true" '.checked( 'true', self::$force_login, FALSE ).'/> '.self::__( 'Show deals for membership only.' ).'</label><br />';
		}

	}
}
add_action( 'init', array( 'WP_Groupbuy_Theme_UI', 'init' )  );
