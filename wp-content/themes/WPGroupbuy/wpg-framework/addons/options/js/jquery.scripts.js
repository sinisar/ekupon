jQuery(document).ready(function($) {
	// Loop through all color pickers and init the wpColorPicker
	$('.color_picker').each(function() {
		var picker = $(this);

		picker.wpColorPicker({
			change: function(event, ui) {
				// After every change callback to wg_iris_pickColor
				wg_iris_pickColor( picker, picker.wpColorPicker("color") );
				// Refresh the preview and generated css
			},
			clear: function() {
				// clear out the text area
				wg_iris_pickColor( picker, "" );
			}
		});
		// toggle text area when picked
		picker.click(wg_iris_toggle_text(picker));
		wg_iris_toggle_text(picker);
	});

	// set the input value
	function wg_iris_pickColor( e, color ) {
		e.val(color);
	}
	// toggle the text area with the color value
	function wg_iris_toggle_text(e) {
		var default_color = "000000"; // todo, should be value
		if ('' === e.val().replace('#', '')) {
			e.val(default_color);
			wg_iris_pickColor(e, default_color);
		} else {
			wg_iris_pickColor(e, e.val());
		}
	}
});

