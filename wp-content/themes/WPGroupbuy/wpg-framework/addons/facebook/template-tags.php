<?php

// Print FB login button
if ( !function_exists( 'wg_facebook_button' ) ) {
	add_shortcode( 'wg_facebook_button', 'wg_facebook_button' );
	function wg_facebook_button( $button_text = '' ) {
		if ( !is_user_logged_in() && class_exists( 'WP_Groupbuy_Facebook_Connect' ) ) {
			echo WP_Groupbuy_Facebook_Connect::button( $button_text );
		}
	}
}
