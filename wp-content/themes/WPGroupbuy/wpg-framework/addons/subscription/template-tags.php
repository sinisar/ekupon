<?php

// Get the preferred location based on user cookie
if ( !function_exists( 'wg_get_preferred_location' ) ) {
	function wg_get_preferred_location() {
		if ( isset( $_COOKIE[ 'wg_location_preference' ] ) && $wg_location_preference = $_COOKIE[ 'wg_location_preference' ] ) {
			if ( !term_exists( $wg_location_preference, wg_get_deal_location_tax() ) ) {
				return FALSE;
			}
			return apply_filters( 'wg_get_preferred_location', $wg_location_preference );
		}
		return FALSE;
	}
}

add_shortcode( 'wg_subscription_form', 'wg_sub_form_shortcode' );

// Subscription form
function wg_subscription_form( $show_locations = TRUE, $select_location_text = 'Select Location:', $button_text = 'Subscribe', $echo = TRUE ) {
	ob_start();
		?>
		<form action="#" id="wg_subscription_form" method="post" class="clearfix">
			
        <div class="text fl fixPNG"><strong><?php wpg_e( 'Get new deals by email' ); ?></strong></div>
        <div class="bgSubscribeEmail fl"><input type="text" name="email_address" id="email_address" value="<?php wpg_e( 'Enter your email' ); ?>" onblur="if (this.value == '')  {this.value = '<?php wpg_e( 'Enter your email' ); ?>';}" onfocus="if (this.value == '<?php wpg_e( 'Enter your email' ); ?>') {this.value = '';}" /></div>
			<?php
				$locations = wg_get_locations( false );
				$no_city_text = get_option( WP_Groupbuy_List_Services::SIGNUP_CITYNAME_OPTION );
				if ( ( !empty( $locations ) || !empty( $no_city_text ) ) && $show_locations ) {
					?>
						<div class="bgSubscribeEmail fl">
							<?php // wpg_e( $select_location_text ); ?>
							<?php
								$current_location = null;
								if ( isset( $_COOKIE[ 'wg_location_preference' ] ) && $_COOKIE[ 'wg_location_preference' ] != '' ) {
									$current_location = $_COOKIE[ 'wg_location_preference' ];
								} elseif ( is_tax() ) {
									global $wp_query;
									$query_slug = $wp_query->get_queried_object()->slug;
									if ( isset( $query_slug ) && !empty( $query_slug ) ) {
										$current_location = $query_slug;
									}
								}
								echo '<select name="deal_location" id="deal_location" size="1">';
								foreach ( $locations as $location ) {
									echo '<option value="'.$location->slug.'" '.selected( $current_location, $location->slug ).'>'.$location->name.'</option>';
								}
								if ( !empty( $no_city_text ) ) {
									echo '<option value="notfound">'.esc_attr( $no_city_text ).'</option>';
								}
								echo '</select>';
						?>
						</div>
					<?php
				} ?>
			<?php wp_nonce_field( 'wg_subscription' );?>

			<input type="submit" class="bt_send_email" name="wg_subscription" id="wg_subscription" value="<?php wpg_e( $button_text ); ?>" />
			
		</form>
		<?php
	$view = ob_get_clean();
	if ( !$echo ) {
		return apply_filters( 'wg_subscription_form', $view, $show_locations, $select_location_text, $button_text );
	}
	echo apply_filters( 'wg_subscription_form', $view, $show_locations, $select_location_text, $button_text );
}

// Shortcode to add the subscription form to any post
add_shortcode( 'wg_sub', 'wg_sub_form_shortcode' );

function wg_sub_form_shortcode( $atts ) {
	extract( shortcode_atts( array(
				'show_locations' => TRUE,
				'select_location_text' => 'Select Location:',
				'button_text' => 'Continue &rarr;'
			), $atts ) );
	return wg_subscription_form( $show_locations, $select_location_text, $button_text, FALSE );
}