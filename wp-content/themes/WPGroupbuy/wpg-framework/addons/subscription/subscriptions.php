<?php

if ( class_exists( 'WP_Groupbuy_Controller' ) ) {

	include 'template-tags.php';

	abstract class WP_Groupbuy_List_Services extends WP_Groupbuy_Controller {
		const LIST_SUBSCRIBE_OPTION = 'wg_list_service';
		const SIGNUP_REDIRECT_OPTION = 'wg_signup_redirect';
		const SIGNUP_CITYNAME_OPTION = 'wg_signup_city_name';
		const SIGNUP_NOT_FOUND_OPTION = 'wg_signup_not_found';
		const SIGNUP_FOOTER_SCRIPTS_OPTION = 'wg_signup_footer_scripts';
		const REGISTRATION_OPTION = 'wg_signup_on_registration';
		const REGISTRATION_OPTIN = 'wg_subscription_option_in';
		const SUBSCRIPTION_FORM_CUSTOM = 'wg_subscription_custom_html';
		const SUBSCRIPTION_LIGHTBOX_NEW = 'wg_subscription_lightbox_new';

		private static $list_service;
		private static $active_list_service_class;
		protected static $settings_page;
		private static $potential_processors = array();
		protected static $signup_redirect;
		protected static $default_city_name;
		protected static $not_found_redirect;
		protected static $footer_scripts;
		protected static $registration_option;
		protected static $subscription_custom_html;
		protected static $subscription_lightbox_new;

		protected static $redirect = '';

		final public static function init() {
			self::$settings_page = self::register_settings_page( 'subscription', self::__( 'Email Subscription Settings' ), self::__( 'Email Subscription' ), 14, 'theme', 'general' );
			add_action( 'admin_init', array( get_class(), 'register_settings_fields' ), 10, 0 );

			self::get_list_service();

			self::$signup_redirect = get_option( self::SIGNUP_REDIRECT_OPTION, '0' );
			self::$default_city_name = get_option( self::SIGNUP_CITYNAME_OPTION, 'Other cities' );
			self::$registration_option = get_option( self::REGISTRATION_OPTION, 'false' );
			self::$not_found_redirect = get_option( self::SIGNUP_NOT_FOUND_OPTION );
			self::$footer_scripts = get_option( self::SIGNUP_FOOTER_SCRIPTS_OPTION );
			self::$subscription_custom_html = get_option( self::SUBSCRIPTION_FORM_CUSTOM );
			self::$subscription_lightbox_new = get_option( self::SUBSCRIPTION_LIGHTBOX_NEW, 'false' );

			if ( self::$registration_option != 'false' ) {
				add_action( 'wg_account_registration_panes', array( get_class(), 'show_registration_option' ) );
			}
			add_action( 'wp_footer', array( get_class(), 'footer_scripts' ) );

			if ( version_compare( get_bloginfo( 'version' ), '3.2.99', '>=' ) ) { // 3.3. only
				add_action( 'load-wp-groupbuy_page_wp-groupbuy/subscription', array( get_class(), 'options_help_section' ), 45 );
			}

			if ( self::$subscription_lightbox_new == 'true' && !isset( $_COOKIE['wg_skip_subscription'] ) ) {
				add_action( 'init', array( 'WP_Groupbuy_Subscription_Lightbox', 'init' ) );
			}
		}

		public static function options_help_section() {
		
		}

		// Get an instance of the active subscription processor
		public static function get_list_service() {
			do_action( 'wg_register_subscription_services' );
			self::$active_list_service_class = get_option( self::LIST_SUBSCRIBE_OPTION, 'WP_Groupbuy_None' );
			self::$list_service = call_user_func( array( self::$active_list_service_class, 'get_instance' ) );
			return self::$list_service;
		}

		public static function footer_scripts() {

			if ( isset( $_GET['signup-success'] ) ) {
				echo self::$footer_scripts;
			}
		}

		public static function get_settings_page() {
			return self::$settings_page;
		}

		final protected function __clone() {
			trigger_error( __CLASS__.' may not be cloned', E_USER_ERROR );
		}

		final protected function __sleep() {
			trigger_error( __CLASS__.' may not be serialized', E_USER_ERROR );
		}

		protected static function get_instance() {}

		protected function __construct() {
			self::process_subscription_post();
		}

		public static function process_subscription_post() {
			do_action( 'process_subscription_post', $_POST );

			add_action( 'wg_registration', array( self::$active_list_service_class, 'process_registration_subscription' ), 20, 5 );

			if ( isset( $_POST['deal_location'] ) && $_POST['deal_location'] != '' ) {
				add_action( 'init', array( self::$active_list_service_class, 'process_subscription' ) );
			}
		}

		// Process a subscription
		public abstract function process_subscription();
		public abstract function process_registration_subscription();

		// Subclasses have to register to be listed as subscription options
		public  static function register() {}

		public static function success( $location = 0, $email = '' ) {

			if ( !empty( $email ) ) {
				$message = sprintf( wpg__( 'Your email %s has been added.' ), $email );
				WP_Groupbuy_Controller::set_message( $message, 'info' );
			} else {
				$message = sprintf( wpg__( 'Thank You.' ), $email );
				WP_Groupbuy_Controller::set_message( $message, 'info' );
			}

			// Set the redirect url
			// $location = ( $location == 'notfound' ) ? FALSE : $location;
			// if ( !$location && self::$not_found_redirect ) {
			// 	self::$redirect = get_permalink( self::$not_found_redirect );
			// } elseif ( self::$signup_redirect ) {
			// 	self::$redirect = get_permalink( self::$signup_redirect );
			// }
			self::$redirect = get_permalink( self::$signup_redirect );
			if ( self::$redirect == '' ) {
				self::$redirect = wg_get_latest_deal_link( $location );
			}
			if ( function_exists( 'wg_set_location_preference' ) && $location ) {
				wg_set_location_preference( $location );
			}
			self::$redirect = add_query_arg( array( 'signup-success' => '1' ), self::$redirect );
			wp_redirect( apply_filters( 'wg_subscription_success_redirect_url', self::$redirect ) );
			exit();

		}

		public static function register_settings_fields() {

			register_setting( self::$settings_page, self::LIST_SUBSCRIBE_OPTION );
			register_setting( self::$settings_page, self::SIGNUP_REDIRECT_OPTION );
			register_setting( self::$settings_page, self::SIGNUP_CITYNAME_OPTION );
			register_setting( self::$settings_page, self::SIGNUP_NOT_FOUND_OPTION );
			register_setting( self::$settings_page, self::SIGNUP_FOOTER_SCRIPTS_OPTION );
			register_setting( self::$settings_page, self::REGISTRATION_OPTION );
			register_setting( self::$settings_page, self::SUBSCRIPTION_FORM_CUSTOM );
			register_setting( self::$settings_page, self::SUBSCRIPTION_LIGHTBOX_NEW );

			add_settings_field( self::LIST_SUBSCRIBE_OPTION, self::__( 'Select Service' ), array( get_class(), 'display_list_service_selection' ), self::$settings_page );
			add_settings_field( self::SIGNUP_REDIRECT_OPTION, self::__( 'Redirect after signup' ), array( get_class(), 'display_list_service_redirect' ), self::$settings_page );
			add_settings_field( self::SIGNUP_NOT_FOUND_OPTION, self::__( 'No City Redirection' ), array( get_class(), 'display_list_service_not_found' ), self::$settings_page );
			add_settings_field( self::SIGNUP_CITYNAME_OPTION, self::__( 'No City text' ), array( get_class(), 'display_list_service_city' ), self::$settings_page );
			add_settings_field( self::REGISTRATION_OPTION, self::__( 'Show subscription form on registration page?' ), array( get_class(), 'display_registration_subscription_option' ), self::$settings_page );
			if ( self::$active_list_service_class == 'WP_Groupbuy_Custom' ) {
				add_settings_field( self::SUBSCRIPTION_FORM_CUSTOM, self::__( 'Custom HTML subscription form' ), array( get_class(), 'display_custom_list_service' ), self::$settings_page );
			}
			add_settings_field( self::SUBSCRIPTION_LIGHTBOX_NEW, self::__( 'Subscription lightbox' ), array( get_class(), 'display_subscription_lightbox_new' ), self::$settings_page );
		}

		final protected static function add_list_service( $class, $label ) {
			self::$potential_processors[$class] = $label;
		}

		public static function display_subscription_lightbox_new() {
			echo '<label><input type="checkbox" name="'.self::SUBSCRIPTION_LIGHTBOX_NEW.'" value="true" '.checked( 'true', self::$subscription_lightbox_new, FALSE ).'>&nbsp;'.self::__( 'Enable' ).'</label>';
			echo '<img width="16" height="16" src="'.WG_RESOURCES . 'images/help.png'. '" class="help_tip" title="'.self::__( 'Show Subscription Lightbox for your site.' ).'">';
			
		}

		public static function display_list_services() {
			?>
				<a href="http://mailchimp.com"><img src="https://us1.admin.mailchimp.com/_ssl/proxy.php?u=http%3A%2F%2Fgallery.mailchimp.com%2F089443193dd93823f3fed78b4%2Fimages%2FMC_MonkeyReward_15.1.png"></a>
			<?php
		}

		public static function display_list_service_selection() {
			echo '<select id="'.self::LIST_SUBSCRIBE_OPTION.'" name="'.self::LIST_SUBSCRIBE_OPTION.'">';
			foreach ( self::$potential_processors as $class => $label ) {
				echo '<option value="'.$class.'" '.selected( self::$active_list_service_class, $class ).'>'.$label.'</option>';
			}
			echo '</select>';
		}

		public static function display_list_service_redirect() {
			wp_dropdown_pages( array( 'name' => self::SIGNUP_REDIRECT_OPTION, 'echo' => 1, 'show_option_none' => self::__( '* Default' ), 'option_none_value' => '0', 'selected' => self::$signup_redirect ) );
			echo '<img width="16" height="16" src="'.WG_RESOURCES . 'images/help.png'. '" class="help_tip" title="'.self::__( 'Choose page to redirect after visitors choose city' ).'">';
		}


		public static function display_registration_subscription_option() {
			echo '<label><input type="radio" name="'.self::REGISTRATION_OPTION.'" value="true" '.checked( 'true', self::$registration_option, FALSE ).'/> '.self::__( 'Show subscription form.' ).'</label><br />';
			echo '<label><input type="radio" name="'.self::REGISTRATION_OPTION.'" value="checked" '.checked( 'checked', self::$registration_option, FALSE ).'/> '.self::__( 'Show subscription form (with checked options).' ).'</label><br/>';
			echo '<label><input type="radio" name="'.self::REGISTRATION_OPTION.'" value="false" '.checked( 'false', self::$registration_option, FALSE ).'/> '.self::__( 'Don\'t show.' ).'</label>';
		}

		public static function display_list_service_city() {
			echo '<input type="text" size="20" name="'.self::SIGNUP_CITYNAME_OPTION.'" value="'.self::$default_city_name.'" />';
			echo '<img width="16" height="16" src="'.WG_RESOURCES . 'images/help.png'. '" class="help_tip" title="'.self::__( 'The text for visitors who cannot see their city' ).'">';
		}

		public static function display_list_service_not_found() {
			wp_dropdown_pages( array( 'name' => self::SIGNUP_NOT_FOUND_OPTION, 'echo' => 1, 'show_option_none' => self::__( '* Default' ), 'option_none_value' => '0', 'selected' => self::$not_found_redirect ) );
			echo '<img width="16" height="16" src="'.WG_RESOURCES . 'images/help.png'. '" class="help_tip" title="'.self::__( 'Choose page to redirect if no city selected' ).'">';
		}

		public static function display_list_service_scripts() {
			echo '<textarea rows="5" cols="40" name="'.self::SIGNUP_FOOTER_SCRIPTS_OPTION.'">'.self::$footer_scripts.'</textarea>';
		}

		public static function show_registration_option( array $panes ) {

			$view = '<div align="center" class="mBottom20"><label><input type="checkbox" class="regular-text" name="'.self::REGISTRATION_OPTIN.'" '.self::$registration_option.'/>';

			$cookie = wg_get_preferred_location();
			if ( empty( $cookie ) ) {
				$locations = wg_get_locations( false );
				$no_city_text = get_option( self::SIGNUP_CITYNAME_OPTION );
				if ( !empty( $locations ) || !empty( $no_city_text ) ) {

					$view .= self::__( ' Notify me of future deals from' ).'</label>';
					$view .= '<span class="option registration_location_options_wrap">';
					$view .= ' <select name="deal_location" id="deal_location" size="1">';
					$preference = ( isset( $_COOKIE[ 'wg_location_preference' ] ) ) ? $_COOKIE[ 'wg_location_preference' ] : '' ;
					foreach ( $locations as $location ) {
						$view .= '<option value="'.$location->slug.'" '.selected( $preference, $location->slug ).'>'.$location->name.'</option>';
					}
					if ( !empty( $no_city_text ) ) {
						$view .= '<option value="notfound">'.esc_attr( $no_city_text ).'</option>';
					}
					$view .= '</select>';

					$view .= '</span>';
				}
			} else {
				$view .= self::__( ' Notify me of future deals and updates.' ).'</label></div>';
			}

			$panes['subscription'] = array(
				'weight' => 99,
				'body' => $view,
			);
			return $panes;

		}

		public static function display_custom_list_service() {
			echo '<textarea rows="5" cols="40" id="'.self::SUBSCRIPTION_FORM_CUSTOM.'" name="'.self::SUBSCRIPTION_FORM_CUSTOM.'">'.self::$subscription_custom_html.'</textarea>';
		}

		public abstract function get_subscription_method();

	}
	// subscription processors
	foreach ( glob(  get_template_directory() . '/wpg-framework/addons/subscription/list-services/*.class.php' ) as $file_path ) {
		require_once $file_path;
	}
	WP_Groupbuy_List_Services::init();

}
