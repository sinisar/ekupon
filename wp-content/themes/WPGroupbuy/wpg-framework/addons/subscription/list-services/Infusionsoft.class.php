<?php
class WP_Groupbuy_Infusionsoft extends WP_Groupbuy_List_Services {
	const APPNAME = 'wg_infusionsoft_name';
	const APPKEY = 'wg_infusionsoft_key';
	protected static $instance;
	private static $app_name = '';
	private static $app_key = '';
	private static $email = '';
	private static $location = '';

	protected static function get_instance() {
		if ( !( isset( self::$instance ) && is_a( self::$instance, __CLASS__ ) ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	public function get_subscription_method() {
		return self::SUBSCRIPTION_SERVICE;
	}

	protected function __construct() {
		parent::__construct();
		self::$app_name = get_option( self::APPNAME, '' );
		self::$app_key = get_option( self::APPKEY, '' );

		add_action( 'admin_init', array( $this, 'register_settings' ), 10, 0 );

		// Location meta
		add_action ( wg_get_location_tax_slug().'_edit_form_fields', array( get_class(), 'infusion_input_metabox' ), 10, 2 );
		add_action ( wg_get_location_tax_slug().'_add_form_fields', array( get_class(), 'infusion_input_metabox' ), 2 );
		add_action ( 'edited_terms', array( get_class(), 'save_infusion_meta_data' ) );

	}

	public static function register() {
		self::add_list_service( __CLASS__, self::__( 'Infusionsoft (beta)' ) );
	}
	public function process_subscription() {

		include 'utilities/infusion/xmlrpc-2.0/lib/xmlrpc.inc';
		$client = new xmlrpc_client( "https://".self::$app_name.".infusionsoft.com/api/xmlrpc" );
		$client->return_type = "phpvals";
		$client->setSSLVerifyPeer( FALSE );
		$contact = array(
			"Email" =>   $_POST['email_address'],
		);

		if ( WPG_DEV ) error_log( "key: " . print_r( self::$app_name, true ) );

		$call = new xmlrpcmsg( "ContactService.add", array(
				php_xmlrpc_encode( self::$app_key ),   //The encrypted API key
				php_xmlrpc_encode( $contact )  //The contact array
			) );

		if ( WPG_DEV ) error_log( "call: " . print_r( $call, true ) );

		$result = $client->send( $call );

		if ( !$result->faultCode() ) {

			$term = get_term_by( 'slug', $_POST['deal_location'], wg_get_location_tax_slug() );
			$groupID = get_metadata( 'location_terms', $term->term_id, 'infusion_group', TRUE );
			$conID = $result->value();

			$call = new xmlrpcmsg( "APIEmailService.optIn", array(
					php_xmlrpc_encode( self::$app_key ),   //The encrypted API key
					php_xmlrpc_encode( $_POST['email_address'] ),  //The contact ID
					php_xmlrpc_encode( 'API Opt In' ),  //The Group ID
				) );

			if ( WPG_DEV ) error_log( "opt call: " . print_r( $call, true ) );
			$result = $client->send( $call );
			if ( WPG_DEV ) error_log( "opt result: " . print_r( $result, true ) );

			$call = new xmlrpcmsg( "ContactService.addToGroup", array(
					php_xmlrpc_encode( self::$app_key ),   //The encrypted API key
					php_xmlrpc_encode( (int)$conID ),  //The contact ID
					php_xmlrpc_encode( (int)$groupID ),  //The Group ID
				) );

			if ( WPG_DEV ) error_log( "atg call: " . print_r( $call, true ) );
			$result = $client->send( $call );

			$call = new xmlrpcmsg( "ContactService.runActionSequence", array(
					php_xmlrpc_encode( self::$app_key ),     //The encrypted API key
					php_xmlrpc_encode( (int)$conID ),   //The contact ID
					php_xmlrpc_encode( (int)$groupID ),  //The Action ID
				) );

			if ( WPG_DEV ) error_log( "as call: " . print_r( $call, true ) );
			$result = $client->send( $call );

			parent::success( $_POST['deal_location'], $_POST['email_address'] );

		} else {
			WP_Groupbuy_Controller::set_message( $result->faultString() );
		}

		return;
	}

	public function process_registration_subscription( $user = null, $user_login = null, $user_email = null, $password = null, $post = null ) {
		if ( !$post[ parent::REGISTRATION_OPTIN ] )
			return;

		require_once 'utilities/infusion/isdk.php';
		$iSDK = new iSDK();

		//connect to the API
		if ( $iSDK->cfgCon( self::$app_name ) ) {

			$contact=array( 'Email' => $_POST['email_address'] );

			if ( !empty( $contact['Email'] ) ) {
				$returnFields = array( 'Id' );
				$dups = $iSDK->findByEmail( $contact['Email'], $returnFields );

				if ( !empty( $dups ) ) {
					$iSDK->updateCon( $dups[0]['Id'], $contact );
					$iSDK->runAS( $dups[0]['Id'], $actionId );
				} else {
					$newCon = $iSDK->addCon( $contact );
					$iSDK->runAS( $newCon, $actionId );
				}
				return true;
			}
			else {
				return;
			}
		} // connection end
		return;
	}

	public function register_settings() {
		$page = WP_Groupbuy_List_Services::get_settings_page();
		$section = 'wg_constantcontact_sub';
		add_settings_section( $section, self::__( 'Inufusionsoft Subscription' ), array( $this, 'display_settings_section' ), $page );
		register_setting( $page, self::APPNAME );
		register_setting( $page, self::APPKEY );
		add_settings_field( self::APPNAME, self::__( 'App Name' ), null, $page, $section );
		add_settings_field( self::APPNAME, self::__( 'App Name' ), array( $this, 'display_name_field' ), $page, $section );
		add_settings_field( self::APPKEY, self::__( 'App Key' ), array( $this, 'display_key_field' ), $page, $section );

		// Location meta
		add_action ( wg_get_location_tax_slug().'_edit_form_fields', array( get_class(), 'infusion_input_metabox' ), 10, 2 );
		add_action ( 'edited_terms', array( get_class(), 'save_infusion_meta_data' ) );
	}

	public static function display_name_field() {
		echo '<input type="text" name="'.self::APPNAME.'" value="'.self::$app_name.'" />';
	}

	public static function display_key_field() {
		echo '<input type="text" name="'.self::APPKEY.'" value="'.self::$app_key.'" />';
		echo '<br/><span class="description">'.self::__( 'How to find your API Key? <a href="http://ug.infusionsoft.com/article/AA-00442/0/How-do-I-enable-the-Infusionsoft-API-and-generate-an-API-Key.html" target="_blank">Click here</a>.' ).'</small>';
	}

	public static function infusion_input_metabox( $tag ) {
		$infusion_group = get_metadata( 'location_terms', $tag->term_id, 'infusion_group', TRUE );
?>
			</tbody>
		</table>
		<h3>Infusionsoft</h3>
		<table class="form-table">
			<tbody>
				<tr class="form-field">
					<th scope="row" valign="top"><label for="infusion_group"><?php _e( '	Infusionsoft Tag or Action ID' ) ?></label></th>
					<td><input type="text" size="40" value="<?php echo $infusion_group; ?>" id="infusion_group" name="infusion_group" /></td>
				</tr>
			</tbody>
		</table>
		<?php
	}

	public static function save_infusion_meta_data( $term_id ) {
		if ( isset( $_POST['infusion_group'] ) ) {
			$infusion_group = esc_attr( $_POST['infusion_group'] );
			update_metadata( 'location_terms', $term_id, 'infusion_group', $infusion_group );
		}
	}
}
WP_Groupbuy_Infusionsoft::register();
