<?php
$categories = array();
foreach ( $options as $term ) {
	$term_ob = get_term_by( 'slug', $term, wg_get_deal_location_tax() );
	$categories[] = $term_ob->name;
}
$option = implode( ", ", $categories ); ?>
<div id="mc_subscriptions" class="user_info clearfix">
	<p>
		<p><span class="contact_title"><?php wpg_e( 'Email Subscription: ' ); ?></span>
		<?php echo $option; ?>
	</p>
</div>
