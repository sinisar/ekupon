<fieldset id="wg-account-account-info">
	<legend class="mTop10"><?php wpg_e( 'Get the latest Deals' ); ?></legend>
	<table class="collapsable subscription form-table">
		<tbody>
				<tr>
					<td width="160" valign="top">
						<label><?php wpg_e( 'Select Locations' ) ?></label><br/>
					</td>
					<td>
						<?php
							$locations = wg_get_locations( FALSE );
							foreach ( $locations as $location ) {
								$checked = ( $optin == 'checked' || in_array( $location->slug, (array)$options ) ) ? 'checked="checked"' : '' ;
								echo '<span class="mRight20 twocol"><input id="'.$location->slug.'" type="checkbox" name="'.$name.'[]" value="'.$location->slug.'" '.$checked.'><label for="'.$location->slug.'">'.$location->name.'</label></span>';
							} ?>
					</td>
				</tr>
		</tbody>
	</table>
</fieldset>
