<?php

class WP_Groupbuy_None extends WP_Groupbuy_List_Services {
	protected static $instance;


	protected static function get_instance() {
		if ( !( isset( self::$instance ) && is_a( self::$instance, __CLASS__ ) ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	public function get_subscription_method() {
		return self::SUBSCRIPTION_SERVICE;
	}

	protected function __construct() {
		parent::__construct();
		add_action( 'wg_subscription_form', array( get_class(), 'wg_subscription_form' ), 10, 4 );
	}

	public static function register() {
		self::add_list_service( __CLASS__, self::__( 'None' ) );
	}

	public static function wg_subscription_form( $view, $show_locations, $select_location_text, $button_text ) {
?>
		<form action="" id="wg_subscription_form" method="post" class="clearfix">
			<fieldset>
			<?php
		$locations = wg_get_locations( false );
		$no_city_text = get_option( WP_Groupbuy_List_Services::SIGNUP_CITYNAME_OPTION );
		if ( ( !empty( $locations ) || !empty( $no_city_text ) ) && $show_locations ) {
?>
							<div class="text fl fixPNG"><strong><?php wpg_e( $select_location_text ); ?></strong></div>
							<?php
			global $wp_query;
			$query_slug = $wp_query->get_queried_object()->slug;
			$current_location = ( !empty( $query_slug ) ) ? $query_slug : $_COOKIE[ 'wg_location_preference' ] ;
			echo '<div class="bgSubscribeEmail fl"><select name="deal_location" id="deal_location" size="1">';
			foreach ( $locations as $location ) {
				echo '<option value="'.$location->slug.'" '.selected( $current_location, $location->slug ).'>'.$location->name.'</option>';
			}
			if ( !empty( $no_city_text ) ) {
				echo '<option value="notfound">'.esc_attr( $no_city_text ).'</option>';
			}
			echo '</select></div>';
?>
					<?php
		} ?>
			<?php wp_nonce_field( 'wg_subscription' );?>
			<input type="submit" class="bt_send_email" name="wg_subscription" id="wg_subscription" value="<?php wpg_e( $button_text ); ?>"/>
			</fieldset>
		</form>
		<?php
	}

	public function process_subscription() {
		parent::success( $_POST['deal_location'], null );
	}

	public function process_registration_subscription( $userId = null, $user_login = null, $user_email = null, $password = null, $post = null ) {
		return;
	}
}
WP_Groupbuy_None::register();
