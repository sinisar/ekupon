<?php

include("xmlrpc-2.0/lib/xmlrpc.inc");
$client = new xmlrpc_client("https://mach2.infusionsoft.com/api/xmlrpc");
$client->return_type = "phpvals";
$client->setSSLVerifyPeer(FALSE);

function addGrp($CID, $GID) {
	global $client;
	
	$key = get_option(WP_Groupbuy_Infusionsoft::APPKEY);
	
	$call = new xmlrpcmsg("ContactService.addToGroup", array(
		php_xmlrpc_encode($key), 		#The encrypted API key
		php_xmlrpc_encode($CID),		#The contact ID
		php_xmlrpc_encode($GID),		#The Group ID
	));

	$result = $client->send($call);

	if(!$result->faultCode()) {
		print "Contact added to group " . $GID;
		print "<BR>";
	} else {
		print $result->faultCode() . "<BR>";
		print $result->faultString() . "<BR>";
	}
}

function addCamp($CID, $CMP) {
	global $client;
	
	$key = get_option(WP_Groupbuy_Infusionsoft::APPKEY);
	
	
	$call = new xmlrpcmsg("ContactService.addToCampaign", array(
		php_xmlrpc_encode($key), 		#The encrypted API key
		php_xmlrpc_encode($CID),		#The contact ID
		php_xmlrpc_encode($CMP),		#The Campaign ID
	));
	$result = $client->send($call);

	if(!$result->faultCode()) {
		print "Contact added to Campaign " . $CMP;
		print "<BR>";
	} else {
		print $result->faultCode() . "<BR>";
		print $result->faultString() . "<BR>";
	}
}
