<?php

if (!function_exists('is_scalar')) {
    function is_scalar($val)
    {
        // Check input
        return (is_bool($val) || is_int($val) || is_float($val) || is_string($val));
    }
}

?>