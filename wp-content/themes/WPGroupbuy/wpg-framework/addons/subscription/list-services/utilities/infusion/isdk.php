<?php

include("xmlrpc-2.0/lib/xmlrpc.inc");

class iSDK {
	
// CONNECTOR
public function cfgCon($name,$dbOn="on") {
  $this->debug = $dbOn;
  $name = get_option(WP_Groupbuy_Infusionsoft::APPNAME);
  $key = get_option(WP_Groupbuy_Infusionsoft::APPKEY);
  if ($name) {
    $this->client = new xmlrpc_client("https://" . $name .
".infusionsoft.com/api/xmlrpc");
  }


  $this->client->return_type = "phpvals";

  $this->client->setSSLVerifyPeer(FALSE);
  $this->key = $key;

  if ($this->appEcho("connected?")) {
    return TRUE;
  } else { return FALSE; }

  return TRUE;
}

public function vendorCon($name,$user,$pass,$dbOn="on") {
  $this->debug = $dbOn;

  $name = get_option(WP_Groupbuy_Infusionsoft::APPNAME);
  $key = get_option(WP_Groupbuy_Infusionsoft::APPKEY);

  if ($name) {
    $this->client = new xmlrpc_client("https://" . $name .
".infusionsoft.com/api/xmlrpc");
  }

  $this->client->return_type = "phpvals";

  $this->client->setSSLVerifyPeer(FALSE);

  $this->key = $key;

  $carray = array(
    php_xmlrpc_encode($this->key),
    php_xmlrpc_encode($user),
    php_xmlrpc_encode(md5($pass)));

  $this->key = $this->methodCaller("DataService.getTemporaryKey",$carray);

  if ($this->appEcho("connected?")) {
    return TRUE;
  } else { return FALSE; }

  return TRUE;
}

public function appEcho($txt) {

    $carray = array(
                    php_xmlrpc_encode($txt));

    return $this->methodCaller("DataService.echo",$carray);
}

public function methodCaller($service,$callArray) {
    $call = new xmlrpcmsg($service, $callArray);
    $result = $this->client->send($call);
    if(!$result->faultCode()) {
      return $result->value();
    } else {
      if ($this->debug=="kill"){
        die("ERROR: " . $result->faultCode() . " - " .
$result->faultString());
      } elseif ($this->debug=="on") {
        return "ERROR: " . $result->faultCode() . " - " .
$result->faultString();
      } elseif ($this->debug=="off") {
      }
    }
}


// FILE  SERVICE
public function getFile($fileID) {

    $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode((int)$fileID));
    $result = $this->methodCaller("FileService.getFile",$carray);
    return $result;
}

public function uploadFile($fileName,$base64Enc,$cid=0) {
    $result = 0;
    if($cid==0) {
      $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode($fileName),
                    php_xmlrpc_encode($base64Enc));
      $result = $this->methodCaller("FileService.uploadFile",$carray);
    } else {
      $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode((int)$cid),
                    php_xmlrpc_encode($fileName),
                    php_xmlrpc_encode($base64Enc));
      $result = $this->methodCaller("FileService.uploadFile",$carray);
    }
    return $result;
}

public function replaceFile($fileID,$base64Enc) {
    $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode((int)$fileID),
                    php_xmlrpc_encode($base64Enc));
    $result = $this->methodCaller("FileService.replaceFile",$carray);
    return $result;
}

public function renameFile($fileID,$fileName) {
    $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode((int)$fileID),
                    php_xmlrpc_encode($fileName));
    $result = $this->methodCaller("FileService.renameFile",$carray);
    return $result;
}

public function getDownloadUrl($fileID) {
    $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode((int)$fileID));
    $result = $this->methodCaller("FileService.getDownloadUrl",$carray);
    return $result;
}


//CONTACT SERVICE
public function addCon($cMap, $optReason = "") {

    $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode($cMap,array('auto_dates')));
    $conID = $this->methodCaller("ContactService.add",$carray);
    if (!empty($cMap['Email'])) {
      if ($optReason == "") { $this->optIn($cMap['Email']); } else { $this->optIn($cMap['Email'],$optReason); }
    }
    return $conID;
}

public function updateCon($cid, $cMap) {

    $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode((int)$cid),
                    php_xmlrpc_encode($cMap,array('auto_dates')));
    return $this->methodCaller("ContactService.update",$carray);
}

public function findByEmail($eml, $fMap) {

    $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode($eml),
                    php_xmlrpc_encode($fMap));
    return $this->methodCaller("ContactService.findByEmail",$carray);
}

public function loadCon($cid, $rFields) {

    $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode((int)$cid),
                    php_xmlrpc_encode($rFields));
    return $this->methodCaller("ContactService.load",$carray);
}

public function grpAssign($cid, $gid) {

    $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode((int)$cid),
                    php_xmlrpc_encode((int)$gid));
    return $this->methodCaller("ContactService.addToGroup",$carray);
}

public function grpRemove($cid, $gid) {

    $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode((int)$cid),
                    php_xmlrpc_encode((int)$gid));
    return $this->methodCaller("ContactService.removeFromGroup",$carray);
}

public function campAssign($cid, $campId) {

    $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode((int)$cid),
                    php_xmlrpc_encode((int)$campId));
    return $this->methodCaller("ContactService.addToCampaign",$carray);
}

public function getNextCampaignStep($cid, $campId) {

    $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode((int)$cid),
                    php_xmlrpc_encode((int)$campId));
    return
$this->methodCaller("ContactService.getNextCampaignStep",$carray);
}

public function getCampaigneeStepDetails($cid, $stepId) {

    $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode((int)$cid),
                    php_xmlrpc_encode((int)$stepId));
    return
$this->methodCaller("ContactService.getCampaigneeStepDetails",$carray);
}

public function rescheduleCampaignStep($cidList, $campId) {

    $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode($cidList),
                    php_xmlrpc_encode((int)$campId));
    return
$this->methodCaller("ContactService.rescheduleCampaignStep",$carray);
}

public function campRemove($cid, $campId) {

    $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode((int)$cid),
                    php_xmlrpc_encode((int)$campId));
    return $this->methodCaller("ContactService.removeFromCampaign",$carray);
}

public function campPause($cid, $campId) {

    $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode((int)$cid),
                    php_xmlrpc_encode((int)$campId));
    return $this->methodCaller("ContactService.pauseCampaign",$carray);
}

public function runAS($cid, $aid) {

    $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode((int)$cid),
                    php_xmlrpc_encode((int)$aid));
    return $this->methodCaller("ContactService.runActionSequence",$carray);
}

//DATA SERVICE

public function dsGetSetting($module,$setting) {
    $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode($module),
                    php_xmlrpc_encode($setting));
    return $this->methodCaller("DataService.getAppSetting",$carray);
}


public function dsAdd($tName,$iMap) {
    $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode($tName),
                    php_xmlrpc_encode($iMap,array('auto_dates')));

    return $this->methodCaller("DataService.add",$carray);
}

public function dsDelete($tName,$id) {
    $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode($tName),
                    php_xmlrpc_encode((int)$id));

    return $this->methodCaller("DataService.delete",$carray);
}

public function dsUpdate($tName,$id,$iMap) {

    $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode($tName),
                    php_xmlrpc_encode((int)$id),
                    php_xmlrpc_encode($iMap, array('auto_dates')));

    return $this->methodCaller("DataService.update",$carray);
}

public function dsLoad($tName,$id,$rFields) {

    $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode($tName),
                    php_xmlrpc_encode((int)$id),
                    php_xmlrpc_encode($rFields));

    return $this->methodCaller("DataService.load",$carray);
}

public function dsFind($tName,$limit,$page,$field,$value,$rFields) {

    $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode($tName),
                    php_xmlrpc_encode((int)$limit),
                    php_xmlrpc_encode((int)$page),
                    php_xmlrpc_encode($field),
                    php_xmlrpc_encode($value),
                    php_xmlrpc_encode($rFields));

    return $this->methodCaller("DataService.findByField",$carray);
}

public function dsQuery($tName,$limit,$page,$query,$rFields) {

    $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode($tName),
                    php_xmlrpc_encode((int)$limit),
                    php_xmlrpc_encode((int)$page),
                    php_xmlrpc_encode($query,array('auto_dates')),
                    php_xmlrpc_encode($rFields));

    return $this->methodCaller("DataService.query",$carray);
}

public function addCustomField($context,$displayName,$dataType,$groupID) {

    $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode($context),
                    php_xmlrpc_encode($displayName),
                    php_xmlrpc_encode($dataType),
                    php_xmlrpc_encode((int)$groupID));

    return $this->methodCaller("DataService.addCustomField",$carray);
}

public function authenticateUser($userName,$password) {
    $password = strtolower(md5($password));
    $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode($userName),
                    php_xmlrpc_encode($password));

    return $this->methodCaller("DataService.authenticateUser",$carray);
}

public function updateCustomField($fieldId, $fieldValues) {

    $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode((int)$fieldId),
                    php_xmlrpc_encode($fieldValues));
    return $this->methodCaller("DataService.updateCustomField",$carray);
}

//INVOICE SERVICE

public function deleteInvoice($Id) {
    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$Id));
    return $this->methodCaller("InvoiceService.deleteInvoice",$carray);
}

public function deleteSubscription($Id) {
    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$Id));
    return $this->methodCaller("InvoiceService.deleteSubscription",$carray);
}

public function getPayments($Id) {
    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$Id));
    return $this->methodCaller("InvoiceService.getPayments",$carray);
}

public function setInvoiceSyncStatus($Id,$syncStatus) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$Id),
        php_xmlrpc_encode($syncStatus));
    return
$this->methodCaller("InvoiceService.setInvoiceSyncStatus",$carray);
}
public function setPaymentSyncStatus($Id,$Status) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$Id),
        php_xmlrpc_encode($Status));
    return
$this->methodCaller("InvoiceService.setPaymentSyncStatus",$carray);
}
public function getPluginStatus($className) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode($className));
    return $this->methodCaller("InvoiceService.getPluginStatus",$carray);
}
public function getAllShippingOptions() {
    $carray = array(
        php_xmlrpc_encode($this->key));
    return
$this->methodCaller("InvoiceService.getAllShippingOptions",$carray);
}
public function getAllPaymentOptions() {
    $carray = array(
        php_xmlrpc_encode($this->key));
    return
$this->methodCaller("InvoiceService.getAllPaymentOptions",$carray);
}

public function
manualPmt($invId,$amt,$payDate,$payType,$payDesc,$bypassComm) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$invId),
        php_xmlrpc_encode($amt),
        php_xmlrpc_encode($payDate,array('auto_dates')),
        php_xmlrpc_encode($payType),
        php_xmlrpc_encode($payDesc),
        php_xmlrpc_encode($bypassComm));
    return $this->methodCaller("InvoiceService.addManualPayment",$carray);
}

public function commOverride($invId,$affId,$prodId,$percentage,$amt,$payType,$desc,$date) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$invId),
          php_xmlrpc_encode((int)$affId),
        php_xmlrpc_encode((int)$prodId),
        php_xmlrpc_encode($percentage),
        php_xmlrpc_encode($amt),
        php_xmlrpc_encode($payType),
        php_xmlrpc_encode($desc),
        php_xmlrpc_encode($date,array('auto_dates')));

    return
$this->methodCaller("InvoiceService.addOrderCommissionOverride",$carray);
}

public function addOrderItem($ordId,$prodId,$type,$price,$qty,$Desc,$Notes)
{

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$ordId),
        php_xmlrpc_encode((int)$prodId),
        php_xmlrpc_encode($type),
        php_xmlrpc_encode($price),
        php_xmlrpc_encode($qty),
        php_xmlrpc_encode($Desc),
        php_xmlrpc_encode($notes));

    return $this->methodCaller("InvoiceService.addOrderItem",$carray);
}

public function payPlan($ordId,$aCharge,$ccId,$merchId,$retry,$retryAmt,$initialPmt,$initialPmtDate,$planStartDate,$numPmts,$pmtDays) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$ordId),
        php_xmlrpc_encode($aCharge),
        php_xmlrpc_encode((int)$ccId),
        php_xmlrpc_encode($merchId),
        php_xmlrpc_encode($retry),
        php_xmlrpc_encode($retryAmt),
        php_xmlrpc_encode($initialPmt),
        php_xmlrpc_encode($initialPmtDate,array('auto_dates')),
        php_xmlrpc_encode($planStartDate,array('auto_dates')),
        php_xmlrpc_encode($numPmts),
        php_xmlrpc_encode($pmtDays));

    return $this->methodCaller("InvoiceService.addPaymentPlan",$carray);
}

public function recurringCommOverride($recId,$affId,$amt,$payType,$desc) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$recId),
        php_xmlrpc_encode((int)$affId),
        php_xmlrpc_encode($amt),
        php_xmlrpc_encode($payType),
        php_xmlrpc_encode($desc));

    return
$this->methodCaller("InvoiceService.addRecurringCommissionOverride",$carray)
;
}

public function addRecurring($cid,$allowDup,$progId,$merchId,$ccId,$affId,$daysToCharge) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$cid),
        php_xmlrpc_encode($allowDup),
        php_xmlrpc_encode((int)$progId),
        php_xmlrpc_encode((int)$merchId),
        php_xmlrpc_encode((int)$ccId),
        php_xmlrpc_encode((int)$affId),
        php_xmlrpc_encode($daysToCharge));
    return $this->methodCaller("InvoiceService.addRecurringOrder",$carray);
}

public function addRecurringAdv($cid,$allowDup,$progId,$qty,$price,$allowTax,$merchId,$ccId,$affId,$daysToCharge) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$cid),
        php_xmlrpc_encode($allowDup),
        php_xmlrpc_encode((int)$progId),
        php_xmlrpc_encode($qty),
        php_xmlrpc_encode($price),
        php_xmlrpc_encode($allowTax),
        php_xmlrpc_encode($merchId),
        php_xmlrpc_encode((int)$ccId),
        php_xmlrpc_encode((int)$affId),
        php_xmlrpc_encode($daysToCharge));
    return $this->methodCaller("InvoiceService.addRecurringOrder",$carray);
}

public function amtOwed($invId) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$invId));

    return
$this->methodCaller("InvoiceService.calculateAmountOwed",$carray);
}

public function getInvoiceId($orderId) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$orderId));

    return $this->methodCaller("InvoiceService.getInvoiceId",$carray);
}

public function getOrderId($invoiceId) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$invoiceId));

    return $this->methodCaller("InvoiceService.getOrderId",$carray);
}

public function chargeInvoice($invId,$notes,$ccId,$merchId,$bypassComm) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$invId),
        php_xmlrpc_encode($notes),
        php_xmlrpc_encode((int)$ccId),
        php_xmlrpc_encode((int)$merchId),
        php_xmlrpc_encode($bypassComm));

    return $this->methodCaller("InvoiceService.chargeInvoice",$carray);
}

public function blankOrder($conId,$desc,$oDate,$leadAff,$saleAff) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$conId),
        php_xmlrpc_encode($desc),
        php_xmlrpc_encode($oDate,array('auto_dates')),
        php_xmlrpc_encode((int)$leadAff),
        php_xmlrpc_encode((int)$saleAff));

    return $this->methodCaller("InvoiceService.createBlankOrder",$carray);
}

public function recurringInvoice($rid) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$rid));

    return
$this->methodCaller("InvoiceService.createInvoiceForRecurring",$carray);
}

public function locateCard($cid,$last4) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$cid),
        php_xmlrpc_encode($last4));

    return $this->methodCaller("InvoiceService.locateExistingCard",$carray);
}

public function validateCard($ccId) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$ccId));

    return $this->methodCaller("InvoiceService.validateCreditCard",$carray);
}

public function updateSubscriptionNextBillDate($subscriptionId,$nextBillDate) {

    $carray = array(
                    php_xmlrpc_encode($this->key),
                    php_xmlrpc_encode((int)$subscriptionId),
                    php_xmlrpc_encode($nextBillDate,array('auto_dates')));

    return
$this->methodCaller("InvoiceService.updateJobRecurringNextBillDate",$carray)
;
}

// API EMAIL SERVICE

public function attachEmail($cId, $fromName, $fromAddress, $toAddress, $ccAddresses,
                            $bccAddresses, $contentType, $subject, $htmlBody, $txtBody,
                            $header, $strRecvdDate, $strSentDate,$emailSentType=1) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$cId),
        php_xmlrpc_encode($fromName),
        php_xmlrpc_encode($fromAddress),
        php_xmlrpc_encode($toAddress),
        php_xmlrpc_encode($ccAddresses),
        php_xmlrpc_encode($bccAddresses),
        php_xmlrpc_encode($contentType),
        php_xmlrpc_encode($subject),
        php_xmlrpc_encode($htmlBody),
        php_xmlrpc_encode($txtBody),
        php_xmlrpc_encode($header),
        php_xmlrpc_encode($strRecvdDate),
        php_xmlrpc_encode($strSentDate),
        php_xmlrpc_encode($emailSentType));

    return $this->methodCaller("APIEmailService.attachEmail",$carray);
}

public function sendEmail($conList, $fromAddress, $toAddress, $ccAddresses, $bccAddresses, $contentType, $subject, $htmlBody, $txtBody) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode($conList),
        php_xmlrpc_encode($fromAddress),
        php_xmlrpc_encode($toAddress),
        php_xmlrpc_encode($ccAddresses),
        php_xmlrpc_encode($bccAddresses),
        php_xmlrpc_encode($contentType),
        php_xmlrpc_encode($subject),
        php_xmlrpc_encode($htmlBody),
        php_xmlrpc_encode($txtBody));

    return $this->methodCaller("APIEmailService.sendEmail",$carray);
}


public function sendTemplate($conList, $template) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode($conList),
        php_xmlrpc_encode($template));

    return $this->methodCaller("APIEmailService.sendEmail",$carray);
}

public function createEmailTemplate($title, $userID, $fromAddress, $toAddress, $ccAddresses, $bccAddresses, $contentType, $subject, $htmlBody,
$txtBody) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode($title),
        php_xmlrpc_encode((int)$userID),
        php_xmlrpc_encode($fromAddress),
        php_xmlrpc_encode($toAddress),
        php_xmlrpc_encode($ccAddresses),
        php_xmlrpc_encode($bccAddresses),
        php_xmlrpc_encode($contentType),
        php_xmlrpc_encode($subject),
        php_xmlrpc_encode($htmlBody),
        php_xmlrpc_encode($txtBody));

    return
$this->methodCaller("APIEmailService.createEmailTemplate",$carray);
}

public function getEmailTemplate($templateId) {
  $carray = array(php_xmlrpc_encode($this->key),
php_xmlrpc_encode((int)$templateId));
  return $this->methodCaller("APIEmailService.getEmailTemplate",$carray);
}

public function updateEmailTemplate($templateID, $title, $categories, $fromAddress, $toAddress, $ccAddress, $bccAddress, $subject, $textBody, $htmlBody, $contentType, $mergeContext) {
  $carray = array(php_xmlrpc_encode($this->key),
                  php_xmlrpc_encode((int)$templateID),
                  php_xmlrpc_encode($title),
                  php_xmlrpc_encode($categories),
                  php_xmlrpc_encode($fromAddress),
                  php_xmlrpc_encode($toAddress),
                  php_xmlrpc_encode($ccAddress),
                  php_xmlrpc_encode($bccAddress),
                  php_xmlrpc_encode($subject),
                  php_xmlrpc_encode($textBody),
                  php_xmlrpc_encode($htmlBody),
                  php_xmlrpc_encode($contentType),
                  php_xmlrpc_encode($mergeContext));
  return $this->methodCaller("APIEmailService.updateEmailTemplate",$carray);
}

public function optStatus($email) {
    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode($email));
    return $this->methodCaller("APIEmailService.getOptStatus", $carray); }


public function optIn($email, $reason='API Opt In') {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode($email),
        php_xmlrpc_encode($reason));

    return $this->methodCaller("APIEmailService.optIn",$carray);
}

public function optOut($email, $reason='API Opt Out') {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode($email),
        php_xmlrpc_encode($reason));

    return $this->methodCaller("APIEmailService.optOut",$carray);
}

//AFFILIATE SYSTEM FUNCTIONS

public function affClawbacks($affId, $startDate, $endDate) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$affId),
        php_xmlrpc_encode($startDate),
        php_xmlrpc_encode($endDate));

    return $this->methodCaller("APIAffiliateService.affClawbacks",$carray);
}

public function affCommissions($affId, $startDate, $endDate) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$affId),
        php_xmlrpc_encode($startDate,array('auto_dates')),
        php_xmlrpc_encode($endDate,array('auto_dates')));

    return
$this->methodCaller("APIAffiliateService.affCommissions",$carray);
}

public function affPayouts($affId, $startDate, $endDate) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$affId),
        php_xmlrpc_encode($startDate),
        php_xmlrpc_encode($endDate));

    return $this->methodCaller("APIAffiliateService.affPayouts",$carray);
}

public function affRunningTotals($affList) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode($affList));

    return
$this->methodCaller("APIAffiliateService.affRunningTotals",$carray);
}

public function affSummary($affList, $startDate, $endDate) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode($affList),
        php_xmlrpc_encode($startDate),
        php_xmlrpc_encode($endDate));

    return $this->methodCaller("APIAffiliateService.affSummary",$carray);
}

// TICKET SYSTEM FUNCTIONS

public function addMoveNotes($ticketList, $moveNotes, $moveToStageId,
$notifyIds) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode($ticketList),
        php_xmlrpc_encode($moveNotes),
        php_xmlrpc_encode($moveToStageId),
        php_xmlrpc_encode($notifyIds));

    return $this->methodCaller("ServiceCallService.addMoveNotes",$carray);
}

public function moveTicketStage($ticketID, $ticketStage, $moveNotes,
$notifyIds) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$ticketID),
        php_xmlrpc_encode($ticketStage),
        php_xmlrpc_encode($moveNotes),
        php_xmlrpc_encode($notifyIds));

    return
$this->methodCaller("ServiceCallService.moveTicketStage",$carray);
}

//ADDITIONAL public functions

public function infuDate($dateStr) {
    $dArray=date_parse($dateStr);
    if ($dArray['error_count']<1) {
        $tStamp =
mktime($dArray['hour'],$dArray['minute'],$dArray['second'],$dArray['month'],
$dArray['day'],$dArray['year']);
        return date('Ymd\TH:i:s',$tStamp);
    } else {
        foreach ($dArray['errors'] as $err) {
            echo "ERROR: " . $err . "<br />";
        }
        die("The above errors prevented the application from executing properly.");
    }
}

// Search Service public functions

public function savedSearchAllFields($savedSearchId, $userId, $page) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$savedSearchId),
        php_xmlrpc_encode((int)$userId),
        php_xmlrpc_encode((int)$page));

    return
$this->methodCaller("SearchService.getSavedSearchResultsAllFields",$carray);
}

public function savedSearch($savedSearchId, $userId, $page, $fields) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$savedSearchId),
        php_xmlrpc_encode((int)$userId),
        php_xmlrpc_encode((int)$page),
        php_xmlrpc_encode($fields));

    return
$this->methodCaller("SearchService.getSavedSearchResults",$carray);
}

public function getAvailableFields($savedSearchId, $userId) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$savedSearchId),
        php_xmlrpc_encode((int)$userId));

    return $this->methodCaller("SearchService.getAllReportColumns",$carray);
}

public function getDefaultQuickSearch($userId) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$userId));

    return
$this->methodCaller("SearchService.getDefaultQuickSearch",$carray);
}

public function getQuickSearches($userId) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode((int)$userId));

    return
$this->methodCaller("SearchService.getAvailableQuickSearches",$carray);
}

public function quickSearch($quickSearchType, $userId, $filterData, $page,
$limit) {

    $carray = array(
        php_xmlrpc_encode($this->key),
        php_xmlrpc_encode($quickSearchType),
        php_xmlrpc_encode((int)$userId),
        php_xmlrpc_encode($filterData),
        php_xmlrpc_encode((int)$page),
        php_xmlrpc_encode((int)$limit));

    return $this->methodCaller("SearchService.quickSearch",$carray);
}
}
?>