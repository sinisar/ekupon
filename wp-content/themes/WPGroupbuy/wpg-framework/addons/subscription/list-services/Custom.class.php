<?php

class WP_Groupbuy_Custom extends WP_Groupbuy_List_Services {
	protected static $instance;


	protected static function get_instance() {
		if ( !( isset( self::$instance ) && is_a( self::$instance, __CLASS__ ) ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	public function get_subscription_method() {
		return self::SUBSCRIPTION_SERVICE;
	}

	protected function __construct() {
		parent::__construct();
		add_action( 'wg_subscription_form', array( get_class(), 'wg_subscription_form' ), 10, 4 );
	}

	public static function register() {
		self::add_list_service( __CLASS__, self::__( 'Custom Form' ) );
	}

	public static function wg_subscription_form( $view, $show_locations, $select_location_text, $button_text ) {
		$view = get_option( WP_Groupbuy_List_Services::SUBSCRIPTION_FORM_CUSTOM );
		return $view;
	}

	public function process_subscription() {
		if ( isset( $_POST['deal_location'] ) ) {
			parent::success( $_POST['deal_location'], null );
		}

	}

	public function process_registration_subscription( $userId = null, $user_login = null, $user_email = null, $password = null, $post = null ) {
		return;
	}
}
WP_Groupbuy_Custom::register();
