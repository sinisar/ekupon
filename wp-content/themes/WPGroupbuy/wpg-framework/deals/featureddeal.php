    <div class="main-boxcont">
            <div class="cont-maindear" id="featured_deal_<?php the_ID() ?>">
              <div class="mainimg">
                <div class="v6ItemHomeHover hidden"> <div class="v6ItemHotHoverText" style="text-align: center;"><a class="v6ImageHover" href = '<?php the_permalink() ?>'>
                  
                  </a></div>
                  <div class="v6ItemHomeHoverButton">
                    <div style="text-align: center;"> <a href="<?php the_permalink() ?>" class="mLeft5 v6BtnDetail fixPNG"><img title="<?php wpg_e('Read'); ?> <?php the_title() ?>" class="fixPNG" src="<?php echo get_template_directory_uri(); ?>/style/images/blank.gif" width="128" height="35" alt="" /></a> </div>
                  </div>
                </div>
                <a href="<?php the_permalink() ?>" class="setDealWh">
                  <span class="v7price_promotion fixPNG mLeft400 mTop10"><?php wg_amount_saved() ?></span>
                  <?php if ( has_post_thumbnail() ): ?>
                  <?php the_post_thumbnail('wpg_475x475') ?>
                  <?php else : ?>
                  <img class="no_img 475" src="<?php echo get_template_directory_uri(); ?>/style/images/background/no_img_475.png" alt="" />
                  <?php endif; ?>
                </a> 
              </div>
              <div class="right-contdear">
                <div class="title-maindear">
                  <div class="v6TitleShort">
                    <div class="fl">
                    <h2><a href="<?php the_permalink() ?>" title="<?php the_title() ?>" ><?php the_title() ?></a></h2>
                    </div>
                    <div class="c"></div>
                  </div>
                </div>
                <div class="boxPrices">
                  <div class="boxPrices_view fixPNG">
                    <div class="v6Price mTop10" style="text-align: center;"> <?php wg_price(); ?> </div>
                    <div class="pricebuys v7inlinetype" style="text-align: center;"><?php ?> <span class="noline"><?php $max = wg_get_max_purchases(); $bought = wg_get_number_of_purchases(); $remain = $max - $bought; printf( wpg__( 'Only %s items left to buy' ), $remain ); //echo __('Only %s items left to buy', $remain) ?></span> </div>
                    
                    <!-- BUYNOW BUTTON -->
                    <div class="v6BuyNow" >                    
                    <?php if ( wg_is_sold_out() ) : ?>  
            <a href="#"><div class="retinaiconbox soldout_btn"><span class="retinaicon-shopping-cart"></span> <span class="buynow_text"><?php wpg_e('Sold out') ?></span></div></a>
          <?php elseif ( wg_deal_availability() && !wg_is_deal_complete() ): ?>
                    	<a href="<?php wg_add_to_cart_url() ?>"><div class="retinaiconbox buynow_btn"><span class="retinaicon-shopping-cart"></span> <span class="buynow_text"><?php wpg_e('Buy now') ?></span></div></a>
					<?php else : ?>	
						<a href="#"><div class="retinaiconbox soldout_btn"><span class="retinaicon-shopping-cart"></span> <span class="buynow_text"><?php wpg_e('Unavailable') ?></span></div></a>
					<?php endif ?>
                    </div>
                    <!-- END BUYNOW BUTTON -->
                  </div>
                </div>
                
                <div class="shopinfo">
                <div class="shopBuy" style="text-align: center;">
                  <div class="shopTitleInfo">
                    <?php wpg_e('Value') ?>
                  </div>
                  <div class="shopMoreInfo">
                    <?php echo str_replace('.00','',wg_get_formatted_money(wg_get_deal_worth())) ?>
                  </div>
                </div>
                <div class="shoppromotion" style="text-align: center;">
                  <div class="shopTitleInfo">
                    <?php wpg_e('Discount') ?>
                  </div>
                  <div class="shopMoreInfo">
				  <?php wg_amount_saved() ?></div>
                </div>
                <div class="shoptime" style="text-align: center;">
                  <div class="shopTitleInfo shopTitleTimeInfo">
                    <?php wpg_e('You save') ?>
                  </div>
                  <div class="shopMoreInfo">
                    <?php echo str_replace('.00','',wg_get_formatted_money(wg_get_deal_worth() - wg_get_price())) ?>
                  </div>
                </div>
              </div>
                
                <div class="v6BorderBot" >
                  <div class="v6BottomPrice">
                    <div class="v6Buyersnew mTop10 "> <b><?php wg_number_of_purchases() ?></b> <?php wpg_e('bought already!') ?>
                      <div class="mainBlueBoxNew pTop10">
                        
                        <?php wg_get_purchases_by_account() ?>
                          
                      </div>
                    </div>
                  </div>
                </div>
                <?php if(!wg_get_expiration_date($post->ID)){ ?>
                <div class="v6BorderBot pTop5 border_bt_none">
                  <div class="v6Timer">
                    <div class="v6Gray fl"></div>
                    <div class="v6DisplayTime"> <?php wg_deal_countdown( true ) ?> </div>
                    <div class="c"></div>
                  </div>
                </div>
                <?php } ?>
              </div>
              <div class="c"></div>
              <div class="bottomTopDeal">
                <div class="fl">
                  <div class="v6ItemHomeHoverTextD">
                    <div class="pageView fl fixPNG"> <?php if(function_exists('the_views')) { the_views(); } ?> </div>
                    <div class="c"></div>
                  </div>
                </div>
                <div class="fr">
                  <div class="v6Fb">
                    <div class="ovhide">
                    <?php get_template_part('wpg-framework/inc/social-share') ?>
                    </div>
                  </div>
                </div>
                <div class="c"></div>
              </div>
            </div>
          </div>