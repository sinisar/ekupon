<!-- DEALS BLOCK -->
<div id="<?php the_ID() ?>" class="weekly <?php evenodd(); ?>" align="center">
  <div class="homecate_c">
    <div id="scrollbox16 zi10">
      <div class="v6TitleCate" id="dockCat16">
        <h2 class="fl"><?php wg_deal_categories(); ?></h2>
        <div class="fl link"> <a href="#" class="fl v7linkcate"><?php wpg_e('View more') ?></a> </div>
        <div class="c"></div>
      </div>
      <div class="c"></div>
    </div>
    
    <div class="v6CatePage">
      <div id="jCarouselNew16" class="jCarouselNew"> 
        
        <!-- UL LIST -->
        <ul class="listHomeCate">
          
          <!-- DEAL ITEM -->
          <li class="v6Item">
            <div class="v6ItemCon">
              <div class="v6ItemContent">
                <div class="v6ItemImg">
                  <a href="<?php the_permalink() ?>" >
                  <div class="v7price_promotionsmall"><?php wg_amount_saved() ?></div>
                  	<?php if ( has_post_thumbnail() ): ?>
					<?php the_post_thumbnail('wpg_210x158') ?>
                    <?php else : ?>
                    <div class="no_img 210"><img src="<?php echo get_template_directory_uri();?>/style/images/background/no_img_210.png" /></div>
                    <?php endif; ?>
                 </a>
                  <div class="v7InfoType v7BottomBuy">
                    <div class="c"></div>
                  </div>
                </div>
                <div class="v6ItemTitle" align="left">
                  <div><a href="<?php the_permalink() ?>" title="<?php wpg_e('Visit') ?> <?php the_title() ?>"><?php the_title() ?></a> </div>
                </div>
                <div class="v6ItemPrice">
                  <div class="fl pTop5 v7price">
                    <div class="v7PromotionSmall"><?php wg_price(); ?></div>
                    <div class="v7bnew f12"><?php wpg_e('Bought:') ?> <b><?php wg_number_of_purchases() ?></b></div>
                  </div>
                  <div class="v7typeproduct fl v7proTypeSmall c555"></div>
                  <a href="<?php the_permalink() ?>" class="v7viewSmall fr fixPNG"><img src="<?php echo get_template_directory_uri(); ?>/style/images/blank.gif" width="39" height="30" alt="" /></a>
                  <div class="c"></div>
                </div>
              </div>
            </div>
          </li>
          <!-- DEAL ITEM -->
        </ul>
        <!-- END UL LIST -->
        <div class="c"></div>
      </div>
    </div>
  </div>
</div>
<div class="c"></div>
</div>
<!-- END DEALS BLOCK -->