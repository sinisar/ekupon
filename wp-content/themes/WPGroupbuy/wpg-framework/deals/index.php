<?php
	do_action('wg_deals_view');
	get_header();
?>

<div class="main home_ver7">
  <div class="main-content takeover">
      <!-- HOMEPAGE BANNER -->
      <?php dynamic_sidebar( 'homepage-banner' ); ?>
      <!-- HOMEPAGE BANNER-->
    <div class="v6ContentCat">
      <div>
        <div class="main-width v7Soldbg ">
          <div class="v6TitleCat fl">
            <h2 class="fontNormal">
              <?php wg_deals_index_title() ?>
            </h2>
          </div>
          <div class="c"></div>
          <?php if ( ! have_posts() ) : ?>
          <?php get_template_part( 'wpg-framework/deals/no-deals', 'wpg-framework/deals/index' ); ?>
          <?php endif; ?>
          <?php while ( have_posts() ) : the_post(); ?>
          <?php get_template_part( 'wpg-framework/inc/loop-item', 'wpg-framework/inc/deal-item' ); ?>
          <?php endwhile; ?>
          <?php global $wp_query; if ( $wp_query->max_num_pages > 1 ) : ?>
          <?php get_template_part( 'wpg-framework/inc/loop-nav', 'wpg-framework/inc/index-nav' ); ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php // do_action('wg_above_default_sidebar') ?>
<?php // dynamic_sidebar( 'deals-sidebar' );?>
<?php // do_action('wg_below_default_sidebar') ?>
<?php get_footer(); ?>
