<?php
$checkout = WP_Groupbuy_Checkouts::get_instance();
$current_page = $checkout->get_current_page();
$guest_checkout = get_option( 'wg_guest_checkout' );
?>
</form>
<div class="hr hr2"></div>

<div class="tabset">
<ul class="tabs">
  <li class="tab"><a id="member_tab" href="#member-checkout" class="selected">
    <?php wpg_e('Member Checkout'); ?>
    </a></li>

  <?php if ( $guest_checkout ): ?>
  <li class="tab"><a id="guest_tab" href="#guest-checkout">
    <?php wpg_e('Guest Checkout'); ?>
    </a></li>
  <?php endif; ?>

</ul>
<div class="panel dsp_block" id="member-checkout">
  <div id="checkout_login_register_forms">
    <div class="one_half"> 
      <form class="wg_checkout_<?php echo $current_page; ?>" action="<?php wg_checkout_url(); ?>" method="post">
        <input type="hidden" name="wg_checkout_action" value="<?php echo $current_page; ?>" />
        <input type="hidden" name="wg_account_action" value="wg_account_register" />
        <input type="hidden" name="wg_login_or_register" value="1" />
        <?php print $args['registration_form']; ?> 
      </form>
    </div>
    <div class="one_half last">
      <form class="wg_checkout_<?php echo $current_page; ?>" action="<?php wg_checkout_url(); ?>" method="post">
        <input type="hidden" name="wg_checkout_action" value="<?php echo $current_page; ?>" />
        <input type="hidden" name="wg_account_action" value="wg_account_register" />
        <input type="hidden" name="wg_login_or_register" value="1" />
        <input type="hidden" name="wg_user_guest_purchase" id="wg_user_guest_purchase"  value="0"  />
        <?php print $args['login_form']; ?>
      </form>
    </div>
  </div>
  <div class="c"></div>
</div>

<?php if ( $guest_checkout ): ?>
<div class="panel dsp_block" id="guest-checkout">
  <div id="checkout_show">
    <form class="wg_checkout_<?php echo $current_page; ?>" action="<?php wg_checkout_url(); ?>" method="post">
      <input type="hidden" name="wg_checkout_action" value="<?php echo $current_page; ?>" />
      <input type="hidden" name="wg_login_or_register" value="1" />
      <input type="hidden" name="wg_user_guest_purchase" id="wg_user_guest_purchase"  value="1"  />
<?php endif; ?>