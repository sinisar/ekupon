<div class="mt38"></div>
	
	<fieldset id="wg-shipping">
    <legend><?php wpg_e('Shipping Information'); ?></legend>
		<table class="shipping">
			<tbody>

				<?php foreach ( $fields as $key => $data ): ?>
					<tr>
						<?php if ( $data['type'] != 'checkbox' ): ?>
							<td class="edit-td"><?php wg_form_label($key, $data, 'shipping'); ?></td>
							<td><?php wg_form_field($key, $data, 'shipping'); ?></td>
						<?php else: ?>
							<td colspan="2">
								<label for="wg_shipping_<?php echo $key; ?>"><?php wg_form_field($key, $data, 'shipping'); ?> <?php echo $data['label']; ?></label>
							</td>
						<?php endif; ?>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</fieldset>
