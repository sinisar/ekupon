<div class="mt38"></div>
    <script type="text/javascript">
        jQuery(function($) {
            $('#wg_gifting_send_gift_to_me').on('click', function () {
                if (this.checked) {
                    $('#recipient_label').addClass('hidden');
                    $('#recipient_input').addClass('hidden');
                }
                else {

                    $('#recipient_label').removeClass('hidden');
                    $('#recipient_input').removeClass('hidden');
                }
            });
        });
    </script>
	<fieldset id="wg_billing">
    <legend><?php wpg_e('Gift Options'); ?></legend>
		<table class="billing">
			<tbody>
				<?php foreach ( $fields as $key => $data ): ?>
					<td>
						<?php if ( $data['type'] != 'checkbox' ): ?>
                            <?php if ($key == "recipient"): ?>
                                <td class="edit-td">
                                    <div id="recipient_label">
                                        <?php wg_form_label($key, $data, 'gifting'); ?>
                                    </div>
                                </td>
                                <td>
                                    <div id="recipient_input">
                                        <?php wg_form_field($key, $data, 'gifting'); ?>
                                    </div>
                                </td>
                            <?php else: ?>
                                <td class="edit-td"><?php wg_form_label($key, $data, 'gifting'); ?></td>
                                <td><?php wg_form_field($key, $data, 'gifting'); ?></td>
                            <?php endif; ?>
						<?php else: ?>
							<td colspan="2">
                                <label for="wg_gifting_<?php echo $key; ?>"><?php wg_form_field($key, $data, 'gifting'); ?> <?php echo $data['label']; ?></label>
							</td>
						<?php endif; ?>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</fieldset>

