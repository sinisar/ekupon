<div class="mt38"></div>
	<fieldset id="wg_billing">
    <legend><?php wpg_e('Billing Information'); ?></legend>
		<table class="billing">
			<tbody>

				<?php foreach ( $fields as $key => $data ): ?>
					<tr>
						<?php if ( $data['type'] != 'checkbox' ): ?>
							<td class="edit-td"><?php wg_form_label($key, $data, 'billing'); ?></td>
							<td><?php wg_form_field($key, $data, 'billing'); ?></td>
						<?php else: ?>
							<td colspan="2">
								<label for="wg_billing_<?php echo $key; ?>"><?php wg_form_field($key, $data, 'billing'); ?> <?php echo $data['label']; ?></label>
							</td>
						<?php endif; ?>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</fieldset>
