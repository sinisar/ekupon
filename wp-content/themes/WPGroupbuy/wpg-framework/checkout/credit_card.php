<?php
	// Check account balances for (account) balance and affiliate (credits).
	$balance = wg_get_account_balance( get_current_user_id(), 'balance' );
	$reward_points = wg_get_account_balance( get_current_user_id(), 'points' );
	$cart = WP_Groupbuy_Cart::get_instance();
?>
                
<div class="mt38"></div>
	<?php 
	$payment_processor = WP_Groupbuy_Payment_Processors::get_payment_processor();
	$payment_class = get_class( $payment_processor );
	$payment_name = $payment_processor->get_payment_method();
	$single_method = ( count( $fields['payment_method'] ) < 2 );
	if (($fields != null) || (is_a($payment_processor, 'WP_Groupbuy_Offsite_Manual_Purchasing'))) {
	?>
		<fieldset id="wg-credit-card">
    	<legend><?php wpg_e('Payment Information'); ?></legend>
		<table>
			<tbody>
				<!-- Payment method -->

				<?php foreach ( $fields as $key => $data ): ?>
					<?php if ( $data['weight'] < 1 && strpos( $key, 'cc_' ) == false ) : ?>
					<tr>
						<?php if ( $data['type'] != 'checkbox' ): ?>
							<td class="edit-td"><?php wg_form_label($key, $data, 'credit'); ?></td>
							<td><?php wg_form_field($key, $data, 'credit'); ?>
                            </td>
						<?php else: ?>
							<td colspan="2">
								<label for="wg_credit_<?php echo $key; ?>"><?php wg_form_field($key, $data, 'credit'); ?> <?php echo $data['label']; ?></label>
							</td>
						<?php endif; ?>
					</tr>
					<?php
						unset( $fields[$key] );
					endif; ?>
				<?php endforeach; ?>

				<?php if ( ( $balance || $reward_points ) && $cart->get_total() ) : // if there is balance or reward point then add reward, balance method ?> 
					<?php if ( $single_method ) : ?>
					<tr>
						<td></td>
						<td>
							<span class="wg-form-field-radio">
								<label for="wg_credit_payment_method_<?php echo $payment_class ?>">
									<input type="radio" name="wg_credit_payment_method" id="wg_credit_payment_method_<?php echo $payment_class ?>" value="credit" checked> <?php echo $payment_name ?>
								</label>
							</span>
						</td>
					</tr>
					<?php endif; ?>
					<tr>
						<td></td>
						<td>
							<span class="wg-form-field-radio">
								<label for="wg_credit_payment_method_WP_Groupbuy_Credit_Balance">
									<input type="radio" name="wg_credit_payment_method" id="wg_credit_payment_method_WP_Groupbuy_Credit_Balance" value="WP_Groupbuy_Credit_Balance"> <?php wpg_e('Rewards Point/Account Balance'); ?>
								</label>
							</span>
						</td>
					</tr>
				<?php endif;?>

				<!-- End Payment method -->

                <!-- Credit Card -->
				<?php if ( $fields['cc_name'] ): ?>
					<tr class="wg_credit_card_field_wrap">
						<td><?php wg_form_label('cc_name', $fields['cc_name'], 'credit'); ?></td>
						<td><?php wg_form_field('cc_name', $fields['cc_name'], 'credit'); ?></td>
					</tr>
				<?php
					unset( $fields['cc_name'] );
				endif; ?>
				<?php if ( $fields['cc_number'] ): ?>
					<tr class="wg_credit_card_field_wrap">
						<td valign="top"><?php wg_form_label('cc_number', $fields['cc_number'], 'credit'); ?></td>
						<td valign="middle"><?php wg_form_field('cc_number', $fields['cc_number'], 'credit'); ?> <span id="card_image"></span></td>
					</tr>
				<?php 
					unset( $fields['cc_number'] );
				endif; ?>
				<?php if ( $fields['cc_expiration_month'] && $fields['cc_expiration_year'] ): ?>
					<tr class="wg_credit_card_field_wrap">
						<td><?php wg_form_label('cc_expiration_year', $fields['cc_expiration_year'], 'credit'); ?></td>
						<td><?php wg_form_field('cc_expiration_month', $fields['cc_expiration_month'], 'credit'); ?> <?php wg_form_field('cc_expiration_year', $fields['cc_expiration_year'], 'credit'); ?></td>
					</tr>
				<?php 
					unset( $fields['cc_expiration_month'] );
					unset( $fields['cc_expiration_year'] );
				endif; ?>
				<?php if ( $fields['cc_cvv'] ): ?>
					<tr class="wg_credit_card_field_wrap">
						<td><?php wg_form_label('cc_cvv', $fields['cc_cvv'], 'credit'); ?></td>
						<td><?php wg_form_field('cc_cvv', $fields['cc_cvv'], 'credit'); ?></td>
					</tr>
				<?php
					unset( $fields['cc_cvv'] );
				endif; ?>
                <!-- Credit Card -->
                
                <!-- Credit & Balance -->
				<?php foreach ( $fields as $key => $data ): ?>
                    <?php $is_account_credit = in_array( $key, array ('account_balance', 'affiliate_credits' ) ); ?>
					<tr class="<?php echo $is_account_credit ? 'reward_balance' : 'wg_credit_card_field_wrap' ?>">
						<?php if ( $data['type'] != 'checkbox' ): ?>
							<td valign="top"><?php wg_form_label($key, $data, 'credit'); ?></td>
							<td valign="middle"><?php wg_form_field($key, $data, 'credit'); ?></td>
						<?php else: ?>
							<td colspan="2">
								<label for="wg_credit_<?php echo $key; ?>"><?php wg_form_field($key, $data, 'credit'); ?> <?php echo $data['label']; ?></label>
							</td>
						<?php endif; ?>
					</tr>
				<?php endforeach; ?>
                <!-- End Credit & Balance -->
             
                <!-- Offline method -->
                <tr class="payment_guides">
					<td></td>
					<td><?php							
							do_action('show_payment_guide');
                        ?>
                    </td>
					</tr>
                <!-- End Offline method -->
			</tbody>
		</table>
	</fieldset>
	<?php } ?>
<script>
	jQuery(function($) {
		
		$payment_methods = jQuery('input:radio[name=wg_credit_payment_method]');

		function toggleFields(input) {
			var $cc_option_value = jQuery(input).val();
			if ( $cc_option_value !== 'credit') {
				jQuery('.wg_credit_card_field_wrap').hide().find('input').attr('required', false);
			}
			else {
				jQuery('.wg_credit_card_field_wrap').show('fast').find('input').attr('required', true);
			};

			if ( $cc_option_value !== 'WP_Groupbuy_Credit_Balance') {
				jQuery('tr.reward_balance').hide();
			}
			else {
				jQuery('tr.reward_balance').show('fast');
			};
			$('.payment_guide').hide();
			$("#" + $cc_option_value + '.payment_guide' ).show('fast');
		}

		if ( $payment_methods.length > 1 ) {
			toggleFields(jQuery('input:radio[name=wg_credit_payment_method]:checked'));
		}

		jQuery('input:radio[name=wg_credit_payment_method]').change( function() {
			toggleFields(this) }
		);
	});
</script>
	