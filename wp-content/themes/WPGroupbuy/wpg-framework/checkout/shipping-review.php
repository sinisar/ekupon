<div id="wpg_checkout" class="mt38 mBottom20">
	<fieldset id="wg-shipping">
    <legend><?php wpg_e('Shipping Information'); ?></legend>
		<table>
			<tbody>
				<tr>
					<th scope="row"><?php wpg_e('Name'); ?></th>
					<td><?php esc_html_e($data['first_name']); ?> <?php esc_html_e($data['last_name']); ?></td>
				</tr>
				<tr>
					<th scope="row"><?php wpg_e('Address'); ?></th>
					<td>
						<?php if ( $data['street'] ) { echo $data['street'].'<br />'; } ?>
						<?php if ( $data['city'] ) { echo $data['city'].','; } ?>
						<?php echo $data['zone'].' '; ?>
						<?php echo $data['postal_code'].' '; ?>
						<?php if ( $data['country'] ) { echo '<br />'.$data['country']; } ?>
					</td>
				</tr>
			</tbody>
		</table>
	</fieldset>
    </div>