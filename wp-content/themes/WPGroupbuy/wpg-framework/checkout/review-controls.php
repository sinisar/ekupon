<p align="center">
  <input type="checkbox" name="terms" id="terms" value="true" />
  <label for="terms" class="agree_terms"><?php printf(__('Strinjam se s <a target="_blank" href="%s" title="Splošni pogoji uporabe">splošnimi pogoji uporabe</a> in <a href="%s" target="_blank" title="Zaščita osebnih podatkov">Zaščito osebnih podatkov</a>.', 'wpgroupbuy'), get_permalink(TOS_PAGEID ), get_permalink(PP_PAGEID ) ); ?></label>
</p>
<div id="wpg_checkout" class="mTop10 mBottom10" align="center">
  <input class="profileAddGoldV6" type="submit" value="<?php wpg_e( 'Submit Order' ); ?>" name="wpg_checkout_button" onclick="if(!this.form.terms.checked){jQuery('.agree_terms').attr('style', 'color:red;');return false}" />
</div>
