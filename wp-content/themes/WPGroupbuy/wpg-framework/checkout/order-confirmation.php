<div class="mBottom20">
    <h3>
        Vaše naročilo je uspešno zaključeno, v primeru plačila z uporabo UPN naloga,
        ste na vaš e-poštni naslov prejeli obvestilo z nadaljnimi informacijami za plačilo.
    </h3>
</div>
<table class="listingOrder border_1_ddd border_bt_none border_t_none border_r_none" cellspacing="0" cellpadding="0" width="100%">
	<thead>
		<tr>
			<th scope="col" colspan="2" class="headProfile noneborderbot bRight "><?php wpg_e('Your Order Summary'); ?></th>
			<th scope="col" colspan="1" class="headProfile noneborderbot bRight "><?php wpg_e('Quantity'); ?></th>
			<th scope="col" colspan="1" class="headProfile noneborderbot bRight "><?php wpg_e('Price'); ?></th>
		</tr>
	</thead>
	<tfoot>
		<?php if ( $shipping > 0 ): ?>
			<tr class="">
				<th scope="row" colspan="3" class="itemProfile bRight"><?php wpg_e('Shipping'); ?></th>
				<td class="itemProfile bRight">
					<?php wg_formatted_money($shipping); ?>
				</td>
			</tr>
		<?php endif ?>
		<?php if ( $tax > 0 ): ?>
			<tr class="">
				<th scope="row" colspan="3" class="itemProfile bRight"><?php wpg_e('Tax'); ?></th>
				<td class="itemProfile bRight">
					<?php wg_formatted_money($tax); ?>
				</td>
			</tr>
		<?php endif ?>
		<tr class="">
			<th scope="row" colspan="3" class="itemProfile bRight"><?php wpg_e('Total'); ?></th>
			<td class="itemProfile bRight">
				<?php wg_formatted_money($total); ?>
			</td>
		</tr>
		<tr class="">
			<th scope="row" colspan="3" class="itemProfile bRight"><?php wpg_e('Order Number'); ?></th>
			<td class="itemProfile bRight">#<?php echo $order_number; ?></td>
		</tr>
		<?php if ( isset($checkout->cache['affiliate_credits']) && $checkout->cache['affiliate_credits'] > 0 ): ?>
			<tr class="">
				<th scope="row" colspan="3" class="itemProfile bRight"><?php wpg_e('Credits Used'); ?></th>
				<td class="itemProfile bRight">
					<?php echo wg_get_number_format($checkout->cache['affiliate_credits'],'.',','); ?>
				</td>
			</tr>
		<?php endif ?>
	</tfoot>

	<tbody>
		<?php foreach ($products as $product): ?>
			<tr>
				<td class="itemProfile bRight" colspan="2">
                
					<?php 
						$deal = WP_Groupbuy_Deal::get_instance($product['deal_id']);
						echo $deal->get_title($product['data']); ?>
				</td>
				<td class="itemProfile bRight"><?php echo $product['quantity']; ?></td>
				<td class="itemProfile bRight"><?php echo wg_formatted_money($product['unit_price']); ?></td>
			</tr>
		<?php endforeach ?>
	</tbody>

</table>
<?php 
	// Build a multidimensional array with the key being the deal id.
	// Test if any vouchers are not yet available.
	$vouchers = wg_get_vouchers_by_purchase_id( $order_number );
	$deal_and_vouchers = array();
	$all_vouchers_active = TRUE;
	if ( !empty( $vouchers ) ) {
		foreach ( $vouchers as $voucher_id ) {
			$deal_id = wg_get_vouchers_deal_id( $voucher_id );
			$deal_and_vouchers[$deal_id][] = $voucher_id;
			if ( !wg_is_voucher_active( $voucher_id ) ) {
				$all_vouchers_active = FALSE;
			}
		}
	}

	// Markup is heavily borrowed from account/view.php
	if ( !empty( $deal_and_vouchers ) ) {
		?>
			<div class="">
				

				<?php if ( WP_Groupbuy_Purchase::POST_TYPE != get_query_var('post_type') ): // Don't show on purchase template ?>

					<?php if ( !is_user_logged_in() || ( function_exists( 'wg_is_user_guest_purchaser' ) && wg_is_user_guest_purchaser() ) ) : // Don't show if the user is logged in and a guest user ?>

						<?php if ( !$all_vouchers_active ): // only show if necessary ?>
							<p class="contrast_light message"><strong><?php wpg_e( 'Some of your order is pending.' ); ?></strong>  <br/><?php wpg_e( 'Save this url to retrieve your invoice(s) later:' ); ?> <a href="<?php echo $lookup_url; ?>"><?php echo $lookup_url; ?></a></p>
						<?php else: ?>
							<p class="contrast_light message"><?php wpg_e( 'Save this url to retrieve your invoice(s) later:' ); ?> <a href="<?php echo $lookup_url; ?>"><?php echo $lookup_url; ?></a></p>
						<?php endif ?>
					<?php endif; ?>
				<?php endif ?>


				<?php foreach ( $deal_and_vouchers as $deal_id => $vouchers ): ?>
					<div class="mt38">
				       
						<h2 class="sTitle"><?php wpg_e('Your Voucher Information') ?></h2>
						
						<div class="mBottom10">
						    <?php wpg_e('Deal:') ?> <a href="<?php echo get_permalink( $deal_id ); ?>"><?php echo get_the_title( $deal_id ); ?></a>
						</div>
						<div class="">
																						
							<table class="listingOrder border_1_ddd border_bt_none border_t_none border_r_none" cellspacing="0" cellpadding="0" width="100%">
								<thead>
									<tr>
										<th class="headProfile noneborderbot bRight "><?php wpg_e('Code'); ?></th>
										<th class="headProfile noneborderbot bRight "><?php wpg_e('Status'); ?></th>
										<th class="headProfile noneborderbot bRight "><?php wpg_e('Download'); ?></th>
										<th class="headProfile noneborderbot bRight "><?php wpg_e('Expires'); ?></th>
									</tr>
								</thead>
								<tbody>
									<?php if ( $payment_status == WP_Groupbuy_Payment::STATUS_COMPLETE ) { ?>
										<?php foreach ( $vouchers as $voucher_id ): ?>
											<tr>
												<td class="itemProfile bRight">
													<?php wg_voucher_code( $voucher_id ) ?>
												</td>
												<td class="itemProfile bRight">
													
													<?php
                                                    if ( wg_has_shipping( $deal_id ) ) {
                                                        wpg_e('Shipped');
                                                    } elseif ( wg_is_voucher_claimed( $voucher_id ) ) {
														print wpg__('Used: ') . wg_get_voucher_claimed($voucher_id);
                                                    } else {
                                                        if ( wg_is_voucher_gift( $voucher_id ) ) {
                                                            wpg_e('Gift');
                                                        }  else {
                                                        if (wg_is_voucher_claimed( $voucher_id )) {
																print wpg__('Used: ') . wg_get_voucher_claimed($voucher_id);
                                                            } else {
                                                        ?>
                                                            <a href="#" rel="<?php echo $voucher_id; ?>" class="voucher_mark_claimed fl mBottom10 viewdetailOrder viewdetailOrderSmall" id="claim_voucher_<?php echo $voucher_id; ?>"><?php wpg_e('Claim Voucher') ?></a>
                                                    <?php   }
                                                        }
                                                    }
                                                    ?>
													
												</td>
												<td class="itemProfile bRight">
													<?php if ( wg_is_voucher_claimed( $voucher_id )){ ?>
														
														  <?php wpg_e('Redeemed') ?>
														
													<?php } elseif ( wg_is_voucher_active( $voucher_id ) ) {
                                                        if ( wg_is_voucher_gift( $voucher_id ) ) {
                                                            wpg_e('Gift');
                                                        }  else {
                                                        if ( wg_is_voucher_redeemed( $voucher_id )  && !wg_is_voucher_claimed( $voucher_id )) {
                                                                print wpg__('Redeemed: ') . wg_get_voucher_redemption_date($voucher_id);
                                                            } else {
                                                        ?>
                                                            <script type="text/javascript">
                                                                jQuery(function($){
                                                                    $('#download_voucher_<?php echo $voucher_id; ?>') .click(function() {
                                                                        window.open("<?php echo (wg_voucher_link_to_pdf($voucher_id)); ?>", "_blank"); // will open new tab on document ready
                                                                    });
                                                                });
                                                            </script>
                                                            <a href="#"  rel="<?php echo $voucher_id; ?>" title="<?php wpg_e( 'Download Voucher' ) ?>" class="voucher_mark_redeemed fl mBottom10 voucher_download viewdetailOrder viewdetailOrderSmall" id="download_voucher_<?php echo $voucher_id; ?>"><?php wpg_e('Download') ?></a>
													<?php   } 
                                                        }
                                                    } else { ?>
														<?php wpg_e('Pending') ?>
													<?php } ?>
												</td>
												<td class="itemProfile bRight">
													
													<?php 
														if ( wg_get_voucher_expiration_date( $voucher_id ) ): ?>
														<?php wg_voucher_expiration_date( $voucher_id ); ?>
													<?php else: ?>
														N/A
													<?php endif ?>
												</td>
											</tr>
										<?php endforeach ?>
									<?php } else { ?>
										<tr>
											<td colspan="4" style="padding: 20px; border-bottom: 1px solid rgb(226, 226, 226); border-right: 1px solid #E2E2E2;">
												<?php wpg_e('Pending') ?>
											</td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
						<div class="mTop10">
                            Pregled vseh kuponov je na voljo <a href="<?php wg_voucher_url(true); ?>" target="_blank">tukaj</a>.
                        </div>
					
					</div>
				<?php endforeach ?>
			</div>
		<?php
	}
 ?>
