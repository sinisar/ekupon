<div class="mt38"></div>
	<fieldset id="wg_billing">
    <legend><?php wpg_e('Your Payment Information '); ?></legend>
		<table class="billing_info">
			<tbody>
				<?php foreach ( $fields as $key => $data ): ?>
					<tr>
						<th scope="row"><?php echo $data['label']; ?></th>
						<td><?php echo $data['value']; ?></td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</fieldset>

</div>