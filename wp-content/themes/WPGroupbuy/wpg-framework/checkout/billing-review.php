<div class="mt38"></div>
	<fieldset id="wg_billing">
    <legend><?php wpg_e('Your Billing Information'); ?></legend>
        <table class="billing_info">
			<tbody>
				<tr>
					<th scope="row"><?php wpg_e('Name'); ?></th>
					<td class="edit-td first">
                        <?php esc_html_e($data['first_name']); ?> <?php esc_html_e($data['last_name']); ?>
                    </td>
				</tr>
				<tr>
					<th scope="row"><?php wpg_e('Address'); ?></th>
					<td class="edit-td first">
						<?php if ( $data['street'] ) { echo $data['street']; } ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?php wpg_e('City'); ?></th>
                    <td class="edit-td first">
                        <?php
                            if ( $data['city'] ) {
                                echo $data['city'];
                                if($data['postal_code']) {
                                    echo $data['postal_code'] . ', ';
                                }
                            }
                        ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?php wpg_e('Country'); ?></th>
                    <td class="edit-td first">
                        <?php echo $data['country'].' '; ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?php wpg_e('Phone'); ?></th>
                    <td class="edit-td first">
						<?php echo $data['phone'].' '; ?>
					</td>
				</tr>
			</tbody>
		</table>
	</fieldset>
