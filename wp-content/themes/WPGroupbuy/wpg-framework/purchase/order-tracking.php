<?php wpg_e('Please enter your order number and billing city to tracking information of your purchase.') ?>
<form action="<?php echo $action; ?>" method="post" id="wg_order_lookup" class="mTop10">
	<table class="">
		<tbody>
			<tr>
				<td class="edit-td"><?php wg_form_label( $order_option_name, array( 'label' => 'Order #' ), 'order_lookup' ); ?></td>
				<td class="">
					<?php wg_form_field( $order_option_name, array( 'type' => 'text' ), 'order_lookup' ); ?>
				</td>
			</tr>
			<tr>
				<td><?php wg_form_label( $city_option_name, array( 'label' => "Order's Billing City" ), 'order_lookup' ); ?></td>
				<td class="">
					<?php wg_form_field( $city_option_name, array( 'type' => 'text' ), 'order_lookup' ); ?>
				</td>
			</tr>
			<?php wp_nonce_field( $nonce_id ); ?>
		</tbody>
	</table>
    <div align="center">
	<input type="submit" name="submit" value="<?php wpg_e('View Order') ?>" class="profileAddGoldV6">
    </div>
</form>
