<div class="blockSoldItem blockSoldHide1 pBottom10 blockSoldItemBorder">
  <h3 class="title m0 newqc"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
  <div class="info mTop5 mBottom5">
  <?php if ( has_post_thumbnail() ):?>
  <a href="<?php the_permalink(); ?>" class="soldImgqc mRight15"> <?php the_post_thumbnail( 'wpg_88x88' ); ?> <span class="box_discountsale"><?php wg_amount_saved() ?></span> </a>
  <?php else:?>
  <a href="<?php the_permalink(); ?>" class="soldImgqc mRight15"> <div class="no_img_sm w88 h88"></div> <span class="box_discountsale"><?php wg_amount_saved() ?></span> </a>
  <?php endif ?>
    <div class="soldInfo soldInfonew wauto">
      <div class="title m0 newqc"><a href="<?php the_permalink(); ?>"><span></span></a></div>
      <div class="mTop5"><?php wpg_e('Price:') ?> <b><?php wg_price(); ?></b> </div>
      <div>
      <?php
			$post_date = get_the_date();
			$post_date = date_parse($post_date);
			$post_date = $post_date[month].'&ndash;'.$post_date[day].'&ndash;'.$post_date[year];
			$post_date = '<span class="date"><strong>You can buy this starting on: '.$post_date.'</strong></span>';
		?>
      </div>
    </div>
    <div class="c"></div>
  </div>
</div>