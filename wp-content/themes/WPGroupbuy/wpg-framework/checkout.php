<?php 

$checkout = WP_Groupbuy_Checkouts::get_instance();

global $wp_query;
$wp_query->is_home = false;


get_header(); 
switch (wg_get_current_checkout_page()) {
case 'confirmation':
	$title = 'Your Purchase Confirmation';
	break;
case 'review':
	$title = 'Your Purchase Review';
	break;
case 'payment':
default:
	$title = 'Purchase';
	break;
}
?>
<!-- CHECKOUT -->

<div class="main">
  <div class="mainPadding"> 
    <!-- LEFT CONTENT -->
    <div class="leftMainBox">
      <div class="sell1 mTop10">
        <div class="profileCus">
          <div class="profileHeaderTop">
            <div class="fl profileHeaderTitle">
              <?php echo $checkout->get_title(); ?>
            </div>
            <div class="c"></div>
          </div>
        </div>
        
        <div class="main_viewprofile">
          <div class="main-boxcontFull m0">
            <div class="profileContainer">
              <div id="wpg_checkout" class="pTop10">
                  <?php echo $checkout->view_checkout() ?>
                  <!--On login-or-register page-->
                  <?php if ( !is_user_logged_in() ) { ?>
                  </div></div></div>
                  <?php } ?>
                  <div class="c"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END LEFT CONTENT --> 
    
    <!-- RIGHT SIDE -->
    <div class="rightMainBox">
      <div>
        <div class="mBottom15 mTop10"> 
          <?php dynamic_sidebar( 'cart-sidebar' ); ?>
          
        </div>
      </div>
      <div></div>
    </div>
    <!-- END RIGHT SIDE -->
    <div class="c"></div>
  </div>
</div>

<!-- END CHECKOUT -->
<?php get_footer(); ?>
