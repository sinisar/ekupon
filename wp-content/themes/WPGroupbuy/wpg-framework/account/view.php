<?php $current_user = wp_get_current_user(); ?>
<div id="dashboard_container" class="pTop10">
	<?php do_action('account_section_before_account') ?>
	<div id="acc-setting" class="left-box-acc <?php echo wg_merchant_enabled() ? '' : 'full' ?>">
		<div class="sTitle"><h2><?php wpg_e('Your Account'); ?></h2></div>
        <div id="scrolleracc">
		<div>
			<p><span class="contact_title"><?php wpg_e('Email:') ?></span> <?php echo $current_user->user_email ?></p>
			<p><span class="contact_title"><?php wpg_e('Password:') ?></span> *******</p>
		</div>
		<?php foreach ( $panes as $pane ) {
				echo $pane['body'];
			} ?>
            <a href="<?php wg_account_edit_url(); ?>" class="viewdetailOrder wsmall viewdetailOrderSmall"><?php wpg_e('Edit'); ?></a>
		<?php do_action('account_settings_section') ?>
        </div>
	</div>
	<?php do_action('account_section_before_biz') ?>
	
	<?php if ( wg_merchant_enabled() ): ?>
		
	<div id="business-setting" class="right-box-acc">
    <div class="sTitle"><h2><?php wpg_e('Business Account') ?></h2></div>
		 <div id="scroller">
		<?php if (wg_account_has_merchant()): ?>
			<p><span class="contact_title"><?php wpg_e('Company Name:') ?></span> <?php echo wg_merchant_name(wg_account_merchant_id()); ?></p>
			<p><span class="contact_title"><?php wpg_e('Address:') ?></span> <?php echo wg_merchant_street(wg_account_merchant_id()); ?>, <?php echo wg_merchant_city(wg_account_merchant_id()); ?>, <?php echo wg_merchant_state(wg_account_merchant_id()); ?>, <?php echo wg_merchant_zip(wg_account_merchant_id()); ?>, <?php echo wg_merchant_country(wg_account_merchant_id()); ?> </p>
			<p><span class="contact_title"><?php wpg_e('Phone:') ?></span> <?php echo wg_merchant_phone(wg_account_merchant_id()); ?></p>
			<p><span class="contact_title"><?php wpg_e('Website:') ?></span> <?php echo wg_merchant_website(wg_account_merchant_id()); ?></p>
			<p><span class="contact_title"><?php wpg_e('Facebook:') ?></span> <?php echo wg_merchant_facebook(wg_account_merchant_id()); ?></p>
			<p><span class="contact_title"><?php wpg_e('Twitter:') ?></span> <?php echo wg_merchant_twitter(wg_account_merchant_id()); ?></p>
			<p><span class="contact_title"><?php wpg_e('Business Category:') ?></span> <?php echo wg_merchants_type_url(wg_account_merchant_id()); ?></p>
			<p>
				<?php
					$merchant = get_post(wg_account_merchant_id());
					if ( !empty($merchant->post_content) ) {
						?>
							<h3><?php wpg_e('Your Business Description'); ?></h3>
						<?php
						echo apply_filters('the_content', $merchant->post_content );
					}
					?>
			</p>
            
            <?php if (wg_account_has_merchant()) { ?><a href="<?php wg_merchant_edit_url(); ?>" class="viewdetailOrder wsmall viewdetailOrderSmall"><?php wpg_e('Edit'); ?></a> <?php } ?>
		<?php else: ?>
			<?php wpg_e('Are you a business owner?') ?> <a class="header_edit_link" href="<?php wg_merchant_register_url(); ?>"><?php wpg_e('Register your business now.'); ?></a>
		<?php endif ?>
		<?php do_action('account_biz_section') ?>
	</div>

	<?php endif ?>

    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.slimscroll.min.js"></script>
    <script type="text/javascript">
    jQuery(function(){
    jQuery('#scroller').slimScroll({
        height: '200px',
        size: '2px',
        color: '#333'
        });
	jQuery('#scrolleracc').slimScroll({
        height: '200px',
        size: '2px',
        color: '#333'
        });
    });
	</script>
    </div>
	<div class="c"></div>
	<?php do_action('account_section_before_dash') ?>
	<div class="pTop20">
		<div class="sTitle"><h2><?php wpg_e('Recent Purchases'); ?></h2></div>
        		
		<?php
			$vouchers= null;
			$args=array(
				'post_type' => wg_get_voucher_post_type(),
				'post_status' => 'publish',
				'posts_per_page' => 5, // return this many
				
			);
			$vouchers = new WP_Query($args);
			
			if ($vouchers->have_posts()) {
				?>
                <table width="100%" cellspacing="0" cellpadding="0" class="listingOrder border_1_ddd border_bt_none border_t_none">
                  <thead>
                    <tr align="left">
                      <th class="headProfile noneborderbot bRight" align="left" width="120" ><?php wpg_e('Voucher'); ?></th>
                      <!--<th class="headProfile noneborderbot bRight " width="80" align="left"><?php wpg_e('Status'); ?></th>-->
                      <th class="headProfileCenter noneborderbot pLeft5 bRight" align="center" width="80"><?php wpg_e('Expires'); ?></th>
                      <th class="headProfileCenter noneborderbot" width="80" align="center"><?php wpg_e('Status'); ?></th>
                    </tr>
                  </thead>
					
					<tbody>
					<?php
					while ($vouchers->have_posts()) : $vouchers->the_post();
						$dealID = wg_get_vouchers_deal_id();
						if ( $dealID ) :
						?>
						<tr>
							<td valign="top" class="itemProfile bRight" align="left">
                            <div class="borderImg fl mRight5 w49 h48"> 
                      <a href="<?php echo get_permalink($dealID); ?>" class="titleProfile">
                      <?php if ( get_the_post_thumbnail( $dealID) ) { ?>
					  <?php echo get_the_post_thumbnail( $dealID, array(49,48)); ?>
                      <?php } else { ?>
                      <div class="no_img_mn w49 h48"></div>
                      <?php }?>
                      </a>
                      </div>
								<a href="<?php echo get_permalink($dealID); ?>" class="titleProfile"><?php echo get_the_title(); ?></a>
								<?php if (wg_has_merchant_name($dealID)): ?>
									<div class="onlineMerchant newicon2 pTop10"> <?php wpg_e('From: ') ?>
<a class="c0575b3" target="_blank" href="<?php wg_merchant_url($dealID) ?>"><?php wg_merchant_name($dealID) ?></a>
</div>
								<?php endif ?>
                            <td valign="top" align="center" class="itemProfile bRight">

                                <?php
                                if ( wg_get_voucher_expiration_date() ): ?>
                                    <?php wg_voucher_expiration_date(); ?>
                                <?php else: ?>
                                    N/A
                                <?php endif ?>
                            </td>
                            <td valign="top" align="center" class="itemProfile">
								<?php
									if ( wg_has_shipping($dealID)) {
										wpg_e('Shipped');
									}
                                    //  if only redeemed, but not yet used
                                    elseif ( wg_is_voucher_redeemed( get_the_ID() )  && !wg_is_voucher_claimed( get_the_ID() )) {
                                        print wpg__('Redeemed: ') . wg_get_voucher_redemption_date();
                                    } elseif (wg_is_voucher_claimed( get_the_ID() )) {
                                        //
                                        print wpg__('Used: ') . wg_get_voucher_claimed();
                                    } else {
										?>
                                            <script type="text/javascript">
                                                jQuery(function($){
                                                    $('#download_voucher_<?php the_ID() ?>').click(function() {
                                                        window.open("<?php echo(wg_voucher_link_to_pdf()); ?>", "_blank"); // will open new tab on document ready
                                                    });
                                                });
                                            </script>
											<a href="#"  rel="<?php the_ID() ?>" class="voucher_mark_redeemed alt_button viewdetailOrder viewdetailOrderSmall" id="download_voucher_<?php the_ID() ?>"><?php wpg_e('Download') ?></a>
										<?php
									}
								?>
							</td>

						</tr>
						<?php
						endif;
					endwhile;
					?>
					</tbody>
				</table>
                
                <div class="c mBottom10"></div>
				<?php if ($vouchers->found_posts > 10): ?>
					<a href="<?php wg_voucher_url(); ?>" class="viewdetailOrder viewdetailOrderSmall mTop10"><?php wpg_e('View More'); ?></a>
				<?php endif ?>
				<?php
			} else {
				?>
					<p><?php wpg_e('You have not purchased any '); ?><a href="<?php echo wg_get_deals_link() ?>" title="<?php wpg_e('Browse Active Deals') ?>"><?php wpg_e('Browse Active Deals'); ?></a>.</p>
				<?php
			}
		?>
        
		<?php do_action('account_dash_section') ?>
	</div>
</div>