<?php
global $wp;
$current_url = home_url(add_query_arg(array(),$wp->request));
$is_user = TRUE;

if (strpos($current_url, 'business') !== false) {
    $is_user = FALSE;
}
?>

<div class="profileTabV6">
  <ul class="reportTypev6">
    <li class="fl"><a href="<?php wpg_account_link(); ?>" class="fixPNG <?php if (wg_on_account_page() ) echo 'active' ?>">
      <?php wpg_e('Dashboard') ?>
      </a></li>
    <li class="fl"><a href="<?php wg_voucher_url( true ); ?>" class="fixPNG <?php if ( (wg_on_user_voucher_page( $is_user )) || (wg_on_user_voucher_active_page( true )) || (wg_on_user_voucher_used_page( true )) || (wg_on_user_voucher_expired_page( true )) ) echo 'active' ?>">
      <?php wpg_e('My Vouchers') ?>
      </a></li>
    <?php if ( wg_account_has_merchant() ) : ?>
    <li class="fl"><a href="<?php wg_merchant_account_url(); ?>" class="fixPNG <?php if (wg_on_merchant_dashboard_page()) echo 'active' ?>">
      <?php wpg_e('Business') ?>
      </a></li>
    <?php endif; ?>
    <?php if (wg_account_has_merchant()): ?>
    <li class="fl"><a href="<?php wg_business_voucher_url(); ?>" class="fixPNG <?php if ( (wg_on_business_voucher_page(!$is_user))) echo 'active' ?>">
      <?php wpg_e('Business Vouchers') ?>

      </a>
    </li>
    <?php endif ?>
    <?php
    //echo "Submit new deal url: ".wg_deal_submission_url();
    //if (wg_account_has_merchant()): ?>
      <!--
    <li class="fl"><a href="<?php //wg_deal_submission_url(); ?>" class="fixPNG <?php //if ( wg_on_deal_submit_page() ) echo 'active' ?>">
      <?php //wpg_e('Submit Deal') ?>
      </a></li>
    <?php //endif ?>
    -->
  </ul>
  <div class="c"></div>
</div>
