<?php get_header();

global $wp;
$current_url = home_url(add_query_arg(array(),$wp->request));
$is_user = TRUE;

if (strpos($current_url, 'business') !== false) {
$is_user = FALSE;
}
?>
<div class="main">
  <div class="mainPadding"> 
    <!-- LEFT CONTENT -->
    <div class="leftMainBox">
      <div class="sell1 mTop10">
      
      <?php if ( is_user_logged_in() ): ?>
        <div class="profileCus">
          <?php get_template_part( 'wpg-framework/account/topnav' ); ?>
          <?php get_template_part( 'wpg-framework/account/nav' ); ?>
        </div>
        <?php endif ?>
        
        <div class="main_viewprofile">
          <div class="main-boxcontFull">
            <div class="listing_profile">
              <div class="mTop10 aleft">
                <?php if (wg_on_voucher_page()): ?>
                <ul class="menuFilters">
                  <li class="fl"><a href="<?php wg_voucher_url($is_user); ?>">
                    <?php wpg_e('View All') ?>
                    </a></li>
                  <li class="fl"><a href="<?php wg_voucher_active_url( $is_user ); ?>" class="<?php if (wg_on_user_voucher_active_page( $is_user )) echo 'active' ?>">
                    <?php wpg_e('Active Vouchers') ?>
                    </a></li>
                  <li class="fl"><a href="<?php wg_voucher_used_url( $is_user ); ?>" class="<?php if (wg_on_user_voucher_used_page( $is_user )) echo 'active' ?>">
                    <?php wpg_e('Used Vouchers') ?>
                    </a></li>
                  <li class="fl"><a href="<?php wg_voucher_expired_url( $is_user ); ?>" class="<?php if (wg_on_user_voucher_expired_page( $is_user)) echo 'active' ?>">
                    <?php wpg_e('Expired Vouchers') ?>
                    </a></li>
                </ul>
                <?php endif ?>
                <div class="c"></div>
              </div>
              <div id="listData">
                
                <table width="100%" cellspacing="0" cellpadding="0" class="listingOrder border_1_ddd border_bt_none border_t_none">
                  <thead>
                    <tr align="left">
                      <th class="headProfile noneborderbot bRight" align="left" colspan="2"><?php wpg_e('Deal'); ?></th>
                      <th class="headProfile noneborderbot bRight " width="120" align="left"><?php wpg_e('Voucher code'); ?></th>
                      <th class="headProfileCenter noneborderbot bRight " width="80" align="left"><?php wpg_e('Expires'); ?></th>
                      <th class="headProfileCenter noneborderbot pLeft5 bRight" width="100" align="left"><?php wpg_e('Status'); ?></th>
                        <?php if(wg_on_all_business_voucher_page() || wg_on_business_voucher_active_page() || wg_on_business_voucher_expired_page()): ?>
                        <th class="headProfileCenter noneborderbot pLeft5 bRight" width="100" align="left"><?php wpg_e('Action'); ?></th>
                        <?php endif ?>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
					
          if (have_posts()) {
              while (have_posts()) {
                  the_post();

              $voucher = WP_Groupbuy_Voucher::get_instance(get_the_ID());
              $purchase = $voucher->get_purchase();
              $dealID = $voucher->get_deal_id();

              $exp = ( float ) get_post_meta( $dealID, '_voucher_expiration_date', TRUE );

              $show = false;
              
              if ( !get_query_var( WP_Groupbuy_Vouchers::FILTER_QUERY_VAR )
              || ( wg_on_user_voucher_active_page( $is_user ) && ( !wg_is_voucher_claimed( get_the_ID() ) && ( !$exp || $exp > current_time( 'timestamp' ) ) ) )
              || ( wg_on_voucher_expired_page() && ( $exp && $exp < current_time( 'timestamp' ) ) )
              || ( wg_on_voucher_used_page() && wg_is_voucher_claimed( get_the_ID() ) )
              || ( wg_on_all_business_voucher_page() )
              || ( wg_on_business_voucher_active_page() && ( !wg_is_voucher_claimed( get_the_ID() ) && ( !$exp || $exp > current_time( 'timestamp' ) ) ) )
              || ( wg_on_business_voucher_used_page() && wg_is_voucher_claimed( get_the_ID() ) )
              || ( wg_on_business_voucher_expired_page() && ( $exp && $exp < current_time( 'timestamp' ) ) )

              ) { 

					?>
                    <tr>
                      <td valign="top" class="itemProfile bRight" align="left" colspan="2">
                      <div class="borderImg fl mRight5 w49 h48"> 
                      <a href="<?php echo get_permalink($dealID); ?>" class="titleProfile">
                      <?php if ( get_the_post_thumbnail( $dealID) ) { ?>
                      <?php echo get_the_post_thumbnail( $dealID, array(49,48)); ?>
					  <?php } else { ?>
                      <div class="no_img_mn w49 h48"></div>
                      <?php }?>
                      </a>
                      </div>
                      
                        <a href="<?php echo get_permalink($dealID); ?>" class="titleProfile"><?php echo get_the_title(); ?></a>
                        <?php if (wg_has_merchant_name($dealID)): ?>
                        <div class="onlineMerchant newicon2 pTop10"> <?php wpg_e('From: ') ?> <a target="_blank" href="<?php wg_merchant_url($dealID) ?>"><?php wg_merchant_name($dealID) ?></a>
</div> <?php endif ?>
                        </td>
                      <td valign="top" align="left" class="itemProfile bRight"><?php wg_voucher_code() ?></td>
                      <td valign="top" align="center" class="itemProfile bRight">
					  <?php if ( wg_get_voucher_expiration_date() ): ?>
                        <?php wg_voucher_expiration_date(); ?>
                        <?php else: ?>
                        N/A
                        <?php endif ?></td>
                      <td valign="top" align="left" class="itemProfile bRight">
					  <?php
						if ( wg_has_shipping($dealID)) {
							wpg_e('Shipped');
						}
						//  if only redeemed, but not yet used
						elseif ( wg_is_voucher_redeemed( get_the_ID() )  && !wg_is_voucher_claimed( get_the_ID() )) {
						    print wpg__('Redeemed: ') . wg_get_voucher_redemption_date();
						 } elseif (wg_is_voucher_claimed( get_the_ID() )) {
                            print wpg__('Used: ') . wg_get_voucher_claimed();
                        }
                        else {
						    if ( wg_is_voucher_active( )  && ($exp > current_time( 'timestamp' ) )) {
						?>
                            <script type="text/javascript">
                                jQuery(function($){
                                    $('#download_voucher_<?php the_ID() ?>').click(function() {
                                        window.open("<?php echo(wg_voucher_link_to_pdf()); ?>", "_blank"); // will open new tab on document ready
                                    });
                                });
                            </script>

                        <a href="#"  rel="<?php the_ID() ?>" class="voucher_mark_redeemed alt_button viewdetailOrder viewdetailOrderSmall" id="download_voucher_<?php the_ID() ?>"><?php wpg_e('Download') ?></a>
                        <?php } else {
                                wpg_e("Expired");
                            }
                        } ?>
                      </td>

                        <?php if(!$is_user && !wg_is_voucher_claimed(get_the_ID()) && wg_is_voucher_redeemed(get_the_ID()) ) :?>
                      <td valign="top" align="left" class="itemProfile bRight">


                          <script type="text/javascript">
                              jQuery(function($){
                                  $('#claim_voucher_<?php the_ID() ?>').click(function() {
                                      <?php echo(wg_voucher_link_to_pdf()); ?>
                                  });
                              });
                          </script>

                          <a href="#"  rel="<?php the_ID() ?>" class="voucher_mark_claimed alt_button viewdetailOrder viewdetailOrderSmall" id="claim_voucher_<?php the_ID() ?>"><?php wpg_e('Claim Voucher') ?></a>


                      </td>
                        <?php elseif ( wg_on_all_business_voucher_page() || wg_on_business_voucher_active_page() || wg_on_business_voucher_expired_page() ): ?>
                      <td valign="top" align="left" class="itemProfile bRight">
                          <?php
                          if ( wg_is_voucher_redeemed( get_the_ID() )  && !wg_is_voucher_claimed( get_the_ID() )) {
                              print wpg__('Redeemed: ') . wg_get_voucher_redemption_date();
                          } elseif (wg_is_voucher_claimed( get_the_ID() )) {
                              print wpg__('Used: ') . wg_get_voucher_claimed();
                          } else {
                              wpg_e("Not yet redeemed.");
                          } ?>

                      </td>
                        <?php endif;?>

                    </tr>
                    <?php }
              } ?>
                    
                    <?php } else { ?>
                <tr>
                  <td class="itemProfile" colspan="6"><?php wpg_e('No vouchers found.'); ?></td>
                </tr>
                <?php } ?>
                  </tbody>
                </table>
                <?php
				
				if (  @$deals->max_num_pages > 1 ) :
					?>
                <div class="c"></div>
                <div id="nav-below" class="navigation clearfix mTop10">
                  <?php 
							$pages = intval(ceil($deals->found_posts / 5) );
							if ( $paged < $pages ) :  ?>
                  <div class="nav-previous">
                    <?php next_posts_link( wpg__( '<span class="meta-nav">&larr;</span> Older purchases' ) ); ?>
                  </div>
                  <?php endif ?>
                  <div class="nav-next">
                    <?php previous_posts_link( wpg__( 'Latest purchases <span class="meta-nav">&rarr;</span>' ) ); ?>
                  </div>
                </div>
                <!-- #nav-below -->
                <?php endif; ?>
				
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END LEFT CONTENT --> 
    
    <!-- RIGHT SIDE -->
    <div class="rightMainBox">
      <div>
        <div class="mBottom15 mTop10">
          <?php get_template_part( 'wpg-framework/inc/account-sidebar' ); ?>
          <?php dynamic_sidebar( 'account-sidebar' ); ?>
        </div>
      </div>
      <div></div>
    </div>
    <!-- END RIGHT SIDE -->
    <div class="c"></div>
  </div>
</div>
<?php get_footer(); ?>
