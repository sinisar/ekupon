<div class="profileHeaderTop">
  <div class="fl profileHeaderTitle">
    <?php if (wg_on_account_page() ) { ?>
    	<?php wpg_e('Dashboard') ?>
    <?php } elseif ( (wg_on_voucher_page()) || (wg_on_voucher_active_page()) || (wg_on_voucher_used_page()) || (wg_on_voucher_expired_page()) ){ ?>
    	<?php wpg_e('My Vouchers') ?>
    <?php } elseif (wg_on_merchant_dashboard_page()) { ?>
    	<?php wpg_e('Your Business') ?>
    <?php //} elseif ( wg_on_deal_submit_page() ) { ?>
    	<?php //wpg_e('Submit New Deal') ?>
    <?php } else { ?>
    <?php the_title(); ?>
    <?php } ?>
  </div>
  <!--<div class="fr"> <a href="#" class="profileAddGoldV6">
    <?php wpg_e('Deposit') ?>
    </a> </div>-->
  <div class="fr profilesmoney">
    <?php
			// Check account balances for (account) balance and affiliate (credits).
			$balance = wg_get_account_balance( get_current_user_id(), 'balance' );
			$reward_points = wg_get_account_balance( get_current_user_id(), 'points' );
		 ?>
    <?php if ( $balance ): ?>
    <?php wpg_e('Account Balance:'); ?>
    <?php wg_formatted_money( $balance ); ?>
    <br/>
    <?php endif ?>
    <?php if ( $reward_points ): ?>
    <?php wpg_e('Reward Points:'); ?>
    <?php wg_number_format( $reward_points, FALSE, ',' ); ?>
    <?php endif ?>
  </div>
  <div class="c"></div>
</div>
