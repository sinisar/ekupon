<fieldset id="wg-account-subscription-info">
<legend><?php wpg_e('E-Mail Nofitication'); ?></legend>
<div class="mBottom10 mTop5"><?php wpg_e('You\'ll get below notification Email, but you can uncheck them.'); ?></div>

	<table class="">
		<tbody>
			<?php foreach ( $fields as $key => $data ): ?>
				<tr>
					<?php if ( $data['type'] != 'checkbox' ): ?>
						<td><?php //wg_form_label($key, $data, 'subscription'); ?></td>
						<td><?php wg_form_field($key, $data, 'subscription'); ?></td>
					<?php else: ?>
						<td colspan="2">
							<label for="wg_contact_<?php echo $key; ?>"><?php wg_form_field($key, $data, 'subscription'); ?> <?php echo $data['label']; ?></label>
						</td>
					<?php endif; ?>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</fieldset>