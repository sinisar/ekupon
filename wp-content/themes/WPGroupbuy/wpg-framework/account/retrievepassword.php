<div id="wpg_login_form" class="pBottom10">

<form name="lostpasswordform" id="lostpasswordform" action="<?php echo wp_lostpassword_url(); ?>" method="post" class="">
	<div class="sTitle"><h2><?php wpg_e('Forgot Password') ?></h2></div>
    <?php wpg_e('Please enter your username or the e-mail address you used when signing up.'); ?>
	<table class="fl">
		<tbody>
			<tr>
				
				<td colspan="2"><input tabindex="21" type="text" name="user_login" id="user_login" /></td>
			</tr>
			<?php do_action('lostpassword_form'); ?>
			<tr>
				<td>
                    <input tabindex="22" type="submit" name="wp-submit" id="wp-submit" class="profileAddGoldV6" value="<?php wpg_e('Get Password'); ?>" />
                </td>
			</tr>
		</tbody>
	</table>
</form>
<script type="text/javascript">
jQuery(document).ready(function(){
  	jQuery("#lostpasswordform").validate({
		rules: {
			'user_login': "required"
		  }
	});
	
	jQuery("#user_login").focus();
 });
</script>
</div>
<div class="c"></div>