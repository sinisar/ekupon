<div class="mt38"></div>
<?php do_action('wg_account_register_form_contact_info'); ?>
<fieldset id="wg-account-contact-info">
<legend><?php wpg_e('Billing Information') ?></legend>
	<table class="">
		<tbody>
			<?php foreach ( $fields as $key => $data ): ?>
				<tr>
					<?php if ( $data['type'] != 'checkbox' ): ?>
						<td class="edit-td"><?php wg_form_label($key, $data, 'contact'); ?></td>
						<td><?php wg_form_field($key, $data, 'contact'); ?></td>
					<?php else: ?>
						<td colspan="2">
							<label for="wg_contact_<?php echo $key; ?>"><?php wg_form_field($key, $data, 'contact'); ?> <?php echo $data['label']; ?></label>
						</td>
					<?php endif; ?>
				</tr>
			<?php endforeach; ?>
            <tr>
                <td><label for="cptch_block"><?php wpg_e('Captcha') ?>:</label></td>
                <td class="">
                    <?php
                    do_action( 'wg_register_form');
                    do_action( 'cptch_verify'); ?>
                </td>
            </tr>
		</tbody>
	</table>
</fieldset>