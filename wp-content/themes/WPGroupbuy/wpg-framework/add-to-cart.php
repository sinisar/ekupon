<div id="wpg_addcart">
<?php
if ( function_exists('wg_deal_has_attributes') && wg_deal_has_attributes() ) {
	?>
	<form class="add-to-cart" method="post" action="<?php wg_add_to_cart_action(); ?>">
		<?php foreach ( $fields as $field ): ?>
			<?php echo $field; ?>
		<?php endforeach; ?>
		<input class="bt_addtocart mLeft30" type="submit" value="<?php wpg_e($button_text); ?>" />
	</form>
<?php } ?>
</div>
