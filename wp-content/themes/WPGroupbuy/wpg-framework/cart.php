<?php get_header();?>
<!-- CART -->

<div class="main">
  <div class="mainPadding"> 
    <!-- LEFT CONTENT -->
    <div class="leftMainBox">
      <div class="sell1 mTop10">
        <div class="profileCus">
          <div class="profileHeaderTop">
            <div class="fl profileHeaderTitle">
              <?php wpg_e('My Cart') ?>
            </div>
            <div class="c"></div>
          </div>
        </div>
        
        <div class="main_viewprofile">
          <div class="main-boxcontFull m0">
            <div class="profileContainer">
              <div class="pTop10">
                <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
                <?php the_content(); ?>
                <div class="fr"><a class="keepshp" href="<?php echo site_url(); ?>"><?php wpg_e('Keep Shopping &#8594;') ?></a></div>
                <?php endwhile; // end of the loop. ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END LEFT CONTENT --> 
    
    <!-- RIGHT SIDE -->
    <div class="rightMainBox">
      <div>
        <div class="mBottom15 mTop10"> 
          <?php dynamic_sidebar( 'cart-sidebar' ); ?>
          
        </div>
      </div>
      <div></div>
    </div>
    <!-- END RIGHT SIDE -->
    <div class="c"></div>
  </div>
</div>
<!-- END CART -->

<?php get_footer(); ?>
