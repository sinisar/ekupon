<?php get_header(); ?>
<!-- REPORT -->

<div class="main">
  <div class="mainPadding"> 
    <!-- LEFT CONTENT -->
    <div class="leftMainBox">
      <div class="sell1 mTop10">
      
      <?php if ( is_user_logged_in() ): ?>
        <div class="profileCus">
          <?php get_template_part( 'wpg-framework/account/topnav' ); ?>
          <?php get_template_part( 'wpg-framework/account/nav' ); ?>
        </div>
        <?php endif ?>
        
        <div class="main_viewprofile">
          <div class="main-boxcontFull m0">
            <div class="profileContainer">
              <div class="">
                <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
                <div class="">
                <a href="<?php wg_current_report_csv_download_url(); ?>" class="viewdetailOrder viewdetailOrderSmall mBottom10 fl"><?php wpg_e('csv download') ?></a> <a class="report-back"  href="javascript:history.go(-1);" title="Back"></a>
                
                </div>
                <div class="c"></div>
                <?php the_content(); ?>
                <?php endwhile; // end of the loop. ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END LEFT CONTENT --> 
    
    <!-- RIGHT SIDE -->
    <div class="rightMainBox">
      <div>
        <div class="mBottom15 mTop10"> 
          <?php get_template_part( 'wpg-framework/inc/account-sidebar' ); ?>
          <?php dynamic_sidebar( 'account-sidebar' ); ?>
          
        </div>
      </div>
      <div></div>
    </div>
    <!-- END RIGHT SIDE -->
    <div class="c"></div>
  </div>
</div>
<!-- END REPORT -->

<?php get_footer(); ?>