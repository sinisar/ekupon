<?php
	$claimed = FALSE;
	if ( isset($_GET['wg_voucher_claim']) && $_GET['wg_voucher_claim'] != '' ) {
		$vouchers = WP_Groupbuy_Voucher::get_voucher_by_security_code($_GET['wg_voucher_claim']);
		if ( !empty($vouchers) ) {
			$voucher = WP_Groupbuy_Voucher::get_instance($vouchers[0]);
			if ( is_a($voucher,'WP_Groupbuy_Voucher') ) {
				$claimed = $voucher->get_claimed_date();
			}
		}
	}
	?>	
<?php if ( FALSE != $claimed ) : ?>
	<div id="voucher_claimed_warning" class="main_block clearfix">
		<p class="warning button font_xx_large"><h2><?php wpg_e('Voucher successfully claimed!') ?></h2></p>
		<?php $redemption_data = $voucher->get_redemption_data(); ?>
		<span class="voucher_info_title wg_ff"><?php wpg_e('Voucher marked claimed:') ?></span> <span class="voucher_info button"><b><?php echo date(get_option('date_format'),$claimed) ?></b></span><br>
		<?php if($redemption_data['name'] != FALSE) : ?>
        <span class="voucher_info_title wg_ff"><?php wpg_e('Redeemer Name:') ?></span> <span class="voucher_info button"><b><?php echo $redemption_data['name'] ?></b></span><br>
        <?php endif;
        if($redemption_data['date']): ?>
		<span class="voucher_info_title wg_ff"><?php wpg_e('Redemption Date:') ?></span> <span class="voucher_info button"><b><?php echo $redemption_data['date'] ?></b></span><br>
        <?php endif;
        if($redemption_data['total'] != FALSE): ?>
		<span class="voucher_info_title wg_ff"><?php wpg_e('Redemption Total:') ?></span> <span class="voucher_info button"><b><?php echo $redemption_data['total'] ?></b></span><br>
        <?endif;
        if($redemption_data['notes']):?>
		<span class="voucher_info_title wg_ff"><?php wpg_e('Redemption Notes:') ?></span> <span class="voucher_info button"><b><?php echo $redemption_data['notes'] ?></b></span><br>
        <?php endif;?>
	</div>
<?php else: ?>
	<form id="claim_voucher"  class="main_block registration_layout"  action="" method="post">

	<table class="">
		<tbody>
			<tr>
				<td class="edit-td">
					<?php wg_form_label('claim', array('label'=>'Security Code'), 'voucher'); ?>
				</td>
				<td class="wg-form-field wg-form-field-text">
					<?php 
						$code = (isset($_GET['wg_voucher_claim'])&&$_GET['wg_voucher_claim']!='') ? $_GET['wg_voucher_claim'] : '' ;
						wg_form_field('claim', array('type'=>'text','default'=>$code), 'voucher'); 
						?>
				</td>
			</tr>
			<tr>
				<td>
					<?php wg_form_label('data[name]', array('label'=>'Redeemers Name'), 'voucher_redemption'); ?>
				</td>
				<td class="wg-form-field wg-form-field-text"><?php wg_form_field('data[name]', array('type'=>'text'), 'voucher_redemption'); ?></td>
			</tr>
			<tr>
				<td>
					<?php wg_form_label('data[date]', array('label'=>'Redeemers Date'), 'voucher_redemption'); ?>
				</td>
				<td class="wg-form-field wg-form-field-text">
					<?php wg_form_field('data[date]', array('type'=>'text'), 'voucher_redemption'); ?>
				</td>
			</tr>
			<tr>
				<td>
					<?php wg_form_label('data[total]', array('label'=>'Total Paid'), 'voucher_redemption'); ?>
				</td>
				<td class="wg-form-field wg-form-field-text">
					<?php wg_form_field('data[total]', array('type'=>'text'), 'voucher_redemption'); ?>
				</td>
			</tr>
			<tr>
				<td>
					<?php wg_form_label('data[notes]', array('label'=>'Notes'), 'voucher_redemption'); ?>
				</td>
				<td class="wg-form-field wg-form-field-text">
					<?php wg_form_field('data[notes]', array('type'=>'textarea'), 'voucher_redemption'); ?>
				</td>
			</tr>
		</tbody>
	</table>
	<?php 
		if ( isset($_GET['redirect_to']) && $_GET['redirect_to'] != '') {
			echo '<input type="hidden" value="'.$_GET['redirect_to'].'">';
		}
	 ?>
	
    <div class="" align="center">
	<input type="submit" class="profileAddGoldV6" value="<?php wpg_e('Claimed') ?>" />
</div>

</form>
<?php endif ?>

