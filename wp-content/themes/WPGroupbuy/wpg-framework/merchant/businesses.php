<?php get_header(); ?>


<div class="main">
  <div class="block-content">
  <?php if ( ! have_posts() ) : ?>
            <div class="lbreakcumb fixPNG mBottom10" id="catFilter">
              <div class="rbreakcumb fixPNG">
                <div class="cenbreakcumb fixPNG">
                  <span class="mLeft10"><?php wpg_e('Are you a business owner? Be the first one here, ') ?> </span> <a class="header_edit_link" href="<?php wg_merchant_register_url(); ?>"><?php wpg_e('register your business today.'); ?></a>
                </div>
              </div>
            </div>
        <?php endif; ?>

    <!-- LEFT SIDE -->
    
    <div class="leftcontents">
      <div class="leftcontents" id=""> 
        
        <!-- POST -->        
        <?php while ( have_posts() ) : the_post(); ?>
        <?php get_template_part( 'wpg-framework/inc/loop-merchant', 'wpg-framework/inc/loop-item' ); ?>
        <?php endwhile; ?>
        <?php if (  $wp_query->max_num_pages > 1 ) : ?>
        <?php get_template_part( 'wpg-framework/inc/loop-nav', 'wpg-framework/inc/index-nav' ); ?>
        <?php endif; ?>
        
        <!-- END POST --> 
      </div>
    </div>
    <!-- END LEFT SIDE --> 
    
    <!-- RIGHT SIDE -->
    <div class="right_contents">
    
    <div class="main_view">
      <div class="main-boxcontFull">
        <div class="mBottom15 pTop10">
          <div class="newTitleV6 p0 soldLineNew pBottom10">
            <?php wpg_e('Merchant Types') ?>
          </div>
          <div class="mLeft5">
            <?php if ( have_posts() ) : ?>
				<?php wg_get_all_merchant_types_list( 'ul', 'All', 'pTop10' ) ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
    
      <?php dynamic_sidebar( 'merchant-sidebar' ); ?>
    </div>
    <!-- END RIGHT SIDE -->
    <div class="clear"></div>
  </div>
</div>
<?php get_footer(); ?>
