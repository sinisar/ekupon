<form id="wpg_merchant_register" method="post"  enctype="multipart/form-data" action="<?php wg_merchant_edit_url(); ?>">
	<h2><?php wpg_e('Merchant Information'); ?></h2>
	<input type="hidden" name="wg_merchant_action" value="<?php echo WP_Groupbuy_Merchants_Edit::FORM_ACTION; ?>" />
	<table class="fl">
		<tbody>
			<?php foreach ( $fields as $key => $data ): ?>
				<tr>
					<?php if ( $data['type'] != 'checkbox' ): ?>
						<td class="edit-td"><?php wg_form_label($key, $data, 'contact'); ?></td>
						<td><?php wg_form_field($key, $data, 'contact'); ?></td>
					<?php else: ?>
						<td colspan="2">
							<label for="wg_contact_<?php echo $key; ?>"><?php wg_form_field($key, $data, 'contact'); ?> <?php echo $data['label']; ?></label>
						</td>
					<?php endif; ?>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
    <div class="c"></div>
	<?php self::load_view('merchant/edit-controls', array()); ?>
</form>
