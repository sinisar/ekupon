<?php get_header(); ?>
<div class="main home_ver7">
  <div class="main-content">
    <div class="v6ContentCa mTop10">
      <div class=" weekly2">
        <div class="main-width v7Soldbg  main ">
          <div class="v6TitleCat fl">
            <h2 class="fontNormal">
              <?php the_title() ?>
            </h2>
          </div>
          <div class="merchant-back "><a class="link_block" href="javascript:history.go(-1);" title="<?php wpg_e('Back') ?>"></a></div>
          <div class="c"></div>
          <div class="v6ContentCate pTop5">
            <div class="">
              <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
              
              <div id="" class="merchant-post">
              <div class="blog-post-thumb">
                <?php the_post_thumbnail('wpg_208x120', array('title' => get_the_title())); ?>
              </div>
              
              <div class="blog-post-sub">
              
              </div>
              <div class="merchant-post-content mRight20">
                <?php the_content(); ?>
              </div>
            </div>
            
            <div id="" class="merchant-widget">
            <div class="main_view">
              <div class="main-boxcontFull">
                <div class="mBottom15 pTop10">
                  <div class="newTitleV6 p0 soldLineNew pBottom10"><?php wpg_e('Even More') ?></div>
                  <div class="textwidget mTop10">
                  <?php get_template_part('wpg-framework/inc/social-share') ?>
                  <br clear="all"/>
                  <?php wpg_e('Category:'); ?> <?php wg_get_merchants_types_list(get_the_ID()) ?>
              
              <?php if (wg_has_merchant_website()): ?>
              <?php wpg_e('Links:'); ?>
              <a href="<?php wg_merchant_website() ?>"><?php wpg_e('Website') ?></a> /
              <?php endif ?>
              
              <?php if (wg_has_merchant_facebook()): ?>
              <a href="<?php wg_merchant_facebook() ?>"><?php wpg_e('Facebook') ?></a> /
              <?php endif ?>
              
              <?php if (wg_has_merchant_twitter()): ?>
              <a href="<?php wg_merchant_twitter() ?>"><?php wpg_e('Twitter') ?></a>
              <?php endif ?>
                  </div>
                </div>
              </div>
            </div>
			<?php // dynamic_sidebar( 'merchant-sidebar' ); ?>
            
            </div>
              
              <?php endwhile; // end of the loop. ?>
            </div>
          </div>
          <div class="c"></div>
        </div>
      </div>
      <div>
        <div class="main-width v7Soldbg ">
          <div class="v6TitleCat fl">
            <h2 class="fontNormal"><?php printf(wpg__('Deals offered by %s'), get_the_title() ); ?></h2>
          </div>
          <div class="c"></div>
          <?php 
			$merch_deals = wg_get_merchant_deals_query();
			if ( $merch_deals && $merch_deals->have_posts() ) :
				while ($merch_deals->have_posts()) : $merch_deals->the_post();
					?>
					  <?php get_template_part('wpg-framework/inc/loop-item') ?>
					  <?php
				endwhile;
			else:
		 ?>
          <div class="lbreakcumb fixPNG" id="catFilter">
            <div class="rbreakcumb fixPNG">
              <div class="cenbreakcumb fixPNG"> <span class="mLeft10"><?php printf(wpg__('There are no active deals for %s.'), get_the_title() ); ?></span> </div>
            </div>
          </div>
          <?php
			endif;
			wp_reset_query();
		  ?>
          <div class="c"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
