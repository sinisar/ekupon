<script type="text/javascript" charset="utf-8">
	jQuery(document).ready(function($){
		
		var post_url = '<?php echo admin_url() ?>admin-ajax.php';
		
		// After an API key is entered
		jQuery(".deal_publish_button").live('click', function(e) {
			var post_id = $(this).attr("rel");
		    jQuery.ajax({
			    type: "POST",
			    url: post_url,
			    data: {
			    	action: 'wg_deal_publish',
			        deal_id: post_id
			    },
			    cache: false,
			    success: function(html) {
			    	jQuery("#status-" + post_id).html(html).show();
			    }
		    });
		    jQuery(this).removeClass('deal_publish_button');
		    jQuery(this).addClass('deal_suspend_button');
		    return false;
		});
		// After an API key is entered
		jQuery(".deal_suspend_button").live('click', function(e) {
			var post_id = $(this).attr("rel");
			jQuery.ajax({
			    type: "POST",
			    url: post_url,
			    data: {
			    	action: 'wg_deal_draft',
			        deal_id: post_id
			    },
			    cache: false,
			    success: function(html) {
			    	jQuery("#status-" + post_id).html(html).show();
			    }
		    });
			jQuery(this).removeClass('deal_suspend_button');
		    jQuery(this).addClass('deal_publish_button');
		    return false;
		});
	});
</script>
<div id="dashboard_wrap" class="clearfix">
<div>
	<div class="sTitle"><h2><?php wpg_e('Manage Your Deals') ?></h2></div>
	<?php
	
	// Purchase history
		if ( wg_account_merchant_id() ) {
			$md_in = wg_get_merchants_deal_ids(wg_account_merchant_id());
			if ($md_in == -1) {
				echo '<p>'.wpg__('No sales info.').'</p>';
			} else {
				$deals= null;
				$args=array(
					'post_type' => wg_get_deal_post_type(),
					'post__in' => wg_get_merchants_deal_ids(wg_account_merchant_id()),
					'post_status' => array('publish','draft','pending','future','private'),
					'posts_per_page' => -1, // return this many
					
				);
				$deals = new WP_Query($args);
				if ($deals->have_posts()) {
					?>
	                
	                <a href="<?php wg_merchant_purchases_report_url( $postID ) ?>" class="fr dsp_inh mTop-35"><?php wpg_e('View Purchase History') ?></a>
				<div class="c"></div>
					<table id="business-deal" class="listingOrder border_1_ddd border_bt_none border_t_none" cellspacing="0" cellpadding="0" width="100%">
						
						<thead>
							<tr align="left">
								<th class="headProfile noneborderbot bRight" width="65px"><?php wpg_e('Status'); ?></th>
								<th class="headProfile noneborderbot bRight"><?php wpg_e('Deal'); ?></th>
								<th class="headProfile noneborderbot bRight" width="70px"><?php wpg_e('Total Sold'); ?></th>
								<th class="headProfile noneborderbot bRight" width="100px"><?php wpg_e('Published Date'); ?></th>
								<th class="headProfile noneborderbot " width="230px"><?php wpg_e('Reports'); ?></th>
							</tr>
						</thead>
						
						<tbody>
						<?php
					while ($deals->have_posts()) : $deals->the_post();
						?>
							<tr>
								<td id="status-<?php echo get_the_ID();?>" class="itemProfile bRight"><?php switch ( get_post_status(get_the_ID())) {
	                                                case 'publish':
	                                                    ?><span class="publish"><?php echo wpg__('Published');?></span><?php
	                                                    $tmp_status = 0;
	                                                    break;
													case 'draft':
	                                                    ?><span class="pending"><?php echo wpg__('Pending');?></span><?php
	                                                    $tmp_status = 1;
	                                                    break;
	                                                default:
	                                                    echo wpg__('Default');
	                                                    break;
	                                            } ?></td>
								<td class="itemProfile bRight">
									<a href="<?php the_permalink() ?>" style="float: left; margin-right: 10px;"><?php the_title() ?></a>
                                    <!--
									<div id="action-dl">
		                                <a href="<?php //wg_deal_edit_url() ?>" class="retinaicon-edit action-ic" title="<?php //wpg_e('Edit deal') ?>">&nbsp;</a>
	                                    <?php //if(get_option(WP_Groupbuy_Theme_UI::ENABLE_MERCHANT_EDIT, 1)){ ?>
		                                <a href="#" class="<?php //if($tmp_status == 1){ echo 'deal_publish_button'; } else {echo 'deal_suspend_button';} ?> alt_button contrast_button retinaicon-retweet action-ic" title="<?php //wpg_e('Change status') ?>" rel="<?php //the_ID() ?>">&nbsp;</a>
	                                    <?php //} ?>
		                            </div>
		                            -->
								</td>
								<td class="itemProfile bRight"><?php wg_number_of_purchases() ?></td>
								<td class="itemProfile bRight"><?php the_date('','',' '). the_time() ?></td>
								<td class="itemProfile ">
									<span class="fl mRight5"><?php wg_merchant_purchase_report_link() ?></span>
									<span class="fl"><?php wg_merchant_voucher_report_link() ?></span>
								</td>
							</tr>
						<?php
					endwhile;
					?>
						</tbody>
					</table>
					<?php
				} else {
					echo '<p>'.wpg__('No sales info.').'</p>';
				}
			}
		} else { ?>
			<?php wpg_e('Are you a business owner?') ?> <a class="header_edit_link" href="<?php wg_merchant_register_url(); ?>"><?php wpg_e('Register your business today.'); ?></a>
		<?php } ?>
</div>
</div>