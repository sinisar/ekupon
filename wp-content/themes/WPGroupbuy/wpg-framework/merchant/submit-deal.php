<form id="wpg_deal_submission" method="post" action="" enctype="multipart/form-data" class="">
	<input type="hidden" name="wg_deal_submission" value="<?php esc_attr_e($form_action); ?>" />
	<table class="collapsable form-table">
		<tbody>
			<?php foreach ( $fields as $key => $data ): ?>
				<tr>
					<?php if ( $data['type'] == 'heading' ): ?>
						<td colspan="2"><div class="sTitle"><h2><?php echo $data['label']; ?></h2></div></td>
					<?php elseif( $data['type'] != 'checkbox' ): ?>
						<?php if ( isset( $data['label'] ) && $data['label'] ): ?>
							<td class="edit-td"><?php wg_form_label( $key, $data, 'deal' ); ?></td>
							<td><?php wg_form_field($key, $data, 'deal'); ?></td>
						<?php else: ?>
							<td colspan='2'><?php wg_form_field($key, $data, 'deal'); ?></td>
						<?php endif ?>
					<?php else: ?>
						<td colspan="2">
							<label for="wg_contact_<?php echo $key; ?>"><?php wg_form_field($key, $data, 'deal'); ?> <?php echo $data['label']; ?></label>
						</td>
					<?php endif; ?>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<?php self::load_view('merchant/submit-controls', array()); ?>
</form>