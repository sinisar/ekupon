<div class="main_view">
  <div class="main-boxcontFull">
    <div class="contents pTop10"> <a href="<?php the_permalink() ?>" class="v7linkSold">
      <?php the_title() ?>
      </a>
      <div class="blog-post-sub">
        <?php if (wg_has_merchant_website()): ?>
					<a href="<?php wg_merchant_website() ?>"><?php wpg_e('Website') ?></a> /
				<?php endif ?>
				<?php if (wg_has_merchant_facebook()): ?>
					<a href="<?php wg_merchant_facebook() ?>"><?php wpg_e('Facebook') ?></a> /
				<?php endif ?>
				<?php if (wg_has_merchant_twitter()): ?>
					<a href="<?php wg_merchant_twitter() ?>"><?php wpg_e('Twitter') ?></a>
				<?php endif ?>
      </div>
      <div>
        <div id="" class="blog-post">
          <div class="blog-post-thumb"> <a href="<?php the_permalink() ?>">
            <?php the_post_thumbnail('wpg_208x120', array('title' => get_the_title())); ?>
            </a></div>
          <div class="merchant-post-content">
            <?php the_excerpt(); ?>
            <p><a href="<?php the_permalink() ?>" class="biz_moreinfo wg_ff alignright"><?php wpg_e('Read More') ?></a></p>
          </div>
        </div>
      </div>
      <div class="c"></div>
    </div>
  </div>
</div>