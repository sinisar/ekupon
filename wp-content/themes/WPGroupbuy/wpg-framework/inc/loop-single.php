<?php the_content(); ?>

<?php if ( comments_open() || '0' != get_comments_number() ) : ?>
	<div id="comments_wrap"  class="border_top clearfix">
<?php comments_template( '', true ); ?>
	</div>
<?php endif; ?>