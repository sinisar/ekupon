<?php global $wp; ?>
<?php if ( is_user_logged_in() ) : ?>

<div class="main_view">
  <div class="main-boxcontFull">
    <div class="mBottom15 pTop10">
      <div class="newTitleV6 p0 soldLineNew pBottom10">
        <?php wpg_e('Account Menu') ?>
      </div>
      <div class="mLeft5">
        <ul class="helpPayBox">
          <li class="EditAcc">
            <div class="icon"><a href="<?php wg_account_edit_url(); ?>">
              <?php wpg_e('Edit Account Profile') ?>
              </a></div>
          </li>
          <li class="voucher_ico">
            <div class="icon"><a href="<?php wg_voucher_url(); ?>">
              <?php wpg_e('My Vouchers') ?>
              </a></div>
          </li>
          <!--
          <li class="salerpt">
            <div class="icon fixPNG"><a href="<?php wg_credits_report_url() ?>">
              <?php wpg_e('Credit Report') ?>
              </a></div>
          </li>
        -->
          <li class="ShopCart">
            <div class="icon"><a href="<?php wg_cart_url(); ?>">
              <?php wpg_e('My Cart') ?>
              </a></div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>

<?php if (wg_account_has_merchant()): ?>
<div class="main_view">
  <div class="main-boxcontFull">
    <div class="mBottom15 pTop10">
      <div class="newTitleV6 p0 soldLineNew pBottom10">
        <?php wpg_e('Merchant Menu') ?>
      </div>
      <div class="mLeft5">
        <ul class="helpPayBox">
        <li class="biz_ico">
            <div class="icon"><a href="<?php wg_merchant_account_url(); ?>" >
              <?php wpg_e('Business Dashboard') ?>
              </a></div>
          </li>
          <li class="EditAcc">
            <div class="icon"><a href="<?php wg_merchant_edit_url(); ?>" >
              <?php wpg_e('Edit Business Profile') ?>
              </a></div>
          </li>
            <!--
          <li class="adddeal">
            <div class="icon fixPNG"><a href="<?php wg_deal_submission_url(); ?>">
              <?php wpg_e('Submit Deal') ?>
              </a></div>
          </li>
          -->
          <li class="salerpt">
            <div class="icon fixPNG">
            
              <a href="<?php wg_merchant_purchases_report_url(wg_account_merchant_id()); ?>"><?php wpg_e('View My Sales:'); ?> </a> <?php wg_merchant_total_sold(); ?>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<?php endif ?>

<?php if ( is_super_admin() ) : ?>
<div class="main_view">
  <div class="main-boxcontFull">
    <div class="mBottom15 pTop10">
      <div class="newTitleV6 p0 soldLineNew pBottom10">
        <?php wpg_e('Admin Menu') ?>
      </div>
      <div class="mLeft5">
        <ul class="helpPayBox">
          <li class="wpad">
            <div class="icon"><a target="_blank" href="<?php $url = admin_url(); echo $url; ?>" >
              <?php wpg_e('WordPress Dashboard') ?>
              </a></div>
          </li>
          <!--
          <li class="users">
            <div class="icon fixPNG"><a href="<?php wg_accounts_report_url(); ?>">
              <?php //wpg_e('All Accounts Report') ?>
              </a></div>
          </li>
          
          <li class="chart">
            <div class="icon fixPNG"><a href="<?php wg_purchases_report_url(); ?>">
              <?php //wpg_e('All Sales Report') ?>
              </a></div>
          </li>
        
          <li class="salerpt">
            <div class="icon fixPNG"><a href="<?php wg_credit_purchases_report_url() ?>">
              <?php //wpg_e('All Credit Payments Report') ?>
              </a></div>
          </li>
        -->
        </ul>
      </div>
    </div>
  </div>
</div>
<?php endif ?>

<?php endif; ?>