<div class="fr">

<!-- Facebook like button -->
<div class="fl">
<div class="fb-root"></div>
<div class="fb-like" data-href="<?php echo wg_get_voucher_permalink() ?>" data-width="100" data-layout="button_count" data-show-faces="false" data-send="false"></div>
</div>

<!-- Tweet button -->
<div class="fl tw_btn">
<a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php echo wg_get_voucher_permalink() ?>" data-text="<?php wpg_e( 'Get the great deal here' ) ?>" data-via="<?php wpg_e( 'viaTweeter' ) ?>" data-hashtags="deal">Tweet</a>
<script type="text/javascript">!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
</div>

<!-- G +1 button -->
<div class="g-plusone fl" data-size="medium" data-href="<?php echo wg_get_voucher_permalink() ?>"></div>
<script type="text/javascript">gapi.plusone.go();</script>

	<!-- OR UNCOMMENT HERE TO USE SHARETHIS SHARING
    
    <span class='st_facebook_hcount' displayText='Facebook' st_url="<?php wg_share_link() ?>"></span>
	<span class='st_twitter_hcount' displayText='Tweet' st_url="<?php wg_share_link() ?>"></span>
	<span class='st_pinterest_hcount' displayText='Pinterest' st_url="<?php wg_share_link() ?>"></span>
	<span class='st_email_button' displayText='Email' st_url="<?php wg_share_link() ?>"></span>-->
</div>