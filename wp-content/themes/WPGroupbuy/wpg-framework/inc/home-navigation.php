<div class="header">
    <div class="headerBg fixPNG ">
      <div class="headerWidth">
        <div class="headerTop">
          <!-- LOGO -->
          <h1 class="headerLogo fl fixPNG m0" style="background: url('<?php wg_header_logo() ?>') no-repeat"> <a href="<?php echo site_url() ?>"><img src="<?php echo get_template_directory_uri(); ?>/style/images/blank.gif" width="216" height="59" alt="<?php bloginfo( 'name' ); ?>" /></a> </h1>
          <!-- END LOGO -->
          
          <!-- MENU -->
          <div class="headerTabMenu fl" id="header-menu">
          <div id="mobile-header">
            <a id="responsive-menu-button" href="#sidr-main"><?php wpg_e( 'Menu' ) ?></a>
          </div>
          <div id="navigation-menu">
          <?php wp_nav_menu( array( 'sort_column' => 'menu_order', 'theme_location' => 'header', 'depth' =>'2', 'container' => 'none' ) ); ?>
          </div>
          <div class="c"></div>
          </div>
          <!-- END MENU -->
          
          <!-- HEADER BUTTON -->
          <div class="headerBarMenu">
          <?php if ( !is_user_logged_in() ): ?>
          <div class="headerInfo mLeft10">
            <div class="bgall">
              <div class="bleft">
                <div class="content"> <a href="<?php wg_account_register_url(); ?>" class="v7linkbtn p20"><?php wpg_e( 'Register' ) ?></a> </div>
              </div>
            </div>
          </div>
          <div class="headerInfo">
            <div class="bgall">
              <div class="bleft">
                <div class="content"> 
                  <a href="<?php wg_account_url() ?>" class="v7linkbtn p20"><?php wpg_e( 'Login' ) ?></a> </div>
              </div>
            </div>
          </div>
          
          <div class="shopnewIconHeader fr mr10">
           <div class="retinaiconbox"><a href="javascript:" onclick="fbActionConnect();" title="<?php wpg_e( 'Login with Facebook' ) ?>"><span class="retinaicon-facebook retinaicon-circ"></span></a></div>
        </div>
        
        <div class="shopnewIconHeader fr mr10">
           <div class="retinaiconbox"><a href="<?php wg_cart_url() ?>" title="<?php wpg_e( 'Your Shopping Cart' ) ?>"><span class="retinaicon-shopping-cart retinaicon-circ"></span><span class="item"><?php wg_cart_item_count() ?></span></a></div> 
        </div>
        
					<?php else: ?>
          <div class="headerInfo mLeft10 <?php if ( !is_user_logged_in() ) echo 'hide'; ?>">
          <div class="bgall">
            <div class="bleft">
              <div class="content"> 
                <div class="btProfile" show="0" onclick="shop.header.actionDisplay.viewMenuUnOrderLike(this,0,event)"> <a href="#" class="fwhite"> <b class="fl"><?php wg_name() ?></b> <span class="c"></span> </a>
                  <div class="ctrlMenu hidden" id="0">
                    <div class="v7arrowMT fixtextRight"></div>
                    <div class="v7buidFocus">
                      <div id="account-menu" class="cusMenu">
                      <ul>
                      <li><a href="<?php wpg_account_link(); ?>"><span class="icAccount"><?php wpg_e( 'Your Account' ) ?></span></a></li>
                      
                      <?php if ( wg_merchant_enabled() ): ?>
                      <li><a href="<?php wg_merchant_account_url(); ?>"><span class="icShop"><?php wpg_e( 'Business Account' ) ?></span></a></li>
                      <?php endif; ?>
                      
                      <li><a href="<?php wg_voucher_url(); ?>"><span class="icCoupon"><?php wpg_e( 'My Vouchers' ) ?></span></a></li>
                      <?php if ( is_super_admin() ) : ?>
                      <li><a href="<?php echo admin_url(); ?>" target="_blank"><span class="icWP"><?php wpg_e( 'WP Admin' ) ?></span></a></li>
                      <?php endif ?>
                       <li><?php wg_logout_url(); ?></li>
                       </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="c"></div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="shopnewIconHeader fr mr10">
           <a href="<?php wg_cart_url() ?>" title="<?php wpg_e( 'Your Shopping Cart' ) ?>"><div class="retinaiconbox"><span class="retinaicon-shopping-cart retinaicon-circ"></span><span class="item"><?php wg_cart_item_count() ?></span></div> </a>
        </div>
		<?php endif ?>
        
          <div class="c"></div>
        </div>
        </div>
        <!-- END HEADER BUTTON -->
        
        <!-- HEADER BOTTOM -->
        <div class="headerBottom">
        <div class="headerBottomW">
          <div class="headerFloat">
          
          <!-- CATEGORY -->
            <div class="fixPNG topCat topCatHover">
                <div class="title fixPNG" onclick="shop.header.menuCatClick()">
                <div><?php wpg_e( 'Deals Menu' ) ?></div>
              </div>
              <div id="category-menu" class="content">
              <?php
				if ( has_nav_menu( 'dealmenu' ) ) {
					wp_nav_menu( array( 'sort_column' => 'menu_order', 'theme_location' => 'dealmenu', 'depth' =>'2', 'container' => '' ) );
				} else{
					wg_list_categories();
				} ?>
                <div class="c"></div>
              </div>
            </div>
            <!-- END CATEGORY -->
            
            <!-- HOME SIDEBAR -->
            <?php dynamic_sidebar( 'home-sidebar' );?>
            <!-- END HOME SIDEBAR -->
            
          </div>
          <!--Fix IE 7-->
          <div class="c"></div>
          <div>
          <!-- LOCATION -->
          <?php $locations = wg_get_locations();
			if ( !empty( $locations ) && !is_wp_error( $locations ) ) : ?>
            <div class="headerCity" onmouseover="shop.header.city.menuCity(this,true)" onmouseout="shop.header.city.menuCity(this,false)">
              <div class="title">
                <div class="currentBg"><?php wg_current_location_extended(); ?></div>
              </div>
              <div id="location-menu" class="content hidden"> <?php wg_list_locations(); ?> </div>
            </div>
             <?php endif; ?>
            <!-- END LOCATION -->
            
            <!-- SUBSCRIPTION -->
            <?php //if ( !is_user_logged_in() ): ?>
            <a href="javascript:void(0)" class="regMailButton" rel="nofollow" onclick="shop.header.email.hideFormEmail()"><?php wpg_e( 'Get the Latest Deals' ) ?></a>
            <?php // endif ?>
            <!-- END SUBSCRIPTION -->
            
            <!-- SEARCH BOX -->
            <div id="search-box-menu" class="searchBoxTop">
			<?php get_search_form(); ?>
            </div>
            <!-- END SEARCH BOX -->
          </div>
          <div class="c"></div>
        </div>
        <!-- END HEADER BOTTOM -->
        
      </div>
    </div>
    </div>
    </div>
    <div class="c"></div>
    <div id="txtaccmenu"><h1><?php wpg_e( 'Account Menu' ) ?></h1></div>
    <div id="txtmainmenu"><h1><?php wpg_e( 'Main Menu' ) ?></h1></div>
    <div id="txtcatmenu"><h1><?php wpg_e( 'Category Menu' ) ?></h1></div>
    <div id="txtcitymenu"><h1><?php wpg_e( 'Chose Location' ) ?></h1></div>
    
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.sidr.min.js"></script>
    <?php if (is_user_logged_in()) { ?>
		<script type="text/javascript">
        jQuery('#responsive-menu-button').sidr({
            name: 'sidr-main',
            source: '#search-box-menu, #txtmainmenu, #navigation-menu, #txtcatmenu, #category-menu, #txtcitymenu, #location-menu, #txtaccmenu, #account-menu'
        });
        </script>
    <?php } else { ?>
		<script type="text/javascript">
        jQuery('#responsive-menu-button').sidr({
            name: 'sidr-main',
            source: '#search-box-menu, #txtmainmenu, #navigation-menu, #txtcatmenu, #category-menu, #txtcitymenu, #location-menu'
        });
        </script>
    <?php } ?>