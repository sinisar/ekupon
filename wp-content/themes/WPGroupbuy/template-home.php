<?php 
/**
* Template Name: Home style 1
**/
get_header(); ?>
<div class="main home_ver7">
  <div class="main-width">
    <div class="block-content">
      <div class="left-box"> </div>
      <div class="right-box"> 
        
        <!-- FEATURED ITEM -->
        <?php
				$deal_query= null;
				$args=array(
					'post_type' => wg_get_deal_post_type(),
					'orderby' => 'rand', // Show random
					'posts_per_page' => -1, //Show all featured deal
					'post_status' => 'publish',
					'tax_query' => array(
						array(
							'taxonomy' => wg_get_deal_cat_slug(),
							'terms' => wg_get_featured_cat(),
							'field' => 'id',
						)
					),
					'meta_query' => array(
						array(
							'key' => '_expiration_date',
							'value' => array(0, current_time('timestamp')),
							'compare' => 'NOT BETWEEN'
						)),
				);
				if ( wg_has_location_preference() ) {
					$location = wg_get_preferred_location();
					$args = array_merge( array(wg_get_deal_location_tax() => $location), $args);
				}
				$deal_query = new WP_Query($args);
		?>
        <?php while ( $deal_query->have_posts() ) { $deal_query->the_post();
          $tmp_max_purchases = get_post_meta( $post->ID, '_max_purchases', true ); 
          if (get_post_meta( $post->ID, '_number_of_purchases', true )) { 
            $tmp_number_of_purchases = get_post_meta( $post->ID, '_number_of_purchases', true );
          } else{
            $tmp_number_of_purchases = 0;
          }
          if( $tmp_max_purchases > $tmp_number_of_purchases){ 
             get_template_part( 'wpg-framework/deals/featureddeal' ); 
             break; 
           } 
         } 
        wp_reset_query(); ?>
        
        <!-- END FEATURED ITEM --> 
        
      </div>
      <div class="clear"></div>
    </div>
    <div></div>
  </div>
  
  <!-- MAIN CONTENT -->
  <div class="main-content"> 
    
    <!-- ADS BANNER -->
    <?php dynamic_sidebar( 'ads-home' ); ?>
    <!-- END ADS BANNER --> 
    
    <!-- DEALS BLOCK -->
    <div class="mTop20 pTop15">
      <?php
	  $taxid = get_the_terms( $post->ID , wg_get_deal_cat_slug() );
		if($taxid) {
			foreach( $taxid as $xid ) {
				echo $xid->term_id;
			}
		}
	  // for a given post type, return all
		$post_type = wg_get_deal_post_type();
		$tax = wg_get_deal_cat_slug();
		
		$tax_terms = get_terms(
			$tax, array(
				'hide_empty' => 1,
				'orderby' => 'name',
				'order' => 'ASC',
				'exclude' => wg_get_featured_cat(),
				'parent' => 0,
			));
					
		if ($tax_terms) {
			foreach ($tax_terms as $tax_term) {
				$args = array(
					'post_type' => $post_type,
					"$tax" => $tax_term->slug,
					'post_status' => 'publish',
					'posts_per_page' => 8,
					'orderby' => 'rand',
					'ignore_sticky_posts' => 1,
					'meta_query' => array(
						array(
							'meta_key' => '_expiration_date',
							'meta_value' => array(0, current_time('timestamp')),
							'compare' => 'NOT BETWEEN'
						)),
					); // END $args
					
				$my_query = null;
				if ( wg_has_location_preference() ) {
					$location = wg_get_preferred_location();
					$args = array_merge( array(wg_get_deal_location_tax() => $location), $args);
				}
				$my_query = new WP_Query($args);
				
				if ($my_query->have_posts()) { ?>
      <div class="weekly <?php evenodd(); ?>" style="text-align: center;">
        <div class="homecate_c">
          <div id="scrollbox<?php echo $tax_term->term_id; ?>" class="zi10">
            <div class="v6TitleCate" id="dockCat<?php echo $tax_term->term_id; ?>">
              <h2 class="fl"><?php echo $tax_term->name; ?></h2>
              <div class="fl link"> <a href="<?php echo get_term_link( $tax_term->slug, wg_get_deal_cat_slug() ) ?>" class="fl v7linkcate">
                <?php wpg_e('View More') ?>
                </a> </div>
              <div class="c"></div>
            </div>
            <div class="c"></div>
          </div>
          <div class="v6ContentCate pTop5">
            <div id="ca-container-<?php echo $tax_term->term_id; ?>" class="ca-container">
              <div class="ca-wrapper">
                <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
                <!-- DEAL ITEM -->
                <div class="ca-item ca-item-<?php the_ID() ?>">
                
                <!-- ITEM -->
                  <div class="ca-item-main"> <a href="<?php the_permalink() ?>" >
                    <div class="v7price_promotionsmall">
                      <?php wg_amount_saved() ?>
                    </div>
                    <div class="ca-icon">
                      <?php if ( has_post_thumbnail() ): ?>
                      <?php the_post_thumbnail('wpg_210x158') ?>
                      <?php else : ?>
                      <div class="no_img 210"><img alt="no-img" src="<?php echo get_template_directory_uri(); ?>/style/images/background/no_img_210.png" /></div>
                      <?php endif; ?>
                    </div>
                    </a> <a class="deal_title" href="<?php the_permalink() ?>" >
                    <?php the_title() ?>
                    </a>
                    <div class="fl pTop5">
                      <div class="v7PromotionSmall">
                        <?php wg_price(); ?>
                      </div>
                      <div class="v7bnew f12">
                        <?php wpg_e('Bought:') ?>
                        <b>
                        <?php wg_number_of_purchases() ?>
                        </b></div>
                    </div>
                    <a href="#" class="ca-more">
                    <?php wpg_e('View') ?>
                    </a> </div>
                    <!-- END ITEM -->
                    
                    <!-- MORE INFO ITEM -->
                  <div class="ca-content-wrapper">
                    <div class="ca-content">
                      <h6>
                        <?php the_title() ?>
                      </h6>
                      <a href="#" class="ca-close">
                      <?php wpg_e('close') ?>
                      </a>
                      
                      <div class="ca-column">
                        <?php 
							$content = get_the_content();
							$content = apply_filters('the_content', substr($content, 0, 500 ));
							echo $content;
						?>
                        <a href="<?php the_permalink() ?>" >
                        <?php wpg_e('Read more') ?>
                        </a>
                        <div class="c"></div>
                        <div class="fl mTop10">
                          <?php get_template_part('wpg-framework/inc/social-share') ?>
                        </div>
                      </div>
                      
                      <div class="ca-column2">
                        <div class="v6BuyNow fr">
                          <div class="shopinfo">
                            <div class="shopBuy" style="text-align: center;">
                              <div class="shopTitleInfo">
                                <?php wpg_e('Value') ?>
                              </div>
                              <div class="shopMoreInfo"> <?php echo str_replace('.00','',wg_get_formatted_money(wg_get_deal_worth())) ?> </div>
                            </div>
                            <div class="shoppromotion" style="text-align: center;">
                              <div class="shopTitleInfo">
                                <?php wpg_e('Discount') ?>
                              </div>
                              <div class="shopMoreInfo">
                                <?php wg_amount_saved() ?>
                              </div>
                            </div>
                            <div class="shoptime" style="text-align: center;">
                              <div class="shopTitleInfo shopTitleTimeInfo">
                                <?php wpg_e('You save') ?>
                              </div>
                              <div class="shopMoreInfo"> <?php echo str_replace('.00','',wg_get_formatted_money(wg_get_deal_worth() - wg_get_price())) ?> </div>
                            </div>
                          </div>
                          
                          <!-- COUNTDOWN -->
                          <?php if ( wg_deal_availability() && wg_has_expiration() ): ?>
                          <div class="v6BorderBot mBottom20 border_bt_none">
                            <div class="v6Timer">
                              <div class="v6Gray fl"></div>
                              <div class="v6DisplayTime">
                                <?php wg_deal_countdown(true); ?>
                                <noscript>
                                <?php wg_get_deal_end_date(); ?>
                                </noscript>
                              </div>
                              <div class="c"></div>
                            </div>
                          </div>
                          <?php endif ?>
                          <!-- END COUNTDOWN -->
                          
                          <?php if ( wg_is_sold_out() ) : ?>
                          <a href="#">
                          <div class="retinaiconbox soldout_btn"><span class="retinaicon-shopping-cart"></span> <span class="buynow_text">
                            <?php wpg_e('Sold out') ?>
                            </span></div>
                          </a>
                          <?php elseif ( wg_deal_availability() && !wg_is_deal_complete() ): ?>
                          <a href="<?php wg_add_to_cart_url() ?>">
                          <div class="retinaiconbox buynow_btn"><span class="retinaicon-shopping-cart"></span> <span class="buynow_text">
                            <?php wpg_e('Buy now') ?>
                            </span></div>
                          </a>
                          <?php else : ?>
                          <a href="#">
                          <div class="retinaiconbox soldout_btn"><span class="retinaicon-shopping-cart"></span> <span class="buynow_text">
                            <?php wpg_e('Unavailable') ?>
                            </span></div>
                          </a>
                          <?php endif ?>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- END MORE INFO ITEM -->
                </div>
                <!-- END DEAL ITEM -->
                <?php endwhile; ?>
              </div>
            </div>
            <div class="c"></div>
          </div>
        </div>
      </div>
      <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.contentcarousel.js"></script> 
      <script type="text/javascript">
			jQuery('#ca-container-<?php echo $tax_term->term_id; ?>').contentcarousel();
	  </script>
      <?php } // END if have_posts loop
				wp_reset_query();
			} // END foreach $tax_terms
		} // END if $tax_terms
		
		?>
    </div>
    <!-- END DEALS BLOCK --> 
  </div>
  <!-- END MAIN CONTENT --> 
  
</div>
<?php get_footer(); ?>