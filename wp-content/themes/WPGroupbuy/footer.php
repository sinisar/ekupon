<div class="c"></div>
<!-- FOOTER -->
<div class="footer">
  <div id="copyright">
    <div class="centerFooters footerWidth mTop20 mBottom10 lh2">
      <div class="fl byFooters">
        <?php dynamic_sidebar( 'deal_footer_one' ); ?>
      </div>
      <div class="fl byFooters2">
        <?php dynamic_sidebar( 'deal_footer_two' ); ?>
      </div>
      <div class="fl byFooters2">
        <?php dynamic_sidebar( 'deal_footer_three' ); ?>
       </div>
       <div class="fl byFooters2">
        <?php dynamic_sidebar( 'deal_footer_four' ); ?>
       </div>
       <div class="fl byFooters2">
        <?php dynamic_sidebar( 'deal_footer_five' ); ?>
       </div>
      
      <div class="c"></div>
    </div>
    <div class="bottom-footer pTop5">
      <div class="content-copyright">
        <div id="footer_nav"  class="fl"><?php wpg_e( '&copy;' ) ?> <a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a> <?php printf( wpg__( '%s.  All rights reserved.' ), date( 'Y' ) );?> <br/><?php wp_nav_menu( array( 'sort_column' => 'menu_order', 'theme_location' => 'footer', 'depth' =>'1', 'container' => 'none' ) ); ?></div>
        <div class="fr"><a href="http://www.wpgroupbuy.com" target="_blank" rel="follow" title="<?php wpg_e( 'WordPress Groupbuy Theme' ) ?>"><img src="<?php echo get_template_directory_uri(); ?>/style/images/wpg-footer.png" alt="<?php wpg_e( 'WordPress Groupbuy Theme' ) ?>" width="92" height="36"/></a></div>
        <div class="c"></div>
      </div>
    </div>
  </div>
</div>
<!-- END FOOTER -->
</div>

<?php wp_footer(); ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.easing.1.3.js"></script>
<a href="#" class="scrollup"><?php wpg_e( 'Back on Top' ) ?></a>
</body>
</html>