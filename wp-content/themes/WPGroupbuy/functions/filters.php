<?php
 
// Custom colors array
function wg_custom_color_registrations() {
	$colors = array(
		'wpg_heading' => array(
			'selectors' => 'h1, h2, h3, h4, h5',
			'name' => wpg__( 'Headings Font Face' ),
			'rules' => array(
				'font-family' => 'Arial',
			)
		),
		'body_rt_ff' => array(
			'selectors' => 'body, .body',
			'name' => wpg__( 'Body/Default Font Face' ),
			'rules' => array(
				'font-family' => 'Arial',
			)
		),
		'body' => array(
			'selectors' => 'body',
			'name' => wpg__( 'Site Body' ),
			'rules' => array(
				'background-color' => 'e7e7e7',
				'color' => '555555',
			)
		),
		'footer_text' => array(
			'selectors' => '.centerFooters',
			'name' => wpg__( 'Footer Text' ),
			'rules' => array(
				'color' => '000000',
			)
		),
		'heading' => array(
			'selectors' => 'h1, h2, h3, h4, h5',
			'name' => wpg__( 'Headings' ),
			'rules' => array(
				'color' => '404040',
			)
		),
		'a_link' => array(
			'selectors' => 'a',
			'name' => wpg__( 'A Link' ),
			'rules' => array(
				'color' => '00a0dc',
			)
		),
		'header' => array(
			'selectors' => '.headerBg',
			'name' => wpg__( 'Header Background' ),
			'rules' => array(
				'background-color' => '0072b0',
			)
		),
		'header_bottom' => array(
			'selectors' => '.headerBottom',
			'name' => wpg__( 'Header Bottom Background' ),
			'rules' => array(
				'background-color' => 'FFF',
			)
		),
		'header_menu' => array(
			'selectors' => '#header-menu ul li a, .headerInfo .content a.fwhite',
			'name' => wpg__( 'Main Menu' ),
			'rules' => array(
				'color' => 'ffffff',
			)
		),
		'header_sub_menu' => array(
			'selectors' => '#header-menu ul li ul.sub-menu li a, #header-menu ul.menu li.current_page_item ul.sub-menu li a',
			'name' => wpg__( 'Sub-menu' ),
			'rules' => array(
				'background' => '0068a8',
			)
		),
		'header_sub_menu_hover' => array(
			'selectors' => '#header-menu ul li ul.sub-menu li a:hover, #header-menu ul.menu li.current_page_item ul.sub-menu li a:hover',
			'name' => wpg__( 'Sub-menu hover' ),
			'rules' => array(
				'background-color' => '0068a8',
			)
		),
		'header_sub_menu_line' => array(
			'selectors' => '#header-menu ul li ul.sub-menu li a',
			'name' => wpg__( 'Sub-menu Solid' ),
			'rules' => array(
				'border-top-color' => '0170b5'
			)
		),
		'header_acc_btn' => array(
			'selectors' => '.headerInfo .bgall',
			'name' => wpg__( 'Account Button' ),
			'rules' => array(
				'background-color' => '0072b0'
			)
		),
		'header_btn' => array(
			'selectors' => '.shopnewIconHeader .retinaiconbox .retinaicon-circ',
			'name' => wpg__( 'Login/Cart Button' ),
			'rules' => array(
				'background-color' => '006198',
				'color' => 'abc8d7'
			)
		),
		'main_buynow_btn' => array(
			'selectors' => '.buynow_btn',
			'name' => wpg__( 'Buy Now Button' ),
			'rules' => array(
				'background-color' => '629607',
			)
		),
		'main_soldout_btn' => array(
			'selectors' => '.soldout_btn',
			'name' => wpg__( 'Sold Out Button' ),
			'rules' => array(
				'background-color' => '525252',
			)
		),
		'main_deal_btn' => array(
			'selectors' => '.ca-more, .ca-more-2',
			'name' => wpg__( 'View Deal Button' ),
			'rules' => array(
				'background-color' => '0072b0',
				'color' => 'FFFFFF'
			)
		),
		'footer' => array(
			'selectors' => '#copyright',
			'name' => wpg__( 'Footer Areas' ),
			'rules' => array(
				'background-color' => 'FFFFFF',
			)
		),
	);
	return apply_filters( 'wg_platinum_flavor_options', $colors );
}

// Removes the wrapper div from wp_page_menu
add_filter( 'wp_page_menu', 'remove_container', 10, 2 );
function remove_container( $menu, $args ) {
	$menu = strip_tags( $menu, '<li><a>' );
	return '<ul class="'.$args['menu_class'].'">'.$menu.'</ul>';
}

// add choose category field
add_filter( 'wg_deal_submission_fields', 'deal_submission_categories', 10, 2 );
add_action( 'submit_deal', 'save_new_fields', 10, 1 );
add_action( 'edit_deal', 'save_new_fields', 10, 1 );
function deal_submission_categories( $fields, $deal = null) {
    $args=array(
        'orderby' => 'name',
        'hide_empty'=> 0,
        'parent' => 0,
        'order' => 'ASC',
        'taxonomy' => 'wg_category'
    );
    $wg_cats = array();
    $categories=get_categories($args);
    foreach($categories as $category){
    	$wg_cats[$category->slug] = $category->name;
    }
    array_unshift($wg_cats, "Choose category");
    
    if ( $deal ) {
    	$terms = wp_get_object_terms( $deal->get_id(), 'wg_category' );
    }
    
    $cat = !empty( $terms ) ? $terms[0]->slug : '';
    
    $fields['deal_cat'] = array(
        'weight' => 3,
        'label' => 'Category:',
        'type' => 'select',
        'required' => FALSE, // change to TRUE if this is a required field
        'options' => $wg_cats,
        'default' => $cat,
        'description' => 'Select your deal\'s category'
    );
    return $fields;
}
function save_new_fields( WP_Groupbuy_Deal $deal ) {
	$cat = isset( $_POST['wg_deal_deal_cat'] ) ? $_POST['wg_deal_deal_cat'] : '';
	$cat = get_term_by( 'slug', $cat, 'wg_category' );
	wp_set_object_terms( $deal->get_id(), $cat->term_id, 'wg_category' );
}

/* Remove .00 from price

add_filter('wg_get_formatted_money','unformat_money',10,2); 
function unformat_money($string,$amount) { 
    $symbol = wg_get_currency_symbol();
    $money = $symbol . $amount;
	// $money = $symbol . number_format($amount, 0, '', ' '); // add space in thousand
    return $money; 
}
*/

/* Active all voucher code for offline payment method

add_action('init', 'activate_vouchers_init_cb', 1000);  
function activate_vouchers_init_cb(){  
    add_action( 'purchase_completed', 'activate_all_purchases', 100, 1 ); 
}  
function activate_all_purchases( WP_Groupbuy_Purchase $purchase ) {  
    $items_captured = array(); // Creating simple array of items that are captured 
    foreach ( $purchase->get_products() as $item ) { 
        $items_captured[] = $item['deal_id']; 
    } 
    $payments = WP_Groupbuy_Payment::get_payments_for_purchase( $purchase->get_id() ); 
    foreach ( $payments as $payment_id ) { 
        $payment = WP_Groupbuy_Payment::get_instance( $payment_id ); 
        do_action( 'payment_authorized', $payment ); 
        do_action( 'payment_captured', $payment, $items_captured ); 
        do_action( 'payment_complete', $payment ); 
        $payment->set_status( WP_Groupbuy_Payment::STATUS_COMPLETE ); 
        WP_Groupbuy_Vouchers::activate_vouchers( $payment, $items_captured ); 
    } 
}
*/