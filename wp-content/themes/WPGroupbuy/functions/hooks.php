<?php

// Messaging
remove_action( 'loop_start', array( 'WP_Groupbuy_Controller', 'do_loop_start' ) );

// Location redirects
// redirect from homepage
add_action( 'pre_wpg_head', 'wg_redirect_from_home' );
// Set location
add_action( 'wg_deal_view', 'wg_set_location_preference' );
add_action( 'wg_deals_view', 'wg_set_location_preference' );

function wg_must_login() {
	return (( !is_user_logged_in() && wg_force_login_option() != 'false' ) && 
		( wg_force_login_option() == 'subscriptions' && is_singular('wg_deal') ) && 
		!( wg_on_login_page() || wg_on_registration_page() || wg_on_reset_password_page() ) );
}

// Redirect to the latest deal
function wg_redirect_from_home() {
	// Redirect if login required.
	if ( wg_must_login() ) {
		wg_set_message( wpg__( 'Membership Required! Please login or registration.' ));
		wg_login_required();
		return;
	} elseif ( is_home() && ( is_user_logged_in() || wg_has_location_preference() ) ) {
		// Redirect logged in users
		// wp_redirect( apply_filters( 'wg_latest_deal_redirect', wg_get_latest_deal_link() ) );
		// exit();
	}

}

// Set the users location
function wg_set_location_preference( $location = null ) {

	$cookie_time = apply_filters( 'wg_set_location_preference_time', time() + 24 * 60 * 60 * 30 );

	if ( !headers_sent() ) {
		if ( !$location ) {
			if ( !empty( $_GET['location'] ) ) {
				$location = $_GET['location'];
			} elseif ( is_tax() ) {
				global $wp_query;
				$wp_query_ob = $wp_query->get_queried_object();
				if ( $wp_query_ob->taxonomy == wg_get_deal_location_tax() ) {
					$location = $wp_query_ob->slug;
				}
			}
		}
		
		setcookie( 'wg_location_preference', $location, $cookie_time, '/' );
		do_action( 'wg_set_location_preference', $location );
	}

	return $location;
}

// Get location preference based on cookie
function wg_get_location_preference( $user_id = null ) {
	if ( isset( $_COOKIE[ 'wg_location_preference' ] ) && $_COOKIE[ 'wg_location_preference' ] != '' ) {
		return $_COOKIE[ 'wg_location_preference' ];
	}
	return FALSE;
}

// Does user set location
function wg_has_location_preference( $user_id = null ) {
	if ( FALSE != wg_get_location_preference() ) {
		return TRUE;
	}
	return;
}

// Get location preference based on cookie
function wg_get_session_location_preference( $user_id = null ) {
	if ( isset( $_SESSION['wg_session_location_preference'] ) && $_SESSION['wg_session_location_preference'] != '' ) {
		return $_SESSION['wg_session_location_preference'];
	}
	return FALSE;
}

// Does user set location
function wg_has_session_location_preference( $user_id = null ) {
	if ( FALSE != wg_get_session_location_preference() ) {
		return TRUE;
	}
	return;
}

// Filter WP classes
add_filter ( 'post_class' , 'wg_extend_post_class' );
add_filter( 'body_class', 'body_browser_classes' );

// Add custom post classes for deals
function wg_extend_post_class( $classes ) {
	global $current_class;
	$classes[] = $current_class;
	$current_class = ( $current_class == 'item_odd' ) ? 'item_even' : 'item_odd'; // Reset
	if ( wg_deal_availability() && wg_has_expiration() ) {
		$end_date = wg_get_expiration_date();
		$difference = $end_date - current_time( 'timestamp' );
		if ( floor( $difference/60/60/24 ) >= 1 ) {
			if ( floor( $difference/60/60/24 ) == 1 ) {
				$classes[] = 'day_remaining';
				$classes[] = ( floor( $difference/60/60/24 ) ) . '-day_remaining';
			} else {
				$classes[] = 'days_remaining';
				$classes[] = ( floor( $difference/60/60/24 ) ) . '-days_remaining';
			}
		} elseif ( floor( $difference/60/60 ) >= 1 ) {
			if ( floor( $difference/60/60 ) == 1 ) {
				$classes[] = 'less_hour_remaining'; // Less than an hour remaining
				$classes[] = 'hour_remaining';
				$classes[] = ( floor( $difference/60/60 ) ) . '-hour_remaining';
			} else {
				$classes[] = 'less_day_remaining';
				$classes[] = 'hours_remaining';
				$classes[] = ( floor( $difference/60/60 ) ) . '-hours_remaining';
			}
		} else {
			if ( floor( $difference/60 ) == 1 ) {
				$classes[] = 'less_hour_remaining'; // Less than an hour remaining
				$classes[] = 'minute_remaining';
				$classes[] = ( floor( $difference/60 ) ) . '-minute_remaining';
			} else {
				$classes[] = 'less_hour_remaining'; // Less than an hour remaining
				$classes[] = 'minutes_remaining';
				$classes[] = ( floor( $difference/60 ) ) . '-minutes_remaining';
			}
		}
	}
	return $classes;
}

// Add browser types to the body class
function body_browser_classes( $classes ) {
	global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;

	// Browser classes
	if ( $is_lynx ) $classes[] = 'lynx';
	elseif ( $is_gecko ) $classes[] = 'gecko';
	elseif ( $is_opera ) $classes[] = 'opera';
	elseif ( $is_NS4 ) $classes[] = 'ns4';
	elseif ( $is_safari ) $classes[] = 'safari';
	elseif ( $is_chrome ) $classes[] = 'chrome';
	elseif ( $is_IE ) {
		$classes[] = 'ie';
		//if the browser is IE6
		if ( strpos( $_SERVER['HTTP_USER_AGENT'], 'MSIE 6' ) ) {
			$classes[] = 'ie6'; //add 'ie6' class to the body class array
		}
		//if the browser is IE7
		if ( strpos( $_SERVER['HTTP_USER_AGENT'], 'MSIE 7' ) ) {
			$classes[] = 'ie7'; //add 'ie7' class to the body class array
		}
		//if the browser is IE8
		if ( strpos( $_SERVER['HTTP_USER_AGENT'], 'MSIE 8' ) ) {
			$classes[] = 'ie8'; //add 'ie8' class to the body class array
		}
	}
	elseif ( $is_iphone ) $classes[] = 'iphone';
	else $classes[] = 'unknown';
	return $classes;
}


// Comments
function group_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;

	switch ( $comment->comment_type ) :
	case '' :
?>
				<li class="comment">
					<div id="comment-<?php comment_ID(); ?>" class="comment-wrap clearfix">

						<div class="comment-content">
							<div class="comment-meta">
								<div class="comment-author-avatar avatar">
									<?php if ( function_exists( 'get_avatar' ) ) {
		echo get_avatar( $comment, 55 );
	}; ?>
								</div>
								<div class="reply_link">
									<?php if ( function_exists( 'comment_reply_link' ) ) {
		comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ), $comment, $post );
	}  ?>
								</div>
								<cite><?php comment_author_link() ?></cite>
								<a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>" class="comment-permalink"><?php comment_date( 'l, F j Y' ); ?></a><?php edit_comment_link( wpg__( '(Edit)' ), '&nbsp;&nbsp;', '' ) ?>
							</div>

						    <div class="comment-text comment-body">
								<?php comment_text() ?>
						        <?php if ( $comment->comment_approved == '0' ) : ?>
						        <p><em><?php wpg_e( 'Your comment is awaiting moderation.' ); ?></em></p>
						        <?php endif; ?>
						    </div>
						</div>
					</div>
				</li>
			<?php
	break;
case 'pingback'  :
case 'trackback' :
?>
			<div id="comment-<?php comment_ID(); ?>" class="trackback comment-wrap <?php cfct_comment_class(); ?> clearfix">
				<div class="comment-content">
					<div class="comment-meta">
						<cite><?php comment_author_link() ?></cite>
						<a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>" class="comment-permalink"><?php printf( wpg__( 'Tracked on %s' ), comment_date() ); ?></a> <?php edit_comment_link( wpg__( '(Edit)' ), '&nbsp;&nbsp;', '' ) ?>
					</div>

				    <div class="comment-text">
						<?php str_replace( '[...]', '...', comment_text() ); ?>
				    </div>
				</div>
			</div>
			<?php
	break;
	endswitch;
}