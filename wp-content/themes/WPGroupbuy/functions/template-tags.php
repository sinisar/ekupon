<?php

// Subscription form
if ( !function_exists('wg_platinum_subscription_form') ) {
	function wg_platinum_subscription_form() {
		global $wp_query;
		$query_slug = $wp_query->get_queried_object()->slug;
		$current_location = ( !empty( $query_slug ) ) ? $query_slug : $_COOKIE[ 'wg_location_preference' ] ;
	?>
	        <form action="" id="wg_subscription_form" method="post" class="clearfix 1">
	            <label for="email_address" class="font_large"><?php wpg_e( 'Signup to get deals sent to you!' ); ?></label>
	            <input type="text" name="email_address" id="email_address" class="text-input" value="<?php wpg_e( 'Enter your email address' ); ?>" onblur="if (this.value == '')  {this.value = '<?php wpg_e( 'Enter your email address' ); ?>';}" onfocus="if (this.value == '<?php wpg_e( 'Enter your email address' ); ?>') {this.value = '';}"/>
				<?php
		if ( $current_location != '' ) {
			echo '<input type="hidden" name="deal_location" value="'.$current_location.'" id="deal_location">';
		} else {
			$locations = wg_get_locations( false );
			$no_city_text = get_option( WP_Groupbuy_List_Services::SIGNUP_CITYNAME_OPTION );
			if ( !empty( $locations ) || !empty( $no_city_text ) ) {
	?>
								<span class="option location_options_wrap">
									<label for="locations"><?php wpg_e( $select_location_text ); ?></label>
									<?php
				$current_location = null;
				if ( isset( $_COOKIE[ 'wg_location_preference' ] ) && $_COOKIE[ 'wg_location_preference' ] != '' ) {
					$current_location = $_COOKIE[ 'wg_location_preference' ];
				} elseif ( is_tax() ) {
					global $wp_query;
					$query_slug = $wp_query->get_queried_object()->slug;
					if ( isset( $query_slug ) && !empty( $query_slug ) ) {
						$current_location = $query_slug;
					}
				}
				echo '<select name="deal_location" id="deal_location" size="1">';
				foreach ( $locations as $location ) {
					echo '<option value="'.$location->slug.'" '.selected( $current_location, $location->slug ).'>'.$location->name.'</option>';
				}
				if ( !empty( $no_city_text ) ) {
					echo '<option value="notfound">'.esc_attr( $no_city_text ).'</option>';
				}
				echo '</select>';
	?>
								</span>
							<?php
			}
		} ?>
	            <input type="hidden" name="single_email" value="" id="single_email">
	            <?php wp_nonce_field( 'wg_subscription' ); ?>
	            <input type="submit" class="submit font_medium" name="wg_subscription" id="wg_subscription" value="<?php wpg_e( 'Signup' ); ?>"/>
	        </form>
	    <?php
	}
}
