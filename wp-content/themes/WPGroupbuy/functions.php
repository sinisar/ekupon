<?php

/*	REQUIRED: Include core files
 	Backward compatible:
	Deactivate plugin if it's v2.3+
	Include core files if it's v2.3+
	Activate plugin if it's v2.2- and not activated.
 */
$core_file = locate_template( 'core/WPGroupbuy.php' );

if ( $core_file ) {
	if ( !defined( 'WG_CORE' ) ) { // Old version
		if ( defined( 'WG_PATH' ) ) {
			if ( include_once( ABSPATH . 'wp-admin/includes/plugin.php' ) ) {
				deactivate_plugins( 'wpgroupbuy-plugin/WPGroupbuy.php', false, is_network_admin() );
				wp_die( 'Old version of WPGropubuy plugin deactivated, please refresh this page.' );
			} else {
				add_action( 'admin_notices', function() {
					echo '<div class="update-nag">Please <a href="'.get_admin_url().'plugins.php">Deactivate</a> WPGroupbuy Plugin</div>';
				});
			}
		} else {
			require_once( $core_file );
		}
	}
} elseif (!defined( 'WG_PATH' ) ) {
	$error = activate_plugin( 'wpgroupbuy-plugin/WPGroupbuy.php' );
	if ( $error ) {
		wp_die( 'Please <a href="'.get_admin_url().'plugin-install.php">Install</a> WPGroupbuy Plugin' );
	} else {
		wp_die( 'WPGropubuy plugin is just activated, please refresh this page.' );
	}
}


if ( !defined( 'WG_PATH' ) ) {
	if ( $core_file ) {
		include_once( $core_file );
	} else {
	}
} elseif ( !defined( 'WG_CORE' ) ) {
}

define( 'WPG_THEME_NAME', 'WPGroupbuy Theme' );
define( 'WPG_THEME_SLUG', 'WPGroupbuy' );
define( 'WPG_THEME_VERSION', '2.3' );
define( 'WG_THEME_COMPAT_VERSION', '2.3' );
define( 'SS_BASE_URL', get_template_directory_uri() . '/' );

// check version requirements
if ( !version_compare( WP_Groupbuy::WG_VERSION, WG_THEME_COMPAT_VERSION, '>=' ) ) {
	if ( !is_admin() && $_SERVER['PHP_SELF'] != '/wp-login.php'  ) {
		wp_die( 'This theme requires WPGroupbuy to be upgraded to at least version '. WG_THEME_COMPAT_VERSION );
	}
	return;
}
function wg_ptheme_current_version() { return WPG_THEME_VERSION; }
// Remove admin bar for users
if ( !is_admin() && !current_user_can( 'edit_posts' ) ) {
	show_admin_bar( false );
}
// Word Limiter
function limit_words($string, $word_limit) {
	$words = explode(' ', $string);
	return implode(' ', array_slice($words, 0, $word_limit));
}

// Style even/odd
function evenodd() {
	global $post_num;
	if ( ++$post_num % 2 )
		$class = 'weekly2';
	else
		$class = ' ';
	echo $class;
}
// Theme Scripts


function wpg_theme_enqueue_scripts() {

	wp_register_style( 'wg-timestamp-jquery-ui-css', WG_RESOURCES . 'css/wpg/jquery-ui.custom.css' );

	wp_enqueue_style( 'bootstrap',  get_template_directory_uri() . '/style/bootstrap.min.css' );
	wp_enqueue_style( 'custom',  get_template_directory_uri() . '/style/custom.css' );
	wp_enqueue_style( 'template_style', get_template_directory_uri().'/style.css', null , wg_ptheme_current_version(), 'screen' );

	if ( get_option(WP_Groupbuy_Theme_UI::ENABLE_RTL, 0) ) {
		wp_enqueue_style( 'media_queries_style_rtl', get_template_directory_uri().'/style/rtl.css', null, wg_ptheme_current_version() );
	}
	
	wp_enqueue_style( 'styles' , get_template_directory_uri() . '/style/styles.css' );
	wp_enqueue_style( 'default' , get_template_directory_uri() . '/style/default.css' );
	wp_enqueue_style( 'jscrollpane' , get_template_directory_uri() . '/style/jquery.jscrollpane.css' );

	if ( TEMPLATEPATH != STYLESHEETPATH  ) {
		wp_enqueue_style( 'wpg_child_style', get_bloginfo( 'stylesheet_url' ), null, wg_ptheme_current_version() );
	}

	// WP Core Scripts
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'jquery-ui-core' );
	wp_enqueue_script( 'jquery-ui-tabs' );

	// WPG Scripts
	wp_enqueue_script( 'bootstrap', get_template_directory_uri().'/js/bootstrap.min.js' );
	wp_enqueue_script( 'wpg-jquery-mobilemenu', get_template_directory_uri() . '/js/jquery.mobilemenu.js', array( 'jquery' ) , wg_ptheme_current_version() );
	wp_enqueue_script( 'wpg-jquery-template', get_template_directory_uri().'/js/jquery.template.js', array( 'jquery', 'jquery-ui-core', 'jquery-ui-tabs' ), wg_ptheme_current_version(), false );
	wp_enqueue_script( 'core', get_template_directory_uri() . '/js/core.js', array( 'jquery' ) );
	wp_enqueue_script( 'shortcodes', get_template_directory_uri() . '/js/shortcodes.js', array( 'jquery' ) );
	wp_enqueue_script( 'fitvids', get_template_directory_uri() . '/js/fitvids.js', array( 'jquery' ) );
	wp_enqueue_script( 'plusone', 'https://apis.google.com/js/plusone.js' );

	// For threaded comments
	if ( is_singular() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	// Localization
	$wpg_template_jquery_translation_array = array( 'used' => wpg__( 'Used ' ) );
	wp_localize_script( 'wpg-jquery-template', 'wpg_js_object', $wpg_template_jquery_translation_array );
}

add_action( 'wp_enqueue_scripts', 'wpg_theme_enqueue_scripts' );

function wg_head_script_variables() {
	?>
	<script type="text/javascript">
		var wg_ajax_url = '<?php echo admin_url() ?>admin-ajax.php';
		//<![CDATA[
		var wg_loading = '<img src="<?php echo get_template_directory_uri() ?>/style/images/loading.gif" alt="Loading" id="ajax_gif" width="16" height="16"/>';
		//]]>
	</script>
	<?php
}
add_action( 'wp_head', 'wg_head_script_variables' );
// Theme Support
// thumbnails size
add_theme_support( 'post-thumbnails' );
add_image_size( 'wpg_48x48', 48, 48, true );
add_image_size( 'wpg_60x60', 60, 60, true );
add_image_size( 'wpg_88x88', 88, 88, true );
add_image_size( 'wpg_208x120', 208, 120, true );
add_image_size( 'wpg_475x475', 475, 475, true ); // Featured
add_image_size( 'wpg_290x290', 290, 290, true ); // Deal thumb
add_image_size( 'wpg_700', 700, 1500, true ); // Deal page size
add_image_size( 'wpg_300x180', 300, 180, true );
add_image_size( 'wpg_250x110', 250, 110, true );
add_image_size( 'wpg_150w', 150, 9999 );
add_image_size( 'wpg_100x100', 100, 100, true );
add_image_size( 'wpg_200x130', 200, 130, true );
add_image_size( 'wpg_200x150', 200, 150, true );
add_image_size( 'wpg_160x100', 160, 100, true );
// RSS feed
add_theme_support( 'automatic-feed-links' );
// Menu
register_nav_menus( array(
	'header' => wpg__( 'Header Menu' ),
	'footer' => wpg__( 'Footer Menu' ),
	'dealmenu' => wpg__( 'Deal Menu' )
	) );
// Custom background
if ( !function_exists( 'custom-background' ) ) {
	add_theme_support( 'custom-background' );
}
//  replace custom-background from body to custom class named takeover
add_theme_support( 'custom-background', array(
    'wp-head-callback' => 'change_custom_background_class',
    'default-color'    => '000000',
    'default-image'    => '', //get_option('theme_mods_WPGroupbuy'),
));
function change_custom_background_class() {
    ob_start();

    _custom_background_cb(); // Default handler

    $style = ob_get_clean();
    $style = str_replace( 'body.custom-background', '.takeover', $style );
    echo $style;
}

// Support translation, put your .mo, .po file in languages/ folder
load_theme_textdomain( WP_Groupbuy::TEXT_DOMAIN, trailingslashit( get_stylesheet_directory() ) . 'languages' );
$locale = get_locale();
$child_locale_file = trailingslashit( STYLESHEETPATH ) . 'languages/'.WP_Groupbuy::TEXT_DOMAIN.'-'.$locale.'.mo';
$locale_file = trailingslashit( get_template_directory() ) . 'languages/'.WP_Groupbuy::TEXT_DOMAIN.'-'.$locale.'.mo';
if ( is_readable( $child_locale_file ) ) {
	load_textdomain( WP_Groupbuy::TEXT_DOMAIN, $child_locale_file );
}
if ( is_readable( $locale_file ) ) {
	load_textdomain( WP_Groupbuy::TEXT_DOMAIN, $locale_file );
}
// Require Files
$required_files = array(
	'/wpg-framework/addons/translate/translator.php',
	'/functions/sidebars.php',
	'/functions/hooks.php',
	'/functions/shortcodes.php',
	'/functions/filters.php',
	'/functions/template-tags.php',
	'/wpg-framework/addons/options/theme-options.php',
	'/wpg-framework/addons/facebook/facebook.class.php',
	'/wpg-framework/addons/subscription/subscriptions.php',
	'/wpg-framework/addons/deal-meta/custom-deal-meta.php',
	'/wpg-framework/addons/deal-lightbox/deal-lightbox.php',
);

foreach ( $required_files as $file ) {
	if ( $template_file = locate_template( $file ) ) {
		include_once $template_file ;
	}
}

function remove_http($input = '') {
	$input = trim($input, '/');
	// If scheme not included, prepend it
	if (!preg_match('#^http(s)?://#', $input)) {
		$input = 'http://' . $input;
	}
	$urlParts = parse_url($input);
	// remove www
	$domain = preg_replace('/^www\./', '', $urlParts['host']);
	$domain.=$urlParts['path'];
	$domain.=$urlParts['query'];
	return $domain;
}
function getFeaturedImg(){
	global $post;
	$error_img = get_bloginfo('template_url').'/style/images/no_img.png';
	$images = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail' ); // Get the image URL
	if ($images) { // if there is featured image
	    return remove_http($images[0]); // get the image
	}else{
	    return remove_http($error_img); // else show error image
	}
}
function getFeaturedImgDeal($deal_id){
	$error_img = get_bloginfo('template_url').'/style/images/no_img.png';
	$images = wp_get_attachment_image_src(get_post_thumbnail_id($deal_id), 'single-post-thumbnail' ); // Get the image URL
	if ($images) {
		return remove_http($images[0]);
	}else{
		return remove_http($error_img);
	}
}
// Redirect to home page, not latest deal
function get_wpg_latest_deal_link_custom () {
	return site_url('/');
}
add_filter('get_wpg_latest_deal_link','get_wpg_latest_deal_link_custom');

// style the password entry form
function custom_password_form() {
	global $post;
	$label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
	$o = '<form action="' . get_option('siteurl') . '/wp-login.php?action=postpass" method="post">
	' . __( "<p>This section of the website is password protected. Please enter your password to continue.</p>" ) . '
	<p><label for="' . $label . '">' . __( "Password:" ) . ' </label><input name="post_password" id="' . $label . '" type="password" size="20" /></p><p><input type="submit" name="Submit" value="' . esc_attr__( "Submit" ) . '" /></p>
</form>
';
return $o;
}
add_filter( 'the_password_form', 'custom_password_form' );
?>