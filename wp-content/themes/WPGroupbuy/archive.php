<?php get_header(); ?>

<div class="main">
  <div class="block-content"> 
    <!-- LEFT SIDE -->
    
    <div class="leftcontents">
      <div class="leftcontents">

        <!-- POST -->
              
              <?php get_template_part( 'loop' ); ?>

        <!-- END POST --> 
      </div>
    </div>
    <!-- END LEFT SIDE --> 
    
    <!-- RIGHT SIDE -->
    <div class="right_contents">
      <?php dynamic_sidebar('blog-sidebar'); ?>
    </div>
    <!-- END RIGHT SIDE -->
    <div class="clear"></div>
  </div>
</div>
		
<?php get_footer(); ?>