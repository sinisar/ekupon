<?php get_header(); ?>

<div class="main">
  <div class="block-content"> 
    <!-- LEFT SIDE -->
    
    <div class="leftcontents">
      <div class="leftcontents">

        <!-- POST -->
             <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?> 
              <div class="main_view">
              <div class="main-boxcontFull">
                <div class="contents">
                <div class="sTitle mTop10"><h2><?php the_title(); ?></h2></div>
                  <div class="blog-post-sub mTop10">
					<?php the_time('F j, Y') ?>
                    /
                    <?php the_category(', '); ?>
                    /
                    <?php if ( comments_open() || '0' != get_comments_number() ) : ?>
                    <?php comments_popup_link( wpg__( 'Leave a comment' ), wpg__( '1 Comment' ), wpg__( '% Comments' ) ); ?>
                    <?php endif; ?>
                    <div class="fr"><?php get_template_part('wpg-framework/inc/social-share') ?></div>
                  </div>
                    <div class="blog-post">
                      <div class="blog-post-thumb">
                      </div>
                      <div>
                        <div class="mBottom20"><?php the_content(); ?></div>
                        <?php if ( comments_open() || '0' != get_comments_number() ) : ?>
                        <div id="blog-comment" class="">
                            <?php comments_template( '', true ); ?>
                        </div>
                        <?php endif; ?>
                      </div>
                    </div>
                  </div>
                  <div class="c"></div>
                </div>
              </div>
            </div>
            <?php endwhile; ?>
        <!-- END POST --> 
      </div>
    </div>
    <!-- END LEFT SIDE --> 
    
    <!-- RIGHT SIDE -->
    <div class="right_contents">
      <?php dynamic_sidebar('blog-sidebar'); ?>
    </div>
    <!-- END RIGHT SIDE -->
    <div class="clear"></div>
  </div>
</div>
		
<?php get_footer(); ?>