<?php if ( ! have_posts() ) : ?>
<div class="main_view">
  <div class="main-boxcontFull">
      <div class="pTop10 pBottom10 fl aleft">
        <div class="pBottom10"><?php wpg_e( 'Opps! We can\'t see what you\'re looking for.' ); ?></div>
        
		<?php get_search_form(); ?>
      </div>
      <div class="c"></div>
    </div>
  </div>
</div>
<?php endif; ?>
<?php while ( have_posts() ) : the_post(); ?>

<div class="main_view">
  <div class="main-boxcontFull">
    <div class="contents pTop10"> <a href="<?php the_permalink() ?>" class="v7linkSold">
      <?php the_title() ?>
      </a>
      <div class="blog-post-sub">
        <?php the_time('F j, Y') ?>
        /
        <?php the_category(', '); ?>
        /
        <?php if ( comments_open() || '0' != get_comments_number() ) : ?>
        <?php comments_popup_link( wpg__( 'Leave a comment' ), wpg__( '1 Comment' ), wpg__( '% Comments' ) ); ?>
        <?php endif; ?>
      </div>
      <div>
        <div class="blog-post">
          <div class="blog-post-thumb"> <a href="<?php the_permalink() ?>">
            <?php if (function_exists('the_post_thumbnail')) { the_post_thumbnail( array( 200, 130 ) ); } ?>
            </a></div>
          <div class="blog-post-content">
            <?php the_excerpt(); ?>
          </div>
        </div>
      </div>
      <div class="c"></div>
    </div>
  </div>
</div>
<?php endwhile; ?>

<?php if (  $wp_query->max_num_pages > 1 ) : ?>
<div class="c"></div>
<div class="v7lineaaafff"></div>
<div class="mBottom20" align="center">
<span class="v7linkSold fixPNG mRight20"><?php previous_posts_link( wpg__( '&larr; Previous Page' ), $wp_query->max_num_pages ); ?></span>
<span class="v7linkSold fixPNG"><?php next_posts_link( wpg__( 'Next Page &rarr;' ), $wp_query->max_num_pages ); ?></span>
</div>
<?php endif; ?>
