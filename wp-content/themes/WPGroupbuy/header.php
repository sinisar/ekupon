<?php 
  do_action("pre_wpg_head");
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>" />
<title><?php
		global $page, $paged;
		wp_title( '-', true, 'right' );
		// Add the blog name.
		bloginfo( 'name' );
		// Add the blog description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			echo " - $site_description";
		// Add a page number if necessary:
		if ( $paged >= 2 || $page >= 2 )
			echo ' ? ' . sprintf( wpg__( 'Page %s' ), max( $paged, $page ) ); 
		?>
</title>
<meta name="description" content="<?php echo $site_description ?>" />
<meta name="keywords" content="wp, wordpress theme, wordpress plugin, wpgroupbuy, groupbuy, group buying, deal site, groupon clone, groupon wordpress theme, groupon wordpress plugin, groupon for wordpress" />
<link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png" />
<meta name="dcterms.rightsHolder" content="Copyright (c) by <?php bloginfo( 'name' ); ?>" />
<meta name="author" content="<?php bloginfo( 'name' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<!-- Facebook Meta Tags -->
<?php if (have_posts()):while(have_posts()):the_post(); endwhile; endif;?>
<meta property="og:site_name" content="<?php bloginfo('name'); ?>" />
<meta property="og:url" content="<?php the_permalink() ?>"/>
<meta property="og:title" content="<?php single_post_title(''); ?>" />
<?php if (is_single()) { ?>
<meta property="og:description" content="<?php echo strip_tags(get_the_excerpt()); ?>" />
<meta property="og:type" content="article" />
<meta property="og:image" content="<?php if ( has_post_thumbnail() ) {
	echo wp_get_attachment_thumb_url(get_post_thumbnail_id($post->ID)); 
	} else {
?><?php wg_header_logo() ?><?php } ?>" />
<?php } else { ?>
<meta property="og:description" content="<?php bloginfo('description'); ?>" />
<meta property="og:type" content="website" />
<meta property="og:image" content="<?php wg_header_logo() ?>" /> 
<?php } ?>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<?php wp_head(); ?>

<!--[if lt IE 9]>
  <script src="//css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
<script type="text/javascript">
  window.___gcfg = {
    parsetags: 'explicit'
  };
</script>
<!--[if IE]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js" 
  type="text/javascript"></script>
<script type="text/javascript">
jQuery(function($){
  $('#ie8 .regMailButton').click(function(event) {
    $('#ie8 #formRegMail').toggle();
  });
});
</script>
<![endif]-->
</head>

<body <?php body_class(); ?>>
<!-- FACEBOOK CODE -->
<div id="fb-root"></div>
<script type="text/javascript">
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '180852078639935', // App ID
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });
    // Additional initialization code here
  };
  // Load the SDK Asynchronously
  (function(d){
     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/en_US/all.js";
     ref.parentNode.insertBefore(js, ref);
   }(document));
</script>
<!-- END FACEBOOK CODE -->
<!-- NOTIFICATION BAR -->
<div id="notification">
  <?php wg_display_messages(); ?>
</div>
<!-- END NOTIFICATION BAR -->
<!-- EMAIL FORM -->
<div id="formRegMail" class="dsp_none">
  <div class="regContent">
    <?php wg_subscription_form(); ?>
    <a href="javascript:void(0)" onclick="shop.header.email.hideFormEmail()" class="bt_hide_email">
    <?php wpg_e( 'Close [ x ]' ) ?>
    </a></div>
</div>
<!-- END EMAIL FORM -->
<div class="wrap">
<!-- HEADER -->
<?php
if ( is_home() || is_front_page() || is_page_template('template-home.php') || is_page_template('template-home-location.php') || is_page_template('template-home-2.php') || is_page_template('template-home-location-2.php') || is_page_template('template-home-2-fullwidth.php') ) {
	get_template_part( 'wpg-framework/inc/home-navigation', 'header' );
} else {
	get_template_part( 'wpg-framework/inc/navigation', 'header' );
} ?>
<!-- END HEADER -->