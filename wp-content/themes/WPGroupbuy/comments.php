<div id="comments">
  <?php if ( post_password_required() ) : ?>
  <p class="nopassword">
    <?php _e( 'This post is password protected. Enter the password to view any comments.', 'wpgroupbuy' ); ?>
  </p>
</div>

<?php
return; endif;
if ( have_comments() ) : ?>
<h4 id="comments-title">
  <?php
	printf( _n( 'One Response to %2$s', '%1$s Responses to %2$s', get_comments_number() ), number_format_i18n( get_comments_number() ), '<em>' . get_the_title() . '</em>' ); ?>
</h4>
<ol class="commentlist">
  <?php
			wp_list_comments( array( 'callback' => 'group_comment' ) );
		?>
</ol>
<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
<div class="navigation">
  <div class="nav-previous">
    <?php previous_comments_link( __( '<span class="meta-nav">&larr;</span> Older Comments', 'wpgroupbuy' ) ); ?>
  </div>
  <div class="nav-next">
    <?php next_comments_link( __( 'Newer Comments <span class="meta-nav">&rarr;</span>', 'wpgroupbuy' ) ); ?>
  </div>
</div>
<!-- .navigation -->
<?php endif; ?>
<?php 
else :
	if ( ! comments_open() ) :
	echo '<p class="nocomments"><?php _e( "Comments are closed.", "wpgroupbuy" ); ?></p>';
	endif;
endif;

	comment_form(); ?>
</div>
<!-- #comments --> 