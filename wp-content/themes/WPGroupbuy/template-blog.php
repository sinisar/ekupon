<?php
/*
Template Name: Blog Template
*/
get_header(); ?>

<div class="main">
  <div class="block-content"> 
    <!-- LEFT SIDE -->
    
    <div class="leftcontents">
      <div class="leftcontents">

        <!-- POST -->
              
              <?php
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$args = array(
					'post_status' => 'publish',
					//'cat' => 3, // Uncomment and add the category ID if you want to filter this loop.
					'paged' => $paged,
					);
				$wp_query = new WP_Query($args);
				get_template_part( 'loop' );
				?>

        <!-- END POST --> 
      </div>
    </div>
    <!-- END LEFT SIDE --> 
    
    <!-- RIGHT SIDE -->
    <div class="right_contents">
      <?php dynamic_sidebar('blog-sidebar'); ?>
    </div>
    <!-- END RIGHT SIDE -->
    <div class="clear"></div>
  </div>
</div>

<?php get_footer(); ?>
