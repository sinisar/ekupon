<?php get_header(); ?>

<div class="main">
  <div class="block-content"> 
    <!-- LEFT SIDE -->
    
    <div class="leftcontents">
      <div class="leftcontents">

        <!-- POST -->
              <div class="main_view">
              <div class="main-boxcontFull">
                <div class="contents">
                <div class="sTitle mTop10"><h2><?php the_title(); ?></h2></div>
                  
                    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

						<?php get_template_part('wpg-framework/inc/loop-single') ?>

						<?php endwhile; ?>
                  </div>
                  <div class="c"></div>
                </div>
              </div>
            </div>
        <!-- END POST --> 
      </div>
    </div>
    <!-- END LEFT SIDE --> 
    
    <!-- RIGHT SIDE -->
    <div class="right_contents">
      <?php dynamic_sidebar('page-sidebar'); ?>
    </div>
    <!-- END RIGHT SIDE -->
    <div class="clear"></div>
  </div>
</div>
		
<?php get_footer(); ?>