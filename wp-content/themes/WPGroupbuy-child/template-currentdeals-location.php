<?php
/*
Template Name: Current Deals (based on user location)
*/

get_header(); ?>

<div class="main home_ver7">
  <div class="main-content">
    <div class="v6ContentCat">
      <div>
        <div class="main-width v7Soldbg ">
          <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
          <?php the_content(); ?>
          <?php wp_link_pages( array( 'before' => '<div class="page-link">' . wpg__( 'Pages:' ), 'after' => '</div>' ) ); ?>
          <?php endwhile;
                	
				$deal_query= null;
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$args=array(
					'post_type' => wg_get_deal_post_type(),
					'post_status' => 'publish',
					'paged' => $paged,
					'meta_query' => array(
						array(
							'key' => '_expiration_date',
							'value' => array(0, current_time('timestamp')),
							'compare' => 'NOT BETWEEN'
						)),
					
				);
				// get prefered location if it's set
				if ( wg_has_location_preference() ) {
					$location = wg_get_preferred_location();
					$args = array_merge( array(wg_get_deal_location_tax() => $location), $args);
				}
					
				$deal_query = new WP_Query($args);
				?>
          <?php if ( ! $deal_query->have_posts() ) : ?>
          <?php get_template_part( 'deal/no-deals', 'deal/index' ); ?>
          <?php endif; ?>
          <?php $count; while ( $deal_query->have_posts() ) : $deal_query->the_post(); $count++; $zebra = ($count % 2) ? ' odd' : ' even'; ?>
          <?php get_template_part( 'wpg-framework/inc/loop-item', 'wpg-framework/inc/deal-item' ); ?>
          <?php endwhile; ?>
          <?php if (  $deal_query->max_num_pages > 1 ) : ?>
          <div class="c"></div>
                <div class="v7lineaaafff"></div>
          			<span class="v7linkSold fixPNG mRight20"><?php next_posts_link( wpg__( 'Next Page &rarr;' ), $deal_query->max_num_pages ); ?></span>
                    <span class="v7linkSold fixPNG"><?php previous_posts_link( wpg__( '&larr; Previous Page' ), $deal_query->max_num_pages ); ?></span>
                    <div class="mBottom20"></div>
          <?php endif; ?>
          <?php wp_reset_query(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php // do_action('wg_above_default_sidebar') ?>
<?php // dynamic_sidebar( 'deals-sidebar' );?>
<?php // do_action('wg_below_default_sidebar') ?>
<?php get_footer(); ?>
