<?php

add_action( 'widgets_init', 'wpg_sidebar_init' );
function wpg_sidebar_init() {
	register_sidebar(
		array(
			'name' => 'Footer Widget Area (col 1 - 320px)',
			'id'            => 'deal_footer_one',
			'description'   => 'Used for the first column of the footer.',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '<div class="clear"></div></div>',
			'before_title' => '<h2 class="widget-title wg_ff">',
			'after_title' => '</h2>'
		)
	);
	register_sidebar(
		array(
			'name' => 'Footer Widget Area (col 2 - 165px)',
			'id'            => 'deal_footer_two',
			'description'   => 'Used for the second column of the footer.',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '<div class="clear"></div></div>',
			'before_title' => '<h2 class="widget-title wg_ff">',
			'after_title' => '</h2>'
		)
	);
	register_sidebar(
		array(
			'name' => 'Footer Widget Area (col 3 - 165px)',
			'id'            => 'deal_footer_three',
			'description'   => 'Used for the third column of the footer.',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '<div class="clear"></div></div>',
			'before_title' => '<h2 class="widget-title wg_ff">',
			'after_title' => '</h2>'
		)
	);
	register_sidebar(
		array(
			'name' => 'Footer Widget Area (col 4 - 165px)',
			'id'            => 'deal_footer_four',
			'description'   => 'Used for the third column of the footer.',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '<div class="clear"></div></div>',
			'before_title' => '<h2 class="widget-title wg_ff">',
			'after_title' => '</h2>'
		)
	);
	register_sidebar(
		array(
			'name' => 'Footer Widget Area (col 5 - 165px)',
			'id'            => 'deal_footer_five',
			'description'   => 'Used for the third column of the footer.',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '<div class="clear"></div></div>',
			'before_title' => '<h2 class="widget-title wg_ff">',
			'after_title' => '</h2>'
		)
	);
	register_sidebar(
		array(
			'name' => 'Cart Sidebar',
			'id'            => 'cart-sidebar',
			'description'   => 'Used on the shopping cart page.',
			'before_widget' => '<div class="main_view"><div class="main-boxcontFull"><div class="mBottom15 pTop10">',
			'after_widget' => '</div></div></div>',
			'before_title' => '<div class="newTitleV6 p0 soldLineNew pBottom10 mBottom10">',
			'after_title' => '</div>'
		)
	);
	register_sidebar(
		array(
			'name' => 'Deal Sidebar',
			'id'            => 'deal-sidebar',
			'description'   => 'Used on the single deal page',
			'before_widget' => '<div class="main_view"><div class="main-boxcontFull"><div class="contents pLeft0">',
			'after_widget' => '</div></div></div>',
			'before_title' => '<h2 class="dotLineNew titleblue14 mTop10">',
			'after_title' => '</h2>'
		)
	);
	/*register_sidebar(
		array(
			'name' => 'Deals Sidebar',
			'id'            => 'deals-sidebar',
			'description'   => 'Used on the deals index pages',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '<div class="clear"></div></div>',
			'before_title' => '<h2 class="widget-title wg_ff">',
			'after_title' => '</h2>'
		)
	);*/
	register_sidebar(
		array(
			'name' => 'Blog Sidebar',
			'id'            => 'blog-sidebar',
			'description'   => 'Used on the order/purchase page and order lookup page',
			'before_widget' => '<div class="main_view"><div class="main-boxcontFull"><div class="contents pLeft0">',
			'after_widget' => '</div></div></div>',
			'before_title' => '<h2 class="dotLineNew titleblue14 mTop10">',
			'after_title' => '</h2>'
		)
	);
	register_sidebar(
		array(
			'name' => 'Page Sidebar',
			'id'            => 'page-sidebar',
			'description'   => 'Used on all pages',
			'before_widget' => '<div class="main_view"><div class="main-boxcontFull"><div class="contents pLeft0">',
			'after_widget' => '</div></div></div>',
			'before_title' => '<h2 class="dotLineNew titleblue14 mTop10">',
			'after_title' => '</h2>'
		)
	);
	register_sidebar(
		array(
			'name' => 'Merchants Sidebar',
			'id'            => 'merchant-sidebar',
			'description'   => 'Used on all merchant directory pages',
			'before_widget' => '<div class="main_view"><div class="main-boxcontFull"><div class="mBottom15 pTop10">',
			'after_widget' => '</div></div></div>',
			'before_title' => '<div class="newTitleV6 p0 soldLineNew pBottom10 mBottom10">',
			'after_title' => '</div>'
		)
	);
	register_sidebar(
		array(
			'name' => 'Account Sidebar',
			'id'            => 'account-sidebar',
			'description'   => 'Used on all user account pages',
			'before_widget' => '<div class="main_view"><div class="main-boxcontFull"><div class="mBottom15 pTop10">',
			'after_widget' => '</div></div></div>',
			'before_title' => '<div class="newTitleV6 p0 soldLineNew pBottom10 mBottom10">',
			'after_title' => '</div>'
		)
	);
	/*register_sidebar(
		array(
			'name' => 'Order Sidebar',
			'id'            => 'order-sidebar',
			'description'   => 'Used on all user order pages',
			'before_widget' => '<div class="main_view"><div class="main-boxcontFull"><div class="mBottom15 pTop10">',
			'after_widget' => '</div></div></div>',
			'before_title' => '<div class="newTitleV6 p0 soldLineNew pBottom10">',
			'after_title' => '</div>'
		)
	);*/
	register_sidebar(
		array(
			'name' => 'Home Sidebar (Featured)',
			'id'            => 'home-sidebar',
			'description'   => 'Small sidebar on home page (featured)',
			'before_widget' => '<div class="box-frame box-suport"><div id="box-suport">',
			'after_widget' => '</div></div>',
			'before_title' => '<b>',
			'after_title' => '</b>'
		)
	);
	register_sidebar(
		array(
			'name' => 'Ads on Home',
			'id'            => 'ads-home',
			'description'   => 'Insert HTML code to show ads on Home page, below the featured deal',
			'before_widget' => '<div class="eventCarBanner mTop15" align="center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);

    register_sidebar(
        array(
            'name' => 'Homepage Banner',
            'id'            => 'homepage-banner',
            'description'   => 'Add urls and text for homepage banner (left and right side banner).',
            'before_widget' => '<div class="homepageBanner" align="center">',
            'after_widget' => '</div>',
            'before_title' => '',
            'after_title' => ''
        )
    );

	register_sidebar(
		array(
			'name' => 'Ads on Deal Single',
			'id'            => 'ads-deal',
			'description'   => 'Insert HTML code to show ads on Deal single page',
			'before_widget' => '<div class="eventCarBanner mTop15" align="center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
}
