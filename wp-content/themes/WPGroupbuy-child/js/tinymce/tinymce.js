/*-----------------------------------------------------------------------------------*/
/*	Add TinyMCE deals_description Button
 /*-----------------------------------------------------------------------------------*/
(function() {
    tinymce.create('tinymce.plugins.deals_description', {
        init : function(ed, url) {
            ed.addButton('deals_description', {
                title : 'Add Latest Deals Description',
                image : url+'/deals.png',
                onclick : function() {
                    ed.selection.setContent('[deals_description content="This is my deals description."]');

                }
            });
        },
        createControl : function(n, cm) {
            return null;
        },
    });
    tinymce.PluginManager.add('deals_description', tinymce.plugins.deals_description);
})();