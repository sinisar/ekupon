/**
 * Created by sinisa on 01/12/2017.
 */

jQuery(document).ready(function($) {
    //alert("inside: " + templateUrl +'-child/custom/authors.php');
    $("#deal_author").autocomplete({ // myevent_name is the id of the textbox on which we are applying auto complete process
        source:templateUrl +'-child/wpg-framework/custom/authors.php', // sunil_results.php is the file having all the results
        minLength:3
    });
});