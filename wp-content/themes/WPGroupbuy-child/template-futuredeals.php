<?php
/*
Template Name: Future Deals
*/

get_header(); ?>

<div class="main home_ver7">
  <div class="main-content">
    <div class="v6ContentCat footerPages">
      <div>
        <div class="main-width v7Soldbg ">
          <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
          <?php the_content(); ?>
          <?php wp_link_pages( array( 'before' => '<div class="page-link">' . wpg__( 'Pages:' ), 'after' => '</div>' ) ); ?>
          <?php endwhile;
                	
				$deal_query= null;
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$args=array(
					'post_type' => wg_get_deal_post_type(),
					'post_status' => 'future',
					'paged' => $paged,
				);
				$deal_query = new WP_Query($args);
				?>
          <?php if ( ! $deal_query->have_posts() ) : ?>
          <?php get_template_part( 'deals/no-deals', 'deals/index' ); ?>
          <?php endif; ?>
          <?php $count; while ( $deal_query->have_posts() ) : $deal_query->the_post(); $count++; $zebra = ($count % 2) ? ' odd' : ' even'; ?>
          <?php get_template_part( 'wpg-framework/inc/loop-item-future', 'wpg-framework/inc/deal-item-future' ); ?>
          <?php endwhile; ?>
          <?php if (  $deal_query->max_num_pages > 1 ) : ?>
          <div class="c"></div>
                <div class="v7lineaaafff"></div>
          <div class="mBottom20" align="center">
          			<span class="v7linkSold fixPNG mRight20"><?php previous_posts_link( wpg__( '&larr; Previous Page' ), $deal_query->max_num_pages ); ?></span>
                    <span class="v7linkSold fixPNG"><?php next_posts_link( wpg__( 'Next Page &rarr;' ), $deal_query->max_num_pages ); ?></span>
                    
                </div>
          <?php endif; ?>
          <?php wp_reset_query(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php // do_action('wg_above_default_sidebar') ?>
<?php // dynamic_sidebar( 'deals-sidebar' );?>
<?php // do_action('wg_below_default_sidebar') ?>
<?php get_footer(); ?>
