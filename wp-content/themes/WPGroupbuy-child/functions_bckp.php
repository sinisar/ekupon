<?php
define( 'WPG_THEME_SLUG', 'WPGroupbuy' );

// functions.php for Child theme

// If you need to remove any features from parent theme, overwrite js, styles... please read more at
// http://wiki.wpgroupbuy.com/knowledgebase/customize-your-theme/ (section 4)
/*
add_action( 'after_setup_theme', 'remove_parent_theme_features', 10 );

function remove_parent_theme_features() {
    // your code here
	
}
*/

function use_parent_theme_stylesheet() {
	// Use the parent theme's stylesheet
	return get_template_directory_uri() . '/style.css';
}

function use_parent_theme_stylesheet_directory()
{
	return get_template_directory_uri();
}

function my_theme_styles() {
	$themeVersion = wp_get_theme()->get('Version');

	// Enqueue our style.css with our own version
	wp_enqueue_style('child-theme-style', get_stylesheet_directory_uri() . '-child/style.css', array(), $themeVersion);
    wp_enqueue_style('child-theme-responsive', get_stylesheet_directory_uri() . '-child/style/responsive.css', array(), $themeVersion);
    wp_enqueue_style('child-theme-shortcodes', get_stylesheet_directory_uri() . '-child/style/shortcodes.css', array(), $themeVersion);
}

// Filter get_stylesheet_uri() to return the parent theme's stylesheet
add_filter('stylesheet_uri', 'use_parent_theme_stylesheet');

//	filter stylesheet_directory_uri and return parent directory
add_filter('stylesheet_directory_uri', 'use_parent_theme_stylesheet_directory');

// Enqueue this theme's scripts and styles (after parent theme)
add_action('wp_enqueue_scripts', 'my_theme_styles', 20);

/* Script, Styles overwrite */
/*
add_action( 'wp_print_scripts', 'child_overwrite_scripts', 100 );
add_action( 'wp_print_styles', 'child_overwrite_styles', 100 );

function child_overwrite_scripts() {
    wp_deregister_script( 'wpg-jquery-template' );
	wp_deregister_script( 'wpg-ul-to-select' );
	wp_deregister_script( 'wpg-admin-tiptip' );
	
	wp_enqueue_script( 'childtheme-jstemplate-script', get_stylesheet_directory_uri() . '/js/jquery.template.js' );
	wp_enqueue_script( 'childtheme-wpg-ul-to-select', get_stylesheet_directory_uri() . '/js/jquery.mobilemenu.js' );
	wp_enqueue_script( 'childtheme-wpg-admin-tiptip', get_stylesheet_directory_uri() . '/js/jquery.tipTip.min.js' );
}
*/
/* Overwrite language files */
/*
load_child_theme_textdomain( WP_Groupbuy::TEXT_DOMAIN, trailingslashit( get_stylesheet_directory() ) . 'languages' );
$locale = get_locale();
$child_locale_file = trailingslashit( STYLESHEETPATH ) . 'languages/'.WP_Groupbuy::TEXT_DOMAIN.'-'.$locale.'.mo';
$locale_file = trailingslashit( get_template_directory() ) . 'languages/'.WP_Groupbuy::TEXT_DOMAIN.'-'.$locale.'.mo';
if ( is_readable( $child_locale_file ) ) {
	load_textdomain( WP_Groupbuy::TEXT_DOMAIN, $child_locale_file );
} elseif ( is_readable( $locale_file ) ) {
	load_textdomain( WP_Groupbuy::TEXT_DOMAIN, $locale_file );
}
*/
/* Overwrite image size */
/*
function child_theme_image_size() {
	add_image_size( 'wpg_48x48', 48, 48, true );
	add_image_size( 'wpg_60x60', 60, 60, true );
	add_image_size( 'wpg_88x88', 88, 88, true );
	add_image_size( 'wpg_208x120', 208, 120, true );
	add_image_size( 'wpg_475x475', 475, 475, true ); // Featured
	add_image_size( 'wpg_290x290', 290, 290, true ); // Deal thumb
	add_image_size( 'wpg_217x217', 217, 217, true ); // Small thumb on home page
	add_image_size( 'wpg_700', 700, 1500, true ); // Deal page size
	add_image_size( 'wpg_300x180', 300, 180, true );
	add_image_size( 'wpg_250x110', 250, 110, true );
	add_image_size( 'wpg_150w', 150, 9999 );
	add_image_size( 'wpg_100x100', 100, 100, true );
	add_image_size( 'wpg_200x130', 200, 130, true );
	add_image_size( 'wpg_200x150', 200, 150, true );
	add_image_size( 'wpg_160x100', 160, 100, true );
}
add_action( 'after_setup_theme', 'child_theme_image_size', 11 );
// Menu
register_nav_menus( array(
	'header_sub' => wpg__( 'Sub-Header Menu' ),
) );
*/
?>
