<?php
/*
Template Name: Full Width Page
*/
get_header(); ?>

<div class="main home_ver7">
  <div class="main-width">
    <div class="block-content"> </div>
  </div>
  <div class="main-content">
    <div class="sContainer box-suport">
      <div class="sWrapper">
        <div class="sTitle">
          <h2><?php the_title() ?></h2>
        </div>
        <div class="sContent">
          <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
          <?php get_template_part('wpg-framework/inc/loop-single') ?>
          <?php endwhile; ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
