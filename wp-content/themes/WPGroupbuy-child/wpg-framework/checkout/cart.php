<table class="listingOrder border_1_ddd border_bt_none border_t_none border_r_none" cellspacing="0" cellpadding="0" width="100%">
	<thead>
	<tr>
		<?php foreach ( $columns as $key => $label ): ?>
			<th class="headProfile noneborderbot bRight cart-<?php esc_attr_e($key); ?>" scope="col"><?php esc_html_e($label); ?></th>
		<?php endforeach; ?>
	</tr>
	</thead>
	<tbody>
	<?php foreach ( $items as $item ): ?>
		<tr class="<?php esc_attr_e($key); ?>">
			<?php foreach ( $columns as $key => $label ): ?>
				<td class="itemProfile bRight cart-<?php esc_attr_e($key); ?>">
					<?php if ( isset($item[$key]) ) { echo $item[$key]; } ?>
				</td>
			<?php endforeach; ?>
		</tr>
	<?php endforeach; ?>
	<?php
	foreach ( $line_items as $key => $line ):
		if (esc_html__($line['data']) != wg_get_formatted_money('0')) :
			?>
			<tr class="<?php esc_attr_e($key); ?>">
				<th class="bBottom bRight" scope="row" colspan="<?php echo count($columns)-1; ?>"><?php esc_html_e($line['label']); ?></th>
				<td class="itemProfile bRight cart-line-item-<?php esc_attr_e($key); ?>"><?php esc_html_e($line['data']); ?></td>
			</tr>
			<?php
		endif;
	endforeach; ?>
	</tbody>
</table>