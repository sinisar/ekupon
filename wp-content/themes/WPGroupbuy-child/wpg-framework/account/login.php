<div id="wpg_login_form" class="pBottom10">
	<form action="<?php wg_account_login_url(); ?>" method="post" id="wpg_login" class="">
		<div class="sTitle"><h2><?php wpg_e('Login to your account') ?></h2></div>
		<table class="fl">
			<tbody>
			<tr>
				<td class="edit-td"><label for="log"><?php wpg_e('Your Username') ?>:</label></td>
				<td><input tabindex="11" type="text" name="log" id="log" class="text-input-sm" /></td>
			</tr>
			<tr>
				<td><label for="pwd"><?php wpg_e('Your Password') ?>:</label></td>
				<td><input tabindex="12" type="password" name="pwd" id="pwd" class="text-input-sm" /></td>
			</tr>
            <tr>
                <td class=""></td>
                <td class="fl">
					<?php wp_nonce_field('wg_login_action','wg_login'); ?>
					<?php do_action('wpg_login_form_fields');
?>
					<label for="rememberme"><input name="rememberme" id="rememberme" type="checkbox" checked="checked" value="forever" /> <?php wpg_e('Keep me signed in'); ?></label>
					<div class="c"></div>
					<?php if ( isset( $args['redirect'] ) ) : ?>
						<input type="hidden" name="redirect_to" value="<?php echo $args['redirect']; ?>" />
					<?php endif; ?>
					<div id="wg_login" class="fl w117"><input type="submit" name="submit" value="<?php wpg_e('Sign In') ?>" class="fl profileAddGoldV6" /></div>
					<div id="fb_login" class="fl mLeft5 w117"><?php do_shortcode('[wg_facebook_button]'); ?></div>
				</td>
			</tr>
			<tr>
				<td></td>
				<td><?php do_action( 'wordpress_social_login' ); ?></td>
			</tr>
            <tr>

				<td><a href="<?php echo wp_lostpassword_url(); ?>" class="forgot_password"><?php wpg_e('Forgot your password&#63;'); ?></a>
					<div class="c"></div>
					<a href="<?php wg_account_register_url() ?>"><?php wpg_e('Don&rsquo;t have an account&#63; Register.'); ?></a>
					<div class="c"></div>

				</td>
			</tr>
			</tbody>
		</table>

	</form>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery("#wpg_login").validate({
				rules: {
					'log': "required",
					'pwd': "required"
				}
			});
			jQuery("#log").focus();
		});
	</script>
</div>
<div class="c"></div>