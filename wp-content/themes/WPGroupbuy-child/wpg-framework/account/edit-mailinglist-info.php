<fieldset id="wg-account-mailinglist-info">
<legend><?php wpg_e('Account Mailinglist Information'); ?></legend>

	<table class="">
		<tbody>
			<?php foreach ( $fields as $key => $data ): ?>
				<tr>
					<?php if ( $data['type'] != 'checkbox' ): ?>
                        <td><?php wg_form_field($key, $data, 'mailinglist'); ?></td>
					<?php else: ?>
						<td colspan="2">
							<label for="wg_account_<?php echo $key; ?>"><?php wg_form_field($key, $data, 'account'); ?> <?php echo $data['label']; ?></label>
						</td>
					<?php endif; ?>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</fieldset> 