<div class="list-rands-over" id="<?php the_ID() ?>">
    <div class="list-rands">
        <div class="box-frame-center">
            <div class="v6ItemHotImg v6hover v7Relative">

                <?php if ( has_post_thumbnail() ): ?>
                    <?php if ( !wg_is_deal_complete() ): ?>
                        <div class="v7price_promotion fixPNG">
                            <?php wg_amount_saved() ?>
                        </div>
                    <?php else : ?>
                        <div class="fixPNG soldout_tag"></div>
                    <?php endif ?>
            <a href="<?php the_permalink() ?>">

                    <?php the_post_thumbnail('wpg_290x290') ?>
            </a>
                <?php else : ?>
                    <?php if ( !wg_is_deal_complete() ): ?>
                        <div class="v7price_promotion fixPNG">
                            <?php wg_amount_saved() ?>
                        </div>
                    <?php else : ?>
                        <div class="fixPNG soldout_tag"></div>
                    <?php endif ?>
            <a href="<?php the_permalink() ?>">

                    <div class="no_img 290"><img src="<?php echo get_template_directory_uri(); ?>/style/images/background/no_img_290.png" /></div>
            </a>
                <?php endif; ?>
                <div class="v7InfoType">
                    <div class="c"></div>
                </div>
            </div>
            <div class="v6ContentHot">
                <div class="titledeals2" align="left">
          <h3 class="m0 titleseoV6"><a href="<?php the_permalink() ?>" title="<?php wpg_e('Read'); ?> <?php the_title() ?>">
                        <?php the_title() ?></h3>
            </a></h3>
                </div>
            </div>
            <div class="v6TopBorder fixPNG">
                <div class="fl v7prices mTop3">
                    <div class="v7PromotionBigSold">
                        <?php wg_price(); ?>
                    </div>
                    <?php if (wg_has_expiration()): ?>
                        <div class="v7bnew mTop10">
                            <?php //wg_deal_countdown( true ) ?>
                            <?php wpg_e('On'); ?> <?php the_date(); ?>
                        </div>
                    <?php endif ?>
                </div>
        <a href="<?php the_permalink() ?>" class="ca-more v7view fr fixPNG mTop20 w45"><?php wpg_e('View'); ?></a>
                <div class="c"></div>
            </div>
        </div>
    </div>
</div>
