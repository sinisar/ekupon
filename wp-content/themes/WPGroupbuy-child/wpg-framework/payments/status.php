<div id="wg-payment-status" xmlns="http://www.w3.org/1999/html">
    <p><strong>Status plačila</strong></p>
    <?php if( wg_is_transaction_in_process($transaction_id) ) { ?>
        <p>
            Prosimo, ne zapirajte vašega brskalnika in počakajte na rezultat procesiranja transakcije. Plačilo je še
            vedno v teku ...
        </p>
    <?php } else { ?>
        <p>
            Pri procesiranju plačila je prišlo do napake. Transakcija je bila <strong>neuspešna</strong>.
            </br>
            Nakup lahko ponovite <a href="<?php echo WP_Groupbuy_Carts::get_url(); ?>">tukaj</a>.
        </p>
    <?php } ?>
</div>