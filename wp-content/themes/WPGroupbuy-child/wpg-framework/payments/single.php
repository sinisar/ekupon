<?php get_header(); ?>
<!-- PAYMENT -->

<div class="main">
    <div class="mainPadding">
        <!-- LEFT CONTENT -->
        <div class="leftMainBox">
            <div class="sell1 mTop10">
                    <div class="profileCus">
                        <div class="profileHeaderTop">
                            <div class="fl profileHeaderTitle">
                                <?php the_title(); ?>
                            </div>
                        </div>
                    </div>

                <div class="main_viewprofile">
                    <div class="">
                        <div class="profileContainer">
                            <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
                                <?php the_content(); ?>
                            <?php endwhile; // end of the loop. ?>

                        </div>
                    </div>
                </div>
                <div class="c"></div>

            </div>
        </div>
        <!-- END LEFT CONTENT -->


        <div class="c"></div>
    </div>
</div>
<!-- END PAYMENT -->

<?php get_footer(); ?>
