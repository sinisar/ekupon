<?php 
	do_action('wg_deal_view');
	get_header(); ?>
<div class="main">
<!-- ADS BANNER -->
<?php dynamic_sidebar( 'ads-deal' ); ?>
<!-- END ADS BANNER -->
  
  <div class="block-content"> 
    <!-- LEFT SIDE -->
    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <div class="leftcontents">
      <div class="leftcontents">
        <div class="main_view">
          <div class="cont-maindear"> 
            <!-- THUMB IMG -->
            <div class="mainimg">
              
              <?php if (wg_get_price() != 0) { ?>
              <div class="v7price_promotion fixPNG mLeft400 mTop10">
                <?php wg_amount_saved() ?>
              </div>
              <?php } else { ?>
              
              <?php } ?>
              
              <?php if ( wg_has_featured_content() ) :?>
              	<?php wg_featured_content(); ?>
              <?php elseif ( has_post_thumbnail() ) : ?>
              	<?php the_post_thumbnail('wpg_475x475'); ?>
              <?php else: ?>
            	<div class="no_img 475"><img src="<?php echo get_stylesheet_directory_uri(); ?>/style/images/background/no_img_475.png" /></div>
              <?php endif; ?>
              </div>
            
            <!-- END THUMB IMG -->
            
            <div class="right-contdear width256">
              <div class="title-maindear">
                <div class="v6TitleShort h70">
                  <h1>
                    <?php the_title(); ?>
                  </h1>
                </div>
              </div>
              <!-- PRICE -->
              <div id="deal_price_box" class="boxPrices">
                <div class="boxPrices_view fixPNG width283"> 
                  <!--<div class="boxPricesPadding">-->
                  <?php if (wg_get_price() != 0) { ?>
                  <div>
                  <div class="v6Price mTop10" align="center">
						<?php wg_price(); ?>
                  </div>
                  <div class="pricebuys v7inlinetype" align="center"><span class="noline"><?php wpg_e('Bought:') ?></span> <?php wg_number_of_purchases() ?> <span class="mLeft10"></span> <?php $max = wg_get_max_purchases(); $bought = wg_get_number_of_purchases(); echo $remain = $max - $bought; ?>/<?php wg_max_purchases(); ?> <span class="noline"><?php wpg_e('available') ?></span> </div></div>
                  
                  <?php } else { ?>
                  <div>
                  <div class="v6Price freedeal mTop10" align="center">
						<?php wpg_e('It\'s Free') ?>
                  </div>
                  </div>
				  <?php } ; ?>
                  
                  <?php if (function_exists('wg_deal_has_attributes') && wg_deal_has_attributes()): ?>
                  <div id="wpg_addcart">
				  <?php wg_add_to_cart_form() ?>
                  </div>
                  <?php else: ?>
                  <div class="v6BuyNow">
                    <?php if ( wg_deal_availability() || !wg_is_deal_complete() ): ?>
                    <a href="<?php wg_add_to_cart_url() ?>"><div class="retinaiconbox buynow_btn"><span class="retinaicon-shopping-cart"></span> <span class="buynow_text"><?php wpg_e('Buy now') ?></span></div></a>
                    <?php elseif ( wg_is_sold_out() ) : ?>
                    <a href="#"><div class="retinaiconbox soldout_btn"><span class="retinaicon-shopping-cart"></span> <span class="buynow_text"><?php wpg_e('Sold out') ?></span></div></a>
                    <?php else : ?>
                    <a href="#"><div class="retinaiconbox soldout_btn"><span class="retinaicon-shopping-cart"></span> <span class="buynow_text"><?php wpg_e('Unavailable') ?></span></div></a>
                    <?php endif ?>
                  </div>
                  <?php endif ?>
                  <!--</div>--> 
                </div>
              </div>
              <!-- END PRICE -->
              <?php if (wg_get_price() == 0) { ?>
              
              <?php } else { ?>
              <div id="deal_info_bar" class="shopinfo">
                <div class="shopBuy" align="center">
                  <div class="shopTitleInfo">
                    <?php wpg_e('Value') ?>
                  </div>
                  <div class="shopMoreInfo">
                    <?php echo str_replace('.00','',wg_get_formatted_money(wg_get_deal_worth())) ?>
                  </div>
                </div>
                <div class="shoppromotion" align="center">
                  <div class="shopTitleInfo">
                    <?php wpg_e('Discount') ?>
                  </div>
                  <div class="shopMoreInfo">
				  <?php wg_amount_saved() ?></div>
                </div>
                <div class="shoptime" align="center">
                  <div class="shopTitleInfo shopTitleTimeInfo">
                    <?php wpg_e('You save') ?>
                  </div>
                  <div class="shopMoreInfo">
                    <?php echo str_replace('.00','',wg_get_formatted_money(wg_get_deal_worth() - wg_get_price())) ?>
                  </div>
                </div>
              </div>
              
              <!-- DYNAMIC PRICE -->
              <?php if (wg_has_dynamic_price()): ?>
              <div id="dynamic_price" class="shopinfo pLeft20 pBottom10 pRight20 hauto">
              	<div class="shopTitleInfo txt_trs_none">
                    <?php wpg_e('More offer') ?> <?php wg_dynamic_prices() ?>
                  </div>
              </div>
              <?php endif ?>
              <!-- END DYNAMIC PRICE --> 
              
              <!-- BAR -->
              <?php
				if ( wg_get_min_purchases() ) {
				$percentage = ( wg_get_number_of_purchases() / wg_get_min_purchases() ) * 100;
				$remaining =  wg_get_min_purchases()-wg_get_number_of_purchases();
				$remaining_message = ( (int)$remaining > 0 ) ? sprintf(wpg__('%s more to get the deal'), $remaining) : wpg__('The deal is on!') ;
				if ( wg_get_number_of_purchases() == 0 ) $percentage = '5';
				?>
              <?php if ($percentage >= 100) { ?>
              <!-- hide it -->
              <?php } else if ($percentage < 100) { ?>  
              <div id="progress_bar" class="wg_ff percentage_<?php echo $percentage ?> clearfix">
                        <div class="progress"></div>
                        <span class="progress_bar_wrap">
                        <span class="progress_bar_progress contrast" style="width:<?php echo $percentage ?>%;"></span>
                        <span class="remaining_tip"><?php echo $remaining_message ?></span></span> </div>
              <?php } } ?>
              <!-- END BAR -->
              <?php }; ?>
              
              <!-- INFO -->
              <?php if ( wg_is_deal_complete()  ) { ?>
              <div class="v6BorderBot">
                <div class="v6BottomPrice">
                  <div class="v6Buyersnew ">
                    <div class="mainBlueBoxNew">
                      <?php if ( wg_is_sold_out() ) : ?>
                      <?php printf(wpg__('This deal reached the maximum amount of %s buyers.'), wg_get_max_purchases() ); ?>
                      <?php elseif ( !wg_deal_availability() && wg_has_expiration() ) : ?>
                      <?php printf(wpg__('You missed this deal!'), wg_get_max_purchases() ); ?>
                      <?php else : ?>
                      <?php printf(wpg__('This deal failed to reach the minimum %s amount of buyers.'), wg_get_min_purchases()); ?>
                      <?php endif; ?>
                    </div>
                  </div>
                </div>
              </div>
              <?php } ?>
              <!-- END INFO --> 
                            
              <?php if (wg_get_price() == 0) { ?>
              
              <?php } else { ?>
              <!-- COUNTDOWN -->
              <?php if ( wg_deal_availability() && wg_has_expiration() ): ?>
              <div class="v6BorderBot border_bt_none">
                <div class="v6Timer">
                  <div class="v6Gray fl"></div>
                  <div class="v6DisplayTime">
                    
                    <?php wg_deal_countdown(true); ?>
                    <noscript>
                    <?php wg_get_deal_end_date(); ?>
                    </noscript>
                    
                  </div>
                  <div class="c"></div>
                </div>
              </div>
              <?php endif ?>
              <!-- END COUNTDOWN --> 
              <?php }; ?>
            </div>
            <div class="c"></div>
            <div class="bottomTopDeal">
            	<div class="fl">
                  <div class="v6ItemHomeHoverTextD">
                    <div class="pageView fl fixPNG"> <?php if(function_exists('the_views')) { the_views(); } ?> </div>
                    <div class="attractive fl fixPNG"> <?php if(function_exists('the_ratings')) { the_ratings(); } ?> </div>
                    <div class="c"></div>
                  </div>
                </div>
              <div class="fr">
                <div class="v6Fb">
                  <div class="ovhide">
                    <?php get_template_part('wpg-framework/inc/social-share') ?>
                  </div>
                </div>
              </div>
              <div class="c"></div>
            </div>
          </div>
        </div>
        <div class="boxdetailAdvan">
          <div class="boximage_view cs_df">
            <div class="contents pTop0">
              <table>
                <tbody>
                  <tr>
                    <td width="65%" valign="top" align="left"><div class="advantagesconditions">
                        <h3 class="title_bold_blueGay m0">
                          <?php wpg_e('Highlights:') ?>
                        </h3>
                        <div class="content_deal mTop10">
                          <?php wg_highlights(); ?>
                        </div>
                      </div></td>
                    <td width="1" bgcolor="#e3e3e3" class="pRight0"></td>
                    <td valign="top" align="left" width="35%"><div class="advantagesconditionsright mLeft10">
                        <h3 class="title_bold_blueGay m0">
                          <?php wpg_e('Fine Print:') ?>
                        </h3>
                        <div class="mTop10 content_deal">
                          <?php wg_fine_print(); ?>
                        </div>
                      </div></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="bot_left_cate fixPng">
            <div class="bot_right_cate fixPng">
              <div class="bot_cen_view"></div>
            </div>
          </div>
        </div>
                
        <!-- DEAL INFO -->
        <div class="main_view">
          <div class="main-boxcontFull">
            <div class="contents">
              <h3 class="title_bold">
                <?php wpg_e('Deal Information') ?>
              </h3>
              <div>
                <?php the_content(); ?>
                <div class="mBottom20"></div>
                <?php if ( wg_has_merchant() ): ?>
                <div class="sTitle mBottom0">
                  <h2>
                    <?php wpg_e('This deal offer by') ?>
                    <a href="<?php wg_merchant_url(wg_get_merchant_id()) ?>" title="<?php wg_merchant_name(wg_get_merchant_id()) ?>">
                    <?php wg_merchant_name(wg_get_merchant_id()); ?>
                    </a></h2>
                </div>
                <div id="" class="blog-post">
                  <div class="blog-post-thumb"> <a href="<?php wg_merchant_url(wg_get_merchant_id()) ?>"> <?php echo get_the_post_thumbnail(wg_get_merchant_id(),'wpg_150w', array('title' => get_the_title())); ?> </a></div>
                  
                  <div class="blog-post-sub">
                    <?php if (wg_has_merchant_website(wg_get_merchant_id())): ?>
                    <a href="<?php wg_merchant_website(wg_get_merchant_id()) ?>">
                    <?php wpg_e('Website') ?>
                    </a> /
                    <?php endif ?>
                    <?php if (wg_has_merchant_facebook(wg_get_merchant_id())): ?>
                    <a href="<?php wg_merchant_facebook(wg_get_merchant_id()) ?>">
                    <?php wpg_e('Facebook') ?>
                    </a> /
                    <?php endif ?>
                    <?php if (wg_has_merchant_twitter(wg_get_merchant_id())): ?>
                    <a href="<?php wg_merchant_twitter(wg_get_merchant_id()) ?>">
                    <?php wpg_e('Twitter') ?>
                    </a>
                    <?php endif ?>
                  </div>
                  <div class="merchant-post-content">
                    <?php if (wg_has_merchant_excerpt(wg_get_merchant_id())): ?>
                    <?php wg_merchant_excerpt(wg_get_merchant_id()); ?>
                    <span class="read_more"><a href="<?php wg_merchant_url(wg_get_merchant_id()) ?>" title="<?php wg_merchant_name(wg_get_merchant_id()) ?>">
                    <?php wpg_e('Read More') ?>
                    </a></span>
                    <?php endif ?>
                  </div>
                </div>
                <?php endif ?>
              </div>
              <div class="c"></div>
            </div>
          </div>
        </div>
        <!-- END DEAL INFO --> 
        
        <!-- LOCATION -->
        <div class="main_view pBottom10">
          <div class="main-boxcontFull">
            <div class="contents">
              <div class="adr_contents">
                
                <div class="title_adr">
                  <div class="title_bold dash_bottom pTop10 pBottom5">
                    <?php wpg_e('Where to use voucher?') ?>
                  </div>
                  <?php wg_deal_voucher_locations(); ?>
                </div>
                <div class="image_map">
                  <?php if ( wg_has_map() ) : ?>
                  <div class="map section clearfix">
                    <div class="section_content">
                      <?php wg_map(); ?>
                    </div>
                  </div>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>
          <div class="clear"></div>
        </div>
        <!-- END LOCATION --> 
        
        <!-- COMMENT -->
        <?php if ( comments_open() || '0' != get_comments_number() ) : ?>
        <div class="main_view">
          <div class="main-boxcontFull">
            <div class="contents">
              <h3 class="title_bold">
                <?php wpg_e('Discuss This Deal') ?>
              </h3>
              <div>
                <?php comments_template( '', true ); ?>
              </div>
              <div class="c"></div>
            </div>
          </div>
        </div>
        <?php endif; ?>
        <!-- END COMMENT --> 
      </div>
    </div>
    <?php endwhile; // end of the loop. ?>
    <!-- END LEFT SIDE --> 
    
    <!-- RIGHT SIDE -->
    <div class="right_contents">
      <?php do_action('wg_above_default_sidebar') ?>
      <?php dynamic_sidebar( 'deal-sidebar' );?>
      <?php do_action('wg_below_default_sidebar') ?>
    </div>
    <!-- END RIGHT SIDE -->
    <div class="clear"></div>
  </div>
</div>
<?php get_footer(); ?>