<?php

define('WP_DEBUG', true);
define('WP_DEBUG_DISPLAY', false);

ini_set('log_errors',TRUE);
//  ini_set('display_errors','On');
ini_set('error_reporting', E_ALL);
ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
//  print dirname(__FILE__) . '/error_log.txt';

$core_file = locate_template( 'core/WPGroupbuy.php' );

if ( $core_file ) {
    if ( !defined( 'WG_CORE' ) ) { // Old version
        if ( defined( 'WG_PATH' ) ) {
            if ( include_once( ABSPATH . 'wp-admin/includes/plugin.php' ) ) {
                deactivate_plugins( 'wpgroupbuy-plugin/WPGroupbuy.php', false, is_network_admin() );
                wp_die( 'Old version of WPGropubuy plugin deactivated, please refresh this page.' );
            } else {
                add_action( 'admin_notices', function() {
                    echo '<div class="update-nag">Please <a href="'.get_admin_url().'plugins.php">Deactivate</a> WPGroupbuy Plugin</div>';
                });
            }
        } else {
            require_once( $core_file );
        }
    }
} elseif (!defined( 'WG_PATH' ) ) {
    $error = activate_plugin( 'wpgroupbuy-plugin/WPGroupbuy.php' );
    if ( $error ) {
        wp_die( 'Please <a href="'.get_admin_url().'plugin-install.php">Install</a> WPGroupbuy Plugin' );
    } else {
        wp_die( 'WPGropubuy plugin is just activated, please refresh this page.' );
    }
}


if ( !defined( 'WG_PATH' ) ) {
    if ( $core_file ) {
        include_once( $core_file );
    } else {
    }
} elseif ( !defined( 'WG_CORE' ) ) {
}
/*
 * TODO: remove - Constant WPG_THEME_NAME already defined in
 *
define( 'WPG_THEME_NAME', 'WPGroupbuy Child Theme' );
define( 'WPG_THEME_SLUG', 'WPGroupbuy-child' );
define( 'WPG_THEME_VERSION', '2.2' );
define( 'WG_THEME_COMPAT_VERSION', '2.2' );
define( 'SS_BASE_URL', get_template_directory_uri() . '/' );
*/

// functions.php for Child theme

// If you need to remove any features from parent theme, overwrite js, styles... please read more at
// http://wiki.wpgroupbuy.com/knowledgebase/customize-your-theme/ (section 4)

add_action( 'after_setup_theme', 'remove_parent_theme_features', 10 );

function remove_parent_theme_features() {
    // your code here
}

function use_parent_theme_stylesheet() {
    // Use the parent theme's stylesheet
    return get_template_directory_uri() . '/style.css';

}

function use_parent_theme_stylesheet_directory()
{
    return get_template_directory_uri();
}

function my_theme_styles() {
    $themeVersion = wp_get_theme()->get('Version');
    // Enqueue our style.css with our own version
    wp_enqueue_style('child-theme-style', get_template_directory_uri() . '-child/style.css', array(), $themeVersion);
    wp_enqueue_style('child-theme-shortcodes', get_template_directory_uri() . '-child/style/shortcodes.css', array(), $themeVersion);
    wp_enqueue_style('child-theme-responsive', get_template_directory_uri() . '-child/style/responsive.css', array(), $themeVersion);

}


// Filter get_stylesheet_uri() to return the parent theme's stylesheet
add_filter('stylesheet_uri', 'use_parent_theme_stylesheet');

//	filter stylesheet_directory_uri and return parent directory
add_filter('stylesheet_directory_uri', 'use_parent_theme_stylesheet_directory');

// Enqueue this theme's scripts and styles (after parent theme)
add_action('wp_enqueue_scripts', 'my_theme_styles', 20);

/* Script, Styles overwrite */

add_action( 'wp_print_scripts', 'child_overwrite_scripts', 100 );
//add_action( 'wp_print_styles', 'child_overwrite_styles', 100 );
function child_overwrite_scripts() {
    //break;
    wp_deregister_script( 'wpg-jquery-template' );
    wp_deregister_script( 'wpg-ul-to-select' );
    wp_deregister_script( 'wpg-admin-tiptip' );

    wp_enqueue_script( 'childtheme-jstemplate-script', get_stylesheet_directory_uri() . '/js/jquery.template.js' );
    wp_enqueue_script( 'childtheme-wpg-ul-to-select', get_stylesheet_directory_uri() . '/js/jquery.mobilemenu.js' );
    wp_enqueue_script( 'childtheme-wpg-admin-tiptip', get_stylesheet_directory_uri() . '/core/resources/js/jquery.tipTip.min.js' );
}
/* Overwrite language files */
/*
load_child_theme_textdomain( WP_Groupbuy::TEXT_DOMAIN, trailingslashit( get_stylesheet_directory() ) . 'languages' );
$locale = get_locale();
$child_locale_file = trailingslashit( STYLESHEETPATH ) . 'languages/'.WP_Groupbuy::TEXT_DOMAIN.'-'.$locale.'.mo';
$locale_file = trailingslashit( get_template_directory() ) . 'languages/'.WP_Groupbuy::TEXT_DOMAIN.'-'.$locale.'.mo';
if ( is_readable( $child_locale_file ) ) {
    load_textdomain( WP_Groupbuy::TEXT_DOMAIN, $child_locale_file );
} elseif ( is_readable( $locale_file ) ) {
    load_textdomain( WP_Groupbuy::TEXT_DOMAIN, $locale_file );
}
*/
/* Overwrite image size */

function child_theme_image_size() {
    add_image_size( 'wpg_640x640', 640, 640, true ); // Featured 640
    add_image_size( 'wpg_640x430', 640, 430, true ); // Featured 640x430
    add_image_size( 'wpg_210x158', 210, 158, true ); // Small thumb on home page
}
add_action( 'after_setup_theme', 'child_theme_image_size', 11 );


function add_meta_tags($meta_tag_key, $meta_tag_value="", $meta_content_tag="", $meta_content_value="") {

    global $post;
    if ( is_single() ) {
        //print "yes";
        $meta = strip_tags( $post->post_content );
        $meta = strip_shortcodes( $meta );
        $meta = str_replace( array("\n", "\r", "\t"), ' ', $meta );
        $meta = substr( $meta, 0, 125 );
        $keywords = get_the_category( $post->ID );
        $metakeywords = '';
        foreach ( $keywords as $keyword ) {
            $metakeywords .= $keyword->cat_name . ", ";
        }
        echo '<meta name="description" content="' . $meta . '" />' . "\n";
        echo '<meta name="keywords" content="' . $metakeywords . '" />' . "\n";
    }
    else{
        //print "not";
        //print_r($post);
        //echo '<meta http-equiv="refresh" content="5">';
        echo '<meta '.$meta_tag_key.'="'.$meta_tag_value.'" '.$meta_content_tag.'="'.$meta_content_value.'" >';
    }
}
add_action( 'wp_head', 'add_meta_tags' , 10, 4 );

//	add additional field for priority on taxonomy terms: location, category
add_action( 'wg_location_add_form_fields', 'add_taxonomy_priority', 10, 2 );
add_action( 'wg_category_add_form_fields', 'add_taxonomy_priority', 10, 2 );

function add_taxonomy_priority($taxonomy) {
    ?><div class="form-field term-priority">
    <label for="tag-priority"><?php _e('Priority/Order', 'wpgroupbuy-plugin'); ?></label>
    <input name="tag-priority" id="tag-priority" type="text" value="" size="40"/>
    <p>Enter the number for priority (order) of terms.</p>
    </div><?php
}
//	save additional field for priority on taxonomy terms: location, category
add_action('created_wg_location', 'save_taxonomy_priority', 10, 2 );
add_action('created_wg_category', 'save_taxonomy_priority', 10, 2 );

function save_taxonomy_priority( $term_id, $tt_id ){
    $group = '1';
    if( isset( $_POST['tag-priority'] ) && '' !== $_POST['tag-priority'] ){
        $group = sanitize_title( $_POST['tag-priority'] );
    }
    add_term_meta( $term_id, 'tag-priority', $group, true );

}
//	edit additional field for priority on taxonomy terms: location, category
add_action( 'wg_location_edit_form_fields', 'edit_taxonomy_priority_field', 10, 2 );
add_action( 'wg_category_edit_form_fields', 'edit_taxonomy_priority_field', 10, 2 );

function edit_taxonomy_priority_field( $term, $taxonomy ){
    // get current term
    $term_priority = get_term_meta( $term->term_id, 'tag-priority', true );
    ?><tr class="form-field tag-priority-wrap">
    <th scope="row"><label for="tag-priority"><?php _e('Priority/Order', 'wpgroupbuy-plugin'); ?></label></th>
    <td><input name="tag-priority" id="tag-priority" type="text" value="<?php echo $term_priority; ?>" size="40"/></td>
    </tr>
    <?php
}
//	update additional field for priority on taxonomy terms: location, category
add_action( 'edited_wg_location', 'update_taxonomy_term_meta', 10, 2 );
add_action( 'edited_wg_category', 'update_taxonomy_term_meta', 10, 2 );

function update_taxonomy_term_meta( $term_id, $tt_id ) {
    $group = '1';
    if( isset( $_POST['tag-priority'] ) && '' !== $_POST['tag-priority'] ) {
        $group = sanitize_title( $_POST['tag-priority'] );
    }
    update_term_meta( $term_id, 'tag-priority', $group );
}
//	add column for list display for additional field for priority on taxonomy terms: location, category
add_filter('manage_edit-wg_location_columns', 'add_taxonomy_priority_column' );
add_filter('manage_edit-wg_category_columns', 'add_taxonomy_priority_column' );

function add_taxonomy_priority_column( $columns ){
    $columns['tag-priority'] = __( 'Priority / Order', 'wpgroupbuy-plugin' );
    return $columns;
}
//	add column content for list display for additional field for priority on taxonomy terms: location, category
add_filter('manage_wg_location_custom_column', 'add_taxonomy_priority_column_content', 10, 3 );
add_filter('manage_wg_category_custom_column', 'add_taxonomy_priority_column_content', 10, 3 );


function posts_for_current_author($query) {
    global $pagenow;

    if( 'edit.php' != $pagenow || !$query->is_admin )
        return $query;

    if( !current_user_can( 'edit_others_posts' ) ) {
        global $user_ID;
        $query->set('author', $user_ID );
    }
    return $query;
}
add_filter('pre_get_posts', 'posts_for_current_author');

function add_taxonomy_priority_column_content( $content, $column_name, $term_id ){

    if( $column_name !== 'tag-priority' ){
        return $content;
    }

    $term_id = absint( $term_id );
    $taxonomy_term = get_term_meta( $term_id, 'tag-priority', true );
    if( !empty( $taxonomy_term ) ){
        $content .= esc_attr( $taxonomy_term );
    }
    return $content;
}
//	make column for list display sortable for additional field for priority on taxonomy terms: location, category
add_filter( 'manage_edit-wg_location_sortable_columns', 'add_taxonomy_priority_column_sortable' );
add_filter( 'manage_edit-wg_category_sortable_columns', 'add_taxonomy_priority_column_sortable' );

function add_taxonomy_priority_column_sortable( $columns ){
    $columns[ 'tag-priority' ] = 'tag-priority';
    return $columns;
}

add_filter( 'terms_clauses', 'taxonomy_priority_orderby_taxonomy', 10, 2 );
function taxonomy_priority_orderby_taxonomy( $clauses, $taxonomies ) {
    global $wpdb;


    if ( ! is_admin() ) {
        return $clauses;
    }

    /*
     *
    SELECT t.*, tt.*, tm.meta_value
    FROM im_terms
    INNER JOIN im_term_taxonomy AS tt ON t.term_id = tt.term_id INNER JOIN im_termmeta AS tm ON t.term_id = tm.term_id
    WHERE tt.taxonomy IN ('wg_category') AND
                        t.term_id NOT IN (10) AND
                        tt.parent = '0' AND
                        tm.meta_key = 'tag-priority'
    ORDER BY CONVERT(SUBSTRING_INDEX(tm.meta_value,'-',-1),UNSIGNED INTEGER) ASC
     */
    if( $taxonomies[0] == 'wg_category' || $taxonomies[0] == 'wg_location') {
        if($_GET['orderby'] == 'tag-priority' && $clauses['orderby'] == 'ORDER BY t.name') {
            $clauses['fields'] .= ', tm.meta_value';
            $clauses['join'] .= " INNER JOIN ". $wpdb->prefix ."termmeta AS tm ON t.term_id = tm.term_id";
            $clauses['where'] .= " AND
						t.term_id NOT IN (10) AND
						tt.parent = '0' AND
						tm.meta_key = 'tag-priority' ";
            $clauses['orderby'] = "ORDER BY CONVERT(SUBSTRING_INDEX(tm.meta_value,'-',-1),UNSIGNED INTEGER)";
            $clauses['order'] = $_GET['order'];
        }
    }
    return $clauses;
}

function get_tax_terms_by_meta($tax, $meta_key) {
    $tax_terms = get_terms(
        $tax, array(
        'hide_empty' => 1,
        'orderby' => 'name',
        'order' => 'ASC',
        'exclude' => wg_get_featured_cat(),
        'parent' => 0,
    ));


    $term_meta_array = array();
    $tax_terms_meta = array();
    if ($tax_terms) {
        //print "yes;";
        foreach ($tax_terms as $tax_term) {
            $term_meta_array = get_term_meta($tax_term->term_id, $meta_key);

            $tax_term = (array) $tax_term;
            $tax_term['meta_key'] = $meta_key;

            $tax_term['meta_value'] = $term_meta_array[0];
            $tax_term = (object)$tax_term;
            $tax_terms_meta[] = $tax_term;
        }
    }
    usort($tax_terms_meta, 'object_cmp');
    return $tax_terms_meta;
}

function object_cmp($value1, $value2) {
    if((int)$value1->meta_value == (int)$value2->meta_value){
        return 0;
    }
    return ((int)$value1->meta_value < (int)$value2->meta_value) ? -1 : 1;
}

function get_tax_terms_orderby_meta_value( $taxonomy ) {
    global $wpdb;

    $query = "SELECT t.*, tt.*, tm.meta_value
	FROM ". $wpdb->prefix ."terms AS t
    INNER JOIN ". $wpdb->prefix ."term_taxonomy AS tt ON t.term_id = tt.term_id 
    INNER JOIN ". $wpdb->prefix ."termmeta AS tm ON t.term_id = tm.term_id
    WHERE tt.taxonomy IN ('".$taxonomy."') AND
	t.term_id NOT IN (10) AND
	tt.parent = '0' AND
	tm.meta_key = 'tag-priority' AND
	tt.count > 0
    ORDER BY CONVERT(SUBSTRING_INDEX(tm.meta_value,'-',-1),UNSIGNED INTEGER) ASC";

    $ordered_tax_terms_meta = $wpdb->get_results($wpdb->prepare($query, array()));
    return $ordered_tax_terms_meta;
}

function display_soliloquy_slider_with_dynamic_images() {
    global $post;

    if (class_exists('Dynamic_Featured_Image')) {
        global $dynamic_featured_image;
        $featured_images = $dynamic_featured_image->get_featured_images($post->ID);
        if(count($featured_images) == 0) {
            return the_post_thumbnail('wpg_640x480');
        }
        $atts = array('slug' =>'deal-slider');
        return soliloquy_slider_shortcode($atts, $featured_images);
    }
}

/**
 * In order to load proper soliloquy slider by slug or id, it has to contain ta least ONE image, otherwise it will load the default one.
 * @param $atts
 * @param $featured_images
 * @return mixed|void
 */
function soliloquy_slider_shortcode( $atts, $featured_images) {

    global $post;
    $soliloquy = new Soliloquy_Shortcode_Lite(); //Soliloquy_Shortcode_Lite::get_instance();

    $slider_id = false;
    if ( empty( $atts ) ) {
        $slider_id = $post->ID;
        $data      = is_preview() ? $soliloquy->base->_get_slider( $slider_id ) : $soliloquy->base->get_slider( $slider_id );
    } else if ( isset( $atts['id'] ) ) {
        $slider_id = (int) $atts['id'];
        $data      = is_preview() ? $soliloquy->base->_get_slider( $slider_id ) : $soliloquy->base->get_slider( $slider_id );
    } else if ( isset( $atts['slug'] ) ) {
        $slider_id = $atts['slug'];
        $data      = is_preview() ? $soliloquy->base->_get_slider_by_slug( $slider_id ) : $soliloquy->base->get_slider_by_slug( $slider_id );
    } else {
        // A custom attribute must have been passed. Allow it to be filtered to grab data from a custom source.
        $data = apply_filters( 'soliloquy_custom_slider_data', false, $atts, $post );
    }

    //	create property slider
    /*
    Array
    (
        [0] => Array
        (
            [thumb] => http://razvoj.e-kupon.si/izkoristime/wp-content/uploads/miska-1-150x127.jpeg
            [full] => http://razvoj.e-kupon.si/izkoristime/wp-content/uploads/miska-1.jpeg
            [attachment_id] => 16348
        )

    [1] => Array
    (
        [thumb] => http://razvoj.e-kupon.si/izkoristime/wp-content/uploads/d32d55baed7eae2d023509818323b9ad-150x150.jpg
            [full] => http://razvoj.e-kupon.si/izkoristime/wp-content/uploads/d32d55baed7eae2d023509818323b9ad.jpg
            [attachment_id] => 1866
        )

    )

    Slider:
    [slider] => Array
        (
            [17573] => Array
                (
                    [status] => pending
                    [src] => http://razvoj.e-kupon.si/izkoristime/wp-content/uploads/14362592_1274591752559965_6421337781566380031_o.jpg
                    [title] => 14362592_1274591752559965_6421337781566380031_o
                    [link] =>
                    [alt] => 14362592_1274591752559965_6421337781566380031_o
                    [caption] =>
                    [type] => image
                )

        )

    */
    $slider = array();
    $slider_inner_array = array();
    $attachment_id = 0;

    //  print "content of featured images: ";
    //  print_r($featured_images);

    foreach($featured_images as $featured_image) {
        $attachment_id = $featured_image['attachment_id'];
        $title = basename($featured_image['full']);
        $title = basename($featured_image['full']);
        $slider_inner_array['status'] = 'active';
        $slider_inner_array['src'] = $featured_image['medium'];

        $slider_inner_array['title'] = $title;
        $slider_inner_array['link'] = '';
        $slider_inner_array['alt'] = $title;
        $slider_inner_array['caption'] = '';
        $slider_inner_array['type'] = 'image';
        $slider[$attachment_id] = $slider_inner_array;
    }
    $data['slider'] = $slider;

    // If there is no data and the attribute used is an ID, try slug as well.
    if ( ! $data && isset( $atts['id'] ) ) {
        $slider_id = $atts['id'];
        $data      = is_preview() ? $soliloquy->base->_get_slider_by_slug( $slider_id ) : $soliloquy->base->get_slider_by_slug( $slider_id );
    }

    // Allow the data to be filtered before it is stored and used to create the slider output.
    $data = apply_filters( 'soliloquy_pre_data', $data, $slider_id );

    // If there is no data to output or the slider is inactive, do nothing.
    if ( ! $data || empty( $data['slider'] ) || isset( $data['status'] ) && 'inactive' == $data['status'] && ! is_preview() ) {
        return false;
    }

    // If the data is to be randomized, do it now.
    if ( $soliloquy->get_config( 'random', $data ) ) {
        $data = $soliloquy->shuffle( $data );
    }

    $soliloquy->data[$data['id']] = $data;
    $slider                  = '';
    $i                       = 1;

    // If this is a feed view, customize the output and return early.
    if ( is_feed() ) {
        return $soliloquy->do_feed_output( $data );
    }

    // Load scripts and styles.
    wp_enqueue_style( $soliloquy->base->plugin_slug . '-style' );
    wp_enqueue_script( $soliloquy->base->plugin_slug . '-script' );

    // Load custom slider themes if necessary.
    if ( 'base' !== $soliloquy->get_config( 'slider_theme', $data ) ) {
        $soliloquy->load_slider_theme( $soliloquy->get_config( 'slider_theme', $data ) );
    }

    // Load slider init code in the footer.
    add_action( 'wp_footer', array( $soliloquy, 'slider_init' ), 1000 );
    // Run a hook before the slider output begins but after scripts and inits have been set.
    do_action( 'tmp_before_output', $data );

    // Apply a filter before starting the slider HTML.
    $slider = apply_filters( 'soliloquy_output_start', $slider, $data );

    // Build out the slider HTML.
    $slider .= '<div aria-live="' . $soliloquy->get_config( 'aria_live', $data ) . '" id="soliloquy-container-' . sanitize_html_class( $data['id'] ) . '" class="' . $soliloquy->get_slider_classes( $data ) . '" style="max-width:' . $soliloquy->get_config( 'slider_width', $data ) . 'px;max-height:' . $soliloquy->get_config( 'slider_height', $data ) . 'px;' . apply_filters( 'soliloquy_output_container_style', '', $data ) . '"' . apply_filters( 'soliloquy_output_container_attr', '', $data ) . '>';
    $slider .= '<ul id="soliloquy-' . sanitize_html_class( $data['id'] ) . '" class="soliloquy-slider soliloquy-slides soliloquy-wrap soliloquy-clear">';
    $slider = apply_filters( 'soliloquy_output_before_container', $slider, $data );

    foreach ($data['slider'] as $id => $item ) {
        // Skip over images that are pending (ignore if in Preview mode).
        if ( isset( $item['status'] ) && 'pending' == $item['status'] && ! is_preview() ) {
            continue;
        }

        // Allow filtering of individual items.
        $item     = apply_filters( 'soliloquy_output_item_data', $item, $id, $data, $i );
        $slider   = apply_filters( 'soliloquy_output_before_item', $slider, $id, $item, $data, $i );
        $output   = '<li aria-hidden="true" class="' . $soliloquy->get_slider_item_classes( $item, $i, $data ) . '"' . apply_filters( 'soliloquy_output_item_attr', '', $id, $item, $data, $i ) . ' draggable="false" style="list-style:none">';
        $output .= $soliloquy->get_slide( $id, $item, $data, $i );
        $output .= '</li>';
        $output  = apply_filters( 'soliloquy_output_single_item', $output, $id, $item, $data, $i );
        $slider .= $output;
        $slider  = apply_filters( 'soliloquy_output_after_item', $slider, $id, $item, $data, $i );
        // Increment the iterator.
        $i++;
    }

    $slider = apply_filters( 'soliloquy_output_after_container', $slider, $data );
    $slider .= '</ul>';
    $slider  = apply_filters( 'soliloquy_output_end', $slider, $data );
    $slider .= '</div>';

    // Increment the counter.
    $soliloquy->counter++;

    // Add no JS fallback support.
    $no_js   = apply_filters( 'soliloquy_output_no_js', '<noscript><style type="text/css">#soliloquy-container-' . sanitize_html_class( $data['id'] ) . '{opacity:1}</style></noscript>', $data );
    $slider .= $no_js;

    // Return the slider HTML.
    return apply_filters( 'soliloquy_output', $slider, $data );

}

add_filter( 'wp_dropdown_users_args', 'change_user_dropdown', 10, 2 );

function change_user_dropdown( $query_args, $r='' ) {
    $screen = get_current_screen();
    // list users whose role is e.g. 'Merchant' for 'post' post type
    if( $screen->post_type == WP_Groupbuy_Deal::POST_TYPE ):
        $query_args['role'] = array('Merchant');
        // unset default role
        unset( $query_args['who'] );
    endif;

    return $query_args;
}

function searchfilter($query) {

    if ($query->is_search && !is_admin() ) {
        $query->set('post_type', array('wg_deal'));
    }

    return $query;
}

add_filter('pre_get_posts','searchfilter');

/*
//  reselt salt after password reset
function reset_salt() {
    print "inside";
    break;

    $user_id = ( isset($_GET['user_id'] ) ? $_GET['user_id'] : '' );
    update_user_meta($user_id, '_user_salt', '');
}

do_action('after_password_reset', 'reset_salt', 10, 2 );
*/
add_filter( 'send_password_change_email', '__return_false');

?>